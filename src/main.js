global.riot = require("riot");
require("riot-hot-reload");
//riot.reload('my-component')
require('riot-routehandler');

var routes = require("./router").default;
import "./common/common-index";
import "./components/components-index";
import "./views/views-index";
//import "./common/routehandler";


riot.router = riot.mount("routehandler", {
    routes: routes,
    options: {
        hashbang: true,
        base: "/#"
    }
});