var auth = function (ctx, next, page) {
    var logindata = localStorage.getItem('login');
    if (logindata) return next();

    page.redirect('/login')
}
var routes = [{
        route: "/",
        tag: "login"
    }, {
        route: "/login",
        tag: "login"
    },
    {
        route: "/logout",
        tag: "logout"
    },
    {
        route: "/app/*",
        use: auth
    },
    {
        route: "/app/",
        tag: "app",
        routes: [{
                route: "/",
                tag: "home"
            },
            {
                route: "/partner/list",
                tag: "partner-list"
            },
            {
                route: "/partner/item/:id",
                tag: "partner-item",
                use: function (ctx, next, page) {
                    $.ajaxsys({
                        url: "/api/partner/get/",
                        data: {
                            obj: JSON.stringify({
                                id: ctx.params.id
                            })
                        },
                        success: function (data) {
                            page.data = data;
                            next();
                        }
                    })

                }
            },
            {
                route: "/gift/list",
                tag: "gift-list"
            },
            {
                route: "/gift/item/:id",
                tag: "gift-item",
                use: function (ctx, next, page) {
                    $.ajaxsys({
                        url: "/api/gift/get/",
                        data: {
                            obj: JSON.stringify({
                                id: ctx.params.id
                            })
                        },
                        success: function (data) {
                            page.data = data;
                            next();
                        }
                    })

                }
            },
            {
                route: "/consultation/list",
                tag: "consultation-list"
            },
            {
                route: "/consultation/item/:id",
                tag: "consultation-item",
                use: function (ctx, next, page) {

                    $.ajaxsys({
                        url: "/api/consultations/get/",
                        data: {
                            obj: JSON.stringify({
                                id: ctx.params.id
                            })
                        },
                        success: function (data) {
                            page.data = data;
                            next();
                        }
                    })

                }
            }
        ]
    },
];


export default routes;

//https://www.npmjs.com/package/riot-routehandler