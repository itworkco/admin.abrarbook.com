global.baseurl = 'https://www.abrarbook.com';
global.baseimgurl = 'https://www.abrarbook.com/';
$.ajaxSetup({
    url: baseurl,
    type: 'POST',
    crossDomain: true,
    dataType: 'json',
    cache: true,
    global: false,
    beforeSend: function (xhr, settings) {
        settings.url = baseurl + settings.url;
        if (!settings.noloader) {
            NProgress.start();
        }
    },
    complete: function () {
        NProgress.done();
    }
});

$.ajaxsys = function (parm) {
    var u = localStorage.getItem('login');
    u = u ? JSON.parse(u) : {};
    if (u) parm.data.authkey = u.authkey;
    parm.type = "POST";
    parm.cache = false;
    $.ajax(parm);
}

$.validator.setDefaults({
    errorElement: "em",
    errorPlacement: function (error, element) {
        error.addClass("help-block").css({
            'margin': 0
        });
        if (element.prop("type") === "checkbox") {
            error.insertAfter(element.parent("label"));
        } else {
            element.parents("[class^='form-group']:first").find('label').append(error)

        }
    },
    highlight: function (element, errorClass, validClass) {
        $(element).parents("[class^='form-group']:first").addClass("has-error").removeClass("has-success");
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).parents("[class^='form-group']:first").addClass("has-success").removeClass("has-error");
    },
    invalidHandler: function (event, validator) {
        var errors = validator.numberOfInvalids();
        $.toast({
            text: "يوجد " + errors + " حقول بها اخطاء برجاء مرجعتها.",
            icon: 'error'
        })
    }
});

//  $.validator.addMethod("regex",
//      function (value, element, regexp) {
//          var re = new RegExp(regexp);
//          return this.optional(element) || re.test(value);
//      }, "قيمة الحقل غير صحيحة");


//  $.validator.addMethod(
//      "intltelinput",
//      function (value, element, val) {
//          return this.optional(element) || $(element).intlTelInput('isValidNumber');
//      }, "رقم الهاتف غير صحيح");

$.extend(true, $.fn.dataTable.defaults, {

    //  rowReorder: {
    //      selector: ':not(.control)'
    //  },
    // dom: "<'row'<'col-sm-6'f><'col-sm-6'l>>" +
    //     "<'row'<'col-sm-12'tr>>" +
    //     "<'row'<'col-sm-5'p><'col-sm-7'i>>",
    "language": {
        "sEmptyTable": "لا يوجد بيانات",
        "sProcessing": "جارٍ التحميل...",
        "sLengthMenu": "أظهر _MENU_ مدخلات",
        "sZeroRecords": "لم يعثر على أية سجلات",
        "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
        "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
        "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
        "sInfoPostFix": "",
        "sSearch": "",
        "sUrl": "",
        "oPaginate": {
            "sFirst": "الأول",
            "sPrevious": "السابق",
            "sNext": "التالي",
            "sLast": "الأخير"
        }
    },
    searching: false,
    "paging": true,
    "processing": true,
    "serverSide": true,
    "destroy": true,
    "iDisplayStart": 0,
    "iDisplayLength": 100,
    bLengthChange: false,
    "bSort": false,
    "ajax": {
        type: 'POST',
        dataType: 'json',
        beforeSend: function (xhr, settings) {
            settings.url = baseurl + settings.url;

            var str = settings.data;
            var prms = str.split("&").reduce(function (prev, curr, i, arr) {
                var p = curr.split("=");
                prev[decodeURIComponent(p[0])] = decodeURIComponent(p[1]);
                return prev;
            }, {});

            var o = prms.obj ? JSON.parse(prms.obj) : {};
            o.length = prms.length;
            o.start = prms.start;
            if (!o.key) o.key = prms["search[value]"];

            var u = localStorage.getItem('login');
            u = u ? JSON.parse(u) : {};

            settings.data = $.param({
                obj: JSON.stringify(o),
                authkey: u.authkey
            })

        },
        dataFilter: function (response) {
            var j = JSON.parse(response);
            var ret = {};
            ret.data = j;
            if (j.length == 0) {
                ret.recordsTotal = 0;
                ret.recordsFiltered = 0;
            } else {
                ret.recordsTotal = j[0].totalcount;
                ret.recordsFiltered = j[0].totalcount;
            }
            return JSON.stringify(ret);
        }
    }
});

$.toast.options = {
    showHideTransition: 'slide',
    hideAfter: 5000,
    loader: false,
    allowToastClose: true,
    stack: 3,
    position: 'bottom-left'
}

swal.setDefaults({
    showCancelButton: true,
    confirmButtonColor: "#5cb85c",
    cancelButtonColor: "#d9534f",
    confirmButtonText: "نعم",
    cancelButtonText: "إلغاء",
    allowOutsideClick: false
});

//  Flatpickr.l10ns.default.firstDayOfWeek = 6;
//  Flatpickr.localize(Flatpickr.l10ns.ar);





//moment.updateLocale('"en-us"', {});

global.clsForm = {
    getXmlData: function (selector) {
        var j = [{}];
        $(selector).find(".val_xml").each(function (index, ctr) {
            j[0][$(ctr).attr('data-base')] = clsForm.getValue($(ctr));
        });
        var x2js = new X2JS();
        return x2js.json2xml_str({
            Item: j
        });
    },
    getControls: function (elem, attr) {
        if (!attr) {
            attr = 'data-base';
        }
        var formdata = {};
        $(elem).find(".val[" + attr + "]").each(function (index, ctr) {
            var rel = $(ctr).attr(attr).toLowerCase();
            var type = $(ctr).attr("type");
            if ($(ctr).is('select') || type == 'ctr-select') {
                formdata[rel + '_text'] = clsForm.getText($(ctr));
            }
            formdata[rel] = clsForm.getValue($(ctr));
        });
        return formdata;
    },
    getText: function ($obj) {
        var type = $obj.attr("type");
        if ($obj.is('select')) {
            var text = $obj.find('option:selected')
            if (text.length > 0) {
                if ($obj.attr('multiple') && typeof text.text() == "object") {
                    return text.text().join(',');
                }
                return text.text();
            }
        } else if (type == 'ctr-select') {
            return $obj[0].text();
        }
    },
    getValue: function ($obj) {
        var type = $obj.attr("type");
        if (type == "checkbox") {
            return $obj.is(":checked");
        } else if ($obj.data('select2')) {
            if ($obj.attr('multiple')) {
                if ($obj.select2('val')) {
                    return $obj.select2('val').join(',');
                }
            }
            return $obj.select2('val');
        } else if ($obj.is('select')) {
            if ($obj.attr('multiple')) {
                return $obj.val().join(',')
            }
            return $obj.val();
        } else if ($obj.is(':input')) return $obj.val();
        else if ($obj[0]._tag) return $obj[0]._tag.val();
        else if ($obj[0].val != undefined) return $obj[0].val();
        else if (!type) {
            return $obj.html();
        }
    },
    setControl: function (FormSelector, json, attr) {
        if (!attr) {
            attr = 'data-base';
        }
        for (var key in json) {

            var elem = $(FormSelector).find("[" + attr + "='" + key + "']");

            var type = elem.attr("type");
            if (type == "checkbox" || type == "radio") {
                if (json[key] == 1) {
                    elem.attr('checked', 'checked');
                }
            } else if (elem.is("input") || elem.is("textarea")) {
                elem.val(json[key]);
            } else if (elem.is("select")) {
                if (elem.data('select2')) {
                    elem.select2('val', json[key]);
                } else {
                    elem.find('option').each(function (indx, opt) {
                        if (typeof json[key] === 'string' || typeof json[key] === 'integer') {
                            if ($(opt).val() == json[key]) {
                                $(opt).attr('selected', 'selected');
                            }
                        } else {
                            json[key].map(function (v) {
                                if ($(opt).val() == v) {
                                    $(opt).attr('selected', 'selected');
                                }
                            });
                        }
                    });
                }
            } else if (elem.length && elem[0]._tag) elem[0]._tag.val(json[key]);
            else {
                elem.html(json[key]);
            }
        }

    }
}

global.clsFile = {
    getName: function (file) {
        var f = file.replace(/^.*(\\|\/|\:)/, '');
        return f.replace('.' + clsFile.getExtension(file), '');
    },
    getExtension: function (file) {
        return file.replace(/^.*?\.([a-zA-Z0-9]+)$/, "$1");
    },
    download: function (path) {
        /* download functio = download_file */
        $('#down_fram').remove();
        $("body").append("<iframe id='down_fram' src='" + clsAjax.api + "?action=download_file&path=" + path + "' style='display: none;'></iframe>");
    }
}