(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
(function (process){
(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global.page = factory());
}(this, (function () { 'use strict';

var isarray = Array.isArray || function (arr) {
  return Object.prototype.toString.call(arr) == '[object Array]';
};

/**
 * Expose `pathToRegexp`.
 */
var pathToRegexp_1 = pathToRegexp;
var parse_1 = parse;
var compile_1 = compile;
var tokensToFunction_1 = tokensToFunction;
var tokensToRegExp_1 = tokensToRegExp;

/**
 * The main path matching regexp utility.
 *
 * @type {RegExp}
 */
var PATH_REGEXP = new RegExp([
  // Match escaped characters that would otherwise appear in future matches.
  // This allows the user to escape special characters that won't transform.
  '(\\\\.)',
  // Match Express-style parameters and un-named parameters with a prefix
  // and optional suffixes. Matches appear as:
  //
  // "/:test(\\d+)?" => ["/", "test", "\d+", undefined, "?", undefined]
  // "/route(\\d+)"  => [undefined, undefined, undefined, "\d+", undefined, undefined]
  // "/*"            => ["/", undefined, undefined, undefined, undefined, "*"]
  '([\\/.])?(?:(?:\\:(\\w+)(?:\\(((?:\\\\.|[^()])+)\\))?|\\(((?:\\\\.|[^()])+)\\))([+*?])?|(\\*))'
].join('|'), 'g');

/**
 * Parse a string for the raw tokens.
 *
 * @param  {String} str
 * @return {Array}
 */
function parse (str) {
  var tokens = [];
  var key = 0;
  var index = 0;
  var path = '';
  var res;

  while ((res = PATH_REGEXP.exec(str)) != null) {
    var m = res[0];
    var escaped = res[1];
    var offset = res.index;
    path += str.slice(index, offset);
    index = offset + m.length;

    // Ignore already escaped sequences.
    if (escaped) {
      path += escaped[1];
      continue
    }

    // Push the current path onto the tokens.
    if (path) {
      tokens.push(path);
      path = '';
    }

    var prefix = res[2];
    var name = res[3];
    var capture = res[4];
    var group = res[5];
    var suffix = res[6];
    var asterisk = res[7];

    var repeat = suffix === '+' || suffix === '*';
    var optional = suffix === '?' || suffix === '*';
    var delimiter = prefix || '/';
    var pattern = capture || group || (asterisk ? '.*' : '[^' + delimiter + ']+?');

    tokens.push({
      name: name || key++,
      prefix: prefix || '',
      delimiter: delimiter,
      optional: optional,
      repeat: repeat,
      pattern: escapeGroup(pattern)
    });
  }

  // Match any characters still remaining.
  if (index < str.length) {
    path += str.substr(index);
  }

  // If the path exists, push it onto the end.
  if (path) {
    tokens.push(path);
  }

  return tokens
}

/**
 * Compile a string to a template function for the path.
 *
 * @param  {String}   str
 * @return {Function}
 */
function compile (str) {
  return tokensToFunction(parse(str))
}

/**
 * Expose a method for transforming tokens into the path function.
 */
function tokensToFunction (tokens) {
  // Compile all the tokens into regexps.
  var matches = new Array(tokens.length);

  // Compile all the patterns before compilation.
  for (var i = 0; i < tokens.length; i++) {
    if (typeof tokens[i] === 'object') {
      matches[i] = new RegExp('^' + tokens[i].pattern + '$');
    }
  }

  return function (obj) {
    var path = '';
    var data = obj || {};

    for (var i = 0; i < tokens.length; i++) {
      var token = tokens[i];

      if (typeof token === 'string') {
        path += token;

        continue
      }

      var value = data[token.name];
      var segment;

      if (value == null) {
        if (token.optional) {
          continue
        } else {
          throw new TypeError('Expected "' + token.name + '" to be defined')
        }
      }

      if (isarray(value)) {
        if (!token.repeat) {
          throw new TypeError('Expected "' + token.name + '" to not repeat, but received "' + value + '"')
        }

        if (value.length === 0) {
          if (token.optional) {
            continue
          } else {
            throw new TypeError('Expected "' + token.name + '" to not be empty')
          }
        }

        for (var j = 0; j < value.length; j++) {
          segment = encodeURIComponent(value[j]);

          if (!matches[i].test(segment)) {
            throw new TypeError('Expected all "' + token.name + '" to match "' + token.pattern + '", but received "' + segment + '"')
          }

          path += (j === 0 ? token.prefix : token.delimiter) + segment;
        }

        continue
      }

      segment = encodeURIComponent(value);

      if (!matches[i].test(segment)) {
        throw new TypeError('Expected "' + token.name + '" to match "' + token.pattern + '", but received "' + segment + '"')
      }

      path += token.prefix + segment;
    }

    return path
  }
}

/**
 * Escape a regular expression string.
 *
 * @param  {String} str
 * @return {String}
 */
function escapeString (str) {
  return str.replace(/([.+*?=^!:${}()[\]|\/])/g, '\\$1')
}

/**
 * Escape the capturing group by escaping special characters and meaning.
 *
 * @param  {String} group
 * @return {String}
 */
function escapeGroup (group) {
  return group.replace(/([=!:$\/()])/g, '\\$1')
}

/**
 * Attach the keys as a property of the regexp.
 *
 * @param  {RegExp} re
 * @param  {Array}  keys
 * @return {RegExp}
 */
function attachKeys (re, keys) {
  re.keys = keys;
  return re
}

/**
 * Get the flags for a regexp from the options.
 *
 * @param  {Object} options
 * @return {String}
 */
function flags (options) {
  return options.sensitive ? '' : 'i'
}

/**
 * Pull out keys from a regexp.
 *
 * @param  {RegExp} path
 * @param  {Array}  keys
 * @return {RegExp}
 */
function regexpToRegexp (path, keys) {
  // Use a negative lookahead to match only capturing groups.
  var groups = path.source.match(/\((?!\?)/g);

  if (groups) {
    for (var i = 0; i < groups.length; i++) {
      keys.push({
        name: i,
        prefix: null,
        delimiter: null,
        optional: false,
        repeat: false,
        pattern: null
      });
    }
  }

  return attachKeys(path, keys)
}

/**
 * Transform an array into a regexp.
 *
 * @param  {Array}  path
 * @param  {Array}  keys
 * @param  {Object} options
 * @return {RegExp}
 */
function arrayToRegexp (path, keys, options) {
  var parts = [];

  for (var i = 0; i < path.length; i++) {
    parts.push(pathToRegexp(path[i], keys, options).source);
  }

  var regexp = new RegExp('(?:' + parts.join('|') + ')', flags(options));

  return attachKeys(regexp, keys)
}

/**
 * Create a path regexp from string input.
 *
 * @param  {String} path
 * @param  {Array}  keys
 * @param  {Object} options
 * @return {RegExp}
 */
function stringToRegexp (path, keys, options) {
  var tokens = parse(path);
  var re = tokensToRegExp(tokens, options);

  // Attach keys back to the regexp.
  for (var i = 0; i < tokens.length; i++) {
    if (typeof tokens[i] !== 'string') {
      keys.push(tokens[i]);
    }
  }

  return attachKeys(re, keys)
}

/**
 * Expose a function for taking tokens and returning a RegExp.
 *
 * @param  {Array}  tokens
 * @param  {Array}  keys
 * @param  {Object} options
 * @return {RegExp}
 */
function tokensToRegExp (tokens, options) {
  options = options || {};

  var strict = options.strict;
  var end = options.end !== false;
  var route = '';
  var lastToken = tokens[tokens.length - 1];
  var endsWithSlash = typeof lastToken === 'string' && /\/$/.test(lastToken);

  // Iterate over the tokens and create our regexp string.
  for (var i = 0; i < tokens.length; i++) {
    var token = tokens[i];

    if (typeof token === 'string') {
      route += escapeString(token);
    } else {
      var prefix = escapeString(token.prefix);
      var capture = token.pattern;

      if (token.repeat) {
        capture += '(?:' + prefix + capture + ')*';
      }

      if (token.optional) {
        if (prefix) {
          capture = '(?:' + prefix + '(' + capture + '))?';
        } else {
          capture = '(' + capture + ')?';
        }
      } else {
        capture = prefix + '(' + capture + ')';
      }

      route += capture;
    }
  }

  // In non-strict mode we allow a slash at the end of match. If the path to
  // match already ends with a slash, we remove it for consistency. The slash
  // is valid at the end of a path match, not in the middle. This is important
  // in non-ending mode, where "/test/" shouldn't match "/test//route".
  if (!strict) {
    route = (endsWithSlash ? route.slice(0, -2) : route) + '(?:\\/(?=$))?';
  }

  if (end) {
    route += '$';
  } else {
    // In non-ending mode, we need the capturing groups to match as much as
    // possible by using a positive lookahead to the end or next path segment.
    route += strict && endsWithSlash ? '' : '(?=\\/|$)';
  }

  return new RegExp('^' + route, flags(options))
}

/**
 * Normalize the given path string, returning a regular expression.
 *
 * An empty array can be passed in for the keys, which will hold the
 * placeholder key descriptions. For example, using `/user/:id`, `keys` will
 * contain `[{ name: 'id', delimiter: '/', optional: false, repeat: false }]`.
 *
 * @param  {(String|RegExp|Array)} path
 * @param  {Array}                 [keys]
 * @param  {Object}                [options]
 * @return {RegExp}
 */
function pathToRegexp (path, keys, options) {
  keys = keys || [];

  if (!isarray(keys)) {
    options = keys;
    keys = [];
  } else if (!options) {
    options = {};
  }

  if (path instanceof RegExp) {
    return regexpToRegexp(path, keys, options)
  }

  if (isarray(path)) {
    return arrayToRegexp(path, keys, options)
  }

  return stringToRegexp(path, keys, options)
}

pathToRegexp_1.parse = parse_1;
pathToRegexp_1.compile = compile_1;
pathToRegexp_1.tokensToFunction = tokensToFunction_1;
pathToRegexp_1.tokensToRegExp = tokensToRegExp_1;

/**
   * Module dependencies.
   */

  

  /**
   * Module exports.
   */

  var page_js = page;
  page.default = page;
  page.Context = Context;
  page.Route = Route;
  page.sameOrigin = sameOrigin;

  /**
   * Short-cuts for global-object checks
   */

  var hasDocument = ('undefined' !== typeof document);
  var hasWindow = ('undefined' !== typeof window);
  var hasHistory = ('undefined' !== typeof history);
  var hasProcess = typeof process !== 'undefined';

  /**
   * Detect click event
   */
  var clickEvent = hasDocument && document.ontouchstart ? 'touchstart' : 'click';

  /**
   * To work properly with the URL
   * history.location generated polyfill in https://github.com/devote/HTML5-History-API
   */

  var isLocation = hasWindow && !!(window.history.location || window.location);

  /**
   * Perform initial dispatch.
   */

  var dispatch = true;


  /**
   * Decode URL components (query string, pathname, hash).
   * Accommodates both regular percent encoding and x-www-form-urlencoded format.
   */
  var decodeURLComponents = true;

  /**
   * Base path.
   */

  var base = '';

  /**
   * Strict path matching.
   */

  var strict = false;

  /**
   * Running flag.
   */

  var running;

  /**
   * HashBang option
   */

  var hashbang = false;

  /**
   * Previous context, for capturing
   * page exit events.
   */

  var prevContext;

  /**
   * The window for which this `page` is running
   */
  var pageWindow;

  /**
   * Register `path` with callback `fn()`,
   * or route `path`, or redirection,
   * or `page.start()`.
   *
   *   page(fn);
   *   page('*', fn);
   *   page('/user/:id', load, user);
   *   page('/user/' + user.id, { some: 'thing' });
   *   page('/user/' + user.id);
   *   page('/from', '/to')
   *   page();
   *
   * @param {string|!Function|!Object} path
   * @param {Function=} fn
   * @api public
   */

  function page(path, fn) {
    // <callback>
    if ('function' === typeof path) {
      return page('*', path);
    }

    // route <path> to <callback ...>
    if ('function' === typeof fn) {
      var route = new Route(/** @type {string} */ (path));
      for (var i = 1; i < arguments.length; ++i) {
        page.callbacks.push(route.middleware(arguments[i]));
      }
      // show <path> with [state]
    } else if ('string' === typeof path) {
      page['string' === typeof fn ? 'redirect' : 'show'](path, fn);
      // start [options]
    } else {
      page.start(path);
    }
  }

  /**
   * Callback functions.
   */

  page.callbacks = [];
  page.exits = [];

  /**
   * Current path being processed
   * @type {string}
   */
  page.current = '';

  /**
   * Number of pages navigated to.
   * @type {number}
   *
   *     page.len == 0;
   *     page('/login');
   *     page.len == 1;
   */

  page.len = 0;

  /**
   * Get or set basepath to `path`.
   *
   * @param {string} path
   * @api public
   */

  page.base = function(path) {
    if (0 === arguments.length) return base;
    base = path;
  };

  /**
   * Get or set strict path matching to `enable`
   *
   * @param {boolean} enable
   * @api public
   */

  page.strict = function(enable) {
    if (0 === arguments.length) return strict;
    strict = enable;
  };

  /**
   * Bind with the given `options`.
   *
   * Options:
   *
   *    - `click` bind to click events [true]
   *    - `popstate` bind to popstate [true]
   *    - `dispatch` perform initial dispatch [true]
   *
   * @param {Object} options
   * @api public
   */

  page.start = function(options) {
    options = options || {};
    if (running) return;
    running = true;
    pageWindow = options.window || (hasWindow && window);
    if (false === options.dispatch) dispatch = false;
    if (false === options.decodeURLComponents) decodeURLComponents = false;
    if (false !== options.popstate && hasWindow) pageWindow.addEventListener('popstate', onpopstate, false);
    if (false !== options.click && hasDocument) {
      pageWindow.document.addEventListener(clickEvent, onclick, false);
    }
    hashbang = !!options.hashbang;
    if(hashbang && hasWindow && !hasHistory) {
      pageWindow.addEventListener('hashchange', onpopstate, false);
    }
    if (!dispatch) return;

    var url;
    if(isLocation) {
      var loc = pageWindow.location;

      if(hashbang && ~loc.hash.indexOf('#!')) {
        url = loc.hash.substr(2) + loc.search;
      } else if (hashbang) {
        url = loc.search + loc.hash;
      } else {
        url = loc.pathname + loc.search + loc.hash;
      }
    }

    page.replace(url, null, true, dispatch);
  };

  /**
   * Unbind click and popstate event handlers.
   *
   * @api public
   */

  page.stop = function() {
    if (!running) return;
    page.current = '';
    page.len = 0;
    running = false;
    hasDocument && pageWindow.document.removeEventListener(clickEvent, onclick, false);
    hasWindow && pageWindow.removeEventListener('popstate', onpopstate, false);
    hasWindow && pageWindow.removeEventListener('hashchange', onpopstate, false);
  };

  /**
   * Show `path` with optional `state` object.
   *
   * @param {string} path
   * @param {Object=} state
   * @param {boolean=} dispatch
   * @param {boolean=} push
   * @return {!Context}
   * @api public
   */

  page.show = function(path, state, dispatch, push) {
    var ctx = new Context(path, state),
      prev = prevContext;
    prevContext = ctx;
    page.current = ctx.path;
    if (false !== dispatch) page.dispatch(ctx, prev);
    if (false !== ctx.handled && false !== push) ctx.pushState();
    return ctx;
  };

  /**
   * Goes back in the history
   * Back should always let the current route push state and then go back.
   *
   * @param {string} path - fallback path to go back if no more history exists, if undefined defaults to page.base
   * @param {Object=} state
   * @api public
   */

  page.back = function(path, state) {
    if (page.len > 0) {
      // this may need more testing to see if all browsers
      // wait for the next tick to go back in history
      hasHistory && pageWindow.history.back();
      page.len--;
    } else if (path) {
      setTimeout(function() {
        page.show(path, state);
      });
    }else{
      setTimeout(function() {
        page.show(getBase(), state);
      });
    }
  };


  /**
   * Register route to redirect from one path to other
   * or just redirect to another route
   *
   * @param {string} from - if param 'to' is undefined redirects to 'from'
   * @param {string=} to
   * @api public
   */
  page.redirect = function(from, to) {
    // Define route from a path to another
    if ('string' === typeof from && 'string' === typeof to) {
      page(from, function(e) {
        setTimeout(function() {
          page.replace(/** @type {!string} */ (to));
        }, 0);
      });
    }

    // Wait for the push state and replace it with another
    if ('string' === typeof from && 'undefined' === typeof to) {
      setTimeout(function() {
        page.replace(from);
      }, 0);
    }
  };

  /**
   * Replace `path` with optional `state` object.
   *
   * @param {string} path
   * @param {Object=} state
   * @param {boolean=} init
   * @param {boolean=} dispatch
   * @return {!Context}
   * @api public
   */


  page.replace = function(path, state, init, dispatch) {
    var ctx = new Context(path, state),
      prev = prevContext;
    prevContext = ctx;
    page.current = ctx.path;
    ctx.init = init;
    ctx.save(); // save before dispatching, which may redirect
    if (false !== dispatch) page.dispatch(ctx, prev);
    return ctx;
  };

  /**
   * Dispatch the given `ctx`.
   *
   * @param {Context} ctx
   * @api private
   */

  page.dispatch = function(ctx, prev) {
    var i = 0,
      j = 0;

    function nextExit() {
      var fn = page.exits[j++];
      if (!fn) return nextEnter();
      fn(prev, nextExit);
    }

    function nextEnter() {
      var fn = page.callbacks[i++];

      if (ctx.path !== page.current) {
        ctx.handled = false;
        return;
      }
      if (!fn) return unhandled(ctx);
      fn(ctx, nextEnter);
    }

    if (prev) {
      nextExit();
    } else {
      nextEnter();
    }
  };

  /**
   * Unhandled `ctx`. When it's not the initial
   * popstate then redirect. If you wish to handle
   * 404s on your own use `page('*', callback)`.
   *
   * @param {Context} ctx
   * @api private
   */
  function unhandled(ctx) {
    if (ctx.handled) return;
    var current;

    if (hashbang) {
      current = isLocation && getBase() + pageWindow.location.hash.replace('#!', '');
    } else {
      current = isLocation && pageWindow.location.pathname + pageWindow.location.search;
    }

    if (current === ctx.canonicalPath) return;
    page.stop();
    ctx.handled = false;
    isLocation && (pageWindow.location.href = ctx.canonicalPath);
  }

  /**
   * Register an exit route on `path` with
   * callback `fn()`, which will be called
   * on the previous context when a new
   * page is visited.
   */
  page.exit = function(path, fn) {
    if (typeof path === 'function') {
      return page.exit('*', path);
    }

    var route = new Route(path);
    for (var i = 1; i < arguments.length; ++i) {
      page.exits.push(route.middleware(arguments[i]));
    }
  };

  /**
   * Remove URL encoding from the given `str`.
   * Accommodates whitespace in both x-www-form-urlencoded
   * and regular percent-encoded form.
   *
   * @param {string} val - URL component to decode
   */
  function decodeURLEncodedURIComponent(val) {
    if (typeof val !== 'string') { return val; }
    return decodeURLComponents ? decodeURIComponent(val.replace(/\+/g, ' ')) : val;
  }

  /**
   * Initialize a new "request" `Context`
   * with the given `path` and optional initial `state`.
   *
   * @constructor
   * @param {string} path
   * @param {Object=} state
   * @api public
   */

  function Context(path, state) {
    var pageBase = getBase();
    if ('/' === path[0] && 0 !== path.indexOf(pageBase)) path = pageBase + (hashbang ? '#!' : '') + path;
    var i = path.indexOf('?');

    this.canonicalPath = path;
    this.path = path.replace(pageBase, '') || '/';
    if (hashbang) this.path = this.path.replace('#!', '') || '/';

    this.title = (hasDocument && pageWindow.document.title);
    this.state = state || {};
    this.state.path = path;
    this.querystring = ~i ? decodeURLEncodedURIComponent(path.slice(i + 1)) : '';
    this.pathname = decodeURLEncodedURIComponent(~i ? path.slice(0, i) : path);
    this.params = {};

    // fragment
    this.hash = '';
    if (!hashbang) {
      if (!~this.path.indexOf('#')) return;
      var parts = this.path.split('#');
      this.path = this.pathname = parts[0];
      this.hash = decodeURLEncodedURIComponent(parts[1]) || '';
      this.querystring = this.querystring.split('#')[0];
    }
  }

  /**
   * Expose `Context`.
   */

  page.Context = Context;

  /**
   * Push state.
   *
   * @api private
   */

  Context.prototype.pushState = function() {
    page.len++;
    if (hasHistory) {
        pageWindow.history.pushState(this.state, this.title,
          hashbang && this.path !== '/' ? '#!' + this.path : this.canonicalPath);
    }
  };

  /**
   * Save the context state.
   *
   * @api public
   */

  Context.prototype.save = function() {
    if (hasHistory && pageWindow.location.protocol !== 'file:') {
        pageWindow.history.replaceState(this.state, this.title,
          hashbang && this.path !== '/' ? '#!' + this.path : this.canonicalPath);
    }
  };

  /**
   * Initialize `Route` with the given HTTP `path`,
   * and an array of `callbacks` and `options`.
   *
   * Options:
   *
   *   - `sensitive`    enable case-sensitive routes
   *   - `strict`       enable strict matching for trailing slashes
   *
   * @constructor
   * @param {string} path
   * @param {Object=} options
   * @api private
   */

  function Route(path, options) {
    options = options || {};
    options.strict = options.strict || strict;
    this.path = (path === '*') ? '(.*)' : path;
    this.method = 'GET';
    this.regexp = pathToRegexp_1(this.path,
      this.keys = [],
      options);
  }

  /**
   * Expose `Route`.
   */

  page.Route = Route;

  /**
   * Return route middleware with
   * the given callback `fn()`.
   *
   * @param {Function} fn
   * @return {Function}
   * @api public
   */

  Route.prototype.middleware = function(fn) {
    var self = this;
    return function(ctx, next) {
      if (self.match(ctx.path, ctx.params)) return fn(ctx, next);
      next();
    };
  };

  /**
   * Check if this route matches `path`, if so
   * populate `params`.
   *
   * @param {string} path
   * @param {Object} params
   * @return {boolean}
   * @api private
   */

  Route.prototype.match = function(path, params) {
    var keys = this.keys,
      qsIndex = path.indexOf('?'),
      pathname = ~qsIndex ? path.slice(0, qsIndex) : path,
      m = this.regexp.exec(decodeURIComponent(pathname));

    if (!m) return false;

    for (var i = 1, len = m.length; i < len; ++i) {
      var key = keys[i - 1];
      var val = decodeURLEncodedURIComponent(m[i]);
      if (val !== undefined || !(hasOwnProperty.call(params, key.name))) {
        params[key.name] = val;
      }
    }

    return true;
  };


  /**
   * Handle "populate" events.
   */

  var onpopstate = (function () {
    var loaded = false;
    if ( ! hasWindow ) {
      return;
    }
    if (hasDocument && document.readyState === 'complete') {
      loaded = true;
    } else {
      window.addEventListener('load', function() {
        setTimeout(function() {
          loaded = true;
        }, 0);
      });
    }
    return function onpopstate(e) {
      if (!loaded) return;
      if (e.state) {
        var path = e.state.path;
        page.replace(path, e.state);
      } else if (isLocation) {
        var loc = pageWindow.location;
        page.show(loc.pathname + loc.hash, undefined, undefined, false);
      }
    };
  })();
  /**
   * Handle "click" events.
   */

  /* jshint +W054 */
  function onclick(e) {
    if (1 !== which(e)) return;

    if (e.metaKey || e.ctrlKey || e.shiftKey) return;
    if (e.defaultPrevented) return;

    // ensure link
    // use shadow dom when available if not, fall back to composedPath() for browsers that only have shady
    var el = e.target;
    var eventPath = e.path || (e.composedPath ? e.composedPath() : null);

    if(eventPath) {
      for (var i = 0; i < eventPath.length; i++) {
        if (!eventPath[i].nodeName) continue;
        if (eventPath[i].nodeName.toUpperCase() !== 'A') continue;
        if (!eventPath[i].href) continue;

        el = eventPath[i];
        break;
      }
    }
    // continue ensure link
    // el.nodeName for svg links are 'a' instead of 'A'
    while (el && 'A' !== el.nodeName.toUpperCase()) el = el.parentNode;
    if (!el || 'A' !== el.nodeName.toUpperCase()) return;

    // check if link is inside an svg
    // in this case, both href and target are always inside an object
    var svg = (typeof el.href === 'object') && el.href.constructor.name === 'SVGAnimatedString';

    // Ignore if tag has
    // 1. "download" attribute
    // 2. rel="external" attribute
    if (el.hasAttribute('download') || el.getAttribute('rel') === 'external') return;

    // ensure non-hash for the same path
    var link = el.getAttribute('href');
    if(!hashbang && samePath(el) && (el.hash || '#' === link)) return;

    // Check for mailto: in the href
    if (link && link.indexOf('mailto:') > -1) return;

    // check target
    // svg target is an object and its desired value is in .baseVal property
    if (svg ? el.target.baseVal : el.target) return;

    // x-origin
    // note: svg links that are not relative don't call click events (and skip page.js)
    // consequently, all svg links tested inside page.js are relative and in the same origin
    if (!svg && !sameOrigin(el.href)) return;

    // rebuild path
    // There aren't .pathname and .search properties in svg links, so we use href
    // Also, svg href is an object and its desired value is in .baseVal property
    var path = svg ? el.href.baseVal : (el.pathname + el.search + (el.hash || ''));

    path = path[0] !== '/' ? '/' + path : path;

    // strip leading "/[drive letter]:" on NW.js on Windows
    if (hasProcess && path.match(/^\/[a-zA-Z]:\//)) {
      path = path.replace(/^\/[a-zA-Z]:\//, '/');
    }

    // same page
    var orig = path;
    var pageBase = getBase();

    if (path.indexOf(pageBase) === 0) {
      path = path.substr(base.length);
    }

    if (hashbang) path = path.replace('#!', '');

    if (pageBase && orig === path) return;

    e.preventDefault();
    page.show(orig);
  }

  /**
   * Event button.
   */

  function which(e) {
    e = e || (hasWindow && window.event);
    return null == e.which ? e.button : e.which;
  }

  /**
   * Convert to a URL object
   */
  function toURL(href) {
    if(typeof URL === 'function' && isLocation) {
      return new URL(href, location.toString());
    } else if (hasDocument) {
      var anc = document.createElement('a');
      anc.href = href;
      return anc;
    }
  }

  /**
   * Check if `href` is the same origin.
   */

  function sameOrigin(href) {
    if(!href || !isLocation) return false;
    var url = toURL(href);

    var loc = pageWindow.location;
    return loc.protocol === url.protocol &&
      loc.hostname === url.hostname &&
      loc.port === url.port;
  }

  function samePath(url) {
    if(!isLocation) return false;
    var loc = pageWindow.location;
    return url.pathname === loc.pathname &&
      url.search === loc.search;
  }

  /**
   * Gets the `base`, which depends on whether we are using History or
   * hashbang routing.
   */
  function getBase() {
    if(!!base) return base;
    var loc = hasWindow && pageWindow && pageWindow.location;
    return (hasWindow && hashbang && loc && loc.protocol === 'file:') ? loc.pathname : base;
  }

  page.sameOrigin = sameOrigin;

return page_js;

})));

}).call(this,require('_process'))

},{"_process":2}],2:[function(require,module,exports){
// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };

},{}],3:[function(require,module,exports){
(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('riot')) :
	typeof define === 'function' && define.amd ? define(['exports', 'riot'], factory) :
	(factory((global.riotHotReload = global.riotHotReload || {}),global.riot));
}(this, (function (exports,riot) { 'use strict';

var nonState = 'isMounted opts'.split(' ');

function reload(name) {
  riot.util.styleManager.inject();

  var elems = document.querySelectorAll((name + ", [data-is=" + name + "]"));
  var tags = [];

  for (var i = 0; i < elems.length; i++) {
    var el = elems[i], oldTag = el._tag, v;
    reload.trigger('before-unmount', oldTag);
    oldTag.unmount(true); // detach the old tag

    // reset the innerHTML and attributes to how they were before mount
    el.innerHTML = oldTag.__.innerHTML;
    (oldTag.__.instAttrs || []).map(function(attr) {
      el.setAttribute(attr.name, attr.value);
    });

    // copy options for creating the new tag
    var newOpts = {};
    for(key in oldTag.opts) {
      newOpts[key] = oldTag.opts[key];
    }
    newOpts.parent = oldTag.parent;

    // create the new tag
    reload.trigger('before-mount', newOpts, oldTag);
    var newTag = riot.mount(el, newOpts)[0];

    // copy state from the old to new tag
    for(var key in oldTag) {
      v = oldTag[key];
      if (~nonState.indexOf(key)) { continue }
      newTag[key] = v;
    }
    newTag.update();
    tags.push(newTag);
    reload.trigger('after-mount', newTag, oldTag);
  }

  return tags
}

riot.observable(reload);
riot.reload = reload;
if (riot.default) { riot.default.reload = reload; }

exports.reload = reload;
exports['default'] = reload;

Object.defineProperty(exports, '__esModule', { value: true });

})));

},{"riot":5}],4:[function(require,module,exports){
riot.tag2('routehandler', '<div data-is="{tagname}"></div>', '', '', function (opts) {
  var page;

  page = null;

  if (typeof exports === "object" && (typeof exports !== "undefined" && exports !== null)) {
    page = require('page');
  } else if (window.page == null) {
    return console.log('Page.js not found - please check it has been npm installed or included in your page');
  } else {
    page = window.page;
  }

  this.on('mount', (function (_this) {
    return function () {
      var basepath, ref;
      _this.tagstack = [];
      if (opts.routes) {
        _this.mountRoutes({
          handler: _this
        }, opts.routes);
        if ((ref = opts.options) != null ? ref.base : void 0) {
          basepath = opts.options.base;
          page.base(basepath);
          delete opts.options.base;
        }
        return page(opts.options);
      }
    };
  })(this));

  this.mountRoutes = (function (_this) {
    return function (parent, routes) {
      var route;
      return route = _this.findRoute(null, routes, function (tree, req) {
        var i, idx, len, nexttag, ref, ref1, ref2, ref3, removeTag, results, routeopts, tag;
        delete opts.routes;
        routeopts = opts;
        routeopts.page = page;
        routeopts.params = req.params;
        routeopts.state = req.state;
        tag = _this;
        for (idx = i = 0, len = tree.length; i < len; idx = ++i) {
          route = tree[idx];
          if (_this.tagstack[idx] && _this.tagstack[idx].tagname === route.tag) {
            nexttag = _this.tagstack[idx].nexttag;
            riot.update();
          } else {
            if (tag && (route != null ? route.tag : void 0)) {
              nexttag = tag.setTag(route.tag, routeopts);
            }
          }
          _this.tagstack[idx] = {
            tagname: route.tag,
            nexttag: nexttag,
            tag: tag
          };
          tag = (nexttag != null ? (ref = nexttag[0]) != null ? ref.tags.routehandler : void 0 : void 0) || (nexttag != null ? (ref1 = nexttag[0]) != null ? (ref2 = ref1.root.querySelector('routehandler')) != null ? ref2._tag : void 0 : void 0 : void 0);
        }
        results = [];
        while (idx < _this.tagstack.length) {
          removeTag = _this.tagstack.pop();
          results.push((ref3 = removeTag.nexttag[0]) != null ? ref3.unmount(true) : void 0);
        }
        return results;
      });
    };
  })(this);

  this.setTag = (function (_this) {
    return function (tagname, routeopts) {
      var tag;
      _this.root.childNodes[0].setAttribute("data-is", tagname);
      _this.tags[tagname];
      tag = riot.mount(tagname, routeopts);
      tag[0].opts = routeopts;
      return tag;
    };
  })(this);

  this.findRoute = (function (_this) {
    return function (parents, routes, cback) {
      var i, len, parentpath, results, route, subparents;
      parentpath = parents ? parents.map(function (ob) {
        return ob.route;
      }).join("").replace(/\/\//g, '/') : "";
      results = [];
      for (i = 0, len = routes.length; i < len; i++) {
        route = routes[i];
        if ((route.use != null) && typeof route.use === "function") {
          (function (route) {
            var mainroute;
            mainroute = (parentpath + route.route).replace(/\/\//g, '/');
            return page(mainroute, function (ctx, next) {
              if (mainroute !== "*") {
                cback([route], ctx);
              }
              return route.use(ctx, next, page);
            });
          })(route);
        }
        if (route.tag != null) {
          subparents = parents ? parents.slice() : [];
          subparents.push(route);
          (function (subparents) {
            var mainroute, thisroute;
            thisroute = route;
            mainroute = (parentpath + route.route).replace(/\/\//g, '/');
            return page(mainroute, function (req, next) {
              var ref;
              cback(subparents, req);
              if ((ref = thisroute.routes) != null ? ref.filter(function (route) {
                  return route.route === "/";
                }).length : void 0) {
                return next();
              }
            });
          })(subparents);
        }
        if (route.routes) {
          results.push(_this.findRoute(subparents, route.routes, cback));
        } else {
          results.push(void 0);
        }
      }
      return results;
    };
  })(this);
});
},{"page":1}],5:[function(require,module,exports){
/* Riot v3.11.1, @license MIT */
(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (factory((global.riot = {})));
}(this, (function (exports) { 'use strict';

  /**
   * Shorter and fast way to select a single node in the DOM
   * @param   { String } selector - unique dom selector
   * @param   { Object } ctx - DOM node where the target of our search will is located
   * @returns { Object } dom node found
   */
  function $(selector, ctx) {
    return (ctx || document).querySelector(selector)
  }

  var
    // be aware, internal usage
    // ATTENTION: prefix the global dynamic variables with `__`
    // tags instances cache
    __TAGS_CACHE = [],
    // tags implementation cache
    __TAG_IMPL = {},
    YIELD_TAG = 'yield',

    /**
     * Const
     */
    GLOBAL_MIXIN = '__global_mixin',

    // riot specific prefixes or attributes
    ATTRS_PREFIX = 'riot-',

    // Riot Directives
    REF_DIRECTIVES = ['ref', 'data-ref'],
    IS_DIRECTIVE = 'data-is',
    CONDITIONAL_DIRECTIVE = 'if',
    LOOP_DIRECTIVE = 'each',
    LOOP_NO_REORDER_DIRECTIVE = 'no-reorder',
    SHOW_DIRECTIVE = 'show',
    HIDE_DIRECTIVE = 'hide',
    KEY_DIRECTIVE = 'key',
    RIOT_EVENTS_KEY = '__riot-events__',

    // for typeof == '' comparisons
    T_STRING = 'string',
    T_OBJECT = 'object',
    T_UNDEF  = 'undefined',
    T_FUNCTION = 'function',

    XLINK_NS = 'http://www.w3.org/1999/xlink',
    SVG_NS = 'http://www.w3.org/2000/svg',
    XLINK_REGEX = /^xlink:(\w+)/,

    WIN = typeof window === T_UNDEF ? /* istanbul ignore next */ undefined : window,

    // special native tags that cannot be treated like the others
    RE_SPECIAL_TAGS = /^(?:t(?:body|head|foot|[rhd])|caption|col(?:group)?|opt(?:ion|group))$/,
    RE_SPECIAL_TAGS_NO_OPTION = /^(?:t(?:body|head|foot|[rhd])|caption|col(?:group)?)$/,
    RE_EVENTS_PREFIX = /^on/,
    RE_HTML_ATTRS = /([-\w]+) ?= ?(?:"([^"]*)|'([^']*)|({[^}]*}))/g,
    // some DOM attributes must be normalized
    CASE_SENSITIVE_ATTRIBUTES = {
      'viewbox': 'viewBox',
      'preserveaspectratio': 'preserveAspectRatio'
    },
    /**
     * Matches boolean HTML attributes in the riot tag definition.
     * With a long list like this, a regex is faster than `[].indexOf` in most browsers.
     * @const {RegExp}
     * @see [attributes.md](https://github.com/riot/compiler/blob/dev/doc/attributes.md)
     */
    RE_BOOL_ATTRS = /^(?:disabled|checked|readonly|required|allowfullscreen|auto(?:focus|play)|compact|controls|default|formnovalidate|hidden|ismap|itemscope|loop|multiple|muted|no(?:resize|shade|validate|wrap)?|open|reversed|seamless|selected|sortable|truespeed|typemustmatch)$/,
    // version# for IE 8-11, 0 for others
    IE_VERSION = (WIN && WIN.document || /* istanbul ignore next */ {}).documentMode | 0;

  /**
   * Create a generic DOM node
   * @param   { String } name - name of the DOM node we want to create
   * @returns { Object } DOM node just created
   */
  function makeElement(name) {
    return name === 'svg' ? document.createElementNS(SVG_NS, name) : document.createElement(name)
  }

  /**
   * Set any DOM attribute
   * @param { Object } dom - DOM node we want to update
   * @param { String } name - name of the property we want to set
   * @param { String } val - value of the property we want to set
   */
  function setAttribute(dom, name, val) {
    var xlink = XLINK_REGEX.exec(name);
    if (xlink && xlink[1])
      { dom.setAttributeNS(XLINK_NS, xlink[1], val); }
    else
      { dom.setAttribute(name, val); }
  }

  var styleNode;
  // Create cache and shortcut to the correct property
  var cssTextProp;
  var byName = {};
  var needsInject = false;

  // skip the following code on the server
  if (WIN) {
    styleNode = ((function () {
      // create a new style element with the correct type
      var newNode = makeElement('style');
      // replace any user node or insert the new one into the head
      var userNode = $('style[type=riot]');

      setAttribute(newNode, 'type', 'text/css');
      /* istanbul ignore next */
      if (userNode) {
        if (userNode.id) { newNode.id = userNode.id; }
        userNode.parentNode.replaceChild(newNode, userNode);
      } else { document.head.appendChild(newNode); }

      return newNode
    }))();
    cssTextProp = styleNode.styleSheet;
  }

  /**
   * Object that will be used to inject and manage the css of every tag instance
   */
  var styleManager = {
    styleNode: styleNode,
    /**
     * Save a tag style to be later injected into DOM
     * @param { String } css - css string
     * @param { String } name - if it's passed we will map the css to a tagname
     */
    add: function add(css, name) {
      byName[name] = css;
      needsInject = true;
    },
    /**
     * Inject all previously saved tag styles into DOM
     * innerHTML seems slow: http://jsperf.com/riot-insert-style
     */
    inject: function inject() {
      if (!WIN || !needsInject) { return }
      needsInject = false;
      var style = Object.keys(byName)
        .map(function (k) { return byName[k]; })
        .join('\n');
      /* istanbul ignore next */
      if (cssTextProp) { cssTextProp.cssText = style; }
      else { styleNode.innerHTML = style; }
    },

    /**
     * Remove a tag style of injected DOM later.
     * @param {String} name a registered tagname
     */
    remove: function remove(name) {
      delete byName[name];
      needsInject = true;
    }
  }

  /**
   * The riot template engine
   * @version v3.0.8
   */

  /* istanbul ignore next */
  var skipRegex = (function () { //eslint-disable-line no-unused-vars

    var beforeReChars = '[{(,;:?=|&!^~>%*/';

    var beforeReWords = [
      'case',
      'default',
      'do',
      'else',
      'in',
      'instanceof',
      'prefix',
      'return',
      'typeof',
      'void',
      'yield'
    ];

    var wordsLastChar = beforeReWords.reduce(function (s, w) {
      return s + w.slice(-1)
    }, '');

    var RE_REGEX = /^\/(?=[^*>/])[^[/\\]*(?:(?:\\.|\[(?:\\.|[^\]\\]*)*\])[^[\\/]*)*?\/[gimuy]*/;
    var RE_VN_CHAR = /[$\w]/;

    function prev (code, pos) {
      while (--pos >= 0 && /\s/.test(code[pos])){ }
      return pos
    }

    function _skipRegex (code, start) {

      var re = /.*/g;
      var pos = re.lastIndex = start++;
      var match = re.exec(code)[0].match(RE_REGEX);

      if (match) {
        var next = pos + match[0].length;

        pos = prev(code, pos);
        var c = code[pos];

        if (pos < 0 || ~beforeReChars.indexOf(c)) {
          return next
        }

        if (c === '.') {

          if (code[pos - 1] === '.') {
            start = next;
          }

        } else if (c === '+' || c === '-') {

          if (code[--pos] !== c ||
              (pos = prev(code, pos)) < 0 ||
              !RE_VN_CHAR.test(code[pos])) {
            start = next;
          }

        } else if (~wordsLastChar.indexOf(c)) {

          var end = pos + 1;

          while (--pos >= 0 && RE_VN_CHAR.test(code[pos])){ }
          if (~beforeReWords.indexOf(code.slice(pos + 1, end))) {
            start = next;
          }
        }
      }

      return start
    }

    return _skipRegex

  })();

  /**
   * riot.util.brackets
   *
   * - `brackets    ` - Returns a string or regex based on its parameter
   * - `brackets.set` - Change the current riot brackets
   *
   * @module
   */

  /* global riot */

  /* istanbul ignore next */
  var brackets = (function (UNDEF) {

    var
      REGLOB = 'g',

      R_MLCOMMS = /\/\*[^*]*\*+(?:[^*\/][^*]*\*+)*\//g,

      R_STRINGS = /"[^"\\]*(?:\\[\S\s][^"\\]*)*"|'[^'\\]*(?:\\[\S\s][^'\\]*)*'|`[^`\\]*(?:\\[\S\s][^`\\]*)*`/g,

      S_QBLOCKS = R_STRINGS.source + '|' +
        /(?:\breturn\s+|(?:[$\w\)\]]|\+\+|--)\s*(\/)(?![*\/]))/.source + '|' +
        /\/(?=[^*\/])[^[\/\\]*(?:(?:\[(?:\\.|[^\]\\]*)*\]|\\.)[^[\/\\]*)*?([^<]\/)[gim]*/.source,

      UNSUPPORTED = RegExp('[\\' + 'x00-\\x1F<>a-zA-Z0-9\'",;\\\\]'),

      NEED_ESCAPE = /(?=[[\]()*+?.^$|])/g,

      S_QBLOCK2 = R_STRINGS.source + '|' + /(\/)(?![*\/])/.source,

      FINDBRACES = {
        '(': RegExp('([()])|'   + S_QBLOCK2, REGLOB),
        '[': RegExp('([[\\]])|' + S_QBLOCK2, REGLOB),
        '{': RegExp('([{}])|'   + S_QBLOCK2, REGLOB)
      },

      DEFAULT = '{ }';

    var _pairs = [
      '{', '}',
      '{', '}',
      /{[^}]*}/,
      /\\([{}])/g,
      /\\({)|{/g,
      RegExp('\\\\(})|([[({])|(})|' + S_QBLOCK2, REGLOB),
      DEFAULT,
      /^\s*{\^?\s*([$\w]+)(?:\s*,\s*(\S+))?\s+in\s+(\S.*)\s*}/,
      /(^|[^\\]){=[\S\s]*?}/
    ];

    var
      cachedBrackets = UNDEF,
      _regex,
      _cache = [],
      _settings;

    function _loopback (re) { return re }

    function _rewrite (re, bp) {
      if (!bp) { bp = _cache; }
      return new RegExp(
        re.source.replace(/{/g, bp[2]).replace(/}/g, bp[3]), re.global ? REGLOB : ''
      )
    }

    function _create (pair) {
      if (pair === DEFAULT) { return _pairs }

      var arr = pair.split(' ');

      if (arr.length !== 2 || UNSUPPORTED.test(pair)) {
        throw new Error('Unsupported brackets "' + pair + '"')
      }
      arr = arr.concat(pair.replace(NEED_ESCAPE, '\\').split(' '));

      arr[4] = _rewrite(arr[1].length > 1 ? /{[\S\s]*?}/ : _pairs[4], arr);
      arr[5] = _rewrite(pair.length > 3 ? /\\({|})/g : _pairs[5], arr);
      arr[6] = _rewrite(_pairs[6], arr);
      arr[7] = RegExp('\\\\(' + arr[3] + ')|([[({])|(' + arr[3] + ')|' + S_QBLOCK2, REGLOB);
      arr[8] = pair;
      return arr
    }

    function _brackets (reOrIdx) {
      return reOrIdx instanceof RegExp ? _regex(reOrIdx) : _cache[reOrIdx]
    }

    _brackets.split = function split (str, tmpl, _bp) {
      // istanbul ignore next: _bp is for the compiler
      if (!_bp) { _bp = _cache; }

      var
        parts = [],
        match,
        isexpr,
        start,
        pos,
        re = _bp[6];

      var qblocks = [];
      var prevStr = '';
      var mark, lastIndex;

      isexpr = start = re.lastIndex = 0;

      while ((match = re.exec(str))) {

        lastIndex = re.lastIndex;
        pos = match.index;

        if (isexpr) {

          if (match[2]) {

            var ch = match[2];
            var rech = FINDBRACES[ch];
            var ix = 1;

            rech.lastIndex = lastIndex;
            while ((match = rech.exec(str))) {
              if (match[1]) {
                if (match[1] === ch) { ++ix; }
                else if (!--ix) { break }
              } else {
                rech.lastIndex = pushQBlock(match.index, rech.lastIndex, match[2]);
              }
            }
            re.lastIndex = ix ? str.length : rech.lastIndex;
            continue
          }

          if (!match[3]) {
            re.lastIndex = pushQBlock(pos, lastIndex, match[4]);
            continue
          }
        }

        if (!match[1]) {
          unescapeStr(str.slice(start, pos));
          start = re.lastIndex;
          re = _bp[6 + (isexpr ^= 1)];
          re.lastIndex = start;
        }
      }

      if (str && start < str.length) {
        unescapeStr(str.slice(start));
      }

      parts.qblocks = qblocks;

      return parts

      function unescapeStr (s) {
        if (prevStr) {
          s = prevStr + s;
          prevStr = '';
        }
        if (tmpl || isexpr) {
          parts.push(s && s.replace(_bp[5], '$1'));
        } else {
          parts.push(s);
        }
      }

      function pushQBlock(_pos, _lastIndex, slash) { //eslint-disable-line
        if (slash) {
          _lastIndex = skipRegex(str, _pos);
        }

        if (tmpl && _lastIndex > _pos + 2) {
          mark = '\u2057' + qblocks.length + '~';
          qblocks.push(str.slice(_pos, _lastIndex));
          prevStr += str.slice(start, _pos) + mark;
          start = _lastIndex;
        }
        return _lastIndex
      }
    };

    _brackets.hasExpr = function hasExpr (str) {
      return _cache[4].test(str)
    };

    _brackets.loopKeys = function loopKeys (expr) {
      var m = expr.match(_cache[9]);

      return m
        ? { key: m[1], pos: m[2], val: _cache[0] + m[3].trim() + _cache[1] }
        : { val: expr.trim() }
    };

    _brackets.array = function array (pair) {
      return pair ? _create(pair) : _cache
    };

    function _reset (pair) {
      if ((pair || (pair = DEFAULT)) !== _cache[8]) {
        _cache = _create(pair);
        _regex = pair === DEFAULT ? _loopback : _rewrite;
        _cache[9] = _regex(_pairs[9]);
      }
      cachedBrackets = pair;
    }

    function _setSettings (o) {
      var b;

      o = o || {};
      b = o.brackets;
      Object.defineProperty(o, 'brackets', {
        set: _reset,
        get: function () { return cachedBrackets },
        enumerable: true
      });
      _settings = o;
      _reset(b);
    }

    Object.defineProperty(_brackets, 'settings', {
      set: _setSettings,
      get: function () { return _settings }
    });

    /* istanbul ignore next: in the browser riot is always in the scope */
    _brackets.settings = typeof riot !== 'undefined' && riot.settings || {};
    _brackets.set = _reset;
    _brackets.skipRegex = skipRegex;

    _brackets.R_STRINGS = R_STRINGS;
    _brackets.R_MLCOMMS = R_MLCOMMS;
    _brackets.S_QBLOCKS = S_QBLOCKS;
    _brackets.S_QBLOCK2 = S_QBLOCK2;

    return _brackets

  })();

  /**
   * @module tmpl
   *
   * tmpl          - Root function, returns the template value, render with data
   * tmpl.hasExpr  - Test the existence of a expression inside a string
   * tmpl.loopKeys - Get the keys for an 'each' loop (used by `_each`)
   */

  /* istanbul ignore next */
  var tmpl = (function () {

    var _cache = {};

    function _tmpl (str, data) {
      if (!str) { return str }

      return (_cache[str] || (_cache[str] = _create(str))).call(
        data, _logErr.bind({
          data: data,
          tmpl: str
        })
      )
    }

    _tmpl.hasExpr = brackets.hasExpr;

    _tmpl.loopKeys = brackets.loopKeys;

    // istanbul ignore next
    _tmpl.clearCache = function () { _cache = {}; };

    _tmpl.errorHandler = null;

    function _logErr (err, ctx) {

      err.riotData = {
        tagName: ctx && ctx.__ && ctx.__.tagName,
        _riot_id: ctx && ctx._riot_id  //eslint-disable-line camelcase
      };

      if (_tmpl.errorHandler) { _tmpl.errorHandler(err); }
      else if (
        typeof console !== 'undefined' &&
        typeof console.error === 'function'
      ) {
        console.error(err.message);
        console.log('<%s> %s', err.riotData.tagName || 'Unknown tag', this.tmpl); // eslint-disable-line
        console.log(this.data); // eslint-disable-line
      }
    }

    function _create (str) {
      var expr = _getTmpl(str);

      if (expr.slice(0, 11) !== 'try{return ') { expr = 'return ' + expr; }

      return new Function('E', expr + ';')    // eslint-disable-line no-new-func
    }

    var RE_DQUOTE = /\u2057/g;
    var RE_QBMARK = /\u2057(\d+)~/g;

    function _getTmpl (str) {
      var parts = brackets.split(str.replace(RE_DQUOTE, '"'), 1);
      var qstr = parts.qblocks;
      var expr;

      if (parts.length > 2 || parts[0]) {
        var i, j, list = [];

        for (i = j = 0; i < parts.length; ++i) {

          expr = parts[i];

          if (expr && (expr = i & 1

              ? _parseExpr(expr, 1, qstr)

              : '"' + expr
                  .replace(/\\/g, '\\\\')
                  .replace(/\r\n?|\n/g, '\\n')
                  .replace(/"/g, '\\"') +
                '"'

            )) { list[j++] = expr; }

        }

        expr = j < 2 ? list[0]
             : '[' + list.join(',') + '].join("")';

      } else {

        expr = _parseExpr(parts[1], 0, qstr);
      }

      if (qstr.length) {
        expr = expr.replace(RE_QBMARK, function (_, pos) {
          return qstr[pos]
            .replace(/\r/g, '\\r')
            .replace(/\n/g, '\\n')
        });
      }
      return expr
    }

    var RE_CSNAME = /^(?:(-?[_A-Za-z\xA0-\xFF][-\w\xA0-\xFF]*)|\u2057(\d+)~):/;
    var
      RE_BREND = {
        '(': /[()]/g,
        '[': /[[\]]/g,
        '{': /[{}]/g
      };

    function _parseExpr (expr, asText, qstr) {

      expr = expr
        .replace(/\s+/g, ' ').trim()
        .replace(/\ ?([[\({},?\.:])\ ?/g, '$1');

      if (expr) {
        var
          list = [],
          cnt = 0,
          match;

        while (expr &&
              (match = expr.match(RE_CSNAME)) &&
              !match.index
          ) {
          var
            key,
            jsb,
            re = /,|([[{(])|$/g;

          expr = RegExp.rightContext;
          key  = match[2] ? qstr[match[2]].slice(1, -1).trim().replace(/\s+/g, ' ') : match[1];

          while (jsb = (match = re.exec(expr))[1]) { skipBraces(jsb, re); }

          jsb  = expr.slice(0, match.index);
          expr = RegExp.rightContext;

          list[cnt++] = _wrapExpr(jsb, 1, key);
        }

        expr = !cnt ? _wrapExpr(expr, asText)
             : cnt > 1 ? '[' + list.join(',') + '].join(" ").trim()' : list[0];
      }
      return expr

      function skipBraces (ch, re) {
        var
          mm,
          lv = 1,
          ir = RE_BREND[ch];

        ir.lastIndex = re.lastIndex;
        while (mm = ir.exec(expr)) {
          if (mm[0] === ch) { ++lv; }
          else if (!--lv) { break }
        }
        re.lastIndex = lv ? expr.length : ir.lastIndex;
      }
    }

    // istanbul ignore next: not both
    var // eslint-disable-next-line max-len
      JS_CONTEXT = '"in this?this:' + (typeof window !== 'object' ? 'global' : 'window') + ').',
      JS_VARNAME = /[,{][\$\w]+(?=:)|(^ *|[^$\w\.{])(?!(?:typeof|true|false|null|undefined|in|instanceof|is(?:Finite|NaN)|void|NaN|new|Date|RegExp|Math)(?![$\w]))([$_A-Za-z][$\w]*)/g,
      JS_NOPROPS = /^(?=(\.[$\w]+))\1(?:[^.[(]|$)/;

    function _wrapExpr (expr, asText, key) {
      var tb;

      expr = expr.replace(JS_VARNAME, function (match, p, mvar, pos, s) {
        if (mvar) {
          pos = tb ? 0 : pos + match.length;

          if (mvar !== 'this' && mvar !== 'global' && mvar !== 'window') {
            match = p + '("' + mvar + JS_CONTEXT + mvar;
            if (pos) { tb = (s = s[pos]) === '.' || s === '(' || s === '['; }
          } else if (pos) {
            tb = !JS_NOPROPS.test(s.slice(pos));
          }
        }
        return match
      });

      if (tb) {
        expr = 'try{return ' + expr + '}catch(e){E(e,this)}';
      }

      if (key) {

        expr = (tb
            ? 'function(){' + expr + '}.call(this)' : '(' + expr + ')'
          ) + '?"' + key + '":""';

      } else if (asText) {

        expr = 'function(v){' + (tb
            ? expr.replace('return ', 'v=') : 'v=(' + expr + ')'
          ) + ';return v||v===0?v:""}.call(this)';
      }

      return expr
    }

    _tmpl.version = brackets.version = 'v3.0.8';

    return _tmpl

  })();

  /* istanbul ignore next */
  var observable = function(el) {

    /**
     * Extend the original object or create a new empty one
     * @type { Object }
     */

    el = el || {};

    /**
     * Private variables
     */
    var callbacks = {},
      slice = Array.prototype.slice;

    /**
     * Public Api
     */

    // extend the el object adding the observable methods
    Object.defineProperties(el, {
      /**
       * Listen to the given `event` ands
       * execute the `callback` each time an event is triggered.
       * @param  { String } event - event id
       * @param  { Function } fn - callback function
       * @returns { Object } el
       */
      on: {
        value: function(event, fn) {
          if (typeof fn == 'function')
            { (callbacks[event] = callbacks[event] || []).push(fn); }
          return el
        },
        enumerable: false,
        writable: false,
        configurable: false
      },

      /**
       * Removes the given `event` listeners
       * @param   { String } event - event id
       * @param   { Function } fn - callback function
       * @returns { Object } el
       */
      off: {
        value: function(event, fn) {
          if (event == '*' && !fn) { callbacks = {}; }
          else {
            if (fn) {
              var arr = callbacks[event];
              for (var i = 0, cb; cb = arr && arr[i]; ++i) {
                if (cb == fn) { arr.splice(i--, 1); }
              }
            } else { delete callbacks[event]; }
          }
          return el
        },
        enumerable: false,
        writable: false,
        configurable: false
      },

      /**
       * Listen to the given `event` and
       * execute the `callback` at most once
       * @param   { String } event - event id
       * @param   { Function } fn - callback function
       * @returns { Object } el
       */
      one: {
        value: function(event, fn) {
          function on() {
            el.off(event, on);
            fn.apply(el, arguments);
          }
          return el.on(event, on)
        },
        enumerable: false,
        writable: false,
        configurable: false
      },

      /**
       * Execute all callback functions that listen to
       * the given `event`
       * @param   { String } event - event id
       * @returns { Object } el
       */
      trigger: {
        value: function(event) {
          var arguments$1 = arguments;


          // getting the arguments
          var arglen = arguments.length - 1,
            args = new Array(arglen),
            fns,
            fn,
            i;

          for (i = 0; i < arglen; i++) {
            args[i] = arguments$1[i + 1]; // skip first argument
          }

          fns = slice.call(callbacks[event] || [], 0);

          for (i = 0; fn = fns[i]; ++i) {
            fn.apply(el, args);
          }

          if (callbacks['*'] && event != '*')
            { el.trigger.apply(el, ['*', event].concat(args)); }

          return el
        },
        enumerable: false,
        writable: false,
        configurable: false
      }
    });

    return el

  };

  /**
   * Short alias for Object.getOwnPropertyDescriptor
   */
  function getPropDescriptor (o, k) {
    return Object.getOwnPropertyDescriptor(o, k)
  }

  /**
   * Check if passed argument is undefined
   * @param   { * } value -
   * @returns { Boolean } -
   */
  function isUndefined(value) {
    return typeof value === T_UNDEF
  }

  /**
   * Check whether object's property could be overridden
   * @param   { Object }  obj - source object
   * @param   { String }  key - object property
   * @returns { Boolean } true if writable
   */
  function isWritable(obj, key) {
    var descriptor = getPropDescriptor(obj, key);
    return isUndefined(obj[key]) || descriptor && descriptor.writable
  }

  /**
   * Extend any object with other properties
   * @param   { Object } src - source object
   * @returns { Object } the resulting extended object
   *
   * var obj = { foo: 'baz' }
   * extend(obj, {bar: 'bar', foo: 'bar'})
   * console.log(obj) => {bar: 'bar', foo: 'bar'}
   *
   */
  function extend(src) {
    var obj;
    var i = 1;
    var args = arguments;
    var l = args.length;

    for (; i < l; i++) {
      if (obj = args[i]) {
        for (var key in obj) {
          // check if this property of the source object could be overridden
          if (isWritable(src, key))
            { src[key] = obj[key]; }
        }
      }
    }
    return src
  }

  /**
   * Alias for Object.create
   */
  function create(src) {
    return Object.create(src)
  }

  var settings = extend(create(brackets.settings), {
    skipAnonymousTags: true,
    // handle the auto updates on any DOM event
    autoUpdate: true
  })

  /**
   * Shorter and fast way to select multiple nodes in the DOM
   * @param   { String } selector - DOM selector
   * @param   { Object } ctx - DOM node where the targets of our search will is located
   * @returns { Object } dom nodes found
   */
  function $$(selector, ctx) {
    return [].slice.call((ctx || document).querySelectorAll(selector))
  }

  /**
   * Create a document text node
   * @returns { Object } create a text node to use as placeholder
   */
  function createDOMPlaceholder() {
    return document.createTextNode('')
  }

  /**
   * Toggle the visibility of any DOM node
   * @param   { Object }  dom - DOM node we want to hide
   * @param   { Boolean } show - do we want to show it?
   */

  function toggleVisibility(dom, show) {
    dom.style.display = show ? '' : 'none';
    dom.hidden = show ? false : true;
  }

  /**
   * Get the value of any DOM attribute on a node
   * @param   { Object } dom - DOM node we want to parse
   * @param   { String } name - name of the attribute we want to get
   * @returns { String | undefined } name of the node attribute whether it exists
   */
  function getAttribute(dom, name) {
    return dom.getAttribute(name)
  }

  /**
   * Remove any DOM attribute from a node
   * @param   { Object } dom - DOM node we want to update
   * @param   { String } name - name of the property we want to remove
   */
  function removeAttribute(dom, name) {
    dom.removeAttribute(name);
  }

  /**
   * Set the inner html of any DOM node SVGs included
   * @param { Object } container - DOM node where we'll inject new html
   * @param { String } html - html to inject
   * @param { Boolean } isSvg - svg tags should be treated a bit differently
   */
  /* istanbul ignore next */
  function setInnerHTML(container, html, isSvg) {
    // innerHTML is not supported on svg tags so we neet to treat them differently
    if (isSvg) {
      var node = container.ownerDocument.importNode(
        new DOMParser()
          .parseFromString(("<svg xmlns=\"" + SVG_NS + "\">" + html + "</svg>"), 'application/xml')
          .documentElement,
        true
      );

      container.appendChild(node);
    } else {
      container.innerHTML = html;
    }
  }

  /**
   * Minimize risk: only zero or one _space_ between attr & value
   * @param   { String }   html - html string we want to parse
   * @param   { Function } fn - callback function to apply on any attribute found
   */
  function walkAttributes(html, fn) {
    if (!html) { return }
    var m;
    while (m = RE_HTML_ATTRS.exec(html))
      { fn(m[1].toLowerCase(), m[2] || m[3] || m[4]); }
  }

  /**
   * Create a document fragment
   * @returns { Object } document fragment
   */
  function createFragment() {
    return document.createDocumentFragment()
  }

  /**
   * Insert safely a tag to fix #1962 #1649
   * @param   { HTMLElement } root - children container
   * @param   { HTMLElement } curr - node to insert
   * @param   { HTMLElement } next - node that should preceed the current node inserted
   */
  function safeInsert(root, curr, next) {
    root.insertBefore(curr, next.parentNode && next);
  }

  /**
   * Convert a style object to a string
   * @param   { Object } style - style object we need to parse
   * @returns { String } resulting css string
   * @example
   * styleObjectToString({ color: 'red', height: '10px'}) // => 'color: red; height: 10px'
   */
  function styleObjectToString(style) {
    return Object.keys(style).reduce(function (acc, prop) {
      return (acc + " " + prop + ": " + (style[prop]) + ";")
    }, '')
  }

  /**
   * Walk down recursively all the children tags starting dom node
   * @param   { Object }   dom - starting node where we will start the recursion
   * @param   { Function } fn - callback to transform the child node just found
   * @param   { Object }   context - fn can optionally return an object, which is passed to children
   */
  function walkNodes(dom, fn, context) {
    if (dom) {
      var res = fn(dom, context);
      var next;
      // stop the recursion
      if (res === false) { return }

      dom = dom.firstChild;

      while (dom) {
        next = dom.nextSibling;
        walkNodes(dom, fn, res);
        dom = next;
      }
    }
  }



  var dom = /*#__PURE__*/Object.freeze({
    $$: $$,
    $: $,
    createDOMPlaceholder: createDOMPlaceholder,
    mkEl: makeElement,
    setAttr: setAttribute,
    toggleVisibility: toggleVisibility,
    getAttr: getAttribute,
    remAttr: removeAttribute,
    setInnerHTML: setInnerHTML,
    walkAttrs: walkAttributes,
    createFrag: createFragment,
    safeInsert: safeInsert,
    styleObjectToString: styleObjectToString,
    walkNodes: walkNodes
  });

  /**
   * Check against the null and undefined values
   * @param   { * }  value -
   * @returns {Boolean} -
   */
  function isNil(value) {
    return isUndefined(value) || value === null
  }

  /**
   * Check if passed argument is empty. Different from falsy, because we dont consider 0 or false to be blank
   * @param { * } value -
   * @returns { Boolean } -
   */
  function isBlank(value) {
    return isNil(value) || value === ''
  }

  /**
   * Check if passed argument is a function
   * @param   { * } value -
   * @returns { Boolean } -
   */
  function isFunction(value) {
    return typeof value === T_FUNCTION
  }

  /**
   * Check if passed argument is an object, exclude null
   * NOTE: use isObject(x) && !isArray(x) to excludes arrays.
   * @param   { * } value -
   * @returns { Boolean } -
   */
  function isObject(value) {
    return value && typeof value === T_OBJECT // typeof null is 'object'
  }

  /**
   * Check if a DOM node is an svg tag or part of an svg
   * @param   { HTMLElement }  el - node we want to test
   * @returns {Boolean} true if it's an svg node
   */
  function isSvg(el) {
    var owner = el.ownerSVGElement;
    return !!owner || owner === null
  }

  /**
   * Check if passed argument is a kind of array
   * @param   { * } value -
   * @returns { Boolean } -
   */
  function isArray(value) {
    return Array.isArray(value) || value instanceof Array
  }

  /**
   * Check if the passed argument is a boolean attribute
   * @param   { String } value -
   * @returns { Boolean } -
   */
  function isBoolAttr(value) {
    return RE_BOOL_ATTRS.test(value)
  }

  /**
   * Check if passed argument is a string
   * @param   { * } value -
   * @returns { Boolean } -
   */
  function isString(value) {
    return typeof value === T_STRING
  }



  var check = /*#__PURE__*/Object.freeze({
    isBlank: isBlank,
    isFunction: isFunction,
    isObject: isObject,
    isSvg: isSvg,
    isWritable: isWritable,
    isArray: isArray,
    isBoolAttr: isBoolAttr,
    isNil: isNil,
    isString: isString,
    isUndefined: isUndefined
  });

  /**
   * Check whether an array contains an item
   * @param   { Array } array - target array
   * @param   { * } item - item to test
   * @returns { Boolean } -
   */
  function contains(array, item) {
    return array.indexOf(item) !== -1
  }

  /**
   * Specialized function for looping an array-like collection with `each={}`
   * @param   { Array } list - collection of items
   * @param   {Function} fn - callback function
   * @returns { Array } the array looped
   */
  function each(list, fn) {
    var len = list ? list.length : 0;
    var i = 0;
    for (; i < len; i++) { fn(list[i], i); }
    return list
  }

  /**
   * Faster String startsWith alternative
   * @param   { String } str - source string
   * @param   { String } value - test string
   * @returns { Boolean } -
   */
  function startsWith(str, value) {
    return str.slice(0, value.length) === value
  }

  /**
   * Function returning always a unique identifier
   * @returns { Number } - number from 0...n
   */
  var uid = (function uid() {
    var i = -1;
    return function () { return ++i; }
  })()

  /**
   * Helper function to set an immutable property
   * @param   { Object } el - object where the new property will be set
   * @param   { String } key - object key where the new property will be stored
   * @param   { * } value - value of the new property
   * @param   { Object } options - set the propery overriding the default options
   * @returns { Object } - the initial object
   */
  function define(el, key, value, options) {
    Object.defineProperty(el, key, extend({
      value: value,
      enumerable: false,
      writable: false,
      configurable: true
    }, options));
    return el
  }

  /**
   * Convert a string containing dashes to camel case
   * @param   { String } str - input string
   * @returns { String } my-string -> myString
   */
  function toCamel(str) {
    return str.replace(/-(\w)/g, function (_, c) { return c.toUpperCase(); })
  }

  /**
   * Warn a message via console
   * @param   {String} message - warning message
   */
  function warn(message) {
    if (console && console.warn) { console.warn(message); }
  }



  var misc = /*#__PURE__*/Object.freeze({
    contains: contains,
    each: each,
    getPropDescriptor: getPropDescriptor,
    startsWith: startsWith,
    uid: uid,
    defineProperty: define,
    objectCreate: create,
    extend: extend,
    toCamel: toCamel,
    warn: warn
  });

  /**
   * Set the property of an object for a given key. If something already
   * exists there, then it becomes an array containing both the old and new value.
   * @param { Object } obj - object on which to set the property
   * @param { String } key - property name
   * @param { Object } value - the value of the property to be set
   * @param { Boolean } ensureArray - ensure that the property remains an array
   * @param { Number } index - add the new item in a certain array position
   */
  function arrayishAdd(obj, key, value, ensureArray, index) {
    var dest = obj[key];
    var isArr = isArray(dest);
    var hasIndex = !isUndefined(index);

    if (dest && dest === value) { return }

    // if the key was never set, set it once
    if (!dest && ensureArray) { obj[key] = [value]; }
    else if (!dest) { obj[key] = value; }
    // if it was an array and not yet set
    else {
      if (isArr) {
        var oldIndex = dest.indexOf(value);
        // this item never changed its position
        if (oldIndex === index) { return }
        // remove the item from its old position
        if (oldIndex !== -1) { dest.splice(oldIndex, 1); }
        // move or add the item
        if (hasIndex) {
          dest.splice(index, 0, value);
        } else {
          dest.push(value);
        }
      } else { obj[key] = [dest, value]; }
    }
  }

  /**
   * Detect the tag implementation by a DOM node
   * @param   { Object } dom - DOM node we need to parse to get its tag implementation
   * @returns { Object } it returns an object containing the implementation of a custom tag (template and boot function)
   */
  function get(dom) {
    return dom.tagName && __TAG_IMPL[getAttribute(dom, IS_DIRECTIVE) ||
      getAttribute(dom, IS_DIRECTIVE) || dom.tagName.toLowerCase()]
  }

  /**
   * Get the tag name of any DOM node
   * @param   { Object } dom - DOM node we want to parse
   * @param   { Boolean } skipDataIs - hack to ignore the data-is attribute when attaching to parent
   * @returns { String } name to identify this dom node in riot
   */
  function getName(dom, skipDataIs) {
    var child = get(dom);
    var namedTag = !skipDataIs && getAttribute(dom, IS_DIRECTIVE);
    return namedTag && !tmpl.hasExpr(namedTag) ?
      namedTag : child ? child.name : dom.tagName.toLowerCase()
  }

  /**
   * Return a temporary context containing also the parent properties
   * @this Tag
   * @param { Tag } - temporary tag context containing all the parent properties
   */
  function inheritParentProps() {
    if (this.parent) { return extend(create(this), this.parent) }
    return this
  }

  /*
    Includes hacks needed for the Internet Explorer version 9 and below
    See: http://kangax.github.io/compat-table/es5/#ie8
         http://codeplanet.io/dropping-ie8/
  */

  var
    reHasYield  = /<yield\b/i,
    reYieldAll  = /<yield\s*(?:\/>|>([\S\s]*?)<\/yield\s*>|>)/ig,
    reYieldSrc  = /<yield\s+to=['"]([^'">]*)['"]\s*>([\S\s]*?)<\/yield\s*>/ig,
    reYieldDest = /<yield\s+from=['"]?([-\w]+)['"]?\s*(?:\/>|>([\S\s]*?)<\/yield\s*>)/ig,
    rootEls = { tr: 'tbody', th: 'tr', td: 'tr', col: 'colgroup' },
    tblTags = IE_VERSION && IE_VERSION < 10 ? RE_SPECIAL_TAGS : RE_SPECIAL_TAGS_NO_OPTION,
    GENERIC = 'div',
    SVG = 'svg';


  /*
    Creates the root element for table or select child elements:
    tr/th/td/thead/tfoot/tbody/caption/col/colgroup/option/optgroup
  */
  function specialTags(el, tmpl, tagName) {

    var
      select = tagName[0] === 'o',
      parent = select ? 'select>' : 'table>';

    // trim() is important here, this ensures we don't have artifacts,
    // so we can check if we have only one element inside the parent
    el.innerHTML = '<' + parent + tmpl.trim() + '</' + parent;
    parent = el.firstChild;

    // returns the immediate parent if tr/th/td/col is the only element, if not
    // returns the whole tree, as this can include additional elements
    /* istanbul ignore next */
    if (select) {
      parent.selectedIndex = -1;  // for IE9, compatible w/current riot behavior
    } else {
      // avoids insertion of cointainer inside container (ex: tbody inside tbody)
      var tname = rootEls[tagName];
      if (tname && parent.childElementCount === 1) { parent = $(tname, parent); }
    }
    return parent
  }

  /*
    Replace the yield tag from any tag template with the innerHTML of the
    original tag in the page
  */
  function replaceYield(tmpl, html) {
    // do nothing if no yield
    if (!reHasYield.test(tmpl)) { return tmpl }

    // be careful with #1343 - string on the source having `$1`
    var src = {};

    html = html && html.replace(reYieldSrc, function (_, ref, text) {
      src[ref] = src[ref] || text;   // preserve first definition
      return ''
    }).trim();

    return tmpl
      .replace(reYieldDest, function (_, ref, def) {  // yield with from - to attrs
        return src[ref] || def || ''
      })
      .replace(reYieldAll, function (_, def) {        // yield without any "from"
        return html || def || ''
      })
  }

  /**
   * Creates a DOM element to wrap the given content. Normally an `DIV`, but can be
   * also a `TABLE`, `SELECT`, `TBODY`, `TR`, or `COLGROUP` element.
   *
   * @param   { String } tmpl  - The template coming from the custom tag definition
   * @param   { String } html - HTML content that comes from the DOM element where you
   *           will mount the tag, mostly the original tag in the page
   * @param   { Boolean } isSvg - true if the root node is an svg
   * @returns { HTMLElement } DOM element with _tmpl_ merged through `YIELD` with the _html_.
   */
  function mkdom(tmpl, html, isSvg) {
    var match   = tmpl && tmpl.match(/^\s*<([-\w]+)/);
    var  tagName = match && match[1].toLowerCase();
    var el = makeElement(isSvg ? SVG : GENERIC);

    // replace all the yield tags with the tag inner html
    tmpl = replaceYield(tmpl, html);

    /* istanbul ignore next */
    if (tblTags.test(tagName))
      { el = specialTags(el, tmpl, tagName); }
    else
      { setInnerHTML(el, tmpl, isSvg); }

    return el
  }

  var EVENT_ATTR_RE = /^on/;

  /**
   * True if the event attribute starts with 'on'
   * @param   { String } attribute - event attribute
   * @returns { Boolean }
   */
  function isEventAttribute(attribute) {
    return EVENT_ATTR_RE.test(attribute)
  }

  /**
   * Loop backward all the parents tree to detect the first custom parent tag
   * @param   { Object } tag - a Tag instance
   * @returns { Object } the instance of the first custom parent tag found
   */
  function getImmediateCustomParent(tag) {
    var ptag = tag;
    while (ptag.__.isAnonymous) {
      if (!ptag.parent) { break }
      ptag = ptag.parent;
    }
    return ptag
  }

  /**
   * Trigger DOM events
   * @param   { HTMLElement } dom - dom element target of the event
   * @param   { Function } handler - user function
   * @param   { Object } e - event object
   */
  function handleEvent(dom, handler, e) {
    var ptag = this.__.parent;
    var item = this.__.item;

    if (!item)
      { while (ptag && !item) {
        item = ptag.__.item;
        ptag = ptag.__.parent;
      } }

    // override the event properties
    /* istanbul ignore next */
    if (isWritable(e, 'currentTarget')) { e.currentTarget = dom; }
    /* istanbul ignore next */
    if (isWritable(e, 'target')) { e.target = e.srcElement; }
    /* istanbul ignore next */
    if (isWritable(e, 'which')) { e.which = e.charCode || e.keyCode; }

    e.item = item;

    handler.call(this, e);

    // avoid auto updates
    if (!settings.autoUpdate) { return }

    if (!e.preventUpdate) {
      var p = getImmediateCustomParent(this);
      // fixes #2083
      if (p.isMounted) { p.update(); }
    }
  }

  /**
   * Attach an event to a DOM node
   * @param { String } name - event name
   * @param { Function } handler - event callback
   * @param { Object } dom - dom node
   * @param { Tag } tag - tag instance
   */
  function setEventHandler(name, handler, dom, tag) {
    var eventName;
    var cb = handleEvent.bind(tag, dom, handler);

    // avoid to bind twice the same event
    // possible fix for #2332
    dom[name] = null;

    // normalize event name
    eventName = name.replace(RE_EVENTS_PREFIX, '');

    // cache the listener into the listeners array
    if (!contains(tag.__.listeners, dom)) { tag.__.listeners.push(dom); }
    if (!dom[RIOT_EVENTS_KEY]) { dom[RIOT_EVENTS_KEY] = {}; }
    if (dom[RIOT_EVENTS_KEY][name]) { dom.removeEventListener(eventName, dom[RIOT_EVENTS_KEY][name]); }

    dom[RIOT_EVENTS_KEY][name] = cb;
    dom.addEventListener(eventName, cb, false);
  }

  /**
   * Create a new child tag including it correctly into its parent
   * @param   { Object } child - child tag implementation
   * @param   { Object } opts - tag options containing the DOM node where the tag will be mounted
   * @param   { String } innerHTML - inner html of the child node
   * @param   { Object } parent - instance of the parent tag including the child custom tag
   * @returns { Object } instance of the new child tag just created
   */
  function initChild(child, opts, innerHTML, parent) {
    var tag = createTag(child, opts, innerHTML);
    var tagName = opts.tagName || getName(opts.root, true);
    var ptag = getImmediateCustomParent(parent);
    // fix for the parent attribute in the looped elements
    define(tag, 'parent', ptag);
    // store the real parent tag
    // in some cases this could be different from the custom parent tag
    // for example in nested loops
    tag.__.parent = parent;

    // add this tag to the custom parent tag
    arrayishAdd(ptag.tags, tagName, tag);

    // and also to the real parent tag
    if (ptag !== parent)
      { arrayishAdd(parent.tags, tagName, tag); }

    return tag
  }

  /**
   * Removes an item from an object at a given key. If the key points to an array,
   * then the item is just removed from the array.
   * @param { Object } obj - object on which to remove the property
   * @param { String } key - property name
   * @param { Object } value - the value of the property to be removed
   * @param { Boolean } ensureArray - ensure that the property remains an array
  */
  function arrayishRemove(obj, key, value, ensureArray) {
    if (isArray(obj[key])) {
      var index = obj[key].indexOf(value);
      if (index !== -1) { obj[key].splice(index, 1); }
      if (!obj[key].length) { delete obj[key]; }
      else if (obj[key].length === 1 && !ensureArray) { obj[key] = obj[key][0]; }
    } else if (obj[key] === value)
      { delete obj[key]; } // otherwise just delete the key
  }

  /**
   * Adds the elements for a virtual tag
   * @this Tag
   * @param { Node } src - the node that will do the inserting or appending
   * @param { Tag } target - only if inserting, insert before this tag's first child
   */
  function makeVirtual(src, target) {
    var this$1 = this;

    var head = createDOMPlaceholder();
    var tail = createDOMPlaceholder();
    var frag = createFragment();
    var sib;
    var el;

    this.root.insertBefore(head, this.root.firstChild);
    this.root.appendChild(tail);

    this.__.head = el = head;
    this.__.tail = tail;

    while (el) {
      sib = el.nextSibling;
      frag.appendChild(el);
      this$1.__.virts.push(el); // hold for unmounting
      el = sib;
    }

    if (target)
      { src.insertBefore(frag, target.__.head); }
    else
      { src.appendChild(frag); }
  }

  /**
   * makes a tag virtual and replaces a reference in the dom
   * @this Tag
   * @param { tag } the tag to make virtual
   * @param { ref } the dom reference location
   */
  function makeReplaceVirtual(tag, ref) {
    var frag = createFragment();
    makeVirtual.call(tag, frag);
    ref.parentNode.replaceChild(frag, ref);
  }

  /**
   * Update dynamically created data-is tags with changing expressions
   * @param { Object } expr - expression tag and expression info
   * @param { Tag }    parent - parent for tag creation
   * @param { String } tagName - tag implementation we want to use
   */
  function updateDataIs(expr, parent, tagName) {
    var tag = expr.tag || expr.dom._tag;
    var ref;

    var ref$1 = tag ? tag.__ : {};
    var head = ref$1.head;
    var isVirtual = expr.dom.tagName === 'VIRTUAL';

    if (tag && expr.tagName === tagName) {
      tag.update();
      return
    }

    // sync _parent to accommodate changing tagnames
    if (tag) {
      // need placeholder before unmount
      if(isVirtual) {
        ref = createDOMPlaceholder();
        head.parentNode.insertBefore(ref, head);
      }

      tag.unmount(true);
    }

    // unable to get the tag name
    if (!isString(tagName)) { return }

    expr.impl = __TAG_IMPL[tagName];

    // unknown implementation
    if (!expr.impl) { return }

    expr.tag = tag = initChild(
      expr.impl, {
        root: expr.dom,
        parent: parent,
        tagName: tagName
      },
      expr.dom.innerHTML,
      parent
    );

    each(expr.attrs, function (a) { return setAttribute(tag.root, a.name, a.value); });
    expr.tagName = tagName;
    tag.mount();

    // root exist first time, after use placeholder
    if (isVirtual) { makeReplaceVirtual(tag, ref || tag.root); }

    // parent is the placeholder tag, not the dynamic tag so clean up
    parent.__.onUnmount = function () {
      var delName = tag.opts.dataIs;
      arrayishRemove(tag.parent.tags, delName, tag);
      arrayishRemove(tag.__.parent.tags, delName, tag);
      tag.unmount();
    };
  }

  /**
   * Nomalize any attribute removing the "riot-" prefix
   * @param   { String } attrName - original attribute name
   * @returns { String } valid html attribute name
   */
  function normalizeAttrName(attrName) {
    if (!attrName) { return null }
    attrName = attrName.replace(ATTRS_PREFIX, '');
    if (CASE_SENSITIVE_ATTRIBUTES[attrName]) { attrName = CASE_SENSITIVE_ATTRIBUTES[attrName]; }
    return attrName
  }

  /**
   * Update on single tag expression
   * @this Tag
   * @param { Object } expr - expression logic
   * @returns { undefined }
   */
  function updateExpression(expr) {
    if (this.root && getAttribute(this.root,'virtualized')) { return }

    var dom = expr.dom;
    // remove the riot- prefix
    var attrName = normalizeAttrName(expr.attr);
    var isToggle = contains([SHOW_DIRECTIVE, HIDE_DIRECTIVE], attrName);
    var isVirtual = expr.root && expr.root.tagName === 'VIRTUAL';
    var ref = this.__;
    var isAnonymous = ref.isAnonymous;
    var parent = dom && (expr.parent || dom.parentNode);
    // detect the style attributes
    var isStyleAttr = attrName === 'style';
    var isClassAttr = attrName === 'class';

    var value;

    // if it's a tag we could totally skip the rest
    if (expr._riot_id) {
      if (expr.__.wasCreated) {
        expr.update();
      // if it hasn't been mounted yet, do that now.
      } else {
        expr.mount();
        if (isVirtual) {
          makeReplaceVirtual(expr, expr.root);
        }
      }
      return
    }

    // if this expression has the update method it means it can handle the DOM changes by itself
    if (expr.update) { return expr.update() }

    var context = isToggle && !isAnonymous ? inheritParentProps.call(this) : this;

    // ...it seems to be a simple expression so we try to calculate its value
    value = tmpl(expr.expr, context);

    var hasValue = !isBlank(value);
    var isObj = isObject(value);

    // convert the style/class objects to strings
    if (isObj) {
      if (isClassAttr) {
        value = tmpl(JSON.stringify(value), this);
      } else if (isStyleAttr) {
        value = styleObjectToString(value);
      }
    }

    // remove original attribute
    if (expr.attr && (!expr.wasParsedOnce || !hasValue || value === false)) {
      // remove either riot-* attributes or just the attribute name
      removeAttribute(dom, getAttribute(dom, expr.attr) ? expr.attr : attrName);
    }

    // for the boolean attributes we don't need the value
    // we can convert it to checked=true to checked=checked
    if (expr.bool) { value = value ? attrName : false; }
    if (expr.isRtag) { return updateDataIs(expr, this, value) }
    if (expr.wasParsedOnce && expr.value === value) { return }

    // update the expression value
    expr.value = value;
    expr.wasParsedOnce = true;

    // if the value is an object (and it's not a style or class attribute) we can not do much more with it
    if (isObj && !isClassAttr && !isStyleAttr && !isToggle) { return }
    // avoid to render undefined/null values
    if (!hasValue) { value = ''; }

    // textarea and text nodes have no attribute name
    if (!attrName) {
      // about #815 w/o replace: the browser converts the value to a string,
      // the comparison by "==" does too, but not in the server
      value += '';
      // test for parent avoids error with invalid assignment to nodeValue
      if (parent) {
        // cache the parent node because somehow it will become null on IE
        // on the next iteration
        expr.parent = parent;
        if (parent.tagName === 'TEXTAREA') {
          parent.value = value;                    // #1113
          if (!IE_VERSION) { dom.nodeValue = value; }  // #1625 IE throws here, nodeValue
        }                                         // will be available on 'updated'
        else { dom.nodeValue = value; }
      }
      return
    }

    switch (true) {
    // handle events binding
    case isFunction(value):
      if (isEventAttribute(attrName)) {
        setEventHandler(attrName, value, dom, this);
      }
      break
    // show / hide
    case isToggle:
      toggleVisibility(dom, attrName === HIDE_DIRECTIVE ? !value : value);
      break
    // handle attributes
    default:
      if (expr.bool) {
        dom[attrName] = value;
      }

      if (attrName === 'value' && dom.value !== value) {
        dom.value = value;
      } else if (hasValue && value !== false) {
        setAttribute(dom, attrName, value);
      }

      // make sure that in case of style changes
      // the element stays hidden
      if (isStyleAttr && dom.hidden) { toggleVisibility(dom, false); }
    }
  }

  /**
   * Update all the expressions in a Tag instance
   * @this Tag
   * @param { Array } expressions - expression that must be re evaluated
   */
  function update(expressions) {
    each(expressions, updateExpression.bind(this));
  }

  /**
   * We need to update opts for this tag. That requires updating the expressions
   * in any attributes on the tag, and then copying the result onto opts.
   * @this Tag
   * @param   {Boolean} isLoop - is it a loop tag?
   * @param   { Tag }  parent - parent tag node
   * @param   { Boolean }  isAnonymous - is it a tag without any impl? (a tag not registered)
   * @param   { Object }  opts - tag options
   * @param   { Array }  instAttrs - tag attributes array
   */
  function updateOpts(isLoop, parent, isAnonymous, opts, instAttrs) {
    // isAnonymous `each` tags treat `dom` and `root` differently. In this case
    // (and only this case) we don't need to do updateOpts, because the regular parse
    // will update those attrs. Plus, isAnonymous tags don't need opts anyway
    if (isLoop && isAnonymous) { return }
    var ctx = isLoop ? inheritParentProps.call(this) : parent || this;

    each(instAttrs, function (attr) {
      if (attr.expr) { updateExpression.call(ctx, attr.expr); }
      // normalize the attribute names
      opts[toCamel(attr.name).replace(ATTRS_PREFIX, '')] = attr.expr ? attr.expr.value : attr.value;
    });
  }

  /**
   * Update the tag expressions and options
   * @param { Tag } tag - tag object
   * @param { * } data - data we want to use to extend the tag properties
   * @param { Array } expressions - component expressions array
   * @returns { Tag } the current tag instance
   */
  function componentUpdate(tag, data, expressions) {
    var __ = tag.__;
    var nextOpts = {};
    var canTrigger = tag.isMounted && !__.skipAnonymous;

    // inherit properties from the parent tag
    if (__.isAnonymous && __.parent) { extend(tag, __.parent); }
    extend(tag, data);

    updateOpts.apply(tag, [__.isLoop, __.parent, __.isAnonymous, nextOpts, __.instAttrs]);

    if (
      canTrigger &&
      tag.isMounted &&
      isFunction(tag.shouldUpdate) && !tag.shouldUpdate(data, nextOpts)
    ) {
      return tag
    }

    extend(tag.opts, nextOpts);

    if (canTrigger) { tag.trigger('update', data); }
    update.call(tag, expressions);
    if (canTrigger) { tag.trigger('updated'); }

    return tag
  }

  /**
   * Get selectors for tags
   * @param   { Array } tags - tag names to select
   * @returns { String } selector
   */
  function query(tags) {
    // select all tags
    if (!tags) {
      var keys = Object.keys(__TAG_IMPL);
      return keys + query(keys)
    }

    return tags
      .filter(function (t) { return !/[^-\w]/.test(t); })
      .reduce(function (list, t) {
        var name = t.trim().toLowerCase();
        return list + ",[" + IS_DIRECTIVE + "=\"" + name + "\"]"
      }, '')
  }

  /**
   * Another way to create a riot tag a bit more es6 friendly
   * @param { HTMLElement } el - tag DOM selector or DOM node/s
   * @param { Object } opts - tag logic
   * @returns { Tag } new riot tag instance
   */
  function Tag(el, opts) {
    // get the tag properties from the class constructor
    var ref = this;
    var name = ref.name;
    var tmpl = ref.tmpl;
    var css = ref.css;
    var attrs = ref.attrs;
    var onCreate = ref.onCreate;
    // register a new tag and cache the class prototype
    if (!__TAG_IMPL[name]) {
      tag(name, tmpl, css, attrs, onCreate);
      // cache the class constructor
      __TAG_IMPL[name].class = this.constructor;
    }

    // mount the tag using the class instance
    mount$1(el, name, opts, this);
    // inject the component css
    if (css) { styleManager.inject(); }

    return this
  }

  /**
   * Create a new riot tag implementation
   * @param   { String }   name - name/id of the new riot tag
   * @param   { String }   tmpl - tag template
   * @param   { String }   css - custom tag css
   * @param   { String }   attrs - root tag attributes
   * @param   { Function } fn - user function
   * @returns { String } name/id of the tag just created
   */
  function tag(name, tmpl, css, attrs, fn) {
    if (isFunction(attrs)) {
      fn = attrs;

      if (/^[\w-]+\s?=/.test(css)) {
        attrs = css;
        css = '';
      } else
        { attrs = ''; }
    }

    if (css) {
      if (isFunction(css))
        { fn = css; }
      else
        { styleManager.add(css, name); }
    }

    name = name.toLowerCase();
    __TAG_IMPL[name] = { name: name, tmpl: tmpl, attrs: attrs, fn: fn };

    return name
  }

  /**
   * Create a new riot tag implementation (for use by the compiler)
   * @param   { String }   name - name/id of the new riot tag
   * @param   { String }   tmpl - tag template
   * @param   { String }   css - custom tag css
   * @param   { String }   attrs - root tag attributes
   * @param   { Function } fn - user function
   * @returns { String } name/id of the tag just created
   */
  function tag2(name, tmpl, css, attrs, fn) {
    if (css) { styleManager.add(css, name); }

    __TAG_IMPL[name] = { name: name, tmpl: tmpl, attrs: attrs, fn: fn };

    return name
  }

  /**
   * Mount a tag using a specific tag implementation
   * @param   { * } selector - tag DOM selector or DOM node/s
   * @param   { String } tagName - tag implementation name
   * @param   { Object } opts - tag logic
   * @returns { Array } new tags instances
   */
  function mount(selector, tagName, opts) {
    var tags = [];
    var elem, allTags;

    function pushTagsTo(root) {
      if (root.tagName) {
        var riotTag = getAttribute(root, IS_DIRECTIVE), tag;

        // have tagName? force riot-tag to be the same
        if (tagName && riotTag !== tagName) {
          riotTag = tagName;
          setAttribute(root, IS_DIRECTIVE, tagName);
        }

        tag = mount$1(root, riotTag || root.tagName.toLowerCase(), opts);

        if (tag)
          { tags.push(tag); }
      } else if (root.length)
        { each(root, pushTagsTo); } // assume nodeList
    }

    // inject styles into DOM
    styleManager.inject();

    if (isObject(tagName)) {
      opts = tagName;
      tagName = 0;
    }

    // crawl the DOM to find the tag
    if (isString(selector)) {
      selector = selector === '*' ?
        // select all registered tags
        // & tags found with the riot-tag attribute set
        allTags = query() :
        // or just the ones named like the selector
        selector + query(selector.split(/, */));

      // make sure to pass always a selector
      // to the querySelectorAll function
      elem = selector ? $$(selector) : [];
    }
    else
      // probably you have passed already a tag or a NodeList
      { elem = selector; }

    // select all the registered and mount them inside their root elements
    if (tagName === '*') {
      // get all custom tags
      tagName = allTags || query();
      // if the root els it's just a single tag
      if (elem.tagName)
        { elem = $$(tagName, elem); }
      else {
        // select all the children for all the different root elements
        var nodeList = [];

        each(elem, function (_el) { return nodeList.push($$(tagName, _el)); });

        elem = nodeList;
      }
      // get rid of the tagName
      tagName = 0;
    }

    pushTagsTo(elem);

    return tags
  }

  // Create a mixin that could be globally shared across all the tags
  var mixins = {};
  var globals = mixins[GLOBAL_MIXIN] = {};
  var mixins_id = 0;

  /**
   * Create/Return a mixin by its name
   * @param   { String }  name - mixin name (global mixin if object)
   * @param   { Object }  mix - mixin logic
   * @param   { Boolean } g - is global?
   * @returns { Object }  the mixin logic
   */
  function mixin(name, mix, g) {
    // Unnamed global
    if (isObject(name)) {
      mixin(("__" + (mixins_id++) + "__"), name, true);
      return
    }

    var store = g ? globals : mixins;

    // Getter
    if (!mix) {
      if (isUndefined(store[name]))
        { throw new Error(("Unregistered mixin: " + name)) }

      return store[name]
    }

    // Setter
    store[name] = isFunction(mix) ?
      extend(mix.prototype, store[name] || {}) && mix :
      extend(store[name] || {}, mix);
  }

  /**
   * Update all the tags instances created
   * @returns { Array } all the tags instances
   */
  function update$1() {
    return each(__TAGS_CACHE, function (tag) { return tag.update(); })
  }

  function unregister(name) {
    styleManager.remove(name);
    return delete __TAG_IMPL[name]
  }

  var version = 'v3.11.1';

  var core = /*#__PURE__*/Object.freeze({
    Tag: Tag,
    tag: tag,
    tag2: tag2,
    mount: mount,
    mixin: mixin,
    update: update$1,
    unregister: unregister,
    version: version
  });

  /**
   * Add a mixin to this tag
   * @returns { Tag } the current tag instance
   */
  function componentMixin(tag$$1) {
    var mixins = [], len = arguments.length - 1;
    while ( len-- > 0 ) mixins[ len ] = arguments[ len + 1 ];

    each(mixins, function (mix) {
      var instance;
      var obj;
      var props = [];

      // properties blacklisted and will not be bound to the tag instance
      var propsBlacklist = ['init', '__proto__'];

      mix = isString(mix) ? mixin(mix) : mix;

      // check if the mixin is a function
      if (isFunction(mix)) {
        // create the new mixin instance
        instance = new mix();
      } else { instance = mix; }

      var proto = Object.getPrototypeOf(instance);

      // build multilevel prototype inheritance chain property list
      do { props = props.concat(Object.getOwnPropertyNames(obj || instance)); }
      while (obj = Object.getPrototypeOf(obj || instance))

      // loop the keys in the function prototype or the all object keys
      each(props, function (key) {
        // bind methods to tag
        // allow mixins to override other properties/parent mixins
        if (!contains(propsBlacklist, key)) {
          // check for getters/setters
          var descriptor = getPropDescriptor(instance, key) || getPropDescriptor(proto, key);
          var hasGetterSetter = descriptor && (descriptor.get || descriptor.set);

          // apply method only if it does not already exist on the instance
          if (!tag$$1.hasOwnProperty(key) && hasGetterSetter) {
            Object.defineProperty(tag$$1, key, descriptor);
          } else {
            tag$$1[key] = isFunction(instance[key]) ?
              instance[key].bind(tag$$1) :
              instance[key];
          }
        }
      });

      // init method will be called automatically
      if (instance.init)
        { instance.init.bind(tag$$1)(tag$$1.opts); }
    });

    return tag$$1
  }

  /**
   * Move the position of a custom tag in its parent tag
   * @this Tag
   * @param   { String } tagName - key where the tag was stored
   * @param   { Number } newPos - index where the new tag will be stored
   */
  function moveChild(tagName, newPos) {
    var parent = this.parent;
    var tags;
    // no parent no move
    if (!parent) { return }

    tags = parent.tags[tagName];

    if (isArray(tags))
      { tags.splice(newPos, 0, tags.splice(tags.indexOf(this), 1)[0]); }
    else { arrayishAdd(parent.tags, tagName, this); }
  }

  /**
   * Move virtual tag and all child nodes
   * @this Tag
   * @param { Node } src  - the node that will do the inserting
   * @param { Tag } target - insert before this tag's first child
   */
  function moveVirtual(src, target) {
    var this$1 = this;

    var el = this.__.head;
    var sib;
    var frag = createFragment();

    while (el) {
      sib = el.nextSibling;
      frag.appendChild(el);
      el = sib;
      if (el === this$1.__.tail) {
        frag.appendChild(el);
        src.insertBefore(frag, target.__.head);
        break
      }
    }
  }

  /**
   * Convert the item looped into an object used to extend the child tag properties
   * @param   { Object } expr - object containing the keys used to extend the children tags
   * @param   { * } key - value to assign to the new object returned
   * @param   { * } val - value containing the position of the item in the array
   * @returns { Object } - new object containing the values of the original item
   *
   * The variables 'key' and 'val' are arbitrary.
   * They depend on the collection type looped (Array, Object)
   * and on the expression used on the each tag
   *
   */
  function mkitem(expr, key, val) {
    var item = {};
    item[expr.key] = key;
    if (expr.pos) { item[expr.pos] = val; }
    return item
  }

  /**
   * Unmount the redundant tags
   * @param   { Array } items - array containing the current items to loop
   * @param   { Array } tags - array containing all the children tags
   */
  function unmountRedundant(items, tags, filteredItemsCount) {
    var i = tags.length;
    var j = items.length - filteredItemsCount;

    while (i > j) {
      i--;
      remove.apply(tags[i], [tags, i]);
    }
  }


  /**
   * Remove a child tag
   * @this Tag
   * @param   { Array } tags - tags collection
   * @param   { Number } i - index of the tag to remove
   */
  function remove(tags, i) {
    tags.splice(i, 1);
    this.unmount();
    arrayishRemove(this.parent, this, this.__.tagName, true);
  }

  /**
   * Move the nested custom tags in non custom loop tags
   * @this Tag
   * @param   { Number } i - current position of the loop tag
   */
  function moveNestedTags(i) {
    var this$1 = this;

    each(Object.keys(this.tags), function (tagName) {
      moveChild.apply(this$1.tags[tagName], [tagName, i]);
    });
  }

  /**
   * Move a child tag
   * @this Tag
   * @param   { HTMLElement } root - dom node containing all the loop children
   * @param   { Tag } nextTag - instance of the next tag preceding the one we want to move
   * @param   { Boolean } isVirtual - is it a virtual tag?
   */
  function move(root, nextTag, isVirtual) {
    if (isVirtual)
      { moveVirtual.apply(this, [root, nextTag]); }
    else
      { safeInsert(root, this.root, nextTag.root); }
  }

  /**
   * Insert and mount a child tag
   * @this Tag
   * @param   { HTMLElement } root - dom node containing all the loop children
   * @param   { Tag } nextTag - instance of the next tag preceding the one we want to insert
   * @param   { Boolean } isVirtual - is it a virtual tag?
   */
  function insert(root, nextTag, isVirtual) {
    if (isVirtual)
      { makeVirtual.apply(this, [root, nextTag]); }
    else
      { safeInsert(root, this.root, nextTag.root); }
  }

  /**
   * Append a new tag into the DOM
   * @this Tag
   * @param   { HTMLElement } root - dom node containing all the loop children
   * @param   { Boolean } isVirtual - is it a virtual tag?
   */
  function append(root, isVirtual) {
    if (isVirtual)
      { makeVirtual.call(this, root); }
    else
      { root.appendChild(this.root); }
  }

  /**
   * Return the value we want to use to lookup the postion of our items in the collection
   * @param   { String }  keyAttr         - lookup string or expression
   * @param   { * }       originalItem    - original item from the collection
   * @param   { Object }  keyedItem       - object created by riot via { item, i in collection }
   * @param   { Boolean } hasKeyAttrExpr  - flag to check whether the key is an expression
   * @returns { * } value that we will use to figure out the item position via collection.indexOf
   */
  function getItemId(keyAttr, originalItem, keyedItem, hasKeyAttrExpr) {
    if (keyAttr) {
      return hasKeyAttrExpr ?  tmpl(keyAttr, keyedItem) :  originalItem[keyAttr]
    }

    return originalItem
  }

  /**
   * Manage tags having the 'each'
   * @param   { HTMLElement } dom - DOM node we need to loop
   * @param   { Tag } parent - parent tag instance where the dom node is contained
   * @param   { String } expr - string contained in the 'each' attribute
   * @returns { Object } expression object for this each loop
   */
  function _each(dom, parent, expr) {
    var mustReorder = typeof getAttribute(dom, LOOP_NO_REORDER_DIRECTIVE) !== T_STRING || removeAttribute(dom, LOOP_NO_REORDER_DIRECTIVE);
    var keyAttr = getAttribute(dom, KEY_DIRECTIVE);
    var hasKeyAttrExpr = keyAttr ? tmpl.hasExpr(keyAttr) : false;
    var tagName = getName(dom);
    var impl = __TAG_IMPL[tagName];
    var parentNode = dom.parentNode;
    var placeholder = createDOMPlaceholder();
    var child = get(dom);
    var ifExpr = getAttribute(dom, CONDITIONAL_DIRECTIVE);
    var tags = [];
    var isLoop = true;
    var innerHTML = dom.innerHTML;
    var isAnonymous = !__TAG_IMPL[tagName];
    var isVirtual = dom.tagName === 'VIRTUAL';
    var oldItems = [];

    // remove the each property from the original tag
    removeAttribute(dom, LOOP_DIRECTIVE);
    removeAttribute(dom, KEY_DIRECTIVE);

    // parse the each expression
    expr = tmpl.loopKeys(expr);
    expr.isLoop = true;

    if (ifExpr) { removeAttribute(dom, CONDITIONAL_DIRECTIVE); }

    // insert a marked where the loop tags will be injected
    parentNode.insertBefore(placeholder, dom);
    parentNode.removeChild(dom);

    expr.update = function updateEach() {
      // get the new items collection
      expr.value = tmpl(expr.val, parent);

      var items = expr.value;
      var frag = createFragment();
      var isObject = !isArray(items) && !isString(items);
      var root = placeholder.parentNode;
      var tmpItems = [];
      var hasKeys = isObject && !!items;

      // if this DOM was removed the update here is useless
      // this condition fixes also a weird async issue on IE in our unit test
      if (!root) { return }

      // object loop. any changes cause full redraw
      if (isObject) {
        items = items ? Object.keys(items).map(function (key) { return mkitem(expr, items[key], key); }) : [];
      }

      // store the amount of filtered items
      var filteredItemsCount = 0;

      // loop all the new items
      each(items, function (_item, index) {
        var i = index - filteredItemsCount;
        var item = !hasKeys && expr.key ? mkitem(expr, _item, index) : _item;

        // skip this item because it must be filtered
        if (ifExpr && !tmpl(ifExpr, extend(create(parent), item))) {
          filteredItemsCount ++;
          return
        }

        var itemId = getItemId(keyAttr, _item, item, hasKeyAttrExpr);
        // reorder only if the items are not objects
        // or a key attribute has been provided
        var doReorder = !isObject && mustReorder && typeof _item === T_OBJECT || keyAttr;
        var oldPos = oldItems.indexOf(itemId);
        var isNew = oldPos === -1;
        var pos = !isNew && doReorder ? oldPos : i;
        // does a tag exist in this position?
        var tag = tags[pos];
        var mustAppend = i >= oldItems.length;
        var mustCreate = doReorder && isNew || !doReorder && !tag || !tags[i];

        // new tag
        if (mustCreate) {
          tag = createTag(impl, {
            parent: parent,
            isLoop: isLoop,
            isAnonymous: isAnonymous,
            tagName: tagName,
            root: dom.cloneNode(isAnonymous),
            item: item,
            index: i,
          }, innerHTML);

          // mount the tag
          tag.mount();

          if (mustAppend)
            { append.apply(tag, [frag || root, isVirtual]); }
          else
            { insert.apply(tag, [root, tags[i], isVirtual]); }

          if (!mustAppend) { oldItems.splice(i, 0, item); }
          tags.splice(i, 0, tag);
          if (child) { arrayishAdd(parent.tags, tagName, tag, true); }
        } else if (pos !== i && doReorder) {
          // move
          if (keyAttr || contains(items, oldItems[pos])) {
            move.apply(tag, [root, tags[i], isVirtual]);
            // move the old tag instance
            tags.splice(i, 0, tags.splice(pos, 1)[0]);
            // move the old item
            oldItems.splice(i, 0, oldItems.splice(pos, 1)[0]);
          }

          // update the position attribute if it exists
          if (expr.pos) { tag[expr.pos] = i; }

          // if the loop tags are not custom
          // we need to move all their custom tags into the right position
          if (!child && tag.tags) { moveNestedTags.call(tag, i); }
        }

        // cache the original item to use it in the events bound to this node
        // and its children
        extend(tag.__, {
          item: item,
          index: i,
          parent: parent
        });

        tmpItems[i] = itemId;

        if (!mustCreate) { tag.update(item); }
      });

      // remove the redundant tags
      unmountRedundant(items, tags, filteredItemsCount);

      // clone the items array
      oldItems = tmpItems.slice();

      root.insertBefore(frag, placeholder);
    };

    expr.unmount = function () {
      each(tags, function (t) { t.unmount(); });
    };

    return expr
  }

  var RefExpr = {
    init: function init(dom, parent, attrName, attrValue) {
      this.dom = dom;
      this.attr = attrName;
      this.rawValue = attrValue;
      this.parent = parent;
      this.hasExp = tmpl.hasExpr(attrValue);
      return this
    },
    update: function update() {
      var old = this.value;
      var customParent = this.parent && getImmediateCustomParent(this.parent);
      // if the referenced element is a custom tag, then we set the tag itself, rather than DOM
      var tagOrDom = this.dom.__ref || this.tag || this.dom;

      this.value = this.hasExp ? tmpl(this.rawValue, this.parent) : this.rawValue;

      // the name changed, so we need to remove it from the old key (if present)
      if (!isBlank(old) && customParent) { arrayishRemove(customParent.refs, old, tagOrDom); }
      if (!isBlank(this.value) && isString(this.value)) {
        // add it to the refs of parent tag (this behavior was changed >=3.0)
        if (customParent) { arrayishAdd(
          customParent.refs,
          this.value,
          tagOrDom,
          // use an array if it's a looped node and the ref is not an expression
          null,
          this.parent.__.index
        ); }

        if (this.value !== old) {
          setAttribute(this.dom, this.attr, this.value);
        }
      } else {
        removeAttribute(this.dom, this.attr);
      }

      // cache the ref bound to this dom node
      // to reuse it in future (see also #2329)
      if (!this.dom.__ref) { this.dom.__ref = tagOrDom; }
    },
    unmount: function unmount() {
      var tagOrDom = this.tag || this.dom;
      var customParent = this.parent && getImmediateCustomParent(this.parent);
      if (!isBlank(this.value) && customParent)
        { arrayishRemove(customParent.refs, this.value, tagOrDom); }
    }
  }

  /**
   * Create a new ref directive
   * @param   { HTMLElement } dom - dom node having the ref attribute
   * @param   { Tag } context - tag instance where the DOM node is located
   * @param   { String } attrName - either 'ref' or 'data-ref'
   * @param   { String } attrValue - value of the ref attribute
   * @returns { RefExpr } a new RefExpr object
   */
  function createRefDirective(dom, tag, attrName, attrValue) {
    return create(RefExpr).init(dom, tag, attrName, attrValue)
  }

  /**
   * Trigger the unmount method on all the expressions
   * @param   { Array } expressions - DOM expressions
   */
  function unmountAll(expressions) {
    each(expressions, function (expr) {
      if (expr.unmount) { expr.unmount(true); }
      else if (expr.tagName) { expr.tag.unmount(true); }
      else if (expr.unmount) { expr.unmount(); }
    });
  }

  var IfExpr = {
    init: function init(dom, tag, expr) {
      removeAttribute(dom, CONDITIONAL_DIRECTIVE);
      extend(this, { tag: tag, expr: expr, stub: createDOMPlaceholder(), pristine: dom });
      var p = dom.parentNode;
      p.insertBefore(this.stub, dom);
      p.removeChild(dom);

      return this
    },
    update: function update$$1() {
      this.value = tmpl(this.expr, this.tag);

      if (!this.stub.parentNode) { return }

      if (this.value && !this.current) { // insert
        this.current = this.pristine.cloneNode(true);
        this.stub.parentNode.insertBefore(this.current, this.stub);
        this.expressions = parseExpressions.apply(this.tag, [this.current, true]);
      } else if (!this.value && this.current) { // remove
        this.unmount();
        this.current = null;
        this.expressions = [];
      }

      if (this.value) { update.call(this.tag, this.expressions); }
    },
    unmount: function unmount() {
      if (this.current) {
        if (this.current._tag) {
          this.current._tag.unmount();
        } else if (this.current.parentNode) {
          this.current.parentNode.removeChild(this.current);
        }
      }

      unmountAll(this.expressions || []);
    }
  }

  /**
   * Create a new if directive
   * @param   { HTMLElement } dom - if root dom node
   * @param   { Tag } context - tag instance where the DOM node is located
   * @param   { String } attr - if expression
   * @returns { IFExpr } a new IfExpr object
   */
  function createIfDirective(dom, tag, attr) {
    return create(IfExpr).init(dom, tag, attr)
  }

  /**
   * Walk the tag DOM to detect the expressions to evaluate
   * @this Tag
   * @param   { HTMLElement } root - root tag where we will start digging the expressions
   * @param   { Boolean } mustIncludeRoot - flag to decide whether the root must be parsed as well
   * @returns { Array } all the expressions found
   */
  function parseExpressions(root, mustIncludeRoot) {
    var this$1 = this;

    var expressions = [];

    walkNodes(root, function (dom) {
      var type = dom.nodeType;
      var attr;
      var tagImpl;

      if (!mustIncludeRoot && dom === root) { return }

      // text node
      if (type === 3 && dom.parentNode.tagName !== 'STYLE' && tmpl.hasExpr(dom.nodeValue))
        { expressions.push({dom: dom, expr: dom.nodeValue}); }

      if (type !== 1) { return }

      var isVirtual = dom.tagName === 'VIRTUAL';

      // loop. each does it's own thing (for now)
      if (attr = getAttribute(dom, LOOP_DIRECTIVE)) {
        if(isVirtual) { setAttribute(dom, 'loopVirtual', true); } // ignore here, handled in _each
        expressions.push(_each(dom, this$1, attr));
        return false
      }

      // if-attrs become the new parent. Any following expressions (either on the current
      // element, or below it) become children of this expression.
      if (attr = getAttribute(dom, CONDITIONAL_DIRECTIVE)) {
        expressions.push(createIfDirective(dom, this$1, attr));
        return false
      }

      if (attr = getAttribute(dom, IS_DIRECTIVE)) {
        if (tmpl.hasExpr(attr)) {
          expressions.push({
            isRtag: true,
            expr: attr,
            dom: dom,
            attrs: [].slice.call(dom.attributes)
          });

          return false
        }
      }

      // if this is a tag, stop traversing here.
      // we ignore the root, since parseExpressions is called while we're mounting that root
      tagImpl = get(dom);

      if(isVirtual) {
        if(getAttribute(dom, 'virtualized')) {dom.parentElement.removeChild(dom); } // tag created, remove from dom
        if(!tagImpl && !getAttribute(dom, 'virtualized') && !getAttribute(dom, 'loopVirtual'))  // ok to create virtual tag
          { tagImpl = { tmpl: dom.outerHTML }; }
      }

      if (tagImpl && (dom !== root || mustIncludeRoot)) {
        var hasIsDirective = getAttribute(dom, IS_DIRECTIVE);
        if(isVirtual && !hasIsDirective) { // handled in update
          // can not remove attribute like directives
          // so flag for removal after creation to prevent maximum stack error
          setAttribute(dom, 'virtualized', true);
          var tag = createTag(
            {tmpl: dom.outerHTML},
            {root: dom, parent: this$1},
            dom.innerHTML
          );

          expressions.push(tag); // no return, anonymous tag, keep parsing
        } else {
          if (hasIsDirective && isVirtual)
            { warn(("Virtual tags shouldn't be used together with the \"" + IS_DIRECTIVE + "\" attribute - https://github.com/riot/riot/issues/2511")); }

          expressions.push(
            initChild(
              tagImpl,
              {
                root: dom,
                parent: this$1
              },
              dom.innerHTML,
              this$1
            )
          );
          return false
        }
      }

      // attribute expressions
      parseAttributes.apply(this$1, [dom, dom.attributes, function (attr, expr) {
        if (!expr) { return }
        expressions.push(expr);
      }]);
    });

    return expressions
  }

  /**
   * Calls `fn` for every attribute on an element. If that attr has an expression,
   * it is also passed to fn.
   * @this Tag
   * @param   { HTMLElement } dom - dom node to parse
   * @param   { Array } attrs - array of attributes
   * @param   { Function } fn - callback to exec on any iteration
   */
  function parseAttributes(dom, attrs, fn) {
    var this$1 = this;

    each(attrs, function (attr) {
      if (!attr) { return false }

      var name = attr.name;
      var bool = isBoolAttr(name);
      var expr;

      if (contains(REF_DIRECTIVES, name) && dom.tagName.toLowerCase() !== YIELD_TAG) {
        expr =  createRefDirective(dom, this$1, name, attr.value);
      } else if (tmpl.hasExpr(attr.value)) {
        expr = {dom: dom, expr: attr.value, attr: name, bool: bool};
      }

      fn(attr, expr);
    });
  }

  /**
   * Manage the mount state of a tag triggering also the observable events
   * @this Tag
   * @param { Boolean } value - ..of the isMounted flag
   */
  function setMountState(value) {
    var ref = this.__;
    var isAnonymous = ref.isAnonymous;

    define(this, 'isMounted', value);

    if (!isAnonymous) {
      if (value) { this.trigger('mount'); }
      else {
        this.trigger('unmount');
        this.off('*');
        this.__.wasCreated = false;
      }
    }
  }

  /**
   * Mount the current tag instance
   * @returns { Tag } the current tag instance
   */
  function componentMount(tag$$1, dom, expressions, opts) {
    var __ = tag$$1.__;
    var root = __.root;
    root._tag = tag$$1; // keep a reference to the tag just created

    // Read all the attrs on this instance. This give us the info we need for updateOpts
    parseAttributes.apply(__.parent, [root, root.attributes, function (attr, expr) {
      if (!__.isAnonymous && RefExpr.isPrototypeOf(expr)) { expr.tag = tag$$1; }
      attr.expr = expr;
      __.instAttrs.push(attr);
    }]);

    // update the root adding custom attributes coming from the compiler
    walkAttributes(__.impl.attrs, function (k, v) { __.implAttrs.push({name: k, value: v}); });
    parseAttributes.apply(tag$$1, [root, __.implAttrs, function (attr, expr) {
      if (expr) { expressions.push(expr); }
      else { setAttribute(root, attr.name, attr.value); }
    }]);

    // initialiation
    updateOpts.apply(tag$$1, [__.isLoop, __.parent, __.isAnonymous, opts, __.instAttrs]);

    // add global mixins
    var globalMixin = mixin(GLOBAL_MIXIN);

    if (globalMixin && !__.skipAnonymous) {
      for (var i in globalMixin) {
        if (globalMixin.hasOwnProperty(i)) {
          tag$$1.mixin(globalMixin[i]);
        }
      }
    }

    if (__.impl.fn) { __.impl.fn.call(tag$$1, opts); }

    if (!__.skipAnonymous) { tag$$1.trigger('before-mount'); }

    // parse layout after init. fn may calculate args for nested custom tags
    each(parseExpressions.apply(tag$$1, [dom, __.isAnonymous]), function (e) { return expressions.push(e); });

    tag$$1.update(__.item);

    if (!__.isAnonymous && !__.isInline) {
      while (dom.firstChild) { root.appendChild(dom.firstChild); }
    }

    define(tag$$1, 'root', root);

    // if we need to wait that the parent "mount" or "updated" event gets triggered
    if (!__.skipAnonymous && tag$$1.parent) {
      var p = getImmediateCustomParent(tag$$1.parent);
      p.one(!p.isMounted ? 'mount' : 'updated', function () {
        setMountState.call(tag$$1, true);
      });
    } else {
      // otherwise it's not a child tag we can trigger its mount event
      setMountState.call(tag$$1, true);
    }

    tag$$1.__.wasCreated = true;

    return tag$$1
  }

  /**
   * Unmount the tag instance
   * @param { Boolean } mustKeepRoot - if it's true the root node will not be removed
   * @returns { Tag } the current tag instance
   */
  function tagUnmount(tag, mustKeepRoot, expressions) {
    var __ = tag.__;
    var root = __.root;
    var tagIndex = __TAGS_CACHE.indexOf(tag);
    var p = root.parentNode;

    if (!__.skipAnonymous) { tag.trigger('before-unmount'); }

    // clear all attributes coming from the mounted tag
    walkAttributes(__.impl.attrs, function (name) {
      if (startsWith(name, ATTRS_PREFIX))
        { name = name.slice(ATTRS_PREFIX.length); }

      removeAttribute(root, name);
    });

    // remove all the event listeners
    tag.__.listeners.forEach(function (dom) {
      Object.keys(dom[RIOT_EVENTS_KEY]).forEach(function (eventName) {
        dom.removeEventListener(eventName, dom[RIOT_EVENTS_KEY][eventName]);
      });
    });

    // remove tag instance from the global tags cache collection
    if (tagIndex !== -1) { __TAGS_CACHE.splice(tagIndex, 1); }

    // clean up the parent tags object
    if (__.parent && !__.isAnonymous) {
      var ptag = getImmediateCustomParent(__.parent);

      if (__.isVirtual) {
        Object
          .keys(tag.tags)
          .forEach(function (tagName) { return arrayishRemove(ptag.tags, tagName, tag.tags[tagName]); });
      } else {
        arrayishRemove(ptag.tags, __.tagName, tag);
      }
    }

    // unmount all the virtual directives
    if (tag.__.virts) {
      each(tag.__.virts, function (v) {
        if (v.parentNode) { v.parentNode.removeChild(v); }
      });
    }

    // allow expressions to unmount themselves
    unmountAll(expressions);
    each(__.instAttrs, function (a) { return a.expr && a.expr.unmount && a.expr.unmount(); });

    // clear the tag html if it's necessary
    if (mustKeepRoot) { setInnerHTML(root, ''); }
    // otherwise detach the root tag from the DOM
    else if (p) { p.removeChild(root); }

    // custom internal unmount function to avoid relying on the observable
    if (__.onUnmount) { __.onUnmount(); }

    // weird fix for a weird edge case #2409 and #2436
    // some users might use your software not as you've expected
    // so I need to add these dirty hacks to mitigate unexpected issues
    if (!tag.isMounted) { setMountState.call(tag, true); }

    setMountState.call(tag, false);

    delete root._tag;

    return tag
  }

  /**
   * Tag creation factory function
   * @constructor
   * @param { Object } impl - it contains the tag template, and logic
   * @param { Object } conf - tag options
   * @param { String } innerHTML - html that eventually we need to inject in the tag
   */
  function createTag(impl, conf, innerHTML) {
    if ( impl === void 0 ) impl = {};
    if ( conf === void 0 ) conf = {};

    var tag = conf.context || {};
    var opts = conf.opts || {};
    var parent = conf.parent;
    var isLoop = conf.isLoop;
    var isAnonymous = !!conf.isAnonymous;
    var skipAnonymous = settings.skipAnonymousTags && isAnonymous;
    var item = conf.item;
    // available only for the looped nodes
    var index = conf.index;
    // All attributes on the Tag when it's first parsed
    var instAttrs = [];
    // expressions on this type of Tag
    var implAttrs = [];
    var tmpl = impl.tmpl;
    var expressions = [];
    var root = conf.root;
    var tagName = conf.tagName || getName(root);
    var isVirtual = tagName === 'virtual';
    var isInline = !isVirtual && !tmpl;
    var dom;

    if (isInline || isLoop && isAnonymous) {
      dom = root;
    } else {
      if (!isVirtual) { root.innerHTML = ''; }
      dom = mkdom(tmpl, innerHTML, isSvg(root));
    }

    // make this tag observable
    if (!skipAnonymous) { observable(tag); }

    // only call unmount if we have a valid __TAG_IMPL (has name property)
    if (impl.name && root._tag) { root._tag.unmount(true); }

    define(tag, '__', {
      impl: impl,
      root: root,
      skipAnonymous: skipAnonymous,
      implAttrs: implAttrs,
      isAnonymous: isAnonymous,
      instAttrs: instAttrs,
      innerHTML: innerHTML,
      tagName: tagName,
      index: index,
      isLoop: isLoop,
      isInline: isInline,
      item: item,
      parent: parent,
      // tags having event listeners
      // it would be better to use weak maps here but we can not introduce breaking changes now
      listeners: [],
      // these vars will be needed only for the virtual tags
      virts: [],
      wasCreated: false,
      tail: null,
      head: null
    });

    // tag protected properties
    return [
      ['isMounted', false],
      // create a unique id to this tag
      // it could be handy to use it also to improve the virtual dom rendering speed
      ['_riot_id', uid()],
      ['root', root],
      ['opts', opts, { writable: true, enumerable: true }],
      ['parent', parent || null],
      // protect the "tags" and "refs" property from being overridden
      ['tags', {}],
      ['refs', {}],
      ['update', function (data) { return componentUpdate(tag, data, expressions); }],
      ['mixin', function () {
        var mixins = [], len = arguments.length;
        while ( len-- ) mixins[ len ] = arguments[ len ];

        return componentMixin.apply(void 0, [ tag ].concat( mixins ));
    }],
      ['mount', function () { return componentMount(tag, dom, expressions, opts); }],
      ['unmount', function (mustKeepRoot) { return tagUnmount(tag, mustKeepRoot, expressions); }]
    ].reduce(function (acc, ref) {
      var key = ref[0];
      var value = ref[1];
      var opts = ref[2];

      define(tag, key, value, opts);
      return acc
    }, extend(tag, item))
  }

  /**
   * Mount a tag creating new Tag instance
   * @param   { Object } root - dom node where the tag will be mounted
   * @param   { String } tagName - name of the riot tag we want to mount
   * @param   { Object } opts - options to pass to the Tag instance
   * @param   { Object } ctx - optional context that will be used to extend an existing class ( used in riot.Tag )
   * @returns { Tag } a new Tag instance
   */
  function mount$1(root, tagName, opts, ctx) {
    var impl = __TAG_IMPL[tagName];
    var implClass = __TAG_IMPL[tagName].class;
    var context = ctx || (implClass ? create(implClass.prototype) : {});
    // cache the inner HTML to fix #855
    var innerHTML = root._innerHTML = root._innerHTML || root.innerHTML;
    var conf = extend({ root: root, opts: opts, context: context }, { parent: opts ? opts.parent : null });
    var tag;

    if (impl && root) { tag = createTag(impl, conf, innerHTML); }

    if (tag && tag.mount) {
      tag.mount(true);
      // add this tag to the virtualDom variable
      if (!contains(__TAGS_CACHE, tag)) { __TAGS_CACHE.push(tag); }
    }

    return tag
  }



  var tags = /*#__PURE__*/Object.freeze({
    arrayishAdd: arrayishAdd,
    getTagName: getName,
    inheritParentProps: inheritParentProps,
    mountTo: mount$1,
    selectTags: query,
    arrayishRemove: arrayishRemove,
    getTag: get,
    initChildTag: initChild,
    moveChildTag: moveChild,
    makeReplaceVirtual: makeReplaceVirtual,
    getImmediateCustomParentTag: getImmediateCustomParent,
    makeVirtual: makeVirtual,
    moveVirtual: moveVirtual,
    unmountAll: unmountAll,
    createIfDirective: createIfDirective,
    createRefDirective: createRefDirective
  });

  /**
   * Riot public api
   */
  var settings$1 = settings;
  var util = {
    tmpl: tmpl,
    brackets: brackets,
    styleManager: styleManager,
    vdom: __TAGS_CACHE,
    styleNode: styleManager.styleNode,
    // export the riot internal utils as well
    dom: dom,
    check: check,
    misc: misc,
    tags: tags
  };

  // export the core props/methods
  var Tag$1 = Tag;
  var tag$1 = tag;
  var tag2$1 = tag2;
  var mount$2 = mount;
  var mixin$1 = mixin;
  var update$2 = update$1;
  var unregister$1 = unregister;
  var version$1 = version;
  var observable$1 = observable;

  var riot$1 = extend({}, core, {
    observable: observable,
    settings: settings$1,
    util: util,
  })

  exports.settings = settings$1;
  exports.util = util;
  exports.Tag = Tag$1;
  exports.tag = tag$1;
  exports.tag2 = tag2$1;
  exports.mount = mount$2;
  exports.mixin = mixin$1;
  exports.update = update$2;
  exports.unregister = unregister$1;
  exports.version = version$1;
  exports.observable = observable$1;
  exports.default = riot$1;

  Object.defineProperty(exports, '__esModule', { value: true });

})));

},{}],6:[function(require,module,exports){
(function (global){
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

global.baseurl = 'https://www.abrarbook.com';
global.baseimgurl = 'https://www.abrarbook.com/';
$.ajaxSetup({
    url: baseurl,
    type: 'POST',
    crossDomain: true,
    dataType: 'json',
    cache: true,
    global: false,
    beforeSend: function beforeSend(xhr, settings) {
        settings.url = baseurl + settings.url;
        if (!settings.noloader) {
            NProgress.start();
        }
    },
    complete: function complete() {
        NProgress.done();
    }
});

$.ajaxsys = function (parm) {
    var u = localStorage.getItem('login');
    u = u ? JSON.parse(u) : {};
    if (u) parm.data.authkey = u.authkey;
    parm.type = "POST";
    parm.cache = false;
    $.ajax(parm);
};

$.validator.setDefaults({
    errorElement: "em",
    errorPlacement: function errorPlacement(error, element) {
        error.addClass("help-block").css({
            'margin': 0
        });
        if (element.prop("type") === "checkbox") {
            error.insertAfter(element.parent("label"));
        } else {
            element.parents("[class^='form-group']:first").find('label').append(error);
        }
    },
    highlight: function highlight(element, errorClass, validClass) {
        $(element).parents("[class^='form-group']:first").addClass("has-error").removeClass("has-success");
    },
    unhighlight: function unhighlight(element, errorClass, validClass) {
        $(element).parents("[class^='form-group']:first").addClass("has-success").removeClass("has-error");
    },
    invalidHandler: function invalidHandler(event, validator) {
        var errors = validator.numberOfInvalids();
        $.toast({
            text: "يوجد " + errors + " حقول بها اخطاء برجاء مرجعتها.",
            icon: 'error'
        });
    }
});

//  $.validator.addMethod("regex",
//      function (value, element, regexp) {
//          var re = new RegExp(regexp);
//          return this.optional(element) || re.test(value);
//      }, "قيمة الحقل غير صحيحة");


//  $.validator.addMethod(
//      "intltelinput",
//      function (value, element, val) {
//          return this.optional(element) || $(element).intlTelInput('isValidNumber');
//      }, "رقم الهاتف غير صحيح");

$.extend(true, $.fn.dataTable.defaults, {

    //  rowReorder: {
    //      selector: ':not(.control)'
    //  },
    // dom: "<'row'<'col-sm-6'f><'col-sm-6'l>>" +
    //     "<'row'<'col-sm-12'tr>>" +
    //     "<'row'<'col-sm-5'p><'col-sm-7'i>>",
    "language": {
        "sEmptyTable": "لا يوجد بيانات",
        "sProcessing": "جارٍ التحميل...",
        "sLengthMenu": "أظهر _MENU_ مدخلات",
        "sZeroRecords": "لم يعثر على أية سجلات",
        "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
        "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
        "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
        "sInfoPostFix": "",
        "sSearch": "",
        "sUrl": "",
        "oPaginate": {
            "sFirst": "الأول",
            "sPrevious": "السابق",
            "sNext": "التالي",
            "sLast": "الأخير"
        }
    },
    searching: false,
    "paging": true,
    "processing": true,
    "serverSide": true,
    "destroy": true,
    "iDisplayStart": 0,
    "iDisplayLength": 100,
    bLengthChange: false,
    "bSort": false,
    "ajax": {
        type: 'POST',
        dataType: 'json',
        beforeSend: function beforeSend(xhr, settings) {
            settings.url = baseurl + settings.url;

            var str = settings.data;
            var prms = str.split("&").reduce(function (prev, curr, i, arr) {
                var p = curr.split("=");
                prev[decodeURIComponent(p[0])] = decodeURIComponent(p[1]);
                return prev;
            }, {});

            var o = prms.obj ? JSON.parse(prms.obj) : {};
            o.length = prms.length;
            o.start = prms.start;
            if (!o.key) o.key = prms["search[value]"];

            var u = localStorage.getItem('login');
            u = u ? JSON.parse(u) : {};

            settings.data = $.param({
                obj: JSON.stringify(o),
                authkey: u.authkey
            });
        },
        dataFilter: function dataFilter(response) {
            var j = JSON.parse(response);
            var ret = {};
            ret.data = j;
            if (j.length == 0) {
                ret.recordsTotal = 0;
                ret.recordsFiltered = 0;
            } else {
                ret.recordsTotal = j[0].totalcount;
                ret.recordsFiltered = j[0].totalcount;
            }
            return JSON.stringify(ret);
        }
    }
});

$.toast.options = {
    showHideTransition: 'slide',
    hideAfter: 5000,
    loader: false,
    allowToastClose: true,
    stack: 3,
    position: 'bottom-left'
};

swal.setDefaults({
    showCancelButton: true,
    confirmButtonColor: "#5cb85c",
    cancelButtonColor: "#d9534f",
    confirmButtonText: "نعم",
    cancelButtonText: "إلغاء",
    allowOutsideClick: false
});

//  Flatpickr.l10ns.default.firstDayOfWeek = 6;
//  Flatpickr.localize(Flatpickr.l10ns.ar);


//moment.updateLocale('"en-us"', {});

global.clsForm = {
    getXmlData: function getXmlData(selector) {
        var j = [{}];
        $(selector).find(".val_xml").each(function (index, ctr) {
            j[0][$(ctr).attr('data-base')] = clsForm.getValue($(ctr));
        });
        var x2js = new X2JS();
        return x2js.json2xml_str({
            Item: j
        });
    },
    getControls: function getControls(elem, attr) {
        if (!attr) {
            attr = 'data-base';
        }
        var formdata = {};
        $(elem).find(".val[" + attr + "]").each(function (index, ctr) {
            var rel = $(ctr).attr(attr).toLowerCase();
            var type = $(ctr).attr("type");
            if ($(ctr).is('select') || type == 'ctr-select') {
                formdata[rel + '_text'] = clsForm.getText($(ctr));
            }
            formdata[rel] = clsForm.getValue($(ctr));
        });
        return formdata;
    },
    getText: function getText($obj) {
        var type = $obj.attr("type");
        if ($obj.is('select')) {
            var text = $obj.find('option:selected');
            if (text.length > 0) {
                if ($obj.attr('multiple') && _typeof(text.text()) == "object") {
                    return text.text().join(',');
                }
                return text.text();
            }
        } else if (type == 'ctr-select') {
            return $obj[0].text();
        }
    },
    getValue: function getValue($obj) {
        var type = $obj.attr("type");
        if (type == "checkbox") {
            return $obj.is(":checked");
        } else if ($obj.data('select2')) {
            if ($obj.attr('multiple')) {
                if ($obj.select2('val')) {
                    return $obj.select2('val').join(',');
                }
            }
            return $obj.select2('val');
        } else if ($obj.is('select')) {
            if ($obj.attr('multiple')) {
                return $obj.val().join(',');
            }
            return $obj.val();
        } else if ($obj.is(':input')) return $obj.val();else if ($obj[0]._tag) return $obj[0]._tag.val();else if ($obj[0].val != undefined) return $obj[0].val();else if (!type) {
            return $obj.html();
        }
    },
    setControl: function setControl(FormSelector, json, attr) {
        if (!attr) {
            attr = 'data-base';
        }
        for (var key in json) {

            var elem = $(FormSelector).find("[" + attr + "='" + key + "']");

            var type = elem.attr("type");
            if (type == "checkbox" || type == "radio") {
                if (json[key] == 1) {
                    elem.attr('checked', 'checked');
                }
            } else if (elem.is("input") || elem.is("textarea")) {
                elem.val(json[key]);
            } else if (elem.is("select")) {
                if (elem.data('select2')) {
                    elem.select2('val', json[key]);
                } else {
                    elem.find('option').each(function (indx, opt) {
                        if (typeof json[key] === 'string' || typeof json[key] === 'integer') {
                            if ($(opt).val() == json[key]) {
                                $(opt).attr('selected', 'selected');
                            }
                        } else {
                            json[key].map(function (v) {
                                if ($(opt).val() == v) {
                                    $(opt).attr('selected', 'selected');
                                }
                            });
                        }
                    });
                }
            } else if (elem.length && elem[0]._tag) elem[0]._tag.val(json[key]);else {
                elem.html(json[key]);
            }
        }
    }
};

global.clsFile = {
    getName: function getName(file) {
        var f = file.replace(/^.*(\\|\/|\:)/, '');
        return f.replace('.' + clsFile.getExtension(file), '');
    },
    getExtension: function getExtension(file) {
        return file.replace(/^.*?\.([a-zA-Z0-9]+)$/, "$1");
    },
    download: function download(path) {
        /* download functio = download_file */
        $('#down_fram').remove();
        $("body").append("<iframe id='down_fram' src='" + clsAjax.api + "?action=download_file&path=" + path + "' style='display: none;'></iframe>");
    }
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],7:[function(require,module,exports){
"use strict";

require("./menu.html");

require("./file-uploader.html");

require("./ddl-lookup.html");

},{"./ddl-lookup.html":8,"./file-uploader.html":9,"./menu.html":10}],8:[function(require,module,exports){
var riot = require('riot');
module.exports = riot.tag2('ddl-lookup', '<select ref="myselect" class="form-control "> <option value="">برجاء الاختيار</option> </select>', '', '', function(opts) {
        var self = this;

        self.on('mount', function () {
            self = this;
            var parentobj = self.parent.opts.page.data;
            if (parentobj && parentobj.length) {
                var key_text = self.opts.dataTxt;
                var key_val = self.opts.dataBase;
                $(self.refs.myselect).append($("<option />").attr('selected', 'selected').text(parentobj[0][
                    key_text
                ]));
            }

            var set = {
                dir: 'rtl',
                language: "ar",
                theme: 'bootstrap4',
                tags: true
            }
            if (self.opts.dataAjaxUrl) {
                set.ajax = {
                    type: 'POST',
                    url: self.opts.dataAjaxUrl,
                    dataType: 'json',
                    data: function (params) {
                        return {
                            obj: JSON.stringify({
                                key: params.term
                            })
                        };
                    },
                    processResults: function (data) {
                        data.map(function (d) {
                            d.text = d[self.opts.dataAjaxText];
                            d.id = d[self.opts.dataAjaxVal];
                        });
                        return {
                            results: data
                        };
                    }
                };
            }

            $(self.refs.myselect).select2(set);

        });
        self.on('update', function () {
            self = this;
        });
        self.valid = function () {
            var isvalid = self.baseurl.length > 0;
            if (isvalid) {
                $(self.refs.holder).addClass('has-success').removeClass('has-error');
            } else {
                $(self.refs.holder).addClass('has-error').removeClass('has-success');
            }
            return isvalid;
        }
        self.val = function (val) {
            if (val) {
                $(self.refs.myselect).val(val).trigger('change');
            }
            return $.trim($(self.refs.myselect).val());
        }
});

},{"riot":5}],9:[function(require,module,exports){
(function (global){
var riot = require('riot');
module.exports = riot.tag2('file-uploader', '<div ref="holder" onclick="{fnOpen}" class="input-group col-xs-12"> <span class="input-group-append"> <button class="file-upload-browse btn btn-info" type="button"> <i class="mdi mdi-cloud-upload"></i> </button> </span> <input ref="mytext" type="text" riot-value="{baseurl}" dir="ltr" class="form-control file-upload-info" disabled="" placeholder="تحميل"> <input ref="myfile" onchange="{fnFileChanged}" type="file" accept="image/*"> </div> <div ref="holder_image" style="display: none;"> <img riot-src="{base64}" class="img-review"> </div>', 'file-uploader [type="file"],[data-is="file-uploader"] [type="file"]{ display: none; } file-uploader .img-review,[data-is="file-uploader"] .img-review{ max-width: 200px; max-height: 200px; }', '', function(opts) {
        var self = this;

        self.base64 = "";
        self.baseurl = "";
        self.on('mount', function () {
            self = this;
        });
        self.on('update', function () {
            self = this;
            if (self.baseurl.length > 0) {
                $(self.refs.holder).popover({
                    trigger: 'hover',
                    html: true,
                    content: function () {
                        return $(self.refs.holder_image).html();
                    }
                })
            }

        });
        self.valid = function () {
            var isvalid = self.baseurl.length > 0;
            if (isvalid) {
                $(self.refs.holder).addClass('has-success').removeClass('has-error');
            } else {
                $(self.refs.holder).addClass('has-error').removeClass('has-success');
            }
            return isvalid;
        }
        self.val = function (val) {
            if (val) {
                self.baseurl = val;
                toDataUrl(global.baseimgurl + val, function (base64) {
                    self.base64 = base64;
                    self.update();
                });
            }
            return $.trim(self.base64);
        }
        self.fnOpen = function () {
            $(self.refs.myfile).click();
        }
        self.fnFileChanged = function (e) {
            var fils = self.refs.myfile.files;
            if (fils.length == 0) {
                return;
            }
            var f = fils[0]
            $(self.refs.mytext).val(f.name);
            getFileContentAsBase64Input(f, function (content) {
                self.base64 = content;
                self.baseurl = f.name;
                self.update();
            });
        }

        function getFileContentAsBase64Input(file, callback) {
            var reader = new FileReader();
            reader.onloadend = function (e) {
                var content = this.result;
                callback(content);
            };
            reader.readAsDataURL(file);
        }

        function toDataUrl(url, callback) {
            var xhr = new XMLHttpRequest();
            xhr.onload = function () {
                var reader = new FileReader();
                reader.onloadend = function () {
                    callback(reader.result);
                }
                reader.readAsDataURL(xhr.response);
            };
            xhr.open('GET', url);
            xhr.responseType = 'blob';
            xhr.send();
        }
});

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"riot":5}],10:[function(require,module,exports){
var riot = require('riot');
module.exports = riot.tag2('menu', '<ul class="nav page-navigation"> <li class="nav-item"> <a href="#/app/" class="nav-link"> <i class="link-icon mdi mdi-television"></i> <span class="menu-title">الرئيسية</span> </a> </li> <li class="nav-item"> <a href="#/app/partner/list" class="nav-link"> <i class="link-icon mdi mdi-asterisk"></i> <span class="menu-title">الشركاء</span> <i class="menu-arrow"></i> </a> <div class="submenu"> <ul class="submenu-item"> <li class="nav-item"> <a class="nav-link" href="#/app/partner/list">قائمة الشركاء</a> </li> <li class="nav-item"> <a class="nav-link" href="#/app/gift/list">الهدايا</a> </li> </ul> </div> </li> <li class="nav-item"> <a href="#/app/consultation/list" class="nav-link"> <i class="link-icon mdi mdi-asterisk"></i> <span class="menu-title">التطبيق</span> <i class="menu-arrow"></i> </a> <div class="submenu"> <ul class="submenu-item"> <li class="nav-item"> <a class="nav-link" href="#/app/consultation/list">الاستشارات</a> </li> </ul> </div> </li> </ul>', '', '', function(opts) {
});

},{"riot":5}],11:[function(require,module,exports){
(function (global){
"use strict";

require("./common/common-index");

require("./components/components-index");

require("./views/views-index");

global.riot = require("riot");
require("riot-hot-reload");
//riot.reload('my-component')
require('riot-routehandler');

var routes = require("./router").default;

//import "./common/routehandler";


riot.router = riot.mount("routehandler", {
    routes: routes,
    options: {
        hashbang: true,
        base: "/#"
    }
});

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./common/common-index":6,"./components/components-index":7,"./router":12,"./views/views-index":23,"riot":5,"riot-hot-reload":3,"riot-routehandler":4}],12:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var auth = function auth(ctx, next, page) {
    var logindata = localStorage.getItem('login');
    if (logindata) return next();

    page.redirect('/login');
};
var routes = [{
    route: "/",
    tag: "login"
}, {
    route: "/login",
    tag: "login"
}, {
    route: "/logout",
    tag: "logout"
}, {
    route: "/app/*",
    use: auth
}, {
    route: "/app/",
    tag: "app",
    routes: [{
        route: "/",
        tag: "home"
    }, {
        route: "/partner/list",
        tag: "partner-list"
    }, {
        route: "/partner/item/:id",
        tag: "partner-item",
        use: function use(ctx, next, page) {
            $.ajaxsys({
                url: "/api/partner/get/",
                data: {
                    obj: JSON.stringify({
                        id: ctx.params.id
                    })
                },
                success: function success(data) {
                    page.data = data;
                    next();
                }
            });
        }
    }, {
        route: "/gift/list",
        tag: "gift-list"
    }, {
        route: "/gift/item/:id",
        tag: "gift-item",
        use: function use(ctx, next, page) {
            $.ajaxsys({
                url: "/api/gift/get/",
                data: {
                    obj: JSON.stringify({
                        id: ctx.params.id
                    })
                },
                success: function success(data) {
                    page.data = data;
                    next();
                }
            });
        }
    }, {
        route: "/consultation/list",
        tag: "consultation-list"
    }, {
        route: "/consultation/item/:id",
        tag: "consultation-item",
        use: function use(ctx, next, page) {

            $.ajaxsys({
                url: "/api/consultations/get/",
                data: {
                    obj: JSON.stringify({
                        id: ctx.params.id
                    })
                },
                success: function success(data) {
                    page.data = data;
                    next();
                }
            });
        }
    }]
}];

exports.default = routes;

//https://www.npmjs.com/package/riot-routehandler

},{}],13:[function(require,module,exports){
var riot = require('riot');
module.exports = riot.tag2('app', '<div class="container-scroller"> <nav class="navbar horizontal-layout col-lg-12 col-12 p-0"> <div class="container d-flex flex-row"> <div class="text-center navbar-brand-wrapper d-flex align-items-top"> <a class="navbar-brand brand-logo" href="#/app/"> <img src="images/logo.png" alt="logo"> </a> <a class="navbar-brand brand-logo-mini" href="#/app/"> <img src="images/logo.png" alt="logo"> </a> </div> <div class="navbar-menu-wrapper d-flex align-items-center"> <form class=" ml-auto" action="#"></form> <ul class="navbar-nav navbar-nav-right mr-0"> <li class="nav-item dropdown d-none d-xl-inline-block"> <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false"> <img class="img-xs rounded-circle" src="images/user.png" alt="Profile image"> </a> <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown"> <a href="#/logout" class="dropdown-item"> Sign Out </a> </div> </li> </ul> <button class="navbar-toggler align-self-center" type="button" data-toggle="minimize"> <span class="mdi mdi-menu"></span> </button> </div> </div> <div class="nav-bottom"> <div class="container"> <menu></menu> </div> </div> </nav> <div class="container-fluid page-body-wrapper"> <div class="main-panel"> <div class="content-wrapper"> <routehandler></routehandler> </div> </div> </div> </div>', '', '', function(opts) {
    var self = this;
    self.on('mount', function () {

    });
});

},{"riot":5}],14:[function(require,module,exports){
var riot = require('riot');
module.exports = riot.tag2('consultation-item', '<div class="row"> <div class="col-12 grid-margin"> <div class="card"> <div class="card-body"> <h4 class="card-title">بيانات الاستشارة</h4> <form ref="myform" onsubmit="{fnsave}" class="form-sample"> <div class="row"> <div class="col-md-6"> <div class="form-group row"> <label class="col-sm-4 col-form-label">تاريخ السؤال</label> <div class="col-sm-8"> <input type="text" data-base="datecreate_f" class="form-control val" readonly> </div> </div> </div> <div class="col-md-6"> <div class="form-group row"> <label class="col-sm-4 col-form-label">اسم السائل</label> <div class="col-sm-8"> <input type="text" data-base="person_name" class="form-control" readonly> </div> </div> </div> <div class="col-md-12"> <div class="form-group row"> <label class="col-sm-2 col-form-label"> السؤال</label> <div class="col-sm-10"> <textarea data-base="question" rows="3" class="form-control" readonly></textarea> </div> </div> </div> <div class="col-md-12"> <div class="form-group row"> <label class="col-sm-2 col-form-label"> الإجابة</label> <div class="col-sm-10"> <textarea data-base="answer" rows="3" class="form-control val" required></textarea> </div> </div> </div> <div class="col-md-12"> <div class="form-group row"> <label class="col-sm-2 col-form-label"> </label> <div class="col-sm-10 "> <button class="btn btn-primary">حفظ</button> </div> </div> </div> </div> </form> </div> </div> </div> </div>', '', '', function(opts) {
        var self = this;

        self.on('mount', function () {
            self = this;
            $(self.refs.myform).validate();
            if (self.opts.params.id > -1 && self.opts.page.data) {
                clsForm.setControl(self.refs.myform, self.opts.page.data[0]);
            }
        });
        self.fnsave = function (e) {
            e.preventDefault();
            if (!$(self.refs.myform).valid()) {
                return;
            }
            var j = clsForm.getControls(self.refs.myform);
            j.id = self.opts.params.id;
            $.ajaxsys({
                url: "/api/consultations/set/",
                data: {
                    obj: JSON.stringify(j)
                },
                success: function (data) {
                    $.toast({
                        text: "تم الحفظ",
                        icon: 'success'
                    });
                    self.opts.page('/app/consultation/list');
                }
            });
        }
});

},{"riot":5}],15:[function(require,module,exports){
var riot = require('riot');
module.exports = riot.tag2('consultation-list', '<div class="card"> <div class="card-body"> <h4 class="card-title">قائمة الإستشارات</h4> <div class="row"> <div class="col-12 "> <table ref="mytable" class="table table-hover table-bordered"></table> </div> </div> </div> </div>', '', '', function(opts) {
        var self = this;
        self.on('mount', function () {
            $(self.refs.mytable).DataTable({
                "ajax": {
                    url: '/api/consultations/get/',
                    data: {
                        obj: JSON.stringify({
                            aaa: 11
                        })
                    }
                },
                "columns": [{
                    title: "#",
                    orderable: false,
                    render: function (data, type, full, meta) {
                        return Handlebars.compile(
                            `
<a href='#/app/consultation/item/{{id}}'>#{{id}}</a>`)(
                            full);
                    }
                }, {
                    title: "الاسم",
                    orderable: false,
                    render: function (data, type, full, meta) {
                        return Handlebars.compile(
                            `
<a href='#/app/consultation/item/{{id}}'>{{person_name}}</a>`
                        )(
                            full);
                    }
                }, {
                    title: "السؤال",
                    orderable: false,
                    data: 'question'

                }]
            })
        })
});

},{"riot":5}],16:[function(require,module,exports){
var riot = require('riot');
module.exports = riot.tag2('gift-item', '<div class="row"> <div class="col-12 grid-margin"> <div class="card"> <div class="card-body"> <h4 class="card-title">بيانات الهدية</h4> <form ref="myform" onsubmit="{fnsave}" class="form-sample"> <div class="row"> <div class="col-md-6"> <div class="form-group row"> <label class="col-sm-4 col-form-label">عنوان الهدية</label> <div class="col-sm-8"> <input type="text" data-base="title" class="form-control val" required> </div> </div> <div class="form-group row"> <label class="col-sm-4 col-form-label">الصورة</label> <div class="col-sm-8"> <file-uploader ref="img" data-base="img"></file-uploader> </div> </div> </div> <div class="col-md-6"> <div class="form-group row"> <label class="col-sm-4 col-form-label">العدد</label> <div class="col-sm-8"> <input type="tel" data-base="giftcount" class="form-control val" required> </div> </div> <div class="form-group row"> <label class="col-sm-4 col-form-label">الشريك</label> <div class="col-sm-8"> <ddl-lookup data-base="partner_id" data-txt="partner_name" data-ajax-url="/api/partner/get/" data-ajax-val="id" data-ajax-text="name" class="val"></ddl-lookup> </div> </div> </div> <div class="col-md-12"> <div class="form-group row"> <label class="col-sm-2 col-form-label"> وصف الهدية </label> <div class="col-sm-10"> <textarea data-base="description" rows="3" class="form-control val" required></textarea> </div> </div> </div> <div class="col-md-12"> <div class="form-group row"> <label class="col-sm-2 col-form-label"> </label> <div class="col-sm-10 "> <button class="btn btn-primary">حفظ</button> </div> </div> </div> </div> </form> </div> </div> </div> </div>', '', '', function(opts) {
        var self = this;

        self.on('mount', function () {
            self = this;
            $(self.refs.myform).validate();
            if (self.opts.params.id > -1 && self.opts.page.data) {
                clsForm.setControl(self.refs.myform, self.opts.page.data[0]);
            }
        });
        self.fnsave = function (e) {
            e.preventDefault();
            if (!$(self.refs.myform).valid() || !self.refs.img.valid()) {
                return;
            }
            var j = clsForm.getControls(self.refs.myform);
            j.id = self.opts.params.id;
            $.ajaxsys({
                url: "/api/gift/set/",
                data: {
                    obj: JSON.stringify(j),
                    "base64_img": self.refs.img.val()
                },
                success: function (data) {
                    $.toast({
                        text: "تم الحفظ",
                        icon: 'success'
                    });
                    self.opts.page('/app/gift/list');
                }
            });
        }
});

},{"riot":5}],17:[function(require,module,exports){
var riot = require('riot');
module.exports = riot.tag2('gift-list', '<div class="card"> <div class="card-body"> <h4 class="card-title"> <span>قائمة الهدايا</span> <a href="#/app/gift/item/-1" class="btn btn-info btn-xs float-left"> جديد</a> </h4> <div class="row"> <div class="col-12 "> <table ref="mytable" class="table table-hover table-bordered"></table> </div> </div> </div> </div>', '', '', function(opts) {
        var self = this;
        self.on('mount', function () {
            $(self.refs.mytable).DataTable({
                "ajax": {
                    url: '/api/gift/get/',
                    data: {
                        obj: JSON.stringify({
                            aaa: 11
                        })
                    }
                },
                "columns": [{
                    title: "الاسم",
                    orderable: false,
                    render: function (data, type, full, meta) {
                        return Handlebars.compile(
                            `
<a href='#/app/gift/item/{{id}}'>{{title}}</a>`
                        )(
                            full);
                    }
                }, {
                    title: "عدد الهدايا",
                    orderable: false,
                    data: 'giftcount'
                }, {
                    title: "الشريك",
                    orderable: false,
                    render: function (data, type, full, meta) {
                        return Handlebars.compile(
                            `
<a href='#/app/partner/item/{{partner_id}}'>{{partner_name}}</a>`
                        )(
                            full);
                    }
                }]
            })
        })
});

},{"riot":5}],18:[function(require,module,exports){
var riot = require('riot');
module.exports = riot.tag2('home', '', '', '', function(opts) {
});

},{"riot":5}],19:[function(require,module,exports){
var riot = require('riot');
module.exports = riot.tag2('login', '<div class="container-scroller"> <div class="container-fluid page-body-wrapper full-page-wrapper"> <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one"> <div class="row w-100 mx-auto"> <div class="col-lg-4 mx-auto"> <div class="auto-form-wrapper"> <form ref="myform" onsubmit="{login}"> <div class="form-group"> <label class="label">البريد او رقم الهاتف</label> <input type="text" data-base="username" required dir="ltr" class="form-control val" placeholder="البريد او رقم الهاتف"> </div> <div class="form-group"> <label class="label">كلمة المرور</label> <input type="password" data-base="password" required dir="ltr" class="form-control val" placeholder="*********"> </div> <div class="form-group"> <button class="btn btn-primary submit-btn btn-block">تسجيل الدخول</button> </div> </form> </div> </div> </div> </div> </div> </div>', 'login .auth.auth-bg-1,[data-is="login"] .auth.auth-bg-1{ background-image: url("images/BlueBackground.jpg"); }', '', function(opts) {
        var self = this;
        self.on('mount', function () {
            $(self.refs.myform).validate();
        });

        self.login = function (e) {
            e.preventDefault();
            if (!$(self.refs.myform).valid()) {
                return;
            }
            var j = clsForm.getControls(self.refs.myform)
            j.rule_id = 2;
            $.ajaxsys({
                url: "/api/person/login/",
                data: {
                    obj: JSON.stringify(j)
                },
                success: function (data) {

                    if (data.length == 0) {
                        $.toast({
                            text: "يوجد خطأ في البيانات",
                            icon: 'error'
                        })
                        return false;
                    }

                    if (data[0]["error_ar"]) {
                        $.toast({
                            text: data[0]["error_ar"],
                            icon: 'error'
                        });
                        return false;
                    }

                    localStorage.setItem('login', JSON.stringify(data[0]));
                    self.opts.page('/app/');
                }
            });

        }
});

},{"riot":5}],20:[function(require,module,exports){
var riot = require('riot');
module.exports = riot.tag2('logout', '', '', '', function(opts) {
        var self = this;
        self.on('mount', function () {
            localStorage.removeItem('login');
            self.opts.page('/login/');
        });
});

},{"riot":5}],21:[function(require,module,exports){
var riot = require('riot');
module.exports = riot.tag2('partner-item', '<div class="row"> <div class="col-12 grid-margin"> <div class="card"> <div class="card-body"> <h4 class="card-title">بيانات الشريك</h4> <form ref="myform" onsubmit="{fnsave}" class="form-sample"> <div class="row"> <div class="col-md-6"> <div class="form-group row"> <label class="col-sm-4 col-form-label">الاسم</label> <div class="col-sm-8"> <input type="text" data-base="name" class="form-control val" required> </div> </div> <div class="form-group row"> <label class="col-sm-4 col-form-label">الصورة</label> <div class="col-sm-8"> <file-uploader ref="img" data-base="img"></file-uploader> </div> </div> </div> <div class="col-md-6"> <div class="form-group row"> <label class="col-sm-4 col-form-label">رقم الجوال</label> <div class="col-sm-8"> <input type="tel" data-base="phone" class="form-control val" required> </div> </div> </div> <div class="col-md-12"> <div class="form-group row"> <label class="col-sm-2 col-form-label"> العنوان</label> <div class="col-sm-10"> <textarea data-base="address" rows="3" class="form-control val" required></textarea> </div> </div> </div> <div class="col-md-12"> <div class="form-group row"> <label class="col-sm-2 col-form-label"> نبذة عن الشريك</label> <div class="col-sm-10"> <textarea data-base="content" rows="3" class="form-control val" required></textarea> </div> </div> </div> <div class="col-md-12"> <div class="form-group row"> <label class="col-sm-2 col-form-label"> </label> <div class="col-sm-10 "> <button class="btn btn-primary">حفظ</button> </div> </div> </div> </div> </form> </div> </div> </div> </div>', '', '', function(opts) {
        var self = this;

        self.on('mount', function () {
            self = this;
            $(self.refs.myform).validate();
            if (self.opts.params.id > -1 && self.opts.page.data) {
                clsForm.setControl(self.refs.myform, self.opts.page.data[0]);
            }
        });
        self.fnsave = function (e) {
            e.preventDefault();
            if (!$(self.refs.myform).valid() || !self.refs.img.valid()) {
                return;
            }
            var j = clsForm.getControls(self.refs.myform);
            j.id = self.opts.params.id;
            $.ajaxsys({
                url: "/api/partner/set/",
                data: {
                    obj: JSON.stringify(j),
                    "base64_img": self.refs.img.val()
                },
                success: function (data) {
                    $.toast({
                        text: "تم الحفظ",
                        icon: 'success'
                    });
                    self.opts.page('/app/partner/list');
                }
            });
        }
});

},{"riot":5}],22:[function(require,module,exports){
var riot = require('riot');
module.exports = riot.tag2('partner-list', '<div class="card"> <div class="card-body"> <h4 class="card-title"> <span>قائمة الشركاء</span> <a href="#/app/partner/item/-1" class="btn btn-info btn-xs float-left"> جديد</a> </h4> <div class="row"> <div class="col-12 "> <table ref="mytable" class="table table-hover table-bordered"></table> </div> </div> </div> </div>', '', '', function(opts) {
        var self = this;
        self.on('mount', function () {
            $(self.refs.mytable).DataTable({
                "ajax": {
                    url: '/api/partner/get/',
                    data: {
                        obj: JSON.stringify({
                            aaa: 11
                        })
                    }
                },
                "columns": [{
                    title: "الاسم",
                    orderable: false,
                    render: function (data, type, full, meta) {
                        return Handlebars.compile(
                            `
<a href='#/app/partner/item/{{id}}'>{{name}}</a>`
                        )(
                            full);
                    }
                }, {
                    title: "عدد الهدايا",
                    orderable: false,
                    data: 'giftcount'
                }]
            })
        })
});

},{"riot":5}],23:[function(require,module,exports){
"use strict";

require("./app.html");

require("./home.html");

require("./login.html");

require("./logout.html");

require("./consultation-item.html");

require("./consultation-list.html");

require("./gift-item.html");

require("./gift-list.html");

require("./partner-item.html");

require("./partner-list.html");

},{"./app.html":13,"./consultation-item.html":14,"./consultation-list.html":15,"./gift-item.html":16,"./gift-list.html":17,"./home.html":18,"./login.html":19,"./logout.html":20,"./partner-item.html":21,"./partner-list.html":22}]},{},[11])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJub2RlX21vZHVsZXMvcGFnZS9wYWdlLmpzIiwibm9kZV9tb2R1bGVzL3Byb2Nlc3MvYnJvd3Nlci5qcyIsIm5vZGVfbW9kdWxlcy9yaW90LWhvdC1yZWxvYWQvaW5kZXguanMiLCJub2RlX21vZHVsZXMvcmlvdC1yb3V0ZWhhbmRsZXIvbGliL3JvdXRlaGFuZGxlci5qcyIsIm5vZGVfbW9kdWxlcy9yaW90L3Jpb3QuanMiLCJzcmMvY29tbW9uL2NvbW1vbi1pbmRleC5qcyIsInNyYy9jb21wb25lbnRzL2NvbXBvbmVudHMtaW5kZXguanMiLCJEOi9fUHJvamVjdHMvYWJyYXJib29rL2FkbWluLmFicmFyYm9vay5jb20vc3JjL2NvbXBvbmVudHMvZGRsLWxvb2t1cC5odG1sIiwiRDovX1Byb2plY3RzL2FicmFyYm9vay9hZG1pbi5hYnJhcmJvb2suY29tL3NyYy9jb21wb25lbnRzL2ZpbGUtdXBsb2FkZXIuaHRtbCIsIkQ6L19Qcm9qZWN0cy9hYnJhcmJvb2svYWRtaW4uYWJyYXJib29rLmNvbS9zcmMvY29tcG9uZW50cy9tZW51Lmh0bWwiLCJzcmMvbWFpbi5qcyIsInNyYy9yb3V0ZXIuanMiLCJEOi9fUHJvamVjdHMvYWJyYXJib29rL2FkbWluLmFicmFyYm9vay5jb20vc3JjL3ZpZXdzL2FwcC5odG1sIiwiRDovX1Byb2plY3RzL2FicmFyYm9vay9hZG1pbi5hYnJhcmJvb2suY29tL3NyYy92aWV3cy9jb25zdWx0YXRpb24taXRlbS5odG1sIiwiRDovX1Byb2plY3RzL2FicmFyYm9vay9hZG1pbi5hYnJhcmJvb2suY29tL3NyYy92aWV3cy9jb25zdWx0YXRpb24tbGlzdC5odG1sIiwiRDovX1Byb2plY3RzL2FicmFyYm9vay9hZG1pbi5hYnJhcmJvb2suY29tL3NyYy92aWV3cy9naWZ0LWl0ZW0uaHRtbCIsIkQ6L19Qcm9qZWN0cy9hYnJhcmJvb2svYWRtaW4uYWJyYXJib29rLmNvbS9zcmMvdmlld3MvZ2lmdC1saXN0Lmh0bWwiLCJEOi9fUHJvamVjdHMvYWJyYXJib29rL2FkbWluLmFicmFyYm9vay5jb20vc3JjL3ZpZXdzL2hvbWUuaHRtbCIsIkQ6L19Qcm9qZWN0cy9hYnJhcmJvb2svYWRtaW4uYWJyYXJib29rLmNvbS9zcmMvdmlld3MvbG9naW4uaHRtbCIsIkQ6L19Qcm9qZWN0cy9hYnJhcmJvb2svYWRtaW4uYWJyYXJib29rLmNvbS9zcmMvdmlld3MvbG9nb3V0Lmh0bWwiLCJEOi9fUHJvamVjdHMvYWJyYXJib29rL2FkbWluLmFicmFyYm9vay5jb20vc3JjL3ZpZXdzL3BhcnRuZXItaXRlbS5odG1sIiwiRDovX1Byb2plY3RzL2FicmFyYm9vay9hZG1pbi5hYnJhcmJvb2suY29tL3NyYy92aWV3cy9wYXJ0bmVyLWxpc3QuaHRtbCIsInNyYy92aWV3cy92aWV3cy1pbmRleC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQ3ZuQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN4TEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDNURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNoSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUMvZ0dBLE9BQU8sT0FBUCxHQUFpQiwyQkFBakI7QUFDQSxPQUFPLFVBQVAsR0FBb0IsNEJBQXBCO0FBQ0EsRUFBRSxTQUFGLENBQVk7QUFDUixTQUFLLE9BREc7QUFFUixVQUFNLE1BRkU7QUFHUixpQkFBYSxJQUhMO0FBSVIsY0FBVSxNQUpGO0FBS1IsV0FBTyxJQUxDO0FBTVIsWUFBUSxLQU5BO0FBT1IsZ0JBQVksb0JBQVUsR0FBVixFQUFlLFFBQWYsRUFBeUI7QUFDakMsaUJBQVMsR0FBVCxHQUFlLFVBQVUsU0FBUyxHQUFsQztBQUNBLFlBQUksQ0FBQyxTQUFTLFFBQWQsRUFBd0I7QUFDcEIsc0JBQVUsS0FBVjtBQUNIO0FBQ0osS0FaTztBQWFSLGNBQVUsb0JBQVk7QUFDbEIsa0JBQVUsSUFBVjtBQUNIO0FBZk8sQ0FBWjs7QUFrQkEsRUFBRSxPQUFGLEdBQVksVUFBVSxJQUFWLEVBQWdCO0FBQ3hCLFFBQUksSUFBSSxhQUFhLE9BQWIsQ0FBcUIsT0FBckIsQ0FBUjtBQUNBLFFBQUksSUFBSSxLQUFLLEtBQUwsQ0FBVyxDQUFYLENBQUosR0FBb0IsRUFBeEI7QUFDQSxRQUFJLENBQUosRUFBTyxLQUFLLElBQUwsQ0FBVSxPQUFWLEdBQW9CLEVBQUUsT0FBdEI7QUFDUCxTQUFLLElBQUwsR0FBWSxNQUFaO0FBQ0EsU0FBSyxLQUFMLEdBQWEsS0FBYjtBQUNBLE1BQUUsSUFBRixDQUFPLElBQVA7QUFDSCxDQVBEOztBQVNBLEVBQUUsU0FBRixDQUFZLFdBQVosQ0FBd0I7QUFDcEIsa0JBQWMsSUFETTtBQUVwQixvQkFBZ0Isd0JBQVUsS0FBVixFQUFpQixPQUFqQixFQUEwQjtBQUN0QyxjQUFNLFFBQU4sQ0FBZSxZQUFmLEVBQTZCLEdBQTdCLENBQWlDO0FBQzdCLHNCQUFVO0FBRG1CLFNBQWpDO0FBR0EsWUFBSSxRQUFRLElBQVIsQ0FBYSxNQUFiLE1BQXlCLFVBQTdCLEVBQXlDO0FBQ3JDLGtCQUFNLFdBQU4sQ0FBa0IsUUFBUSxNQUFSLENBQWUsT0FBZixDQUFsQjtBQUNILFNBRkQsTUFFTztBQUNILG9CQUFRLE9BQVIsQ0FBZ0IsNkJBQWhCLEVBQStDLElBQS9DLENBQW9ELE9BQXBELEVBQTZELE1BQTdELENBQW9FLEtBQXBFO0FBRUg7QUFDSixLQVptQjtBQWFwQixlQUFXLG1CQUFVLE9BQVYsRUFBbUIsVUFBbkIsRUFBK0IsVUFBL0IsRUFBMkM7QUFDbEQsVUFBRSxPQUFGLEVBQVcsT0FBWCxDQUFtQiw2QkFBbkIsRUFBa0QsUUFBbEQsQ0FBMkQsV0FBM0QsRUFBd0UsV0FBeEUsQ0FBb0YsYUFBcEY7QUFDSCxLQWZtQjtBQWdCcEIsaUJBQWEscUJBQVUsT0FBVixFQUFtQixVQUFuQixFQUErQixVQUEvQixFQUEyQztBQUNwRCxVQUFFLE9BQUYsRUFBVyxPQUFYLENBQW1CLDZCQUFuQixFQUFrRCxRQUFsRCxDQUEyRCxhQUEzRCxFQUEwRSxXQUExRSxDQUFzRixXQUF0RjtBQUNILEtBbEJtQjtBQW1CcEIsb0JBQWdCLHdCQUFVLEtBQVYsRUFBaUIsU0FBakIsRUFBNEI7QUFDeEMsWUFBSSxTQUFTLFVBQVUsZ0JBQVYsRUFBYjtBQUNBLFVBQUUsS0FBRixDQUFRO0FBQ0osa0JBQU0sVUFBVSxNQUFWLEdBQW1CLGdDQURyQjtBQUVKLGtCQUFNO0FBRkYsU0FBUjtBQUlIO0FBekJtQixDQUF4Qjs7QUE0QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxFQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBRSxFQUFGLENBQUssU0FBTCxDQUFlLFFBQTlCLEVBQXdDOztBQUVwQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQkFBWTtBQUNSLHVCQUFlLGdCQURQO0FBRVIsdUJBQWUsaUJBRlA7QUFHUix1QkFBZSxvQkFIUDtBQUlSLHdCQUFnQix1QkFKUjtBQUtSLGlCQUFTLDZDQUxEO0FBTVIsc0JBQWMsMkJBTk47QUFPUix5QkFBaUIsK0JBUFQ7QUFRUix3QkFBZ0IsRUFSUjtBQVNSLG1CQUFXLEVBVEg7QUFVUixnQkFBUSxFQVZBO0FBV1IscUJBQWE7QUFDVCxzQkFBVSxPQUREO0FBRVQseUJBQWEsUUFGSjtBQUdULHFCQUFTLFFBSEE7QUFJVCxxQkFBUztBQUpBO0FBWEwsS0FSd0I7QUEwQnBDLGVBQVcsS0ExQnlCO0FBMkJwQyxjQUFVLElBM0IwQjtBQTRCcEMsa0JBQWMsSUE1QnNCO0FBNkJwQyxrQkFBYyxJQTdCc0I7QUE4QnBDLGVBQVcsSUE5QnlCO0FBK0JwQyxxQkFBaUIsQ0EvQm1CO0FBZ0NwQyxzQkFBa0IsR0FoQ2tCO0FBaUNwQyxtQkFBZSxLQWpDcUI7QUFrQ3BDLGFBQVMsS0FsQzJCO0FBbUNwQyxZQUFRO0FBQ0osY0FBTSxNQURGO0FBRUosa0JBQVUsTUFGTjtBQUdKLG9CQUFZLG9CQUFVLEdBQVYsRUFBZSxRQUFmLEVBQXlCO0FBQ2pDLHFCQUFTLEdBQVQsR0FBZSxVQUFVLFNBQVMsR0FBbEM7O0FBRUEsZ0JBQUksTUFBTSxTQUFTLElBQW5CO0FBQ0EsZ0JBQUksT0FBTyxJQUFJLEtBQUosQ0FBVSxHQUFWLEVBQWUsTUFBZixDQUFzQixVQUFVLElBQVYsRUFBZ0IsSUFBaEIsRUFBc0IsQ0FBdEIsRUFBeUIsR0FBekIsRUFBOEI7QUFDM0Qsb0JBQUksSUFBSSxLQUFLLEtBQUwsQ0FBVyxHQUFYLENBQVI7QUFDQSxxQkFBSyxtQkFBbUIsRUFBRSxDQUFGLENBQW5CLENBQUwsSUFBaUMsbUJBQW1CLEVBQUUsQ0FBRixDQUFuQixDQUFqQztBQUNBLHVCQUFPLElBQVA7QUFDSCxhQUpVLEVBSVIsRUFKUSxDQUFYOztBQU1BLGdCQUFJLElBQUksS0FBSyxHQUFMLEdBQVcsS0FBSyxLQUFMLENBQVcsS0FBSyxHQUFoQixDQUFYLEdBQWtDLEVBQTFDO0FBQ0EsY0FBRSxNQUFGLEdBQVcsS0FBSyxNQUFoQjtBQUNBLGNBQUUsS0FBRixHQUFVLEtBQUssS0FBZjtBQUNBLGdCQUFJLENBQUMsRUFBRSxHQUFQLEVBQVksRUFBRSxHQUFGLEdBQVEsS0FBSyxlQUFMLENBQVI7O0FBRVosZ0JBQUksSUFBSSxhQUFhLE9BQWIsQ0FBcUIsT0FBckIsQ0FBUjtBQUNBLGdCQUFJLElBQUksS0FBSyxLQUFMLENBQVcsQ0FBWCxDQUFKLEdBQW9CLEVBQXhCOztBQUVBLHFCQUFTLElBQVQsR0FBZ0IsRUFBRSxLQUFGLENBQVE7QUFDcEIscUJBQUssS0FBSyxTQUFMLENBQWUsQ0FBZixDQURlO0FBRXBCLHlCQUFTLEVBQUU7QUFGUyxhQUFSLENBQWhCO0FBS0gsU0ExQkc7QUEyQkosb0JBQVksb0JBQVUsUUFBVixFQUFvQjtBQUM1QixnQkFBSSxJQUFJLEtBQUssS0FBTCxDQUFXLFFBQVgsQ0FBUjtBQUNBLGdCQUFJLE1BQU0sRUFBVjtBQUNBLGdCQUFJLElBQUosR0FBVyxDQUFYO0FBQ0EsZ0JBQUksRUFBRSxNQUFGLElBQVksQ0FBaEIsRUFBbUI7QUFDZixvQkFBSSxZQUFKLEdBQW1CLENBQW5CO0FBQ0Esb0JBQUksZUFBSixHQUFzQixDQUF0QjtBQUNILGFBSEQsTUFHTztBQUNILG9CQUFJLFlBQUosR0FBbUIsRUFBRSxDQUFGLEVBQUssVUFBeEI7QUFDQSxvQkFBSSxlQUFKLEdBQXNCLEVBQUUsQ0FBRixFQUFLLFVBQTNCO0FBQ0g7QUFDRCxtQkFBTyxLQUFLLFNBQUwsQ0FBZSxHQUFmLENBQVA7QUFDSDtBQXZDRztBQW5DNEIsQ0FBeEM7O0FBOEVBLEVBQUUsS0FBRixDQUFRLE9BQVIsR0FBa0I7QUFDZCx3QkFBb0IsT0FETjtBQUVkLGVBQVcsSUFGRztBQUdkLFlBQVEsS0FITTtBQUlkLHFCQUFpQixJQUpIO0FBS2QsV0FBTyxDQUxPO0FBTWQsY0FBVTtBQU5JLENBQWxCOztBQVNBLEtBQUssV0FBTCxDQUFpQjtBQUNiLHNCQUFrQixJQURMO0FBRWIsd0JBQW9CLFNBRlA7QUFHYix1QkFBbUIsU0FITjtBQUliLHVCQUFtQixLQUpOO0FBS2Isc0JBQWtCLE9BTEw7QUFNYix1QkFBbUI7QUFOTixDQUFqQjs7QUFTQTtBQUNBOzs7QUFNQTs7QUFFQSxPQUFPLE9BQVAsR0FBaUI7QUFDYixnQkFBWSxvQkFBVSxRQUFWLEVBQW9CO0FBQzVCLFlBQUksSUFBSSxDQUFDLEVBQUQsQ0FBUjtBQUNBLFVBQUUsUUFBRixFQUFZLElBQVosQ0FBaUIsVUFBakIsRUFBNkIsSUFBN0IsQ0FBa0MsVUFBVSxLQUFWLEVBQWlCLEdBQWpCLEVBQXNCO0FBQ3BELGNBQUUsQ0FBRixFQUFLLEVBQUUsR0FBRixFQUFPLElBQVAsQ0FBWSxXQUFaLENBQUwsSUFBaUMsUUFBUSxRQUFSLENBQWlCLEVBQUUsR0FBRixDQUFqQixDQUFqQztBQUNILFNBRkQ7QUFHQSxZQUFJLE9BQU8sSUFBSSxJQUFKLEVBQVg7QUFDQSxlQUFPLEtBQUssWUFBTCxDQUFrQjtBQUNyQixrQkFBTTtBQURlLFNBQWxCLENBQVA7QUFHSCxLQVZZO0FBV2IsaUJBQWEscUJBQVUsSUFBVixFQUFnQixJQUFoQixFQUFzQjtBQUMvQixZQUFJLENBQUMsSUFBTCxFQUFXO0FBQ1AsbUJBQU8sV0FBUDtBQUNIO0FBQ0QsWUFBSSxXQUFXLEVBQWY7QUFDQSxVQUFFLElBQUYsRUFBUSxJQUFSLENBQWEsVUFBVSxJQUFWLEdBQWlCLEdBQTlCLEVBQW1DLElBQW5DLENBQXdDLFVBQVUsS0FBVixFQUFpQixHQUFqQixFQUFzQjtBQUMxRCxnQkFBSSxNQUFNLEVBQUUsR0FBRixFQUFPLElBQVAsQ0FBWSxJQUFaLEVBQWtCLFdBQWxCLEVBQVY7QUFDQSxnQkFBSSxPQUFPLEVBQUUsR0FBRixFQUFPLElBQVAsQ0FBWSxNQUFaLENBQVg7QUFDQSxnQkFBSSxFQUFFLEdBQUYsRUFBTyxFQUFQLENBQVUsUUFBVixLQUF1QixRQUFRLFlBQW5DLEVBQWlEO0FBQzdDLHlCQUFTLE1BQU0sT0FBZixJQUEwQixRQUFRLE9BQVIsQ0FBZ0IsRUFBRSxHQUFGLENBQWhCLENBQTFCO0FBQ0g7QUFDRCxxQkFBUyxHQUFULElBQWdCLFFBQVEsUUFBUixDQUFpQixFQUFFLEdBQUYsQ0FBakIsQ0FBaEI7QUFDSCxTQVBEO0FBUUEsZUFBTyxRQUFQO0FBQ0gsS0F6Qlk7QUEwQmIsYUFBUyxpQkFBVSxJQUFWLEVBQWdCO0FBQ3JCLFlBQUksT0FBTyxLQUFLLElBQUwsQ0FBVSxNQUFWLENBQVg7QUFDQSxZQUFJLEtBQUssRUFBTCxDQUFRLFFBQVIsQ0FBSixFQUF1QjtBQUNuQixnQkFBSSxPQUFPLEtBQUssSUFBTCxDQUFVLGlCQUFWLENBQVg7QUFDQSxnQkFBSSxLQUFLLE1BQUwsR0FBYyxDQUFsQixFQUFxQjtBQUNqQixvQkFBSSxLQUFLLElBQUwsQ0FBVSxVQUFWLEtBQXlCLFFBQU8sS0FBSyxJQUFMLEVBQVAsS0FBc0IsUUFBbkQsRUFBNkQ7QUFDekQsMkJBQU8sS0FBSyxJQUFMLEdBQVksSUFBWixDQUFpQixHQUFqQixDQUFQO0FBQ0g7QUFDRCx1QkFBTyxLQUFLLElBQUwsRUFBUDtBQUNIO0FBQ0osU0FSRCxNQVFPLElBQUksUUFBUSxZQUFaLEVBQTBCO0FBQzdCLG1CQUFPLEtBQUssQ0FBTCxFQUFRLElBQVIsRUFBUDtBQUNIO0FBQ0osS0F2Q1k7QUF3Q2IsY0FBVSxrQkFBVSxJQUFWLEVBQWdCO0FBQ3RCLFlBQUksT0FBTyxLQUFLLElBQUwsQ0FBVSxNQUFWLENBQVg7QUFDQSxZQUFJLFFBQVEsVUFBWixFQUF3QjtBQUNwQixtQkFBTyxLQUFLLEVBQUwsQ0FBUSxVQUFSLENBQVA7QUFDSCxTQUZELE1BRU8sSUFBSSxLQUFLLElBQUwsQ0FBVSxTQUFWLENBQUosRUFBMEI7QUFDN0IsZ0JBQUksS0FBSyxJQUFMLENBQVUsVUFBVixDQUFKLEVBQTJCO0FBQ3ZCLG9CQUFJLEtBQUssT0FBTCxDQUFhLEtBQWIsQ0FBSixFQUF5QjtBQUNyQiwyQkFBTyxLQUFLLE9BQUwsQ0FBYSxLQUFiLEVBQW9CLElBQXBCLENBQXlCLEdBQXpCLENBQVA7QUFDSDtBQUNKO0FBQ0QsbUJBQU8sS0FBSyxPQUFMLENBQWEsS0FBYixDQUFQO0FBQ0gsU0FQTSxNQU9BLElBQUksS0FBSyxFQUFMLENBQVEsUUFBUixDQUFKLEVBQXVCO0FBQzFCLGdCQUFJLEtBQUssSUFBTCxDQUFVLFVBQVYsQ0FBSixFQUEyQjtBQUN2Qix1QkFBTyxLQUFLLEdBQUwsR0FBVyxJQUFYLENBQWdCLEdBQWhCLENBQVA7QUFDSDtBQUNELG1CQUFPLEtBQUssR0FBTCxFQUFQO0FBQ0gsU0FMTSxNQUtBLElBQUksS0FBSyxFQUFMLENBQVEsUUFBUixDQUFKLEVBQXVCLE9BQU8sS0FBSyxHQUFMLEVBQVAsQ0FBdkIsS0FDRixJQUFJLEtBQUssQ0FBTCxFQUFRLElBQVosRUFBa0IsT0FBTyxLQUFLLENBQUwsRUFBUSxJQUFSLENBQWEsR0FBYixFQUFQLENBQWxCLEtBQ0EsSUFBSSxLQUFLLENBQUwsRUFBUSxHQUFSLElBQWUsU0FBbkIsRUFBOEIsT0FBTyxLQUFLLENBQUwsRUFBUSxHQUFSLEVBQVAsQ0FBOUIsS0FDQSxJQUFJLENBQUMsSUFBTCxFQUFXO0FBQ1osbUJBQU8sS0FBSyxJQUFMLEVBQVA7QUFDSDtBQUNKLEtBOURZO0FBK0RiLGdCQUFZLG9CQUFVLFlBQVYsRUFBd0IsSUFBeEIsRUFBOEIsSUFBOUIsRUFBb0M7QUFDNUMsWUFBSSxDQUFDLElBQUwsRUFBVztBQUNQLG1CQUFPLFdBQVA7QUFDSDtBQUNELGFBQUssSUFBSSxHQUFULElBQWdCLElBQWhCLEVBQXNCOztBQUVsQixnQkFBSSxPQUFPLEVBQUUsWUFBRixFQUFnQixJQUFoQixDQUFxQixNQUFNLElBQU4sR0FBYSxJQUFiLEdBQW9CLEdBQXBCLEdBQTBCLElBQS9DLENBQVg7O0FBRUEsZ0JBQUksT0FBTyxLQUFLLElBQUwsQ0FBVSxNQUFWLENBQVg7QUFDQSxnQkFBSSxRQUFRLFVBQVIsSUFBc0IsUUFBUSxPQUFsQyxFQUEyQztBQUN2QyxvQkFBSSxLQUFLLEdBQUwsS0FBYSxDQUFqQixFQUFvQjtBQUNoQix5QkFBSyxJQUFMLENBQVUsU0FBVixFQUFxQixTQUFyQjtBQUNIO0FBQ0osYUFKRCxNQUlPLElBQUksS0FBSyxFQUFMLENBQVEsT0FBUixLQUFvQixLQUFLLEVBQUwsQ0FBUSxVQUFSLENBQXhCLEVBQTZDO0FBQ2hELHFCQUFLLEdBQUwsQ0FBUyxLQUFLLEdBQUwsQ0FBVDtBQUNILGFBRk0sTUFFQSxJQUFJLEtBQUssRUFBTCxDQUFRLFFBQVIsQ0FBSixFQUF1QjtBQUMxQixvQkFBSSxLQUFLLElBQUwsQ0FBVSxTQUFWLENBQUosRUFBMEI7QUFDdEIseUJBQUssT0FBTCxDQUFhLEtBQWIsRUFBb0IsS0FBSyxHQUFMLENBQXBCO0FBQ0gsaUJBRkQsTUFFTztBQUNILHlCQUFLLElBQUwsQ0FBVSxRQUFWLEVBQW9CLElBQXBCLENBQXlCLFVBQVUsSUFBVixFQUFnQixHQUFoQixFQUFxQjtBQUMxQyw0QkFBSSxPQUFPLEtBQUssR0FBTCxDQUFQLEtBQXFCLFFBQXJCLElBQWlDLE9BQU8sS0FBSyxHQUFMLENBQVAsS0FBcUIsU0FBMUQsRUFBcUU7QUFDakUsZ0NBQUksRUFBRSxHQUFGLEVBQU8sR0FBUCxNQUFnQixLQUFLLEdBQUwsQ0FBcEIsRUFBK0I7QUFDM0Isa0NBQUUsR0FBRixFQUFPLElBQVAsQ0FBWSxVQUFaLEVBQXdCLFVBQXhCO0FBQ0g7QUFDSix5QkFKRCxNQUlPO0FBQ0gsaUNBQUssR0FBTCxFQUFVLEdBQVYsQ0FBYyxVQUFVLENBQVYsRUFBYTtBQUN2QixvQ0FBSSxFQUFFLEdBQUYsRUFBTyxHQUFQLE1BQWdCLENBQXBCLEVBQXVCO0FBQ25CLHNDQUFFLEdBQUYsRUFBTyxJQUFQLENBQVksVUFBWixFQUF3QixVQUF4QjtBQUNIO0FBQ0osNkJBSkQ7QUFLSDtBQUNKLHFCQVpEO0FBYUg7QUFDSixhQWxCTSxNQWtCQSxJQUFJLEtBQUssTUFBTCxJQUFlLEtBQUssQ0FBTCxFQUFRLElBQTNCLEVBQWlDLEtBQUssQ0FBTCxFQUFRLElBQVIsQ0FBYSxHQUFiLENBQWlCLEtBQUssR0FBTCxDQUFqQixFQUFqQyxLQUNGO0FBQ0QscUJBQUssSUFBTCxDQUFVLEtBQUssR0FBTCxDQUFWO0FBQ0g7QUFDSjtBQUVKO0FBdEdZLENBQWpCOztBQXlHQSxPQUFPLE9BQVAsR0FBaUI7QUFDYixhQUFTLGlCQUFVLElBQVYsRUFBZ0I7QUFDckIsWUFBSSxJQUFJLEtBQUssT0FBTCxDQUFhLGVBQWIsRUFBOEIsRUFBOUIsQ0FBUjtBQUNBLGVBQU8sRUFBRSxPQUFGLENBQVUsTUFBTSxRQUFRLFlBQVIsQ0FBcUIsSUFBckIsQ0FBaEIsRUFBNEMsRUFBNUMsQ0FBUDtBQUNILEtBSlk7QUFLYixrQkFBYyxzQkFBVSxJQUFWLEVBQWdCO0FBQzFCLGVBQU8sS0FBSyxPQUFMLENBQWEsdUJBQWIsRUFBc0MsSUFBdEMsQ0FBUDtBQUNILEtBUFk7QUFRYixjQUFVLGtCQUFVLElBQVYsRUFBZ0I7QUFDdEI7QUFDQSxVQUFFLFlBQUYsRUFBZ0IsTUFBaEI7QUFDQSxVQUFFLE1BQUYsRUFBVSxNQUFWLENBQWlCLGlDQUFpQyxRQUFRLEdBQXpDLEdBQStDLDZCQUEvQyxHQUErRSxJQUEvRSxHQUFzRixvQ0FBdkc7QUFDSDtBQVpZLENBQWpCOzs7Ozs7O0FDeFJBOztBQUVBOztBQUNBOzs7Q0NIQSxBQUVJLEFBRUksQUE0QkksQUFjQSxBQTBDUSxBQUlSLEFBNEJBLEFBSUosQUFRSSxBQUlKLEFBVVI7Q0F4SVE7Q0FJQTtDQUVJLEFBd0ZBO0NBdEZBO0NBRUE7Q0FFSTtDQUVBO0NBRUE7Q0FFSTtDQUVKOztDQU1KO0NBRUk7Q0FFQTtDQUVBO0NBRUE7O0NBSUo7Q0FFSTtDQUVJLEFBMEZoQixBQUVBO0NBMUZnQjtDQUVBO0NBRUE7Q0FFSSxBQXNCQTtDQTFFcEIsQUFzRHdCLEFBNEV4QjtDQTFFNEI7Q0FFSjtDQUVKLEFBa0JBLEFBSVI7Q0FwQkk7Q0FFQTtDQUVJO0NBRUk7Q0FFQTtDQUVKLEFBb0JoQixBQU1BOztDQXRCb0I7Ozs7O0NBWWhCOztDQU1KOzs7Q0FNQTtDQUVJO0NBRUE7Q0FFSSxBQUlBO0NBaEhaLEFBOEdROzs7Q0FNQTs7Q0FJSjtDQUVJO0NBRUk7O0NBSUo7Ozs7Ozs7Ozs7O0NDeEhKLEFBTUosQUFNQSxBQWtDZ0IsQUFJUixBQWtCQSxBQUlKLEFBZ0JJLEFBSUosQUFNQSxBQVVJLEFBZ0JKLEFBb0JBLEFBZ0JRLEFBWVIsQUFZQSxBQVVBO0NBbExBO0NBSUE7Q0FFQTtDQUVBO0NBRUksQUFNQTtDQUpKLEFBMEJBLEFBZ0NRLEFBb0NKO0NBNUZKOztDQUlJO0NBRUk7Q0F0QlosQUF3QmdCLEFBd0loQjtDQXRJZ0IsQUF3SlI7Q0F0SlE7Q0FsQ2hCLEFBb0NvQjs7Q0FJUjs7O0NBUVI7Q0FFSSxBQW9JQTtDQWxJQTtDQXhFUixBQTBFWSxBQUlBO0NBRko7OztDQU1BOztDQUlKO0NBRUk7Q0FFSTtDQUVBO0NBRUk7Q0FFQSxBQW9DSjs7O0NBOUdSLEFBZ0ZJOztDQUlKLEFBd0ZJO0NBdEZBOztDQTlGSixBQWtHQTtDQUVJO0NBRUEsQUEwRVIsQUFFSSxBQWdCSjtDQTFGWTs7Q0FJSjtDQUVBO0NBMUhKLEFBRUksQUEwSEE7Q0FFSTtDQUVBOzs7O0NBWVI7Q0FoSlIsQUFrSlksQUFzQkksQUFrQ1IsQUFVUjtDQWhFWTtDQUVJO0NBRUE7Q0FFSixBQXdCQTtDQXRCQTs7Q0FsSkEsQUF3Sko7Q0FFSTtDQTVKSSxBQThKSjs7Q0FJSTtDQUVJOztDQUlKOztDQUlKO0NBeEtKLEFBMEtJO0NBRUE7Ozs7Ozs7Ozs7OztDQ3hMWixBQUVJLEFBRUksQUFFSSxBQUVJLEFBRUEsQUFFSixBQUVKLEFBRUEsQUFFSSxBQUVJLEFBRUEsQUFFQSxBQUVKLEFBRUEsQUFFSSxBQUVJLEFBRUksQUFFSixBQUVBLEFBRUksQUFFSixBQUVKLEFBRUosQUFFSixBQUVBLEFBRUksQUFFSSxBQUVBLEFBRUEsQUFFSixBQUVBLEFBRUksQUFFSSxBQUVJLEFBRUosQUFFSixBQUVKLEFBRUosQUFFQSxBQUVJLEFBRUEsQUFFSSxBQUVBLEFBRUEsQUFFSixBQUVBLEFBRUksQUFFSSxBQUVJLEFBRUEsQUFFSSxBQUVJLEFBRUosQUFFQSxBQUVJLEFBRUosQUFFQSxBQUVJLEFBRUosQUFFQSxBQUVJLEFBRUosQUFFQSxBQUVJLEFBRUosQUFFQSxBQUVJLEFBRUosQUFFSixBQUVKLEFBRUEsQUFFSSxBQUVBLEFBRUksQUFFSSxBQUVKLEFBRUEsQUFFSSxBQUVKLEFBRUEsQUFFSSxBQUVKLEFBRUEsQUFFSSxBQUVKLEFBRUEsQUFFSSxBQUVKLEFBRUEsQUFFSSxBQUVKLEFBRUEsQUFFSSxBQUVKLEFBRUEsQUFFSSxBQUVKLEFBRUosQUFFSixBQUVBLEFBRUksQUFFQSxBQUVJLEFBRUksQUFFSixBQUVBLEFBRUksQUFFSixBQUVBLEFBRUksQUFFSixBQUVKLEFBRUosQUFFSixBQUVKLEFBRUosQUFFQSxBQUVJLEFBRUksQUFFQSxBQUVBLEFBRUosQUFFQSxBQUVJLEFBRUksQUFFSSxBQUVKLEFBRUEsQUFFSSxBQUVKLEFBRUEsQUFFSSxBQUVKLEFBRUEsQUFFSSxBQUVKLEFBRUosQUFFSixBQUVKLEFBRUosQUFFQSxBQUVBLEFBRUo7Ozs7Ozs7O0FDeFJBOztBQUNBOztBQUNBOztBQVJBLE9BQU8sSUFBUCxHQUFjLFFBQVEsTUFBUixDQUFkO0FBQ0EsUUFBUSxpQkFBUjtBQUNBO0FBQ0EsUUFBUSxtQkFBUjs7QUFFQSxJQUFJLFNBQVMsUUFBUSxVQUFSLEVBQW9CLE9BQWpDOztBQUlBOzs7QUFHQSxLQUFLLE1BQUwsR0FBYyxLQUFLLEtBQUwsQ0FBVyxjQUFYLEVBQTJCO0FBQ3JDLFlBQVEsTUFENkI7QUFFckMsYUFBUztBQUNMLGtCQUFVLElBREw7QUFFTCxjQUFNO0FBRkQ7QUFGNEIsQ0FBM0IsQ0FBZDs7Ozs7Ozs7OztBQ1pBLElBQUksT0FBTyxTQUFQLElBQU8sQ0FBVSxHQUFWLEVBQWUsSUFBZixFQUFxQixJQUFyQixFQUEyQjtBQUNsQyxRQUFJLFlBQVksYUFBYSxPQUFiLENBQXFCLE9BQXJCLENBQWhCO0FBQ0EsUUFBSSxTQUFKLEVBQWUsT0FBTyxNQUFQOztBQUVmLFNBQUssUUFBTCxDQUFjLFFBQWQ7QUFDSCxDQUxEO0FBTUEsSUFBSSxTQUFTLENBQUM7QUFDTixXQUFPLEdBREQ7QUFFTixTQUFLO0FBRkMsQ0FBRCxFQUdOO0FBQ0MsV0FBTyxRQURSO0FBRUMsU0FBSztBQUZOLENBSE0sRUFPVDtBQUNJLFdBQU8sU0FEWDtBQUVJLFNBQUs7QUFGVCxDQVBTLEVBV1Q7QUFDSSxXQUFPLFFBRFg7QUFFSSxTQUFLO0FBRlQsQ0FYUyxFQWVUO0FBQ0ksV0FBTyxPQURYO0FBRUksU0FBSyxLQUZUO0FBR0ksWUFBUSxDQUFDO0FBQ0QsZUFBTyxHQUROO0FBRUQsYUFBSztBQUZKLEtBQUQsRUFJSjtBQUNJLGVBQU8sZUFEWDtBQUVJLGFBQUs7QUFGVCxLQUpJLEVBUUo7QUFDSSxlQUFPLG1CQURYO0FBRUksYUFBSyxjQUZUO0FBR0ksYUFBSyxhQUFVLEdBQVYsRUFBZSxJQUFmLEVBQXFCLElBQXJCLEVBQTJCO0FBQzVCLGNBQUUsT0FBRixDQUFVO0FBQ04scUJBQUssbUJBREM7QUFFTixzQkFBTTtBQUNGLHlCQUFLLEtBQUssU0FBTCxDQUFlO0FBQ2hCLDRCQUFJLElBQUksTUFBSixDQUFXO0FBREMscUJBQWY7QUFESCxpQkFGQTtBQU9OLHlCQUFTLGlCQUFVLElBQVYsRUFBZ0I7QUFDckIseUJBQUssSUFBTCxHQUFZLElBQVo7QUFDQTtBQUNIO0FBVkssYUFBVjtBQWFIO0FBakJMLEtBUkksRUEyQko7QUFDSSxlQUFPLFlBRFg7QUFFSSxhQUFLO0FBRlQsS0EzQkksRUErQko7QUFDSSxlQUFPLGdCQURYO0FBRUksYUFBSyxXQUZUO0FBR0ksYUFBSyxhQUFVLEdBQVYsRUFBZSxJQUFmLEVBQXFCLElBQXJCLEVBQTJCO0FBQzVCLGNBQUUsT0FBRixDQUFVO0FBQ04scUJBQUssZ0JBREM7QUFFTixzQkFBTTtBQUNGLHlCQUFLLEtBQUssU0FBTCxDQUFlO0FBQ2hCLDRCQUFJLElBQUksTUFBSixDQUFXO0FBREMscUJBQWY7QUFESCxpQkFGQTtBQU9OLHlCQUFTLGlCQUFVLElBQVYsRUFBZ0I7QUFDckIseUJBQUssSUFBTCxHQUFZLElBQVo7QUFDQTtBQUNIO0FBVkssYUFBVjtBQWFIO0FBakJMLEtBL0JJLEVBa0RKO0FBQ0ksZUFBTyxvQkFEWDtBQUVJLGFBQUs7QUFGVCxLQWxESSxFQXNESjtBQUNJLGVBQU8sd0JBRFg7QUFFSSxhQUFLLG1CQUZUO0FBR0ksYUFBSyxhQUFVLEdBQVYsRUFBZSxJQUFmLEVBQXFCLElBQXJCLEVBQTJCOztBQUU1QixjQUFFLE9BQUYsQ0FBVTtBQUNOLHFCQUFLLHlCQURDO0FBRU4sc0JBQU07QUFDRix5QkFBSyxLQUFLLFNBQUwsQ0FBZTtBQUNoQiw0QkFBSSxJQUFJLE1BQUosQ0FBVztBQURDLHFCQUFmO0FBREgsaUJBRkE7QUFPTix5QkFBUyxpQkFBVSxJQUFWLEVBQWdCO0FBQ3JCLHlCQUFLLElBQUwsR0FBWSxJQUFaO0FBQ0E7QUFDSDtBQVZLLGFBQVY7QUFhSDtBQWxCTCxLQXRESTtBQUhaLENBZlMsQ0FBYjs7a0JBaUdlLE07O0FBRWY7OztDQ3pHQSxBQVlRLEFBRUUsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVGLEFBRUYsQUFFQSxBQUlFLEFBRUUsQUFFRSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBSUEsQUFFRixBQUVGLEFBRUYsQUFFQSxBQUVFLEFBSUosQUFFRixBQUVBLEFBSUksQUFFRixBQUVGLEFBRUYsQUFNQSxBQU1NLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFQSxBQVVBLEFBRUY7Q0E5QkksQUFJRSxBQWdCRjtDQWhHRixBQUVFLEFBRUEsQUFFRSxBQW9CSSxBQWdCUSxBQWNSLEFBUUYsQUFrQkEsQUFnQko7Q0FJQTs7Ozs7O0NDaEZnQyxBQUVKLEFBRUosQUFZUSxBQUVKLEFBRUosQUFZUSxBQUVKLEFBRUosQUFZUSxBQUVKLEFBRUosQUFZUSxBQUVKLEFBRUosQUFFSixBQUlSLEFBRUosQUFFSixBQUVKLEFBa0JRLEFBWUEsQUE0QkksQUFJUjtDQTFEQTtDQUlBO0NBRUk7Q0FsQlEsQUFvQlI7Q0FFQTtDQWhGNEIsQUFrRnhCOztDQUlSLEFBa0NZLEFBTVI7Q0F0Q0o7Q0FFSTtDQUVBO0NBRUk7O0NBMUhJLEFBTVksQUFFSSxBQUlJLEFBVVIsQUFFSSxBQWNKLEFBRUksQUFJSSxBQVVSLEFBRUksQUFJSSxBQVVSLEFBRUksQUFzRHhCO0NBRUE7Q0FFQTtDQTlJWixBQWdKZ0IsQUE0QmhCO0NBMUJnQjtDQTlJUixBQXdHSixBQXdDZ0IsQUFzQmhCO0NBcEJZO0NBNUlJLEFBOElKO0NBdEpaLEFBSVEsQUFFSSxBQU1RLEFBMEVnQixBQWtFcEI7Q0FFSTtDQTVJSSxBQU1RLEFBVVIsQUFNUSxBQVVSLEFBTVEsQUFVUixBQU1RLEFBVVIsQUFNUSxBQXdFWjs7Q0FJSjs7Ozs7Ozs7O0NDaEtoQixBQUVJLEFBRUksQUFFQSxBQUlRLEFBRUosQUFFSixBQUVKLEFBRUosQUFzQmdCLEFBY1EsQUFNUixBQVlRLEFBUVI7Q0ExRFo7Q0FFQTtDQUVJO0NBRUk7Q0E5QmhCLEFBZ0NvQixBQXNFcEI7Q0FwRW9CO0NBWmhCLEFBY29CLEFBZ0VwQjtDQTlEd0I7Q0FFSixBQXdEWixBQUVKOztDQXREUSxBQW9CQSxBQW9CQTtDQTFFQSxBQW9DQTtDQUVJO0NBRUEsQUFrQkEsQUFvQkE7Q0FwQ0EsQUFrQkE7Q0FoQkksQUFrQkE7O0NBZHhCO0NBRTRCLEFBb0JBOzs7Q0FkUjs7Ozs7Q0FVcEI7Q0FFd0I7Ozs7Q0FRSjs7Q0FJQTtDQUlKOzs7Ozs7OztDQ3BFb0IsQUFFSixBQVVJLEFBRUosQUFFSixBQVlRLEFBRUosQUFZSSxBQUVKLEFBRUosQUFZUSxBQUVKLEFBRUosQUFZUSxBQUVKLEFBRUosQUFFSixBQUlSLEFBRUosQUFFSixBQUVKLEFBa0JRLEFBWUEsQUE4QkksQUFJUjtDQTVEQTtDQUlBO0NBRUk7Q0FsQlEsQUFvQlI7Q0FFQTtDQUVJOztDQUlSLEFBb0NZLEFBTVI7Q0F4Q0o7Q0FFSTtDQUVBO0NBRUk7O0NBcElJLEFBTVksQUFFSSxBQUlJLEFBTVIsQUFFSSxBQWNKLEFBRUksQUFJSSxBQU1SLEFBRUksQUFnQkosQUFFSSxBQUlJLEFBVVIsQUFFSSxBQXNEeEI7Q0FFQTtDQUVBO0NBeEpaLEFBZ0V3QyxBQTBGeEIsQUE0QlosQUFFSjtDQTVCZ0I7Q0F4SlIsQUFrSEosQUF3Q2dCO0NBMUhvQixBQThCSSxBQWdDSixBQThEcEI7Q0FFSjtDQXhKSSxBQTBKSjtDQWxLWixBQUlRLEFBRUksQUFNUSxBQXdKSjtDQUVJO0NBeEpJLEFBTVEsQUFZQSxBQVVSLEFBTVEsQUFZQSxBQVlSLEFBTVEsQUFVUixBQU1RLEFBMEVaOztDQUlKOzs7Ozs7Ozs7Q0M1S2hCLEFBRUksQUFVSSxBQUlRLEFBOEJBLEFBY1EsQUFRUixBQW9CUSxBQVFSO0NBcEVaO0NBRUE7Q0FFSTtDQUVJO0NBRUksQUFzRXBCO0NBcEVvQjtDQVpoQixBQWNvQixBQWdFcEI7Q0E5RHdCO0NBRUosQUF3RFosQUFFSjs7Q0F0RFEsQUFzQkEsQUFRQTtDQWhFQSxBQW9DQTtDQTlDSixBQWdEUTtDQUVBLEFBb0JBLEFBUUE7Q0ExQkEsQUE0QkE7Q0ExQkksQUE0QkE7O0NBOUVSLEFBRUosQUFRSSxBQUVKLEFBRUosQUFFSixBQXNDSjtDQUV3QixBQTRCQTtDQTFCSSxBQTRCQTs7O0NBeEZaLEFBa0VJOztDQTFFcEIsQUE4RW9COztDQUlBOzs7OztDQVVwQjs7OztDQVFnQjs7Ozs7OztDQ3BHaEIsQUFJSSxBQUVBLEFBRUo7Ozs7O0NDSlEsQUFFSSxBQWN3QixBQUlKLEFBSUksQUFJSixBQUlJLEFBRUosQUFRWSxBQUlSLEFBSUosQUFNUixBQUVKLEFBRUosQUFFSixBQUVKLEFBRUosQUFzQlEsQUFnQ1EsQUFnQkEsQUFRSixBQVVSLEFBUUksQUFFSjtDQXhIZ0MsQUEwQmhDLEFBMEZBO0NBeEZBO0NBbEJvQixBQW9CaEI7Q0FFSixBQXdEZ0IsQUFjWjtDQXRKWixBQW9GUTtDQUVJO0NBRUE7Q0FFSTs7Q0E1RVksQUFFSSxBQVVBLEFBSUksQUFJSixBQU1BLEFBRUksQUFFSSxBQVFKLEFBMEN4QjtDQUVBO0NBRUE7Q0FFSSxBQXdFaEI7Q0F0RWdCO0NBOUJaLEFBZ0NnQixBQXdEaEI7Q0F0RFk7Q0FFQTtDQUlJO0NBeEdKLEFBRUksQUF3R0ksQUFnQkEsQUFnQ3BCLEFBUUE7Q0F4SmdDLEFBa0dSO0NBbEh4QixBQW9Id0IsQUFnQkE7Q0FkSjtDQUVBLEFBZ0JBOztDQTlIQSxBQW9ISjs7Q0FJUTs7Ozs7Q0FZUjtDQUVBOzs7Ozs7Ozs7Ozs7O0NDbEpwQixBQUVJLEFBWUEsQUFFSjtDQVpRO0NBRUE7Q0FFSTtDQUVBO0NBRUo7Ozs7O0NDYzRCLEFBRUosQUFVSSxBQUVKLEFBRUosQUFZUSxBQUVKLEFBRUosQUFZUSxBQUVKLEFBRUosQUFZUSxBQUVKLEFBRUosQUFZUSxBQUVKLEFBRUosQUFFSixBQUlSLEFBRUosQUFFSixBQUVKLEFBa0JRLEFBWUEsQUE4QkksQUFJUjtDQTVEQTtDQUlBO0NBRUk7Q0FsQlEsQUFvQlI7Q0FFQTtDQUVJOztDQUlSLEFBb0NZLEFBTVI7Q0F4Q0o7Q0FFSTtDQUVBO0NBRUk7O0NBdElJLEFBTVksQUFFSSxBQUlJLEFBTVIsQUFFSSxBQWNKLEFBRUksQUFJSSxBQVVSLEFBRUksQUFJSSxBQVVSLEFBRUksQUFJSSxBQVVSLEFBRUksQUFzRHhCO0NBRUE7Q0FFQTtDQTFKWixBQTRKZ0IsQUE4QmhCO0NBNUJnQjtDQTFKUixBQW9ISixBQXdDZ0IsQUF3QmhCO0NBcEpvQyxBQWdFQSxBQThEcEI7Q0FFSjtDQTFKSSxBQTRKSjtDQXBLWixBQUlRLEFBRUksQUFNUSxBQTBKSjtDQUVJO0NBMUpJLEFBTVEsQUFZQSxBQVVSLEFBTVEsQUFVUixBQU1RLEFBVVIsQUFNUSxBQVVSLEFBTVEsQUEwRVo7O0NBSUo7Ozs7Ozs7OztDQzlLaEIsQUFFSSxBQUlRLEFBTUosQUFJUSxBQThCQSxBQWNRLEFBUVI7Q0F4Q1o7Q0FFQTtDQUVJO0NBRUk7Q0FwQ2hCLEFBc0NvQixBQWtEcEI7Q0FoRG9CO0NBWmhCLEFBY29CLEFBNENwQjtDQTFDd0I7Q0FFSixBQW9DWixBQUVKOztDQWxDUSxBQXNCQTtDQXhEQSxBQW9DQTtDQTlDSixBQWdEUTtDQUVBLEFBb0JBO0NBbEJBO0NBRUk7O0NBbERSLEFBRUosQUFRSSxBQUVKLEFBRUosQUFFSixBQXNDSjtDQUV3QjtDQUVJOzs7Q0FNUjs7Q0FJQTtDQUVKOzs7Ozs7Ozs7QUNoRmY7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBQ0EiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigpe2Z1bmN0aW9uIHIoZSxuLHQpe2Z1bmN0aW9uIG8oaSxmKXtpZighbltpXSl7aWYoIWVbaV0pe3ZhciBjPVwiZnVuY3Rpb25cIj09dHlwZW9mIHJlcXVpcmUmJnJlcXVpcmU7aWYoIWYmJmMpcmV0dXJuIGMoaSwhMCk7aWYodSlyZXR1cm4gdShpLCEwKTt2YXIgYT1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK2krXCInXCIpO3Rocm93IGEuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixhfXZhciBwPW5baV09e2V4cG9ydHM6e319O2VbaV1bMF0uY2FsbChwLmV4cG9ydHMsZnVuY3Rpb24ocil7dmFyIG49ZVtpXVsxXVtyXTtyZXR1cm4gbyhufHxyKX0scCxwLmV4cG9ydHMscixlLG4sdCl9cmV0dXJuIG5baV0uZXhwb3J0c31mb3IodmFyIHU9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZSxpPTA7aTx0Lmxlbmd0aDtpKyspbyh0W2ldKTtyZXR1cm4gb31yZXR1cm4gcn0pKCkiLCIoZnVuY3Rpb24gKGdsb2JhbCwgZmFjdG9yeSkge1xuXHR0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcgJiYgdHlwZW9mIG1vZHVsZSAhPT0gJ3VuZGVmaW5lZCcgPyBtb2R1bGUuZXhwb3J0cyA9IGZhY3RvcnkoKSA6XG5cdHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZCA/IGRlZmluZShmYWN0b3J5KSA6XG5cdChnbG9iYWwucGFnZSA9IGZhY3RvcnkoKSk7XG59KHRoaXMsIChmdW5jdGlvbiAoKSB7ICd1c2Ugc3RyaWN0JztcblxudmFyIGlzYXJyYXkgPSBBcnJheS5pc0FycmF5IHx8IGZ1bmN0aW9uIChhcnIpIHtcbiAgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChhcnIpID09ICdbb2JqZWN0IEFycmF5XSc7XG59O1xuXG4vKipcbiAqIEV4cG9zZSBgcGF0aFRvUmVnZXhwYC5cbiAqL1xudmFyIHBhdGhUb1JlZ2V4cF8xID0gcGF0aFRvUmVnZXhwO1xudmFyIHBhcnNlXzEgPSBwYXJzZTtcbnZhciBjb21waWxlXzEgPSBjb21waWxlO1xudmFyIHRva2Vuc1RvRnVuY3Rpb25fMSA9IHRva2Vuc1RvRnVuY3Rpb247XG52YXIgdG9rZW5zVG9SZWdFeHBfMSA9IHRva2Vuc1RvUmVnRXhwO1xuXG4vKipcbiAqIFRoZSBtYWluIHBhdGggbWF0Y2hpbmcgcmVnZXhwIHV0aWxpdHkuXG4gKlxuICogQHR5cGUge1JlZ0V4cH1cbiAqL1xudmFyIFBBVEhfUkVHRVhQID0gbmV3IFJlZ0V4cChbXG4gIC8vIE1hdGNoIGVzY2FwZWQgY2hhcmFjdGVycyB0aGF0IHdvdWxkIG90aGVyd2lzZSBhcHBlYXIgaW4gZnV0dXJlIG1hdGNoZXMuXG4gIC8vIFRoaXMgYWxsb3dzIHRoZSB1c2VyIHRvIGVzY2FwZSBzcGVjaWFsIGNoYXJhY3RlcnMgdGhhdCB3b24ndCB0cmFuc2Zvcm0uXG4gICcoXFxcXFxcXFwuKScsXG4gIC8vIE1hdGNoIEV4cHJlc3Mtc3R5bGUgcGFyYW1ldGVycyBhbmQgdW4tbmFtZWQgcGFyYW1ldGVycyB3aXRoIGEgcHJlZml4XG4gIC8vIGFuZCBvcHRpb25hbCBzdWZmaXhlcy4gTWF0Y2hlcyBhcHBlYXIgYXM6XG4gIC8vXG4gIC8vIFwiLzp0ZXN0KFxcXFxkKyk/XCIgPT4gW1wiL1wiLCBcInRlc3RcIiwgXCJcXGQrXCIsIHVuZGVmaW5lZCwgXCI/XCIsIHVuZGVmaW5lZF1cbiAgLy8gXCIvcm91dGUoXFxcXGQrKVwiICA9PiBbdW5kZWZpbmVkLCB1bmRlZmluZWQsIHVuZGVmaW5lZCwgXCJcXGQrXCIsIHVuZGVmaW5lZCwgdW5kZWZpbmVkXVxuICAvLyBcIi8qXCIgICAgICAgICAgICA9PiBbXCIvXCIsIHVuZGVmaW5lZCwgdW5kZWZpbmVkLCB1bmRlZmluZWQsIHVuZGVmaW5lZCwgXCIqXCJdXG4gICcoW1xcXFwvLl0pPyg/Oig/OlxcXFw6KFxcXFx3KykoPzpcXFxcKCgoPzpcXFxcXFxcXC58W14oKV0pKylcXFxcKSk/fFxcXFwoKCg/OlxcXFxcXFxcLnxbXigpXSkrKVxcXFwpKShbKyo/XSk/fChcXFxcKikpJ1xuXS5qb2luKCd8JyksICdnJyk7XG5cbi8qKlxuICogUGFyc2UgYSBzdHJpbmcgZm9yIHRoZSByYXcgdG9rZW5zLlxuICpcbiAqIEBwYXJhbSAge1N0cmluZ30gc3RyXG4gKiBAcmV0dXJuIHtBcnJheX1cbiAqL1xuZnVuY3Rpb24gcGFyc2UgKHN0cikge1xuICB2YXIgdG9rZW5zID0gW107XG4gIHZhciBrZXkgPSAwO1xuICB2YXIgaW5kZXggPSAwO1xuICB2YXIgcGF0aCA9ICcnO1xuICB2YXIgcmVzO1xuXG4gIHdoaWxlICgocmVzID0gUEFUSF9SRUdFWFAuZXhlYyhzdHIpKSAhPSBudWxsKSB7XG4gICAgdmFyIG0gPSByZXNbMF07XG4gICAgdmFyIGVzY2FwZWQgPSByZXNbMV07XG4gICAgdmFyIG9mZnNldCA9IHJlcy5pbmRleDtcbiAgICBwYXRoICs9IHN0ci5zbGljZShpbmRleCwgb2Zmc2V0KTtcbiAgICBpbmRleCA9IG9mZnNldCArIG0ubGVuZ3RoO1xuXG4gICAgLy8gSWdub3JlIGFscmVhZHkgZXNjYXBlZCBzZXF1ZW5jZXMuXG4gICAgaWYgKGVzY2FwZWQpIHtcbiAgICAgIHBhdGggKz0gZXNjYXBlZFsxXTtcbiAgICAgIGNvbnRpbnVlXG4gICAgfVxuXG4gICAgLy8gUHVzaCB0aGUgY3VycmVudCBwYXRoIG9udG8gdGhlIHRva2Vucy5cbiAgICBpZiAocGF0aCkge1xuICAgICAgdG9rZW5zLnB1c2gocGF0aCk7XG4gICAgICBwYXRoID0gJyc7XG4gICAgfVxuXG4gICAgdmFyIHByZWZpeCA9IHJlc1syXTtcbiAgICB2YXIgbmFtZSA9IHJlc1szXTtcbiAgICB2YXIgY2FwdHVyZSA9IHJlc1s0XTtcbiAgICB2YXIgZ3JvdXAgPSByZXNbNV07XG4gICAgdmFyIHN1ZmZpeCA9IHJlc1s2XTtcbiAgICB2YXIgYXN0ZXJpc2sgPSByZXNbN107XG5cbiAgICB2YXIgcmVwZWF0ID0gc3VmZml4ID09PSAnKycgfHwgc3VmZml4ID09PSAnKic7XG4gICAgdmFyIG9wdGlvbmFsID0gc3VmZml4ID09PSAnPycgfHwgc3VmZml4ID09PSAnKic7XG4gICAgdmFyIGRlbGltaXRlciA9IHByZWZpeCB8fCAnLyc7XG4gICAgdmFyIHBhdHRlcm4gPSBjYXB0dXJlIHx8IGdyb3VwIHx8IChhc3RlcmlzayA/ICcuKicgOiAnW14nICsgZGVsaW1pdGVyICsgJ10rPycpO1xuXG4gICAgdG9rZW5zLnB1c2goe1xuICAgICAgbmFtZTogbmFtZSB8fCBrZXkrKyxcbiAgICAgIHByZWZpeDogcHJlZml4IHx8ICcnLFxuICAgICAgZGVsaW1pdGVyOiBkZWxpbWl0ZXIsXG4gICAgICBvcHRpb25hbDogb3B0aW9uYWwsXG4gICAgICByZXBlYXQ6IHJlcGVhdCxcbiAgICAgIHBhdHRlcm46IGVzY2FwZUdyb3VwKHBhdHRlcm4pXG4gICAgfSk7XG4gIH1cblxuICAvLyBNYXRjaCBhbnkgY2hhcmFjdGVycyBzdGlsbCByZW1haW5pbmcuXG4gIGlmIChpbmRleCA8IHN0ci5sZW5ndGgpIHtcbiAgICBwYXRoICs9IHN0ci5zdWJzdHIoaW5kZXgpO1xuICB9XG5cbiAgLy8gSWYgdGhlIHBhdGggZXhpc3RzLCBwdXNoIGl0IG9udG8gdGhlIGVuZC5cbiAgaWYgKHBhdGgpIHtcbiAgICB0b2tlbnMucHVzaChwYXRoKTtcbiAgfVxuXG4gIHJldHVybiB0b2tlbnNcbn1cblxuLyoqXG4gKiBDb21waWxlIGEgc3RyaW5nIHRvIGEgdGVtcGxhdGUgZnVuY3Rpb24gZm9yIHRoZSBwYXRoLlxuICpcbiAqIEBwYXJhbSAge1N0cmluZ30gICBzdHJcbiAqIEByZXR1cm4ge0Z1bmN0aW9ufVxuICovXG5mdW5jdGlvbiBjb21waWxlIChzdHIpIHtcbiAgcmV0dXJuIHRva2Vuc1RvRnVuY3Rpb24ocGFyc2Uoc3RyKSlcbn1cblxuLyoqXG4gKiBFeHBvc2UgYSBtZXRob2QgZm9yIHRyYW5zZm9ybWluZyB0b2tlbnMgaW50byB0aGUgcGF0aCBmdW5jdGlvbi5cbiAqL1xuZnVuY3Rpb24gdG9rZW5zVG9GdW5jdGlvbiAodG9rZW5zKSB7XG4gIC8vIENvbXBpbGUgYWxsIHRoZSB0b2tlbnMgaW50byByZWdleHBzLlxuICB2YXIgbWF0Y2hlcyA9IG5ldyBBcnJheSh0b2tlbnMubGVuZ3RoKTtcblxuICAvLyBDb21waWxlIGFsbCB0aGUgcGF0dGVybnMgYmVmb3JlIGNvbXBpbGF0aW9uLlxuICBmb3IgKHZhciBpID0gMDsgaSA8IHRva2Vucy5sZW5ndGg7IGkrKykge1xuICAgIGlmICh0eXBlb2YgdG9rZW5zW2ldID09PSAnb2JqZWN0Jykge1xuICAgICAgbWF0Y2hlc1tpXSA9IG5ldyBSZWdFeHAoJ14nICsgdG9rZW5zW2ldLnBhdHRlcm4gKyAnJCcpO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBmdW5jdGlvbiAob2JqKSB7XG4gICAgdmFyIHBhdGggPSAnJztcbiAgICB2YXIgZGF0YSA9IG9iaiB8fCB7fTtcblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdG9rZW5zLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgdG9rZW4gPSB0b2tlbnNbaV07XG5cbiAgICAgIGlmICh0eXBlb2YgdG9rZW4gPT09ICdzdHJpbmcnKSB7XG4gICAgICAgIHBhdGggKz0gdG9rZW47XG5cbiAgICAgICAgY29udGludWVcbiAgICAgIH1cblxuICAgICAgdmFyIHZhbHVlID0gZGF0YVt0b2tlbi5uYW1lXTtcbiAgICAgIHZhciBzZWdtZW50O1xuXG4gICAgICBpZiAodmFsdWUgPT0gbnVsbCkge1xuICAgICAgICBpZiAodG9rZW4ub3B0aW9uYWwpIHtcbiAgICAgICAgICBjb250aW51ZVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ0V4cGVjdGVkIFwiJyArIHRva2VuLm5hbWUgKyAnXCIgdG8gYmUgZGVmaW5lZCcpXG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKGlzYXJyYXkodmFsdWUpKSB7XG4gICAgICAgIGlmICghdG9rZW4ucmVwZWF0KSB7XG4gICAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignRXhwZWN0ZWQgXCInICsgdG9rZW4ubmFtZSArICdcIiB0byBub3QgcmVwZWF0LCBidXQgcmVjZWl2ZWQgXCInICsgdmFsdWUgKyAnXCInKVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHZhbHVlLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgIGlmICh0b2tlbi5vcHRpb25hbCkge1xuICAgICAgICAgICAgY29udGludWVcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignRXhwZWN0ZWQgXCInICsgdG9rZW4ubmFtZSArICdcIiB0byBub3QgYmUgZW1wdHknKVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGZvciAodmFyIGogPSAwOyBqIDwgdmFsdWUubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgICBzZWdtZW50ID0gZW5jb2RlVVJJQ29tcG9uZW50KHZhbHVlW2pdKTtcblxuICAgICAgICAgIGlmICghbWF0Y2hlc1tpXS50ZXN0KHNlZ21lbnQpKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdFeHBlY3RlZCBhbGwgXCInICsgdG9rZW4ubmFtZSArICdcIiB0byBtYXRjaCBcIicgKyB0b2tlbi5wYXR0ZXJuICsgJ1wiLCBidXQgcmVjZWl2ZWQgXCInICsgc2VnbWVudCArICdcIicpXG4gICAgICAgICAgfVxuXG4gICAgICAgICAgcGF0aCArPSAoaiA9PT0gMCA/IHRva2VuLnByZWZpeCA6IHRva2VuLmRlbGltaXRlcikgKyBzZWdtZW50O1xuICAgICAgICB9XG5cbiAgICAgICAgY29udGludWVcbiAgICAgIH1cblxuICAgICAgc2VnbWVudCA9IGVuY29kZVVSSUNvbXBvbmVudCh2YWx1ZSk7XG5cbiAgICAgIGlmICghbWF0Y2hlc1tpXS50ZXN0KHNlZ21lbnQpKSB7XG4gICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ0V4cGVjdGVkIFwiJyArIHRva2VuLm5hbWUgKyAnXCIgdG8gbWF0Y2ggXCInICsgdG9rZW4ucGF0dGVybiArICdcIiwgYnV0IHJlY2VpdmVkIFwiJyArIHNlZ21lbnQgKyAnXCInKVxuICAgICAgfVxuXG4gICAgICBwYXRoICs9IHRva2VuLnByZWZpeCArIHNlZ21lbnQ7XG4gICAgfVxuXG4gICAgcmV0dXJuIHBhdGhcbiAgfVxufVxuXG4vKipcbiAqIEVzY2FwZSBhIHJlZ3VsYXIgZXhwcmVzc2lvbiBzdHJpbmcuXG4gKlxuICogQHBhcmFtICB7U3RyaW5nfSBzdHJcbiAqIEByZXR1cm4ge1N0cmluZ31cbiAqL1xuZnVuY3Rpb24gZXNjYXBlU3RyaW5nIChzdHIpIHtcbiAgcmV0dXJuIHN0ci5yZXBsYWNlKC8oWy4rKj89XiE6JHt9KClbXFxdfFxcL10pL2csICdcXFxcJDEnKVxufVxuXG4vKipcbiAqIEVzY2FwZSB0aGUgY2FwdHVyaW5nIGdyb3VwIGJ5IGVzY2FwaW5nIHNwZWNpYWwgY2hhcmFjdGVycyBhbmQgbWVhbmluZy5cbiAqXG4gKiBAcGFyYW0gIHtTdHJpbmd9IGdyb3VwXG4gKiBAcmV0dXJuIHtTdHJpbmd9XG4gKi9cbmZ1bmN0aW9uIGVzY2FwZUdyb3VwIChncm91cCkge1xuICByZXR1cm4gZ3JvdXAucmVwbGFjZSgvKFs9ITokXFwvKCldKS9nLCAnXFxcXCQxJylcbn1cblxuLyoqXG4gKiBBdHRhY2ggdGhlIGtleXMgYXMgYSBwcm9wZXJ0eSBvZiB0aGUgcmVnZXhwLlxuICpcbiAqIEBwYXJhbSAge1JlZ0V4cH0gcmVcbiAqIEBwYXJhbSAge0FycmF5fSAga2V5c1xuICogQHJldHVybiB7UmVnRXhwfVxuICovXG5mdW5jdGlvbiBhdHRhY2hLZXlzIChyZSwga2V5cykge1xuICByZS5rZXlzID0ga2V5cztcbiAgcmV0dXJuIHJlXG59XG5cbi8qKlxuICogR2V0IHRoZSBmbGFncyBmb3IgYSByZWdleHAgZnJvbSB0aGUgb3B0aW9ucy5cbiAqXG4gKiBAcGFyYW0gIHtPYmplY3R9IG9wdGlvbnNcbiAqIEByZXR1cm4ge1N0cmluZ31cbiAqL1xuZnVuY3Rpb24gZmxhZ3MgKG9wdGlvbnMpIHtcbiAgcmV0dXJuIG9wdGlvbnMuc2Vuc2l0aXZlID8gJycgOiAnaSdcbn1cblxuLyoqXG4gKiBQdWxsIG91dCBrZXlzIGZyb20gYSByZWdleHAuXG4gKlxuICogQHBhcmFtICB7UmVnRXhwfSBwYXRoXG4gKiBAcGFyYW0gIHtBcnJheX0gIGtleXNcbiAqIEByZXR1cm4ge1JlZ0V4cH1cbiAqL1xuZnVuY3Rpb24gcmVnZXhwVG9SZWdleHAgKHBhdGgsIGtleXMpIHtcbiAgLy8gVXNlIGEgbmVnYXRpdmUgbG9va2FoZWFkIHRvIG1hdGNoIG9ubHkgY2FwdHVyaW5nIGdyb3Vwcy5cbiAgdmFyIGdyb3VwcyA9IHBhdGguc291cmNlLm1hdGNoKC9cXCgoPyFcXD8pL2cpO1xuXG4gIGlmIChncm91cHMpIHtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGdyb3Vwcy5sZW5ndGg7IGkrKykge1xuICAgICAga2V5cy5wdXNoKHtcbiAgICAgICAgbmFtZTogaSxcbiAgICAgICAgcHJlZml4OiBudWxsLFxuICAgICAgICBkZWxpbWl0ZXI6IG51bGwsXG4gICAgICAgIG9wdGlvbmFsOiBmYWxzZSxcbiAgICAgICAgcmVwZWF0OiBmYWxzZSxcbiAgICAgICAgcGF0dGVybjogbnVsbFxuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIGF0dGFjaEtleXMocGF0aCwga2V5cylcbn1cblxuLyoqXG4gKiBUcmFuc2Zvcm0gYW4gYXJyYXkgaW50byBhIHJlZ2V4cC5cbiAqXG4gKiBAcGFyYW0gIHtBcnJheX0gIHBhdGhcbiAqIEBwYXJhbSAge0FycmF5fSAga2V5c1xuICogQHBhcmFtICB7T2JqZWN0fSBvcHRpb25zXG4gKiBAcmV0dXJuIHtSZWdFeHB9XG4gKi9cbmZ1bmN0aW9uIGFycmF5VG9SZWdleHAgKHBhdGgsIGtleXMsIG9wdGlvbnMpIHtcbiAgdmFyIHBhcnRzID0gW107XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBwYXRoLmxlbmd0aDsgaSsrKSB7XG4gICAgcGFydHMucHVzaChwYXRoVG9SZWdleHAocGF0aFtpXSwga2V5cywgb3B0aW9ucykuc291cmNlKTtcbiAgfVxuXG4gIHZhciByZWdleHAgPSBuZXcgUmVnRXhwKCcoPzonICsgcGFydHMuam9pbignfCcpICsgJyknLCBmbGFncyhvcHRpb25zKSk7XG5cbiAgcmV0dXJuIGF0dGFjaEtleXMocmVnZXhwLCBrZXlzKVxufVxuXG4vKipcbiAqIENyZWF0ZSBhIHBhdGggcmVnZXhwIGZyb20gc3RyaW5nIGlucHV0LlxuICpcbiAqIEBwYXJhbSAge1N0cmluZ30gcGF0aFxuICogQHBhcmFtICB7QXJyYXl9ICBrZXlzXG4gKiBAcGFyYW0gIHtPYmplY3R9IG9wdGlvbnNcbiAqIEByZXR1cm4ge1JlZ0V4cH1cbiAqL1xuZnVuY3Rpb24gc3RyaW5nVG9SZWdleHAgKHBhdGgsIGtleXMsIG9wdGlvbnMpIHtcbiAgdmFyIHRva2VucyA9IHBhcnNlKHBhdGgpO1xuICB2YXIgcmUgPSB0b2tlbnNUb1JlZ0V4cCh0b2tlbnMsIG9wdGlvbnMpO1xuXG4gIC8vIEF0dGFjaCBrZXlzIGJhY2sgdG8gdGhlIHJlZ2V4cC5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCB0b2tlbnMubGVuZ3RoOyBpKyspIHtcbiAgICBpZiAodHlwZW9mIHRva2Vuc1tpXSAhPT0gJ3N0cmluZycpIHtcbiAgICAgIGtleXMucHVzaCh0b2tlbnNbaV0pO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBhdHRhY2hLZXlzKHJlLCBrZXlzKVxufVxuXG4vKipcbiAqIEV4cG9zZSBhIGZ1bmN0aW9uIGZvciB0YWtpbmcgdG9rZW5zIGFuZCByZXR1cm5pbmcgYSBSZWdFeHAuXG4gKlxuICogQHBhcmFtICB7QXJyYXl9ICB0b2tlbnNcbiAqIEBwYXJhbSAge0FycmF5fSAga2V5c1xuICogQHBhcmFtICB7T2JqZWN0fSBvcHRpb25zXG4gKiBAcmV0dXJuIHtSZWdFeHB9XG4gKi9cbmZ1bmN0aW9uIHRva2Vuc1RvUmVnRXhwICh0b2tlbnMsIG9wdGlvbnMpIHtcbiAgb3B0aW9ucyA9IG9wdGlvbnMgfHwge307XG5cbiAgdmFyIHN0cmljdCA9IG9wdGlvbnMuc3RyaWN0O1xuICB2YXIgZW5kID0gb3B0aW9ucy5lbmQgIT09IGZhbHNlO1xuICB2YXIgcm91dGUgPSAnJztcbiAgdmFyIGxhc3RUb2tlbiA9IHRva2Vuc1t0b2tlbnMubGVuZ3RoIC0gMV07XG4gIHZhciBlbmRzV2l0aFNsYXNoID0gdHlwZW9mIGxhc3RUb2tlbiA9PT0gJ3N0cmluZycgJiYgL1xcLyQvLnRlc3QobGFzdFRva2VuKTtcblxuICAvLyBJdGVyYXRlIG92ZXIgdGhlIHRva2VucyBhbmQgY3JlYXRlIG91ciByZWdleHAgc3RyaW5nLlxuICBmb3IgKHZhciBpID0gMDsgaSA8IHRva2Vucy5sZW5ndGg7IGkrKykge1xuICAgIHZhciB0b2tlbiA9IHRva2Vuc1tpXTtcblxuICAgIGlmICh0eXBlb2YgdG9rZW4gPT09ICdzdHJpbmcnKSB7XG4gICAgICByb3V0ZSArPSBlc2NhcGVTdHJpbmcodG9rZW4pO1xuICAgIH0gZWxzZSB7XG4gICAgICB2YXIgcHJlZml4ID0gZXNjYXBlU3RyaW5nKHRva2VuLnByZWZpeCk7XG4gICAgICB2YXIgY2FwdHVyZSA9IHRva2VuLnBhdHRlcm47XG5cbiAgICAgIGlmICh0b2tlbi5yZXBlYXQpIHtcbiAgICAgICAgY2FwdHVyZSArPSAnKD86JyArIHByZWZpeCArIGNhcHR1cmUgKyAnKSonO1xuICAgICAgfVxuXG4gICAgICBpZiAodG9rZW4ub3B0aW9uYWwpIHtcbiAgICAgICAgaWYgKHByZWZpeCkge1xuICAgICAgICAgIGNhcHR1cmUgPSAnKD86JyArIHByZWZpeCArICcoJyArIGNhcHR1cmUgKyAnKSk/JztcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBjYXB0dXJlID0gJygnICsgY2FwdHVyZSArICcpPyc7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNhcHR1cmUgPSBwcmVmaXggKyAnKCcgKyBjYXB0dXJlICsgJyknO1xuICAgICAgfVxuXG4gICAgICByb3V0ZSArPSBjYXB0dXJlO1xuICAgIH1cbiAgfVxuXG4gIC8vIEluIG5vbi1zdHJpY3QgbW9kZSB3ZSBhbGxvdyBhIHNsYXNoIGF0IHRoZSBlbmQgb2YgbWF0Y2guIElmIHRoZSBwYXRoIHRvXG4gIC8vIG1hdGNoIGFscmVhZHkgZW5kcyB3aXRoIGEgc2xhc2gsIHdlIHJlbW92ZSBpdCBmb3IgY29uc2lzdGVuY3kuIFRoZSBzbGFzaFxuICAvLyBpcyB2YWxpZCBhdCB0aGUgZW5kIG9mIGEgcGF0aCBtYXRjaCwgbm90IGluIHRoZSBtaWRkbGUuIFRoaXMgaXMgaW1wb3J0YW50XG4gIC8vIGluIG5vbi1lbmRpbmcgbW9kZSwgd2hlcmUgXCIvdGVzdC9cIiBzaG91bGRuJ3QgbWF0Y2ggXCIvdGVzdC8vcm91dGVcIi5cbiAgaWYgKCFzdHJpY3QpIHtcbiAgICByb3V0ZSA9IChlbmRzV2l0aFNsYXNoID8gcm91dGUuc2xpY2UoMCwgLTIpIDogcm91dGUpICsgJyg/OlxcXFwvKD89JCkpPyc7XG4gIH1cblxuICBpZiAoZW5kKSB7XG4gICAgcm91dGUgKz0gJyQnO1xuICB9IGVsc2Uge1xuICAgIC8vIEluIG5vbi1lbmRpbmcgbW9kZSwgd2UgbmVlZCB0aGUgY2FwdHVyaW5nIGdyb3VwcyB0byBtYXRjaCBhcyBtdWNoIGFzXG4gICAgLy8gcG9zc2libGUgYnkgdXNpbmcgYSBwb3NpdGl2ZSBsb29rYWhlYWQgdG8gdGhlIGVuZCBvciBuZXh0IHBhdGggc2VnbWVudC5cbiAgICByb3V0ZSArPSBzdHJpY3QgJiYgZW5kc1dpdGhTbGFzaCA/ICcnIDogJyg/PVxcXFwvfCQpJztcbiAgfVxuXG4gIHJldHVybiBuZXcgUmVnRXhwKCdeJyArIHJvdXRlLCBmbGFncyhvcHRpb25zKSlcbn1cblxuLyoqXG4gKiBOb3JtYWxpemUgdGhlIGdpdmVuIHBhdGggc3RyaW5nLCByZXR1cm5pbmcgYSByZWd1bGFyIGV4cHJlc3Npb24uXG4gKlxuICogQW4gZW1wdHkgYXJyYXkgY2FuIGJlIHBhc3NlZCBpbiBmb3IgdGhlIGtleXMsIHdoaWNoIHdpbGwgaG9sZCB0aGVcbiAqIHBsYWNlaG9sZGVyIGtleSBkZXNjcmlwdGlvbnMuIEZvciBleGFtcGxlLCB1c2luZyBgL3VzZXIvOmlkYCwgYGtleXNgIHdpbGxcbiAqIGNvbnRhaW4gYFt7IG5hbWU6ICdpZCcsIGRlbGltaXRlcjogJy8nLCBvcHRpb25hbDogZmFsc2UsIHJlcGVhdDogZmFsc2UgfV1gLlxuICpcbiAqIEBwYXJhbSAgeyhTdHJpbmd8UmVnRXhwfEFycmF5KX0gcGF0aFxuICogQHBhcmFtICB7QXJyYXl9ICAgICAgICAgICAgICAgICBba2V5c11cbiAqIEBwYXJhbSAge09iamVjdH0gICAgICAgICAgICAgICAgW29wdGlvbnNdXG4gKiBAcmV0dXJuIHtSZWdFeHB9XG4gKi9cbmZ1bmN0aW9uIHBhdGhUb1JlZ2V4cCAocGF0aCwga2V5cywgb3B0aW9ucykge1xuICBrZXlzID0ga2V5cyB8fCBbXTtcblxuICBpZiAoIWlzYXJyYXkoa2V5cykpIHtcbiAgICBvcHRpb25zID0ga2V5cztcbiAgICBrZXlzID0gW107XG4gIH0gZWxzZSBpZiAoIW9wdGlvbnMpIHtcbiAgICBvcHRpb25zID0ge307XG4gIH1cblxuICBpZiAocGF0aCBpbnN0YW5jZW9mIFJlZ0V4cCkge1xuICAgIHJldHVybiByZWdleHBUb1JlZ2V4cChwYXRoLCBrZXlzLCBvcHRpb25zKVxuICB9XG5cbiAgaWYgKGlzYXJyYXkocGF0aCkpIHtcbiAgICByZXR1cm4gYXJyYXlUb1JlZ2V4cChwYXRoLCBrZXlzLCBvcHRpb25zKVxuICB9XG5cbiAgcmV0dXJuIHN0cmluZ1RvUmVnZXhwKHBhdGgsIGtleXMsIG9wdGlvbnMpXG59XG5cbnBhdGhUb1JlZ2V4cF8xLnBhcnNlID0gcGFyc2VfMTtcbnBhdGhUb1JlZ2V4cF8xLmNvbXBpbGUgPSBjb21waWxlXzE7XG5wYXRoVG9SZWdleHBfMS50b2tlbnNUb0Z1bmN0aW9uID0gdG9rZW5zVG9GdW5jdGlvbl8xO1xucGF0aFRvUmVnZXhwXzEudG9rZW5zVG9SZWdFeHAgPSB0b2tlbnNUb1JlZ0V4cF8xO1xuXG4vKipcbiAgICogTW9kdWxlIGRlcGVuZGVuY2llcy5cbiAgICovXG5cbiAgXG5cbiAgLyoqXG4gICAqIE1vZHVsZSBleHBvcnRzLlxuICAgKi9cblxuICB2YXIgcGFnZV9qcyA9IHBhZ2U7XG4gIHBhZ2UuZGVmYXVsdCA9IHBhZ2U7XG4gIHBhZ2UuQ29udGV4dCA9IENvbnRleHQ7XG4gIHBhZ2UuUm91dGUgPSBSb3V0ZTtcbiAgcGFnZS5zYW1lT3JpZ2luID0gc2FtZU9yaWdpbjtcblxuICAvKipcbiAgICogU2hvcnQtY3V0cyBmb3IgZ2xvYmFsLW9iamVjdCBjaGVja3NcbiAgICovXG5cbiAgdmFyIGhhc0RvY3VtZW50ID0gKCd1bmRlZmluZWQnICE9PSB0eXBlb2YgZG9jdW1lbnQpO1xuICB2YXIgaGFzV2luZG93ID0gKCd1bmRlZmluZWQnICE9PSB0eXBlb2Ygd2luZG93KTtcbiAgdmFyIGhhc0hpc3RvcnkgPSAoJ3VuZGVmaW5lZCcgIT09IHR5cGVvZiBoaXN0b3J5KTtcbiAgdmFyIGhhc1Byb2Nlc3MgPSB0eXBlb2YgcHJvY2VzcyAhPT0gJ3VuZGVmaW5lZCc7XG5cbiAgLyoqXG4gICAqIERldGVjdCBjbGljayBldmVudFxuICAgKi9cbiAgdmFyIGNsaWNrRXZlbnQgPSBoYXNEb2N1bWVudCAmJiBkb2N1bWVudC5vbnRvdWNoc3RhcnQgPyAndG91Y2hzdGFydCcgOiAnY2xpY2snO1xuXG4gIC8qKlxuICAgKiBUbyB3b3JrIHByb3Blcmx5IHdpdGggdGhlIFVSTFxuICAgKiBoaXN0b3J5LmxvY2F0aW9uIGdlbmVyYXRlZCBwb2x5ZmlsbCBpbiBodHRwczovL2dpdGh1Yi5jb20vZGV2b3RlL0hUTUw1LUhpc3RvcnktQVBJXG4gICAqL1xuXG4gIHZhciBpc0xvY2F0aW9uID0gaGFzV2luZG93ICYmICEhKHdpbmRvdy5oaXN0b3J5LmxvY2F0aW9uIHx8IHdpbmRvdy5sb2NhdGlvbik7XG5cbiAgLyoqXG4gICAqIFBlcmZvcm0gaW5pdGlhbCBkaXNwYXRjaC5cbiAgICovXG5cbiAgdmFyIGRpc3BhdGNoID0gdHJ1ZTtcblxuXG4gIC8qKlxuICAgKiBEZWNvZGUgVVJMIGNvbXBvbmVudHMgKHF1ZXJ5IHN0cmluZywgcGF0aG5hbWUsIGhhc2gpLlxuICAgKiBBY2NvbW1vZGF0ZXMgYm90aCByZWd1bGFyIHBlcmNlbnQgZW5jb2RpbmcgYW5kIHgtd3d3LWZvcm0tdXJsZW5jb2RlZCBmb3JtYXQuXG4gICAqL1xuICB2YXIgZGVjb2RlVVJMQ29tcG9uZW50cyA9IHRydWU7XG5cbiAgLyoqXG4gICAqIEJhc2UgcGF0aC5cbiAgICovXG5cbiAgdmFyIGJhc2UgPSAnJztcblxuICAvKipcbiAgICogU3RyaWN0IHBhdGggbWF0Y2hpbmcuXG4gICAqL1xuXG4gIHZhciBzdHJpY3QgPSBmYWxzZTtcblxuICAvKipcbiAgICogUnVubmluZyBmbGFnLlxuICAgKi9cblxuICB2YXIgcnVubmluZztcblxuICAvKipcbiAgICogSGFzaEJhbmcgb3B0aW9uXG4gICAqL1xuXG4gIHZhciBoYXNoYmFuZyA9IGZhbHNlO1xuXG4gIC8qKlxuICAgKiBQcmV2aW91cyBjb250ZXh0LCBmb3IgY2FwdHVyaW5nXG4gICAqIHBhZ2UgZXhpdCBldmVudHMuXG4gICAqL1xuXG4gIHZhciBwcmV2Q29udGV4dDtcblxuICAvKipcbiAgICogVGhlIHdpbmRvdyBmb3Igd2hpY2ggdGhpcyBgcGFnZWAgaXMgcnVubmluZ1xuICAgKi9cbiAgdmFyIHBhZ2VXaW5kb3c7XG5cbiAgLyoqXG4gICAqIFJlZ2lzdGVyIGBwYXRoYCB3aXRoIGNhbGxiYWNrIGBmbigpYCxcbiAgICogb3Igcm91dGUgYHBhdGhgLCBvciByZWRpcmVjdGlvbixcbiAgICogb3IgYHBhZ2Uuc3RhcnQoKWAuXG4gICAqXG4gICAqICAgcGFnZShmbik7XG4gICAqICAgcGFnZSgnKicsIGZuKTtcbiAgICogICBwYWdlKCcvdXNlci86aWQnLCBsb2FkLCB1c2VyKTtcbiAgICogICBwYWdlKCcvdXNlci8nICsgdXNlci5pZCwgeyBzb21lOiAndGhpbmcnIH0pO1xuICAgKiAgIHBhZ2UoJy91c2VyLycgKyB1c2VyLmlkKTtcbiAgICogICBwYWdlKCcvZnJvbScsICcvdG8nKVxuICAgKiAgIHBhZ2UoKTtcbiAgICpcbiAgICogQHBhcmFtIHtzdHJpbmd8IUZ1bmN0aW9ufCFPYmplY3R9IHBhdGhcbiAgICogQHBhcmFtIHtGdW5jdGlvbj19IGZuXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGZ1bmN0aW9uIHBhZ2UocGF0aCwgZm4pIHtcbiAgICAvLyA8Y2FsbGJhY2s+XG4gICAgaWYgKCdmdW5jdGlvbicgPT09IHR5cGVvZiBwYXRoKSB7XG4gICAgICByZXR1cm4gcGFnZSgnKicsIHBhdGgpO1xuICAgIH1cblxuICAgIC8vIHJvdXRlIDxwYXRoPiB0byA8Y2FsbGJhY2sgLi4uPlxuICAgIGlmICgnZnVuY3Rpb24nID09PSB0eXBlb2YgZm4pIHtcbiAgICAgIHZhciByb3V0ZSA9IG5ldyBSb3V0ZSgvKiogQHR5cGUge3N0cmluZ30gKi8gKHBhdGgpKTtcbiAgICAgIGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgKytpKSB7XG4gICAgICAgIHBhZ2UuY2FsbGJhY2tzLnB1c2gocm91dGUubWlkZGxld2FyZShhcmd1bWVudHNbaV0pKTtcbiAgICAgIH1cbiAgICAgIC8vIHNob3cgPHBhdGg+IHdpdGggW3N0YXRlXVxuICAgIH0gZWxzZSBpZiAoJ3N0cmluZycgPT09IHR5cGVvZiBwYXRoKSB7XG4gICAgICBwYWdlWydzdHJpbmcnID09PSB0eXBlb2YgZm4gPyAncmVkaXJlY3QnIDogJ3Nob3cnXShwYXRoLCBmbik7XG4gICAgICAvLyBzdGFydCBbb3B0aW9uc11cbiAgICB9IGVsc2Uge1xuICAgICAgcGFnZS5zdGFydChwYXRoKTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogQ2FsbGJhY2sgZnVuY3Rpb25zLlxuICAgKi9cblxuICBwYWdlLmNhbGxiYWNrcyA9IFtdO1xuICBwYWdlLmV4aXRzID0gW107XG5cbiAgLyoqXG4gICAqIEN1cnJlbnQgcGF0aCBiZWluZyBwcm9jZXNzZWRcbiAgICogQHR5cGUge3N0cmluZ31cbiAgICovXG4gIHBhZ2UuY3VycmVudCA9ICcnO1xuXG4gIC8qKlxuICAgKiBOdW1iZXIgb2YgcGFnZXMgbmF2aWdhdGVkIHRvLlxuICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgKlxuICAgKiAgICAgcGFnZS5sZW4gPT0gMDtcbiAgICogICAgIHBhZ2UoJy9sb2dpbicpO1xuICAgKiAgICAgcGFnZS5sZW4gPT0gMTtcbiAgICovXG5cbiAgcGFnZS5sZW4gPSAwO1xuXG4gIC8qKlxuICAgKiBHZXQgb3Igc2V0IGJhc2VwYXRoIHRvIGBwYXRoYC5cbiAgICpcbiAgICogQHBhcmFtIHtzdHJpbmd9IHBhdGhcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgcGFnZS5iYXNlID0gZnVuY3Rpb24ocGF0aCkge1xuICAgIGlmICgwID09PSBhcmd1bWVudHMubGVuZ3RoKSByZXR1cm4gYmFzZTtcbiAgICBiYXNlID0gcGF0aDtcbiAgfTtcblxuICAvKipcbiAgICogR2V0IG9yIHNldCBzdHJpY3QgcGF0aCBtYXRjaGluZyB0byBgZW5hYmxlYFxuICAgKlxuICAgKiBAcGFyYW0ge2Jvb2xlYW59IGVuYWJsZVxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBwYWdlLnN0cmljdCA9IGZ1bmN0aW9uKGVuYWJsZSkge1xuICAgIGlmICgwID09PSBhcmd1bWVudHMubGVuZ3RoKSByZXR1cm4gc3RyaWN0O1xuICAgIHN0cmljdCA9IGVuYWJsZTtcbiAgfTtcblxuICAvKipcbiAgICogQmluZCB3aXRoIHRoZSBnaXZlbiBgb3B0aW9uc2AuXG4gICAqXG4gICAqIE9wdGlvbnM6XG4gICAqXG4gICAqICAgIC0gYGNsaWNrYCBiaW5kIHRvIGNsaWNrIGV2ZW50cyBbdHJ1ZV1cbiAgICogICAgLSBgcG9wc3RhdGVgIGJpbmQgdG8gcG9wc3RhdGUgW3RydWVdXG4gICAqICAgIC0gYGRpc3BhdGNoYCBwZXJmb3JtIGluaXRpYWwgZGlzcGF0Y2ggW3RydWVdXG4gICAqXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIHBhZ2Uuc3RhcnQgPSBmdW5jdGlvbihvcHRpb25zKSB7XG4gICAgb3B0aW9ucyA9IG9wdGlvbnMgfHwge307XG4gICAgaWYgKHJ1bm5pbmcpIHJldHVybjtcbiAgICBydW5uaW5nID0gdHJ1ZTtcbiAgICBwYWdlV2luZG93ID0gb3B0aW9ucy53aW5kb3cgfHwgKGhhc1dpbmRvdyAmJiB3aW5kb3cpO1xuICAgIGlmIChmYWxzZSA9PT0gb3B0aW9ucy5kaXNwYXRjaCkgZGlzcGF0Y2ggPSBmYWxzZTtcbiAgICBpZiAoZmFsc2UgPT09IG9wdGlvbnMuZGVjb2RlVVJMQ29tcG9uZW50cykgZGVjb2RlVVJMQ29tcG9uZW50cyA9IGZhbHNlO1xuICAgIGlmIChmYWxzZSAhPT0gb3B0aW9ucy5wb3BzdGF0ZSAmJiBoYXNXaW5kb3cpIHBhZ2VXaW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigncG9wc3RhdGUnLCBvbnBvcHN0YXRlLCBmYWxzZSk7XG4gICAgaWYgKGZhbHNlICE9PSBvcHRpb25zLmNsaWNrICYmIGhhc0RvY3VtZW50KSB7XG4gICAgICBwYWdlV2luZG93LmRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoY2xpY2tFdmVudCwgb25jbGljaywgZmFsc2UpO1xuICAgIH1cbiAgICBoYXNoYmFuZyA9ICEhb3B0aW9ucy5oYXNoYmFuZztcbiAgICBpZihoYXNoYmFuZyAmJiBoYXNXaW5kb3cgJiYgIWhhc0hpc3RvcnkpIHtcbiAgICAgIHBhZ2VXaW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignaGFzaGNoYW5nZScsIG9ucG9wc3RhdGUsIGZhbHNlKTtcbiAgICB9XG4gICAgaWYgKCFkaXNwYXRjaCkgcmV0dXJuO1xuXG4gICAgdmFyIHVybDtcbiAgICBpZihpc0xvY2F0aW9uKSB7XG4gICAgICB2YXIgbG9jID0gcGFnZVdpbmRvdy5sb2NhdGlvbjtcblxuICAgICAgaWYoaGFzaGJhbmcgJiYgfmxvYy5oYXNoLmluZGV4T2YoJyMhJykpIHtcbiAgICAgICAgdXJsID0gbG9jLmhhc2guc3Vic3RyKDIpICsgbG9jLnNlYXJjaDtcbiAgICAgIH0gZWxzZSBpZiAoaGFzaGJhbmcpIHtcbiAgICAgICAgdXJsID0gbG9jLnNlYXJjaCArIGxvYy5oYXNoO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdXJsID0gbG9jLnBhdGhuYW1lICsgbG9jLnNlYXJjaCArIGxvYy5oYXNoO1xuICAgICAgfVxuICAgIH1cblxuICAgIHBhZ2UucmVwbGFjZSh1cmwsIG51bGwsIHRydWUsIGRpc3BhdGNoKTtcbiAgfTtcblxuICAvKipcbiAgICogVW5iaW5kIGNsaWNrIGFuZCBwb3BzdGF0ZSBldmVudCBoYW5kbGVycy5cbiAgICpcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgcGFnZS5zdG9wID0gZnVuY3Rpb24oKSB7XG4gICAgaWYgKCFydW5uaW5nKSByZXR1cm47XG4gICAgcGFnZS5jdXJyZW50ID0gJyc7XG4gICAgcGFnZS5sZW4gPSAwO1xuICAgIHJ1bm5pbmcgPSBmYWxzZTtcbiAgICBoYXNEb2N1bWVudCAmJiBwYWdlV2luZG93LmRvY3VtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoY2xpY2tFdmVudCwgb25jbGljaywgZmFsc2UpO1xuICAgIGhhc1dpbmRvdyAmJiBwYWdlV2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3BvcHN0YXRlJywgb25wb3BzdGF0ZSwgZmFsc2UpO1xuICAgIGhhc1dpbmRvdyAmJiBwYWdlV2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2hhc2hjaGFuZ2UnLCBvbnBvcHN0YXRlLCBmYWxzZSk7XG4gIH07XG5cbiAgLyoqXG4gICAqIFNob3cgYHBhdGhgIHdpdGggb3B0aW9uYWwgYHN0YXRlYCBvYmplY3QuXG4gICAqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBwYXRoXG4gICAqIEBwYXJhbSB7T2JqZWN0PX0gc3RhdGVcbiAgICogQHBhcmFtIHtib29sZWFuPX0gZGlzcGF0Y2hcbiAgICogQHBhcmFtIHtib29sZWFuPX0gcHVzaFxuICAgKiBAcmV0dXJuIHshQ29udGV4dH1cbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgcGFnZS5zaG93ID0gZnVuY3Rpb24ocGF0aCwgc3RhdGUsIGRpc3BhdGNoLCBwdXNoKSB7XG4gICAgdmFyIGN0eCA9IG5ldyBDb250ZXh0KHBhdGgsIHN0YXRlKSxcbiAgICAgIHByZXYgPSBwcmV2Q29udGV4dDtcbiAgICBwcmV2Q29udGV4dCA9IGN0eDtcbiAgICBwYWdlLmN1cnJlbnQgPSBjdHgucGF0aDtcbiAgICBpZiAoZmFsc2UgIT09IGRpc3BhdGNoKSBwYWdlLmRpc3BhdGNoKGN0eCwgcHJldik7XG4gICAgaWYgKGZhbHNlICE9PSBjdHguaGFuZGxlZCAmJiBmYWxzZSAhPT0gcHVzaCkgY3R4LnB1c2hTdGF0ZSgpO1xuICAgIHJldHVybiBjdHg7XG4gIH07XG5cbiAgLyoqXG4gICAqIEdvZXMgYmFjayBpbiB0aGUgaGlzdG9yeVxuICAgKiBCYWNrIHNob3VsZCBhbHdheXMgbGV0IHRoZSBjdXJyZW50IHJvdXRlIHB1c2ggc3RhdGUgYW5kIHRoZW4gZ28gYmFjay5cbiAgICpcbiAgICogQHBhcmFtIHtzdHJpbmd9IHBhdGggLSBmYWxsYmFjayBwYXRoIHRvIGdvIGJhY2sgaWYgbm8gbW9yZSBoaXN0b3J5IGV4aXN0cywgaWYgdW5kZWZpbmVkIGRlZmF1bHRzIHRvIHBhZ2UuYmFzZVxuICAgKiBAcGFyYW0ge09iamVjdD19IHN0YXRlXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIHBhZ2UuYmFjayA9IGZ1bmN0aW9uKHBhdGgsIHN0YXRlKSB7XG4gICAgaWYgKHBhZ2UubGVuID4gMCkge1xuICAgICAgLy8gdGhpcyBtYXkgbmVlZCBtb3JlIHRlc3RpbmcgdG8gc2VlIGlmIGFsbCBicm93c2Vyc1xuICAgICAgLy8gd2FpdCBmb3IgdGhlIG5leHQgdGljayB0byBnbyBiYWNrIGluIGhpc3RvcnlcbiAgICAgIGhhc0hpc3RvcnkgJiYgcGFnZVdpbmRvdy5oaXN0b3J5LmJhY2soKTtcbiAgICAgIHBhZ2UubGVuLS07XG4gICAgfSBlbHNlIGlmIChwYXRoKSB7XG4gICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICBwYWdlLnNob3cocGF0aCwgc3RhdGUpO1xuICAgICAgfSk7XG4gICAgfWVsc2V7XG4gICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICBwYWdlLnNob3coZ2V0QmFzZSgpLCBzdGF0ZSk7XG4gICAgICB9KTtcbiAgICB9XG4gIH07XG5cblxuICAvKipcbiAgICogUmVnaXN0ZXIgcm91dGUgdG8gcmVkaXJlY3QgZnJvbSBvbmUgcGF0aCB0byBvdGhlclxuICAgKiBvciBqdXN0IHJlZGlyZWN0IHRvIGFub3RoZXIgcm91dGVcbiAgICpcbiAgICogQHBhcmFtIHtzdHJpbmd9IGZyb20gLSBpZiBwYXJhbSAndG8nIGlzIHVuZGVmaW5lZCByZWRpcmVjdHMgdG8gJ2Zyb20nXG4gICAqIEBwYXJhbSB7c3RyaW5nPX0gdG9cbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG4gIHBhZ2UucmVkaXJlY3QgPSBmdW5jdGlvbihmcm9tLCB0bykge1xuICAgIC8vIERlZmluZSByb3V0ZSBmcm9tIGEgcGF0aCB0byBhbm90aGVyXG4gICAgaWYgKCdzdHJpbmcnID09PSB0eXBlb2YgZnJvbSAmJiAnc3RyaW5nJyA9PT0gdHlwZW9mIHRvKSB7XG4gICAgICBwYWdlKGZyb20sIGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgICBwYWdlLnJlcGxhY2UoLyoqIEB0eXBlIHshc3RyaW5nfSAqLyAodG8pKTtcbiAgICAgICAgfSwgMCk7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICAvLyBXYWl0IGZvciB0aGUgcHVzaCBzdGF0ZSBhbmQgcmVwbGFjZSBpdCB3aXRoIGFub3RoZXJcbiAgICBpZiAoJ3N0cmluZycgPT09IHR5cGVvZiBmcm9tICYmICd1bmRlZmluZWQnID09PSB0eXBlb2YgdG8pIHtcbiAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgIHBhZ2UucmVwbGFjZShmcm9tKTtcbiAgICAgIH0sIDApO1xuICAgIH1cbiAgfTtcblxuICAvKipcbiAgICogUmVwbGFjZSBgcGF0aGAgd2l0aCBvcHRpb25hbCBgc3RhdGVgIG9iamVjdC5cbiAgICpcbiAgICogQHBhcmFtIHtzdHJpbmd9IHBhdGhcbiAgICogQHBhcmFtIHtPYmplY3Q9fSBzdGF0ZVxuICAgKiBAcGFyYW0ge2Jvb2xlYW49fSBpbml0XG4gICAqIEBwYXJhbSB7Ym9vbGVhbj19IGRpc3BhdGNoXG4gICAqIEByZXR1cm4geyFDb250ZXh0fVxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuXG4gIHBhZ2UucmVwbGFjZSA9IGZ1bmN0aW9uKHBhdGgsIHN0YXRlLCBpbml0LCBkaXNwYXRjaCkge1xuICAgIHZhciBjdHggPSBuZXcgQ29udGV4dChwYXRoLCBzdGF0ZSksXG4gICAgICBwcmV2ID0gcHJldkNvbnRleHQ7XG4gICAgcHJldkNvbnRleHQgPSBjdHg7XG4gICAgcGFnZS5jdXJyZW50ID0gY3R4LnBhdGg7XG4gICAgY3R4LmluaXQgPSBpbml0O1xuICAgIGN0eC5zYXZlKCk7IC8vIHNhdmUgYmVmb3JlIGRpc3BhdGNoaW5nLCB3aGljaCBtYXkgcmVkaXJlY3RcbiAgICBpZiAoZmFsc2UgIT09IGRpc3BhdGNoKSBwYWdlLmRpc3BhdGNoKGN0eCwgcHJldik7XG4gICAgcmV0dXJuIGN0eDtcbiAgfTtcblxuICAvKipcbiAgICogRGlzcGF0Y2ggdGhlIGdpdmVuIGBjdHhgLlxuICAgKlxuICAgKiBAcGFyYW0ge0NvbnRleHR9IGN0eFxuICAgKiBAYXBpIHByaXZhdGVcbiAgICovXG5cbiAgcGFnZS5kaXNwYXRjaCA9IGZ1bmN0aW9uKGN0eCwgcHJldikge1xuICAgIHZhciBpID0gMCxcbiAgICAgIGogPSAwO1xuXG4gICAgZnVuY3Rpb24gbmV4dEV4aXQoKSB7XG4gICAgICB2YXIgZm4gPSBwYWdlLmV4aXRzW2orK107XG4gICAgICBpZiAoIWZuKSByZXR1cm4gbmV4dEVudGVyKCk7XG4gICAgICBmbihwcmV2LCBuZXh0RXhpdCk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gbmV4dEVudGVyKCkge1xuICAgICAgdmFyIGZuID0gcGFnZS5jYWxsYmFja3NbaSsrXTtcblxuICAgICAgaWYgKGN0eC5wYXRoICE9PSBwYWdlLmN1cnJlbnQpIHtcbiAgICAgICAgY3R4LmhhbmRsZWQgPSBmYWxzZTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgaWYgKCFmbikgcmV0dXJuIHVuaGFuZGxlZChjdHgpO1xuICAgICAgZm4oY3R4LCBuZXh0RW50ZXIpO1xuICAgIH1cblxuICAgIGlmIChwcmV2KSB7XG4gICAgICBuZXh0RXhpdCgpO1xuICAgIH0gZWxzZSB7XG4gICAgICBuZXh0RW50ZXIoKTtcbiAgICB9XG4gIH07XG5cbiAgLyoqXG4gICAqIFVuaGFuZGxlZCBgY3R4YC4gV2hlbiBpdCdzIG5vdCB0aGUgaW5pdGlhbFxuICAgKiBwb3BzdGF0ZSB0aGVuIHJlZGlyZWN0LiBJZiB5b3Ugd2lzaCB0byBoYW5kbGVcbiAgICogNDA0cyBvbiB5b3VyIG93biB1c2UgYHBhZ2UoJyonLCBjYWxsYmFjaylgLlxuICAgKlxuICAgKiBAcGFyYW0ge0NvbnRleHR9IGN0eFxuICAgKiBAYXBpIHByaXZhdGVcbiAgICovXG4gIGZ1bmN0aW9uIHVuaGFuZGxlZChjdHgpIHtcbiAgICBpZiAoY3R4LmhhbmRsZWQpIHJldHVybjtcbiAgICB2YXIgY3VycmVudDtcblxuICAgIGlmIChoYXNoYmFuZykge1xuICAgICAgY3VycmVudCA9IGlzTG9jYXRpb24gJiYgZ2V0QmFzZSgpICsgcGFnZVdpbmRvdy5sb2NhdGlvbi5oYXNoLnJlcGxhY2UoJyMhJywgJycpO1xuICAgIH0gZWxzZSB7XG4gICAgICBjdXJyZW50ID0gaXNMb2NhdGlvbiAmJiBwYWdlV2luZG93LmxvY2F0aW9uLnBhdGhuYW1lICsgcGFnZVdpbmRvdy5sb2NhdGlvbi5zZWFyY2g7XG4gICAgfVxuXG4gICAgaWYgKGN1cnJlbnQgPT09IGN0eC5jYW5vbmljYWxQYXRoKSByZXR1cm47XG4gICAgcGFnZS5zdG9wKCk7XG4gICAgY3R4LmhhbmRsZWQgPSBmYWxzZTtcbiAgICBpc0xvY2F0aW9uICYmIChwYWdlV2luZG93LmxvY2F0aW9uLmhyZWYgPSBjdHguY2Fub25pY2FsUGF0aCk7XG4gIH1cblxuICAvKipcbiAgICogUmVnaXN0ZXIgYW4gZXhpdCByb3V0ZSBvbiBgcGF0aGAgd2l0aFxuICAgKiBjYWxsYmFjayBgZm4oKWAsIHdoaWNoIHdpbGwgYmUgY2FsbGVkXG4gICAqIG9uIHRoZSBwcmV2aW91cyBjb250ZXh0IHdoZW4gYSBuZXdcbiAgICogcGFnZSBpcyB2aXNpdGVkLlxuICAgKi9cbiAgcGFnZS5leGl0ID0gZnVuY3Rpb24ocGF0aCwgZm4pIHtcbiAgICBpZiAodHlwZW9mIHBhdGggPT09ICdmdW5jdGlvbicpIHtcbiAgICAgIHJldHVybiBwYWdlLmV4aXQoJyonLCBwYXRoKTtcbiAgICB9XG5cbiAgICB2YXIgcm91dGUgPSBuZXcgUm91dGUocGF0aCk7XG4gICAgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyArK2kpIHtcbiAgICAgIHBhZ2UuZXhpdHMucHVzaChyb3V0ZS5taWRkbGV3YXJlKGFyZ3VtZW50c1tpXSkpO1xuICAgIH1cbiAgfTtcblxuICAvKipcbiAgICogUmVtb3ZlIFVSTCBlbmNvZGluZyBmcm9tIHRoZSBnaXZlbiBgc3RyYC5cbiAgICogQWNjb21tb2RhdGVzIHdoaXRlc3BhY2UgaW4gYm90aCB4LXd3dy1mb3JtLXVybGVuY29kZWRcbiAgICogYW5kIHJlZ3VsYXIgcGVyY2VudC1lbmNvZGVkIGZvcm0uXG4gICAqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSB2YWwgLSBVUkwgY29tcG9uZW50IHRvIGRlY29kZVxuICAgKi9cbiAgZnVuY3Rpb24gZGVjb2RlVVJMRW5jb2RlZFVSSUNvbXBvbmVudCh2YWwpIHtcbiAgICBpZiAodHlwZW9mIHZhbCAhPT0gJ3N0cmluZycpIHsgcmV0dXJuIHZhbDsgfVxuICAgIHJldHVybiBkZWNvZGVVUkxDb21wb25lbnRzID8gZGVjb2RlVVJJQ29tcG9uZW50KHZhbC5yZXBsYWNlKC9cXCsvZywgJyAnKSkgOiB2YWw7XG4gIH1cblxuICAvKipcbiAgICogSW5pdGlhbGl6ZSBhIG5ldyBcInJlcXVlc3RcIiBgQ29udGV4dGBcbiAgICogd2l0aCB0aGUgZ2l2ZW4gYHBhdGhgIGFuZCBvcHRpb25hbCBpbml0aWFsIGBzdGF0ZWAuXG4gICAqXG4gICAqIEBjb25zdHJ1Y3RvclxuICAgKiBAcGFyYW0ge3N0cmluZ30gcGF0aFxuICAgKiBAcGFyYW0ge09iamVjdD19IHN0YXRlXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGZ1bmN0aW9uIENvbnRleHQocGF0aCwgc3RhdGUpIHtcbiAgICB2YXIgcGFnZUJhc2UgPSBnZXRCYXNlKCk7XG4gICAgaWYgKCcvJyA9PT0gcGF0aFswXSAmJiAwICE9PSBwYXRoLmluZGV4T2YocGFnZUJhc2UpKSBwYXRoID0gcGFnZUJhc2UgKyAoaGFzaGJhbmcgPyAnIyEnIDogJycpICsgcGF0aDtcbiAgICB2YXIgaSA9IHBhdGguaW5kZXhPZignPycpO1xuXG4gICAgdGhpcy5jYW5vbmljYWxQYXRoID0gcGF0aDtcbiAgICB0aGlzLnBhdGggPSBwYXRoLnJlcGxhY2UocGFnZUJhc2UsICcnKSB8fCAnLyc7XG4gICAgaWYgKGhhc2hiYW5nKSB0aGlzLnBhdGggPSB0aGlzLnBhdGgucmVwbGFjZSgnIyEnLCAnJykgfHwgJy8nO1xuXG4gICAgdGhpcy50aXRsZSA9IChoYXNEb2N1bWVudCAmJiBwYWdlV2luZG93LmRvY3VtZW50LnRpdGxlKTtcbiAgICB0aGlzLnN0YXRlID0gc3RhdGUgfHwge307XG4gICAgdGhpcy5zdGF0ZS5wYXRoID0gcGF0aDtcbiAgICB0aGlzLnF1ZXJ5c3RyaW5nID0gfmkgPyBkZWNvZGVVUkxFbmNvZGVkVVJJQ29tcG9uZW50KHBhdGguc2xpY2UoaSArIDEpKSA6ICcnO1xuICAgIHRoaXMucGF0aG5hbWUgPSBkZWNvZGVVUkxFbmNvZGVkVVJJQ29tcG9uZW50KH5pID8gcGF0aC5zbGljZSgwLCBpKSA6IHBhdGgpO1xuICAgIHRoaXMucGFyYW1zID0ge307XG5cbiAgICAvLyBmcmFnbWVudFxuICAgIHRoaXMuaGFzaCA9ICcnO1xuICAgIGlmICghaGFzaGJhbmcpIHtcbiAgICAgIGlmICghfnRoaXMucGF0aC5pbmRleE9mKCcjJykpIHJldHVybjtcbiAgICAgIHZhciBwYXJ0cyA9IHRoaXMucGF0aC5zcGxpdCgnIycpO1xuICAgICAgdGhpcy5wYXRoID0gdGhpcy5wYXRobmFtZSA9IHBhcnRzWzBdO1xuICAgICAgdGhpcy5oYXNoID0gZGVjb2RlVVJMRW5jb2RlZFVSSUNvbXBvbmVudChwYXJ0c1sxXSkgfHwgJyc7XG4gICAgICB0aGlzLnF1ZXJ5c3RyaW5nID0gdGhpcy5xdWVyeXN0cmluZy5zcGxpdCgnIycpWzBdO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBFeHBvc2UgYENvbnRleHRgLlxuICAgKi9cblxuICBwYWdlLkNvbnRleHQgPSBDb250ZXh0O1xuXG4gIC8qKlxuICAgKiBQdXNoIHN0YXRlLlxuICAgKlxuICAgKiBAYXBpIHByaXZhdGVcbiAgICovXG5cbiAgQ29udGV4dC5wcm90b3R5cGUucHVzaFN0YXRlID0gZnVuY3Rpb24oKSB7XG4gICAgcGFnZS5sZW4rKztcbiAgICBpZiAoaGFzSGlzdG9yeSkge1xuICAgICAgICBwYWdlV2luZG93Lmhpc3RvcnkucHVzaFN0YXRlKHRoaXMuc3RhdGUsIHRoaXMudGl0bGUsXG4gICAgICAgICAgaGFzaGJhbmcgJiYgdGhpcy5wYXRoICE9PSAnLycgPyAnIyEnICsgdGhpcy5wYXRoIDogdGhpcy5jYW5vbmljYWxQYXRoKTtcbiAgICB9XG4gIH07XG5cbiAgLyoqXG4gICAqIFNhdmUgdGhlIGNvbnRleHQgc3RhdGUuXG4gICAqXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIENvbnRleHQucHJvdG90eXBlLnNhdmUgPSBmdW5jdGlvbigpIHtcbiAgICBpZiAoaGFzSGlzdG9yeSAmJiBwYWdlV2luZG93LmxvY2F0aW9uLnByb3RvY29sICE9PSAnZmlsZTonKSB7XG4gICAgICAgIHBhZ2VXaW5kb3cuaGlzdG9yeS5yZXBsYWNlU3RhdGUodGhpcy5zdGF0ZSwgdGhpcy50aXRsZSxcbiAgICAgICAgICBoYXNoYmFuZyAmJiB0aGlzLnBhdGggIT09ICcvJyA/ICcjIScgKyB0aGlzLnBhdGggOiB0aGlzLmNhbm9uaWNhbFBhdGgpO1xuICAgIH1cbiAgfTtcblxuICAvKipcbiAgICogSW5pdGlhbGl6ZSBgUm91dGVgIHdpdGggdGhlIGdpdmVuIEhUVFAgYHBhdGhgLFxuICAgKiBhbmQgYW4gYXJyYXkgb2YgYGNhbGxiYWNrc2AgYW5kIGBvcHRpb25zYC5cbiAgICpcbiAgICogT3B0aW9uczpcbiAgICpcbiAgICogICAtIGBzZW5zaXRpdmVgICAgIGVuYWJsZSBjYXNlLXNlbnNpdGl2ZSByb3V0ZXNcbiAgICogICAtIGBzdHJpY3RgICAgICAgIGVuYWJsZSBzdHJpY3QgbWF0Y2hpbmcgZm9yIHRyYWlsaW5nIHNsYXNoZXNcbiAgICpcbiAgICogQGNvbnN0cnVjdG9yXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBwYXRoXG4gICAqIEBwYXJhbSB7T2JqZWN0PX0gb3B0aW9uc1xuICAgKiBAYXBpIHByaXZhdGVcbiAgICovXG5cbiAgZnVuY3Rpb24gUm91dGUocGF0aCwgb3B0aW9ucykge1xuICAgIG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9O1xuICAgIG9wdGlvbnMuc3RyaWN0ID0gb3B0aW9ucy5zdHJpY3QgfHwgc3RyaWN0O1xuICAgIHRoaXMucGF0aCA9IChwYXRoID09PSAnKicpID8gJyguKiknIDogcGF0aDtcbiAgICB0aGlzLm1ldGhvZCA9ICdHRVQnO1xuICAgIHRoaXMucmVnZXhwID0gcGF0aFRvUmVnZXhwXzEodGhpcy5wYXRoLFxuICAgICAgdGhpcy5rZXlzID0gW10sXG4gICAgICBvcHRpb25zKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBFeHBvc2UgYFJvdXRlYC5cbiAgICovXG5cbiAgcGFnZS5Sb3V0ZSA9IFJvdXRlO1xuXG4gIC8qKlxuICAgKiBSZXR1cm4gcm91dGUgbWlkZGxld2FyZSB3aXRoXG4gICAqIHRoZSBnaXZlbiBjYWxsYmFjayBgZm4oKWAuXG4gICAqXG4gICAqIEBwYXJhbSB7RnVuY3Rpb259IGZuXG4gICAqIEByZXR1cm4ge0Z1bmN0aW9ufVxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBSb3V0ZS5wcm90b3R5cGUubWlkZGxld2FyZSA9IGZ1bmN0aW9uKGZuKSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgIHJldHVybiBmdW5jdGlvbihjdHgsIG5leHQpIHtcbiAgICAgIGlmIChzZWxmLm1hdGNoKGN0eC5wYXRoLCBjdHgucGFyYW1zKSkgcmV0dXJuIGZuKGN0eCwgbmV4dCk7XG4gICAgICBuZXh0KCk7XG4gICAgfTtcbiAgfTtcblxuICAvKipcbiAgICogQ2hlY2sgaWYgdGhpcyByb3V0ZSBtYXRjaGVzIGBwYXRoYCwgaWYgc29cbiAgICogcG9wdWxhdGUgYHBhcmFtc2AuXG4gICAqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBwYXRoXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBwYXJhbXNcbiAgICogQHJldHVybiB7Ym9vbGVhbn1cbiAgICogQGFwaSBwcml2YXRlXG4gICAqL1xuXG4gIFJvdXRlLnByb3RvdHlwZS5tYXRjaCA9IGZ1bmN0aW9uKHBhdGgsIHBhcmFtcykge1xuICAgIHZhciBrZXlzID0gdGhpcy5rZXlzLFxuICAgICAgcXNJbmRleCA9IHBhdGguaW5kZXhPZignPycpLFxuICAgICAgcGF0aG5hbWUgPSB+cXNJbmRleCA/IHBhdGguc2xpY2UoMCwgcXNJbmRleCkgOiBwYXRoLFxuICAgICAgbSA9IHRoaXMucmVnZXhwLmV4ZWMoZGVjb2RlVVJJQ29tcG9uZW50KHBhdGhuYW1lKSk7XG5cbiAgICBpZiAoIW0pIHJldHVybiBmYWxzZTtcblxuICAgIGZvciAodmFyIGkgPSAxLCBsZW4gPSBtLmxlbmd0aDsgaSA8IGxlbjsgKytpKSB7XG4gICAgICB2YXIga2V5ID0ga2V5c1tpIC0gMV07XG4gICAgICB2YXIgdmFsID0gZGVjb2RlVVJMRW5jb2RlZFVSSUNvbXBvbmVudChtW2ldKTtcbiAgICAgIGlmICh2YWwgIT09IHVuZGVmaW5lZCB8fCAhKGhhc093blByb3BlcnR5LmNhbGwocGFyYW1zLCBrZXkubmFtZSkpKSB7XG4gICAgICAgIHBhcmFtc1trZXkubmFtZV0gPSB2YWw7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHRydWU7XG4gIH07XG5cblxuICAvKipcbiAgICogSGFuZGxlIFwicG9wdWxhdGVcIiBldmVudHMuXG4gICAqL1xuXG4gIHZhciBvbnBvcHN0YXRlID0gKGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgbG9hZGVkID0gZmFsc2U7XG4gICAgaWYgKCAhIGhhc1dpbmRvdyApIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgaWYgKGhhc0RvY3VtZW50ICYmIGRvY3VtZW50LnJlYWR5U3RhdGUgPT09ICdjb21wbGV0ZScpIHtcbiAgICAgIGxvYWRlZCA9IHRydWU7XG4gICAgfSBlbHNlIHtcbiAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdsb2FkJywgZnVuY3Rpb24oKSB7XG4gICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbG9hZGVkID0gdHJ1ZTtcbiAgICAgICAgfSwgMCk7XG4gICAgICB9KTtcbiAgICB9XG4gICAgcmV0dXJuIGZ1bmN0aW9uIG9ucG9wc3RhdGUoZSkge1xuICAgICAgaWYgKCFsb2FkZWQpIHJldHVybjtcbiAgICAgIGlmIChlLnN0YXRlKSB7XG4gICAgICAgIHZhciBwYXRoID0gZS5zdGF0ZS5wYXRoO1xuICAgICAgICBwYWdlLnJlcGxhY2UocGF0aCwgZS5zdGF0ZSk7XG4gICAgICB9IGVsc2UgaWYgKGlzTG9jYXRpb24pIHtcbiAgICAgICAgdmFyIGxvYyA9IHBhZ2VXaW5kb3cubG9jYXRpb247XG4gICAgICAgIHBhZ2Uuc2hvdyhsb2MucGF0aG5hbWUgKyBsb2MuaGFzaCwgdW5kZWZpbmVkLCB1bmRlZmluZWQsIGZhbHNlKTtcbiAgICAgIH1cbiAgICB9O1xuICB9KSgpO1xuICAvKipcbiAgICogSGFuZGxlIFwiY2xpY2tcIiBldmVudHMuXG4gICAqL1xuXG4gIC8qIGpzaGludCArVzA1NCAqL1xuICBmdW5jdGlvbiBvbmNsaWNrKGUpIHtcbiAgICBpZiAoMSAhPT0gd2hpY2goZSkpIHJldHVybjtcblxuICAgIGlmIChlLm1ldGFLZXkgfHwgZS5jdHJsS2V5IHx8IGUuc2hpZnRLZXkpIHJldHVybjtcbiAgICBpZiAoZS5kZWZhdWx0UHJldmVudGVkKSByZXR1cm47XG5cbiAgICAvLyBlbnN1cmUgbGlua1xuICAgIC8vIHVzZSBzaGFkb3cgZG9tIHdoZW4gYXZhaWxhYmxlIGlmIG5vdCwgZmFsbCBiYWNrIHRvIGNvbXBvc2VkUGF0aCgpIGZvciBicm93c2VycyB0aGF0IG9ubHkgaGF2ZSBzaGFkeVxuICAgIHZhciBlbCA9IGUudGFyZ2V0O1xuICAgIHZhciBldmVudFBhdGggPSBlLnBhdGggfHwgKGUuY29tcG9zZWRQYXRoID8gZS5jb21wb3NlZFBhdGgoKSA6IG51bGwpO1xuXG4gICAgaWYoZXZlbnRQYXRoKSB7XG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGV2ZW50UGF0aC5sZW5ndGg7IGkrKykge1xuICAgICAgICBpZiAoIWV2ZW50UGF0aFtpXS5ub2RlTmFtZSkgY29udGludWU7XG4gICAgICAgIGlmIChldmVudFBhdGhbaV0ubm9kZU5hbWUudG9VcHBlckNhc2UoKSAhPT0gJ0EnKSBjb250aW51ZTtcbiAgICAgICAgaWYgKCFldmVudFBhdGhbaV0uaHJlZikgY29udGludWU7XG5cbiAgICAgICAgZWwgPSBldmVudFBhdGhbaV07XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cbiAgICAvLyBjb250aW51ZSBlbnN1cmUgbGlua1xuICAgIC8vIGVsLm5vZGVOYW1lIGZvciBzdmcgbGlua3MgYXJlICdhJyBpbnN0ZWFkIG9mICdBJ1xuICAgIHdoaWxlIChlbCAmJiAnQScgIT09IGVsLm5vZGVOYW1lLnRvVXBwZXJDYXNlKCkpIGVsID0gZWwucGFyZW50Tm9kZTtcbiAgICBpZiAoIWVsIHx8ICdBJyAhPT0gZWwubm9kZU5hbWUudG9VcHBlckNhc2UoKSkgcmV0dXJuO1xuXG4gICAgLy8gY2hlY2sgaWYgbGluayBpcyBpbnNpZGUgYW4gc3ZnXG4gICAgLy8gaW4gdGhpcyBjYXNlLCBib3RoIGhyZWYgYW5kIHRhcmdldCBhcmUgYWx3YXlzIGluc2lkZSBhbiBvYmplY3RcbiAgICB2YXIgc3ZnID0gKHR5cGVvZiBlbC5ocmVmID09PSAnb2JqZWN0JykgJiYgZWwuaHJlZi5jb25zdHJ1Y3Rvci5uYW1lID09PSAnU1ZHQW5pbWF0ZWRTdHJpbmcnO1xuXG4gICAgLy8gSWdub3JlIGlmIHRhZyBoYXNcbiAgICAvLyAxLiBcImRvd25sb2FkXCIgYXR0cmlidXRlXG4gICAgLy8gMi4gcmVsPVwiZXh0ZXJuYWxcIiBhdHRyaWJ1dGVcbiAgICBpZiAoZWwuaGFzQXR0cmlidXRlKCdkb3dubG9hZCcpIHx8IGVsLmdldEF0dHJpYnV0ZSgncmVsJykgPT09ICdleHRlcm5hbCcpIHJldHVybjtcblxuICAgIC8vIGVuc3VyZSBub24taGFzaCBmb3IgdGhlIHNhbWUgcGF0aFxuICAgIHZhciBsaW5rID0gZWwuZ2V0QXR0cmlidXRlKCdocmVmJyk7XG4gICAgaWYoIWhhc2hiYW5nICYmIHNhbWVQYXRoKGVsKSAmJiAoZWwuaGFzaCB8fCAnIycgPT09IGxpbmspKSByZXR1cm47XG5cbiAgICAvLyBDaGVjayBmb3IgbWFpbHRvOiBpbiB0aGUgaHJlZlxuICAgIGlmIChsaW5rICYmIGxpbmsuaW5kZXhPZignbWFpbHRvOicpID4gLTEpIHJldHVybjtcblxuICAgIC8vIGNoZWNrIHRhcmdldFxuICAgIC8vIHN2ZyB0YXJnZXQgaXMgYW4gb2JqZWN0IGFuZCBpdHMgZGVzaXJlZCB2YWx1ZSBpcyBpbiAuYmFzZVZhbCBwcm9wZXJ0eVxuICAgIGlmIChzdmcgPyBlbC50YXJnZXQuYmFzZVZhbCA6IGVsLnRhcmdldCkgcmV0dXJuO1xuXG4gICAgLy8geC1vcmlnaW5cbiAgICAvLyBub3RlOiBzdmcgbGlua3MgdGhhdCBhcmUgbm90IHJlbGF0aXZlIGRvbid0IGNhbGwgY2xpY2sgZXZlbnRzIChhbmQgc2tpcCBwYWdlLmpzKVxuICAgIC8vIGNvbnNlcXVlbnRseSwgYWxsIHN2ZyBsaW5rcyB0ZXN0ZWQgaW5zaWRlIHBhZ2UuanMgYXJlIHJlbGF0aXZlIGFuZCBpbiB0aGUgc2FtZSBvcmlnaW5cbiAgICBpZiAoIXN2ZyAmJiAhc2FtZU9yaWdpbihlbC5ocmVmKSkgcmV0dXJuO1xuXG4gICAgLy8gcmVidWlsZCBwYXRoXG4gICAgLy8gVGhlcmUgYXJlbid0IC5wYXRobmFtZSBhbmQgLnNlYXJjaCBwcm9wZXJ0aWVzIGluIHN2ZyBsaW5rcywgc28gd2UgdXNlIGhyZWZcbiAgICAvLyBBbHNvLCBzdmcgaHJlZiBpcyBhbiBvYmplY3QgYW5kIGl0cyBkZXNpcmVkIHZhbHVlIGlzIGluIC5iYXNlVmFsIHByb3BlcnR5XG4gICAgdmFyIHBhdGggPSBzdmcgPyBlbC5ocmVmLmJhc2VWYWwgOiAoZWwucGF0aG5hbWUgKyBlbC5zZWFyY2ggKyAoZWwuaGFzaCB8fCAnJykpO1xuXG4gICAgcGF0aCA9IHBhdGhbMF0gIT09ICcvJyA/ICcvJyArIHBhdGggOiBwYXRoO1xuXG4gICAgLy8gc3RyaXAgbGVhZGluZyBcIi9bZHJpdmUgbGV0dGVyXTpcIiBvbiBOVy5qcyBvbiBXaW5kb3dzXG4gICAgaWYgKGhhc1Byb2Nlc3MgJiYgcGF0aC5tYXRjaCgvXlxcL1thLXpBLVpdOlxcLy8pKSB7XG4gICAgICBwYXRoID0gcGF0aC5yZXBsYWNlKC9eXFwvW2EtekEtWl06XFwvLywgJy8nKTtcbiAgICB9XG5cbiAgICAvLyBzYW1lIHBhZ2VcbiAgICB2YXIgb3JpZyA9IHBhdGg7XG4gICAgdmFyIHBhZ2VCYXNlID0gZ2V0QmFzZSgpO1xuXG4gICAgaWYgKHBhdGguaW5kZXhPZihwYWdlQmFzZSkgPT09IDApIHtcbiAgICAgIHBhdGggPSBwYXRoLnN1YnN0cihiYXNlLmxlbmd0aCk7XG4gICAgfVxuXG4gICAgaWYgKGhhc2hiYW5nKSBwYXRoID0gcGF0aC5yZXBsYWNlKCcjIScsICcnKTtcblxuICAgIGlmIChwYWdlQmFzZSAmJiBvcmlnID09PSBwYXRoKSByZXR1cm47XG5cbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgcGFnZS5zaG93KG9yaWcpO1xuICB9XG5cbiAgLyoqXG4gICAqIEV2ZW50IGJ1dHRvbi5cbiAgICovXG5cbiAgZnVuY3Rpb24gd2hpY2goZSkge1xuICAgIGUgPSBlIHx8IChoYXNXaW5kb3cgJiYgd2luZG93LmV2ZW50KTtcbiAgICByZXR1cm4gbnVsbCA9PSBlLndoaWNoID8gZS5idXR0b24gOiBlLndoaWNoO1xuICB9XG5cbiAgLyoqXG4gICAqIENvbnZlcnQgdG8gYSBVUkwgb2JqZWN0XG4gICAqL1xuICBmdW5jdGlvbiB0b1VSTChocmVmKSB7XG4gICAgaWYodHlwZW9mIFVSTCA9PT0gJ2Z1bmN0aW9uJyAmJiBpc0xvY2F0aW9uKSB7XG4gICAgICByZXR1cm4gbmV3IFVSTChocmVmLCBsb2NhdGlvbi50b1N0cmluZygpKTtcbiAgICB9IGVsc2UgaWYgKGhhc0RvY3VtZW50KSB7XG4gICAgICB2YXIgYW5jID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYScpO1xuICAgICAgYW5jLmhyZWYgPSBocmVmO1xuICAgICAgcmV0dXJuIGFuYztcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogQ2hlY2sgaWYgYGhyZWZgIGlzIHRoZSBzYW1lIG9yaWdpbi5cbiAgICovXG5cbiAgZnVuY3Rpb24gc2FtZU9yaWdpbihocmVmKSB7XG4gICAgaWYoIWhyZWYgfHwgIWlzTG9jYXRpb24pIHJldHVybiBmYWxzZTtcbiAgICB2YXIgdXJsID0gdG9VUkwoaHJlZik7XG5cbiAgICB2YXIgbG9jID0gcGFnZVdpbmRvdy5sb2NhdGlvbjtcbiAgICByZXR1cm4gbG9jLnByb3RvY29sID09PSB1cmwucHJvdG9jb2wgJiZcbiAgICAgIGxvYy5ob3N0bmFtZSA9PT0gdXJsLmhvc3RuYW1lICYmXG4gICAgICBsb2MucG9ydCA9PT0gdXJsLnBvcnQ7XG4gIH1cblxuICBmdW5jdGlvbiBzYW1lUGF0aCh1cmwpIHtcbiAgICBpZighaXNMb2NhdGlvbikgcmV0dXJuIGZhbHNlO1xuICAgIHZhciBsb2MgPSBwYWdlV2luZG93LmxvY2F0aW9uO1xuICAgIHJldHVybiB1cmwucGF0aG5hbWUgPT09IGxvYy5wYXRobmFtZSAmJlxuICAgICAgdXJsLnNlYXJjaCA9PT0gbG9jLnNlYXJjaDtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXRzIHRoZSBgYmFzZWAsIHdoaWNoIGRlcGVuZHMgb24gd2hldGhlciB3ZSBhcmUgdXNpbmcgSGlzdG9yeSBvclxuICAgKiBoYXNoYmFuZyByb3V0aW5nLlxuICAgKi9cbiAgZnVuY3Rpb24gZ2V0QmFzZSgpIHtcbiAgICBpZighIWJhc2UpIHJldHVybiBiYXNlO1xuICAgIHZhciBsb2MgPSBoYXNXaW5kb3cgJiYgcGFnZVdpbmRvdyAmJiBwYWdlV2luZG93LmxvY2F0aW9uO1xuICAgIHJldHVybiAoaGFzV2luZG93ICYmIGhhc2hiYW5nICYmIGxvYyAmJiBsb2MucHJvdG9jb2wgPT09ICdmaWxlOicpID8gbG9jLnBhdGhuYW1lIDogYmFzZTtcbiAgfVxuXG4gIHBhZ2Uuc2FtZU9yaWdpbiA9IHNhbWVPcmlnaW47XG5cbnJldHVybiBwYWdlX2pzO1xuXG59KSkpO1xuIiwiLy8gc2hpbSBmb3IgdXNpbmcgcHJvY2VzcyBpbiBicm93c2VyXG52YXIgcHJvY2VzcyA9IG1vZHVsZS5leHBvcnRzID0ge307XG5cbi8vIGNhY2hlZCBmcm9tIHdoYXRldmVyIGdsb2JhbCBpcyBwcmVzZW50IHNvIHRoYXQgdGVzdCBydW5uZXJzIHRoYXQgc3R1YiBpdFxuLy8gZG9uJ3QgYnJlYWsgdGhpbmdzLiAgQnV0IHdlIG5lZWQgdG8gd3JhcCBpdCBpbiBhIHRyeSBjYXRjaCBpbiBjYXNlIGl0IGlzXG4vLyB3cmFwcGVkIGluIHN0cmljdCBtb2RlIGNvZGUgd2hpY2ggZG9lc24ndCBkZWZpbmUgYW55IGdsb2JhbHMuICBJdCdzIGluc2lkZSBhXG4vLyBmdW5jdGlvbiBiZWNhdXNlIHRyeS9jYXRjaGVzIGRlb3B0aW1pemUgaW4gY2VydGFpbiBlbmdpbmVzLlxuXG52YXIgY2FjaGVkU2V0VGltZW91dDtcbnZhciBjYWNoZWRDbGVhclRpbWVvdXQ7XG5cbmZ1bmN0aW9uIGRlZmF1bHRTZXRUaW1vdXQoKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdzZXRUaW1lb3V0IGhhcyBub3QgYmVlbiBkZWZpbmVkJyk7XG59XG5mdW5jdGlvbiBkZWZhdWx0Q2xlYXJUaW1lb3V0ICgpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ2NsZWFyVGltZW91dCBoYXMgbm90IGJlZW4gZGVmaW5lZCcpO1xufVxuKGZ1bmN0aW9uICgpIHtcbiAgICB0cnkge1xuICAgICAgICBpZiAodHlwZW9mIHNldFRpbWVvdXQgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgIGNhY2hlZFNldFRpbWVvdXQgPSBzZXRUaW1lb3V0O1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY2FjaGVkU2V0VGltZW91dCA9IGRlZmF1bHRTZXRUaW1vdXQ7XG4gICAgICAgIH1cbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgIGNhY2hlZFNldFRpbWVvdXQgPSBkZWZhdWx0U2V0VGltb3V0O1xuICAgIH1cbiAgICB0cnkge1xuICAgICAgICBpZiAodHlwZW9mIGNsZWFyVGltZW91dCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgY2FjaGVkQ2xlYXJUaW1lb3V0ID0gY2xlYXJUaW1lb3V0O1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY2FjaGVkQ2xlYXJUaW1lb3V0ID0gZGVmYXVsdENsZWFyVGltZW91dDtcbiAgICAgICAgfVxuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgY2FjaGVkQ2xlYXJUaW1lb3V0ID0gZGVmYXVsdENsZWFyVGltZW91dDtcbiAgICB9XG59ICgpKVxuZnVuY3Rpb24gcnVuVGltZW91dChmdW4pIHtcbiAgICBpZiAoY2FjaGVkU2V0VGltZW91dCA9PT0gc2V0VGltZW91dCkge1xuICAgICAgICAvL25vcm1hbCBlbnZpcm9tZW50cyBpbiBzYW5lIHNpdHVhdGlvbnNcbiAgICAgICAgcmV0dXJuIHNldFRpbWVvdXQoZnVuLCAwKTtcbiAgICB9XG4gICAgLy8gaWYgc2V0VGltZW91dCB3YXNuJ3QgYXZhaWxhYmxlIGJ1dCB3YXMgbGF0dGVyIGRlZmluZWRcbiAgICBpZiAoKGNhY2hlZFNldFRpbWVvdXQgPT09IGRlZmF1bHRTZXRUaW1vdXQgfHwgIWNhY2hlZFNldFRpbWVvdXQpICYmIHNldFRpbWVvdXQpIHtcbiAgICAgICAgY2FjaGVkU2V0VGltZW91dCA9IHNldFRpbWVvdXQ7XG4gICAgICAgIHJldHVybiBzZXRUaW1lb3V0KGZ1biwgMCk7XG4gICAgfVxuICAgIHRyeSB7XG4gICAgICAgIC8vIHdoZW4gd2hlbiBzb21lYm9keSBoYXMgc2NyZXdlZCB3aXRoIHNldFRpbWVvdXQgYnV0IG5vIEkuRS4gbWFkZG5lc3NcbiAgICAgICAgcmV0dXJuIGNhY2hlZFNldFRpbWVvdXQoZnVuLCAwKTtcbiAgICB9IGNhdGNoKGUpe1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgLy8gV2hlbiB3ZSBhcmUgaW4gSS5FLiBidXQgdGhlIHNjcmlwdCBoYXMgYmVlbiBldmFsZWQgc28gSS5FLiBkb2Vzbid0IHRydXN0IHRoZSBnbG9iYWwgb2JqZWN0IHdoZW4gY2FsbGVkIG5vcm1hbGx5XG4gICAgICAgICAgICByZXR1cm4gY2FjaGVkU2V0VGltZW91dC5jYWxsKG51bGwsIGZ1biwgMCk7XG4gICAgICAgIH0gY2F0Y2goZSl7XG4gICAgICAgICAgICAvLyBzYW1lIGFzIGFib3ZlIGJ1dCB3aGVuIGl0J3MgYSB2ZXJzaW9uIG9mIEkuRS4gdGhhdCBtdXN0IGhhdmUgdGhlIGdsb2JhbCBvYmplY3QgZm9yICd0aGlzJywgaG9wZnVsbHkgb3VyIGNvbnRleHQgY29ycmVjdCBvdGhlcndpc2UgaXQgd2lsbCB0aHJvdyBhIGdsb2JhbCBlcnJvclxuICAgICAgICAgICAgcmV0dXJuIGNhY2hlZFNldFRpbWVvdXQuY2FsbCh0aGlzLCBmdW4sIDApO1xuICAgICAgICB9XG4gICAgfVxuXG5cbn1cbmZ1bmN0aW9uIHJ1bkNsZWFyVGltZW91dChtYXJrZXIpIHtcbiAgICBpZiAoY2FjaGVkQ2xlYXJUaW1lb3V0ID09PSBjbGVhclRpbWVvdXQpIHtcbiAgICAgICAgLy9ub3JtYWwgZW52aXJvbWVudHMgaW4gc2FuZSBzaXR1YXRpb25zXG4gICAgICAgIHJldHVybiBjbGVhclRpbWVvdXQobWFya2VyKTtcbiAgICB9XG4gICAgLy8gaWYgY2xlYXJUaW1lb3V0IHdhc24ndCBhdmFpbGFibGUgYnV0IHdhcyBsYXR0ZXIgZGVmaW5lZFxuICAgIGlmICgoY2FjaGVkQ2xlYXJUaW1lb3V0ID09PSBkZWZhdWx0Q2xlYXJUaW1lb3V0IHx8ICFjYWNoZWRDbGVhclRpbWVvdXQpICYmIGNsZWFyVGltZW91dCkge1xuICAgICAgICBjYWNoZWRDbGVhclRpbWVvdXQgPSBjbGVhclRpbWVvdXQ7XG4gICAgICAgIHJldHVybiBjbGVhclRpbWVvdXQobWFya2VyKTtcbiAgICB9XG4gICAgdHJ5IHtcbiAgICAgICAgLy8gd2hlbiB3aGVuIHNvbWVib2R5IGhhcyBzY3Jld2VkIHdpdGggc2V0VGltZW91dCBidXQgbm8gSS5FLiBtYWRkbmVzc1xuICAgICAgICByZXR1cm4gY2FjaGVkQ2xlYXJUaW1lb3V0KG1hcmtlcik7XG4gICAgfSBjYXRjaCAoZSl7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICAvLyBXaGVuIHdlIGFyZSBpbiBJLkUuIGJ1dCB0aGUgc2NyaXB0IGhhcyBiZWVuIGV2YWxlZCBzbyBJLkUuIGRvZXNuJ3QgIHRydXN0IHRoZSBnbG9iYWwgb2JqZWN0IHdoZW4gY2FsbGVkIG5vcm1hbGx5XG4gICAgICAgICAgICByZXR1cm4gY2FjaGVkQ2xlYXJUaW1lb3V0LmNhbGwobnVsbCwgbWFya2VyKTtcbiAgICAgICAgfSBjYXRjaCAoZSl7XG4gICAgICAgICAgICAvLyBzYW1lIGFzIGFib3ZlIGJ1dCB3aGVuIGl0J3MgYSB2ZXJzaW9uIG9mIEkuRS4gdGhhdCBtdXN0IGhhdmUgdGhlIGdsb2JhbCBvYmplY3QgZm9yICd0aGlzJywgaG9wZnVsbHkgb3VyIGNvbnRleHQgY29ycmVjdCBvdGhlcndpc2UgaXQgd2lsbCB0aHJvdyBhIGdsb2JhbCBlcnJvci5cbiAgICAgICAgICAgIC8vIFNvbWUgdmVyc2lvbnMgb2YgSS5FLiBoYXZlIGRpZmZlcmVudCBydWxlcyBmb3IgY2xlYXJUaW1lb3V0IHZzIHNldFRpbWVvdXRcbiAgICAgICAgICAgIHJldHVybiBjYWNoZWRDbGVhclRpbWVvdXQuY2FsbCh0aGlzLCBtYXJrZXIpO1xuICAgICAgICB9XG4gICAgfVxuXG5cblxufVxudmFyIHF1ZXVlID0gW107XG52YXIgZHJhaW5pbmcgPSBmYWxzZTtcbnZhciBjdXJyZW50UXVldWU7XG52YXIgcXVldWVJbmRleCA9IC0xO1xuXG5mdW5jdGlvbiBjbGVhblVwTmV4dFRpY2soKSB7XG4gICAgaWYgKCFkcmFpbmluZyB8fCAhY3VycmVudFF1ZXVlKSB7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG4gICAgZHJhaW5pbmcgPSBmYWxzZTtcbiAgICBpZiAoY3VycmVudFF1ZXVlLmxlbmd0aCkge1xuICAgICAgICBxdWV1ZSA9IGN1cnJlbnRRdWV1ZS5jb25jYXQocXVldWUpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHF1ZXVlSW5kZXggPSAtMTtcbiAgICB9XG4gICAgaWYgKHF1ZXVlLmxlbmd0aCkge1xuICAgICAgICBkcmFpblF1ZXVlKCk7XG4gICAgfVxufVxuXG5mdW5jdGlvbiBkcmFpblF1ZXVlKCkge1xuICAgIGlmIChkcmFpbmluZykge1xuICAgICAgICByZXR1cm47XG4gICAgfVxuICAgIHZhciB0aW1lb3V0ID0gcnVuVGltZW91dChjbGVhblVwTmV4dFRpY2spO1xuICAgIGRyYWluaW5nID0gdHJ1ZTtcblxuICAgIHZhciBsZW4gPSBxdWV1ZS5sZW5ndGg7XG4gICAgd2hpbGUobGVuKSB7XG4gICAgICAgIGN1cnJlbnRRdWV1ZSA9IHF1ZXVlO1xuICAgICAgICBxdWV1ZSA9IFtdO1xuICAgICAgICB3aGlsZSAoKytxdWV1ZUluZGV4IDwgbGVuKSB7XG4gICAgICAgICAgICBpZiAoY3VycmVudFF1ZXVlKSB7XG4gICAgICAgICAgICAgICAgY3VycmVudFF1ZXVlW3F1ZXVlSW5kZXhdLnJ1bigpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHF1ZXVlSW5kZXggPSAtMTtcbiAgICAgICAgbGVuID0gcXVldWUubGVuZ3RoO1xuICAgIH1cbiAgICBjdXJyZW50UXVldWUgPSBudWxsO1xuICAgIGRyYWluaW5nID0gZmFsc2U7XG4gICAgcnVuQ2xlYXJUaW1lb3V0KHRpbWVvdXQpO1xufVxuXG5wcm9jZXNzLm5leHRUaWNrID0gZnVuY3Rpb24gKGZ1bikge1xuICAgIHZhciBhcmdzID0gbmV3IEFycmF5KGFyZ3VtZW50cy5sZW5ndGggLSAxKTtcbiAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA+IDEpIHtcbiAgICAgICAgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGFyZ3NbaSAtIDFdID0gYXJndW1lbnRzW2ldO1xuICAgICAgICB9XG4gICAgfVxuICAgIHF1ZXVlLnB1c2gobmV3IEl0ZW0oZnVuLCBhcmdzKSk7XG4gICAgaWYgKHF1ZXVlLmxlbmd0aCA9PT0gMSAmJiAhZHJhaW5pbmcpIHtcbiAgICAgICAgcnVuVGltZW91dChkcmFpblF1ZXVlKTtcbiAgICB9XG59O1xuXG4vLyB2OCBsaWtlcyBwcmVkaWN0aWJsZSBvYmplY3RzXG5mdW5jdGlvbiBJdGVtKGZ1biwgYXJyYXkpIHtcbiAgICB0aGlzLmZ1biA9IGZ1bjtcbiAgICB0aGlzLmFycmF5ID0gYXJyYXk7XG59XG5JdGVtLnByb3RvdHlwZS5ydW4gPSBmdW5jdGlvbiAoKSB7XG4gICAgdGhpcy5mdW4uYXBwbHkobnVsbCwgdGhpcy5hcnJheSk7XG59O1xucHJvY2Vzcy50aXRsZSA9ICdicm93c2VyJztcbnByb2Nlc3MuYnJvd3NlciA9IHRydWU7XG5wcm9jZXNzLmVudiA9IHt9O1xucHJvY2Vzcy5hcmd2ID0gW107XG5wcm9jZXNzLnZlcnNpb24gPSAnJzsgLy8gZW1wdHkgc3RyaW5nIHRvIGF2b2lkIHJlZ2V4cCBpc3N1ZXNcbnByb2Nlc3MudmVyc2lvbnMgPSB7fTtcblxuZnVuY3Rpb24gbm9vcCgpIHt9XG5cbnByb2Nlc3Mub24gPSBub29wO1xucHJvY2Vzcy5hZGRMaXN0ZW5lciA9IG5vb3A7XG5wcm9jZXNzLm9uY2UgPSBub29wO1xucHJvY2Vzcy5vZmYgPSBub29wO1xucHJvY2Vzcy5yZW1vdmVMaXN0ZW5lciA9IG5vb3A7XG5wcm9jZXNzLnJlbW92ZUFsbExpc3RlbmVycyA9IG5vb3A7XG5wcm9jZXNzLmVtaXQgPSBub29wO1xucHJvY2Vzcy5wcmVwZW5kTGlzdGVuZXIgPSBub29wO1xucHJvY2Vzcy5wcmVwZW5kT25jZUxpc3RlbmVyID0gbm9vcDtcblxucHJvY2Vzcy5saXN0ZW5lcnMgPSBmdW5jdGlvbiAobmFtZSkgeyByZXR1cm4gW10gfVxuXG5wcm9jZXNzLmJpbmRpbmcgPSBmdW5jdGlvbiAobmFtZSkge1xuICAgIHRocm93IG5ldyBFcnJvcigncHJvY2Vzcy5iaW5kaW5nIGlzIG5vdCBzdXBwb3J0ZWQnKTtcbn07XG5cbnByb2Nlc3MuY3dkID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gJy8nIH07XG5wcm9jZXNzLmNoZGlyID0gZnVuY3Rpb24gKGRpcikge1xuICAgIHRocm93IG5ldyBFcnJvcigncHJvY2Vzcy5jaGRpciBpcyBub3Qgc3VwcG9ydGVkJyk7XG59O1xucHJvY2Vzcy51bWFzayA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gMDsgfTtcbiIsIihmdW5jdGlvbiAoZ2xvYmFsLCBmYWN0b3J5KSB7XG5cdHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0JyAmJiB0eXBlb2YgbW9kdWxlICE9PSAndW5kZWZpbmVkJyA/IGZhY3RvcnkoZXhwb3J0cywgcmVxdWlyZSgncmlvdCcpKSA6XG5cdHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZCA/IGRlZmluZShbJ2V4cG9ydHMnLCAncmlvdCddLCBmYWN0b3J5KSA6XG5cdChmYWN0b3J5KChnbG9iYWwucmlvdEhvdFJlbG9hZCA9IGdsb2JhbC5yaW90SG90UmVsb2FkIHx8IHt9KSxnbG9iYWwucmlvdCkpO1xufSh0aGlzLCAoZnVuY3Rpb24gKGV4cG9ydHMscmlvdCkgeyAndXNlIHN0cmljdCc7XG5cbnZhciBub25TdGF0ZSA9ICdpc01vdW50ZWQgb3B0cycuc3BsaXQoJyAnKTtcblxuZnVuY3Rpb24gcmVsb2FkKG5hbWUpIHtcbiAgcmlvdC51dGlsLnN0eWxlTWFuYWdlci5pbmplY3QoKTtcblxuICB2YXIgZWxlbXMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKChuYW1lICsgXCIsIFtkYXRhLWlzPVwiICsgbmFtZSArIFwiXVwiKSk7XG4gIHZhciB0YWdzID0gW107XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBlbGVtcy5sZW5ndGg7IGkrKykge1xuICAgIHZhciBlbCA9IGVsZW1zW2ldLCBvbGRUYWcgPSBlbC5fdGFnLCB2O1xuICAgIHJlbG9hZC50cmlnZ2VyKCdiZWZvcmUtdW5tb3VudCcsIG9sZFRhZyk7XG4gICAgb2xkVGFnLnVubW91bnQodHJ1ZSk7IC8vIGRldGFjaCB0aGUgb2xkIHRhZ1xuXG4gICAgLy8gcmVzZXQgdGhlIGlubmVySFRNTCBhbmQgYXR0cmlidXRlcyB0byBob3cgdGhleSB3ZXJlIGJlZm9yZSBtb3VudFxuICAgIGVsLmlubmVySFRNTCA9IG9sZFRhZy5fXy5pbm5lckhUTUw7XG4gICAgKG9sZFRhZy5fXy5pbnN0QXR0cnMgfHwgW10pLm1hcChmdW5jdGlvbihhdHRyKSB7XG4gICAgICBlbC5zZXRBdHRyaWJ1dGUoYXR0ci5uYW1lLCBhdHRyLnZhbHVlKTtcbiAgICB9KTtcblxuICAgIC8vIGNvcHkgb3B0aW9ucyBmb3IgY3JlYXRpbmcgdGhlIG5ldyB0YWdcbiAgICB2YXIgbmV3T3B0cyA9IHt9O1xuICAgIGZvcihrZXkgaW4gb2xkVGFnLm9wdHMpIHtcbiAgICAgIG5ld09wdHNba2V5XSA9IG9sZFRhZy5vcHRzW2tleV07XG4gICAgfVxuICAgIG5ld09wdHMucGFyZW50ID0gb2xkVGFnLnBhcmVudDtcblxuICAgIC8vIGNyZWF0ZSB0aGUgbmV3IHRhZ1xuICAgIHJlbG9hZC50cmlnZ2VyKCdiZWZvcmUtbW91bnQnLCBuZXdPcHRzLCBvbGRUYWcpO1xuICAgIHZhciBuZXdUYWcgPSByaW90Lm1vdW50KGVsLCBuZXdPcHRzKVswXTtcblxuICAgIC8vIGNvcHkgc3RhdGUgZnJvbSB0aGUgb2xkIHRvIG5ldyB0YWdcbiAgICBmb3IodmFyIGtleSBpbiBvbGRUYWcpIHtcbiAgICAgIHYgPSBvbGRUYWdba2V5XTtcbiAgICAgIGlmICh+bm9uU3RhdGUuaW5kZXhPZihrZXkpKSB7IGNvbnRpbnVlIH1cbiAgICAgIG5ld1RhZ1trZXldID0gdjtcbiAgICB9XG4gICAgbmV3VGFnLnVwZGF0ZSgpO1xuICAgIHRhZ3MucHVzaChuZXdUYWcpO1xuICAgIHJlbG9hZC50cmlnZ2VyKCdhZnRlci1tb3VudCcsIG5ld1RhZywgb2xkVGFnKTtcbiAgfVxuXG4gIHJldHVybiB0YWdzXG59XG5cbnJpb3Qub2JzZXJ2YWJsZShyZWxvYWQpO1xucmlvdC5yZWxvYWQgPSByZWxvYWQ7XG5pZiAocmlvdC5kZWZhdWx0KSB7IHJpb3QuZGVmYXVsdC5yZWxvYWQgPSByZWxvYWQ7IH1cblxuZXhwb3J0cy5yZWxvYWQgPSByZWxvYWQ7XG5leHBvcnRzWydkZWZhdWx0J10gPSByZWxvYWQ7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG5cbn0pKSk7XG4iLCJyaW90LnRhZzIoJ3JvdXRlaGFuZGxlcicsICc8ZGl2IGRhdGEtaXM9XCJ7dGFnbmFtZX1cIj48L2Rpdj4nLCAnJywgJycsIGZ1bmN0aW9uIChvcHRzKSB7XG4gIHZhciBwYWdlO1xuXG4gIHBhZ2UgPSBudWxsO1xuXG4gIGlmICh0eXBlb2YgZXhwb3J0cyA9PT0gXCJvYmplY3RcIiAmJiAodHlwZW9mIGV4cG9ydHMgIT09IFwidW5kZWZpbmVkXCIgJiYgZXhwb3J0cyAhPT0gbnVsbCkpIHtcbiAgICBwYWdlID0gcmVxdWlyZSgncGFnZScpO1xuICB9IGVsc2UgaWYgKHdpbmRvdy5wYWdlID09IG51bGwpIHtcbiAgICByZXR1cm4gY29uc29sZS5sb2coJ1BhZ2UuanMgbm90IGZvdW5kIC0gcGxlYXNlIGNoZWNrIGl0IGhhcyBiZWVuIG5wbSBpbnN0YWxsZWQgb3IgaW5jbHVkZWQgaW4geW91ciBwYWdlJyk7XG4gIH0gZWxzZSB7XG4gICAgcGFnZSA9IHdpbmRvdy5wYWdlO1xuICB9XG5cbiAgdGhpcy5vbignbW91bnQnLCAoZnVuY3Rpb24gKF90aGlzKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBiYXNlcGF0aCwgcmVmO1xuICAgICAgX3RoaXMudGFnc3RhY2sgPSBbXTtcbiAgICAgIGlmIChvcHRzLnJvdXRlcykge1xuICAgICAgICBfdGhpcy5tb3VudFJvdXRlcyh7XG4gICAgICAgICAgaGFuZGxlcjogX3RoaXNcbiAgICAgICAgfSwgb3B0cy5yb3V0ZXMpO1xuICAgICAgICBpZiAoKHJlZiA9IG9wdHMub3B0aW9ucykgIT0gbnVsbCA/IHJlZi5iYXNlIDogdm9pZCAwKSB7XG4gICAgICAgICAgYmFzZXBhdGggPSBvcHRzLm9wdGlvbnMuYmFzZTtcbiAgICAgICAgICBwYWdlLmJhc2UoYmFzZXBhdGgpO1xuICAgICAgICAgIGRlbGV0ZSBvcHRzLm9wdGlvbnMuYmFzZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcGFnZShvcHRzLm9wdGlvbnMpO1xuICAgICAgfVxuICAgIH07XG4gIH0pKHRoaXMpKTtcblxuICB0aGlzLm1vdW50Um91dGVzID0gKGZ1bmN0aW9uIChfdGhpcykge1xuICAgIHJldHVybiBmdW5jdGlvbiAocGFyZW50LCByb3V0ZXMpIHtcbiAgICAgIHZhciByb3V0ZTtcbiAgICAgIHJldHVybiByb3V0ZSA9IF90aGlzLmZpbmRSb3V0ZShudWxsLCByb3V0ZXMsIGZ1bmN0aW9uICh0cmVlLCByZXEpIHtcbiAgICAgICAgdmFyIGksIGlkeCwgbGVuLCBuZXh0dGFnLCByZWYsIHJlZjEsIHJlZjIsIHJlZjMsIHJlbW92ZVRhZywgcmVzdWx0cywgcm91dGVvcHRzLCB0YWc7XG4gICAgICAgIGRlbGV0ZSBvcHRzLnJvdXRlcztcbiAgICAgICAgcm91dGVvcHRzID0gb3B0cztcbiAgICAgICAgcm91dGVvcHRzLnBhZ2UgPSBwYWdlO1xuICAgICAgICByb3V0ZW9wdHMucGFyYW1zID0gcmVxLnBhcmFtcztcbiAgICAgICAgcm91dGVvcHRzLnN0YXRlID0gcmVxLnN0YXRlO1xuICAgICAgICB0YWcgPSBfdGhpcztcbiAgICAgICAgZm9yIChpZHggPSBpID0gMCwgbGVuID0gdHJlZS5sZW5ndGg7IGkgPCBsZW47IGlkeCA9ICsraSkge1xuICAgICAgICAgIHJvdXRlID0gdHJlZVtpZHhdO1xuICAgICAgICAgIGlmIChfdGhpcy50YWdzdGFja1tpZHhdICYmIF90aGlzLnRhZ3N0YWNrW2lkeF0udGFnbmFtZSA9PT0gcm91dGUudGFnKSB7XG4gICAgICAgICAgICBuZXh0dGFnID0gX3RoaXMudGFnc3RhY2tbaWR4XS5uZXh0dGFnO1xuICAgICAgICAgICAgcmlvdC51cGRhdGUoKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaWYgKHRhZyAmJiAocm91dGUgIT0gbnVsbCA/IHJvdXRlLnRhZyA6IHZvaWQgMCkpIHtcbiAgICAgICAgICAgICAgbmV4dHRhZyA9IHRhZy5zZXRUYWcocm91dGUudGFnLCByb3V0ZW9wdHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBfdGhpcy50YWdzdGFja1tpZHhdID0ge1xuICAgICAgICAgICAgdGFnbmFtZTogcm91dGUudGFnLFxuICAgICAgICAgICAgbmV4dHRhZzogbmV4dHRhZyxcbiAgICAgICAgICAgIHRhZzogdGFnXG4gICAgICAgICAgfTtcbiAgICAgICAgICB0YWcgPSAobmV4dHRhZyAhPSBudWxsID8gKHJlZiA9IG5leHR0YWdbMF0pICE9IG51bGwgPyByZWYudGFncy5yb3V0ZWhhbmRsZXIgOiB2b2lkIDAgOiB2b2lkIDApIHx8IChuZXh0dGFnICE9IG51bGwgPyAocmVmMSA9IG5leHR0YWdbMF0pICE9IG51bGwgPyAocmVmMiA9IHJlZjEucm9vdC5xdWVyeVNlbGVjdG9yKCdyb3V0ZWhhbmRsZXInKSkgIT0gbnVsbCA/IHJlZjIuX3RhZyA6IHZvaWQgMCA6IHZvaWQgMCA6IHZvaWQgMCk7XG4gICAgICAgIH1cbiAgICAgICAgcmVzdWx0cyA9IFtdO1xuICAgICAgICB3aGlsZSAoaWR4IDwgX3RoaXMudGFnc3RhY2subGVuZ3RoKSB7XG4gICAgICAgICAgcmVtb3ZlVGFnID0gX3RoaXMudGFnc3RhY2sucG9wKCk7XG4gICAgICAgICAgcmVzdWx0cy5wdXNoKChyZWYzID0gcmVtb3ZlVGFnLm5leHR0YWdbMF0pICE9IG51bGwgPyByZWYzLnVubW91bnQodHJ1ZSkgOiB2b2lkIDApO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiByZXN1bHRzO1xuICAgICAgfSk7XG4gICAgfTtcbiAgfSkodGhpcyk7XG5cbiAgdGhpcy5zZXRUYWcgPSAoZnVuY3Rpb24gKF90aGlzKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uICh0YWduYW1lLCByb3V0ZW9wdHMpIHtcbiAgICAgIHZhciB0YWc7XG4gICAgICBfdGhpcy5yb290LmNoaWxkTm9kZXNbMF0uc2V0QXR0cmlidXRlKFwiZGF0YS1pc1wiLCB0YWduYW1lKTtcbiAgICAgIF90aGlzLnRhZ3NbdGFnbmFtZV07XG4gICAgICB0YWcgPSByaW90Lm1vdW50KHRhZ25hbWUsIHJvdXRlb3B0cyk7XG4gICAgICB0YWdbMF0ub3B0cyA9IHJvdXRlb3B0cztcbiAgICAgIHJldHVybiB0YWc7XG4gICAgfTtcbiAgfSkodGhpcyk7XG5cbiAgdGhpcy5maW5kUm91dGUgPSAoZnVuY3Rpb24gKF90aGlzKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIChwYXJlbnRzLCByb3V0ZXMsIGNiYWNrKSB7XG4gICAgICB2YXIgaSwgbGVuLCBwYXJlbnRwYXRoLCByZXN1bHRzLCByb3V0ZSwgc3VicGFyZW50cztcbiAgICAgIHBhcmVudHBhdGggPSBwYXJlbnRzID8gcGFyZW50cy5tYXAoZnVuY3Rpb24gKG9iKSB7XG4gICAgICAgIHJldHVybiBvYi5yb3V0ZTtcbiAgICAgIH0pLmpvaW4oXCJcIikucmVwbGFjZSgvXFwvXFwvL2csICcvJykgOiBcIlwiO1xuICAgICAgcmVzdWx0cyA9IFtdO1xuICAgICAgZm9yIChpID0gMCwgbGVuID0gcm91dGVzLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICAgIHJvdXRlID0gcm91dGVzW2ldO1xuICAgICAgICBpZiAoKHJvdXRlLnVzZSAhPSBudWxsKSAmJiB0eXBlb2Ygcm91dGUudXNlID09PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgICAgICAoZnVuY3Rpb24gKHJvdXRlKSB7XG4gICAgICAgICAgICB2YXIgbWFpbnJvdXRlO1xuICAgICAgICAgICAgbWFpbnJvdXRlID0gKHBhcmVudHBhdGggKyByb3V0ZS5yb3V0ZSkucmVwbGFjZSgvXFwvXFwvL2csICcvJyk7XG4gICAgICAgICAgICByZXR1cm4gcGFnZShtYWlucm91dGUsIGZ1bmN0aW9uIChjdHgsIG5leHQpIHtcbiAgICAgICAgICAgICAgaWYgKG1haW5yb3V0ZSAhPT0gXCIqXCIpIHtcbiAgICAgICAgICAgICAgICBjYmFjayhbcm91dGVdLCBjdHgpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIHJldHVybiByb3V0ZS51c2UoY3R4LCBuZXh0LCBwYWdlKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH0pKHJvdXRlKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAocm91dGUudGFnICE9IG51bGwpIHtcbiAgICAgICAgICBzdWJwYXJlbnRzID0gcGFyZW50cyA/IHBhcmVudHMuc2xpY2UoKSA6IFtdO1xuICAgICAgICAgIHN1YnBhcmVudHMucHVzaChyb3V0ZSk7XG4gICAgICAgICAgKGZ1bmN0aW9uIChzdWJwYXJlbnRzKSB7XG4gICAgICAgICAgICB2YXIgbWFpbnJvdXRlLCB0aGlzcm91dGU7XG4gICAgICAgICAgICB0aGlzcm91dGUgPSByb3V0ZTtcbiAgICAgICAgICAgIG1haW5yb3V0ZSA9IChwYXJlbnRwYXRoICsgcm91dGUucm91dGUpLnJlcGxhY2UoL1xcL1xcLy9nLCAnLycpO1xuICAgICAgICAgICAgcmV0dXJuIHBhZ2UobWFpbnJvdXRlLCBmdW5jdGlvbiAocmVxLCBuZXh0KSB7XG4gICAgICAgICAgICAgIHZhciByZWY7XG4gICAgICAgICAgICAgIGNiYWNrKHN1YnBhcmVudHMsIHJlcSk7XG4gICAgICAgICAgICAgIGlmICgocmVmID0gdGhpc3JvdXRlLnJvdXRlcykgIT0gbnVsbCA/IHJlZi5maWx0ZXIoZnVuY3Rpb24gKHJvdXRlKSB7XG4gICAgICAgICAgICAgICAgICByZXR1cm4gcm91dGUucm91dGUgPT09IFwiL1wiO1xuICAgICAgICAgICAgICAgIH0pLmxlbmd0aCA6IHZvaWQgMCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBuZXh0KCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH0pKHN1YnBhcmVudHMpO1xuICAgICAgICB9XG4gICAgICAgIGlmIChyb3V0ZS5yb3V0ZXMpIHtcbiAgICAgICAgICByZXN1bHRzLnB1c2goX3RoaXMuZmluZFJvdXRlKHN1YnBhcmVudHMsIHJvdXRlLnJvdXRlcywgY2JhY2spKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZXN1bHRzLnB1c2godm9pZCAwKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgcmV0dXJuIHJlc3VsdHM7XG4gICAgfTtcbiAgfSkodGhpcyk7XG59KTsiLCIvKiBSaW90IHYzLjExLjEsIEBsaWNlbnNlIE1JVCAqL1xuKGZ1bmN0aW9uIChnbG9iYWwsIGZhY3RvcnkpIHtcbiAgdHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnICYmIHR5cGVvZiBtb2R1bGUgIT09ICd1bmRlZmluZWQnID8gZmFjdG9yeShleHBvcnRzKSA6XG4gIHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZCA/IGRlZmluZShbJ2V4cG9ydHMnXSwgZmFjdG9yeSkgOlxuICAoZmFjdG9yeSgoZ2xvYmFsLnJpb3QgPSB7fSkpKTtcbn0odGhpcywgKGZ1bmN0aW9uIChleHBvcnRzKSB7ICd1c2Ugc3RyaWN0JztcblxuICAvKipcbiAgICogU2hvcnRlciBhbmQgZmFzdCB3YXkgdG8gc2VsZWN0IGEgc2luZ2xlIG5vZGUgaW4gdGhlIERPTVxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IHNlbGVjdG9yIC0gdW5pcXVlIGRvbSBzZWxlY3RvclxuICAgKiBAcGFyYW0gICB7IE9iamVjdCB9IGN0eCAtIERPTSBub2RlIHdoZXJlIHRoZSB0YXJnZXQgb2Ygb3VyIHNlYXJjaCB3aWxsIGlzIGxvY2F0ZWRcbiAgICogQHJldHVybnMgeyBPYmplY3QgfSBkb20gbm9kZSBmb3VuZFxuICAgKi9cbiAgZnVuY3Rpb24gJChzZWxlY3RvciwgY3R4KSB7XG4gICAgcmV0dXJuIChjdHggfHwgZG9jdW1lbnQpLnF1ZXJ5U2VsZWN0b3Ioc2VsZWN0b3IpXG4gIH1cblxuICB2YXJcbiAgICAvLyBiZSBhd2FyZSwgaW50ZXJuYWwgdXNhZ2VcbiAgICAvLyBBVFRFTlRJT046IHByZWZpeCB0aGUgZ2xvYmFsIGR5bmFtaWMgdmFyaWFibGVzIHdpdGggYF9fYFxuICAgIC8vIHRhZ3MgaW5zdGFuY2VzIGNhY2hlXG4gICAgX19UQUdTX0NBQ0hFID0gW10sXG4gICAgLy8gdGFncyBpbXBsZW1lbnRhdGlvbiBjYWNoZVxuICAgIF9fVEFHX0lNUEwgPSB7fSxcbiAgICBZSUVMRF9UQUcgPSAneWllbGQnLFxuXG4gICAgLyoqXG4gICAgICogQ29uc3RcbiAgICAgKi9cbiAgICBHTE9CQUxfTUlYSU4gPSAnX19nbG9iYWxfbWl4aW4nLFxuXG4gICAgLy8gcmlvdCBzcGVjaWZpYyBwcmVmaXhlcyBvciBhdHRyaWJ1dGVzXG4gICAgQVRUUlNfUFJFRklYID0gJ3Jpb3QtJyxcblxuICAgIC8vIFJpb3QgRGlyZWN0aXZlc1xuICAgIFJFRl9ESVJFQ1RJVkVTID0gWydyZWYnLCAnZGF0YS1yZWYnXSxcbiAgICBJU19ESVJFQ1RJVkUgPSAnZGF0YS1pcycsXG4gICAgQ09ORElUSU9OQUxfRElSRUNUSVZFID0gJ2lmJyxcbiAgICBMT09QX0RJUkVDVElWRSA9ICdlYWNoJyxcbiAgICBMT09QX05PX1JFT1JERVJfRElSRUNUSVZFID0gJ25vLXJlb3JkZXInLFxuICAgIFNIT1dfRElSRUNUSVZFID0gJ3Nob3cnLFxuICAgIEhJREVfRElSRUNUSVZFID0gJ2hpZGUnLFxuICAgIEtFWV9ESVJFQ1RJVkUgPSAna2V5JyxcbiAgICBSSU9UX0VWRU5UU19LRVkgPSAnX19yaW90LWV2ZW50c19fJyxcblxuICAgIC8vIGZvciB0eXBlb2YgPT0gJycgY29tcGFyaXNvbnNcbiAgICBUX1NUUklORyA9ICdzdHJpbmcnLFxuICAgIFRfT0JKRUNUID0gJ29iamVjdCcsXG4gICAgVF9VTkRFRiAgPSAndW5kZWZpbmVkJyxcbiAgICBUX0ZVTkNUSU9OID0gJ2Z1bmN0aW9uJyxcblxuICAgIFhMSU5LX05TID0gJ2h0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsnLFxuICAgIFNWR19OUyA9ICdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZycsXG4gICAgWExJTktfUkVHRVggPSAvXnhsaW5rOihcXHcrKS8sXG5cbiAgICBXSU4gPSB0eXBlb2Ygd2luZG93ID09PSBUX1VOREVGID8gLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi8gdW5kZWZpbmVkIDogd2luZG93LFxuXG4gICAgLy8gc3BlY2lhbCBuYXRpdmUgdGFncyB0aGF0IGNhbm5vdCBiZSB0cmVhdGVkIGxpa2UgdGhlIG90aGVyc1xuICAgIFJFX1NQRUNJQUxfVEFHUyA9IC9eKD86dCg/OmJvZHl8aGVhZHxmb290fFtyaGRdKXxjYXB0aW9ufGNvbCg/Omdyb3VwKT98b3B0KD86aW9ufGdyb3VwKSkkLyxcbiAgICBSRV9TUEVDSUFMX1RBR1NfTk9fT1BUSU9OID0gL14oPzp0KD86Ym9keXxoZWFkfGZvb3R8W3JoZF0pfGNhcHRpb258Y29sKD86Z3JvdXApPykkLyxcbiAgICBSRV9FVkVOVFNfUFJFRklYID0gL15vbi8sXG4gICAgUkVfSFRNTF9BVFRSUyA9IC8oWy1cXHddKykgPz0gPyg/OlwiKFteXCJdKil8JyhbXiddKil8KHtbXn1dKn0pKS9nLFxuICAgIC8vIHNvbWUgRE9NIGF0dHJpYnV0ZXMgbXVzdCBiZSBub3JtYWxpemVkXG4gICAgQ0FTRV9TRU5TSVRJVkVfQVRUUklCVVRFUyA9IHtcbiAgICAgICd2aWV3Ym94JzogJ3ZpZXdCb3gnLFxuICAgICAgJ3ByZXNlcnZlYXNwZWN0cmF0aW8nOiAncHJlc2VydmVBc3BlY3RSYXRpbydcbiAgICB9LFxuICAgIC8qKlxuICAgICAqIE1hdGNoZXMgYm9vbGVhbiBIVE1MIGF0dHJpYnV0ZXMgaW4gdGhlIHJpb3QgdGFnIGRlZmluaXRpb24uXG4gICAgICogV2l0aCBhIGxvbmcgbGlzdCBsaWtlIHRoaXMsIGEgcmVnZXggaXMgZmFzdGVyIHRoYW4gYFtdLmluZGV4T2ZgIGluIG1vc3QgYnJvd3NlcnMuXG4gICAgICogQGNvbnN0IHtSZWdFeHB9XG4gICAgICogQHNlZSBbYXR0cmlidXRlcy5tZF0oaHR0cHM6Ly9naXRodWIuY29tL3Jpb3QvY29tcGlsZXIvYmxvYi9kZXYvZG9jL2F0dHJpYnV0ZXMubWQpXG4gICAgICovXG4gICAgUkVfQk9PTF9BVFRSUyA9IC9eKD86ZGlzYWJsZWR8Y2hlY2tlZHxyZWFkb25seXxyZXF1aXJlZHxhbGxvd2Z1bGxzY3JlZW58YXV0byg/OmZvY3VzfHBsYXkpfGNvbXBhY3R8Y29udHJvbHN8ZGVmYXVsdHxmb3Jtbm92YWxpZGF0ZXxoaWRkZW58aXNtYXB8aXRlbXNjb3BlfGxvb3B8bXVsdGlwbGV8bXV0ZWR8bm8oPzpyZXNpemV8c2hhZGV8dmFsaWRhdGV8d3JhcCk/fG9wZW58cmV2ZXJzZWR8c2VhbWxlc3N8c2VsZWN0ZWR8c29ydGFibGV8dHJ1ZXNwZWVkfHR5cGVtdXN0bWF0Y2gpJC8sXG4gICAgLy8gdmVyc2lvbiMgZm9yIElFIDgtMTEsIDAgZm9yIG90aGVyc1xuICAgIElFX1ZFUlNJT04gPSAoV0lOICYmIFdJTi5kb2N1bWVudCB8fCAvKiBpc3RhbmJ1bCBpZ25vcmUgbmV4dCAqLyB7fSkuZG9jdW1lbnRNb2RlIHwgMDtcblxuICAvKipcbiAgICogQ3JlYXRlIGEgZ2VuZXJpYyBET00gbm9kZVxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IG5hbWUgLSBuYW1lIG9mIHRoZSBET00gbm9kZSB3ZSB3YW50IHRvIGNyZWF0ZVxuICAgKiBAcmV0dXJucyB7IE9iamVjdCB9IERPTSBub2RlIGp1c3QgY3JlYXRlZFxuICAgKi9cbiAgZnVuY3Rpb24gbWFrZUVsZW1lbnQobmFtZSkge1xuICAgIHJldHVybiBuYW1lID09PSAnc3ZnJyA/IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnROUyhTVkdfTlMsIG5hbWUpIDogZG9jdW1lbnQuY3JlYXRlRWxlbWVudChuYW1lKVxuICB9XG5cbiAgLyoqXG4gICAqIFNldCBhbnkgRE9NIGF0dHJpYnV0ZVxuICAgKiBAcGFyYW0geyBPYmplY3QgfSBkb20gLSBET00gbm9kZSB3ZSB3YW50IHRvIHVwZGF0ZVxuICAgKiBAcGFyYW0geyBTdHJpbmcgfSBuYW1lIC0gbmFtZSBvZiB0aGUgcHJvcGVydHkgd2Ugd2FudCB0byBzZXRcbiAgICogQHBhcmFtIHsgU3RyaW5nIH0gdmFsIC0gdmFsdWUgb2YgdGhlIHByb3BlcnR5IHdlIHdhbnQgdG8gc2V0XG4gICAqL1xuICBmdW5jdGlvbiBzZXRBdHRyaWJ1dGUoZG9tLCBuYW1lLCB2YWwpIHtcbiAgICB2YXIgeGxpbmsgPSBYTElOS19SRUdFWC5leGVjKG5hbWUpO1xuICAgIGlmICh4bGluayAmJiB4bGlua1sxXSlcbiAgICAgIHsgZG9tLnNldEF0dHJpYnV0ZU5TKFhMSU5LX05TLCB4bGlua1sxXSwgdmFsKTsgfVxuICAgIGVsc2VcbiAgICAgIHsgZG9tLnNldEF0dHJpYnV0ZShuYW1lLCB2YWwpOyB9XG4gIH1cblxuICB2YXIgc3R5bGVOb2RlO1xuICAvLyBDcmVhdGUgY2FjaGUgYW5kIHNob3J0Y3V0IHRvIHRoZSBjb3JyZWN0IHByb3BlcnR5XG4gIHZhciBjc3NUZXh0UHJvcDtcbiAgdmFyIGJ5TmFtZSA9IHt9O1xuICB2YXIgbmVlZHNJbmplY3QgPSBmYWxzZTtcblxuICAvLyBza2lwIHRoZSBmb2xsb3dpbmcgY29kZSBvbiB0aGUgc2VydmVyXG4gIGlmIChXSU4pIHtcbiAgICBzdHlsZU5vZGUgPSAoKGZ1bmN0aW9uICgpIHtcbiAgICAgIC8vIGNyZWF0ZSBhIG5ldyBzdHlsZSBlbGVtZW50IHdpdGggdGhlIGNvcnJlY3QgdHlwZVxuICAgICAgdmFyIG5ld05vZGUgPSBtYWtlRWxlbWVudCgnc3R5bGUnKTtcbiAgICAgIC8vIHJlcGxhY2UgYW55IHVzZXIgbm9kZSBvciBpbnNlcnQgdGhlIG5ldyBvbmUgaW50byB0aGUgaGVhZFxuICAgICAgdmFyIHVzZXJOb2RlID0gJCgnc3R5bGVbdHlwZT1yaW90XScpO1xuXG4gICAgICBzZXRBdHRyaWJ1dGUobmV3Tm9kZSwgJ3R5cGUnLCAndGV4dC9jc3MnKTtcbiAgICAgIC8qIGlzdGFuYnVsIGlnbm9yZSBuZXh0ICovXG4gICAgICBpZiAodXNlck5vZGUpIHtcbiAgICAgICAgaWYgKHVzZXJOb2RlLmlkKSB7IG5ld05vZGUuaWQgPSB1c2VyTm9kZS5pZDsgfVxuICAgICAgICB1c2VyTm9kZS5wYXJlbnROb2RlLnJlcGxhY2VDaGlsZChuZXdOb2RlLCB1c2VyTm9kZSk7XG4gICAgICB9IGVsc2UgeyBkb2N1bWVudC5oZWFkLmFwcGVuZENoaWxkKG5ld05vZGUpOyB9XG5cbiAgICAgIHJldHVybiBuZXdOb2RlXG4gICAgfSkpKCk7XG4gICAgY3NzVGV4dFByb3AgPSBzdHlsZU5vZGUuc3R5bGVTaGVldDtcbiAgfVxuXG4gIC8qKlxuICAgKiBPYmplY3QgdGhhdCB3aWxsIGJlIHVzZWQgdG8gaW5qZWN0IGFuZCBtYW5hZ2UgdGhlIGNzcyBvZiBldmVyeSB0YWcgaW5zdGFuY2VcbiAgICovXG4gIHZhciBzdHlsZU1hbmFnZXIgPSB7XG4gICAgc3R5bGVOb2RlOiBzdHlsZU5vZGUsXG4gICAgLyoqXG4gICAgICogU2F2ZSBhIHRhZyBzdHlsZSB0byBiZSBsYXRlciBpbmplY3RlZCBpbnRvIERPTVxuICAgICAqIEBwYXJhbSB7IFN0cmluZyB9IGNzcyAtIGNzcyBzdHJpbmdcbiAgICAgKiBAcGFyYW0geyBTdHJpbmcgfSBuYW1lIC0gaWYgaXQncyBwYXNzZWQgd2Ugd2lsbCBtYXAgdGhlIGNzcyB0byBhIHRhZ25hbWVcbiAgICAgKi9cbiAgICBhZGQ6IGZ1bmN0aW9uIGFkZChjc3MsIG5hbWUpIHtcbiAgICAgIGJ5TmFtZVtuYW1lXSA9IGNzcztcbiAgICAgIG5lZWRzSW5qZWN0ID0gdHJ1ZTtcbiAgICB9LFxuICAgIC8qKlxuICAgICAqIEluamVjdCBhbGwgcHJldmlvdXNseSBzYXZlZCB0YWcgc3R5bGVzIGludG8gRE9NXG4gICAgICogaW5uZXJIVE1MIHNlZW1zIHNsb3c6IGh0dHA6Ly9qc3BlcmYuY29tL3Jpb3QtaW5zZXJ0LXN0eWxlXG4gICAgICovXG4gICAgaW5qZWN0OiBmdW5jdGlvbiBpbmplY3QoKSB7XG4gICAgICBpZiAoIVdJTiB8fCAhbmVlZHNJbmplY3QpIHsgcmV0dXJuIH1cbiAgICAgIG5lZWRzSW5qZWN0ID0gZmFsc2U7XG4gICAgICB2YXIgc3R5bGUgPSBPYmplY3Qua2V5cyhieU5hbWUpXG4gICAgICAgIC5tYXAoZnVuY3Rpb24gKGspIHsgcmV0dXJuIGJ5TmFtZVtrXTsgfSlcbiAgICAgICAgLmpvaW4oJ1xcbicpO1xuICAgICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cbiAgICAgIGlmIChjc3NUZXh0UHJvcCkgeyBjc3NUZXh0UHJvcC5jc3NUZXh0ID0gc3R5bGU7IH1cbiAgICAgIGVsc2UgeyBzdHlsZU5vZGUuaW5uZXJIVE1MID0gc3R5bGU7IH1cbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogUmVtb3ZlIGEgdGFnIHN0eWxlIG9mIGluamVjdGVkIERPTSBsYXRlci5cbiAgICAgKiBAcGFyYW0ge1N0cmluZ30gbmFtZSBhIHJlZ2lzdGVyZWQgdGFnbmFtZVxuICAgICAqL1xuICAgIHJlbW92ZTogZnVuY3Rpb24gcmVtb3ZlKG5hbWUpIHtcbiAgICAgIGRlbGV0ZSBieU5hbWVbbmFtZV07XG4gICAgICBuZWVkc0luamVjdCA9IHRydWU7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIFRoZSByaW90IHRlbXBsYXRlIGVuZ2luZVxuICAgKiBAdmVyc2lvbiB2My4wLjhcbiAgICovXG5cbiAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cbiAgdmFyIHNraXBSZWdleCA9IChmdW5jdGlvbiAoKSB7IC8vZXNsaW50LWRpc2FibGUtbGluZSBuby11bnVzZWQtdmFyc1xuXG4gICAgdmFyIGJlZm9yZVJlQ2hhcnMgPSAnW3soLDs6Pz18JiFefj4lKi8nO1xuXG4gICAgdmFyIGJlZm9yZVJlV29yZHMgPSBbXG4gICAgICAnY2FzZScsXG4gICAgICAnZGVmYXVsdCcsXG4gICAgICAnZG8nLFxuICAgICAgJ2Vsc2UnLFxuICAgICAgJ2luJyxcbiAgICAgICdpbnN0YW5jZW9mJyxcbiAgICAgICdwcmVmaXgnLFxuICAgICAgJ3JldHVybicsXG4gICAgICAndHlwZW9mJyxcbiAgICAgICd2b2lkJyxcbiAgICAgICd5aWVsZCdcbiAgICBdO1xuXG4gICAgdmFyIHdvcmRzTGFzdENoYXIgPSBiZWZvcmVSZVdvcmRzLnJlZHVjZShmdW5jdGlvbiAocywgdykge1xuICAgICAgcmV0dXJuIHMgKyB3LnNsaWNlKC0xKVxuICAgIH0sICcnKTtcblxuICAgIHZhciBSRV9SRUdFWCA9IC9eXFwvKD89W14qPi9dKVteWy9cXFxcXSooPzooPzpcXFxcLnxcXFsoPzpcXFxcLnxbXlxcXVxcXFxdKikqXFxdKVteW1xcXFwvXSopKj9cXC9bZ2ltdXldKi87XG4gICAgdmFyIFJFX1ZOX0NIQVIgPSAvWyRcXHddLztcblxuICAgIGZ1bmN0aW9uIHByZXYgKGNvZGUsIHBvcykge1xuICAgICAgd2hpbGUgKC0tcG9zID49IDAgJiYgL1xccy8udGVzdChjb2RlW3Bvc10pKXsgfVxuICAgICAgcmV0dXJuIHBvc1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIF9za2lwUmVnZXggKGNvZGUsIHN0YXJ0KSB7XG5cbiAgICAgIHZhciByZSA9IC8uKi9nO1xuICAgICAgdmFyIHBvcyA9IHJlLmxhc3RJbmRleCA9IHN0YXJ0Kys7XG4gICAgICB2YXIgbWF0Y2ggPSByZS5leGVjKGNvZGUpWzBdLm1hdGNoKFJFX1JFR0VYKTtcblxuICAgICAgaWYgKG1hdGNoKSB7XG4gICAgICAgIHZhciBuZXh0ID0gcG9zICsgbWF0Y2hbMF0ubGVuZ3RoO1xuXG4gICAgICAgIHBvcyA9IHByZXYoY29kZSwgcG9zKTtcbiAgICAgICAgdmFyIGMgPSBjb2RlW3Bvc107XG5cbiAgICAgICAgaWYgKHBvcyA8IDAgfHwgfmJlZm9yZVJlQ2hhcnMuaW5kZXhPZihjKSkge1xuICAgICAgICAgIHJldHVybiBuZXh0XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoYyA9PT0gJy4nKSB7XG5cbiAgICAgICAgICBpZiAoY29kZVtwb3MgLSAxXSA9PT0gJy4nKSB7XG4gICAgICAgICAgICBzdGFydCA9IG5leHQ7XG4gICAgICAgICAgfVxuXG4gICAgICAgIH0gZWxzZSBpZiAoYyA9PT0gJysnIHx8IGMgPT09ICctJykge1xuXG4gICAgICAgICAgaWYgKGNvZGVbLS1wb3NdICE9PSBjIHx8XG4gICAgICAgICAgICAgIChwb3MgPSBwcmV2KGNvZGUsIHBvcykpIDwgMCB8fFxuICAgICAgICAgICAgICAhUkVfVk5fQ0hBUi50ZXN0KGNvZGVbcG9zXSkpIHtcbiAgICAgICAgICAgIHN0YXJ0ID0gbmV4dDtcbiAgICAgICAgICB9XG5cbiAgICAgICAgfSBlbHNlIGlmICh+d29yZHNMYXN0Q2hhci5pbmRleE9mKGMpKSB7XG5cbiAgICAgICAgICB2YXIgZW5kID0gcG9zICsgMTtcblxuICAgICAgICAgIHdoaWxlICgtLXBvcyA+PSAwICYmIFJFX1ZOX0NIQVIudGVzdChjb2RlW3Bvc10pKXsgfVxuICAgICAgICAgIGlmICh+YmVmb3JlUmVXb3Jkcy5pbmRleE9mKGNvZGUuc2xpY2UocG9zICsgMSwgZW5kKSkpIHtcbiAgICAgICAgICAgIHN0YXJ0ID0gbmV4dDtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHN0YXJ0XG4gICAgfVxuXG4gICAgcmV0dXJuIF9za2lwUmVnZXhcblxuICB9KSgpO1xuXG4gIC8qKlxuICAgKiByaW90LnV0aWwuYnJhY2tldHNcbiAgICpcbiAgICogLSBgYnJhY2tldHMgICAgYCAtIFJldHVybnMgYSBzdHJpbmcgb3IgcmVnZXggYmFzZWQgb24gaXRzIHBhcmFtZXRlclxuICAgKiAtIGBicmFja2V0cy5zZXRgIC0gQ2hhbmdlIHRoZSBjdXJyZW50IHJpb3QgYnJhY2tldHNcbiAgICpcbiAgICogQG1vZHVsZVxuICAgKi9cblxuICAvKiBnbG9iYWwgcmlvdCAqL1xuXG4gIC8qIGlzdGFuYnVsIGlnbm9yZSBuZXh0ICovXG4gIHZhciBicmFja2V0cyA9IChmdW5jdGlvbiAoVU5ERUYpIHtcblxuICAgIHZhclxuICAgICAgUkVHTE9CID0gJ2cnLFxuXG4gICAgICBSX01MQ09NTVMgPSAvXFwvXFwqW14qXSpcXCorKD86W14qXFwvXVteKl0qXFwqKykqXFwvL2csXG5cbiAgICAgIFJfU1RSSU5HUyA9IC9cIlteXCJcXFxcXSooPzpcXFxcW1xcU1xcc11bXlwiXFxcXF0qKSpcInwnW14nXFxcXF0qKD86XFxcXFtcXFNcXHNdW14nXFxcXF0qKSonfGBbXmBcXFxcXSooPzpcXFxcW1xcU1xcc11bXmBcXFxcXSopKmAvZyxcblxuICAgICAgU19RQkxPQ0tTID0gUl9TVFJJTkdTLnNvdXJjZSArICd8JyArXG4gICAgICAgIC8oPzpcXGJyZXR1cm5cXHMrfCg/OlskXFx3XFwpXFxdXXxcXCtcXCt8LS0pXFxzKihcXC8pKD8hWypcXC9dKSkvLnNvdXJjZSArICd8JyArXG4gICAgICAgIC9cXC8oPz1bXipcXC9dKVteW1xcL1xcXFxdKig/Oig/OlxcWyg/OlxcXFwufFteXFxdXFxcXF0qKSpcXF18XFxcXC4pW15bXFwvXFxcXF0qKSo/KFtePF1cXC8pW2dpbV0qLy5zb3VyY2UsXG5cbiAgICAgIFVOU1VQUE9SVEVEID0gUmVnRXhwKCdbXFxcXCcgKyAneDAwLVxcXFx4MUY8PmEtekEtWjAtOVxcJ1wiLDtcXFxcXFxcXF0nKSxcblxuICAgICAgTkVFRF9FU0NBUEUgPSAvKD89W1tcXF0oKSorPy5eJHxdKS9nLFxuXG4gICAgICBTX1FCTE9DSzIgPSBSX1NUUklOR1Muc291cmNlICsgJ3wnICsgLyhcXC8pKD8hWypcXC9dKS8uc291cmNlLFxuXG4gICAgICBGSU5EQlJBQ0VTID0ge1xuICAgICAgICAnKCc6IFJlZ0V4cCgnKFsoKV0pfCcgICArIFNfUUJMT0NLMiwgUkVHTE9CKSxcbiAgICAgICAgJ1snOiBSZWdFeHAoJyhbW1xcXFxdXSl8JyArIFNfUUJMT0NLMiwgUkVHTE9CKSxcbiAgICAgICAgJ3snOiBSZWdFeHAoJyhbe31dKXwnICAgKyBTX1FCTE9DSzIsIFJFR0xPQilcbiAgICAgIH0sXG5cbiAgICAgIERFRkFVTFQgPSAneyB9JztcblxuICAgIHZhciBfcGFpcnMgPSBbXG4gICAgICAneycsICd9JyxcbiAgICAgICd7JywgJ30nLFxuICAgICAgL3tbXn1dKn0vLFxuICAgICAgL1xcXFwoW3t9XSkvZyxcbiAgICAgIC9cXFxcKHspfHsvZyxcbiAgICAgIFJlZ0V4cCgnXFxcXFxcXFwofSl8KFtbKHtdKXwofSl8JyArIFNfUUJMT0NLMiwgUkVHTE9CKSxcbiAgICAgIERFRkFVTFQsXG4gICAgICAvXlxccyp7XFxeP1xccyooWyRcXHddKykoPzpcXHMqLFxccyooXFxTKykpP1xccytpblxccysoXFxTLiopXFxzKn0vLFxuICAgICAgLyhefFteXFxcXF0pez1bXFxTXFxzXSo/fS9cbiAgICBdO1xuXG4gICAgdmFyXG4gICAgICBjYWNoZWRCcmFja2V0cyA9IFVOREVGLFxuICAgICAgX3JlZ2V4LFxuICAgICAgX2NhY2hlID0gW10sXG4gICAgICBfc2V0dGluZ3M7XG5cbiAgICBmdW5jdGlvbiBfbG9vcGJhY2sgKHJlKSB7IHJldHVybiByZSB9XG5cbiAgICBmdW5jdGlvbiBfcmV3cml0ZSAocmUsIGJwKSB7XG4gICAgICBpZiAoIWJwKSB7IGJwID0gX2NhY2hlOyB9XG4gICAgICByZXR1cm4gbmV3IFJlZ0V4cChcbiAgICAgICAgcmUuc291cmNlLnJlcGxhY2UoL3svZywgYnBbMl0pLnJlcGxhY2UoL30vZywgYnBbM10pLCByZS5nbG9iYWwgPyBSRUdMT0IgOiAnJ1xuICAgICAgKVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIF9jcmVhdGUgKHBhaXIpIHtcbiAgICAgIGlmIChwYWlyID09PSBERUZBVUxUKSB7IHJldHVybiBfcGFpcnMgfVxuXG4gICAgICB2YXIgYXJyID0gcGFpci5zcGxpdCgnICcpO1xuXG4gICAgICBpZiAoYXJyLmxlbmd0aCAhPT0gMiB8fCBVTlNVUFBPUlRFRC50ZXN0KHBhaXIpKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcignVW5zdXBwb3J0ZWQgYnJhY2tldHMgXCInICsgcGFpciArICdcIicpXG4gICAgICB9XG4gICAgICBhcnIgPSBhcnIuY29uY2F0KHBhaXIucmVwbGFjZShORUVEX0VTQ0FQRSwgJ1xcXFwnKS5zcGxpdCgnICcpKTtcblxuICAgICAgYXJyWzRdID0gX3Jld3JpdGUoYXJyWzFdLmxlbmd0aCA+IDEgPyAve1tcXFNcXHNdKj99LyA6IF9wYWlyc1s0XSwgYXJyKTtcbiAgICAgIGFycls1XSA9IF9yZXdyaXRlKHBhaXIubGVuZ3RoID4gMyA/IC9cXFxcKHt8fSkvZyA6IF9wYWlyc1s1XSwgYXJyKTtcbiAgICAgIGFycls2XSA9IF9yZXdyaXRlKF9wYWlyc1s2XSwgYXJyKTtcbiAgICAgIGFycls3XSA9IFJlZ0V4cCgnXFxcXFxcXFwoJyArIGFyclszXSArICcpfChbWyh7XSl8KCcgKyBhcnJbM10gKyAnKXwnICsgU19RQkxPQ0syLCBSRUdMT0IpO1xuICAgICAgYXJyWzhdID0gcGFpcjtcbiAgICAgIHJldHVybiBhcnJcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBfYnJhY2tldHMgKHJlT3JJZHgpIHtcbiAgICAgIHJldHVybiByZU9ySWR4IGluc3RhbmNlb2YgUmVnRXhwID8gX3JlZ2V4KHJlT3JJZHgpIDogX2NhY2hlW3JlT3JJZHhdXG4gICAgfVxuXG4gICAgX2JyYWNrZXRzLnNwbGl0ID0gZnVuY3Rpb24gc3BsaXQgKHN0ciwgdG1wbCwgX2JwKSB7XG4gICAgICAvLyBpc3RhbmJ1bCBpZ25vcmUgbmV4dDogX2JwIGlzIGZvciB0aGUgY29tcGlsZXJcbiAgICAgIGlmICghX2JwKSB7IF9icCA9IF9jYWNoZTsgfVxuXG4gICAgICB2YXJcbiAgICAgICAgcGFydHMgPSBbXSxcbiAgICAgICAgbWF0Y2gsXG4gICAgICAgIGlzZXhwcixcbiAgICAgICAgc3RhcnQsXG4gICAgICAgIHBvcyxcbiAgICAgICAgcmUgPSBfYnBbNl07XG5cbiAgICAgIHZhciBxYmxvY2tzID0gW107XG4gICAgICB2YXIgcHJldlN0ciA9ICcnO1xuICAgICAgdmFyIG1hcmssIGxhc3RJbmRleDtcblxuICAgICAgaXNleHByID0gc3RhcnQgPSByZS5sYXN0SW5kZXggPSAwO1xuXG4gICAgICB3aGlsZSAoKG1hdGNoID0gcmUuZXhlYyhzdHIpKSkge1xuXG4gICAgICAgIGxhc3RJbmRleCA9IHJlLmxhc3RJbmRleDtcbiAgICAgICAgcG9zID0gbWF0Y2guaW5kZXg7XG5cbiAgICAgICAgaWYgKGlzZXhwcikge1xuXG4gICAgICAgICAgaWYgKG1hdGNoWzJdKSB7XG5cbiAgICAgICAgICAgIHZhciBjaCA9IG1hdGNoWzJdO1xuICAgICAgICAgICAgdmFyIHJlY2ggPSBGSU5EQlJBQ0VTW2NoXTtcbiAgICAgICAgICAgIHZhciBpeCA9IDE7XG5cbiAgICAgICAgICAgIHJlY2gubGFzdEluZGV4ID0gbGFzdEluZGV4O1xuICAgICAgICAgICAgd2hpbGUgKChtYXRjaCA9IHJlY2guZXhlYyhzdHIpKSkge1xuICAgICAgICAgICAgICBpZiAobWF0Y2hbMV0pIHtcbiAgICAgICAgICAgICAgICBpZiAobWF0Y2hbMV0gPT09IGNoKSB7ICsraXg7IH1cbiAgICAgICAgICAgICAgICBlbHNlIGlmICghLS1peCkgeyBicmVhayB9XG4gICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcmVjaC5sYXN0SW5kZXggPSBwdXNoUUJsb2NrKG1hdGNoLmluZGV4LCByZWNoLmxhc3RJbmRleCwgbWF0Y2hbMl0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZS5sYXN0SW5kZXggPSBpeCA/IHN0ci5sZW5ndGggOiByZWNoLmxhc3RJbmRleDtcbiAgICAgICAgICAgIGNvbnRpbnVlXG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYgKCFtYXRjaFszXSkge1xuICAgICAgICAgICAgcmUubGFzdEluZGV4ID0gcHVzaFFCbG9jayhwb3MsIGxhc3RJbmRleCwgbWF0Y2hbNF0pO1xuICAgICAgICAgICAgY29udGludWVcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoIW1hdGNoWzFdKSB7XG4gICAgICAgICAgdW5lc2NhcGVTdHIoc3RyLnNsaWNlKHN0YXJ0LCBwb3MpKTtcbiAgICAgICAgICBzdGFydCA9IHJlLmxhc3RJbmRleDtcbiAgICAgICAgICByZSA9IF9icFs2ICsgKGlzZXhwciBePSAxKV07XG4gICAgICAgICAgcmUubGFzdEluZGV4ID0gc3RhcnQ7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKHN0ciAmJiBzdGFydCA8IHN0ci5sZW5ndGgpIHtcbiAgICAgICAgdW5lc2NhcGVTdHIoc3RyLnNsaWNlKHN0YXJ0KSk7XG4gICAgICB9XG5cbiAgICAgIHBhcnRzLnFibG9ja3MgPSBxYmxvY2tzO1xuXG4gICAgICByZXR1cm4gcGFydHNcblxuICAgICAgZnVuY3Rpb24gdW5lc2NhcGVTdHIgKHMpIHtcbiAgICAgICAgaWYgKHByZXZTdHIpIHtcbiAgICAgICAgICBzID0gcHJldlN0ciArIHM7XG4gICAgICAgICAgcHJldlN0ciA9ICcnO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0bXBsIHx8IGlzZXhwcikge1xuICAgICAgICAgIHBhcnRzLnB1c2gocyAmJiBzLnJlcGxhY2UoX2JwWzVdLCAnJDEnKSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcGFydHMucHVzaChzKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBmdW5jdGlvbiBwdXNoUUJsb2NrKF9wb3MsIF9sYXN0SW5kZXgsIHNsYXNoKSB7IC8vZXNsaW50LWRpc2FibGUtbGluZVxuICAgICAgICBpZiAoc2xhc2gpIHtcbiAgICAgICAgICBfbGFzdEluZGV4ID0gc2tpcFJlZ2V4KHN0ciwgX3Bvcyk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodG1wbCAmJiBfbGFzdEluZGV4ID4gX3BvcyArIDIpIHtcbiAgICAgICAgICBtYXJrID0gJ1xcdTIwNTcnICsgcWJsb2Nrcy5sZW5ndGggKyAnfic7XG4gICAgICAgICAgcWJsb2Nrcy5wdXNoKHN0ci5zbGljZShfcG9zLCBfbGFzdEluZGV4KSk7XG4gICAgICAgICAgcHJldlN0ciArPSBzdHIuc2xpY2Uoc3RhcnQsIF9wb3MpICsgbWFyaztcbiAgICAgICAgICBzdGFydCA9IF9sYXN0SW5kZXg7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIF9sYXN0SW5kZXhcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgX2JyYWNrZXRzLmhhc0V4cHIgPSBmdW5jdGlvbiBoYXNFeHByIChzdHIpIHtcbiAgICAgIHJldHVybiBfY2FjaGVbNF0udGVzdChzdHIpXG4gICAgfTtcblxuICAgIF9icmFja2V0cy5sb29wS2V5cyA9IGZ1bmN0aW9uIGxvb3BLZXlzIChleHByKSB7XG4gICAgICB2YXIgbSA9IGV4cHIubWF0Y2goX2NhY2hlWzldKTtcblxuICAgICAgcmV0dXJuIG1cbiAgICAgICAgPyB7IGtleTogbVsxXSwgcG9zOiBtWzJdLCB2YWw6IF9jYWNoZVswXSArIG1bM10udHJpbSgpICsgX2NhY2hlWzFdIH1cbiAgICAgICAgOiB7IHZhbDogZXhwci50cmltKCkgfVxuICAgIH07XG5cbiAgICBfYnJhY2tldHMuYXJyYXkgPSBmdW5jdGlvbiBhcnJheSAocGFpcikge1xuICAgICAgcmV0dXJuIHBhaXIgPyBfY3JlYXRlKHBhaXIpIDogX2NhY2hlXG4gICAgfTtcblxuICAgIGZ1bmN0aW9uIF9yZXNldCAocGFpcikge1xuICAgICAgaWYgKChwYWlyIHx8IChwYWlyID0gREVGQVVMVCkpICE9PSBfY2FjaGVbOF0pIHtcbiAgICAgICAgX2NhY2hlID0gX2NyZWF0ZShwYWlyKTtcbiAgICAgICAgX3JlZ2V4ID0gcGFpciA9PT0gREVGQVVMVCA/IF9sb29wYmFjayA6IF9yZXdyaXRlO1xuICAgICAgICBfY2FjaGVbOV0gPSBfcmVnZXgoX3BhaXJzWzldKTtcbiAgICAgIH1cbiAgICAgIGNhY2hlZEJyYWNrZXRzID0gcGFpcjtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBfc2V0U2V0dGluZ3MgKG8pIHtcbiAgICAgIHZhciBiO1xuXG4gICAgICBvID0gbyB8fCB7fTtcbiAgICAgIGIgPSBvLmJyYWNrZXRzO1xuICAgICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KG8sICdicmFja2V0cycsIHtcbiAgICAgICAgc2V0OiBfcmVzZXQsXG4gICAgICAgIGdldDogZnVuY3Rpb24gKCkgeyByZXR1cm4gY2FjaGVkQnJhY2tldHMgfSxcbiAgICAgICAgZW51bWVyYWJsZTogdHJ1ZVxuICAgICAgfSk7XG4gICAgICBfc2V0dGluZ3MgPSBvO1xuICAgICAgX3Jlc2V0KGIpO1xuICAgIH1cblxuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShfYnJhY2tldHMsICdzZXR0aW5ncycsIHtcbiAgICAgIHNldDogX3NldFNldHRpbmdzLFxuICAgICAgZ2V0OiBmdW5jdGlvbiAoKSB7IHJldHVybiBfc2V0dGluZ3MgfVxuICAgIH0pO1xuXG4gICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQ6IGluIHRoZSBicm93c2VyIHJpb3QgaXMgYWx3YXlzIGluIHRoZSBzY29wZSAqL1xuICAgIF9icmFja2V0cy5zZXR0aW5ncyA9IHR5cGVvZiByaW90ICE9PSAndW5kZWZpbmVkJyAmJiByaW90LnNldHRpbmdzIHx8IHt9O1xuICAgIF9icmFja2V0cy5zZXQgPSBfcmVzZXQ7XG4gICAgX2JyYWNrZXRzLnNraXBSZWdleCA9IHNraXBSZWdleDtcblxuICAgIF9icmFja2V0cy5SX1NUUklOR1MgPSBSX1NUUklOR1M7XG4gICAgX2JyYWNrZXRzLlJfTUxDT01NUyA9IFJfTUxDT01NUztcbiAgICBfYnJhY2tldHMuU19RQkxPQ0tTID0gU19RQkxPQ0tTO1xuICAgIF9icmFja2V0cy5TX1FCTE9DSzIgPSBTX1FCTE9DSzI7XG5cbiAgICByZXR1cm4gX2JyYWNrZXRzXG5cbiAgfSkoKTtcblxuICAvKipcbiAgICogQG1vZHVsZSB0bXBsXG4gICAqXG4gICAqIHRtcGwgICAgICAgICAgLSBSb290IGZ1bmN0aW9uLCByZXR1cm5zIHRoZSB0ZW1wbGF0ZSB2YWx1ZSwgcmVuZGVyIHdpdGggZGF0YVxuICAgKiB0bXBsLmhhc0V4cHIgIC0gVGVzdCB0aGUgZXhpc3RlbmNlIG9mIGEgZXhwcmVzc2lvbiBpbnNpZGUgYSBzdHJpbmdcbiAgICogdG1wbC5sb29wS2V5cyAtIEdldCB0aGUga2V5cyBmb3IgYW4gJ2VhY2gnIGxvb3AgKHVzZWQgYnkgYF9lYWNoYClcbiAgICovXG5cbiAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cbiAgdmFyIHRtcGwgPSAoZnVuY3Rpb24gKCkge1xuXG4gICAgdmFyIF9jYWNoZSA9IHt9O1xuXG4gICAgZnVuY3Rpb24gX3RtcGwgKHN0ciwgZGF0YSkge1xuICAgICAgaWYgKCFzdHIpIHsgcmV0dXJuIHN0ciB9XG5cbiAgICAgIHJldHVybiAoX2NhY2hlW3N0cl0gfHwgKF9jYWNoZVtzdHJdID0gX2NyZWF0ZShzdHIpKSkuY2FsbChcbiAgICAgICAgZGF0YSwgX2xvZ0Vyci5iaW5kKHtcbiAgICAgICAgICBkYXRhOiBkYXRhLFxuICAgICAgICAgIHRtcGw6IHN0clxuICAgICAgICB9KVxuICAgICAgKVxuICAgIH1cblxuICAgIF90bXBsLmhhc0V4cHIgPSBicmFja2V0cy5oYXNFeHByO1xuXG4gICAgX3RtcGwubG9vcEtleXMgPSBicmFja2V0cy5sb29wS2V5cztcblxuICAgIC8vIGlzdGFuYnVsIGlnbm9yZSBuZXh0XG4gICAgX3RtcGwuY2xlYXJDYWNoZSA9IGZ1bmN0aW9uICgpIHsgX2NhY2hlID0ge307IH07XG5cbiAgICBfdG1wbC5lcnJvckhhbmRsZXIgPSBudWxsO1xuXG4gICAgZnVuY3Rpb24gX2xvZ0VyciAoZXJyLCBjdHgpIHtcblxuICAgICAgZXJyLnJpb3REYXRhID0ge1xuICAgICAgICB0YWdOYW1lOiBjdHggJiYgY3R4Ll9fICYmIGN0eC5fXy50YWdOYW1lLFxuICAgICAgICBfcmlvdF9pZDogY3R4ICYmIGN0eC5fcmlvdF9pZCAgLy9lc2xpbnQtZGlzYWJsZS1saW5lIGNhbWVsY2FzZVxuICAgICAgfTtcblxuICAgICAgaWYgKF90bXBsLmVycm9ySGFuZGxlcikgeyBfdG1wbC5lcnJvckhhbmRsZXIoZXJyKTsgfVxuICAgICAgZWxzZSBpZiAoXG4gICAgICAgIHR5cGVvZiBjb25zb2xlICE9PSAndW5kZWZpbmVkJyAmJlxuICAgICAgICB0eXBlb2YgY29uc29sZS5lcnJvciA9PT0gJ2Z1bmN0aW9uJ1xuICAgICAgKSB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyLm1lc3NhZ2UpO1xuICAgICAgICBjb25zb2xlLmxvZygnPCVzPiAlcycsIGVyci5yaW90RGF0YS50YWdOYW1lIHx8ICdVbmtub3duIHRhZycsIHRoaXMudG1wbCk7IC8vIGVzbGludC1kaXNhYmxlLWxpbmVcbiAgICAgICAgY29uc29sZS5sb2codGhpcy5kYXRhKTsgLy8gZXNsaW50LWRpc2FibGUtbGluZVxuICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIF9jcmVhdGUgKHN0cikge1xuICAgICAgdmFyIGV4cHIgPSBfZ2V0VG1wbChzdHIpO1xuXG4gICAgICBpZiAoZXhwci5zbGljZSgwLCAxMSkgIT09ICd0cnl7cmV0dXJuICcpIHsgZXhwciA9ICdyZXR1cm4gJyArIGV4cHI7IH1cblxuICAgICAgcmV0dXJuIG5ldyBGdW5jdGlvbignRScsIGV4cHIgKyAnOycpICAgIC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tbmV3LWZ1bmNcbiAgICB9XG5cbiAgICB2YXIgUkVfRFFVT1RFID0gL1xcdTIwNTcvZztcbiAgICB2YXIgUkVfUUJNQVJLID0gL1xcdTIwNTcoXFxkKyl+L2c7XG5cbiAgICBmdW5jdGlvbiBfZ2V0VG1wbCAoc3RyKSB7XG4gICAgICB2YXIgcGFydHMgPSBicmFja2V0cy5zcGxpdChzdHIucmVwbGFjZShSRV9EUVVPVEUsICdcIicpLCAxKTtcbiAgICAgIHZhciBxc3RyID0gcGFydHMucWJsb2NrcztcbiAgICAgIHZhciBleHByO1xuXG4gICAgICBpZiAocGFydHMubGVuZ3RoID4gMiB8fCBwYXJ0c1swXSkge1xuICAgICAgICB2YXIgaSwgaiwgbGlzdCA9IFtdO1xuXG4gICAgICAgIGZvciAoaSA9IGogPSAwOyBpIDwgcGFydHMubGVuZ3RoOyArK2kpIHtcblxuICAgICAgICAgIGV4cHIgPSBwYXJ0c1tpXTtcblxuICAgICAgICAgIGlmIChleHByICYmIChleHByID0gaSAmIDFcblxuICAgICAgICAgICAgICA/IF9wYXJzZUV4cHIoZXhwciwgMSwgcXN0cilcblxuICAgICAgICAgICAgICA6ICdcIicgKyBleHByXG4gICAgICAgICAgICAgICAgICAucmVwbGFjZSgvXFxcXC9nLCAnXFxcXFxcXFwnKVxuICAgICAgICAgICAgICAgICAgLnJlcGxhY2UoL1xcclxcbj98XFxuL2csICdcXFxcbicpXG4gICAgICAgICAgICAgICAgICAucmVwbGFjZSgvXCIvZywgJ1xcXFxcIicpICtcbiAgICAgICAgICAgICAgICAnXCInXG5cbiAgICAgICAgICAgICkpIHsgbGlzdFtqKytdID0gZXhwcjsgfVxuXG4gICAgICAgIH1cblxuICAgICAgICBleHByID0gaiA8IDIgPyBsaXN0WzBdXG4gICAgICAgICAgICAgOiAnWycgKyBsaXN0LmpvaW4oJywnKSArICddLmpvaW4oXCJcIiknO1xuXG4gICAgICB9IGVsc2Uge1xuXG4gICAgICAgIGV4cHIgPSBfcGFyc2VFeHByKHBhcnRzWzFdLCAwLCBxc3RyKTtcbiAgICAgIH1cblxuICAgICAgaWYgKHFzdHIubGVuZ3RoKSB7XG4gICAgICAgIGV4cHIgPSBleHByLnJlcGxhY2UoUkVfUUJNQVJLLCBmdW5jdGlvbiAoXywgcG9zKSB7XG4gICAgICAgICAgcmV0dXJuIHFzdHJbcG9zXVxuICAgICAgICAgICAgLnJlcGxhY2UoL1xcci9nLCAnXFxcXHInKVxuICAgICAgICAgICAgLnJlcGxhY2UoL1xcbi9nLCAnXFxcXG4nKVxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBleHByXG4gICAgfVxuXG4gICAgdmFyIFJFX0NTTkFNRSA9IC9eKD86KC0/W19BLVphLXpcXHhBMC1cXHhGRl1bLVxcd1xceEEwLVxceEZGXSopfFxcdTIwNTcoXFxkKyl+KTovO1xuICAgIHZhclxuICAgICAgUkVfQlJFTkQgPSB7XG4gICAgICAgICcoJzogL1soKV0vZyxcbiAgICAgICAgJ1snOiAvW1tcXF1dL2csXG4gICAgICAgICd7JzogL1t7fV0vZ1xuICAgICAgfTtcblxuICAgIGZ1bmN0aW9uIF9wYXJzZUV4cHIgKGV4cHIsIGFzVGV4dCwgcXN0cikge1xuXG4gICAgICBleHByID0gZXhwclxuICAgICAgICAucmVwbGFjZSgvXFxzKy9nLCAnICcpLnRyaW0oKVxuICAgICAgICAucmVwbGFjZSgvXFwgPyhbW1xcKHt9LD9cXC46XSlcXCA/L2csICckMScpO1xuXG4gICAgICBpZiAoZXhwcikge1xuICAgICAgICB2YXJcbiAgICAgICAgICBsaXN0ID0gW10sXG4gICAgICAgICAgY250ID0gMCxcbiAgICAgICAgICBtYXRjaDtcblxuICAgICAgICB3aGlsZSAoZXhwciAmJlxuICAgICAgICAgICAgICAobWF0Y2ggPSBleHByLm1hdGNoKFJFX0NTTkFNRSkpICYmXG4gICAgICAgICAgICAgICFtYXRjaC5pbmRleFxuICAgICAgICAgICkge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAga2V5LFxuICAgICAgICAgICAganNiLFxuICAgICAgICAgICAgcmUgPSAvLHwoW1t7KF0pfCQvZztcblxuICAgICAgICAgIGV4cHIgPSBSZWdFeHAucmlnaHRDb250ZXh0O1xuICAgICAgICAgIGtleSAgPSBtYXRjaFsyXSA/IHFzdHJbbWF0Y2hbMl1dLnNsaWNlKDEsIC0xKS50cmltKCkucmVwbGFjZSgvXFxzKy9nLCAnICcpIDogbWF0Y2hbMV07XG5cbiAgICAgICAgICB3aGlsZSAoanNiID0gKG1hdGNoID0gcmUuZXhlYyhleHByKSlbMV0pIHsgc2tpcEJyYWNlcyhqc2IsIHJlKTsgfVxuXG4gICAgICAgICAganNiICA9IGV4cHIuc2xpY2UoMCwgbWF0Y2guaW5kZXgpO1xuICAgICAgICAgIGV4cHIgPSBSZWdFeHAucmlnaHRDb250ZXh0O1xuXG4gICAgICAgICAgbGlzdFtjbnQrK10gPSBfd3JhcEV4cHIoanNiLCAxLCBrZXkpO1xuICAgICAgICB9XG5cbiAgICAgICAgZXhwciA9ICFjbnQgPyBfd3JhcEV4cHIoZXhwciwgYXNUZXh0KVxuICAgICAgICAgICAgIDogY250ID4gMSA/ICdbJyArIGxpc3Quam9pbignLCcpICsgJ10uam9pbihcIiBcIikudHJpbSgpJyA6IGxpc3RbMF07XG4gICAgICB9XG4gICAgICByZXR1cm4gZXhwclxuXG4gICAgICBmdW5jdGlvbiBza2lwQnJhY2VzIChjaCwgcmUpIHtcbiAgICAgICAgdmFyXG4gICAgICAgICAgbW0sXG4gICAgICAgICAgbHYgPSAxLFxuICAgICAgICAgIGlyID0gUkVfQlJFTkRbY2hdO1xuXG4gICAgICAgIGlyLmxhc3RJbmRleCA9IHJlLmxhc3RJbmRleDtcbiAgICAgICAgd2hpbGUgKG1tID0gaXIuZXhlYyhleHByKSkge1xuICAgICAgICAgIGlmIChtbVswXSA9PT0gY2gpIHsgKytsdjsgfVxuICAgICAgICAgIGVsc2UgaWYgKCEtLWx2KSB7IGJyZWFrIH1cbiAgICAgICAgfVxuICAgICAgICByZS5sYXN0SW5kZXggPSBsdiA/IGV4cHIubGVuZ3RoIDogaXIubGFzdEluZGV4O1xuICAgICAgfVxuICAgIH1cblxuICAgIC8vIGlzdGFuYnVsIGlnbm9yZSBuZXh0OiBub3QgYm90aFxuICAgIHZhciAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbWF4LWxlblxuICAgICAgSlNfQ09OVEVYVCA9ICdcImluIHRoaXM/dGhpczonICsgKHR5cGVvZiB3aW5kb3cgIT09ICdvYmplY3QnID8gJ2dsb2JhbCcgOiAnd2luZG93JykgKyAnKS4nLFxuICAgICAgSlNfVkFSTkFNRSA9IC9bLHtdW1xcJFxcd10rKD89Oil8KF4gKnxbXiRcXHdcXC57XSkoPyEoPzp0eXBlb2Z8dHJ1ZXxmYWxzZXxudWxsfHVuZGVmaW5lZHxpbnxpbnN0YW5jZW9mfGlzKD86RmluaXRlfE5hTil8dm9pZHxOYU58bmV3fERhdGV8UmVnRXhwfE1hdGgpKD8hWyRcXHddKSkoWyRfQS1aYS16XVskXFx3XSopL2csXG4gICAgICBKU19OT1BST1BTID0gL14oPz0oXFwuWyRcXHddKykpXFwxKD86W14uWyhdfCQpLztcblxuICAgIGZ1bmN0aW9uIF93cmFwRXhwciAoZXhwciwgYXNUZXh0LCBrZXkpIHtcbiAgICAgIHZhciB0YjtcblxuICAgICAgZXhwciA9IGV4cHIucmVwbGFjZShKU19WQVJOQU1FLCBmdW5jdGlvbiAobWF0Y2gsIHAsIG12YXIsIHBvcywgcykge1xuICAgICAgICBpZiAobXZhcikge1xuICAgICAgICAgIHBvcyA9IHRiID8gMCA6IHBvcyArIG1hdGNoLmxlbmd0aDtcblxuICAgICAgICAgIGlmIChtdmFyICE9PSAndGhpcycgJiYgbXZhciAhPT0gJ2dsb2JhbCcgJiYgbXZhciAhPT0gJ3dpbmRvdycpIHtcbiAgICAgICAgICAgIG1hdGNoID0gcCArICcoXCInICsgbXZhciArIEpTX0NPTlRFWFQgKyBtdmFyO1xuICAgICAgICAgICAgaWYgKHBvcykgeyB0YiA9IChzID0gc1twb3NdKSA9PT0gJy4nIHx8IHMgPT09ICcoJyB8fCBzID09PSAnWyc7IH1cbiAgICAgICAgICB9IGVsc2UgaWYgKHBvcykge1xuICAgICAgICAgICAgdGIgPSAhSlNfTk9QUk9QUy50ZXN0KHMuc2xpY2UocG9zKSk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBtYXRjaFxuICAgICAgfSk7XG5cbiAgICAgIGlmICh0Yikge1xuICAgICAgICBleHByID0gJ3RyeXtyZXR1cm4gJyArIGV4cHIgKyAnfWNhdGNoKGUpe0UoZSx0aGlzKX0nO1xuICAgICAgfVxuXG4gICAgICBpZiAoa2V5KSB7XG5cbiAgICAgICAgZXhwciA9ICh0YlxuICAgICAgICAgICAgPyAnZnVuY3Rpb24oKXsnICsgZXhwciArICd9LmNhbGwodGhpcyknIDogJygnICsgZXhwciArICcpJ1xuICAgICAgICAgICkgKyAnP1wiJyArIGtleSArICdcIjpcIlwiJztcblxuICAgICAgfSBlbHNlIGlmIChhc1RleHQpIHtcblxuICAgICAgICBleHByID0gJ2Z1bmN0aW9uKHYpeycgKyAodGJcbiAgICAgICAgICAgID8gZXhwci5yZXBsYWNlKCdyZXR1cm4gJywgJ3Y9JykgOiAndj0oJyArIGV4cHIgKyAnKSdcbiAgICAgICAgICApICsgJztyZXR1cm4gdnx8dj09PTA/djpcIlwifS5jYWxsKHRoaXMpJztcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGV4cHJcbiAgICB9XG5cbiAgICBfdG1wbC52ZXJzaW9uID0gYnJhY2tldHMudmVyc2lvbiA9ICd2My4wLjgnO1xuXG4gICAgcmV0dXJuIF90bXBsXG5cbiAgfSkoKTtcblxuICAvKiBpc3RhbmJ1bCBpZ25vcmUgbmV4dCAqL1xuICB2YXIgb2JzZXJ2YWJsZSA9IGZ1bmN0aW9uKGVsKSB7XG5cbiAgICAvKipcbiAgICAgKiBFeHRlbmQgdGhlIG9yaWdpbmFsIG9iamVjdCBvciBjcmVhdGUgYSBuZXcgZW1wdHkgb25lXG4gICAgICogQHR5cGUgeyBPYmplY3QgfVxuICAgICAqL1xuXG4gICAgZWwgPSBlbCB8fCB7fTtcblxuICAgIC8qKlxuICAgICAqIFByaXZhdGUgdmFyaWFibGVzXG4gICAgICovXG4gICAgdmFyIGNhbGxiYWNrcyA9IHt9LFxuICAgICAgc2xpY2UgPSBBcnJheS5wcm90b3R5cGUuc2xpY2U7XG5cbiAgICAvKipcbiAgICAgKiBQdWJsaWMgQXBpXG4gICAgICovXG5cbiAgICAvLyBleHRlbmQgdGhlIGVsIG9iamVjdCBhZGRpbmcgdGhlIG9ic2VydmFibGUgbWV0aG9kc1xuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0aWVzKGVsLCB7XG4gICAgICAvKipcbiAgICAgICAqIExpc3RlbiB0byB0aGUgZ2l2ZW4gYGV2ZW50YCBhbmRzXG4gICAgICAgKiBleGVjdXRlIHRoZSBgY2FsbGJhY2tgIGVhY2ggdGltZSBhbiBldmVudCBpcyB0cmlnZ2VyZWQuXG4gICAgICAgKiBAcGFyYW0gIHsgU3RyaW5nIH0gZXZlbnQgLSBldmVudCBpZFxuICAgICAgICogQHBhcmFtICB7IEZ1bmN0aW9uIH0gZm4gLSBjYWxsYmFjayBmdW5jdGlvblxuICAgICAgICogQHJldHVybnMgeyBPYmplY3QgfSBlbFxuICAgICAgICovXG4gICAgICBvbjoge1xuICAgICAgICB2YWx1ZTogZnVuY3Rpb24oZXZlbnQsIGZuKSB7XG4gICAgICAgICAgaWYgKHR5cGVvZiBmbiA9PSAnZnVuY3Rpb24nKVxuICAgICAgICAgICAgeyAoY2FsbGJhY2tzW2V2ZW50XSA9IGNhbGxiYWNrc1tldmVudF0gfHwgW10pLnB1c2goZm4pOyB9XG4gICAgICAgICAgcmV0dXJuIGVsXG4gICAgICAgIH0sXG4gICAgICAgIGVudW1lcmFibGU6IGZhbHNlLFxuICAgICAgICB3cml0YWJsZTogZmFsc2UsXG4gICAgICAgIGNvbmZpZ3VyYWJsZTogZmFsc2VcbiAgICAgIH0sXG5cbiAgICAgIC8qKlxuICAgICAgICogUmVtb3ZlcyB0aGUgZ2l2ZW4gYGV2ZW50YCBsaXN0ZW5lcnNcbiAgICAgICAqIEBwYXJhbSAgIHsgU3RyaW5nIH0gZXZlbnQgLSBldmVudCBpZFxuICAgICAgICogQHBhcmFtICAgeyBGdW5jdGlvbiB9IGZuIC0gY2FsbGJhY2sgZnVuY3Rpb25cbiAgICAgICAqIEByZXR1cm5zIHsgT2JqZWN0IH0gZWxcbiAgICAgICAqL1xuICAgICAgb2ZmOiB7XG4gICAgICAgIHZhbHVlOiBmdW5jdGlvbihldmVudCwgZm4pIHtcbiAgICAgICAgICBpZiAoZXZlbnQgPT0gJyonICYmICFmbikgeyBjYWxsYmFja3MgPSB7fTsgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgaWYgKGZuKSB7XG4gICAgICAgICAgICAgIHZhciBhcnIgPSBjYWxsYmFja3NbZXZlbnRdO1xuICAgICAgICAgICAgICBmb3IgKHZhciBpID0gMCwgY2I7IGNiID0gYXJyICYmIGFycltpXTsgKytpKSB7XG4gICAgICAgICAgICAgICAgaWYgKGNiID09IGZuKSB7IGFyci5zcGxpY2UoaS0tLCAxKTsgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2UgeyBkZWxldGUgY2FsbGJhY2tzW2V2ZW50XTsgfVxuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gZWxcbiAgICAgICAgfSxcbiAgICAgICAgZW51bWVyYWJsZTogZmFsc2UsXG4gICAgICAgIHdyaXRhYmxlOiBmYWxzZSxcbiAgICAgICAgY29uZmlndXJhYmxlOiBmYWxzZVxuICAgICAgfSxcblxuICAgICAgLyoqXG4gICAgICAgKiBMaXN0ZW4gdG8gdGhlIGdpdmVuIGBldmVudGAgYW5kXG4gICAgICAgKiBleGVjdXRlIHRoZSBgY2FsbGJhY2tgIGF0IG1vc3Qgb25jZVxuICAgICAgICogQHBhcmFtICAgeyBTdHJpbmcgfSBldmVudCAtIGV2ZW50IGlkXG4gICAgICAgKiBAcGFyYW0gICB7IEZ1bmN0aW9uIH0gZm4gLSBjYWxsYmFjayBmdW5jdGlvblxuICAgICAgICogQHJldHVybnMgeyBPYmplY3QgfSBlbFxuICAgICAgICovXG4gICAgICBvbmU6IHtcbiAgICAgICAgdmFsdWU6IGZ1bmN0aW9uKGV2ZW50LCBmbikge1xuICAgICAgICAgIGZ1bmN0aW9uIG9uKCkge1xuICAgICAgICAgICAgZWwub2ZmKGV2ZW50LCBvbik7XG4gICAgICAgICAgICBmbi5hcHBseShlbCwgYXJndW1lbnRzKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIGVsLm9uKGV2ZW50LCBvbilcbiAgICAgICAgfSxcbiAgICAgICAgZW51bWVyYWJsZTogZmFsc2UsXG4gICAgICAgIHdyaXRhYmxlOiBmYWxzZSxcbiAgICAgICAgY29uZmlndXJhYmxlOiBmYWxzZVxuICAgICAgfSxcblxuICAgICAgLyoqXG4gICAgICAgKiBFeGVjdXRlIGFsbCBjYWxsYmFjayBmdW5jdGlvbnMgdGhhdCBsaXN0ZW4gdG9cbiAgICAgICAqIHRoZSBnaXZlbiBgZXZlbnRgXG4gICAgICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IGV2ZW50IC0gZXZlbnQgaWRcbiAgICAgICAqIEByZXR1cm5zIHsgT2JqZWN0IH0gZWxcbiAgICAgICAqL1xuICAgICAgdHJpZ2dlcjoge1xuICAgICAgICB2YWx1ZTogZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICB2YXIgYXJndW1lbnRzJDEgPSBhcmd1bWVudHM7XG5cblxuICAgICAgICAgIC8vIGdldHRpbmcgdGhlIGFyZ3VtZW50c1xuICAgICAgICAgIHZhciBhcmdsZW4gPSBhcmd1bWVudHMubGVuZ3RoIC0gMSxcbiAgICAgICAgICAgIGFyZ3MgPSBuZXcgQXJyYXkoYXJnbGVuKSxcbiAgICAgICAgICAgIGZucyxcbiAgICAgICAgICAgIGZuLFxuICAgICAgICAgICAgaTtcblxuICAgICAgICAgIGZvciAoaSA9IDA7IGkgPCBhcmdsZW47IGkrKykge1xuICAgICAgICAgICAgYXJnc1tpXSA9IGFyZ3VtZW50cyQxW2kgKyAxXTsgLy8gc2tpcCBmaXJzdCBhcmd1bWVudFxuICAgICAgICAgIH1cblxuICAgICAgICAgIGZucyA9IHNsaWNlLmNhbGwoY2FsbGJhY2tzW2V2ZW50XSB8fCBbXSwgMCk7XG5cbiAgICAgICAgICBmb3IgKGkgPSAwOyBmbiA9IGZuc1tpXTsgKytpKSB7XG4gICAgICAgICAgICBmbi5hcHBseShlbCwgYXJncyk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYgKGNhbGxiYWNrc1snKiddICYmIGV2ZW50ICE9ICcqJylcbiAgICAgICAgICAgIHsgZWwudHJpZ2dlci5hcHBseShlbCwgWycqJywgZXZlbnRdLmNvbmNhdChhcmdzKSk7IH1cblxuICAgICAgICAgIHJldHVybiBlbFxuICAgICAgICB9LFxuICAgICAgICBlbnVtZXJhYmxlOiBmYWxzZSxcbiAgICAgICAgd3JpdGFibGU6IGZhbHNlLFxuICAgICAgICBjb25maWd1cmFibGU6IGZhbHNlXG4gICAgICB9XG4gICAgfSk7XG5cbiAgICByZXR1cm4gZWxcblxuICB9O1xuXG4gIC8qKlxuICAgKiBTaG9ydCBhbGlhcyBmb3IgT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvclxuICAgKi9cbiAgZnVuY3Rpb24gZ2V0UHJvcERlc2NyaXB0b3IgKG8sIGspIHtcbiAgICByZXR1cm4gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihvLCBrKVxuICB9XG5cbiAgLyoqXG4gICAqIENoZWNrIGlmIHBhc3NlZCBhcmd1bWVudCBpcyB1bmRlZmluZWRcbiAgICogQHBhcmFtICAgeyAqIH0gdmFsdWUgLVxuICAgKiBAcmV0dXJucyB7IEJvb2xlYW4gfSAtXG4gICAqL1xuICBmdW5jdGlvbiBpc1VuZGVmaW5lZCh2YWx1ZSkge1xuICAgIHJldHVybiB0eXBlb2YgdmFsdWUgPT09IFRfVU5ERUZcbiAgfVxuXG4gIC8qKlxuICAgKiBDaGVjayB3aGV0aGVyIG9iamVjdCdzIHByb3BlcnR5IGNvdWxkIGJlIG92ZXJyaWRkZW5cbiAgICogQHBhcmFtICAgeyBPYmplY3QgfSAgb2JqIC0gc291cmNlIG9iamVjdFxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9ICBrZXkgLSBvYmplY3QgcHJvcGVydHlcbiAgICogQHJldHVybnMgeyBCb29sZWFuIH0gdHJ1ZSBpZiB3cml0YWJsZVxuICAgKi9cbiAgZnVuY3Rpb24gaXNXcml0YWJsZShvYmosIGtleSkge1xuICAgIHZhciBkZXNjcmlwdG9yID0gZ2V0UHJvcERlc2NyaXB0b3Iob2JqLCBrZXkpO1xuICAgIHJldHVybiBpc1VuZGVmaW5lZChvYmpba2V5XSkgfHwgZGVzY3JpcHRvciAmJiBkZXNjcmlwdG9yLndyaXRhYmxlXG4gIH1cblxuICAvKipcbiAgICogRXh0ZW5kIGFueSBvYmplY3Qgd2l0aCBvdGhlciBwcm9wZXJ0aWVzXG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gc3JjIC0gc291cmNlIG9iamVjdFxuICAgKiBAcmV0dXJucyB7IE9iamVjdCB9IHRoZSByZXN1bHRpbmcgZXh0ZW5kZWQgb2JqZWN0XG4gICAqXG4gICAqIHZhciBvYmogPSB7IGZvbzogJ2JheicgfVxuICAgKiBleHRlbmQob2JqLCB7YmFyOiAnYmFyJywgZm9vOiAnYmFyJ30pXG4gICAqIGNvbnNvbGUubG9nKG9iaikgPT4ge2JhcjogJ2JhcicsIGZvbzogJ2Jhcid9XG4gICAqXG4gICAqL1xuICBmdW5jdGlvbiBleHRlbmQoc3JjKSB7XG4gICAgdmFyIG9iajtcbiAgICB2YXIgaSA9IDE7XG4gICAgdmFyIGFyZ3MgPSBhcmd1bWVudHM7XG4gICAgdmFyIGwgPSBhcmdzLmxlbmd0aDtcblxuICAgIGZvciAoOyBpIDwgbDsgaSsrKSB7XG4gICAgICBpZiAob2JqID0gYXJnc1tpXSkge1xuICAgICAgICBmb3IgKHZhciBrZXkgaW4gb2JqKSB7XG4gICAgICAgICAgLy8gY2hlY2sgaWYgdGhpcyBwcm9wZXJ0eSBvZiB0aGUgc291cmNlIG9iamVjdCBjb3VsZCBiZSBvdmVycmlkZGVuXG4gICAgICAgICAgaWYgKGlzV3JpdGFibGUoc3JjLCBrZXkpKVxuICAgICAgICAgICAgeyBzcmNba2V5XSA9IG9ialtrZXldOyB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHNyY1xuICB9XG5cbiAgLyoqXG4gICAqIEFsaWFzIGZvciBPYmplY3QuY3JlYXRlXG4gICAqL1xuICBmdW5jdGlvbiBjcmVhdGUoc3JjKSB7XG4gICAgcmV0dXJuIE9iamVjdC5jcmVhdGUoc3JjKVxuICB9XG5cbiAgdmFyIHNldHRpbmdzID0gZXh0ZW5kKGNyZWF0ZShicmFja2V0cy5zZXR0aW5ncyksIHtcbiAgICBza2lwQW5vbnltb3VzVGFnczogdHJ1ZSxcbiAgICAvLyBoYW5kbGUgdGhlIGF1dG8gdXBkYXRlcyBvbiBhbnkgRE9NIGV2ZW50XG4gICAgYXV0b1VwZGF0ZTogdHJ1ZVxuICB9KVxuXG4gIC8qKlxuICAgKiBTaG9ydGVyIGFuZCBmYXN0IHdheSB0byBzZWxlY3QgbXVsdGlwbGUgbm9kZXMgaW4gdGhlIERPTVxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IHNlbGVjdG9yIC0gRE9NIHNlbGVjdG9yXG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gY3R4IC0gRE9NIG5vZGUgd2hlcmUgdGhlIHRhcmdldHMgb2Ygb3VyIHNlYXJjaCB3aWxsIGlzIGxvY2F0ZWRcbiAgICogQHJldHVybnMgeyBPYmplY3QgfSBkb20gbm9kZXMgZm91bmRcbiAgICovXG4gIGZ1bmN0aW9uICQkKHNlbGVjdG9yLCBjdHgpIHtcbiAgICByZXR1cm4gW10uc2xpY2UuY2FsbCgoY3R4IHx8IGRvY3VtZW50KS5xdWVyeVNlbGVjdG9yQWxsKHNlbGVjdG9yKSlcbiAgfVxuXG4gIC8qKlxuICAgKiBDcmVhdGUgYSBkb2N1bWVudCB0ZXh0IG5vZGVcbiAgICogQHJldHVybnMgeyBPYmplY3QgfSBjcmVhdGUgYSB0ZXh0IG5vZGUgdG8gdXNlIGFzIHBsYWNlaG9sZGVyXG4gICAqL1xuICBmdW5jdGlvbiBjcmVhdGVET01QbGFjZWhvbGRlcigpIHtcbiAgICByZXR1cm4gZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoJycpXG4gIH1cblxuICAvKipcbiAgICogVG9nZ2xlIHRoZSB2aXNpYmlsaXR5IG9mIGFueSBET00gbm9kZVxuICAgKiBAcGFyYW0gICB7IE9iamVjdCB9ICBkb20gLSBET00gbm9kZSB3ZSB3YW50IHRvIGhpZGVcbiAgICogQHBhcmFtICAgeyBCb29sZWFuIH0gc2hvdyAtIGRvIHdlIHdhbnQgdG8gc2hvdyBpdD9cbiAgICovXG5cbiAgZnVuY3Rpb24gdG9nZ2xlVmlzaWJpbGl0eShkb20sIHNob3cpIHtcbiAgICBkb20uc3R5bGUuZGlzcGxheSA9IHNob3cgPyAnJyA6ICdub25lJztcbiAgICBkb20uaGlkZGVuID0gc2hvdyA/IGZhbHNlIDogdHJ1ZTtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXQgdGhlIHZhbHVlIG9mIGFueSBET00gYXR0cmlidXRlIG9uIGEgbm9kZVxuICAgKiBAcGFyYW0gICB7IE9iamVjdCB9IGRvbSAtIERPTSBub2RlIHdlIHdhbnQgdG8gcGFyc2VcbiAgICogQHBhcmFtICAgeyBTdHJpbmcgfSBuYW1lIC0gbmFtZSBvZiB0aGUgYXR0cmlidXRlIHdlIHdhbnQgdG8gZ2V0XG4gICAqIEByZXR1cm5zIHsgU3RyaW5nIHwgdW5kZWZpbmVkIH0gbmFtZSBvZiB0aGUgbm9kZSBhdHRyaWJ1dGUgd2hldGhlciBpdCBleGlzdHNcbiAgICovXG4gIGZ1bmN0aW9uIGdldEF0dHJpYnV0ZShkb20sIG5hbWUpIHtcbiAgICByZXR1cm4gZG9tLmdldEF0dHJpYnV0ZShuYW1lKVxuICB9XG5cbiAgLyoqXG4gICAqIFJlbW92ZSBhbnkgRE9NIGF0dHJpYnV0ZSBmcm9tIGEgbm9kZVxuICAgKiBAcGFyYW0gICB7IE9iamVjdCB9IGRvbSAtIERPTSBub2RlIHdlIHdhbnQgdG8gdXBkYXRlXG4gICAqIEBwYXJhbSAgIHsgU3RyaW5nIH0gbmFtZSAtIG5hbWUgb2YgdGhlIHByb3BlcnR5IHdlIHdhbnQgdG8gcmVtb3ZlXG4gICAqL1xuICBmdW5jdGlvbiByZW1vdmVBdHRyaWJ1dGUoZG9tLCBuYW1lKSB7XG4gICAgZG9tLnJlbW92ZUF0dHJpYnV0ZShuYW1lKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBTZXQgdGhlIGlubmVyIGh0bWwgb2YgYW55IERPTSBub2RlIFNWR3MgaW5jbHVkZWRcbiAgICogQHBhcmFtIHsgT2JqZWN0IH0gY29udGFpbmVyIC0gRE9NIG5vZGUgd2hlcmUgd2UnbGwgaW5qZWN0IG5ldyBodG1sXG4gICAqIEBwYXJhbSB7IFN0cmluZyB9IGh0bWwgLSBodG1sIHRvIGluamVjdFxuICAgKiBAcGFyYW0geyBCb29sZWFuIH0gaXNTdmcgLSBzdmcgdGFncyBzaG91bGQgYmUgdHJlYXRlZCBhIGJpdCBkaWZmZXJlbnRseVxuICAgKi9cbiAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cbiAgZnVuY3Rpb24gc2V0SW5uZXJIVE1MKGNvbnRhaW5lciwgaHRtbCwgaXNTdmcpIHtcbiAgICAvLyBpbm5lckhUTUwgaXMgbm90IHN1cHBvcnRlZCBvbiBzdmcgdGFncyBzbyB3ZSBuZWV0IHRvIHRyZWF0IHRoZW0gZGlmZmVyZW50bHlcbiAgICBpZiAoaXNTdmcpIHtcbiAgICAgIHZhciBub2RlID0gY29udGFpbmVyLm93bmVyRG9jdW1lbnQuaW1wb3J0Tm9kZShcbiAgICAgICAgbmV3IERPTVBhcnNlcigpXG4gICAgICAgICAgLnBhcnNlRnJvbVN0cmluZygoXCI8c3ZnIHhtbG5zPVxcXCJcIiArIFNWR19OUyArIFwiXFxcIj5cIiArIGh0bWwgKyBcIjwvc3ZnPlwiKSwgJ2FwcGxpY2F0aW9uL3htbCcpXG4gICAgICAgICAgLmRvY3VtZW50RWxlbWVudCxcbiAgICAgICAgdHJ1ZVxuICAgICAgKTtcblxuICAgICAgY29udGFpbmVyLmFwcGVuZENoaWxkKG5vZGUpO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb250YWluZXIuaW5uZXJIVE1MID0gaHRtbDtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogTWluaW1pemUgcmlzazogb25seSB6ZXJvIG9yIG9uZSBfc3BhY2VfIGJldHdlZW4gYXR0ciAmIHZhbHVlXG4gICAqIEBwYXJhbSAgIHsgU3RyaW5nIH0gICBodG1sIC0gaHRtbCBzdHJpbmcgd2Ugd2FudCB0byBwYXJzZVxuICAgKiBAcGFyYW0gICB7IEZ1bmN0aW9uIH0gZm4gLSBjYWxsYmFjayBmdW5jdGlvbiB0byBhcHBseSBvbiBhbnkgYXR0cmlidXRlIGZvdW5kXG4gICAqL1xuICBmdW5jdGlvbiB3YWxrQXR0cmlidXRlcyhodG1sLCBmbikge1xuICAgIGlmICghaHRtbCkgeyByZXR1cm4gfVxuICAgIHZhciBtO1xuICAgIHdoaWxlIChtID0gUkVfSFRNTF9BVFRSUy5leGVjKGh0bWwpKVxuICAgICAgeyBmbihtWzFdLnRvTG93ZXJDYXNlKCksIG1bMl0gfHwgbVszXSB8fCBtWzRdKTsgfVxuICB9XG5cbiAgLyoqXG4gICAqIENyZWF0ZSBhIGRvY3VtZW50IGZyYWdtZW50XG4gICAqIEByZXR1cm5zIHsgT2JqZWN0IH0gZG9jdW1lbnQgZnJhZ21lbnRcbiAgICovXG4gIGZ1bmN0aW9uIGNyZWF0ZUZyYWdtZW50KCkge1xuICAgIHJldHVybiBkb2N1bWVudC5jcmVhdGVEb2N1bWVudEZyYWdtZW50KClcbiAgfVxuXG4gIC8qKlxuICAgKiBJbnNlcnQgc2FmZWx5IGEgdGFnIHRvIGZpeCAjMTk2MiAjMTY0OVxuICAgKiBAcGFyYW0gICB7IEhUTUxFbGVtZW50IH0gcm9vdCAtIGNoaWxkcmVuIGNvbnRhaW5lclxuICAgKiBAcGFyYW0gICB7IEhUTUxFbGVtZW50IH0gY3VyciAtIG5vZGUgdG8gaW5zZXJ0XG4gICAqIEBwYXJhbSAgIHsgSFRNTEVsZW1lbnQgfSBuZXh0IC0gbm9kZSB0aGF0IHNob3VsZCBwcmVjZWVkIHRoZSBjdXJyZW50IG5vZGUgaW5zZXJ0ZWRcbiAgICovXG4gIGZ1bmN0aW9uIHNhZmVJbnNlcnQocm9vdCwgY3VyciwgbmV4dCkge1xuICAgIHJvb3QuaW5zZXJ0QmVmb3JlKGN1cnIsIG5leHQucGFyZW50Tm9kZSAmJiBuZXh0KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBDb252ZXJ0IGEgc3R5bGUgb2JqZWN0IHRvIGEgc3RyaW5nXG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gc3R5bGUgLSBzdHlsZSBvYmplY3Qgd2UgbmVlZCB0byBwYXJzZVxuICAgKiBAcmV0dXJucyB7IFN0cmluZyB9IHJlc3VsdGluZyBjc3Mgc3RyaW5nXG4gICAqIEBleGFtcGxlXG4gICAqIHN0eWxlT2JqZWN0VG9TdHJpbmcoeyBjb2xvcjogJ3JlZCcsIGhlaWdodDogJzEwcHgnfSkgLy8gPT4gJ2NvbG9yOiByZWQ7IGhlaWdodDogMTBweCdcbiAgICovXG4gIGZ1bmN0aW9uIHN0eWxlT2JqZWN0VG9TdHJpbmcoc3R5bGUpIHtcbiAgICByZXR1cm4gT2JqZWN0LmtleXMoc3R5bGUpLnJlZHVjZShmdW5jdGlvbiAoYWNjLCBwcm9wKSB7XG4gICAgICByZXR1cm4gKGFjYyArIFwiIFwiICsgcHJvcCArIFwiOiBcIiArIChzdHlsZVtwcm9wXSkgKyBcIjtcIilcbiAgICB9LCAnJylcbiAgfVxuXG4gIC8qKlxuICAgKiBXYWxrIGRvd24gcmVjdXJzaXZlbHkgYWxsIHRoZSBjaGlsZHJlbiB0YWdzIHN0YXJ0aW5nIGRvbSBub2RlXG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gICBkb20gLSBzdGFydGluZyBub2RlIHdoZXJlIHdlIHdpbGwgc3RhcnQgdGhlIHJlY3Vyc2lvblxuICAgKiBAcGFyYW0gICB7IEZ1bmN0aW9uIH0gZm4gLSBjYWxsYmFjayB0byB0cmFuc2Zvcm0gdGhlIGNoaWxkIG5vZGUganVzdCBmb3VuZFxuICAgKiBAcGFyYW0gICB7IE9iamVjdCB9ICAgY29udGV4dCAtIGZuIGNhbiBvcHRpb25hbGx5IHJldHVybiBhbiBvYmplY3QsIHdoaWNoIGlzIHBhc3NlZCB0byBjaGlsZHJlblxuICAgKi9cbiAgZnVuY3Rpb24gd2Fsa05vZGVzKGRvbSwgZm4sIGNvbnRleHQpIHtcbiAgICBpZiAoZG9tKSB7XG4gICAgICB2YXIgcmVzID0gZm4oZG9tLCBjb250ZXh0KTtcbiAgICAgIHZhciBuZXh0O1xuICAgICAgLy8gc3RvcCB0aGUgcmVjdXJzaW9uXG4gICAgICBpZiAocmVzID09PSBmYWxzZSkgeyByZXR1cm4gfVxuXG4gICAgICBkb20gPSBkb20uZmlyc3RDaGlsZDtcblxuICAgICAgd2hpbGUgKGRvbSkge1xuICAgICAgICBuZXh0ID0gZG9tLm5leHRTaWJsaW5nO1xuICAgICAgICB3YWxrTm9kZXMoZG9tLCBmbiwgcmVzKTtcbiAgICAgICAgZG9tID0gbmV4dDtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuXG5cbiAgdmFyIGRvbSA9IC8qI19fUFVSRV9fKi9PYmplY3QuZnJlZXplKHtcbiAgICAkJDogJCQsXG4gICAgJDogJCxcbiAgICBjcmVhdGVET01QbGFjZWhvbGRlcjogY3JlYXRlRE9NUGxhY2Vob2xkZXIsXG4gICAgbWtFbDogbWFrZUVsZW1lbnQsXG4gICAgc2V0QXR0cjogc2V0QXR0cmlidXRlLFxuICAgIHRvZ2dsZVZpc2liaWxpdHk6IHRvZ2dsZVZpc2liaWxpdHksXG4gICAgZ2V0QXR0cjogZ2V0QXR0cmlidXRlLFxuICAgIHJlbUF0dHI6IHJlbW92ZUF0dHJpYnV0ZSxcbiAgICBzZXRJbm5lckhUTUw6IHNldElubmVySFRNTCxcbiAgICB3YWxrQXR0cnM6IHdhbGtBdHRyaWJ1dGVzLFxuICAgIGNyZWF0ZUZyYWc6IGNyZWF0ZUZyYWdtZW50LFxuICAgIHNhZmVJbnNlcnQ6IHNhZmVJbnNlcnQsXG4gICAgc3R5bGVPYmplY3RUb1N0cmluZzogc3R5bGVPYmplY3RUb1N0cmluZyxcbiAgICB3YWxrTm9kZXM6IHdhbGtOb2Rlc1xuICB9KTtcblxuICAvKipcbiAgICogQ2hlY2sgYWdhaW5zdCB0aGUgbnVsbCBhbmQgdW5kZWZpbmVkIHZhbHVlc1xuICAgKiBAcGFyYW0gICB7ICogfSAgdmFsdWUgLVxuICAgKiBAcmV0dXJucyB7Qm9vbGVhbn0gLVxuICAgKi9cbiAgZnVuY3Rpb24gaXNOaWwodmFsdWUpIHtcbiAgICByZXR1cm4gaXNVbmRlZmluZWQodmFsdWUpIHx8IHZhbHVlID09PSBudWxsXG4gIH1cblxuICAvKipcbiAgICogQ2hlY2sgaWYgcGFzc2VkIGFyZ3VtZW50IGlzIGVtcHR5LiBEaWZmZXJlbnQgZnJvbSBmYWxzeSwgYmVjYXVzZSB3ZSBkb250IGNvbnNpZGVyIDAgb3IgZmFsc2UgdG8gYmUgYmxhbmtcbiAgICogQHBhcmFtIHsgKiB9IHZhbHVlIC1cbiAgICogQHJldHVybnMgeyBCb29sZWFuIH0gLVxuICAgKi9cbiAgZnVuY3Rpb24gaXNCbGFuayh2YWx1ZSkge1xuICAgIHJldHVybiBpc05pbCh2YWx1ZSkgfHwgdmFsdWUgPT09ICcnXG4gIH1cblxuICAvKipcbiAgICogQ2hlY2sgaWYgcGFzc2VkIGFyZ3VtZW50IGlzIGEgZnVuY3Rpb25cbiAgICogQHBhcmFtICAgeyAqIH0gdmFsdWUgLVxuICAgKiBAcmV0dXJucyB7IEJvb2xlYW4gfSAtXG4gICAqL1xuICBmdW5jdGlvbiBpc0Z1bmN0aW9uKHZhbHVlKSB7XG4gICAgcmV0dXJuIHR5cGVvZiB2YWx1ZSA9PT0gVF9GVU5DVElPTlxuICB9XG5cbiAgLyoqXG4gICAqIENoZWNrIGlmIHBhc3NlZCBhcmd1bWVudCBpcyBhbiBvYmplY3QsIGV4Y2x1ZGUgbnVsbFxuICAgKiBOT1RFOiB1c2UgaXNPYmplY3QoeCkgJiYgIWlzQXJyYXkoeCkgdG8gZXhjbHVkZXMgYXJyYXlzLlxuICAgKiBAcGFyYW0gICB7ICogfSB2YWx1ZSAtXG4gICAqIEByZXR1cm5zIHsgQm9vbGVhbiB9IC1cbiAgICovXG4gIGZ1bmN0aW9uIGlzT2JqZWN0KHZhbHVlKSB7XG4gICAgcmV0dXJuIHZhbHVlICYmIHR5cGVvZiB2YWx1ZSA9PT0gVF9PQkpFQ1QgLy8gdHlwZW9mIG51bGwgaXMgJ29iamVjdCdcbiAgfVxuXG4gIC8qKlxuICAgKiBDaGVjayBpZiBhIERPTSBub2RlIGlzIGFuIHN2ZyB0YWcgb3IgcGFydCBvZiBhbiBzdmdcbiAgICogQHBhcmFtICAgeyBIVE1MRWxlbWVudCB9ICBlbCAtIG5vZGUgd2Ugd2FudCB0byB0ZXN0XG4gICAqIEByZXR1cm5zIHtCb29sZWFufSB0cnVlIGlmIGl0J3MgYW4gc3ZnIG5vZGVcbiAgICovXG4gIGZ1bmN0aW9uIGlzU3ZnKGVsKSB7XG4gICAgdmFyIG93bmVyID0gZWwub3duZXJTVkdFbGVtZW50O1xuICAgIHJldHVybiAhIW93bmVyIHx8IG93bmVyID09PSBudWxsXG4gIH1cblxuICAvKipcbiAgICogQ2hlY2sgaWYgcGFzc2VkIGFyZ3VtZW50IGlzIGEga2luZCBvZiBhcnJheVxuICAgKiBAcGFyYW0gICB7ICogfSB2YWx1ZSAtXG4gICAqIEByZXR1cm5zIHsgQm9vbGVhbiB9IC1cbiAgICovXG4gIGZ1bmN0aW9uIGlzQXJyYXkodmFsdWUpIHtcbiAgICByZXR1cm4gQXJyYXkuaXNBcnJheSh2YWx1ZSkgfHwgdmFsdWUgaW5zdGFuY2VvZiBBcnJheVxuICB9XG5cbiAgLyoqXG4gICAqIENoZWNrIGlmIHRoZSBwYXNzZWQgYXJndW1lbnQgaXMgYSBib29sZWFuIGF0dHJpYnV0ZVxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IHZhbHVlIC1cbiAgICogQHJldHVybnMgeyBCb29sZWFuIH0gLVxuICAgKi9cbiAgZnVuY3Rpb24gaXNCb29sQXR0cih2YWx1ZSkge1xuICAgIHJldHVybiBSRV9CT09MX0FUVFJTLnRlc3QodmFsdWUpXG4gIH1cblxuICAvKipcbiAgICogQ2hlY2sgaWYgcGFzc2VkIGFyZ3VtZW50IGlzIGEgc3RyaW5nXG4gICAqIEBwYXJhbSAgIHsgKiB9IHZhbHVlIC1cbiAgICogQHJldHVybnMgeyBCb29sZWFuIH0gLVxuICAgKi9cbiAgZnVuY3Rpb24gaXNTdHJpbmcodmFsdWUpIHtcbiAgICByZXR1cm4gdHlwZW9mIHZhbHVlID09PSBUX1NUUklOR1xuICB9XG5cblxuXG4gIHZhciBjaGVjayA9IC8qI19fUFVSRV9fKi9PYmplY3QuZnJlZXplKHtcbiAgICBpc0JsYW5rOiBpc0JsYW5rLFxuICAgIGlzRnVuY3Rpb246IGlzRnVuY3Rpb24sXG4gICAgaXNPYmplY3Q6IGlzT2JqZWN0LFxuICAgIGlzU3ZnOiBpc1N2ZyxcbiAgICBpc1dyaXRhYmxlOiBpc1dyaXRhYmxlLFxuICAgIGlzQXJyYXk6IGlzQXJyYXksXG4gICAgaXNCb29sQXR0cjogaXNCb29sQXR0cixcbiAgICBpc05pbDogaXNOaWwsXG4gICAgaXNTdHJpbmc6IGlzU3RyaW5nLFxuICAgIGlzVW5kZWZpbmVkOiBpc1VuZGVmaW5lZFxuICB9KTtcblxuICAvKipcbiAgICogQ2hlY2sgd2hldGhlciBhbiBhcnJheSBjb250YWlucyBhbiBpdGVtXG4gICAqIEBwYXJhbSAgIHsgQXJyYXkgfSBhcnJheSAtIHRhcmdldCBhcnJheVxuICAgKiBAcGFyYW0gICB7ICogfSBpdGVtIC0gaXRlbSB0byB0ZXN0XG4gICAqIEByZXR1cm5zIHsgQm9vbGVhbiB9IC1cbiAgICovXG4gIGZ1bmN0aW9uIGNvbnRhaW5zKGFycmF5LCBpdGVtKSB7XG4gICAgcmV0dXJuIGFycmF5LmluZGV4T2YoaXRlbSkgIT09IC0xXG4gIH1cblxuICAvKipcbiAgICogU3BlY2lhbGl6ZWQgZnVuY3Rpb24gZm9yIGxvb3BpbmcgYW4gYXJyYXktbGlrZSBjb2xsZWN0aW9uIHdpdGggYGVhY2g9e31gXG4gICAqIEBwYXJhbSAgIHsgQXJyYXkgfSBsaXN0IC0gY29sbGVjdGlvbiBvZiBpdGVtc1xuICAgKiBAcGFyYW0gICB7RnVuY3Rpb259IGZuIC0gY2FsbGJhY2sgZnVuY3Rpb25cbiAgICogQHJldHVybnMgeyBBcnJheSB9IHRoZSBhcnJheSBsb29wZWRcbiAgICovXG4gIGZ1bmN0aW9uIGVhY2gobGlzdCwgZm4pIHtcbiAgICB2YXIgbGVuID0gbGlzdCA/IGxpc3QubGVuZ3RoIDogMDtcbiAgICB2YXIgaSA9IDA7XG4gICAgZm9yICg7IGkgPCBsZW47IGkrKykgeyBmbihsaXN0W2ldLCBpKTsgfVxuICAgIHJldHVybiBsaXN0XG4gIH1cblxuICAvKipcbiAgICogRmFzdGVyIFN0cmluZyBzdGFydHNXaXRoIGFsdGVybmF0aXZlXG4gICAqIEBwYXJhbSAgIHsgU3RyaW5nIH0gc3RyIC0gc291cmNlIHN0cmluZ1xuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IHZhbHVlIC0gdGVzdCBzdHJpbmdcbiAgICogQHJldHVybnMgeyBCb29sZWFuIH0gLVxuICAgKi9cbiAgZnVuY3Rpb24gc3RhcnRzV2l0aChzdHIsIHZhbHVlKSB7XG4gICAgcmV0dXJuIHN0ci5zbGljZSgwLCB2YWx1ZS5sZW5ndGgpID09PSB2YWx1ZVxuICB9XG5cbiAgLyoqXG4gICAqIEZ1bmN0aW9uIHJldHVybmluZyBhbHdheXMgYSB1bmlxdWUgaWRlbnRpZmllclxuICAgKiBAcmV0dXJucyB7IE51bWJlciB9IC0gbnVtYmVyIGZyb20gMC4uLm5cbiAgICovXG4gIHZhciB1aWQgPSAoZnVuY3Rpb24gdWlkKCkge1xuICAgIHZhciBpID0gLTE7XG4gICAgcmV0dXJuIGZ1bmN0aW9uICgpIHsgcmV0dXJuICsraTsgfVxuICB9KSgpXG5cbiAgLyoqXG4gICAqIEhlbHBlciBmdW5jdGlvbiB0byBzZXQgYW4gaW1tdXRhYmxlIHByb3BlcnR5XG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gZWwgLSBvYmplY3Qgd2hlcmUgdGhlIG5ldyBwcm9wZXJ0eSB3aWxsIGJlIHNldFxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IGtleSAtIG9iamVjdCBrZXkgd2hlcmUgdGhlIG5ldyBwcm9wZXJ0eSB3aWxsIGJlIHN0b3JlZFxuICAgKiBAcGFyYW0gICB7ICogfSB2YWx1ZSAtIHZhbHVlIG9mIHRoZSBuZXcgcHJvcGVydHlcbiAgICogQHBhcmFtICAgeyBPYmplY3QgfSBvcHRpb25zIC0gc2V0IHRoZSBwcm9wZXJ5IG92ZXJyaWRpbmcgdGhlIGRlZmF1bHQgb3B0aW9uc1xuICAgKiBAcmV0dXJucyB7IE9iamVjdCB9IC0gdGhlIGluaXRpYWwgb2JqZWN0XG4gICAqL1xuICBmdW5jdGlvbiBkZWZpbmUoZWwsIGtleSwgdmFsdWUsIG9wdGlvbnMpIHtcbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkoZWwsIGtleSwgZXh0ZW5kKHtcbiAgICAgIHZhbHVlOiB2YWx1ZSxcbiAgICAgIGVudW1lcmFibGU6IGZhbHNlLFxuICAgICAgd3JpdGFibGU6IGZhbHNlLFxuICAgICAgY29uZmlndXJhYmxlOiB0cnVlXG4gICAgfSwgb3B0aW9ucykpO1xuICAgIHJldHVybiBlbFxuICB9XG5cbiAgLyoqXG4gICAqIENvbnZlcnQgYSBzdHJpbmcgY29udGFpbmluZyBkYXNoZXMgdG8gY2FtZWwgY2FzZVxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IHN0ciAtIGlucHV0IHN0cmluZ1xuICAgKiBAcmV0dXJucyB7IFN0cmluZyB9IG15LXN0cmluZyAtPiBteVN0cmluZ1xuICAgKi9cbiAgZnVuY3Rpb24gdG9DYW1lbChzdHIpIHtcbiAgICByZXR1cm4gc3RyLnJlcGxhY2UoLy0oXFx3KS9nLCBmdW5jdGlvbiAoXywgYykgeyByZXR1cm4gYy50b1VwcGVyQ2FzZSgpOyB9KVxuICB9XG5cbiAgLyoqXG4gICAqIFdhcm4gYSBtZXNzYWdlIHZpYSBjb25zb2xlXG4gICAqIEBwYXJhbSAgIHtTdHJpbmd9IG1lc3NhZ2UgLSB3YXJuaW5nIG1lc3NhZ2VcbiAgICovXG4gIGZ1bmN0aW9uIHdhcm4obWVzc2FnZSkge1xuICAgIGlmIChjb25zb2xlICYmIGNvbnNvbGUud2FybikgeyBjb25zb2xlLndhcm4obWVzc2FnZSk7IH1cbiAgfVxuXG5cblxuICB2YXIgbWlzYyA9IC8qI19fUFVSRV9fKi9PYmplY3QuZnJlZXplKHtcbiAgICBjb250YWluczogY29udGFpbnMsXG4gICAgZWFjaDogZWFjaCxcbiAgICBnZXRQcm9wRGVzY3JpcHRvcjogZ2V0UHJvcERlc2NyaXB0b3IsXG4gICAgc3RhcnRzV2l0aDogc3RhcnRzV2l0aCxcbiAgICB1aWQ6IHVpZCxcbiAgICBkZWZpbmVQcm9wZXJ0eTogZGVmaW5lLFxuICAgIG9iamVjdENyZWF0ZTogY3JlYXRlLFxuICAgIGV4dGVuZDogZXh0ZW5kLFxuICAgIHRvQ2FtZWw6IHRvQ2FtZWwsXG4gICAgd2Fybjogd2FyblxuICB9KTtcblxuICAvKipcbiAgICogU2V0IHRoZSBwcm9wZXJ0eSBvZiBhbiBvYmplY3QgZm9yIGEgZ2l2ZW4ga2V5LiBJZiBzb21ldGhpbmcgYWxyZWFkeVxuICAgKiBleGlzdHMgdGhlcmUsIHRoZW4gaXQgYmVjb21lcyBhbiBhcnJheSBjb250YWluaW5nIGJvdGggdGhlIG9sZCBhbmQgbmV3IHZhbHVlLlxuICAgKiBAcGFyYW0geyBPYmplY3QgfSBvYmogLSBvYmplY3Qgb24gd2hpY2ggdG8gc2V0IHRoZSBwcm9wZXJ0eVxuICAgKiBAcGFyYW0geyBTdHJpbmcgfSBrZXkgLSBwcm9wZXJ0eSBuYW1lXG4gICAqIEBwYXJhbSB7IE9iamVjdCB9IHZhbHVlIC0gdGhlIHZhbHVlIG9mIHRoZSBwcm9wZXJ0eSB0byBiZSBzZXRcbiAgICogQHBhcmFtIHsgQm9vbGVhbiB9IGVuc3VyZUFycmF5IC0gZW5zdXJlIHRoYXQgdGhlIHByb3BlcnR5IHJlbWFpbnMgYW4gYXJyYXlcbiAgICogQHBhcmFtIHsgTnVtYmVyIH0gaW5kZXggLSBhZGQgdGhlIG5ldyBpdGVtIGluIGEgY2VydGFpbiBhcnJheSBwb3NpdGlvblxuICAgKi9cbiAgZnVuY3Rpb24gYXJyYXlpc2hBZGQob2JqLCBrZXksIHZhbHVlLCBlbnN1cmVBcnJheSwgaW5kZXgpIHtcbiAgICB2YXIgZGVzdCA9IG9ialtrZXldO1xuICAgIHZhciBpc0FyciA9IGlzQXJyYXkoZGVzdCk7XG4gICAgdmFyIGhhc0luZGV4ID0gIWlzVW5kZWZpbmVkKGluZGV4KTtcblxuICAgIGlmIChkZXN0ICYmIGRlc3QgPT09IHZhbHVlKSB7IHJldHVybiB9XG5cbiAgICAvLyBpZiB0aGUga2V5IHdhcyBuZXZlciBzZXQsIHNldCBpdCBvbmNlXG4gICAgaWYgKCFkZXN0ICYmIGVuc3VyZUFycmF5KSB7IG9ialtrZXldID0gW3ZhbHVlXTsgfVxuICAgIGVsc2UgaWYgKCFkZXN0KSB7IG9ialtrZXldID0gdmFsdWU7IH1cbiAgICAvLyBpZiBpdCB3YXMgYW4gYXJyYXkgYW5kIG5vdCB5ZXQgc2V0XG4gICAgZWxzZSB7XG4gICAgICBpZiAoaXNBcnIpIHtcbiAgICAgICAgdmFyIG9sZEluZGV4ID0gZGVzdC5pbmRleE9mKHZhbHVlKTtcbiAgICAgICAgLy8gdGhpcyBpdGVtIG5ldmVyIGNoYW5nZWQgaXRzIHBvc2l0aW9uXG4gICAgICAgIGlmIChvbGRJbmRleCA9PT0gaW5kZXgpIHsgcmV0dXJuIH1cbiAgICAgICAgLy8gcmVtb3ZlIHRoZSBpdGVtIGZyb20gaXRzIG9sZCBwb3NpdGlvblxuICAgICAgICBpZiAob2xkSW5kZXggIT09IC0xKSB7IGRlc3Quc3BsaWNlKG9sZEluZGV4LCAxKTsgfVxuICAgICAgICAvLyBtb3ZlIG9yIGFkZCB0aGUgaXRlbVxuICAgICAgICBpZiAoaGFzSW5kZXgpIHtcbiAgICAgICAgICBkZXN0LnNwbGljZShpbmRleCwgMCwgdmFsdWUpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGRlc3QucHVzaCh2YWx1ZSk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7IG9ialtrZXldID0gW2Rlc3QsIHZhbHVlXTsgfVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBEZXRlY3QgdGhlIHRhZyBpbXBsZW1lbnRhdGlvbiBieSBhIERPTSBub2RlXG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gZG9tIC0gRE9NIG5vZGUgd2UgbmVlZCB0byBwYXJzZSB0byBnZXQgaXRzIHRhZyBpbXBsZW1lbnRhdGlvblxuICAgKiBAcmV0dXJucyB7IE9iamVjdCB9IGl0IHJldHVybnMgYW4gb2JqZWN0IGNvbnRhaW5pbmcgdGhlIGltcGxlbWVudGF0aW9uIG9mIGEgY3VzdG9tIHRhZyAodGVtcGxhdGUgYW5kIGJvb3QgZnVuY3Rpb24pXG4gICAqL1xuICBmdW5jdGlvbiBnZXQoZG9tKSB7XG4gICAgcmV0dXJuIGRvbS50YWdOYW1lICYmIF9fVEFHX0lNUExbZ2V0QXR0cmlidXRlKGRvbSwgSVNfRElSRUNUSVZFKSB8fFxuICAgICAgZ2V0QXR0cmlidXRlKGRvbSwgSVNfRElSRUNUSVZFKSB8fCBkb20udGFnTmFtZS50b0xvd2VyQ2FzZSgpXVxuICB9XG5cbiAgLyoqXG4gICAqIEdldCB0aGUgdGFnIG5hbWUgb2YgYW55IERPTSBub2RlXG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gZG9tIC0gRE9NIG5vZGUgd2Ugd2FudCB0byBwYXJzZVxuICAgKiBAcGFyYW0gICB7IEJvb2xlYW4gfSBza2lwRGF0YUlzIC0gaGFjayB0byBpZ25vcmUgdGhlIGRhdGEtaXMgYXR0cmlidXRlIHdoZW4gYXR0YWNoaW5nIHRvIHBhcmVudFxuICAgKiBAcmV0dXJucyB7IFN0cmluZyB9IG5hbWUgdG8gaWRlbnRpZnkgdGhpcyBkb20gbm9kZSBpbiByaW90XG4gICAqL1xuICBmdW5jdGlvbiBnZXROYW1lKGRvbSwgc2tpcERhdGFJcykge1xuICAgIHZhciBjaGlsZCA9IGdldChkb20pO1xuICAgIHZhciBuYW1lZFRhZyA9ICFza2lwRGF0YUlzICYmIGdldEF0dHJpYnV0ZShkb20sIElTX0RJUkVDVElWRSk7XG4gICAgcmV0dXJuIG5hbWVkVGFnICYmICF0bXBsLmhhc0V4cHIobmFtZWRUYWcpID9cbiAgICAgIG5hbWVkVGFnIDogY2hpbGQgPyBjaGlsZC5uYW1lIDogZG9tLnRhZ05hbWUudG9Mb3dlckNhc2UoKVxuICB9XG5cbiAgLyoqXG4gICAqIFJldHVybiBhIHRlbXBvcmFyeSBjb250ZXh0IGNvbnRhaW5pbmcgYWxzbyB0aGUgcGFyZW50IHByb3BlcnRpZXNcbiAgICogQHRoaXMgVGFnXG4gICAqIEBwYXJhbSB7IFRhZyB9IC0gdGVtcG9yYXJ5IHRhZyBjb250ZXh0IGNvbnRhaW5pbmcgYWxsIHRoZSBwYXJlbnQgcHJvcGVydGllc1xuICAgKi9cbiAgZnVuY3Rpb24gaW5oZXJpdFBhcmVudFByb3BzKCkge1xuICAgIGlmICh0aGlzLnBhcmVudCkgeyByZXR1cm4gZXh0ZW5kKGNyZWF0ZSh0aGlzKSwgdGhpcy5wYXJlbnQpIH1cbiAgICByZXR1cm4gdGhpc1xuICB9XG5cbiAgLypcbiAgICBJbmNsdWRlcyBoYWNrcyBuZWVkZWQgZm9yIHRoZSBJbnRlcm5ldCBFeHBsb3JlciB2ZXJzaW9uIDkgYW5kIGJlbG93XG4gICAgU2VlOiBodHRwOi8va2FuZ2F4LmdpdGh1Yi5pby9jb21wYXQtdGFibGUvZXM1LyNpZThcbiAgICAgICAgIGh0dHA6Ly9jb2RlcGxhbmV0LmlvL2Ryb3BwaW5nLWllOC9cbiAgKi9cblxuICB2YXJcbiAgICByZUhhc1lpZWxkICA9IC88eWllbGRcXGIvaSxcbiAgICByZVlpZWxkQWxsICA9IC88eWllbGRcXHMqKD86XFwvPnw+KFtcXFNcXHNdKj8pPFxcL3lpZWxkXFxzKj58PikvaWcsXG4gICAgcmVZaWVsZFNyYyAgPSAvPHlpZWxkXFxzK3RvPVsnXCJdKFteJ1wiPl0qKVsnXCJdXFxzKj4oW1xcU1xcc10qPyk8XFwveWllbGRcXHMqPi9pZyxcbiAgICByZVlpZWxkRGVzdCA9IC88eWllbGRcXHMrZnJvbT1bJ1wiXT8oWy1cXHddKylbJ1wiXT9cXHMqKD86XFwvPnw+KFtcXFNcXHNdKj8pPFxcL3lpZWxkXFxzKj4pL2lnLFxuICAgIHJvb3RFbHMgPSB7IHRyOiAndGJvZHknLCB0aDogJ3RyJywgdGQ6ICd0cicsIGNvbDogJ2NvbGdyb3VwJyB9LFxuICAgIHRibFRhZ3MgPSBJRV9WRVJTSU9OICYmIElFX1ZFUlNJT04gPCAxMCA/IFJFX1NQRUNJQUxfVEFHUyA6IFJFX1NQRUNJQUxfVEFHU19OT19PUFRJT04sXG4gICAgR0VORVJJQyA9ICdkaXYnLFxuICAgIFNWRyA9ICdzdmcnO1xuXG5cbiAgLypcbiAgICBDcmVhdGVzIHRoZSByb290IGVsZW1lbnQgZm9yIHRhYmxlIG9yIHNlbGVjdCBjaGlsZCBlbGVtZW50czpcbiAgICB0ci90aC90ZC90aGVhZC90Zm9vdC90Ym9keS9jYXB0aW9uL2NvbC9jb2xncm91cC9vcHRpb24vb3B0Z3JvdXBcbiAgKi9cbiAgZnVuY3Rpb24gc3BlY2lhbFRhZ3MoZWwsIHRtcGwsIHRhZ05hbWUpIHtcblxuICAgIHZhclxuICAgICAgc2VsZWN0ID0gdGFnTmFtZVswXSA9PT0gJ28nLFxuICAgICAgcGFyZW50ID0gc2VsZWN0ID8gJ3NlbGVjdD4nIDogJ3RhYmxlPic7XG5cbiAgICAvLyB0cmltKCkgaXMgaW1wb3J0YW50IGhlcmUsIHRoaXMgZW5zdXJlcyB3ZSBkb24ndCBoYXZlIGFydGlmYWN0cyxcbiAgICAvLyBzbyB3ZSBjYW4gY2hlY2sgaWYgd2UgaGF2ZSBvbmx5IG9uZSBlbGVtZW50IGluc2lkZSB0aGUgcGFyZW50XG4gICAgZWwuaW5uZXJIVE1MID0gJzwnICsgcGFyZW50ICsgdG1wbC50cmltKCkgKyAnPC8nICsgcGFyZW50O1xuICAgIHBhcmVudCA9IGVsLmZpcnN0Q2hpbGQ7XG5cbiAgICAvLyByZXR1cm5zIHRoZSBpbW1lZGlhdGUgcGFyZW50IGlmIHRyL3RoL3RkL2NvbCBpcyB0aGUgb25seSBlbGVtZW50LCBpZiBub3RcbiAgICAvLyByZXR1cm5zIHRoZSB3aG9sZSB0cmVlLCBhcyB0aGlzIGNhbiBpbmNsdWRlIGFkZGl0aW9uYWwgZWxlbWVudHNcbiAgICAvKiBpc3RhbmJ1bCBpZ25vcmUgbmV4dCAqL1xuICAgIGlmIChzZWxlY3QpIHtcbiAgICAgIHBhcmVudC5zZWxlY3RlZEluZGV4ID0gLTE7ICAvLyBmb3IgSUU5LCBjb21wYXRpYmxlIHcvY3VycmVudCByaW90IGJlaGF2aW9yXG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIGF2b2lkcyBpbnNlcnRpb24gb2YgY29pbnRhaW5lciBpbnNpZGUgY29udGFpbmVyIChleDogdGJvZHkgaW5zaWRlIHRib2R5KVxuICAgICAgdmFyIHRuYW1lID0gcm9vdEVsc1t0YWdOYW1lXTtcbiAgICAgIGlmICh0bmFtZSAmJiBwYXJlbnQuY2hpbGRFbGVtZW50Q291bnQgPT09IDEpIHsgcGFyZW50ID0gJCh0bmFtZSwgcGFyZW50KTsgfVxuICAgIH1cbiAgICByZXR1cm4gcGFyZW50XG4gIH1cblxuICAvKlxuICAgIFJlcGxhY2UgdGhlIHlpZWxkIHRhZyBmcm9tIGFueSB0YWcgdGVtcGxhdGUgd2l0aCB0aGUgaW5uZXJIVE1MIG9mIHRoZVxuICAgIG9yaWdpbmFsIHRhZyBpbiB0aGUgcGFnZVxuICAqL1xuICBmdW5jdGlvbiByZXBsYWNlWWllbGQodG1wbCwgaHRtbCkge1xuICAgIC8vIGRvIG5vdGhpbmcgaWYgbm8geWllbGRcbiAgICBpZiAoIXJlSGFzWWllbGQudGVzdCh0bXBsKSkgeyByZXR1cm4gdG1wbCB9XG5cbiAgICAvLyBiZSBjYXJlZnVsIHdpdGggIzEzNDMgLSBzdHJpbmcgb24gdGhlIHNvdXJjZSBoYXZpbmcgYCQxYFxuICAgIHZhciBzcmMgPSB7fTtcblxuICAgIGh0bWwgPSBodG1sICYmIGh0bWwucmVwbGFjZShyZVlpZWxkU3JjLCBmdW5jdGlvbiAoXywgcmVmLCB0ZXh0KSB7XG4gICAgICBzcmNbcmVmXSA9IHNyY1tyZWZdIHx8IHRleHQ7ICAgLy8gcHJlc2VydmUgZmlyc3QgZGVmaW5pdGlvblxuICAgICAgcmV0dXJuICcnXG4gICAgfSkudHJpbSgpO1xuXG4gICAgcmV0dXJuIHRtcGxcbiAgICAgIC5yZXBsYWNlKHJlWWllbGREZXN0LCBmdW5jdGlvbiAoXywgcmVmLCBkZWYpIHsgIC8vIHlpZWxkIHdpdGggZnJvbSAtIHRvIGF0dHJzXG4gICAgICAgIHJldHVybiBzcmNbcmVmXSB8fCBkZWYgfHwgJydcbiAgICAgIH0pXG4gICAgICAucmVwbGFjZShyZVlpZWxkQWxsLCBmdW5jdGlvbiAoXywgZGVmKSB7ICAgICAgICAvLyB5aWVsZCB3aXRob3V0IGFueSBcImZyb21cIlxuICAgICAgICByZXR1cm4gaHRtbCB8fCBkZWYgfHwgJydcbiAgICAgIH0pXG4gIH1cblxuICAvKipcbiAgICogQ3JlYXRlcyBhIERPTSBlbGVtZW50IHRvIHdyYXAgdGhlIGdpdmVuIGNvbnRlbnQuIE5vcm1hbGx5IGFuIGBESVZgLCBidXQgY2FuIGJlXG4gICAqIGFsc28gYSBgVEFCTEVgLCBgU0VMRUNUYCwgYFRCT0RZYCwgYFRSYCwgb3IgYENPTEdST1VQYCBlbGVtZW50LlxuICAgKlxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IHRtcGwgIC0gVGhlIHRlbXBsYXRlIGNvbWluZyBmcm9tIHRoZSBjdXN0b20gdGFnIGRlZmluaXRpb25cbiAgICogQHBhcmFtICAgeyBTdHJpbmcgfSBodG1sIC0gSFRNTCBjb250ZW50IHRoYXQgY29tZXMgZnJvbSB0aGUgRE9NIGVsZW1lbnQgd2hlcmUgeW91XG4gICAqICAgICAgICAgICB3aWxsIG1vdW50IHRoZSB0YWcsIG1vc3RseSB0aGUgb3JpZ2luYWwgdGFnIGluIHRoZSBwYWdlXG4gICAqIEBwYXJhbSAgIHsgQm9vbGVhbiB9IGlzU3ZnIC0gdHJ1ZSBpZiB0aGUgcm9vdCBub2RlIGlzIGFuIHN2Z1xuICAgKiBAcmV0dXJucyB7IEhUTUxFbGVtZW50IH0gRE9NIGVsZW1lbnQgd2l0aCBfdG1wbF8gbWVyZ2VkIHRocm91Z2ggYFlJRUxEYCB3aXRoIHRoZSBfaHRtbF8uXG4gICAqL1xuICBmdW5jdGlvbiBta2RvbSh0bXBsLCBodG1sLCBpc1N2Zykge1xuICAgIHZhciBtYXRjaCAgID0gdG1wbCAmJiB0bXBsLm1hdGNoKC9eXFxzKjwoWy1cXHddKykvKTtcbiAgICB2YXIgIHRhZ05hbWUgPSBtYXRjaCAmJiBtYXRjaFsxXS50b0xvd2VyQ2FzZSgpO1xuICAgIHZhciBlbCA9IG1ha2VFbGVtZW50KGlzU3ZnID8gU1ZHIDogR0VORVJJQyk7XG5cbiAgICAvLyByZXBsYWNlIGFsbCB0aGUgeWllbGQgdGFncyB3aXRoIHRoZSB0YWcgaW5uZXIgaHRtbFxuICAgIHRtcGwgPSByZXBsYWNlWWllbGQodG1wbCwgaHRtbCk7XG5cbiAgICAvKiBpc3RhbmJ1bCBpZ25vcmUgbmV4dCAqL1xuICAgIGlmICh0YmxUYWdzLnRlc3QodGFnTmFtZSkpXG4gICAgICB7IGVsID0gc3BlY2lhbFRhZ3MoZWwsIHRtcGwsIHRhZ05hbWUpOyB9XG4gICAgZWxzZVxuICAgICAgeyBzZXRJbm5lckhUTUwoZWwsIHRtcGwsIGlzU3ZnKTsgfVxuXG4gICAgcmV0dXJuIGVsXG4gIH1cblxuICB2YXIgRVZFTlRfQVRUUl9SRSA9IC9eb24vO1xuXG4gIC8qKlxuICAgKiBUcnVlIGlmIHRoZSBldmVudCBhdHRyaWJ1dGUgc3RhcnRzIHdpdGggJ29uJ1xuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IGF0dHJpYnV0ZSAtIGV2ZW50IGF0dHJpYnV0ZVxuICAgKiBAcmV0dXJucyB7IEJvb2xlYW4gfVxuICAgKi9cbiAgZnVuY3Rpb24gaXNFdmVudEF0dHJpYnV0ZShhdHRyaWJ1dGUpIHtcbiAgICByZXR1cm4gRVZFTlRfQVRUUl9SRS50ZXN0KGF0dHJpYnV0ZSlcbiAgfVxuXG4gIC8qKlxuICAgKiBMb29wIGJhY2t3YXJkIGFsbCB0aGUgcGFyZW50cyB0cmVlIHRvIGRldGVjdCB0aGUgZmlyc3QgY3VzdG9tIHBhcmVudCB0YWdcbiAgICogQHBhcmFtICAgeyBPYmplY3QgfSB0YWcgLSBhIFRhZyBpbnN0YW5jZVxuICAgKiBAcmV0dXJucyB7IE9iamVjdCB9IHRoZSBpbnN0YW5jZSBvZiB0aGUgZmlyc3QgY3VzdG9tIHBhcmVudCB0YWcgZm91bmRcbiAgICovXG4gIGZ1bmN0aW9uIGdldEltbWVkaWF0ZUN1c3RvbVBhcmVudCh0YWcpIHtcbiAgICB2YXIgcHRhZyA9IHRhZztcbiAgICB3aGlsZSAocHRhZy5fXy5pc0Fub255bW91cykge1xuICAgICAgaWYgKCFwdGFnLnBhcmVudCkgeyBicmVhayB9XG4gICAgICBwdGFnID0gcHRhZy5wYXJlbnQ7XG4gICAgfVxuICAgIHJldHVybiBwdGFnXG4gIH1cblxuICAvKipcbiAgICogVHJpZ2dlciBET00gZXZlbnRzXG4gICAqIEBwYXJhbSAgIHsgSFRNTEVsZW1lbnQgfSBkb20gLSBkb20gZWxlbWVudCB0YXJnZXQgb2YgdGhlIGV2ZW50XG4gICAqIEBwYXJhbSAgIHsgRnVuY3Rpb24gfSBoYW5kbGVyIC0gdXNlciBmdW5jdGlvblxuICAgKiBAcGFyYW0gICB7IE9iamVjdCB9IGUgLSBldmVudCBvYmplY3RcbiAgICovXG4gIGZ1bmN0aW9uIGhhbmRsZUV2ZW50KGRvbSwgaGFuZGxlciwgZSkge1xuICAgIHZhciBwdGFnID0gdGhpcy5fXy5wYXJlbnQ7XG4gICAgdmFyIGl0ZW0gPSB0aGlzLl9fLml0ZW07XG5cbiAgICBpZiAoIWl0ZW0pXG4gICAgICB7IHdoaWxlIChwdGFnICYmICFpdGVtKSB7XG4gICAgICAgIGl0ZW0gPSBwdGFnLl9fLml0ZW07XG4gICAgICAgIHB0YWcgPSBwdGFnLl9fLnBhcmVudDtcbiAgICAgIH0gfVxuXG4gICAgLy8gb3ZlcnJpZGUgdGhlIGV2ZW50IHByb3BlcnRpZXNcbiAgICAvKiBpc3RhbmJ1bCBpZ25vcmUgbmV4dCAqL1xuICAgIGlmIChpc1dyaXRhYmxlKGUsICdjdXJyZW50VGFyZ2V0JykpIHsgZS5jdXJyZW50VGFyZ2V0ID0gZG9tOyB9XG4gICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cbiAgICBpZiAoaXNXcml0YWJsZShlLCAndGFyZ2V0JykpIHsgZS50YXJnZXQgPSBlLnNyY0VsZW1lbnQ7IH1cbiAgICAvKiBpc3RhbmJ1bCBpZ25vcmUgbmV4dCAqL1xuICAgIGlmIChpc1dyaXRhYmxlKGUsICd3aGljaCcpKSB7IGUud2hpY2ggPSBlLmNoYXJDb2RlIHx8IGUua2V5Q29kZTsgfVxuXG4gICAgZS5pdGVtID0gaXRlbTtcblxuICAgIGhhbmRsZXIuY2FsbCh0aGlzLCBlKTtcblxuICAgIC8vIGF2b2lkIGF1dG8gdXBkYXRlc1xuICAgIGlmICghc2V0dGluZ3MuYXV0b1VwZGF0ZSkgeyByZXR1cm4gfVxuXG4gICAgaWYgKCFlLnByZXZlbnRVcGRhdGUpIHtcbiAgICAgIHZhciBwID0gZ2V0SW1tZWRpYXRlQ3VzdG9tUGFyZW50KHRoaXMpO1xuICAgICAgLy8gZml4ZXMgIzIwODNcbiAgICAgIGlmIChwLmlzTW91bnRlZCkgeyBwLnVwZGF0ZSgpOyB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEF0dGFjaCBhbiBldmVudCB0byBhIERPTSBub2RlXG4gICAqIEBwYXJhbSB7IFN0cmluZyB9IG5hbWUgLSBldmVudCBuYW1lXG4gICAqIEBwYXJhbSB7IEZ1bmN0aW9uIH0gaGFuZGxlciAtIGV2ZW50IGNhbGxiYWNrXG4gICAqIEBwYXJhbSB7IE9iamVjdCB9IGRvbSAtIGRvbSBub2RlXG4gICAqIEBwYXJhbSB7IFRhZyB9IHRhZyAtIHRhZyBpbnN0YW5jZVxuICAgKi9cbiAgZnVuY3Rpb24gc2V0RXZlbnRIYW5kbGVyKG5hbWUsIGhhbmRsZXIsIGRvbSwgdGFnKSB7XG4gICAgdmFyIGV2ZW50TmFtZTtcbiAgICB2YXIgY2IgPSBoYW5kbGVFdmVudC5iaW5kKHRhZywgZG9tLCBoYW5kbGVyKTtcblxuICAgIC8vIGF2b2lkIHRvIGJpbmQgdHdpY2UgdGhlIHNhbWUgZXZlbnRcbiAgICAvLyBwb3NzaWJsZSBmaXggZm9yICMyMzMyXG4gICAgZG9tW25hbWVdID0gbnVsbDtcblxuICAgIC8vIG5vcm1hbGl6ZSBldmVudCBuYW1lXG4gICAgZXZlbnROYW1lID0gbmFtZS5yZXBsYWNlKFJFX0VWRU5UU19QUkVGSVgsICcnKTtcblxuICAgIC8vIGNhY2hlIHRoZSBsaXN0ZW5lciBpbnRvIHRoZSBsaXN0ZW5lcnMgYXJyYXlcbiAgICBpZiAoIWNvbnRhaW5zKHRhZy5fXy5saXN0ZW5lcnMsIGRvbSkpIHsgdGFnLl9fLmxpc3RlbmVycy5wdXNoKGRvbSk7IH1cbiAgICBpZiAoIWRvbVtSSU9UX0VWRU5UU19LRVldKSB7IGRvbVtSSU9UX0VWRU5UU19LRVldID0ge307IH1cbiAgICBpZiAoZG9tW1JJT1RfRVZFTlRTX0tFWV1bbmFtZV0pIHsgZG9tLnJlbW92ZUV2ZW50TGlzdGVuZXIoZXZlbnROYW1lLCBkb21bUklPVF9FVkVOVFNfS0VZXVtuYW1lXSk7IH1cblxuICAgIGRvbVtSSU9UX0VWRU5UU19LRVldW25hbWVdID0gY2I7XG4gICAgZG9tLmFkZEV2ZW50TGlzdGVuZXIoZXZlbnROYW1lLCBjYiwgZmFsc2UpO1xuICB9XG5cbiAgLyoqXG4gICAqIENyZWF0ZSBhIG5ldyBjaGlsZCB0YWcgaW5jbHVkaW5nIGl0IGNvcnJlY3RseSBpbnRvIGl0cyBwYXJlbnRcbiAgICogQHBhcmFtICAgeyBPYmplY3QgfSBjaGlsZCAtIGNoaWxkIHRhZyBpbXBsZW1lbnRhdGlvblxuICAgKiBAcGFyYW0gICB7IE9iamVjdCB9IG9wdHMgLSB0YWcgb3B0aW9ucyBjb250YWluaW5nIHRoZSBET00gbm9kZSB3aGVyZSB0aGUgdGFnIHdpbGwgYmUgbW91bnRlZFxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IGlubmVySFRNTCAtIGlubmVyIGh0bWwgb2YgdGhlIGNoaWxkIG5vZGVcbiAgICogQHBhcmFtICAgeyBPYmplY3QgfSBwYXJlbnQgLSBpbnN0YW5jZSBvZiB0aGUgcGFyZW50IHRhZyBpbmNsdWRpbmcgdGhlIGNoaWxkIGN1c3RvbSB0YWdcbiAgICogQHJldHVybnMgeyBPYmplY3QgfSBpbnN0YW5jZSBvZiB0aGUgbmV3IGNoaWxkIHRhZyBqdXN0IGNyZWF0ZWRcbiAgICovXG4gIGZ1bmN0aW9uIGluaXRDaGlsZChjaGlsZCwgb3B0cywgaW5uZXJIVE1MLCBwYXJlbnQpIHtcbiAgICB2YXIgdGFnID0gY3JlYXRlVGFnKGNoaWxkLCBvcHRzLCBpbm5lckhUTUwpO1xuICAgIHZhciB0YWdOYW1lID0gb3B0cy50YWdOYW1lIHx8IGdldE5hbWUob3B0cy5yb290LCB0cnVlKTtcbiAgICB2YXIgcHRhZyA9IGdldEltbWVkaWF0ZUN1c3RvbVBhcmVudChwYXJlbnQpO1xuICAgIC8vIGZpeCBmb3IgdGhlIHBhcmVudCBhdHRyaWJ1dGUgaW4gdGhlIGxvb3BlZCBlbGVtZW50c1xuICAgIGRlZmluZSh0YWcsICdwYXJlbnQnLCBwdGFnKTtcbiAgICAvLyBzdG9yZSB0aGUgcmVhbCBwYXJlbnQgdGFnXG4gICAgLy8gaW4gc29tZSBjYXNlcyB0aGlzIGNvdWxkIGJlIGRpZmZlcmVudCBmcm9tIHRoZSBjdXN0b20gcGFyZW50IHRhZ1xuICAgIC8vIGZvciBleGFtcGxlIGluIG5lc3RlZCBsb29wc1xuICAgIHRhZy5fXy5wYXJlbnQgPSBwYXJlbnQ7XG5cbiAgICAvLyBhZGQgdGhpcyB0YWcgdG8gdGhlIGN1c3RvbSBwYXJlbnQgdGFnXG4gICAgYXJyYXlpc2hBZGQocHRhZy50YWdzLCB0YWdOYW1lLCB0YWcpO1xuXG4gICAgLy8gYW5kIGFsc28gdG8gdGhlIHJlYWwgcGFyZW50IHRhZ1xuICAgIGlmIChwdGFnICE9PSBwYXJlbnQpXG4gICAgICB7IGFycmF5aXNoQWRkKHBhcmVudC50YWdzLCB0YWdOYW1lLCB0YWcpOyB9XG5cbiAgICByZXR1cm4gdGFnXG4gIH1cblxuICAvKipcbiAgICogUmVtb3ZlcyBhbiBpdGVtIGZyb20gYW4gb2JqZWN0IGF0IGEgZ2l2ZW4ga2V5LiBJZiB0aGUga2V5IHBvaW50cyB0byBhbiBhcnJheSxcbiAgICogdGhlbiB0aGUgaXRlbSBpcyBqdXN0IHJlbW92ZWQgZnJvbSB0aGUgYXJyYXkuXG4gICAqIEBwYXJhbSB7IE9iamVjdCB9IG9iaiAtIG9iamVjdCBvbiB3aGljaCB0byByZW1vdmUgdGhlIHByb3BlcnR5XG4gICAqIEBwYXJhbSB7IFN0cmluZyB9IGtleSAtIHByb3BlcnR5IG5hbWVcbiAgICogQHBhcmFtIHsgT2JqZWN0IH0gdmFsdWUgLSB0aGUgdmFsdWUgb2YgdGhlIHByb3BlcnR5IHRvIGJlIHJlbW92ZWRcbiAgICogQHBhcmFtIHsgQm9vbGVhbiB9IGVuc3VyZUFycmF5IC0gZW5zdXJlIHRoYXQgdGhlIHByb3BlcnR5IHJlbWFpbnMgYW4gYXJyYXlcbiAgKi9cbiAgZnVuY3Rpb24gYXJyYXlpc2hSZW1vdmUob2JqLCBrZXksIHZhbHVlLCBlbnN1cmVBcnJheSkge1xuICAgIGlmIChpc0FycmF5KG9ialtrZXldKSkge1xuICAgICAgdmFyIGluZGV4ID0gb2JqW2tleV0uaW5kZXhPZih2YWx1ZSk7XG4gICAgICBpZiAoaW5kZXggIT09IC0xKSB7IG9ialtrZXldLnNwbGljZShpbmRleCwgMSk7IH1cbiAgICAgIGlmICghb2JqW2tleV0ubGVuZ3RoKSB7IGRlbGV0ZSBvYmpba2V5XTsgfVxuICAgICAgZWxzZSBpZiAob2JqW2tleV0ubGVuZ3RoID09PSAxICYmICFlbnN1cmVBcnJheSkgeyBvYmpba2V5XSA9IG9ialtrZXldWzBdOyB9XG4gICAgfSBlbHNlIGlmIChvYmpba2V5XSA9PT0gdmFsdWUpXG4gICAgICB7IGRlbGV0ZSBvYmpba2V5XTsgfSAvLyBvdGhlcndpc2UganVzdCBkZWxldGUgdGhlIGtleVxuICB9XG5cbiAgLyoqXG4gICAqIEFkZHMgdGhlIGVsZW1lbnRzIGZvciBhIHZpcnR1YWwgdGFnXG4gICAqIEB0aGlzIFRhZ1xuICAgKiBAcGFyYW0geyBOb2RlIH0gc3JjIC0gdGhlIG5vZGUgdGhhdCB3aWxsIGRvIHRoZSBpbnNlcnRpbmcgb3IgYXBwZW5kaW5nXG4gICAqIEBwYXJhbSB7IFRhZyB9IHRhcmdldCAtIG9ubHkgaWYgaW5zZXJ0aW5nLCBpbnNlcnQgYmVmb3JlIHRoaXMgdGFnJ3MgZmlyc3QgY2hpbGRcbiAgICovXG4gIGZ1bmN0aW9uIG1ha2VWaXJ0dWFsKHNyYywgdGFyZ2V0KSB7XG4gICAgdmFyIHRoaXMkMSA9IHRoaXM7XG5cbiAgICB2YXIgaGVhZCA9IGNyZWF0ZURPTVBsYWNlaG9sZGVyKCk7XG4gICAgdmFyIHRhaWwgPSBjcmVhdGVET01QbGFjZWhvbGRlcigpO1xuICAgIHZhciBmcmFnID0gY3JlYXRlRnJhZ21lbnQoKTtcbiAgICB2YXIgc2liO1xuICAgIHZhciBlbDtcblxuICAgIHRoaXMucm9vdC5pbnNlcnRCZWZvcmUoaGVhZCwgdGhpcy5yb290LmZpcnN0Q2hpbGQpO1xuICAgIHRoaXMucm9vdC5hcHBlbmRDaGlsZCh0YWlsKTtcblxuICAgIHRoaXMuX18uaGVhZCA9IGVsID0gaGVhZDtcbiAgICB0aGlzLl9fLnRhaWwgPSB0YWlsO1xuXG4gICAgd2hpbGUgKGVsKSB7XG4gICAgICBzaWIgPSBlbC5uZXh0U2libGluZztcbiAgICAgIGZyYWcuYXBwZW5kQ2hpbGQoZWwpO1xuICAgICAgdGhpcyQxLl9fLnZpcnRzLnB1c2goZWwpOyAvLyBob2xkIGZvciB1bm1vdW50aW5nXG4gICAgICBlbCA9IHNpYjtcbiAgICB9XG5cbiAgICBpZiAodGFyZ2V0KVxuICAgICAgeyBzcmMuaW5zZXJ0QmVmb3JlKGZyYWcsIHRhcmdldC5fXy5oZWFkKTsgfVxuICAgIGVsc2VcbiAgICAgIHsgc3JjLmFwcGVuZENoaWxkKGZyYWcpOyB9XG4gIH1cblxuICAvKipcbiAgICogbWFrZXMgYSB0YWcgdmlydHVhbCBhbmQgcmVwbGFjZXMgYSByZWZlcmVuY2UgaW4gdGhlIGRvbVxuICAgKiBAdGhpcyBUYWdcbiAgICogQHBhcmFtIHsgdGFnIH0gdGhlIHRhZyB0byBtYWtlIHZpcnR1YWxcbiAgICogQHBhcmFtIHsgcmVmIH0gdGhlIGRvbSByZWZlcmVuY2UgbG9jYXRpb25cbiAgICovXG4gIGZ1bmN0aW9uIG1ha2VSZXBsYWNlVmlydHVhbCh0YWcsIHJlZikge1xuICAgIHZhciBmcmFnID0gY3JlYXRlRnJhZ21lbnQoKTtcbiAgICBtYWtlVmlydHVhbC5jYWxsKHRhZywgZnJhZyk7XG4gICAgcmVmLnBhcmVudE5vZGUucmVwbGFjZUNoaWxkKGZyYWcsIHJlZik7XG4gIH1cblxuICAvKipcbiAgICogVXBkYXRlIGR5bmFtaWNhbGx5IGNyZWF0ZWQgZGF0YS1pcyB0YWdzIHdpdGggY2hhbmdpbmcgZXhwcmVzc2lvbnNcbiAgICogQHBhcmFtIHsgT2JqZWN0IH0gZXhwciAtIGV4cHJlc3Npb24gdGFnIGFuZCBleHByZXNzaW9uIGluZm9cbiAgICogQHBhcmFtIHsgVGFnIH0gICAgcGFyZW50IC0gcGFyZW50IGZvciB0YWcgY3JlYXRpb25cbiAgICogQHBhcmFtIHsgU3RyaW5nIH0gdGFnTmFtZSAtIHRhZyBpbXBsZW1lbnRhdGlvbiB3ZSB3YW50IHRvIHVzZVxuICAgKi9cbiAgZnVuY3Rpb24gdXBkYXRlRGF0YUlzKGV4cHIsIHBhcmVudCwgdGFnTmFtZSkge1xuICAgIHZhciB0YWcgPSBleHByLnRhZyB8fCBleHByLmRvbS5fdGFnO1xuICAgIHZhciByZWY7XG5cbiAgICB2YXIgcmVmJDEgPSB0YWcgPyB0YWcuX18gOiB7fTtcbiAgICB2YXIgaGVhZCA9IHJlZiQxLmhlYWQ7XG4gICAgdmFyIGlzVmlydHVhbCA9IGV4cHIuZG9tLnRhZ05hbWUgPT09ICdWSVJUVUFMJztcblxuICAgIGlmICh0YWcgJiYgZXhwci50YWdOYW1lID09PSB0YWdOYW1lKSB7XG4gICAgICB0YWcudXBkYXRlKCk7XG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICAvLyBzeW5jIF9wYXJlbnQgdG8gYWNjb21tb2RhdGUgY2hhbmdpbmcgdGFnbmFtZXNcbiAgICBpZiAodGFnKSB7XG4gICAgICAvLyBuZWVkIHBsYWNlaG9sZGVyIGJlZm9yZSB1bm1vdW50XG4gICAgICBpZihpc1ZpcnR1YWwpIHtcbiAgICAgICAgcmVmID0gY3JlYXRlRE9NUGxhY2Vob2xkZXIoKTtcbiAgICAgICAgaGVhZC5wYXJlbnROb2RlLmluc2VydEJlZm9yZShyZWYsIGhlYWQpO1xuICAgICAgfVxuXG4gICAgICB0YWcudW5tb3VudCh0cnVlKTtcbiAgICB9XG5cbiAgICAvLyB1bmFibGUgdG8gZ2V0IHRoZSB0YWcgbmFtZVxuICAgIGlmICghaXNTdHJpbmcodGFnTmFtZSkpIHsgcmV0dXJuIH1cblxuICAgIGV4cHIuaW1wbCA9IF9fVEFHX0lNUExbdGFnTmFtZV07XG5cbiAgICAvLyB1bmtub3duIGltcGxlbWVudGF0aW9uXG4gICAgaWYgKCFleHByLmltcGwpIHsgcmV0dXJuIH1cblxuICAgIGV4cHIudGFnID0gdGFnID0gaW5pdENoaWxkKFxuICAgICAgZXhwci5pbXBsLCB7XG4gICAgICAgIHJvb3Q6IGV4cHIuZG9tLFxuICAgICAgICBwYXJlbnQ6IHBhcmVudCxcbiAgICAgICAgdGFnTmFtZTogdGFnTmFtZVxuICAgICAgfSxcbiAgICAgIGV4cHIuZG9tLmlubmVySFRNTCxcbiAgICAgIHBhcmVudFxuICAgICk7XG5cbiAgICBlYWNoKGV4cHIuYXR0cnMsIGZ1bmN0aW9uIChhKSB7IHJldHVybiBzZXRBdHRyaWJ1dGUodGFnLnJvb3QsIGEubmFtZSwgYS52YWx1ZSk7IH0pO1xuICAgIGV4cHIudGFnTmFtZSA9IHRhZ05hbWU7XG4gICAgdGFnLm1vdW50KCk7XG5cbiAgICAvLyByb290IGV4aXN0IGZpcnN0IHRpbWUsIGFmdGVyIHVzZSBwbGFjZWhvbGRlclxuICAgIGlmIChpc1ZpcnR1YWwpIHsgbWFrZVJlcGxhY2VWaXJ0dWFsKHRhZywgcmVmIHx8IHRhZy5yb290KTsgfVxuXG4gICAgLy8gcGFyZW50IGlzIHRoZSBwbGFjZWhvbGRlciB0YWcsIG5vdCB0aGUgZHluYW1pYyB0YWcgc28gY2xlYW4gdXBcbiAgICBwYXJlbnQuX18ub25Vbm1vdW50ID0gZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIGRlbE5hbWUgPSB0YWcub3B0cy5kYXRhSXM7XG4gICAgICBhcnJheWlzaFJlbW92ZSh0YWcucGFyZW50LnRhZ3MsIGRlbE5hbWUsIHRhZyk7XG4gICAgICBhcnJheWlzaFJlbW92ZSh0YWcuX18ucGFyZW50LnRhZ3MsIGRlbE5hbWUsIHRhZyk7XG4gICAgICB0YWcudW5tb3VudCgpO1xuICAgIH07XG4gIH1cblxuICAvKipcbiAgICogTm9tYWxpemUgYW55IGF0dHJpYnV0ZSByZW1vdmluZyB0aGUgXCJyaW90LVwiIHByZWZpeFxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IGF0dHJOYW1lIC0gb3JpZ2luYWwgYXR0cmlidXRlIG5hbWVcbiAgICogQHJldHVybnMgeyBTdHJpbmcgfSB2YWxpZCBodG1sIGF0dHJpYnV0ZSBuYW1lXG4gICAqL1xuICBmdW5jdGlvbiBub3JtYWxpemVBdHRyTmFtZShhdHRyTmFtZSkge1xuICAgIGlmICghYXR0ck5hbWUpIHsgcmV0dXJuIG51bGwgfVxuICAgIGF0dHJOYW1lID0gYXR0ck5hbWUucmVwbGFjZShBVFRSU19QUkVGSVgsICcnKTtcbiAgICBpZiAoQ0FTRV9TRU5TSVRJVkVfQVRUUklCVVRFU1thdHRyTmFtZV0pIHsgYXR0ck5hbWUgPSBDQVNFX1NFTlNJVElWRV9BVFRSSUJVVEVTW2F0dHJOYW1lXTsgfVxuICAgIHJldHVybiBhdHRyTmFtZVxuICB9XG5cbiAgLyoqXG4gICAqIFVwZGF0ZSBvbiBzaW5nbGUgdGFnIGV4cHJlc3Npb25cbiAgICogQHRoaXMgVGFnXG4gICAqIEBwYXJhbSB7IE9iamVjdCB9IGV4cHIgLSBleHByZXNzaW9uIGxvZ2ljXG4gICAqIEByZXR1cm5zIHsgdW5kZWZpbmVkIH1cbiAgICovXG4gIGZ1bmN0aW9uIHVwZGF0ZUV4cHJlc3Npb24oZXhwcikge1xuICAgIGlmICh0aGlzLnJvb3QgJiYgZ2V0QXR0cmlidXRlKHRoaXMucm9vdCwndmlydHVhbGl6ZWQnKSkgeyByZXR1cm4gfVxuXG4gICAgdmFyIGRvbSA9IGV4cHIuZG9tO1xuICAgIC8vIHJlbW92ZSB0aGUgcmlvdC0gcHJlZml4XG4gICAgdmFyIGF0dHJOYW1lID0gbm9ybWFsaXplQXR0ck5hbWUoZXhwci5hdHRyKTtcbiAgICB2YXIgaXNUb2dnbGUgPSBjb250YWlucyhbU0hPV19ESVJFQ1RJVkUsIEhJREVfRElSRUNUSVZFXSwgYXR0ck5hbWUpO1xuICAgIHZhciBpc1ZpcnR1YWwgPSBleHByLnJvb3QgJiYgZXhwci5yb290LnRhZ05hbWUgPT09ICdWSVJUVUFMJztcbiAgICB2YXIgcmVmID0gdGhpcy5fXztcbiAgICB2YXIgaXNBbm9ueW1vdXMgPSByZWYuaXNBbm9ueW1vdXM7XG4gICAgdmFyIHBhcmVudCA9IGRvbSAmJiAoZXhwci5wYXJlbnQgfHwgZG9tLnBhcmVudE5vZGUpO1xuICAgIC8vIGRldGVjdCB0aGUgc3R5bGUgYXR0cmlidXRlc1xuICAgIHZhciBpc1N0eWxlQXR0ciA9IGF0dHJOYW1lID09PSAnc3R5bGUnO1xuICAgIHZhciBpc0NsYXNzQXR0ciA9IGF0dHJOYW1lID09PSAnY2xhc3MnO1xuXG4gICAgdmFyIHZhbHVlO1xuXG4gICAgLy8gaWYgaXQncyBhIHRhZyB3ZSBjb3VsZCB0b3RhbGx5IHNraXAgdGhlIHJlc3RcbiAgICBpZiAoZXhwci5fcmlvdF9pZCkge1xuICAgICAgaWYgKGV4cHIuX18ud2FzQ3JlYXRlZCkge1xuICAgICAgICBleHByLnVwZGF0ZSgpO1xuICAgICAgLy8gaWYgaXQgaGFzbid0IGJlZW4gbW91bnRlZCB5ZXQsIGRvIHRoYXQgbm93LlxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgZXhwci5tb3VudCgpO1xuICAgICAgICBpZiAoaXNWaXJ0dWFsKSB7XG4gICAgICAgICAgbWFrZVJlcGxhY2VWaXJ0dWFsKGV4cHIsIGV4cHIucm9vdCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIC8vIGlmIHRoaXMgZXhwcmVzc2lvbiBoYXMgdGhlIHVwZGF0ZSBtZXRob2QgaXQgbWVhbnMgaXQgY2FuIGhhbmRsZSB0aGUgRE9NIGNoYW5nZXMgYnkgaXRzZWxmXG4gICAgaWYgKGV4cHIudXBkYXRlKSB7IHJldHVybiBleHByLnVwZGF0ZSgpIH1cblxuICAgIHZhciBjb250ZXh0ID0gaXNUb2dnbGUgJiYgIWlzQW5vbnltb3VzID8gaW5oZXJpdFBhcmVudFByb3BzLmNhbGwodGhpcykgOiB0aGlzO1xuXG4gICAgLy8gLi4uaXQgc2VlbXMgdG8gYmUgYSBzaW1wbGUgZXhwcmVzc2lvbiBzbyB3ZSB0cnkgdG8gY2FsY3VsYXRlIGl0cyB2YWx1ZVxuICAgIHZhbHVlID0gdG1wbChleHByLmV4cHIsIGNvbnRleHQpO1xuXG4gICAgdmFyIGhhc1ZhbHVlID0gIWlzQmxhbmsodmFsdWUpO1xuICAgIHZhciBpc09iaiA9IGlzT2JqZWN0KHZhbHVlKTtcblxuICAgIC8vIGNvbnZlcnQgdGhlIHN0eWxlL2NsYXNzIG9iamVjdHMgdG8gc3RyaW5nc1xuICAgIGlmIChpc09iaikge1xuICAgICAgaWYgKGlzQ2xhc3NBdHRyKSB7XG4gICAgICAgIHZhbHVlID0gdG1wbChKU09OLnN0cmluZ2lmeSh2YWx1ZSksIHRoaXMpO1xuICAgICAgfSBlbHNlIGlmIChpc1N0eWxlQXR0cikge1xuICAgICAgICB2YWx1ZSA9IHN0eWxlT2JqZWN0VG9TdHJpbmcodmFsdWUpO1xuICAgICAgfVxuICAgIH1cblxuICAgIC8vIHJlbW92ZSBvcmlnaW5hbCBhdHRyaWJ1dGVcbiAgICBpZiAoZXhwci5hdHRyICYmICghZXhwci53YXNQYXJzZWRPbmNlIHx8ICFoYXNWYWx1ZSB8fCB2YWx1ZSA9PT0gZmFsc2UpKSB7XG4gICAgICAvLyByZW1vdmUgZWl0aGVyIHJpb3QtKiBhdHRyaWJ1dGVzIG9yIGp1c3QgdGhlIGF0dHJpYnV0ZSBuYW1lXG4gICAgICByZW1vdmVBdHRyaWJ1dGUoZG9tLCBnZXRBdHRyaWJ1dGUoZG9tLCBleHByLmF0dHIpID8gZXhwci5hdHRyIDogYXR0ck5hbWUpO1xuICAgIH1cblxuICAgIC8vIGZvciB0aGUgYm9vbGVhbiBhdHRyaWJ1dGVzIHdlIGRvbid0IG5lZWQgdGhlIHZhbHVlXG4gICAgLy8gd2UgY2FuIGNvbnZlcnQgaXQgdG8gY2hlY2tlZD10cnVlIHRvIGNoZWNrZWQ9Y2hlY2tlZFxuICAgIGlmIChleHByLmJvb2wpIHsgdmFsdWUgPSB2YWx1ZSA/IGF0dHJOYW1lIDogZmFsc2U7IH1cbiAgICBpZiAoZXhwci5pc1J0YWcpIHsgcmV0dXJuIHVwZGF0ZURhdGFJcyhleHByLCB0aGlzLCB2YWx1ZSkgfVxuICAgIGlmIChleHByLndhc1BhcnNlZE9uY2UgJiYgZXhwci52YWx1ZSA9PT0gdmFsdWUpIHsgcmV0dXJuIH1cblxuICAgIC8vIHVwZGF0ZSB0aGUgZXhwcmVzc2lvbiB2YWx1ZVxuICAgIGV4cHIudmFsdWUgPSB2YWx1ZTtcbiAgICBleHByLndhc1BhcnNlZE9uY2UgPSB0cnVlO1xuXG4gICAgLy8gaWYgdGhlIHZhbHVlIGlzIGFuIG9iamVjdCAoYW5kIGl0J3Mgbm90IGEgc3R5bGUgb3IgY2xhc3MgYXR0cmlidXRlKSB3ZSBjYW4gbm90IGRvIG11Y2ggbW9yZSB3aXRoIGl0XG4gICAgaWYgKGlzT2JqICYmICFpc0NsYXNzQXR0ciAmJiAhaXNTdHlsZUF0dHIgJiYgIWlzVG9nZ2xlKSB7IHJldHVybiB9XG4gICAgLy8gYXZvaWQgdG8gcmVuZGVyIHVuZGVmaW5lZC9udWxsIHZhbHVlc1xuICAgIGlmICghaGFzVmFsdWUpIHsgdmFsdWUgPSAnJzsgfVxuXG4gICAgLy8gdGV4dGFyZWEgYW5kIHRleHQgbm9kZXMgaGF2ZSBubyBhdHRyaWJ1dGUgbmFtZVxuICAgIGlmICghYXR0ck5hbWUpIHtcbiAgICAgIC8vIGFib3V0ICM4MTUgdy9vIHJlcGxhY2U6IHRoZSBicm93c2VyIGNvbnZlcnRzIHRoZSB2YWx1ZSB0byBhIHN0cmluZyxcbiAgICAgIC8vIHRoZSBjb21wYXJpc29uIGJ5IFwiPT1cIiBkb2VzIHRvbywgYnV0IG5vdCBpbiB0aGUgc2VydmVyXG4gICAgICB2YWx1ZSArPSAnJztcbiAgICAgIC8vIHRlc3QgZm9yIHBhcmVudCBhdm9pZHMgZXJyb3Igd2l0aCBpbnZhbGlkIGFzc2lnbm1lbnQgdG8gbm9kZVZhbHVlXG4gICAgICBpZiAocGFyZW50KSB7XG4gICAgICAgIC8vIGNhY2hlIHRoZSBwYXJlbnQgbm9kZSBiZWNhdXNlIHNvbWVob3cgaXQgd2lsbCBiZWNvbWUgbnVsbCBvbiBJRVxuICAgICAgICAvLyBvbiB0aGUgbmV4dCBpdGVyYXRpb25cbiAgICAgICAgZXhwci5wYXJlbnQgPSBwYXJlbnQ7XG4gICAgICAgIGlmIChwYXJlbnQudGFnTmFtZSA9PT0gJ1RFWFRBUkVBJykge1xuICAgICAgICAgIHBhcmVudC52YWx1ZSA9IHZhbHVlOyAgICAgICAgICAgICAgICAgICAgLy8gIzExMTNcbiAgICAgICAgICBpZiAoIUlFX1ZFUlNJT04pIHsgZG9tLm5vZGVWYWx1ZSA9IHZhbHVlOyB9ICAvLyAjMTYyNSBJRSB0aHJvd3MgaGVyZSwgbm9kZVZhbHVlXG4gICAgICAgIH0gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIHdpbGwgYmUgYXZhaWxhYmxlIG9uICd1cGRhdGVkJ1xuICAgICAgICBlbHNlIHsgZG9tLm5vZGVWYWx1ZSA9IHZhbHVlOyB9XG4gICAgICB9XG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICBzd2l0Y2ggKHRydWUpIHtcbiAgICAvLyBoYW5kbGUgZXZlbnRzIGJpbmRpbmdcbiAgICBjYXNlIGlzRnVuY3Rpb24odmFsdWUpOlxuICAgICAgaWYgKGlzRXZlbnRBdHRyaWJ1dGUoYXR0ck5hbWUpKSB7XG4gICAgICAgIHNldEV2ZW50SGFuZGxlcihhdHRyTmFtZSwgdmFsdWUsIGRvbSwgdGhpcyk7XG4gICAgICB9XG4gICAgICBicmVha1xuICAgIC8vIHNob3cgLyBoaWRlXG4gICAgY2FzZSBpc1RvZ2dsZTpcbiAgICAgIHRvZ2dsZVZpc2liaWxpdHkoZG9tLCBhdHRyTmFtZSA9PT0gSElERV9ESVJFQ1RJVkUgPyAhdmFsdWUgOiB2YWx1ZSk7XG4gICAgICBicmVha1xuICAgIC8vIGhhbmRsZSBhdHRyaWJ1dGVzXG4gICAgZGVmYXVsdDpcbiAgICAgIGlmIChleHByLmJvb2wpIHtcbiAgICAgICAgZG9tW2F0dHJOYW1lXSA9IHZhbHVlO1xuICAgICAgfVxuXG4gICAgICBpZiAoYXR0ck5hbWUgPT09ICd2YWx1ZScgJiYgZG9tLnZhbHVlICE9PSB2YWx1ZSkge1xuICAgICAgICBkb20udmFsdWUgPSB2YWx1ZTtcbiAgICAgIH0gZWxzZSBpZiAoaGFzVmFsdWUgJiYgdmFsdWUgIT09IGZhbHNlKSB7XG4gICAgICAgIHNldEF0dHJpYnV0ZShkb20sIGF0dHJOYW1lLCB2YWx1ZSk7XG4gICAgICB9XG5cbiAgICAgIC8vIG1ha2Ugc3VyZSB0aGF0IGluIGNhc2Ugb2Ygc3R5bGUgY2hhbmdlc1xuICAgICAgLy8gdGhlIGVsZW1lbnQgc3RheXMgaGlkZGVuXG4gICAgICBpZiAoaXNTdHlsZUF0dHIgJiYgZG9tLmhpZGRlbikgeyB0b2dnbGVWaXNpYmlsaXR5KGRvbSwgZmFsc2UpOyB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIFVwZGF0ZSBhbGwgdGhlIGV4cHJlc3Npb25zIGluIGEgVGFnIGluc3RhbmNlXG4gICAqIEB0aGlzIFRhZ1xuICAgKiBAcGFyYW0geyBBcnJheSB9IGV4cHJlc3Npb25zIC0gZXhwcmVzc2lvbiB0aGF0IG11c3QgYmUgcmUgZXZhbHVhdGVkXG4gICAqL1xuICBmdW5jdGlvbiB1cGRhdGUoZXhwcmVzc2lvbnMpIHtcbiAgICBlYWNoKGV4cHJlc3Npb25zLCB1cGRhdGVFeHByZXNzaW9uLmJpbmQodGhpcykpO1xuICB9XG5cbiAgLyoqXG4gICAqIFdlIG5lZWQgdG8gdXBkYXRlIG9wdHMgZm9yIHRoaXMgdGFnLiBUaGF0IHJlcXVpcmVzIHVwZGF0aW5nIHRoZSBleHByZXNzaW9uc1xuICAgKiBpbiBhbnkgYXR0cmlidXRlcyBvbiB0aGUgdGFnLCBhbmQgdGhlbiBjb3B5aW5nIHRoZSByZXN1bHQgb250byBvcHRzLlxuICAgKiBAdGhpcyBUYWdcbiAgICogQHBhcmFtICAge0Jvb2xlYW59IGlzTG9vcCAtIGlzIGl0IGEgbG9vcCB0YWc/XG4gICAqIEBwYXJhbSAgIHsgVGFnIH0gIHBhcmVudCAtIHBhcmVudCB0YWcgbm9kZVxuICAgKiBAcGFyYW0gICB7IEJvb2xlYW4gfSAgaXNBbm9ueW1vdXMgLSBpcyBpdCBhIHRhZyB3aXRob3V0IGFueSBpbXBsPyAoYSB0YWcgbm90IHJlZ2lzdGVyZWQpXG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gIG9wdHMgLSB0YWcgb3B0aW9uc1xuICAgKiBAcGFyYW0gICB7IEFycmF5IH0gIGluc3RBdHRycyAtIHRhZyBhdHRyaWJ1dGVzIGFycmF5XG4gICAqL1xuICBmdW5jdGlvbiB1cGRhdGVPcHRzKGlzTG9vcCwgcGFyZW50LCBpc0Fub255bW91cywgb3B0cywgaW5zdEF0dHJzKSB7XG4gICAgLy8gaXNBbm9ueW1vdXMgYGVhY2hgIHRhZ3MgdHJlYXQgYGRvbWAgYW5kIGByb290YCBkaWZmZXJlbnRseS4gSW4gdGhpcyBjYXNlXG4gICAgLy8gKGFuZCBvbmx5IHRoaXMgY2FzZSkgd2UgZG9uJ3QgbmVlZCB0byBkbyB1cGRhdGVPcHRzLCBiZWNhdXNlIHRoZSByZWd1bGFyIHBhcnNlXG4gICAgLy8gd2lsbCB1cGRhdGUgdGhvc2UgYXR0cnMuIFBsdXMsIGlzQW5vbnltb3VzIHRhZ3MgZG9uJ3QgbmVlZCBvcHRzIGFueXdheVxuICAgIGlmIChpc0xvb3AgJiYgaXNBbm9ueW1vdXMpIHsgcmV0dXJuIH1cbiAgICB2YXIgY3R4ID0gaXNMb29wID8gaW5oZXJpdFBhcmVudFByb3BzLmNhbGwodGhpcykgOiBwYXJlbnQgfHwgdGhpcztcblxuICAgIGVhY2goaW5zdEF0dHJzLCBmdW5jdGlvbiAoYXR0cikge1xuICAgICAgaWYgKGF0dHIuZXhwcikgeyB1cGRhdGVFeHByZXNzaW9uLmNhbGwoY3R4LCBhdHRyLmV4cHIpOyB9XG4gICAgICAvLyBub3JtYWxpemUgdGhlIGF0dHJpYnV0ZSBuYW1lc1xuICAgICAgb3B0c1t0b0NhbWVsKGF0dHIubmFtZSkucmVwbGFjZShBVFRSU19QUkVGSVgsICcnKV0gPSBhdHRyLmV4cHIgPyBhdHRyLmV4cHIudmFsdWUgOiBhdHRyLnZhbHVlO1xuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIFVwZGF0ZSB0aGUgdGFnIGV4cHJlc3Npb25zIGFuZCBvcHRpb25zXG4gICAqIEBwYXJhbSB7IFRhZyB9IHRhZyAtIHRhZyBvYmplY3RcbiAgICogQHBhcmFtIHsgKiB9IGRhdGEgLSBkYXRhIHdlIHdhbnQgdG8gdXNlIHRvIGV4dGVuZCB0aGUgdGFnIHByb3BlcnRpZXNcbiAgICogQHBhcmFtIHsgQXJyYXkgfSBleHByZXNzaW9ucyAtIGNvbXBvbmVudCBleHByZXNzaW9ucyBhcnJheVxuICAgKiBAcmV0dXJucyB7IFRhZyB9IHRoZSBjdXJyZW50IHRhZyBpbnN0YW5jZVxuICAgKi9cbiAgZnVuY3Rpb24gY29tcG9uZW50VXBkYXRlKHRhZywgZGF0YSwgZXhwcmVzc2lvbnMpIHtcbiAgICB2YXIgX18gPSB0YWcuX187XG4gICAgdmFyIG5leHRPcHRzID0ge307XG4gICAgdmFyIGNhblRyaWdnZXIgPSB0YWcuaXNNb3VudGVkICYmICFfXy5za2lwQW5vbnltb3VzO1xuXG4gICAgLy8gaW5oZXJpdCBwcm9wZXJ0aWVzIGZyb20gdGhlIHBhcmVudCB0YWdcbiAgICBpZiAoX18uaXNBbm9ueW1vdXMgJiYgX18ucGFyZW50KSB7IGV4dGVuZCh0YWcsIF9fLnBhcmVudCk7IH1cbiAgICBleHRlbmQodGFnLCBkYXRhKTtcblxuICAgIHVwZGF0ZU9wdHMuYXBwbHkodGFnLCBbX18uaXNMb29wLCBfXy5wYXJlbnQsIF9fLmlzQW5vbnltb3VzLCBuZXh0T3B0cywgX18uaW5zdEF0dHJzXSk7XG5cbiAgICBpZiAoXG4gICAgICBjYW5UcmlnZ2VyICYmXG4gICAgICB0YWcuaXNNb3VudGVkICYmXG4gICAgICBpc0Z1bmN0aW9uKHRhZy5zaG91bGRVcGRhdGUpICYmICF0YWcuc2hvdWxkVXBkYXRlKGRhdGEsIG5leHRPcHRzKVxuICAgICkge1xuICAgICAgcmV0dXJuIHRhZ1xuICAgIH1cblxuICAgIGV4dGVuZCh0YWcub3B0cywgbmV4dE9wdHMpO1xuXG4gICAgaWYgKGNhblRyaWdnZXIpIHsgdGFnLnRyaWdnZXIoJ3VwZGF0ZScsIGRhdGEpOyB9XG4gICAgdXBkYXRlLmNhbGwodGFnLCBleHByZXNzaW9ucyk7XG4gICAgaWYgKGNhblRyaWdnZXIpIHsgdGFnLnRyaWdnZXIoJ3VwZGF0ZWQnKTsgfVxuXG4gICAgcmV0dXJuIHRhZ1xuICB9XG5cbiAgLyoqXG4gICAqIEdldCBzZWxlY3RvcnMgZm9yIHRhZ3NcbiAgICogQHBhcmFtICAgeyBBcnJheSB9IHRhZ3MgLSB0YWcgbmFtZXMgdG8gc2VsZWN0XG4gICAqIEByZXR1cm5zIHsgU3RyaW5nIH0gc2VsZWN0b3JcbiAgICovXG4gIGZ1bmN0aW9uIHF1ZXJ5KHRhZ3MpIHtcbiAgICAvLyBzZWxlY3QgYWxsIHRhZ3NcbiAgICBpZiAoIXRhZ3MpIHtcbiAgICAgIHZhciBrZXlzID0gT2JqZWN0LmtleXMoX19UQUdfSU1QTCk7XG4gICAgICByZXR1cm4ga2V5cyArIHF1ZXJ5KGtleXMpXG4gICAgfVxuXG4gICAgcmV0dXJuIHRhZ3NcbiAgICAgIC5maWx0ZXIoZnVuY3Rpb24gKHQpIHsgcmV0dXJuICEvW14tXFx3XS8udGVzdCh0KTsgfSlcbiAgICAgIC5yZWR1Y2UoZnVuY3Rpb24gKGxpc3QsIHQpIHtcbiAgICAgICAgdmFyIG5hbWUgPSB0LnRyaW0oKS50b0xvd2VyQ2FzZSgpO1xuICAgICAgICByZXR1cm4gbGlzdCArIFwiLFtcIiArIElTX0RJUkVDVElWRSArIFwiPVxcXCJcIiArIG5hbWUgKyBcIlxcXCJdXCJcbiAgICAgIH0sICcnKVxuICB9XG5cbiAgLyoqXG4gICAqIEFub3RoZXIgd2F5IHRvIGNyZWF0ZSBhIHJpb3QgdGFnIGEgYml0IG1vcmUgZXM2IGZyaWVuZGx5XG4gICAqIEBwYXJhbSB7IEhUTUxFbGVtZW50IH0gZWwgLSB0YWcgRE9NIHNlbGVjdG9yIG9yIERPTSBub2RlL3NcbiAgICogQHBhcmFtIHsgT2JqZWN0IH0gb3B0cyAtIHRhZyBsb2dpY1xuICAgKiBAcmV0dXJucyB7IFRhZyB9IG5ldyByaW90IHRhZyBpbnN0YW5jZVxuICAgKi9cbiAgZnVuY3Rpb24gVGFnKGVsLCBvcHRzKSB7XG4gICAgLy8gZ2V0IHRoZSB0YWcgcHJvcGVydGllcyBmcm9tIHRoZSBjbGFzcyBjb25zdHJ1Y3RvclxuICAgIHZhciByZWYgPSB0aGlzO1xuICAgIHZhciBuYW1lID0gcmVmLm5hbWU7XG4gICAgdmFyIHRtcGwgPSByZWYudG1wbDtcbiAgICB2YXIgY3NzID0gcmVmLmNzcztcbiAgICB2YXIgYXR0cnMgPSByZWYuYXR0cnM7XG4gICAgdmFyIG9uQ3JlYXRlID0gcmVmLm9uQ3JlYXRlO1xuICAgIC8vIHJlZ2lzdGVyIGEgbmV3IHRhZyBhbmQgY2FjaGUgdGhlIGNsYXNzIHByb3RvdHlwZVxuICAgIGlmICghX19UQUdfSU1QTFtuYW1lXSkge1xuICAgICAgdGFnKG5hbWUsIHRtcGwsIGNzcywgYXR0cnMsIG9uQ3JlYXRlKTtcbiAgICAgIC8vIGNhY2hlIHRoZSBjbGFzcyBjb25zdHJ1Y3RvclxuICAgICAgX19UQUdfSU1QTFtuYW1lXS5jbGFzcyA9IHRoaXMuY29uc3RydWN0b3I7XG4gICAgfVxuXG4gICAgLy8gbW91bnQgdGhlIHRhZyB1c2luZyB0aGUgY2xhc3MgaW5zdGFuY2VcbiAgICBtb3VudCQxKGVsLCBuYW1lLCBvcHRzLCB0aGlzKTtcbiAgICAvLyBpbmplY3QgdGhlIGNvbXBvbmVudCBjc3NcbiAgICBpZiAoY3NzKSB7IHN0eWxlTWFuYWdlci5pbmplY3QoKTsgfVxuXG4gICAgcmV0dXJuIHRoaXNcbiAgfVxuXG4gIC8qKlxuICAgKiBDcmVhdGUgYSBuZXcgcmlvdCB0YWcgaW1wbGVtZW50YXRpb25cbiAgICogQHBhcmFtICAgeyBTdHJpbmcgfSAgIG5hbWUgLSBuYW1lL2lkIG9mIHRoZSBuZXcgcmlvdCB0YWdcbiAgICogQHBhcmFtICAgeyBTdHJpbmcgfSAgIHRtcGwgLSB0YWcgdGVtcGxhdGVcbiAgICogQHBhcmFtICAgeyBTdHJpbmcgfSAgIGNzcyAtIGN1c3RvbSB0YWcgY3NzXG4gICAqIEBwYXJhbSAgIHsgU3RyaW5nIH0gICBhdHRycyAtIHJvb3QgdGFnIGF0dHJpYnV0ZXNcbiAgICogQHBhcmFtICAgeyBGdW5jdGlvbiB9IGZuIC0gdXNlciBmdW5jdGlvblxuICAgKiBAcmV0dXJucyB7IFN0cmluZyB9IG5hbWUvaWQgb2YgdGhlIHRhZyBqdXN0IGNyZWF0ZWRcbiAgICovXG4gIGZ1bmN0aW9uIHRhZyhuYW1lLCB0bXBsLCBjc3MsIGF0dHJzLCBmbikge1xuICAgIGlmIChpc0Z1bmN0aW9uKGF0dHJzKSkge1xuICAgICAgZm4gPSBhdHRycztcblxuICAgICAgaWYgKC9eW1xcdy1dK1xccz89Ly50ZXN0KGNzcykpIHtcbiAgICAgICAgYXR0cnMgPSBjc3M7XG4gICAgICAgIGNzcyA9ICcnO1xuICAgICAgfSBlbHNlXG4gICAgICAgIHsgYXR0cnMgPSAnJzsgfVxuICAgIH1cblxuICAgIGlmIChjc3MpIHtcbiAgICAgIGlmIChpc0Z1bmN0aW9uKGNzcykpXG4gICAgICAgIHsgZm4gPSBjc3M7IH1cbiAgICAgIGVsc2VcbiAgICAgICAgeyBzdHlsZU1hbmFnZXIuYWRkKGNzcywgbmFtZSk7IH1cbiAgICB9XG5cbiAgICBuYW1lID0gbmFtZS50b0xvd2VyQ2FzZSgpO1xuICAgIF9fVEFHX0lNUExbbmFtZV0gPSB7IG5hbWU6IG5hbWUsIHRtcGw6IHRtcGwsIGF0dHJzOiBhdHRycywgZm46IGZuIH07XG5cbiAgICByZXR1cm4gbmFtZVxuICB9XG5cbiAgLyoqXG4gICAqIENyZWF0ZSBhIG5ldyByaW90IHRhZyBpbXBsZW1lbnRhdGlvbiAoZm9yIHVzZSBieSB0aGUgY29tcGlsZXIpXG4gICAqIEBwYXJhbSAgIHsgU3RyaW5nIH0gICBuYW1lIC0gbmFtZS9pZCBvZiB0aGUgbmV3IHJpb3QgdGFnXG4gICAqIEBwYXJhbSAgIHsgU3RyaW5nIH0gICB0bXBsIC0gdGFnIHRlbXBsYXRlXG4gICAqIEBwYXJhbSAgIHsgU3RyaW5nIH0gICBjc3MgLSBjdXN0b20gdGFnIGNzc1xuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9ICAgYXR0cnMgLSByb290IHRhZyBhdHRyaWJ1dGVzXG4gICAqIEBwYXJhbSAgIHsgRnVuY3Rpb24gfSBmbiAtIHVzZXIgZnVuY3Rpb25cbiAgICogQHJldHVybnMgeyBTdHJpbmcgfSBuYW1lL2lkIG9mIHRoZSB0YWcganVzdCBjcmVhdGVkXG4gICAqL1xuICBmdW5jdGlvbiB0YWcyKG5hbWUsIHRtcGwsIGNzcywgYXR0cnMsIGZuKSB7XG4gICAgaWYgKGNzcykgeyBzdHlsZU1hbmFnZXIuYWRkKGNzcywgbmFtZSk7IH1cblxuICAgIF9fVEFHX0lNUExbbmFtZV0gPSB7IG5hbWU6IG5hbWUsIHRtcGw6IHRtcGwsIGF0dHJzOiBhdHRycywgZm46IGZuIH07XG5cbiAgICByZXR1cm4gbmFtZVxuICB9XG5cbiAgLyoqXG4gICAqIE1vdW50IGEgdGFnIHVzaW5nIGEgc3BlY2lmaWMgdGFnIGltcGxlbWVudGF0aW9uXG4gICAqIEBwYXJhbSAgIHsgKiB9IHNlbGVjdG9yIC0gdGFnIERPTSBzZWxlY3RvciBvciBET00gbm9kZS9zXG4gICAqIEBwYXJhbSAgIHsgU3RyaW5nIH0gdGFnTmFtZSAtIHRhZyBpbXBsZW1lbnRhdGlvbiBuYW1lXG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gb3B0cyAtIHRhZyBsb2dpY1xuICAgKiBAcmV0dXJucyB7IEFycmF5IH0gbmV3IHRhZ3MgaW5zdGFuY2VzXG4gICAqL1xuICBmdW5jdGlvbiBtb3VudChzZWxlY3RvciwgdGFnTmFtZSwgb3B0cykge1xuICAgIHZhciB0YWdzID0gW107XG4gICAgdmFyIGVsZW0sIGFsbFRhZ3M7XG5cbiAgICBmdW5jdGlvbiBwdXNoVGFnc1RvKHJvb3QpIHtcbiAgICAgIGlmIChyb290LnRhZ05hbWUpIHtcbiAgICAgICAgdmFyIHJpb3RUYWcgPSBnZXRBdHRyaWJ1dGUocm9vdCwgSVNfRElSRUNUSVZFKSwgdGFnO1xuXG4gICAgICAgIC8vIGhhdmUgdGFnTmFtZT8gZm9yY2UgcmlvdC10YWcgdG8gYmUgdGhlIHNhbWVcbiAgICAgICAgaWYgKHRhZ05hbWUgJiYgcmlvdFRhZyAhPT0gdGFnTmFtZSkge1xuICAgICAgICAgIHJpb3RUYWcgPSB0YWdOYW1lO1xuICAgICAgICAgIHNldEF0dHJpYnV0ZShyb290LCBJU19ESVJFQ1RJVkUsIHRhZ05hbWUpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGFnID0gbW91bnQkMShyb290LCByaW90VGFnIHx8IHJvb3QudGFnTmFtZS50b0xvd2VyQ2FzZSgpLCBvcHRzKTtcblxuICAgICAgICBpZiAodGFnKVxuICAgICAgICAgIHsgdGFncy5wdXNoKHRhZyk7IH1cbiAgICAgIH0gZWxzZSBpZiAocm9vdC5sZW5ndGgpXG4gICAgICAgIHsgZWFjaChyb290LCBwdXNoVGFnc1RvKTsgfSAvLyBhc3N1bWUgbm9kZUxpc3RcbiAgICB9XG5cbiAgICAvLyBpbmplY3Qgc3R5bGVzIGludG8gRE9NXG4gICAgc3R5bGVNYW5hZ2VyLmluamVjdCgpO1xuXG4gICAgaWYgKGlzT2JqZWN0KHRhZ05hbWUpKSB7XG4gICAgICBvcHRzID0gdGFnTmFtZTtcbiAgICAgIHRhZ05hbWUgPSAwO1xuICAgIH1cblxuICAgIC8vIGNyYXdsIHRoZSBET00gdG8gZmluZCB0aGUgdGFnXG4gICAgaWYgKGlzU3RyaW5nKHNlbGVjdG9yKSkge1xuICAgICAgc2VsZWN0b3IgPSBzZWxlY3RvciA9PT0gJyonID9cbiAgICAgICAgLy8gc2VsZWN0IGFsbCByZWdpc3RlcmVkIHRhZ3NcbiAgICAgICAgLy8gJiB0YWdzIGZvdW5kIHdpdGggdGhlIHJpb3QtdGFnIGF0dHJpYnV0ZSBzZXRcbiAgICAgICAgYWxsVGFncyA9IHF1ZXJ5KCkgOlxuICAgICAgICAvLyBvciBqdXN0IHRoZSBvbmVzIG5hbWVkIGxpa2UgdGhlIHNlbGVjdG9yXG4gICAgICAgIHNlbGVjdG9yICsgcXVlcnkoc2VsZWN0b3Iuc3BsaXQoLywgKi8pKTtcblxuICAgICAgLy8gbWFrZSBzdXJlIHRvIHBhc3MgYWx3YXlzIGEgc2VsZWN0b3JcbiAgICAgIC8vIHRvIHRoZSBxdWVyeVNlbGVjdG9yQWxsIGZ1bmN0aW9uXG4gICAgICBlbGVtID0gc2VsZWN0b3IgPyAkJChzZWxlY3RvcikgOiBbXTtcbiAgICB9XG4gICAgZWxzZVxuICAgICAgLy8gcHJvYmFibHkgeW91IGhhdmUgcGFzc2VkIGFscmVhZHkgYSB0YWcgb3IgYSBOb2RlTGlzdFxuICAgICAgeyBlbGVtID0gc2VsZWN0b3I7IH1cblxuICAgIC8vIHNlbGVjdCBhbGwgdGhlIHJlZ2lzdGVyZWQgYW5kIG1vdW50IHRoZW0gaW5zaWRlIHRoZWlyIHJvb3QgZWxlbWVudHNcbiAgICBpZiAodGFnTmFtZSA9PT0gJyonKSB7XG4gICAgICAvLyBnZXQgYWxsIGN1c3RvbSB0YWdzXG4gICAgICB0YWdOYW1lID0gYWxsVGFncyB8fCBxdWVyeSgpO1xuICAgICAgLy8gaWYgdGhlIHJvb3QgZWxzIGl0J3MganVzdCBhIHNpbmdsZSB0YWdcbiAgICAgIGlmIChlbGVtLnRhZ05hbWUpXG4gICAgICAgIHsgZWxlbSA9ICQkKHRhZ05hbWUsIGVsZW0pOyB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgLy8gc2VsZWN0IGFsbCB0aGUgY2hpbGRyZW4gZm9yIGFsbCB0aGUgZGlmZmVyZW50IHJvb3QgZWxlbWVudHNcbiAgICAgICAgdmFyIG5vZGVMaXN0ID0gW107XG5cbiAgICAgICAgZWFjaChlbGVtLCBmdW5jdGlvbiAoX2VsKSB7IHJldHVybiBub2RlTGlzdC5wdXNoKCQkKHRhZ05hbWUsIF9lbCkpOyB9KTtcblxuICAgICAgICBlbGVtID0gbm9kZUxpc3Q7XG4gICAgICB9XG4gICAgICAvLyBnZXQgcmlkIG9mIHRoZSB0YWdOYW1lXG4gICAgICB0YWdOYW1lID0gMDtcbiAgICB9XG5cbiAgICBwdXNoVGFnc1RvKGVsZW0pO1xuXG4gICAgcmV0dXJuIHRhZ3NcbiAgfVxuXG4gIC8vIENyZWF0ZSBhIG1peGluIHRoYXQgY291bGQgYmUgZ2xvYmFsbHkgc2hhcmVkIGFjcm9zcyBhbGwgdGhlIHRhZ3NcbiAgdmFyIG1peGlucyA9IHt9O1xuICB2YXIgZ2xvYmFscyA9IG1peGluc1tHTE9CQUxfTUlYSU5dID0ge307XG4gIHZhciBtaXhpbnNfaWQgPSAwO1xuXG4gIC8qKlxuICAgKiBDcmVhdGUvUmV0dXJuIGEgbWl4aW4gYnkgaXRzIG5hbWVcbiAgICogQHBhcmFtICAgeyBTdHJpbmcgfSAgbmFtZSAtIG1peGluIG5hbWUgKGdsb2JhbCBtaXhpbiBpZiBvYmplY3QpXG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gIG1peCAtIG1peGluIGxvZ2ljXG4gICAqIEBwYXJhbSAgIHsgQm9vbGVhbiB9IGcgLSBpcyBnbG9iYWw/XG4gICAqIEByZXR1cm5zIHsgT2JqZWN0IH0gIHRoZSBtaXhpbiBsb2dpY1xuICAgKi9cbiAgZnVuY3Rpb24gbWl4aW4obmFtZSwgbWl4LCBnKSB7XG4gICAgLy8gVW5uYW1lZCBnbG9iYWxcbiAgICBpZiAoaXNPYmplY3QobmFtZSkpIHtcbiAgICAgIG1peGluKChcIl9fXCIgKyAobWl4aW5zX2lkKyspICsgXCJfX1wiKSwgbmFtZSwgdHJ1ZSk7XG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICB2YXIgc3RvcmUgPSBnID8gZ2xvYmFscyA6IG1peGlucztcblxuICAgIC8vIEdldHRlclxuICAgIGlmICghbWl4KSB7XG4gICAgICBpZiAoaXNVbmRlZmluZWQoc3RvcmVbbmFtZV0pKVxuICAgICAgICB7IHRocm93IG5ldyBFcnJvcigoXCJVbnJlZ2lzdGVyZWQgbWl4aW46IFwiICsgbmFtZSkpIH1cblxuICAgICAgcmV0dXJuIHN0b3JlW25hbWVdXG4gICAgfVxuXG4gICAgLy8gU2V0dGVyXG4gICAgc3RvcmVbbmFtZV0gPSBpc0Z1bmN0aW9uKG1peCkgP1xuICAgICAgZXh0ZW5kKG1peC5wcm90b3R5cGUsIHN0b3JlW25hbWVdIHx8IHt9KSAmJiBtaXggOlxuICAgICAgZXh0ZW5kKHN0b3JlW25hbWVdIHx8IHt9LCBtaXgpO1xuICB9XG5cbiAgLyoqXG4gICAqIFVwZGF0ZSBhbGwgdGhlIHRhZ3MgaW5zdGFuY2VzIGNyZWF0ZWRcbiAgICogQHJldHVybnMgeyBBcnJheSB9IGFsbCB0aGUgdGFncyBpbnN0YW5jZXNcbiAgICovXG4gIGZ1bmN0aW9uIHVwZGF0ZSQxKCkge1xuICAgIHJldHVybiBlYWNoKF9fVEFHU19DQUNIRSwgZnVuY3Rpb24gKHRhZykgeyByZXR1cm4gdGFnLnVwZGF0ZSgpOyB9KVxuICB9XG5cbiAgZnVuY3Rpb24gdW5yZWdpc3RlcihuYW1lKSB7XG4gICAgc3R5bGVNYW5hZ2VyLnJlbW92ZShuYW1lKTtcbiAgICByZXR1cm4gZGVsZXRlIF9fVEFHX0lNUExbbmFtZV1cbiAgfVxuXG4gIHZhciB2ZXJzaW9uID0gJ3YzLjExLjEnO1xuXG4gIHZhciBjb3JlID0gLyojX19QVVJFX18qL09iamVjdC5mcmVlemUoe1xuICAgIFRhZzogVGFnLFxuICAgIHRhZzogdGFnLFxuICAgIHRhZzI6IHRhZzIsXG4gICAgbW91bnQ6IG1vdW50LFxuICAgIG1peGluOiBtaXhpbixcbiAgICB1cGRhdGU6IHVwZGF0ZSQxLFxuICAgIHVucmVnaXN0ZXI6IHVucmVnaXN0ZXIsXG4gICAgdmVyc2lvbjogdmVyc2lvblxuICB9KTtcblxuICAvKipcbiAgICogQWRkIGEgbWl4aW4gdG8gdGhpcyB0YWdcbiAgICogQHJldHVybnMgeyBUYWcgfSB0aGUgY3VycmVudCB0YWcgaW5zdGFuY2VcbiAgICovXG4gIGZ1bmN0aW9uIGNvbXBvbmVudE1peGluKHRhZyQkMSkge1xuICAgIHZhciBtaXhpbnMgPSBbXSwgbGVuID0gYXJndW1lbnRzLmxlbmd0aCAtIDE7XG4gICAgd2hpbGUgKCBsZW4tLSA+IDAgKSBtaXhpbnNbIGxlbiBdID0gYXJndW1lbnRzWyBsZW4gKyAxIF07XG5cbiAgICBlYWNoKG1peGlucywgZnVuY3Rpb24gKG1peCkge1xuICAgICAgdmFyIGluc3RhbmNlO1xuICAgICAgdmFyIG9iajtcbiAgICAgIHZhciBwcm9wcyA9IFtdO1xuXG4gICAgICAvLyBwcm9wZXJ0aWVzIGJsYWNrbGlzdGVkIGFuZCB3aWxsIG5vdCBiZSBib3VuZCB0byB0aGUgdGFnIGluc3RhbmNlXG4gICAgICB2YXIgcHJvcHNCbGFja2xpc3QgPSBbJ2luaXQnLCAnX19wcm90b19fJ107XG5cbiAgICAgIG1peCA9IGlzU3RyaW5nKG1peCkgPyBtaXhpbihtaXgpIDogbWl4O1xuXG4gICAgICAvLyBjaGVjayBpZiB0aGUgbWl4aW4gaXMgYSBmdW5jdGlvblxuICAgICAgaWYgKGlzRnVuY3Rpb24obWl4KSkge1xuICAgICAgICAvLyBjcmVhdGUgdGhlIG5ldyBtaXhpbiBpbnN0YW5jZVxuICAgICAgICBpbnN0YW5jZSA9IG5ldyBtaXgoKTtcbiAgICAgIH0gZWxzZSB7IGluc3RhbmNlID0gbWl4OyB9XG5cbiAgICAgIHZhciBwcm90byA9IE9iamVjdC5nZXRQcm90b3R5cGVPZihpbnN0YW5jZSk7XG5cbiAgICAgIC8vIGJ1aWxkIG11bHRpbGV2ZWwgcHJvdG90eXBlIGluaGVyaXRhbmNlIGNoYWluIHByb3BlcnR5IGxpc3RcbiAgICAgIGRvIHsgcHJvcHMgPSBwcm9wcy5jb25jYXQoT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXMob2JqIHx8IGluc3RhbmNlKSk7IH1cbiAgICAgIHdoaWxlIChvYmogPSBPYmplY3QuZ2V0UHJvdG90eXBlT2Yob2JqIHx8IGluc3RhbmNlKSlcblxuICAgICAgLy8gbG9vcCB0aGUga2V5cyBpbiB0aGUgZnVuY3Rpb24gcHJvdG90eXBlIG9yIHRoZSBhbGwgb2JqZWN0IGtleXNcbiAgICAgIGVhY2gocHJvcHMsIGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgLy8gYmluZCBtZXRob2RzIHRvIHRhZ1xuICAgICAgICAvLyBhbGxvdyBtaXhpbnMgdG8gb3ZlcnJpZGUgb3RoZXIgcHJvcGVydGllcy9wYXJlbnQgbWl4aW5zXG4gICAgICAgIGlmICghY29udGFpbnMocHJvcHNCbGFja2xpc3QsIGtleSkpIHtcbiAgICAgICAgICAvLyBjaGVjayBmb3IgZ2V0dGVycy9zZXR0ZXJzXG4gICAgICAgICAgdmFyIGRlc2NyaXB0b3IgPSBnZXRQcm9wRGVzY3JpcHRvcihpbnN0YW5jZSwga2V5KSB8fCBnZXRQcm9wRGVzY3JpcHRvcihwcm90bywga2V5KTtcbiAgICAgICAgICB2YXIgaGFzR2V0dGVyU2V0dGVyID0gZGVzY3JpcHRvciAmJiAoZGVzY3JpcHRvci5nZXQgfHwgZGVzY3JpcHRvci5zZXQpO1xuXG4gICAgICAgICAgLy8gYXBwbHkgbWV0aG9kIG9ubHkgaWYgaXQgZG9lcyBub3QgYWxyZWFkeSBleGlzdCBvbiB0aGUgaW5zdGFuY2VcbiAgICAgICAgICBpZiAoIXRhZyQkMS5oYXNPd25Qcm9wZXJ0eShrZXkpICYmIGhhc0dldHRlclNldHRlcikge1xuICAgICAgICAgICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhZyQkMSwga2V5LCBkZXNjcmlwdG9yKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGFnJCQxW2tleV0gPSBpc0Z1bmN0aW9uKGluc3RhbmNlW2tleV0pID9cbiAgICAgICAgICAgICAgaW5zdGFuY2Vba2V5XS5iaW5kKHRhZyQkMSkgOlxuICAgICAgICAgICAgICBpbnN0YW5jZVtrZXldO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSk7XG5cbiAgICAgIC8vIGluaXQgbWV0aG9kIHdpbGwgYmUgY2FsbGVkIGF1dG9tYXRpY2FsbHlcbiAgICAgIGlmIChpbnN0YW5jZS5pbml0KVxuICAgICAgICB7IGluc3RhbmNlLmluaXQuYmluZCh0YWckJDEpKHRhZyQkMS5vcHRzKTsgfVxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRhZyQkMVxuICB9XG5cbiAgLyoqXG4gICAqIE1vdmUgdGhlIHBvc2l0aW9uIG9mIGEgY3VzdG9tIHRhZyBpbiBpdHMgcGFyZW50IHRhZ1xuICAgKiBAdGhpcyBUYWdcbiAgICogQHBhcmFtICAgeyBTdHJpbmcgfSB0YWdOYW1lIC0ga2V5IHdoZXJlIHRoZSB0YWcgd2FzIHN0b3JlZFxuICAgKiBAcGFyYW0gICB7IE51bWJlciB9IG5ld1BvcyAtIGluZGV4IHdoZXJlIHRoZSBuZXcgdGFnIHdpbGwgYmUgc3RvcmVkXG4gICAqL1xuICBmdW5jdGlvbiBtb3ZlQ2hpbGQodGFnTmFtZSwgbmV3UG9zKSB7XG4gICAgdmFyIHBhcmVudCA9IHRoaXMucGFyZW50O1xuICAgIHZhciB0YWdzO1xuICAgIC8vIG5vIHBhcmVudCBubyBtb3ZlXG4gICAgaWYgKCFwYXJlbnQpIHsgcmV0dXJuIH1cblxuICAgIHRhZ3MgPSBwYXJlbnQudGFnc1t0YWdOYW1lXTtcblxuICAgIGlmIChpc0FycmF5KHRhZ3MpKVxuICAgICAgeyB0YWdzLnNwbGljZShuZXdQb3MsIDAsIHRhZ3Muc3BsaWNlKHRhZ3MuaW5kZXhPZih0aGlzKSwgMSlbMF0pOyB9XG4gICAgZWxzZSB7IGFycmF5aXNoQWRkKHBhcmVudC50YWdzLCB0YWdOYW1lLCB0aGlzKTsgfVxuICB9XG5cbiAgLyoqXG4gICAqIE1vdmUgdmlydHVhbCB0YWcgYW5kIGFsbCBjaGlsZCBub2Rlc1xuICAgKiBAdGhpcyBUYWdcbiAgICogQHBhcmFtIHsgTm9kZSB9IHNyYyAgLSB0aGUgbm9kZSB0aGF0IHdpbGwgZG8gdGhlIGluc2VydGluZ1xuICAgKiBAcGFyYW0geyBUYWcgfSB0YXJnZXQgLSBpbnNlcnQgYmVmb3JlIHRoaXMgdGFnJ3MgZmlyc3QgY2hpbGRcbiAgICovXG4gIGZ1bmN0aW9uIG1vdmVWaXJ0dWFsKHNyYywgdGFyZ2V0KSB7XG4gICAgdmFyIHRoaXMkMSA9IHRoaXM7XG5cbiAgICB2YXIgZWwgPSB0aGlzLl9fLmhlYWQ7XG4gICAgdmFyIHNpYjtcbiAgICB2YXIgZnJhZyA9IGNyZWF0ZUZyYWdtZW50KCk7XG5cbiAgICB3aGlsZSAoZWwpIHtcbiAgICAgIHNpYiA9IGVsLm5leHRTaWJsaW5nO1xuICAgICAgZnJhZy5hcHBlbmRDaGlsZChlbCk7XG4gICAgICBlbCA9IHNpYjtcbiAgICAgIGlmIChlbCA9PT0gdGhpcyQxLl9fLnRhaWwpIHtcbiAgICAgICAgZnJhZy5hcHBlbmRDaGlsZChlbCk7XG4gICAgICAgIHNyYy5pbnNlcnRCZWZvcmUoZnJhZywgdGFyZ2V0Ll9fLmhlYWQpO1xuICAgICAgICBicmVha1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBDb252ZXJ0IHRoZSBpdGVtIGxvb3BlZCBpbnRvIGFuIG9iamVjdCB1c2VkIHRvIGV4dGVuZCB0aGUgY2hpbGQgdGFnIHByb3BlcnRpZXNcbiAgICogQHBhcmFtICAgeyBPYmplY3QgfSBleHByIC0gb2JqZWN0IGNvbnRhaW5pbmcgdGhlIGtleXMgdXNlZCB0byBleHRlbmQgdGhlIGNoaWxkcmVuIHRhZ3NcbiAgICogQHBhcmFtICAgeyAqIH0ga2V5IC0gdmFsdWUgdG8gYXNzaWduIHRvIHRoZSBuZXcgb2JqZWN0IHJldHVybmVkXG4gICAqIEBwYXJhbSAgIHsgKiB9IHZhbCAtIHZhbHVlIGNvbnRhaW5pbmcgdGhlIHBvc2l0aW9uIG9mIHRoZSBpdGVtIGluIHRoZSBhcnJheVxuICAgKiBAcmV0dXJucyB7IE9iamVjdCB9IC0gbmV3IG9iamVjdCBjb250YWluaW5nIHRoZSB2YWx1ZXMgb2YgdGhlIG9yaWdpbmFsIGl0ZW1cbiAgICpcbiAgICogVGhlIHZhcmlhYmxlcyAna2V5JyBhbmQgJ3ZhbCcgYXJlIGFyYml0cmFyeS5cbiAgICogVGhleSBkZXBlbmQgb24gdGhlIGNvbGxlY3Rpb24gdHlwZSBsb29wZWQgKEFycmF5LCBPYmplY3QpXG4gICAqIGFuZCBvbiB0aGUgZXhwcmVzc2lvbiB1c2VkIG9uIHRoZSBlYWNoIHRhZ1xuICAgKlxuICAgKi9cbiAgZnVuY3Rpb24gbWtpdGVtKGV4cHIsIGtleSwgdmFsKSB7XG4gICAgdmFyIGl0ZW0gPSB7fTtcbiAgICBpdGVtW2V4cHIua2V5XSA9IGtleTtcbiAgICBpZiAoZXhwci5wb3MpIHsgaXRlbVtleHByLnBvc10gPSB2YWw7IH1cbiAgICByZXR1cm4gaXRlbVxuICB9XG5cbiAgLyoqXG4gICAqIFVubW91bnQgdGhlIHJlZHVuZGFudCB0YWdzXG4gICAqIEBwYXJhbSAgIHsgQXJyYXkgfSBpdGVtcyAtIGFycmF5IGNvbnRhaW5pbmcgdGhlIGN1cnJlbnQgaXRlbXMgdG8gbG9vcFxuICAgKiBAcGFyYW0gICB7IEFycmF5IH0gdGFncyAtIGFycmF5IGNvbnRhaW5pbmcgYWxsIHRoZSBjaGlsZHJlbiB0YWdzXG4gICAqL1xuICBmdW5jdGlvbiB1bm1vdW50UmVkdW5kYW50KGl0ZW1zLCB0YWdzLCBmaWx0ZXJlZEl0ZW1zQ291bnQpIHtcbiAgICB2YXIgaSA9IHRhZ3MubGVuZ3RoO1xuICAgIHZhciBqID0gaXRlbXMubGVuZ3RoIC0gZmlsdGVyZWRJdGVtc0NvdW50O1xuXG4gICAgd2hpbGUgKGkgPiBqKSB7XG4gICAgICBpLS07XG4gICAgICByZW1vdmUuYXBwbHkodGFnc1tpXSwgW3RhZ3MsIGldKTtcbiAgICB9XG4gIH1cblxuXG4gIC8qKlxuICAgKiBSZW1vdmUgYSBjaGlsZCB0YWdcbiAgICogQHRoaXMgVGFnXG4gICAqIEBwYXJhbSAgIHsgQXJyYXkgfSB0YWdzIC0gdGFncyBjb2xsZWN0aW9uXG4gICAqIEBwYXJhbSAgIHsgTnVtYmVyIH0gaSAtIGluZGV4IG9mIHRoZSB0YWcgdG8gcmVtb3ZlXG4gICAqL1xuICBmdW5jdGlvbiByZW1vdmUodGFncywgaSkge1xuICAgIHRhZ3Muc3BsaWNlKGksIDEpO1xuICAgIHRoaXMudW5tb3VudCgpO1xuICAgIGFycmF5aXNoUmVtb3ZlKHRoaXMucGFyZW50LCB0aGlzLCB0aGlzLl9fLnRhZ05hbWUsIHRydWUpO1xuICB9XG5cbiAgLyoqXG4gICAqIE1vdmUgdGhlIG5lc3RlZCBjdXN0b20gdGFncyBpbiBub24gY3VzdG9tIGxvb3AgdGFnc1xuICAgKiBAdGhpcyBUYWdcbiAgICogQHBhcmFtICAgeyBOdW1iZXIgfSBpIC0gY3VycmVudCBwb3NpdGlvbiBvZiB0aGUgbG9vcCB0YWdcbiAgICovXG4gIGZ1bmN0aW9uIG1vdmVOZXN0ZWRUYWdzKGkpIHtcbiAgICB2YXIgdGhpcyQxID0gdGhpcztcblxuICAgIGVhY2goT2JqZWN0LmtleXModGhpcy50YWdzKSwgZnVuY3Rpb24gKHRhZ05hbWUpIHtcbiAgICAgIG1vdmVDaGlsZC5hcHBseSh0aGlzJDEudGFnc1t0YWdOYW1lXSwgW3RhZ05hbWUsIGldKTtcbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBNb3ZlIGEgY2hpbGQgdGFnXG4gICAqIEB0aGlzIFRhZ1xuICAgKiBAcGFyYW0gICB7IEhUTUxFbGVtZW50IH0gcm9vdCAtIGRvbSBub2RlIGNvbnRhaW5pbmcgYWxsIHRoZSBsb29wIGNoaWxkcmVuXG4gICAqIEBwYXJhbSAgIHsgVGFnIH0gbmV4dFRhZyAtIGluc3RhbmNlIG9mIHRoZSBuZXh0IHRhZyBwcmVjZWRpbmcgdGhlIG9uZSB3ZSB3YW50IHRvIG1vdmVcbiAgICogQHBhcmFtICAgeyBCb29sZWFuIH0gaXNWaXJ0dWFsIC0gaXMgaXQgYSB2aXJ0dWFsIHRhZz9cbiAgICovXG4gIGZ1bmN0aW9uIG1vdmUocm9vdCwgbmV4dFRhZywgaXNWaXJ0dWFsKSB7XG4gICAgaWYgKGlzVmlydHVhbClcbiAgICAgIHsgbW92ZVZpcnR1YWwuYXBwbHkodGhpcywgW3Jvb3QsIG5leHRUYWddKTsgfVxuICAgIGVsc2VcbiAgICAgIHsgc2FmZUluc2VydChyb290LCB0aGlzLnJvb3QsIG5leHRUYWcucm9vdCk7IH1cbiAgfVxuXG4gIC8qKlxuICAgKiBJbnNlcnQgYW5kIG1vdW50IGEgY2hpbGQgdGFnXG4gICAqIEB0aGlzIFRhZ1xuICAgKiBAcGFyYW0gICB7IEhUTUxFbGVtZW50IH0gcm9vdCAtIGRvbSBub2RlIGNvbnRhaW5pbmcgYWxsIHRoZSBsb29wIGNoaWxkcmVuXG4gICAqIEBwYXJhbSAgIHsgVGFnIH0gbmV4dFRhZyAtIGluc3RhbmNlIG9mIHRoZSBuZXh0IHRhZyBwcmVjZWRpbmcgdGhlIG9uZSB3ZSB3YW50IHRvIGluc2VydFxuICAgKiBAcGFyYW0gICB7IEJvb2xlYW4gfSBpc1ZpcnR1YWwgLSBpcyBpdCBhIHZpcnR1YWwgdGFnP1xuICAgKi9cbiAgZnVuY3Rpb24gaW5zZXJ0KHJvb3QsIG5leHRUYWcsIGlzVmlydHVhbCkge1xuICAgIGlmIChpc1ZpcnR1YWwpXG4gICAgICB7IG1ha2VWaXJ0dWFsLmFwcGx5KHRoaXMsIFtyb290LCBuZXh0VGFnXSk7IH1cbiAgICBlbHNlXG4gICAgICB7IHNhZmVJbnNlcnQocm9vdCwgdGhpcy5yb290LCBuZXh0VGFnLnJvb3QpOyB9XG4gIH1cblxuICAvKipcbiAgICogQXBwZW5kIGEgbmV3IHRhZyBpbnRvIHRoZSBET01cbiAgICogQHRoaXMgVGFnXG4gICAqIEBwYXJhbSAgIHsgSFRNTEVsZW1lbnQgfSByb290IC0gZG9tIG5vZGUgY29udGFpbmluZyBhbGwgdGhlIGxvb3AgY2hpbGRyZW5cbiAgICogQHBhcmFtICAgeyBCb29sZWFuIH0gaXNWaXJ0dWFsIC0gaXMgaXQgYSB2aXJ0dWFsIHRhZz9cbiAgICovXG4gIGZ1bmN0aW9uIGFwcGVuZChyb290LCBpc1ZpcnR1YWwpIHtcbiAgICBpZiAoaXNWaXJ0dWFsKVxuICAgICAgeyBtYWtlVmlydHVhbC5jYWxsKHRoaXMsIHJvb3QpOyB9XG4gICAgZWxzZVxuICAgICAgeyByb290LmFwcGVuZENoaWxkKHRoaXMucm9vdCk7IH1cbiAgfVxuXG4gIC8qKlxuICAgKiBSZXR1cm4gdGhlIHZhbHVlIHdlIHdhbnQgdG8gdXNlIHRvIGxvb2t1cCB0aGUgcG9zdGlvbiBvZiBvdXIgaXRlbXMgaW4gdGhlIGNvbGxlY3Rpb25cbiAgICogQHBhcmFtICAgeyBTdHJpbmcgfSAga2V5QXR0ciAgICAgICAgIC0gbG9va3VwIHN0cmluZyBvciBleHByZXNzaW9uXG4gICAqIEBwYXJhbSAgIHsgKiB9ICAgICAgIG9yaWdpbmFsSXRlbSAgICAtIG9yaWdpbmFsIGl0ZW0gZnJvbSB0aGUgY29sbGVjdGlvblxuICAgKiBAcGFyYW0gICB7IE9iamVjdCB9ICBrZXllZEl0ZW0gICAgICAgLSBvYmplY3QgY3JlYXRlZCBieSByaW90IHZpYSB7IGl0ZW0sIGkgaW4gY29sbGVjdGlvbiB9XG4gICAqIEBwYXJhbSAgIHsgQm9vbGVhbiB9IGhhc0tleUF0dHJFeHByICAtIGZsYWcgdG8gY2hlY2sgd2hldGhlciB0aGUga2V5IGlzIGFuIGV4cHJlc3Npb25cbiAgICogQHJldHVybnMgeyAqIH0gdmFsdWUgdGhhdCB3ZSB3aWxsIHVzZSB0byBmaWd1cmUgb3V0IHRoZSBpdGVtIHBvc2l0aW9uIHZpYSBjb2xsZWN0aW9uLmluZGV4T2ZcbiAgICovXG4gIGZ1bmN0aW9uIGdldEl0ZW1JZChrZXlBdHRyLCBvcmlnaW5hbEl0ZW0sIGtleWVkSXRlbSwgaGFzS2V5QXR0ckV4cHIpIHtcbiAgICBpZiAoa2V5QXR0cikge1xuICAgICAgcmV0dXJuIGhhc0tleUF0dHJFeHByID8gIHRtcGwoa2V5QXR0ciwga2V5ZWRJdGVtKSA6ICBvcmlnaW5hbEl0ZW1ba2V5QXR0cl1cbiAgICB9XG5cbiAgICByZXR1cm4gb3JpZ2luYWxJdGVtXG4gIH1cblxuICAvKipcbiAgICogTWFuYWdlIHRhZ3MgaGF2aW5nIHRoZSAnZWFjaCdcbiAgICogQHBhcmFtICAgeyBIVE1MRWxlbWVudCB9IGRvbSAtIERPTSBub2RlIHdlIG5lZWQgdG8gbG9vcFxuICAgKiBAcGFyYW0gICB7IFRhZyB9IHBhcmVudCAtIHBhcmVudCB0YWcgaW5zdGFuY2Ugd2hlcmUgdGhlIGRvbSBub2RlIGlzIGNvbnRhaW5lZFxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IGV4cHIgLSBzdHJpbmcgY29udGFpbmVkIGluIHRoZSAnZWFjaCcgYXR0cmlidXRlXG4gICAqIEByZXR1cm5zIHsgT2JqZWN0IH0gZXhwcmVzc2lvbiBvYmplY3QgZm9yIHRoaXMgZWFjaCBsb29wXG4gICAqL1xuICBmdW5jdGlvbiBfZWFjaChkb20sIHBhcmVudCwgZXhwcikge1xuICAgIHZhciBtdXN0UmVvcmRlciA9IHR5cGVvZiBnZXRBdHRyaWJ1dGUoZG9tLCBMT09QX05PX1JFT1JERVJfRElSRUNUSVZFKSAhPT0gVF9TVFJJTkcgfHwgcmVtb3ZlQXR0cmlidXRlKGRvbSwgTE9PUF9OT19SRU9SREVSX0RJUkVDVElWRSk7XG4gICAgdmFyIGtleUF0dHIgPSBnZXRBdHRyaWJ1dGUoZG9tLCBLRVlfRElSRUNUSVZFKTtcbiAgICB2YXIgaGFzS2V5QXR0ckV4cHIgPSBrZXlBdHRyID8gdG1wbC5oYXNFeHByKGtleUF0dHIpIDogZmFsc2U7XG4gICAgdmFyIHRhZ05hbWUgPSBnZXROYW1lKGRvbSk7XG4gICAgdmFyIGltcGwgPSBfX1RBR19JTVBMW3RhZ05hbWVdO1xuICAgIHZhciBwYXJlbnROb2RlID0gZG9tLnBhcmVudE5vZGU7XG4gICAgdmFyIHBsYWNlaG9sZGVyID0gY3JlYXRlRE9NUGxhY2Vob2xkZXIoKTtcbiAgICB2YXIgY2hpbGQgPSBnZXQoZG9tKTtcbiAgICB2YXIgaWZFeHByID0gZ2V0QXR0cmlidXRlKGRvbSwgQ09ORElUSU9OQUxfRElSRUNUSVZFKTtcbiAgICB2YXIgdGFncyA9IFtdO1xuICAgIHZhciBpc0xvb3AgPSB0cnVlO1xuICAgIHZhciBpbm5lckhUTUwgPSBkb20uaW5uZXJIVE1MO1xuICAgIHZhciBpc0Fub255bW91cyA9ICFfX1RBR19JTVBMW3RhZ05hbWVdO1xuICAgIHZhciBpc1ZpcnR1YWwgPSBkb20udGFnTmFtZSA9PT0gJ1ZJUlRVQUwnO1xuICAgIHZhciBvbGRJdGVtcyA9IFtdO1xuXG4gICAgLy8gcmVtb3ZlIHRoZSBlYWNoIHByb3BlcnR5IGZyb20gdGhlIG9yaWdpbmFsIHRhZ1xuICAgIHJlbW92ZUF0dHJpYnV0ZShkb20sIExPT1BfRElSRUNUSVZFKTtcbiAgICByZW1vdmVBdHRyaWJ1dGUoZG9tLCBLRVlfRElSRUNUSVZFKTtcblxuICAgIC8vIHBhcnNlIHRoZSBlYWNoIGV4cHJlc3Npb25cbiAgICBleHByID0gdG1wbC5sb29wS2V5cyhleHByKTtcbiAgICBleHByLmlzTG9vcCA9IHRydWU7XG5cbiAgICBpZiAoaWZFeHByKSB7IHJlbW92ZUF0dHJpYnV0ZShkb20sIENPTkRJVElPTkFMX0RJUkVDVElWRSk7IH1cblxuICAgIC8vIGluc2VydCBhIG1hcmtlZCB3aGVyZSB0aGUgbG9vcCB0YWdzIHdpbGwgYmUgaW5qZWN0ZWRcbiAgICBwYXJlbnROb2RlLmluc2VydEJlZm9yZShwbGFjZWhvbGRlciwgZG9tKTtcbiAgICBwYXJlbnROb2RlLnJlbW92ZUNoaWxkKGRvbSk7XG5cbiAgICBleHByLnVwZGF0ZSA9IGZ1bmN0aW9uIHVwZGF0ZUVhY2goKSB7XG4gICAgICAvLyBnZXQgdGhlIG5ldyBpdGVtcyBjb2xsZWN0aW9uXG4gICAgICBleHByLnZhbHVlID0gdG1wbChleHByLnZhbCwgcGFyZW50KTtcblxuICAgICAgdmFyIGl0ZW1zID0gZXhwci52YWx1ZTtcbiAgICAgIHZhciBmcmFnID0gY3JlYXRlRnJhZ21lbnQoKTtcbiAgICAgIHZhciBpc09iamVjdCA9ICFpc0FycmF5KGl0ZW1zKSAmJiAhaXNTdHJpbmcoaXRlbXMpO1xuICAgICAgdmFyIHJvb3QgPSBwbGFjZWhvbGRlci5wYXJlbnROb2RlO1xuICAgICAgdmFyIHRtcEl0ZW1zID0gW107XG4gICAgICB2YXIgaGFzS2V5cyA9IGlzT2JqZWN0ICYmICEhaXRlbXM7XG5cbiAgICAgIC8vIGlmIHRoaXMgRE9NIHdhcyByZW1vdmVkIHRoZSB1cGRhdGUgaGVyZSBpcyB1c2VsZXNzXG4gICAgICAvLyB0aGlzIGNvbmRpdGlvbiBmaXhlcyBhbHNvIGEgd2VpcmQgYXN5bmMgaXNzdWUgb24gSUUgaW4gb3VyIHVuaXQgdGVzdFxuICAgICAgaWYgKCFyb290KSB7IHJldHVybiB9XG5cbiAgICAgIC8vIG9iamVjdCBsb29wLiBhbnkgY2hhbmdlcyBjYXVzZSBmdWxsIHJlZHJhd1xuICAgICAgaWYgKGlzT2JqZWN0KSB7XG4gICAgICAgIGl0ZW1zID0gaXRlbXMgPyBPYmplY3Qua2V5cyhpdGVtcykubWFwKGZ1bmN0aW9uIChrZXkpIHsgcmV0dXJuIG1raXRlbShleHByLCBpdGVtc1trZXldLCBrZXkpOyB9KSA6IFtdO1xuICAgICAgfVxuXG4gICAgICAvLyBzdG9yZSB0aGUgYW1vdW50IG9mIGZpbHRlcmVkIGl0ZW1zXG4gICAgICB2YXIgZmlsdGVyZWRJdGVtc0NvdW50ID0gMDtcblxuICAgICAgLy8gbG9vcCBhbGwgdGhlIG5ldyBpdGVtc1xuICAgICAgZWFjaChpdGVtcywgZnVuY3Rpb24gKF9pdGVtLCBpbmRleCkge1xuICAgICAgICB2YXIgaSA9IGluZGV4IC0gZmlsdGVyZWRJdGVtc0NvdW50O1xuICAgICAgICB2YXIgaXRlbSA9ICFoYXNLZXlzICYmIGV4cHIua2V5ID8gbWtpdGVtKGV4cHIsIF9pdGVtLCBpbmRleCkgOiBfaXRlbTtcblxuICAgICAgICAvLyBza2lwIHRoaXMgaXRlbSBiZWNhdXNlIGl0IG11c3QgYmUgZmlsdGVyZWRcbiAgICAgICAgaWYgKGlmRXhwciAmJiAhdG1wbChpZkV4cHIsIGV4dGVuZChjcmVhdGUocGFyZW50KSwgaXRlbSkpKSB7XG4gICAgICAgICAgZmlsdGVyZWRJdGVtc0NvdW50ICsrO1xuICAgICAgICAgIHJldHVyblxuICAgICAgICB9XG5cbiAgICAgICAgdmFyIGl0ZW1JZCA9IGdldEl0ZW1JZChrZXlBdHRyLCBfaXRlbSwgaXRlbSwgaGFzS2V5QXR0ckV4cHIpO1xuICAgICAgICAvLyByZW9yZGVyIG9ubHkgaWYgdGhlIGl0ZW1zIGFyZSBub3Qgb2JqZWN0c1xuICAgICAgICAvLyBvciBhIGtleSBhdHRyaWJ1dGUgaGFzIGJlZW4gcHJvdmlkZWRcbiAgICAgICAgdmFyIGRvUmVvcmRlciA9ICFpc09iamVjdCAmJiBtdXN0UmVvcmRlciAmJiB0eXBlb2YgX2l0ZW0gPT09IFRfT0JKRUNUIHx8IGtleUF0dHI7XG4gICAgICAgIHZhciBvbGRQb3MgPSBvbGRJdGVtcy5pbmRleE9mKGl0ZW1JZCk7XG4gICAgICAgIHZhciBpc05ldyA9IG9sZFBvcyA9PT0gLTE7XG4gICAgICAgIHZhciBwb3MgPSAhaXNOZXcgJiYgZG9SZW9yZGVyID8gb2xkUG9zIDogaTtcbiAgICAgICAgLy8gZG9lcyBhIHRhZyBleGlzdCBpbiB0aGlzIHBvc2l0aW9uP1xuICAgICAgICB2YXIgdGFnID0gdGFnc1twb3NdO1xuICAgICAgICB2YXIgbXVzdEFwcGVuZCA9IGkgPj0gb2xkSXRlbXMubGVuZ3RoO1xuICAgICAgICB2YXIgbXVzdENyZWF0ZSA9IGRvUmVvcmRlciAmJiBpc05ldyB8fCAhZG9SZW9yZGVyICYmICF0YWcgfHwgIXRhZ3NbaV07XG5cbiAgICAgICAgLy8gbmV3IHRhZ1xuICAgICAgICBpZiAobXVzdENyZWF0ZSkge1xuICAgICAgICAgIHRhZyA9IGNyZWF0ZVRhZyhpbXBsLCB7XG4gICAgICAgICAgICBwYXJlbnQ6IHBhcmVudCxcbiAgICAgICAgICAgIGlzTG9vcDogaXNMb29wLFxuICAgICAgICAgICAgaXNBbm9ueW1vdXM6IGlzQW5vbnltb3VzLFxuICAgICAgICAgICAgdGFnTmFtZTogdGFnTmFtZSxcbiAgICAgICAgICAgIHJvb3Q6IGRvbS5jbG9uZU5vZGUoaXNBbm9ueW1vdXMpLFxuICAgICAgICAgICAgaXRlbTogaXRlbSxcbiAgICAgICAgICAgIGluZGV4OiBpLFxuICAgICAgICAgIH0sIGlubmVySFRNTCk7XG5cbiAgICAgICAgICAvLyBtb3VudCB0aGUgdGFnXG4gICAgICAgICAgdGFnLm1vdW50KCk7XG5cbiAgICAgICAgICBpZiAobXVzdEFwcGVuZClcbiAgICAgICAgICAgIHsgYXBwZW5kLmFwcGx5KHRhZywgW2ZyYWcgfHwgcm9vdCwgaXNWaXJ0dWFsXSk7IH1cbiAgICAgICAgICBlbHNlXG4gICAgICAgICAgICB7IGluc2VydC5hcHBseSh0YWcsIFtyb290LCB0YWdzW2ldLCBpc1ZpcnR1YWxdKTsgfVxuXG4gICAgICAgICAgaWYgKCFtdXN0QXBwZW5kKSB7IG9sZEl0ZW1zLnNwbGljZShpLCAwLCBpdGVtKTsgfVxuICAgICAgICAgIHRhZ3Muc3BsaWNlKGksIDAsIHRhZyk7XG4gICAgICAgICAgaWYgKGNoaWxkKSB7IGFycmF5aXNoQWRkKHBhcmVudC50YWdzLCB0YWdOYW1lLCB0YWcsIHRydWUpOyB9XG4gICAgICAgIH0gZWxzZSBpZiAocG9zICE9PSBpICYmIGRvUmVvcmRlcikge1xuICAgICAgICAgIC8vIG1vdmVcbiAgICAgICAgICBpZiAoa2V5QXR0ciB8fCBjb250YWlucyhpdGVtcywgb2xkSXRlbXNbcG9zXSkpIHtcbiAgICAgICAgICAgIG1vdmUuYXBwbHkodGFnLCBbcm9vdCwgdGFnc1tpXSwgaXNWaXJ0dWFsXSk7XG4gICAgICAgICAgICAvLyBtb3ZlIHRoZSBvbGQgdGFnIGluc3RhbmNlXG4gICAgICAgICAgICB0YWdzLnNwbGljZShpLCAwLCB0YWdzLnNwbGljZShwb3MsIDEpWzBdKTtcbiAgICAgICAgICAgIC8vIG1vdmUgdGhlIG9sZCBpdGVtXG4gICAgICAgICAgICBvbGRJdGVtcy5zcGxpY2UoaSwgMCwgb2xkSXRlbXMuc3BsaWNlKHBvcywgMSlbMF0pO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIC8vIHVwZGF0ZSB0aGUgcG9zaXRpb24gYXR0cmlidXRlIGlmIGl0IGV4aXN0c1xuICAgICAgICAgIGlmIChleHByLnBvcykgeyB0YWdbZXhwci5wb3NdID0gaTsgfVxuXG4gICAgICAgICAgLy8gaWYgdGhlIGxvb3AgdGFncyBhcmUgbm90IGN1c3RvbVxuICAgICAgICAgIC8vIHdlIG5lZWQgdG8gbW92ZSBhbGwgdGhlaXIgY3VzdG9tIHRhZ3MgaW50byB0aGUgcmlnaHQgcG9zaXRpb25cbiAgICAgICAgICBpZiAoIWNoaWxkICYmIHRhZy50YWdzKSB7IG1vdmVOZXN0ZWRUYWdzLmNhbGwodGFnLCBpKTsgfVxuICAgICAgICB9XG5cbiAgICAgICAgLy8gY2FjaGUgdGhlIG9yaWdpbmFsIGl0ZW0gdG8gdXNlIGl0IGluIHRoZSBldmVudHMgYm91bmQgdG8gdGhpcyBub2RlXG4gICAgICAgIC8vIGFuZCBpdHMgY2hpbGRyZW5cbiAgICAgICAgZXh0ZW5kKHRhZy5fXywge1xuICAgICAgICAgIGl0ZW06IGl0ZW0sXG4gICAgICAgICAgaW5kZXg6IGksXG4gICAgICAgICAgcGFyZW50OiBwYXJlbnRcbiAgICAgICAgfSk7XG5cbiAgICAgICAgdG1wSXRlbXNbaV0gPSBpdGVtSWQ7XG5cbiAgICAgICAgaWYgKCFtdXN0Q3JlYXRlKSB7IHRhZy51cGRhdGUoaXRlbSk7IH1cbiAgICAgIH0pO1xuXG4gICAgICAvLyByZW1vdmUgdGhlIHJlZHVuZGFudCB0YWdzXG4gICAgICB1bm1vdW50UmVkdW5kYW50KGl0ZW1zLCB0YWdzLCBmaWx0ZXJlZEl0ZW1zQ291bnQpO1xuXG4gICAgICAvLyBjbG9uZSB0aGUgaXRlbXMgYXJyYXlcbiAgICAgIG9sZEl0ZW1zID0gdG1wSXRlbXMuc2xpY2UoKTtcblxuICAgICAgcm9vdC5pbnNlcnRCZWZvcmUoZnJhZywgcGxhY2Vob2xkZXIpO1xuICAgIH07XG5cbiAgICBleHByLnVubW91bnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICBlYWNoKHRhZ3MsIGZ1bmN0aW9uICh0KSB7IHQudW5tb3VudCgpOyB9KTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIGV4cHJcbiAgfVxuXG4gIHZhciBSZWZFeHByID0ge1xuICAgIGluaXQ6IGZ1bmN0aW9uIGluaXQoZG9tLCBwYXJlbnQsIGF0dHJOYW1lLCBhdHRyVmFsdWUpIHtcbiAgICAgIHRoaXMuZG9tID0gZG9tO1xuICAgICAgdGhpcy5hdHRyID0gYXR0ck5hbWU7XG4gICAgICB0aGlzLnJhd1ZhbHVlID0gYXR0clZhbHVlO1xuICAgICAgdGhpcy5wYXJlbnQgPSBwYXJlbnQ7XG4gICAgICB0aGlzLmhhc0V4cCA9IHRtcGwuaGFzRXhwcihhdHRyVmFsdWUpO1xuICAgICAgcmV0dXJuIHRoaXNcbiAgICB9LFxuICAgIHVwZGF0ZTogZnVuY3Rpb24gdXBkYXRlKCkge1xuICAgICAgdmFyIG9sZCA9IHRoaXMudmFsdWU7XG4gICAgICB2YXIgY3VzdG9tUGFyZW50ID0gdGhpcy5wYXJlbnQgJiYgZ2V0SW1tZWRpYXRlQ3VzdG9tUGFyZW50KHRoaXMucGFyZW50KTtcbiAgICAgIC8vIGlmIHRoZSByZWZlcmVuY2VkIGVsZW1lbnQgaXMgYSBjdXN0b20gdGFnLCB0aGVuIHdlIHNldCB0aGUgdGFnIGl0c2VsZiwgcmF0aGVyIHRoYW4gRE9NXG4gICAgICB2YXIgdGFnT3JEb20gPSB0aGlzLmRvbS5fX3JlZiB8fCB0aGlzLnRhZyB8fCB0aGlzLmRvbTtcblxuICAgICAgdGhpcy52YWx1ZSA9IHRoaXMuaGFzRXhwID8gdG1wbCh0aGlzLnJhd1ZhbHVlLCB0aGlzLnBhcmVudCkgOiB0aGlzLnJhd1ZhbHVlO1xuXG4gICAgICAvLyB0aGUgbmFtZSBjaGFuZ2VkLCBzbyB3ZSBuZWVkIHRvIHJlbW92ZSBpdCBmcm9tIHRoZSBvbGQga2V5IChpZiBwcmVzZW50KVxuICAgICAgaWYgKCFpc0JsYW5rKG9sZCkgJiYgY3VzdG9tUGFyZW50KSB7IGFycmF5aXNoUmVtb3ZlKGN1c3RvbVBhcmVudC5yZWZzLCBvbGQsIHRhZ09yRG9tKTsgfVxuICAgICAgaWYgKCFpc0JsYW5rKHRoaXMudmFsdWUpICYmIGlzU3RyaW5nKHRoaXMudmFsdWUpKSB7XG4gICAgICAgIC8vIGFkZCBpdCB0byB0aGUgcmVmcyBvZiBwYXJlbnQgdGFnICh0aGlzIGJlaGF2aW9yIHdhcyBjaGFuZ2VkID49My4wKVxuICAgICAgICBpZiAoY3VzdG9tUGFyZW50KSB7IGFycmF5aXNoQWRkKFxuICAgICAgICAgIGN1c3RvbVBhcmVudC5yZWZzLFxuICAgICAgICAgIHRoaXMudmFsdWUsXG4gICAgICAgICAgdGFnT3JEb20sXG4gICAgICAgICAgLy8gdXNlIGFuIGFycmF5IGlmIGl0J3MgYSBsb29wZWQgbm9kZSBhbmQgdGhlIHJlZiBpcyBub3QgYW4gZXhwcmVzc2lvblxuICAgICAgICAgIG51bGwsXG4gICAgICAgICAgdGhpcy5wYXJlbnQuX18uaW5kZXhcbiAgICAgICAgKTsgfVxuXG4gICAgICAgIGlmICh0aGlzLnZhbHVlICE9PSBvbGQpIHtcbiAgICAgICAgICBzZXRBdHRyaWJ1dGUodGhpcy5kb20sIHRoaXMuYXR0ciwgdGhpcy52YWx1ZSk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJlbW92ZUF0dHJpYnV0ZSh0aGlzLmRvbSwgdGhpcy5hdHRyKTtcbiAgICAgIH1cblxuICAgICAgLy8gY2FjaGUgdGhlIHJlZiBib3VuZCB0byB0aGlzIGRvbSBub2RlXG4gICAgICAvLyB0byByZXVzZSBpdCBpbiBmdXR1cmUgKHNlZSBhbHNvICMyMzI5KVxuICAgICAgaWYgKCF0aGlzLmRvbS5fX3JlZikgeyB0aGlzLmRvbS5fX3JlZiA9IHRhZ09yRG9tOyB9XG4gICAgfSxcbiAgICB1bm1vdW50OiBmdW5jdGlvbiB1bm1vdW50KCkge1xuICAgICAgdmFyIHRhZ09yRG9tID0gdGhpcy50YWcgfHwgdGhpcy5kb207XG4gICAgICB2YXIgY3VzdG9tUGFyZW50ID0gdGhpcy5wYXJlbnQgJiYgZ2V0SW1tZWRpYXRlQ3VzdG9tUGFyZW50KHRoaXMucGFyZW50KTtcbiAgICAgIGlmICghaXNCbGFuayh0aGlzLnZhbHVlKSAmJiBjdXN0b21QYXJlbnQpXG4gICAgICAgIHsgYXJyYXlpc2hSZW1vdmUoY3VzdG9tUGFyZW50LnJlZnMsIHRoaXMudmFsdWUsIHRhZ09yRG9tKTsgfVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBDcmVhdGUgYSBuZXcgcmVmIGRpcmVjdGl2ZVxuICAgKiBAcGFyYW0gICB7IEhUTUxFbGVtZW50IH0gZG9tIC0gZG9tIG5vZGUgaGF2aW5nIHRoZSByZWYgYXR0cmlidXRlXG4gICAqIEBwYXJhbSAgIHsgVGFnIH0gY29udGV4dCAtIHRhZyBpbnN0YW5jZSB3aGVyZSB0aGUgRE9NIG5vZGUgaXMgbG9jYXRlZFxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IGF0dHJOYW1lIC0gZWl0aGVyICdyZWYnIG9yICdkYXRhLXJlZidcbiAgICogQHBhcmFtICAgeyBTdHJpbmcgfSBhdHRyVmFsdWUgLSB2YWx1ZSBvZiB0aGUgcmVmIGF0dHJpYnV0ZVxuICAgKiBAcmV0dXJucyB7IFJlZkV4cHIgfSBhIG5ldyBSZWZFeHByIG9iamVjdFxuICAgKi9cbiAgZnVuY3Rpb24gY3JlYXRlUmVmRGlyZWN0aXZlKGRvbSwgdGFnLCBhdHRyTmFtZSwgYXR0clZhbHVlKSB7XG4gICAgcmV0dXJuIGNyZWF0ZShSZWZFeHByKS5pbml0KGRvbSwgdGFnLCBhdHRyTmFtZSwgYXR0clZhbHVlKVxuICB9XG5cbiAgLyoqXG4gICAqIFRyaWdnZXIgdGhlIHVubW91bnQgbWV0aG9kIG9uIGFsbCB0aGUgZXhwcmVzc2lvbnNcbiAgICogQHBhcmFtICAgeyBBcnJheSB9IGV4cHJlc3Npb25zIC0gRE9NIGV4cHJlc3Npb25zXG4gICAqL1xuICBmdW5jdGlvbiB1bm1vdW50QWxsKGV4cHJlc3Npb25zKSB7XG4gICAgZWFjaChleHByZXNzaW9ucywgZnVuY3Rpb24gKGV4cHIpIHtcbiAgICAgIGlmIChleHByLnVubW91bnQpIHsgZXhwci51bm1vdW50KHRydWUpOyB9XG4gICAgICBlbHNlIGlmIChleHByLnRhZ05hbWUpIHsgZXhwci50YWcudW5tb3VudCh0cnVlKTsgfVxuICAgICAgZWxzZSBpZiAoZXhwci51bm1vdW50KSB7IGV4cHIudW5tb3VudCgpOyB9XG4gICAgfSk7XG4gIH1cblxuICB2YXIgSWZFeHByID0ge1xuICAgIGluaXQ6IGZ1bmN0aW9uIGluaXQoZG9tLCB0YWcsIGV4cHIpIHtcbiAgICAgIHJlbW92ZUF0dHJpYnV0ZShkb20sIENPTkRJVElPTkFMX0RJUkVDVElWRSk7XG4gICAgICBleHRlbmQodGhpcywgeyB0YWc6IHRhZywgZXhwcjogZXhwciwgc3R1YjogY3JlYXRlRE9NUGxhY2Vob2xkZXIoKSwgcHJpc3RpbmU6IGRvbSB9KTtcbiAgICAgIHZhciBwID0gZG9tLnBhcmVudE5vZGU7XG4gICAgICBwLmluc2VydEJlZm9yZSh0aGlzLnN0dWIsIGRvbSk7XG4gICAgICBwLnJlbW92ZUNoaWxkKGRvbSk7XG5cbiAgICAgIHJldHVybiB0aGlzXG4gICAgfSxcbiAgICB1cGRhdGU6IGZ1bmN0aW9uIHVwZGF0ZSQkMSgpIHtcbiAgICAgIHRoaXMudmFsdWUgPSB0bXBsKHRoaXMuZXhwciwgdGhpcy50YWcpO1xuXG4gICAgICBpZiAoIXRoaXMuc3R1Yi5wYXJlbnROb2RlKSB7IHJldHVybiB9XG5cbiAgICAgIGlmICh0aGlzLnZhbHVlICYmICF0aGlzLmN1cnJlbnQpIHsgLy8gaW5zZXJ0XG4gICAgICAgIHRoaXMuY3VycmVudCA9IHRoaXMucHJpc3RpbmUuY2xvbmVOb2RlKHRydWUpO1xuICAgICAgICB0aGlzLnN0dWIucGFyZW50Tm9kZS5pbnNlcnRCZWZvcmUodGhpcy5jdXJyZW50LCB0aGlzLnN0dWIpO1xuICAgICAgICB0aGlzLmV4cHJlc3Npb25zID0gcGFyc2VFeHByZXNzaW9ucy5hcHBseSh0aGlzLnRhZywgW3RoaXMuY3VycmVudCwgdHJ1ZV0pO1xuICAgICAgfSBlbHNlIGlmICghdGhpcy52YWx1ZSAmJiB0aGlzLmN1cnJlbnQpIHsgLy8gcmVtb3ZlXG4gICAgICAgIHRoaXMudW5tb3VudCgpO1xuICAgICAgICB0aGlzLmN1cnJlbnQgPSBudWxsO1xuICAgICAgICB0aGlzLmV4cHJlc3Npb25zID0gW107XG4gICAgICB9XG5cbiAgICAgIGlmICh0aGlzLnZhbHVlKSB7IHVwZGF0ZS5jYWxsKHRoaXMudGFnLCB0aGlzLmV4cHJlc3Npb25zKTsgfVxuICAgIH0sXG4gICAgdW5tb3VudDogZnVuY3Rpb24gdW5tb3VudCgpIHtcbiAgICAgIGlmICh0aGlzLmN1cnJlbnQpIHtcbiAgICAgICAgaWYgKHRoaXMuY3VycmVudC5fdGFnKSB7XG4gICAgICAgICAgdGhpcy5jdXJyZW50Ll90YWcudW5tb3VudCgpO1xuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuY3VycmVudC5wYXJlbnROb2RlKSB7XG4gICAgICAgICAgdGhpcy5jdXJyZW50LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodGhpcy5jdXJyZW50KTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICB1bm1vdW50QWxsKHRoaXMuZXhwcmVzc2lvbnMgfHwgW10pO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBDcmVhdGUgYSBuZXcgaWYgZGlyZWN0aXZlXG4gICAqIEBwYXJhbSAgIHsgSFRNTEVsZW1lbnQgfSBkb20gLSBpZiByb290IGRvbSBub2RlXG4gICAqIEBwYXJhbSAgIHsgVGFnIH0gY29udGV4dCAtIHRhZyBpbnN0YW5jZSB3aGVyZSB0aGUgRE9NIG5vZGUgaXMgbG9jYXRlZFxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IGF0dHIgLSBpZiBleHByZXNzaW9uXG4gICAqIEByZXR1cm5zIHsgSUZFeHByIH0gYSBuZXcgSWZFeHByIG9iamVjdFxuICAgKi9cbiAgZnVuY3Rpb24gY3JlYXRlSWZEaXJlY3RpdmUoZG9tLCB0YWcsIGF0dHIpIHtcbiAgICByZXR1cm4gY3JlYXRlKElmRXhwcikuaW5pdChkb20sIHRhZywgYXR0cilcbiAgfVxuXG4gIC8qKlxuICAgKiBXYWxrIHRoZSB0YWcgRE9NIHRvIGRldGVjdCB0aGUgZXhwcmVzc2lvbnMgdG8gZXZhbHVhdGVcbiAgICogQHRoaXMgVGFnXG4gICAqIEBwYXJhbSAgIHsgSFRNTEVsZW1lbnQgfSByb290IC0gcm9vdCB0YWcgd2hlcmUgd2Ugd2lsbCBzdGFydCBkaWdnaW5nIHRoZSBleHByZXNzaW9uc1xuICAgKiBAcGFyYW0gICB7IEJvb2xlYW4gfSBtdXN0SW5jbHVkZVJvb3QgLSBmbGFnIHRvIGRlY2lkZSB3aGV0aGVyIHRoZSByb290IG11c3QgYmUgcGFyc2VkIGFzIHdlbGxcbiAgICogQHJldHVybnMgeyBBcnJheSB9IGFsbCB0aGUgZXhwcmVzc2lvbnMgZm91bmRcbiAgICovXG4gIGZ1bmN0aW9uIHBhcnNlRXhwcmVzc2lvbnMocm9vdCwgbXVzdEluY2x1ZGVSb290KSB7XG4gICAgdmFyIHRoaXMkMSA9IHRoaXM7XG5cbiAgICB2YXIgZXhwcmVzc2lvbnMgPSBbXTtcblxuICAgIHdhbGtOb2Rlcyhyb290LCBmdW5jdGlvbiAoZG9tKSB7XG4gICAgICB2YXIgdHlwZSA9IGRvbS5ub2RlVHlwZTtcbiAgICAgIHZhciBhdHRyO1xuICAgICAgdmFyIHRhZ0ltcGw7XG5cbiAgICAgIGlmICghbXVzdEluY2x1ZGVSb290ICYmIGRvbSA9PT0gcm9vdCkgeyByZXR1cm4gfVxuXG4gICAgICAvLyB0ZXh0IG5vZGVcbiAgICAgIGlmICh0eXBlID09PSAzICYmIGRvbS5wYXJlbnROb2RlLnRhZ05hbWUgIT09ICdTVFlMRScgJiYgdG1wbC5oYXNFeHByKGRvbS5ub2RlVmFsdWUpKVxuICAgICAgICB7IGV4cHJlc3Npb25zLnB1c2goe2RvbTogZG9tLCBleHByOiBkb20ubm9kZVZhbHVlfSk7IH1cblxuICAgICAgaWYgKHR5cGUgIT09IDEpIHsgcmV0dXJuIH1cblxuICAgICAgdmFyIGlzVmlydHVhbCA9IGRvbS50YWdOYW1lID09PSAnVklSVFVBTCc7XG5cbiAgICAgIC8vIGxvb3AuIGVhY2ggZG9lcyBpdCdzIG93biB0aGluZyAoZm9yIG5vdylcbiAgICAgIGlmIChhdHRyID0gZ2V0QXR0cmlidXRlKGRvbSwgTE9PUF9ESVJFQ1RJVkUpKSB7XG4gICAgICAgIGlmKGlzVmlydHVhbCkgeyBzZXRBdHRyaWJ1dGUoZG9tLCAnbG9vcFZpcnR1YWwnLCB0cnVlKTsgfSAvLyBpZ25vcmUgaGVyZSwgaGFuZGxlZCBpbiBfZWFjaFxuICAgICAgICBleHByZXNzaW9ucy5wdXNoKF9lYWNoKGRvbSwgdGhpcyQxLCBhdHRyKSk7XG4gICAgICAgIHJldHVybiBmYWxzZVxuICAgICAgfVxuXG4gICAgICAvLyBpZi1hdHRycyBiZWNvbWUgdGhlIG5ldyBwYXJlbnQuIEFueSBmb2xsb3dpbmcgZXhwcmVzc2lvbnMgKGVpdGhlciBvbiB0aGUgY3VycmVudFxuICAgICAgLy8gZWxlbWVudCwgb3IgYmVsb3cgaXQpIGJlY29tZSBjaGlsZHJlbiBvZiB0aGlzIGV4cHJlc3Npb24uXG4gICAgICBpZiAoYXR0ciA9IGdldEF0dHJpYnV0ZShkb20sIENPTkRJVElPTkFMX0RJUkVDVElWRSkpIHtcbiAgICAgICAgZXhwcmVzc2lvbnMucHVzaChjcmVhdGVJZkRpcmVjdGl2ZShkb20sIHRoaXMkMSwgYXR0cikpO1xuICAgICAgICByZXR1cm4gZmFsc2VcbiAgICAgIH1cblxuICAgICAgaWYgKGF0dHIgPSBnZXRBdHRyaWJ1dGUoZG9tLCBJU19ESVJFQ1RJVkUpKSB7XG4gICAgICAgIGlmICh0bXBsLmhhc0V4cHIoYXR0cikpIHtcbiAgICAgICAgICBleHByZXNzaW9ucy5wdXNoKHtcbiAgICAgICAgICAgIGlzUnRhZzogdHJ1ZSxcbiAgICAgICAgICAgIGV4cHI6IGF0dHIsXG4gICAgICAgICAgICBkb206IGRvbSxcbiAgICAgICAgICAgIGF0dHJzOiBbXS5zbGljZS5jYWxsKGRvbS5hdHRyaWJ1dGVzKVxuICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgcmV0dXJuIGZhbHNlXG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgLy8gaWYgdGhpcyBpcyBhIHRhZywgc3RvcCB0cmF2ZXJzaW5nIGhlcmUuXG4gICAgICAvLyB3ZSBpZ25vcmUgdGhlIHJvb3QsIHNpbmNlIHBhcnNlRXhwcmVzc2lvbnMgaXMgY2FsbGVkIHdoaWxlIHdlJ3JlIG1vdW50aW5nIHRoYXQgcm9vdFxuICAgICAgdGFnSW1wbCA9IGdldChkb20pO1xuXG4gICAgICBpZihpc1ZpcnR1YWwpIHtcbiAgICAgICAgaWYoZ2V0QXR0cmlidXRlKGRvbSwgJ3ZpcnR1YWxpemVkJykpIHtkb20ucGFyZW50RWxlbWVudC5yZW1vdmVDaGlsZChkb20pOyB9IC8vIHRhZyBjcmVhdGVkLCByZW1vdmUgZnJvbSBkb21cbiAgICAgICAgaWYoIXRhZ0ltcGwgJiYgIWdldEF0dHJpYnV0ZShkb20sICd2aXJ0dWFsaXplZCcpICYmICFnZXRBdHRyaWJ1dGUoZG9tLCAnbG9vcFZpcnR1YWwnKSkgIC8vIG9rIHRvIGNyZWF0ZSB2aXJ0dWFsIHRhZ1xuICAgICAgICAgIHsgdGFnSW1wbCA9IHsgdG1wbDogZG9tLm91dGVySFRNTCB9OyB9XG4gICAgICB9XG5cbiAgICAgIGlmICh0YWdJbXBsICYmIChkb20gIT09IHJvb3QgfHwgbXVzdEluY2x1ZGVSb290KSkge1xuICAgICAgICB2YXIgaGFzSXNEaXJlY3RpdmUgPSBnZXRBdHRyaWJ1dGUoZG9tLCBJU19ESVJFQ1RJVkUpO1xuICAgICAgICBpZihpc1ZpcnR1YWwgJiYgIWhhc0lzRGlyZWN0aXZlKSB7IC8vIGhhbmRsZWQgaW4gdXBkYXRlXG4gICAgICAgICAgLy8gY2FuIG5vdCByZW1vdmUgYXR0cmlidXRlIGxpa2UgZGlyZWN0aXZlc1xuICAgICAgICAgIC8vIHNvIGZsYWcgZm9yIHJlbW92YWwgYWZ0ZXIgY3JlYXRpb24gdG8gcHJldmVudCBtYXhpbXVtIHN0YWNrIGVycm9yXG4gICAgICAgICAgc2V0QXR0cmlidXRlKGRvbSwgJ3ZpcnR1YWxpemVkJywgdHJ1ZSk7XG4gICAgICAgICAgdmFyIHRhZyA9IGNyZWF0ZVRhZyhcbiAgICAgICAgICAgIHt0bXBsOiBkb20ub3V0ZXJIVE1MfSxcbiAgICAgICAgICAgIHtyb290OiBkb20sIHBhcmVudDogdGhpcyQxfSxcbiAgICAgICAgICAgIGRvbS5pbm5lckhUTUxcbiAgICAgICAgICApO1xuXG4gICAgICAgICAgZXhwcmVzc2lvbnMucHVzaCh0YWcpOyAvLyBubyByZXR1cm4sIGFub255bW91cyB0YWcsIGtlZXAgcGFyc2luZ1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGlmIChoYXNJc0RpcmVjdGl2ZSAmJiBpc1ZpcnR1YWwpXG4gICAgICAgICAgICB7IHdhcm4oKFwiVmlydHVhbCB0YWdzIHNob3VsZG4ndCBiZSB1c2VkIHRvZ2V0aGVyIHdpdGggdGhlIFxcXCJcIiArIElTX0RJUkVDVElWRSArIFwiXFxcIiBhdHRyaWJ1dGUgLSBodHRwczovL2dpdGh1Yi5jb20vcmlvdC9yaW90L2lzc3Vlcy8yNTExXCIpKTsgfVxuXG4gICAgICAgICAgZXhwcmVzc2lvbnMucHVzaChcbiAgICAgICAgICAgIGluaXRDaGlsZChcbiAgICAgICAgICAgICAgdGFnSW1wbCxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHJvb3Q6IGRvbSxcbiAgICAgICAgICAgICAgICBwYXJlbnQ6IHRoaXMkMVxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICBkb20uaW5uZXJIVE1MLFxuICAgICAgICAgICAgICB0aGlzJDFcbiAgICAgICAgICAgIClcbiAgICAgICAgICApO1xuICAgICAgICAgIHJldHVybiBmYWxzZVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIC8vIGF0dHJpYnV0ZSBleHByZXNzaW9uc1xuICAgICAgcGFyc2VBdHRyaWJ1dGVzLmFwcGx5KHRoaXMkMSwgW2RvbSwgZG9tLmF0dHJpYnV0ZXMsIGZ1bmN0aW9uIChhdHRyLCBleHByKSB7XG4gICAgICAgIGlmICghZXhwcikgeyByZXR1cm4gfVxuICAgICAgICBleHByZXNzaW9ucy5wdXNoKGV4cHIpO1xuICAgICAgfV0pO1xuICAgIH0pO1xuXG4gICAgcmV0dXJuIGV4cHJlc3Npb25zXG4gIH1cblxuICAvKipcbiAgICogQ2FsbHMgYGZuYCBmb3IgZXZlcnkgYXR0cmlidXRlIG9uIGFuIGVsZW1lbnQuIElmIHRoYXQgYXR0ciBoYXMgYW4gZXhwcmVzc2lvbixcbiAgICogaXQgaXMgYWxzbyBwYXNzZWQgdG8gZm4uXG4gICAqIEB0aGlzIFRhZ1xuICAgKiBAcGFyYW0gICB7IEhUTUxFbGVtZW50IH0gZG9tIC0gZG9tIG5vZGUgdG8gcGFyc2VcbiAgICogQHBhcmFtICAgeyBBcnJheSB9IGF0dHJzIC0gYXJyYXkgb2YgYXR0cmlidXRlc1xuICAgKiBAcGFyYW0gICB7IEZ1bmN0aW9uIH0gZm4gLSBjYWxsYmFjayB0byBleGVjIG9uIGFueSBpdGVyYXRpb25cbiAgICovXG4gIGZ1bmN0aW9uIHBhcnNlQXR0cmlidXRlcyhkb20sIGF0dHJzLCBmbikge1xuICAgIHZhciB0aGlzJDEgPSB0aGlzO1xuXG4gICAgZWFjaChhdHRycywgZnVuY3Rpb24gKGF0dHIpIHtcbiAgICAgIGlmICghYXR0cikgeyByZXR1cm4gZmFsc2UgfVxuXG4gICAgICB2YXIgbmFtZSA9IGF0dHIubmFtZTtcbiAgICAgIHZhciBib29sID0gaXNCb29sQXR0cihuYW1lKTtcbiAgICAgIHZhciBleHByO1xuXG4gICAgICBpZiAoY29udGFpbnMoUkVGX0RJUkVDVElWRVMsIG5hbWUpICYmIGRvbS50YWdOYW1lLnRvTG93ZXJDYXNlKCkgIT09IFlJRUxEX1RBRykge1xuICAgICAgICBleHByID0gIGNyZWF0ZVJlZkRpcmVjdGl2ZShkb20sIHRoaXMkMSwgbmFtZSwgYXR0ci52YWx1ZSk7XG4gICAgICB9IGVsc2UgaWYgKHRtcGwuaGFzRXhwcihhdHRyLnZhbHVlKSkge1xuICAgICAgICBleHByID0ge2RvbTogZG9tLCBleHByOiBhdHRyLnZhbHVlLCBhdHRyOiBuYW1lLCBib29sOiBib29sfTtcbiAgICAgIH1cblxuICAgICAgZm4oYXR0ciwgZXhwcik7XG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogTWFuYWdlIHRoZSBtb3VudCBzdGF0ZSBvZiBhIHRhZyB0cmlnZ2VyaW5nIGFsc28gdGhlIG9ic2VydmFibGUgZXZlbnRzXG4gICAqIEB0aGlzIFRhZ1xuICAgKiBAcGFyYW0geyBCb29sZWFuIH0gdmFsdWUgLSAuLm9mIHRoZSBpc01vdW50ZWQgZmxhZ1xuICAgKi9cbiAgZnVuY3Rpb24gc2V0TW91bnRTdGF0ZSh2YWx1ZSkge1xuICAgIHZhciByZWYgPSB0aGlzLl9fO1xuICAgIHZhciBpc0Fub255bW91cyA9IHJlZi5pc0Fub255bW91cztcblxuICAgIGRlZmluZSh0aGlzLCAnaXNNb3VudGVkJywgdmFsdWUpO1xuXG4gICAgaWYgKCFpc0Fub255bW91cykge1xuICAgICAgaWYgKHZhbHVlKSB7IHRoaXMudHJpZ2dlcignbW91bnQnKTsgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIHRoaXMudHJpZ2dlcigndW5tb3VudCcpO1xuICAgICAgICB0aGlzLm9mZignKicpO1xuICAgICAgICB0aGlzLl9fLndhc0NyZWF0ZWQgPSBmYWxzZTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogTW91bnQgdGhlIGN1cnJlbnQgdGFnIGluc3RhbmNlXG4gICAqIEByZXR1cm5zIHsgVGFnIH0gdGhlIGN1cnJlbnQgdGFnIGluc3RhbmNlXG4gICAqL1xuICBmdW5jdGlvbiBjb21wb25lbnRNb3VudCh0YWckJDEsIGRvbSwgZXhwcmVzc2lvbnMsIG9wdHMpIHtcbiAgICB2YXIgX18gPSB0YWckJDEuX187XG4gICAgdmFyIHJvb3QgPSBfXy5yb290O1xuICAgIHJvb3QuX3RhZyA9IHRhZyQkMTsgLy8ga2VlcCBhIHJlZmVyZW5jZSB0byB0aGUgdGFnIGp1c3QgY3JlYXRlZFxuXG4gICAgLy8gUmVhZCBhbGwgdGhlIGF0dHJzIG9uIHRoaXMgaW5zdGFuY2UuIFRoaXMgZ2l2ZSB1cyB0aGUgaW5mbyB3ZSBuZWVkIGZvciB1cGRhdGVPcHRzXG4gICAgcGFyc2VBdHRyaWJ1dGVzLmFwcGx5KF9fLnBhcmVudCwgW3Jvb3QsIHJvb3QuYXR0cmlidXRlcywgZnVuY3Rpb24gKGF0dHIsIGV4cHIpIHtcbiAgICAgIGlmICghX18uaXNBbm9ueW1vdXMgJiYgUmVmRXhwci5pc1Byb3RvdHlwZU9mKGV4cHIpKSB7IGV4cHIudGFnID0gdGFnJCQxOyB9XG4gICAgICBhdHRyLmV4cHIgPSBleHByO1xuICAgICAgX18uaW5zdEF0dHJzLnB1c2goYXR0cik7XG4gICAgfV0pO1xuXG4gICAgLy8gdXBkYXRlIHRoZSByb290IGFkZGluZyBjdXN0b20gYXR0cmlidXRlcyBjb21pbmcgZnJvbSB0aGUgY29tcGlsZXJcbiAgICB3YWxrQXR0cmlidXRlcyhfXy5pbXBsLmF0dHJzLCBmdW5jdGlvbiAoaywgdikgeyBfXy5pbXBsQXR0cnMucHVzaCh7bmFtZTogaywgdmFsdWU6IHZ9KTsgfSk7XG4gICAgcGFyc2VBdHRyaWJ1dGVzLmFwcGx5KHRhZyQkMSwgW3Jvb3QsIF9fLmltcGxBdHRycywgZnVuY3Rpb24gKGF0dHIsIGV4cHIpIHtcbiAgICAgIGlmIChleHByKSB7IGV4cHJlc3Npb25zLnB1c2goZXhwcik7IH1cbiAgICAgIGVsc2UgeyBzZXRBdHRyaWJ1dGUocm9vdCwgYXR0ci5uYW1lLCBhdHRyLnZhbHVlKTsgfVxuICAgIH1dKTtcblxuICAgIC8vIGluaXRpYWxpYXRpb25cbiAgICB1cGRhdGVPcHRzLmFwcGx5KHRhZyQkMSwgW19fLmlzTG9vcCwgX18ucGFyZW50LCBfXy5pc0Fub255bW91cywgb3B0cywgX18uaW5zdEF0dHJzXSk7XG5cbiAgICAvLyBhZGQgZ2xvYmFsIG1peGluc1xuICAgIHZhciBnbG9iYWxNaXhpbiA9IG1peGluKEdMT0JBTF9NSVhJTik7XG5cbiAgICBpZiAoZ2xvYmFsTWl4aW4gJiYgIV9fLnNraXBBbm9ueW1vdXMpIHtcbiAgICAgIGZvciAodmFyIGkgaW4gZ2xvYmFsTWl4aW4pIHtcbiAgICAgICAgaWYgKGdsb2JhbE1peGluLmhhc093blByb3BlcnR5KGkpKSB7XG4gICAgICAgICAgdGFnJCQxLm1peGluKGdsb2JhbE1peGluW2ldKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChfXy5pbXBsLmZuKSB7IF9fLmltcGwuZm4uY2FsbCh0YWckJDEsIG9wdHMpOyB9XG5cbiAgICBpZiAoIV9fLnNraXBBbm9ueW1vdXMpIHsgdGFnJCQxLnRyaWdnZXIoJ2JlZm9yZS1tb3VudCcpOyB9XG5cbiAgICAvLyBwYXJzZSBsYXlvdXQgYWZ0ZXIgaW5pdC4gZm4gbWF5IGNhbGN1bGF0ZSBhcmdzIGZvciBuZXN0ZWQgY3VzdG9tIHRhZ3NcbiAgICBlYWNoKHBhcnNlRXhwcmVzc2lvbnMuYXBwbHkodGFnJCQxLCBbZG9tLCBfXy5pc0Fub255bW91c10pLCBmdW5jdGlvbiAoZSkgeyByZXR1cm4gZXhwcmVzc2lvbnMucHVzaChlKTsgfSk7XG5cbiAgICB0YWckJDEudXBkYXRlKF9fLml0ZW0pO1xuXG4gICAgaWYgKCFfXy5pc0Fub255bW91cyAmJiAhX18uaXNJbmxpbmUpIHtcbiAgICAgIHdoaWxlIChkb20uZmlyc3RDaGlsZCkgeyByb290LmFwcGVuZENoaWxkKGRvbS5maXJzdENoaWxkKTsgfVxuICAgIH1cblxuICAgIGRlZmluZSh0YWckJDEsICdyb290Jywgcm9vdCk7XG5cbiAgICAvLyBpZiB3ZSBuZWVkIHRvIHdhaXQgdGhhdCB0aGUgcGFyZW50IFwibW91bnRcIiBvciBcInVwZGF0ZWRcIiBldmVudCBnZXRzIHRyaWdnZXJlZFxuICAgIGlmICghX18uc2tpcEFub255bW91cyAmJiB0YWckJDEucGFyZW50KSB7XG4gICAgICB2YXIgcCA9IGdldEltbWVkaWF0ZUN1c3RvbVBhcmVudCh0YWckJDEucGFyZW50KTtcbiAgICAgIHAub25lKCFwLmlzTW91bnRlZCA/ICdtb3VudCcgOiAndXBkYXRlZCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgc2V0TW91bnRTdGF0ZS5jYWxsKHRhZyQkMSwgdHJ1ZSk7XG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gb3RoZXJ3aXNlIGl0J3Mgbm90IGEgY2hpbGQgdGFnIHdlIGNhbiB0cmlnZ2VyIGl0cyBtb3VudCBldmVudFxuICAgICAgc2V0TW91bnRTdGF0ZS5jYWxsKHRhZyQkMSwgdHJ1ZSk7XG4gICAgfVxuXG4gICAgdGFnJCQxLl9fLndhc0NyZWF0ZWQgPSB0cnVlO1xuXG4gICAgcmV0dXJuIHRhZyQkMVxuICB9XG5cbiAgLyoqXG4gICAqIFVubW91bnQgdGhlIHRhZyBpbnN0YW5jZVxuICAgKiBAcGFyYW0geyBCb29sZWFuIH0gbXVzdEtlZXBSb290IC0gaWYgaXQncyB0cnVlIHRoZSByb290IG5vZGUgd2lsbCBub3QgYmUgcmVtb3ZlZFxuICAgKiBAcmV0dXJucyB7IFRhZyB9IHRoZSBjdXJyZW50IHRhZyBpbnN0YW5jZVxuICAgKi9cbiAgZnVuY3Rpb24gdGFnVW5tb3VudCh0YWcsIG11c3RLZWVwUm9vdCwgZXhwcmVzc2lvbnMpIHtcbiAgICB2YXIgX18gPSB0YWcuX187XG4gICAgdmFyIHJvb3QgPSBfXy5yb290O1xuICAgIHZhciB0YWdJbmRleCA9IF9fVEFHU19DQUNIRS5pbmRleE9mKHRhZyk7XG4gICAgdmFyIHAgPSByb290LnBhcmVudE5vZGU7XG5cbiAgICBpZiAoIV9fLnNraXBBbm9ueW1vdXMpIHsgdGFnLnRyaWdnZXIoJ2JlZm9yZS11bm1vdW50Jyk7IH1cblxuICAgIC8vIGNsZWFyIGFsbCBhdHRyaWJ1dGVzIGNvbWluZyBmcm9tIHRoZSBtb3VudGVkIHRhZ1xuICAgIHdhbGtBdHRyaWJ1dGVzKF9fLmltcGwuYXR0cnMsIGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgICBpZiAoc3RhcnRzV2l0aChuYW1lLCBBVFRSU19QUkVGSVgpKVxuICAgICAgICB7IG5hbWUgPSBuYW1lLnNsaWNlKEFUVFJTX1BSRUZJWC5sZW5ndGgpOyB9XG5cbiAgICAgIHJlbW92ZUF0dHJpYnV0ZShyb290LCBuYW1lKTtcbiAgICB9KTtcblxuICAgIC8vIHJlbW92ZSBhbGwgdGhlIGV2ZW50IGxpc3RlbmVyc1xuICAgIHRhZy5fXy5saXN0ZW5lcnMuZm9yRWFjaChmdW5jdGlvbiAoZG9tKSB7XG4gICAgICBPYmplY3Qua2V5cyhkb21bUklPVF9FVkVOVFNfS0VZXSkuZm9yRWFjaChmdW5jdGlvbiAoZXZlbnROYW1lKSB7XG4gICAgICAgIGRvbS5yZW1vdmVFdmVudExpc3RlbmVyKGV2ZW50TmFtZSwgZG9tW1JJT1RfRVZFTlRTX0tFWV1bZXZlbnROYW1lXSk7XG4gICAgICB9KTtcbiAgICB9KTtcblxuICAgIC8vIHJlbW92ZSB0YWcgaW5zdGFuY2UgZnJvbSB0aGUgZ2xvYmFsIHRhZ3MgY2FjaGUgY29sbGVjdGlvblxuICAgIGlmICh0YWdJbmRleCAhPT0gLTEpIHsgX19UQUdTX0NBQ0hFLnNwbGljZSh0YWdJbmRleCwgMSk7IH1cblxuICAgIC8vIGNsZWFuIHVwIHRoZSBwYXJlbnQgdGFncyBvYmplY3RcbiAgICBpZiAoX18ucGFyZW50ICYmICFfXy5pc0Fub255bW91cykge1xuICAgICAgdmFyIHB0YWcgPSBnZXRJbW1lZGlhdGVDdXN0b21QYXJlbnQoX18ucGFyZW50KTtcblxuICAgICAgaWYgKF9fLmlzVmlydHVhbCkge1xuICAgICAgICBPYmplY3RcbiAgICAgICAgICAua2V5cyh0YWcudGFncylcbiAgICAgICAgICAuZm9yRWFjaChmdW5jdGlvbiAodGFnTmFtZSkgeyByZXR1cm4gYXJyYXlpc2hSZW1vdmUocHRhZy50YWdzLCB0YWdOYW1lLCB0YWcudGFnc1t0YWdOYW1lXSk7IH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgYXJyYXlpc2hSZW1vdmUocHRhZy50YWdzLCBfXy50YWdOYW1lLCB0YWcpO1xuICAgICAgfVxuICAgIH1cblxuICAgIC8vIHVubW91bnQgYWxsIHRoZSB2aXJ0dWFsIGRpcmVjdGl2ZXNcbiAgICBpZiAodGFnLl9fLnZpcnRzKSB7XG4gICAgICBlYWNoKHRhZy5fXy52aXJ0cywgZnVuY3Rpb24gKHYpIHtcbiAgICAgICAgaWYgKHYucGFyZW50Tm9kZSkgeyB2LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodik7IH1cbiAgICAgIH0pO1xuICAgIH1cblxuICAgIC8vIGFsbG93IGV4cHJlc3Npb25zIHRvIHVubW91bnQgdGhlbXNlbHZlc1xuICAgIHVubW91bnRBbGwoZXhwcmVzc2lvbnMpO1xuICAgIGVhY2goX18uaW5zdEF0dHJzLCBmdW5jdGlvbiAoYSkgeyByZXR1cm4gYS5leHByICYmIGEuZXhwci51bm1vdW50ICYmIGEuZXhwci51bm1vdW50KCk7IH0pO1xuXG4gICAgLy8gY2xlYXIgdGhlIHRhZyBodG1sIGlmIGl0J3MgbmVjZXNzYXJ5XG4gICAgaWYgKG11c3RLZWVwUm9vdCkgeyBzZXRJbm5lckhUTUwocm9vdCwgJycpOyB9XG4gICAgLy8gb3RoZXJ3aXNlIGRldGFjaCB0aGUgcm9vdCB0YWcgZnJvbSB0aGUgRE9NXG4gICAgZWxzZSBpZiAocCkgeyBwLnJlbW92ZUNoaWxkKHJvb3QpOyB9XG5cbiAgICAvLyBjdXN0b20gaW50ZXJuYWwgdW5tb3VudCBmdW5jdGlvbiB0byBhdm9pZCByZWx5aW5nIG9uIHRoZSBvYnNlcnZhYmxlXG4gICAgaWYgKF9fLm9uVW5tb3VudCkgeyBfXy5vblVubW91bnQoKTsgfVxuXG4gICAgLy8gd2VpcmQgZml4IGZvciBhIHdlaXJkIGVkZ2UgY2FzZSAjMjQwOSBhbmQgIzI0MzZcbiAgICAvLyBzb21lIHVzZXJzIG1pZ2h0IHVzZSB5b3VyIHNvZnR3YXJlIG5vdCBhcyB5b3UndmUgZXhwZWN0ZWRcbiAgICAvLyBzbyBJIG5lZWQgdG8gYWRkIHRoZXNlIGRpcnR5IGhhY2tzIHRvIG1pdGlnYXRlIHVuZXhwZWN0ZWQgaXNzdWVzXG4gICAgaWYgKCF0YWcuaXNNb3VudGVkKSB7IHNldE1vdW50U3RhdGUuY2FsbCh0YWcsIHRydWUpOyB9XG5cbiAgICBzZXRNb3VudFN0YXRlLmNhbGwodGFnLCBmYWxzZSk7XG5cbiAgICBkZWxldGUgcm9vdC5fdGFnO1xuXG4gICAgcmV0dXJuIHRhZ1xuICB9XG5cbiAgLyoqXG4gICAqIFRhZyBjcmVhdGlvbiBmYWN0b3J5IGZ1bmN0aW9uXG4gICAqIEBjb25zdHJ1Y3RvclxuICAgKiBAcGFyYW0geyBPYmplY3QgfSBpbXBsIC0gaXQgY29udGFpbnMgdGhlIHRhZyB0ZW1wbGF0ZSwgYW5kIGxvZ2ljXG4gICAqIEBwYXJhbSB7IE9iamVjdCB9IGNvbmYgLSB0YWcgb3B0aW9uc1xuICAgKiBAcGFyYW0geyBTdHJpbmcgfSBpbm5lckhUTUwgLSBodG1sIHRoYXQgZXZlbnR1YWxseSB3ZSBuZWVkIHRvIGluamVjdCBpbiB0aGUgdGFnXG4gICAqL1xuICBmdW5jdGlvbiBjcmVhdGVUYWcoaW1wbCwgY29uZiwgaW5uZXJIVE1MKSB7XG4gICAgaWYgKCBpbXBsID09PSB2b2lkIDAgKSBpbXBsID0ge307XG4gICAgaWYgKCBjb25mID09PSB2b2lkIDAgKSBjb25mID0ge307XG5cbiAgICB2YXIgdGFnID0gY29uZi5jb250ZXh0IHx8IHt9O1xuICAgIHZhciBvcHRzID0gY29uZi5vcHRzIHx8IHt9O1xuICAgIHZhciBwYXJlbnQgPSBjb25mLnBhcmVudDtcbiAgICB2YXIgaXNMb29wID0gY29uZi5pc0xvb3A7XG4gICAgdmFyIGlzQW5vbnltb3VzID0gISFjb25mLmlzQW5vbnltb3VzO1xuICAgIHZhciBza2lwQW5vbnltb3VzID0gc2V0dGluZ3Muc2tpcEFub255bW91c1RhZ3MgJiYgaXNBbm9ueW1vdXM7XG4gICAgdmFyIGl0ZW0gPSBjb25mLml0ZW07XG4gICAgLy8gYXZhaWxhYmxlIG9ubHkgZm9yIHRoZSBsb29wZWQgbm9kZXNcbiAgICB2YXIgaW5kZXggPSBjb25mLmluZGV4O1xuICAgIC8vIEFsbCBhdHRyaWJ1dGVzIG9uIHRoZSBUYWcgd2hlbiBpdCdzIGZpcnN0IHBhcnNlZFxuICAgIHZhciBpbnN0QXR0cnMgPSBbXTtcbiAgICAvLyBleHByZXNzaW9ucyBvbiB0aGlzIHR5cGUgb2YgVGFnXG4gICAgdmFyIGltcGxBdHRycyA9IFtdO1xuICAgIHZhciB0bXBsID0gaW1wbC50bXBsO1xuICAgIHZhciBleHByZXNzaW9ucyA9IFtdO1xuICAgIHZhciByb290ID0gY29uZi5yb290O1xuICAgIHZhciB0YWdOYW1lID0gY29uZi50YWdOYW1lIHx8IGdldE5hbWUocm9vdCk7XG4gICAgdmFyIGlzVmlydHVhbCA9IHRhZ05hbWUgPT09ICd2aXJ0dWFsJztcbiAgICB2YXIgaXNJbmxpbmUgPSAhaXNWaXJ0dWFsICYmICF0bXBsO1xuICAgIHZhciBkb207XG5cbiAgICBpZiAoaXNJbmxpbmUgfHwgaXNMb29wICYmIGlzQW5vbnltb3VzKSB7XG4gICAgICBkb20gPSByb290O1xuICAgIH0gZWxzZSB7XG4gICAgICBpZiAoIWlzVmlydHVhbCkgeyByb290LmlubmVySFRNTCA9ICcnOyB9XG4gICAgICBkb20gPSBta2RvbSh0bXBsLCBpbm5lckhUTUwsIGlzU3ZnKHJvb3QpKTtcbiAgICB9XG5cbiAgICAvLyBtYWtlIHRoaXMgdGFnIG9ic2VydmFibGVcbiAgICBpZiAoIXNraXBBbm9ueW1vdXMpIHsgb2JzZXJ2YWJsZSh0YWcpOyB9XG5cbiAgICAvLyBvbmx5IGNhbGwgdW5tb3VudCBpZiB3ZSBoYXZlIGEgdmFsaWQgX19UQUdfSU1QTCAoaGFzIG5hbWUgcHJvcGVydHkpXG4gICAgaWYgKGltcGwubmFtZSAmJiByb290Ll90YWcpIHsgcm9vdC5fdGFnLnVubW91bnQodHJ1ZSk7IH1cblxuICAgIGRlZmluZSh0YWcsICdfXycsIHtcbiAgICAgIGltcGw6IGltcGwsXG4gICAgICByb290OiByb290LFxuICAgICAgc2tpcEFub255bW91czogc2tpcEFub255bW91cyxcbiAgICAgIGltcGxBdHRyczogaW1wbEF0dHJzLFxuICAgICAgaXNBbm9ueW1vdXM6IGlzQW5vbnltb3VzLFxuICAgICAgaW5zdEF0dHJzOiBpbnN0QXR0cnMsXG4gICAgICBpbm5lckhUTUw6IGlubmVySFRNTCxcbiAgICAgIHRhZ05hbWU6IHRhZ05hbWUsXG4gICAgICBpbmRleDogaW5kZXgsXG4gICAgICBpc0xvb3A6IGlzTG9vcCxcbiAgICAgIGlzSW5saW5lOiBpc0lubGluZSxcbiAgICAgIGl0ZW06IGl0ZW0sXG4gICAgICBwYXJlbnQ6IHBhcmVudCxcbiAgICAgIC8vIHRhZ3MgaGF2aW5nIGV2ZW50IGxpc3RlbmVyc1xuICAgICAgLy8gaXQgd291bGQgYmUgYmV0dGVyIHRvIHVzZSB3ZWFrIG1hcHMgaGVyZSBidXQgd2UgY2FuIG5vdCBpbnRyb2R1Y2UgYnJlYWtpbmcgY2hhbmdlcyBub3dcbiAgICAgIGxpc3RlbmVyczogW10sXG4gICAgICAvLyB0aGVzZSB2YXJzIHdpbGwgYmUgbmVlZGVkIG9ubHkgZm9yIHRoZSB2aXJ0dWFsIHRhZ3NcbiAgICAgIHZpcnRzOiBbXSxcbiAgICAgIHdhc0NyZWF0ZWQ6IGZhbHNlLFxuICAgICAgdGFpbDogbnVsbCxcbiAgICAgIGhlYWQ6IG51bGxcbiAgICB9KTtcblxuICAgIC8vIHRhZyBwcm90ZWN0ZWQgcHJvcGVydGllc1xuICAgIHJldHVybiBbXG4gICAgICBbJ2lzTW91bnRlZCcsIGZhbHNlXSxcbiAgICAgIC8vIGNyZWF0ZSBhIHVuaXF1ZSBpZCB0byB0aGlzIHRhZ1xuICAgICAgLy8gaXQgY291bGQgYmUgaGFuZHkgdG8gdXNlIGl0IGFsc28gdG8gaW1wcm92ZSB0aGUgdmlydHVhbCBkb20gcmVuZGVyaW5nIHNwZWVkXG4gICAgICBbJ19yaW90X2lkJywgdWlkKCldLFxuICAgICAgWydyb290Jywgcm9vdF0sXG4gICAgICBbJ29wdHMnLCBvcHRzLCB7IHdyaXRhYmxlOiB0cnVlLCBlbnVtZXJhYmxlOiB0cnVlIH1dLFxuICAgICAgWydwYXJlbnQnLCBwYXJlbnQgfHwgbnVsbF0sXG4gICAgICAvLyBwcm90ZWN0IHRoZSBcInRhZ3NcIiBhbmQgXCJyZWZzXCIgcHJvcGVydHkgZnJvbSBiZWluZyBvdmVycmlkZGVuXG4gICAgICBbJ3RhZ3MnLCB7fV0sXG4gICAgICBbJ3JlZnMnLCB7fV0sXG4gICAgICBbJ3VwZGF0ZScsIGZ1bmN0aW9uIChkYXRhKSB7IHJldHVybiBjb21wb25lbnRVcGRhdGUodGFnLCBkYXRhLCBleHByZXNzaW9ucyk7IH1dLFxuICAgICAgWydtaXhpbicsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIG1peGlucyA9IFtdLCBsZW4gPSBhcmd1bWVudHMubGVuZ3RoO1xuICAgICAgICB3aGlsZSAoIGxlbi0tICkgbWl4aW5zWyBsZW4gXSA9IGFyZ3VtZW50c1sgbGVuIF07XG5cbiAgICAgICAgcmV0dXJuIGNvbXBvbmVudE1peGluLmFwcGx5KHZvaWQgMCwgWyB0YWcgXS5jb25jYXQoIG1peGlucyApKTtcbiAgICB9XSxcbiAgICAgIFsnbW91bnQnLCBmdW5jdGlvbiAoKSB7IHJldHVybiBjb21wb25lbnRNb3VudCh0YWcsIGRvbSwgZXhwcmVzc2lvbnMsIG9wdHMpOyB9XSxcbiAgICAgIFsndW5tb3VudCcsIGZ1bmN0aW9uIChtdXN0S2VlcFJvb3QpIHsgcmV0dXJuIHRhZ1VubW91bnQodGFnLCBtdXN0S2VlcFJvb3QsIGV4cHJlc3Npb25zKTsgfV1cbiAgICBdLnJlZHVjZShmdW5jdGlvbiAoYWNjLCByZWYpIHtcbiAgICAgIHZhciBrZXkgPSByZWZbMF07XG4gICAgICB2YXIgdmFsdWUgPSByZWZbMV07XG4gICAgICB2YXIgb3B0cyA9IHJlZlsyXTtcblxuICAgICAgZGVmaW5lKHRhZywga2V5LCB2YWx1ZSwgb3B0cyk7XG4gICAgICByZXR1cm4gYWNjXG4gICAgfSwgZXh0ZW5kKHRhZywgaXRlbSkpXG4gIH1cblxuICAvKipcbiAgICogTW91bnQgYSB0YWcgY3JlYXRpbmcgbmV3IFRhZyBpbnN0YW5jZVxuICAgKiBAcGFyYW0gICB7IE9iamVjdCB9IHJvb3QgLSBkb20gbm9kZSB3aGVyZSB0aGUgdGFnIHdpbGwgYmUgbW91bnRlZFxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IHRhZ05hbWUgLSBuYW1lIG9mIHRoZSByaW90IHRhZyB3ZSB3YW50IHRvIG1vdW50XG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gb3B0cyAtIG9wdGlvbnMgdG8gcGFzcyB0byB0aGUgVGFnIGluc3RhbmNlXG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gY3R4IC0gb3B0aW9uYWwgY29udGV4dCB0aGF0IHdpbGwgYmUgdXNlZCB0byBleHRlbmQgYW4gZXhpc3RpbmcgY2xhc3MgKCB1c2VkIGluIHJpb3QuVGFnIClcbiAgICogQHJldHVybnMgeyBUYWcgfSBhIG5ldyBUYWcgaW5zdGFuY2VcbiAgICovXG4gIGZ1bmN0aW9uIG1vdW50JDEocm9vdCwgdGFnTmFtZSwgb3B0cywgY3R4KSB7XG4gICAgdmFyIGltcGwgPSBfX1RBR19JTVBMW3RhZ05hbWVdO1xuICAgIHZhciBpbXBsQ2xhc3MgPSBfX1RBR19JTVBMW3RhZ05hbWVdLmNsYXNzO1xuICAgIHZhciBjb250ZXh0ID0gY3R4IHx8IChpbXBsQ2xhc3MgPyBjcmVhdGUoaW1wbENsYXNzLnByb3RvdHlwZSkgOiB7fSk7XG4gICAgLy8gY2FjaGUgdGhlIGlubmVyIEhUTUwgdG8gZml4ICM4NTVcbiAgICB2YXIgaW5uZXJIVE1MID0gcm9vdC5faW5uZXJIVE1MID0gcm9vdC5faW5uZXJIVE1MIHx8IHJvb3QuaW5uZXJIVE1MO1xuICAgIHZhciBjb25mID0gZXh0ZW5kKHsgcm9vdDogcm9vdCwgb3B0czogb3B0cywgY29udGV4dDogY29udGV4dCB9LCB7IHBhcmVudDogb3B0cyA/IG9wdHMucGFyZW50IDogbnVsbCB9KTtcbiAgICB2YXIgdGFnO1xuXG4gICAgaWYgKGltcGwgJiYgcm9vdCkgeyB0YWcgPSBjcmVhdGVUYWcoaW1wbCwgY29uZiwgaW5uZXJIVE1MKTsgfVxuXG4gICAgaWYgKHRhZyAmJiB0YWcubW91bnQpIHtcbiAgICAgIHRhZy5tb3VudCh0cnVlKTtcbiAgICAgIC8vIGFkZCB0aGlzIHRhZyB0byB0aGUgdmlydHVhbERvbSB2YXJpYWJsZVxuICAgICAgaWYgKCFjb250YWlucyhfX1RBR1NfQ0FDSEUsIHRhZykpIHsgX19UQUdTX0NBQ0hFLnB1c2godGFnKTsgfVxuICAgIH1cblxuICAgIHJldHVybiB0YWdcbiAgfVxuXG5cblxuICB2YXIgdGFncyA9IC8qI19fUFVSRV9fKi9PYmplY3QuZnJlZXplKHtcbiAgICBhcnJheWlzaEFkZDogYXJyYXlpc2hBZGQsXG4gICAgZ2V0VGFnTmFtZTogZ2V0TmFtZSxcbiAgICBpbmhlcml0UGFyZW50UHJvcHM6IGluaGVyaXRQYXJlbnRQcm9wcyxcbiAgICBtb3VudFRvOiBtb3VudCQxLFxuICAgIHNlbGVjdFRhZ3M6IHF1ZXJ5LFxuICAgIGFycmF5aXNoUmVtb3ZlOiBhcnJheWlzaFJlbW92ZSxcbiAgICBnZXRUYWc6IGdldCxcbiAgICBpbml0Q2hpbGRUYWc6IGluaXRDaGlsZCxcbiAgICBtb3ZlQ2hpbGRUYWc6IG1vdmVDaGlsZCxcbiAgICBtYWtlUmVwbGFjZVZpcnR1YWw6IG1ha2VSZXBsYWNlVmlydHVhbCxcbiAgICBnZXRJbW1lZGlhdGVDdXN0b21QYXJlbnRUYWc6IGdldEltbWVkaWF0ZUN1c3RvbVBhcmVudCxcbiAgICBtYWtlVmlydHVhbDogbWFrZVZpcnR1YWwsXG4gICAgbW92ZVZpcnR1YWw6IG1vdmVWaXJ0dWFsLFxuICAgIHVubW91bnRBbGw6IHVubW91bnRBbGwsXG4gICAgY3JlYXRlSWZEaXJlY3RpdmU6IGNyZWF0ZUlmRGlyZWN0aXZlLFxuICAgIGNyZWF0ZVJlZkRpcmVjdGl2ZTogY3JlYXRlUmVmRGlyZWN0aXZlXG4gIH0pO1xuXG4gIC8qKlxuICAgKiBSaW90IHB1YmxpYyBhcGlcbiAgICovXG4gIHZhciBzZXR0aW5ncyQxID0gc2V0dGluZ3M7XG4gIHZhciB1dGlsID0ge1xuICAgIHRtcGw6IHRtcGwsXG4gICAgYnJhY2tldHM6IGJyYWNrZXRzLFxuICAgIHN0eWxlTWFuYWdlcjogc3R5bGVNYW5hZ2VyLFxuICAgIHZkb206IF9fVEFHU19DQUNIRSxcbiAgICBzdHlsZU5vZGU6IHN0eWxlTWFuYWdlci5zdHlsZU5vZGUsXG4gICAgLy8gZXhwb3J0IHRoZSByaW90IGludGVybmFsIHV0aWxzIGFzIHdlbGxcbiAgICBkb206IGRvbSxcbiAgICBjaGVjazogY2hlY2ssXG4gICAgbWlzYzogbWlzYyxcbiAgICB0YWdzOiB0YWdzXG4gIH07XG5cbiAgLy8gZXhwb3J0IHRoZSBjb3JlIHByb3BzL21ldGhvZHNcbiAgdmFyIFRhZyQxID0gVGFnO1xuICB2YXIgdGFnJDEgPSB0YWc7XG4gIHZhciB0YWcyJDEgPSB0YWcyO1xuICB2YXIgbW91bnQkMiA9IG1vdW50O1xuICB2YXIgbWl4aW4kMSA9IG1peGluO1xuICB2YXIgdXBkYXRlJDIgPSB1cGRhdGUkMTtcbiAgdmFyIHVucmVnaXN0ZXIkMSA9IHVucmVnaXN0ZXI7XG4gIHZhciB2ZXJzaW9uJDEgPSB2ZXJzaW9uO1xuICB2YXIgb2JzZXJ2YWJsZSQxID0gb2JzZXJ2YWJsZTtcblxuICB2YXIgcmlvdCQxID0gZXh0ZW5kKHt9LCBjb3JlLCB7XG4gICAgb2JzZXJ2YWJsZTogb2JzZXJ2YWJsZSxcbiAgICBzZXR0aW5nczogc2V0dGluZ3MkMSxcbiAgICB1dGlsOiB1dGlsLFxuICB9KVxuXG4gIGV4cG9ydHMuc2V0dGluZ3MgPSBzZXR0aW5ncyQxO1xuICBleHBvcnRzLnV0aWwgPSB1dGlsO1xuICBleHBvcnRzLlRhZyA9IFRhZyQxO1xuICBleHBvcnRzLnRhZyA9IHRhZyQxO1xuICBleHBvcnRzLnRhZzIgPSB0YWcyJDE7XG4gIGV4cG9ydHMubW91bnQgPSBtb3VudCQyO1xuICBleHBvcnRzLm1peGluID0gbWl4aW4kMTtcbiAgZXhwb3J0cy51cGRhdGUgPSB1cGRhdGUkMjtcbiAgZXhwb3J0cy51bnJlZ2lzdGVyID0gdW5yZWdpc3RlciQxO1xuICBleHBvcnRzLnZlcnNpb24gPSB2ZXJzaW9uJDE7XG4gIGV4cG9ydHMub2JzZXJ2YWJsZSA9IG9ic2VydmFibGUkMTtcbiAgZXhwb3J0cy5kZWZhdWx0ID0gcmlvdCQxO1xuXG4gIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG5cbn0pKSk7XG4iLCJnbG9iYWwuYmFzZXVybCA9ICdodHRwczovL3d3dy5hYnJhcmJvb2suY29tJztcclxuZ2xvYmFsLmJhc2VpbWd1cmwgPSAnaHR0cHM6Ly93d3cuYWJyYXJib29rLmNvbS8nO1xyXG4kLmFqYXhTZXR1cCh7XHJcbiAgICB1cmw6IGJhc2V1cmwsXHJcbiAgICB0eXBlOiAnUE9TVCcsXHJcbiAgICBjcm9zc0RvbWFpbjogdHJ1ZSxcclxuICAgIGRhdGFUeXBlOiAnanNvbicsXHJcbiAgICBjYWNoZTogdHJ1ZSxcclxuICAgIGdsb2JhbDogZmFsc2UsXHJcbiAgICBiZWZvcmVTZW5kOiBmdW5jdGlvbiAoeGhyLCBzZXR0aW5ncykge1xyXG4gICAgICAgIHNldHRpbmdzLnVybCA9IGJhc2V1cmwgKyBzZXR0aW5ncy51cmw7XHJcbiAgICAgICAgaWYgKCFzZXR0aW5ncy5ub2xvYWRlcikge1xyXG4gICAgICAgICAgICBOUHJvZ3Jlc3Muc3RhcnQoKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgY29tcGxldGU6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBOUHJvZ3Jlc3MuZG9uZSgpO1xyXG4gICAgfVxyXG59KTtcclxuXHJcbiQuYWpheHN5cyA9IGZ1bmN0aW9uIChwYXJtKSB7XHJcbiAgICB2YXIgdSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdsb2dpbicpO1xyXG4gICAgdSA9IHUgPyBKU09OLnBhcnNlKHUpIDoge307XHJcbiAgICBpZiAodSkgcGFybS5kYXRhLmF1dGhrZXkgPSB1LmF1dGhrZXk7XHJcbiAgICBwYXJtLnR5cGUgPSBcIlBPU1RcIjtcclxuICAgIHBhcm0uY2FjaGUgPSBmYWxzZTtcclxuICAgICQuYWpheChwYXJtKTtcclxufVxyXG5cclxuJC52YWxpZGF0b3Iuc2V0RGVmYXVsdHMoe1xyXG4gICAgZXJyb3JFbGVtZW50OiBcImVtXCIsXHJcbiAgICBlcnJvclBsYWNlbWVudDogZnVuY3Rpb24gKGVycm9yLCBlbGVtZW50KSB7XHJcbiAgICAgICAgZXJyb3IuYWRkQ2xhc3MoXCJoZWxwLWJsb2NrXCIpLmNzcyh7XHJcbiAgICAgICAgICAgICdtYXJnaW4nOiAwXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgaWYgKGVsZW1lbnQucHJvcChcInR5cGVcIikgPT09IFwiY2hlY2tib3hcIikge1xyXG4gICAgICAgICAgICBlcnJvci5pbnNlcnRBZnRlcihlbGVtZW50LnBhcmVudChcImxhYmVsXCIpKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBlbGVtZW50LnBhcmVudHMoXCJbY2xhc3NePSdmb3JtLWdyb3VwJ106Zmlyc3RcIikuZmluZCgnbGFiZWwnKS5hcHBlbmQoZXJyb3IpXHJcblxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBoaWdobGlnaHQ6IGZ1bmN0aW9uIChlbGVtZW50LCBlcnJvckNsYXNzLCB2YWxpZENsYXNzKSB7XHJcbiAgICAgICAgJChlbGVtZW50KS5wYXJlbnRzKFwiW2NsYXNzXj0nZm9ybS1ncm91cCddOmZpcnN0XCIpLmFkZENsYXNzKFwiaGFzLWVycm9yXCIpLnJlbW92ZUNsYXNzKFwiaGFzLXN1Y2Nlc3NcIik7XHJcbiAgICB9LFxyXG4gICAgdW5oaWdobGlnaHQ6IGZ1bmN0aW9uIChlbGVtZW50LCBlcnJvckNsYXNzLCB2YWxpZENsYXNzKSB7XHJcbiAgICAgICAgJChlbGVtZW50KS5wYXJlbnRzKFwiW2NsYXNzXj0nZm9ybS1ncm91cCddOmZpcnN0XCIpLmFkZENsYXNzKFwiaGFzLXN1Y2Nlc3NcIikucmVtb3ZlQ2xhc3MoXCJoYXMtZXJyb3JcIik7XHJcbiAgICB9LFxyXG4gICAgaW52YWxpZEhhbmRsZXI6IGZ1bmN0aW9uIChldmVudCwgdmFsaWRhdG9yKSB7XHJcbiAgICAgICAgdmFyIGVycm9ycyA9IHZhbGlkYXRvci5udW1iZXJPZkludmFsaWRzKCk7XHJcbiAgICAgICAgJC50b2FzdCh7XHJcbiAgICAgICAgICAgIHRleHQ6IFwi2YrZiNis2K8gXCIgKyBlcnJvcnMgKyBcIiDYrdmC2YjZhCDYqNmH2Kcg2KfYrti32KfYoSDYqNix2KzYp9ihINmF2LHYrNi52KrZh9inLlwiLFxyXG4gICAgICAgICAgICBpY29uOiAnZXJyb3InXHJcbiAgICAgICAgfSlcclxuICAgIH1cclxufSk7XHJcblxyXG4vLyAgJC52YWxpZGF0b3IuYWRkTWV0aG9kKFwicmVnZXhcIixcclxuLy8gICAgICBmdW5jdGlvbiAodmFsdWUsIGVsZW1lbnQsIHJlZ2V4cCkge1xyXG4vLyAgICAgICAgICB2YXIgcmUgPSBuZXcgUmVnRXhwKHJlZ2V4cCk7XHJcbi8vICAgICAgICAgIHJldHVybiB0aGlzLm9wdGlvbmFsKGVsZW1lbnQpIHx8IHJlLnRlc3QodmFsdWUpO1xyXG4vLyAgICAgIH0sIFwi2YLZitmF2Kkg2KfZhNit2YLZhCDYutmK2LEg2LXYrdmK2K3YqVwiKTtcclxuXHJcblxyXG4vLyAgJC52YWxpZGF0b3IuYWRkTWV0aG9kKFxyXG4vLyAgICAgIFwiaW50bHRlbGlucHV0XCIsXHJcbi8vICAgICAgZnVuY3Rpb24gKHZhbHVlLCBlbGVtZW50LCB2YWwpIHtcclxuLy8gICAgICAgICAgcmV0dXJuIHRoaXMub3B0aW9uYWwoZWxlbWVudCkgfHwgJChlbGVtZW50KS5pbnRsVGVsSW5wdXQoJ2lzVmFsaWROdW1iZXInKTtcclxuLy8gICAgICB9LCBcItix2YLZhSDYp9mE2YfYp9iq2YEg2LrZitixINi12K3ZititXCIpO1xyXG5cclxuJC5leHRlbmQodHJ1ZSwgJC5mbi5kYXRhVGFibGUuZGVmYXVsdHMsIHtcclxuXHJcbiAgICAvLyAgcm93UmVvcmRlcjoge1xyXG4gICAgLy8gICAgICBzZWxlY3RvcjogJzpub3QoLmNvbnRyb2wpJ1xyXG4gICAgLy8gIH0sXHJcbiAgICAvLyBkb206IFwiPCdyb3cnPCdjb2wtc20tNidmPjwnY29sLXNtLTYnbD4+XCIgK1xyXG4gICAgLy8gICAgIFwiPCdyb3cnPCdjb2wtc20tMTIndHI+PlwiICtcclxuICAgIC8vICAgICBcIjwncm93JzwnY29sLXNtLTUncD48J2NvbC1zbS03J2k+PlwiLFxyXG4gICAgXCJsYW5ndWFnZVwiOiB7XHJcbiAgICAgICAgXCJzRW1wdHlUYWJsZVwiOiBcItmE2Kcg2YrZiNis2K8g2KjZitin2YbYp9iqXCIsXHJcbiAgICAgICAgXCJzUHJvY2Vzc2luZ1wiOiBcItis2KfYsdmNINin2YTYqtit2YXZitmELi4uXCIsXHJcbiAgICAgICAgXCJzTGVuZ3RoTWVudVwiOiBcItij2LjZh9ixIF9NRU5VXyDZhdiv2K7ZhNin2KpcIixcclxuICAgICAgICBcInNaZXJvUmVjb3Jkc1wiOiBcItmE2YUg2YrYudir2LEg2LnZhNmJINij2YrYqSDYs9is2YTYp9iqXCIsXHJcbiAgICAgICAgXCJzSW5mb1wiOiBcItil2LjZh9in2LEgX1NUQVJUXyDYpdmE2YkgX0VORF8g2YXZhiDYo9i12YQgX1RPVEFMXyDZhdiv2K7ZhFwiLFxyXG4gICAgICAgIFwic0luZm9FbXB0eVwiOiBcItmK2LnYsdi2IDAg2KXZhNmJIDAg2YXZhiDYo9i12YQgMCDYs9is2YRcIixcclxuICAgICAgICBcInNJbmZvRmlsdGVyZWRcIjogXCIo2YXZhtiq2YLYp9ipINmF2YYg2YXYrNmF2YjYuSBfTUFYXyDZhdmP2K/YrtmEKVwiLFxyXG4gICAgICAgIFwic0luZm9Qb3N0Rml4XCI6IFwiXCIsXHJcbiAgICAgICAgXCJzU2VhcmNoXCI6IFwiXCIsXHJcbiAgICAgICAgXCJzVXJsXCI6IFwiXCIsXHJcbiAgICAgICAgXCJvUGFnaW5hdGVcIjoge1xyXG4gICAgICAgICAgICBcInNGaXJzdFwiOiBcItin2YTYo9mI2YRcIixcclxuICAgICAgICAgICAgXCJzUHJldmlvdXNcIjogXCLYp9mE2LPYp9io2YJcIixcclxuICAgICAgICAgICAgXCJzTmV4dFwiOiBcItin2YTYqtin2YTZilwiLFxyXG4gICAgICAgICAgICBcInNMYXN0XCI6IFwi2KfZhNij2K7ZitixXCJcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgc2VhcmNoaW5nOiBmYWxzZSxcclxuICAgIFwicGFnaW5nXCI6IHRydWUsXHJcbiAgICBcInByb2Nlc3NpbmdcIjogdHJ1ZSxcclxuICAgIFwic2VydmVyU2lkZVwiOiB0cnVlLFxyXG4gICAgXCJkZXN0cm95XCI6IHRydWUsXHJcbiAgICBcImlEaXNwbGF5U3RhcnRcIjogMCxcclxuICAgIFwiaURpc3BsYXlMZW5ndGhcIjogMTAwLFxyXG4gICAgYkxlbmd0aENoYW5nZTogZmFsc2UsXHJcbiAgICBcImJTb3J0XCI6IGZhbHNlLFxyXG4gICAgXCJhamF4XCI6IHtcclxuICAgICAgICB0eXBlOiAnUE9TVCcsXHJcbiAgICAgICAgZGF0YVR5cGU6ICdqc29uJyxcclxuICAgICAgICBiZWZvcmVTZW5kOiBmdW5jdGlvbiAoeGhyLCBzZXR0aW5ncykge1xyXG4gICAgICAgICAgICBzZXR0aW5ncy51cmwgPSBiYXNldXJsICsgc2V0dGluZ3MudXJsO1xyXG5cclxuICAgICAgICAgICAgdmFyIHN0ciA9IHNldHRpbmdzLmRhdGE7XHJcbiAgICAgICAgICAgIHZhciBwcm1zID0gc3RyLnNwbGl0KFwiJlwiKS5yZWR1Y2UoZnVuY3Rpb24gKHByZXYsIGN1cnIsIGksIGFycikge1xyXG4gICAgICAgICAgICAgICAgdmFyIHAgPSBjdXJyLnNwbGl0KFwiPVwiKTtcclxuICAgICAgICAgICAgICAgIHByZXZbZGVjb2RlVVJJQ29tcG9uZW50KHBbMF0pXSA9IGRlY29kZVVSSUNvbXBvbmVudChwWzFdKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBwcmV2O1xyXG4gICAgICAgICAgICB9LCB7fSk7XHJcblxyXG4gICAgICAgICAgICB2YXIgbyA9IHBybXMub2JqID8gSlNPTi5wYXJzZShwcm1zLm9iaikgOiB7fTtcclxuICAgICAgICAgICAgby5sZW5ndGggPSBwcm1zLmxlbmd0aDtcclxuICAgICAgICAgICAgby5zdGFydCA9IHBybXMuc3RhcnQ7XHJcbiAgICAgICAgICAgIGlmICghby5rZXkpIG8ua2V5ID0gcHJtc1tcInNlYXJjaFt2YWx1ZV1cIl07XHJcblxyXG4gICAgICAgICAgICB2YXIgdSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdsb2dpbicpO1xyXG4gICAgICAgICAgICB1ID0gdSA/IEpTT04ucGFyc2UodSkgOiB7fTtcclxuXHJcbiAgICAgICAgICAgIHNldHRpbmdzLmRhdGEgPSAkLnBhcmFtKHtcclxuICAgICAgICAgICAgICAgIG9iajogSlNPTi5zdHJpbmdpZnkobyksXHJcbiAgICAgICAgICAgICAgICBhdXRoa2V5OiB1LmF1dGhrZXlcclxuICAgICAgICAgICAgfSlcclxuXHJcbiAgICAgICAgfSxcclxuICAgICAgICBkYXRhRmlsdGVyOiBmdW5jdGlvbiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgdmFyIGogPSBKU09OLnBhcnNlKHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgdmFyIHJldCA9IHt9O1xyXG4gICAgICAgICAgICByZXQuZGF0YSA9IGo7XHJcbiAgICAgICAgICAgIGlmIChqLmxlbmd0aCA9PSAwKSB7XHJcbiAgICAgICAgICAgICAgICByZXQucmVjb3Jkc1RvdGFsID0gMDtcclxuICAgICAgICAgICAgICAgIHJldC5yZWNvcmRzRmlsdGVyZWQgPSAwO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcmV0LnJlY29yZHNUb3RhbCA9IGpbMF0udG90YWxjb3VudDtcclxuICAgICAgICAgICAgICAgIHJldC5yZWNvcmRzRmlsdGVyZWQgPSBqWzBdLnRvdGFsY291bnQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIEpTT04uc3RyaW5naWZ5KHJldCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59KTtcclxuXHJcbiQudG9hc3Qub3B0aW9ucyA9IHtcclxuICAgIHNob3dIaWRlVHJhbnNpdGlvbjogJ3NsaWRlJyxcclxuICAgIGhpZGVBZnRlcjogNTAwMCxcclxuICAgIGxvYWRlcjogZmFsc2UsXHJcbiAgICBhbGxvd1RvYXN0Q2xvc2U6IHRydWUsXHJcbiAgICBzdGFjazogMyxcclxuICAgIHBvc2l0aW9uOiAnYm90dG9tLWxlZnQnXHJcbn1cclxuXHJcbnN3YWwuc2V0RGVmYXVsdHMoe1xyXG4gICAgc2hvd0NhbmNlbEJ1dHRvbjogdHJ1ZSxcclxuICAgIGNvbmZpcm1CdXR0b25Db2xvcjogXCIjNWNiODVjXCIsXHJcbiAgICBjYW5jZWxCdXR0b25Db2xvcjogXCIjZDk1MzRmXCIsXHJcbiAgICBjb25maXJtQnV0dG9uVGV4dDogXCLZhti52YVcIixcclxuICAgIGNhbmNlbEJ1dHRvblRleHQ6IFwi2KXZhNi62KfYoVwiLFxyXG4gICAgYWxsb3dPdXRzaWRlQ2xpY2s6IGZhbHNlXHJcbn0pO1xyXG5cclxuLy8gIEZsYXRwaWNrci5sMTBucy5kZWZhdWx0LmZpcnN0RGF5T2ZXZWVrID0gNjtcclxuLy8gIEZsYXRwaWNrci5sb2NhbGl6ZShGbGF0cGlja3IubDEwbnMuYXIpO1xyXG5cclxuXHJcblxyXG5cclxuXHJcbi8vbW9tZW50LnVwZGF0ZUxvY2FsZSgnXCJlbi11c1wiJywge30pO1xyXG5cclxuZ2xvYmFsLmNsc0Zvcm0gPSB7XHJcbiAgICBnZXRYbWxEYXRhOiBmdW5jdGlvbiAoc2VsZWN0b3IpIHtcclxuICAgICAgICB2YXIgaiA9IFt7fV07XHJcbiAgICAgICAgJChzZWxlY3RvcikuZmluZChcIi52YWxfeG1sXCIpLmVhY2goZnVuY3Rpb24gKGluZGV4LCBjdHIpIHtcclxuICAgICAgICAgICAgalswXVskKGN0cikuYXR0cignZGF0YS1iYXNlJyldID0gY2xzRm9ybS5nZXRWYWx1ZSgkKGN0cikpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHZhciB4MmpzID0gbmV3IFgySlMoKTtcclxuICAgICAgICByZXR1cm4geDJqcy5qc29uMnhtbF9zdHIoe1xyXG4gICAgICAgICAgICBJdGVtOiBqXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG4gICAgZ2V0Q29udHJvbHM6IGZ1bmN0aW9uIChlbGVtLCBhdHRyKSB7XHJcbiAgICAgICAgaWYgKCFhdHRyKSB7XHJcbiAgICAgICAgICAgIGF0dHIgPSAnZGF0YS1iYXNlJztcclxuICAgICAgICB9XHJcbiAgICAgICAgdmFyIGZvcm1kYXRhID0ge307XHJcbiAgICAgICAgJChlbGVtKS5maW5kKFwiLnZhbFtcIiArIGF0dHIgKyBcIl1cIikuZWFjaChmdW5jdGlvbiAoaW5kZXgsIGN0cikge1xyXG4gICAgICAgICAgICB2YXIgcmVsID0gJChjdHIpLmF0dHIoYXR0cikudG9Mb3dlckNhc2UoKTtcclxuICAgICAgICAgICAgdmFyIHR5cGUgPSAkKGN0cikuYXR0cihcInR5cGVcIik7XHJcbiAgICAgICAgICAgIGlmICgkKGN0cikuaXMoJ3NlbGVjdCcpIHx8IHR5cGUgPT0gJ2N0ci1zZWxlY3QnKSB7XHJcbiAgICAgICAgICAgICAgICBmb3JtZGF0YVtyZWwgKyAnX3RleHQnXSA9IGNsc0Zvcm0uZ2V0VGV4dCgkKGN0cikpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGZvcm1kYXRhW3JlbF0gPSBjbHNGb3JtLmdldFZhbHVlKCQoY3RyKSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIGZvcm1kYXRhO1xyXG4gICAgfSxcclxuICAgIGdldFRleHQ6IGZ1bmN0aW9uICgkb2JqKSB7XHJcbiAgICAgICAgdmFyIHR5cGUgPSAkb2JqLmF0dHIoXCJ0eXBlXCIpO1xyXG4gICAgICAgIGlmICgkb2JqLmlzKCdzZWxlY3QnKSkge1xyXG4gICAgICAgICAgICB2YXIgdGV4dCA9ICRvYmouZmluZCgnb3B0aW9uOnNlbGVjdGVkJylcclxuICAgICAgICAgICAgaWYgKHRleHQubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCRvYmouYXR0cignbXVsdGlwbGUnKSAmJiB0eXBlb2YgdGV4dC50ZXh0KCkgPT0gXCJvYmplY3RcIikge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0ZXh0LnRleHQoKS5qb2luKCcsJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGV4dC50ZXh0KCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2UgaWYgKHR5cGUgPT0gJ2N0ci1zZWxlY3QnKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAkb2JqWzBdLnRleHQoKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgZ2V0VmFsdWU6IGZ1bmN0aW9uICgkb2JqKSB7XHJcbiAgICAgICAgdmFyIHR5cGUgPSAkb2JqLmF0dHIoXCJ0eXBlXCIpO1xyXG4gICAgICAgIGlmICh0eXBlID09IFwiY2hlY2tib3hcIikge1xyXG4gICAgICAgICAgICByZXR1cm4gJG9iai5pcyhcIjpjaGVja2VkXCIpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoJG9iai5kYXRhKCdzZWxlY3QyJykpIHtcclxuICAgICAgICAgICAgaWYgKCRvYmouYXR0cignbXVsdGlwbGUnKSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCRvYmouc2VsZWN0MigndmFsJykpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJG9iai5zZWxlY3QyKCd2YWwnKS5qb2luKCcsJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuICRvYmouc2VsZWN0MigndmFsJyk7XHJcbiAgICAgICAgfSBlbHNlIGlmICgkb2JqLmlzKCdzZWxlY3QnKSkge1xyXG4gICAgICAgICAgICBpZiAoJG9iai5hdHRyKCdtdWx0aXBsZScpKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJG9iai52YWwoKS5qb2luKCcsJylcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gJG9iai52YWwoKTtcclxuICAgICAgICB9IGVsc2UgaWYgKCRvYmouaXMoJzppbnB1dCcpKSByZXR1cm4gJG9iai52YWwoKTtcclxuICAgICAgICBlbHNlIGlmICgkb2JqWzBdLl90YWcpIHJldHVybiAkb2JqWzBdLl90YWcudmFsKCk7XHJcbiAgICAgICAgZWxzZSBpZiAoJG9ialswXS52YWwgIT0gdW5kZWZpbmVkKSByZXR1cm4gJG9ialswXS52YWwoKTtcclxuICAgICAgICBlbHNlIGlmICghdHlwZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gJG9iai5odG1sKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIHNldENvbnRyb2w6IGZ1bmN0aW9uIChGb3JtU2VsZWN0b3IsIGpzb24sIGF0dHIpIHtcclxuICAgICAgICBpZiAoIWF0dHIpIHtcclxuICAgICAgICAgICAgYXR0ciA9ICdkYXRhLWJhc2UnO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmb3IgKHZhciBrZXkgaW4ganNvbikge1xyXG5cclxuICAgICAgICAgICAgdmFyIGVsZW0gPSAkKEZvcm1TZWxlY3RvcikuZmluZChcIltcIiArIGF0dHIgKyBcIj0nXCIgKyBrZXkgKyBcIiddXCIpO1xyXG5cclxuICAgICAgICAgICAgdmFyIHR5cGUgPSBlbGVtLmF0dHIoXCJ0eXBlXCIpO1xyXG4gICAgICAgICAgICBpZiAodHlwZSA9PSBcImNoZWNrYm94XCIgfHwgdHlwZSA9PSBcInJhZGlvXCIpIHtcclxuICAgICAgICAgICAgICAgIGlmIChqc29uW2tleV0gPT0gMSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGVsZW0uYXR0cignY2hlY2tlZCcsICdjaGVja2VkJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZWxlbS5pcyhcImlucHV0XCIpIHx8IGVsZW0uaXMoXCJ0ZXh0YXJlYVwiKSkge1xyXG4gICAgICAgICAgICAgICAgZWxlbS52YWwoanNvbltrZXldKTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChlbGVtLmlzKFwic2VsZWN0XCIpKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZWxlbS5kYXRhKCdzZWxlY3QyJykpIHtcclxuICAgICAgICAgICAgICAgICAgICBlbGVtLnNlbGVjdDIoJ3ZhbCcsIGpzb25ba2V5XSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGVsZW0uZmluZCgnb3B0aW9uJykuZWFjaChmdW5jdGlvbiAoaW5keCwgb3B0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0eXBlb2YganNvbltrZXldID09PSAnc3RyaW5nJyB8fCB0eXBlb2YganNvbltrZXldID09PSAnaW50ZWdlcicpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICgkKG9wdCkudmFsKCkgPT0ganNvbltrZXldKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJChvcHQpLmF0dHIoJ3NlbGVjdGVkJywgJ3NlbGVjdGVkJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBqc29uW2tleV0ubWFwKGZ1bmN0aW9uICh2KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCQob3B0KS52YWwoKSA9PSB2KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQob3B0KS5hdHRyKCdzZWxlY3RlZCcsICdzZWxlY3RlZCcpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZWxlbS5sZW5ndGggJiYgZWxlbVswXS5fdGFnKSBlbGVtWzBdLl90YWcudmFsKGpzb25ba2V5XSk7XHJcbiAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgZWxlbS5odG1sKGpzb25ba2V5XSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG59XHJcblxyXG5nbG9iYWwuY2xzRmlsZSA9IHtcclxuICAgIGdldE5hbWU6IGZ1bmN0aW9uIChmaWxlKSB7XHJcbiAgICAgICAgdmFyIGYgPSBmaWxlLnJlcGxhY2UoL14uKihcXFxcfFxcL3xcXDopLywgJycpO1xyXG4gICAgICAgIHJldHVybiBmLnJlcGxhY2UoJy4nICsgY2xzRmlsZS5nZXRFeHRlbnNpb24oZmlsZSksICcnKTtcclxuICAgIH0sXHJcbiAgICBnZXRFeHRlbnNpb246IGZ1bmN0aW9uIChmaWxlKSB7XHJcbiAgICAgICAgcmV0dXJuIGZpbGUucmVwbGFjZSgvXi4qP1xcLihbYS16QS1aMC05XSspJC8sIFwiJDFcIik7XHJcbiAgICB9LFxyXG4gICAgZG93bmxvYWQ6IGZ1bmN0aW9uIChwYXRoKSB7XHJcbiAgICAgICAgLyogZG93bmxvYWQgZnVuY3RpbyA9IGRvd25sb2FkX2ZpbGUgKi9cclxuICAgICAgICAkKCcjZG93bl9mcmFtJykucmVtb3ZlKCk7XHJcbiAgICAgICAgJChcImJvZHlcIikuYXBwZW5kKFwiPGlmcmFtZSBpZD0nZG93bl9mcmFtJyBzcmM9J1wiICsgY2xzQWpheC5hcGkgKyBcIj9hY3Rpb249ZG93bmxvYWRfZmlsZSZwYXRoPVwiICsgcGF0aCArIFwiJyBzdHlsZT0nZGlzcGxheTogbm9uZTsnPjwvaWZyYW1lPlwiKTtcclxuICAgIH1cclxufSIsImltcG9ydCBcIi4vbWVudS5odG1sXCJcclxuLy9pbXBvcnQgXCIuL3JvdXRlaGFuZGxlci5odG1sXCJcclxuaW1wb3J0IFwiLi9maWxlLXVwbG9hZGVyLmh0bWxcIlxyXG5pbXBvcnQgXCIuL2RkbC1sb29rdXAuaHRtbFwiIiwiPGRkbC1sb29rdXA+XHJcbiAgICA8c2VsZWN0IHJlZj1cIm15c2VsZWN0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2wgXCI+XHJcbiAgICAgICAgPG9wdGlvbiB2YWx1ZT1cIlwiPtio2LHYrNin2KEg2KfZhNin2K7YqtmK2KfYsTwvb3B0aW9uPlxyXG4gICAgPC9zZWxlY3Q+XHJcbiAgICA8c2NyaXB0PlxyXG4gICAgICAgIHZhciBzZWxmID0gdGhpcztcclxuXHJcbiAgICAgICAgc2VsZi5vbignbW91bnQnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHNlbGYgPSB0aGlzO1xyXG4gICAgICAgICAgICB2YXIgcGFyZW50b2JqID0gc2VsZi5wYXJlbnQub3B0cy5wYWdlLmRhdGE7XHJcbiAgICAgICAgICAgIGlmIChwYXJlbnRvYmogJiYgcGFyZW50b2JqLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgdmFyIGtleV90ZXh0ID0gc2VsZi5vcHRzLmRhdGFUeHQ7XHJcbiAgICAgICAgICAgICAgICB2YXIga2V5X3ZhbCA9IHNlbGYub3B0cy5kYXRhQmFzZTtcclxuICAgICAgICAgICAgICAgICQoc2VsZi5yZWZzLm15c2VsZWN0KS5hcHBlbmQoJChcIjxvcHRpb24gLz5cIikuYXR0cignc2VsZWN0ZWQnLCAnc2VsZWN0ZWQnKS50ZXh0KHBhcmVudG9ialswXVtcclxuICAgICAgICAgICAgICAgICAgICBrZXlfdGV4dFxyXG4gICAgICAgICAgICAgICAgXSkpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB2YXIgc2V0ID0ge1xyXG4gICAgICAgICAgICAgICAgZGlyOiAncnRsJyxcclxuICAgICAgICAgICAgICAgIGxhbmd1YWdlOiBcImFyXCIsXHJcbiAgICAgICAgICAgICAgICB0aGVtZTogJ2Jvb3RzdHJhcDQnLFxyXG4gICAgICAgICAgICAgICAgdGFnczogdHJ1ZVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChzZWxmLm9wdHMuZGF0YUFqYXhVcmwpIHtcclxuICAgICAgICAgICAgICAgIHNldC5hamF4ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6ICdQT1NUJyxcclxuICAgICAgICAgICAgICAgICAgICB1cmw6IHNlbGYub3B0cy5kYXRhQWpheFVybCxcclxuICAgICAgICAgICAgICAgICAgICBkYXRhVHlwZTogJ2pzb24nLFxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGE6IGZ1bmN0aW9uIChwYXJhbXMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9iajogSlNPTi5zdHJpbmdpZnkoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleTogcGFyYW1zLnRlcm1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBwcm9jZXNzUmVzdWx0czogZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YS5tYXAoZnVuY3Rpb24gKGQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGQudGV4dCA9IGRbc2VsZi5vcHRzLmRhdGFBamF4VGV4dF07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkLmlkID0gZFtzZWxmLm9wdHMuZGF0YUFqYXhWYWxdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdHM6IGRhdGFcclxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAkKHNlbGYucmVmcy5teXNlbGVjdCkuc2VsZWN0MihzZXQpO1xyXG5cclxuICAgICAgICB9KTtcclxuICAgICAgICBzZWxmLm9uKCd1cGRhdGUnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHNlbGYgPSB0aGlzO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHNlbGYudmFsaWQgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHZhciBpc3ZhbGlkID0gc2VsZi5iYXNldXJsLmxlbmd0aCA+IDA7XHJcbiAgICAgICAgICAgIGlmIChpc3ZhbGlkKSB7XHJcbiAgICAgICAgICAgICAgICAkKHNlbGYucmVmcy5ob2xkZXIpLmFkZENsYXNzKCdoYXMtc3VjY2VzcycpLnJlbW92ZUNsYXNzKCdoYXMtZXJyb3InKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICQoc2VsZi5yZWZzLmhvbGRlcikuYWRkQ2xhc3MoJ2hhcy1lcnJvcicpLnJlbW92ZUNsYXNzKCdoYXMtc3VjY2VzcycpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBpc3ZhbGlkO1xyXG4gICAgICAgIH1cclxuICAgICAgICBzZWxmLnZhbCA9IGZ1bmN0aW9uICh2YWwpIHtcclxuICAgICAgICAgICAgaWYgKHZhbCkge1xyXG4gICAgICAgICAgICAgICAgJChzZWxmLnJlZnMubXlzZWxlY3QpLnZhbCh2YWwpLnRyaWdnZXIoJ2NoYW5nZScpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiAkLnRyaW0oJChzZWxmLnJlZnMubXlzZWxlY3QpLnZhbCgpKTtcclxuICAgICAgICB9XHJcbiAgICA8L3NjcmlwdD5cclxuXHJcbiAgICA8c3R5bGU+XHJcbiAgICA8L3N0eWxlPlxyXG48L2RkbC1sb29rdXA+IiwiPGZpbGUtdXBsb2FkZXI+XHJcbiAgICA8ZGl2IHJlZj1cImhvbGRlclwiIG9uY2xpY2s9e2ZuT3Blbn0gY2xhc3M9XCJpbnB1dC1ncm91cCBjb2wteHMtMTJcIj5cclxuICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFwcGVuZFwiPlxyXG4gICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVwiZmlsZS11cGxvYWQtYnJvd3NlIGJ0biBidG4taW5mb1wiIHR5cGU9XCJidXR0b25cIj5cclxuICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibWRpIG1kaS1jbG91ZC11cGxvYWRcIj48L2k+XHJcbiAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgIDwvc3Bhbj5cclxuICAgICAgICA8aW5wdXQgcmVmPVwibXl0ZXh0XCIgdHlwZT1cInRleHRcIiB2YWx1ZT17YmFzZXVybH0gZGlyPVwibHRyXCIgY2xhc3M9XCJmb3JtLWNvbnRyb2wgZmlsZS11cGxvYWQtaW5mb1wiIGRpc2FibGVkPVwiXCIgcGxhY2Vob2xkZXI9XCLYqtit2YXZitmEXCI+XHJcbiAgICAgICAgPGlucHV0IHJlZj1cIm15ZmlsZVwiIG9uY2hhbmdlPXtmbkZpbGVDaGFuZ2VkfSB0eXBlPVwiZmlsZVwiIGFjY2VwdD1cImltYWdlLypcIj5cclxuXHJcbiAgICA8L2Rpdj5cclxuICAgIDxkaXYgcmVmPVwiaG9sZGVyX2ltYWdlXCIgc3R5bGU9XCJkaXNwbGF5OiBub25lO1wiPlxyXG4gICAgICAgIDxpbWcgc3JjPVwie2Jhc2U2NH1cIiBjbGFzcz1cImltZy1yZXZpZXdcIiAvPlxyXG4gICAgPC9kaXY+XHJcbiAgICA8c2NyaXB0PlxyXG4gICAgICAgIHZhciBzZWxmID0gdGhpcztcclxuXHJcbiAgICAgICAgc2VsZi5iYXNlNjQgPSBcIlwiO1xyXG4gICAgICAgIHNlbGYuYmFzZXVybCA9IFwiXCI7XHJcbiAgICAgICAgc2VsZi5vbignbW91bnQnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHNlbGYgPSB0aGlzO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHNlbGYub24oJ3VwZGF0ZScsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgc2VsZiA9IHRoaXM7XHJcbiAgICAgICAgICAgIGlmIChzZWxmLmJhc2V1cmwubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgJChzZWxmLnJlZnMuaG9sZGVyKS5wb3BvdmVyKHtcclxuICAgICAgICAgICAgICAgICAgICB0cmlnZ2VyOiAnaG92ZXInLFxyXG4gICAgICAgICAgICAgICAgICAgIGh0bWw6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gJChzZWxmLnJlZnMuaG9sZGVyX2ltYWdlKS5odG1sKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9KTtcclxuICAgICAgICBzZWxmLnZhbGlkID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICB2YXIgaXN2YWxpZCA9IHNlbGYuYmFzZXVybC5sZW5ndGggPiAwO1xyXG4gICAgICAgICAgICBpZiAoaXN2YWxpZCkge1xyXG4gICAgICAgICAgICAgICAgJChzZWxmLnJlZnMuaG9sZGVyKS5hZGRDbGFzcygnaGFzLXN1Y2Nlc3MnKS5yZW1vdmVDbGFzcygnaGFzLWVycm9yJyk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAkKHNlbGYucmVmcy5ob2xkZXIpLmFkZENsYXNzKCdoYXMtZXJyb3InKS5yZW1vdmVDbGFzcygnaGFzLXN1Y2Nlc3MnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gaXN2YWxpZDtcclxuICAgICAgICB9XHJcbiAgICAgICAgc2VsZi52YWwgPSBmdW5jdGlvbiAodmFsKSB7XHJcbiAgICAgICAgICAgIGlmICh2YWwpIHtcclxuICAgICAgICAgICAgICAgIHNlbGYuYmFzZXVybCA9IHZhbDtcclxuICAgICAgICAgICAgICAgIHRvRGF0YVVybChnbG9iYWwuYmFzZWltZ3VybCArIHZhbCwgZnVuY3Rpb24gKGJhc2U2NCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlbGYuYmFzZTY0ID0gYmFzZTY0O1xyXG4gICAgICAgICAgICAgICAgICAgIHNlbGYudXBkYXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gJC50cmltKHNlbGYuYmFzZTY0KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgc2VsZi5mbk9wZW4gPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICQoc2VsZi5yZWZzLm15ZmlsZSkuY2xpY2soKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgc2VsZi5mbkZpbGVDaGFuZ2VkID0gZnVuY3Rpb24gKGUpIHtcclxuICAgICAgICAgICAgdmFyIGZpbHMgPSBzZWxmLnJlZnMubXlmaWxlLmZpbGVzO1xyXG4gICAgICAgICAgICBpZiAoZmlscy5sZW5ndGggPT0gMCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHZhciBmID0gZmlsc1swXVxyXG4gICAgICAgICAgICAkKHNlbGYucmVmcy5teXRleHQpLnZhbChmLm5hbWUpO1xyXG4gICAgICAgICAgICBnZXRGaWxlQ29udGVudEFzQmFzZTY0SW5wdXQoZiwgZnVuY3Rpb24gKGNvbnRlbnQpIHtcclxuICAgICAgICAgICAgICAgIHNlbGYuYmFzZTY0ID0gY29udGVudDtcclxuICAgICAgICAgICAgICAgIHNlbGYuYmFzZXVybCA9IGYubmFtZTtcclxuICAgICAgICAgICAgICAgIHNlbGYudXBkYXRlKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIGdldEZpbGVDb250ZW50QXNCYXNlNjRJbnB1dChmaWxlLCBjYWxsYmFjaykge1xyXG4gICAgICAgICAgICB2YXIgcmVhZGVyID0gbmV3IEZpbGVSZWFkZXIoKTtcclxuICAgICAgICAgICAgcmVhZGVyLm9ubG9hZGVuZCA9IGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgY29udGVudCA9IHRoaXMucmVzdWx0O1xyXG4gICAgICAgICAgICAgICAgY2FsbGJhY2soY29udGVudCk7XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHJlYWRlci5yZWFkQXNEYXRhVVJMKGZpbGUpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gdG9EYXRhVXJsKHVybCwgY2FsbGJhY2spIHtcclxuICAgICAgICAgICAgdmFyIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xyXG4gICAgICAgICAgICB4aHIub25sb2FkID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgdmFyIHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKCk7XHJcbiAgICAgICAgICAgICAgICByZWFkZXIub25sb2FkZW5kID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKHJlYWRlci5yZXN1bHQpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcmVhZGVyLnJlYWRBc0RhdGFVUkwoeGhyLnJlc3BvbnNlKTtcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgeGhyLm9wZW4oJ0dFVCcsIHVybCk7XHJcbiAgICAgICAgICAgIHhoci5yZXNwb25zZVR5cGUgPSAnYmxvYic7XHJcbiAgICAgICAgICAgIHhoci5zZW5kKCk7XHJcbiAgICAgICAgfVxyXG4gICAgPC9zY3JpcHQ+XHJcblxyXG4gICAgPHN0eWxlPlxyXG4gICAgICAgIFt0eXBlPVwiZmlsZVwiXSB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuaW1nLXJldmlldyB7XHJcbiAgICAgICAgICAgIG1heC13aWR0aDogMjAwcHg7XHJcbiAgICAgICAgICAgIG1heC1oZWlnaHQ6IDIwMHB4O1xyXG4gICAgICAgIH1cclxuICAgIDwvc3R5bGU+XHJcbjwvZmlsZS11cGxvYWRlcj4iLCI8bWVudT5cclxuICAgIDx1bCBjbGFzcz1cIm5hdiBwYWdlLW5hdmlnYXRpb25cIj5cclxuICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiIy9hcHAvXCIgY2xhc3M9XCJuYXYtbGlua1wiPlxyXG4gICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJsaW5rLWljb24gbWRpIG1kaS10ZWxldmlzaW9uXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJtZW51LXRpdGxlXCI+2KfZhNix2KbZitiz2YrYqTwvc3Bhbj5cclxuICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgIDwvbGk+XHJcbiAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgPGEgaHJlZj1cIiMvYXBwL3BhcnRuZXIvbGlzdFwiIGNsYXNzPVwibmF2LWxpbmtcIj5cclxuICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibGluay1pY29uIG1kaSBtZGktYXN0ZXJpc2tcIj48L2k+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cIm1lbnUtdGl0bGVcIj7Yp9mE2LTYsdmD2KfYoTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibWVudS1hcnJvd1wiPjwvaT5cclxuICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwic3VibWVudVwiPlxyXG4gICAgICAgICAgICAgICAgPHVsIGNsYXNzPVwic3VibWVudS1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIjL2FwcC9wYXJ0bmVyL2xpc3RcIj7Zgtin2KbZhdipINin2YTYtNix2YPYp9ihPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIjL2FwcC9naWZ0L2xpc3RcIj7Yp9mE2YfYr9in2YrYpzwvYT5cclxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9saT5cclxuICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiIy9hcHAvY29uc3VsdGF0aW9uL2xpc3RcIiBjbGFzcz1cIm5hdi1saW5rXCI+XHJcbiAgICAgICAgICAgICAgICA8aSBjbGFzcz1cImxpbmstaWNvbiBtZGkgbWRpLWFzdGVyaXNrXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJtZW51LXRpdGxlXCI+2KfZhNiq2LfYqNmK2YI8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8aSBjbGFzcz1cIm1lbnUtYXJyb3dcIj48L2k+XHJcbiAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInN1Ym1lbnVcIj5cclxuICAgICAgICAgICAgICAgIDx1bCBjbGFzcz1cInN1Ym1lbnUtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiIy9hcHAvY29uc3VsdGF0aW9uL2xpc3RcIj7Yp9mE2KfYs9iq2LTYp9ix2KfYqjwvYT5cclxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9saT5cclxuICAgICAgICA8IS0tIFxyXG4gICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbSBtZWdhLW1lbnVcIj5cclxuICAgICAgICAgICAgPGEgaHJlZj1cIiNcIiBjbGFzcz1cIm5hdi1saW5rXCI+XHJcbiAgICAgICAgICAgICAgICA8aSBjbGFzcz1cImxpbmstaWNvbiBtZGkgbWRpLWFuZHJvaWQtc3R1ZGlvXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJtZW51LXRpdGxlXCI+Rk9STVM8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8aSBjbGFzcz1cIm1lbnUtYXJyb3dcIj48L2k+XHJcbiAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInN1Ym1lbnVcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtZ3JvdXAtd3JhcHBlciByb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLWdyb3VwIGNvbC1tZC0zXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzPVwiY2F0ZWdvcnktaGVhZGluZ1wiPkJhc2ljIEVsZW1lbnRzPC9wPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8dWwgY2xhc3M9XCJzdWJtZW51LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCJiYXNpY19lbGVtZW50cy5odG1sXCI+QmFzaWMgRWxlbWVudHM8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cImFkdmFuY2VkX2VsZW1lbnRzLmh0bWxcIj5BZHZhbmNlZCBFbGVtZW50czwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwidmFsaWRhdGlvbi5odG1sXCI+VmFsaWRhdGlvbjwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwid2l6YXJkLmh0bWxcIj5XaXphcmQ8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cInRleHRfZWRpdG9yLmh0bWxcIj5UZXh0IEVkaXRvcjwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiY29kZV9lZGl0b3IuaHRtbFwiPkNvZGUgRWRpdG9yPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLWdyb3VwIGNvbC1tZC0zXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzPVwiY2F0ZWdvcnktaGVhZGluZ1wiPkNoYXJ0czwvcD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHVsIGNsYXNzPVwic3VibWVudS1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vY2hhcnRzL2NoYXJ0anMuaHRtbFwiPkNoYXJ0IEpzPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi9jaGFydHMvbW9ycmlzLmh0bWxcIj5Nb3JyaXM8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIi4uL2NoYXJ0cy9mbG90LWNoYXJ0Lmh0bWxcIj5GbGFvdDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vY2hhcnRzL2dvb2dsZS1jaGFydHMuaHRtbFwiPkdvb2dsZSBDaGFydDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vY2hhcnRzL3NwYXJrbGluZS5odG1sXCI+U3BhcmtsaW5lPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi9jaGFydHMvYzMuaHRtbFwiPkMzIENoYXJ0PC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi9jaGFydHMvY2hhcnRpc3QuaHRtbFwiPkNoYXJ0aXN0PC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi9jaGFydHMvanVzdEdhZ2UuaHRtbFwiPkp1c3RHYWdlPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLWdyb3VwIGNvbC1tZC0zXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzPVwiY2F0ZWdvcnktaGVhZGluZ1wiPk1hcHM8L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDx1bCBjbGFzcz1cInN1Ym1lbnUtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIi4uL21hcHMvbWFwZWFsLmh0bWxcIj5NYXBlYWw8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIi4uL21hcHMvdmVjdG9yLW1hcC5odG1sXCI+VmVjdG9yIE1hcDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vbWFwcy9nb29nbGUtbWFwcy5odG1sXCI+R29vZ2xlIE1hcDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9saT5cclxuICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiI1wiIGNsYXNzPVwibmF2LWxpbmtcIj5cclxuICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibGluay1pY29uIG1kaSBtZGktYXN0ZXJpc2tcIj48L2k+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cIm1lbnUtdGl0bGVcIj5BUFBTPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJtZW51LWFycm93XCI+PC9pPlxyXG4gICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJzdWJtZW51XCI+XHJcbiAgICAgICAgICAgICAgICA8dWwgY2xhc3M9XCJzdWJtZW51LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIi4uL2FwcHMvZW1haWwuaHRtbFwiPkVtYWlsPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi9hcHBzL2NhbGVuZGFyLmh0bWxcIj5DYWxlbmRhcjwvYT5cclxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vYXBwcy90b2RvLmh0bWxcIj5Ub2RvIExpc3Q8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIi4uL2FwcHMvZ2FsbGVyeS5odG1sXCI+R2FsbGVyeTwvYT5cclxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9saT4gLS0+XHJcbiAgICA8L3VsPlxyXG4gICAgPHNjcmlwdD5cclxuICAgIDwvc2NyaXB0PlxyXG48L21lbnU+IiwiZ2xvYmFsLnJpb3QgPSByZXF1aXJlKFwicmlvdFwiKTtcclxucmVxdWlyZShcInJpb3QtaG90LXJlbG9hZFwiKTtcclxuLy9yaW90LnJlbG9hZCgnbXktY29tcG9uZW50JylcclxucmVxdWlyZSgncmlvdC1yb3V0ZWhhbmRsZXInKTtcclxuXHJcbnZhciByb3V0ZXMgPSByZXF1aXJlKFwiLi9yb3V0ZXJcIikuZGVmYXVsdDtcclxuaW1wb3J0IFwiLi9jb21tb24vY29tbW9uLWluZGV4XCI7XHJcbmltcG9ydCBcIi4vY29tcG9uZW50cy9jb21wb25lbnRzLWluZGV4XCI7XHJcbmltcG9ydCBcIi4vdmlld3Mvdmlld3MtaW5kZXhcIjtcclxuLy9pbXBvcnQgXCIuL2NvbW1vbi9yb3V0ZWhhbmRsZXJcIjtcclxuXHJcblxyXG5yaW90LnJvdXRlciA9IHJpb3QubW91bnQoXCJyb3V0ZWhhbmRsZXJcIiwge1xyXG4gICAgcm91dGVzOiByb3V0ZXMsXHJcbiAgICBvcHRpb25zOiB7XHJcbiAgICAgICAgaGFzaGJhbmc6IHRydWUsXHJcbiAgICAgICAgYmFzZTogXCIvI1wiXHJcbiAgICB9XHJcbn0pOyIsInZhciBhdXRoID0gZnVuY3Rpb24gKGN0eCwgbmV4dCwgcGFnZSkge1xyXG4gICAgdmFyIGxvZ2luZGF0YSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdsb2dpbicpO1xyXG4gICAgaWYgKGxvZ2luZGF0YSkgcmV0dXJuIG5leHQoKTtcclxuXHJcbiAgICBwYWdlLnJlZGlyZWN0KCcvbG9naW4nKVxyXG59XHJcbnZhciByb3V0ZXMgPSBbe1xyXG4gICAgICAgIHJvdXRlOiBcIi9cIixcclxuICAgICAgICB0YWc6IFwibG9naW5cIlxyXG4gICAgfSwge1xyXG4gICAgICAgIHJvdXRlOiBcIi9sb2dpblwiLFxyXG4gICAgICAgIHRhZzogXCJsb2dpblwiXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICAgIHJvdXRlOiBcIi9sb2dvdXRcIixcclxuICAgICAgICB0YWc6IFwibG9nb3V0XCJcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgcm91dGU6IFwiL2FwcC8qXCIsXHJcbiAgICAgICAgdXNlOiBhdXRoXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICAgIHJvdXRlOiBcIi9hcHAvXCIsXHJcbiAgICAgICAgdGFnOiBcImFwcFwiLFxyXG4gICAgICAgIHJvdXRlczogW3tcclxuICAgICAgICAgICAgICAgIHJvdXRlOiBcIi9cIixcclxuICAgICAgICAgICAgICAgIHRhZzogXCJob21lXCJcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgcm91dGU6IFwiL3BhcnRuZXIvbGlzdFwiLFxyXG4gICAgICAgICAgICAgICAgdGFnOiBcInBhcnRuZXItbGlzdFwiXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHJvdXRlOiBcIi9wYXJ0bmVyL2l0ZW0vOmlkXCIsXHJcbiAgICAgICAgICAgICAgICB0YWc6IFwicGFydG5lci1pdGVtXCIsXHJcbiAgICAgICAgICAgICAgICB1c2U6IGZ1bmN0aW9uIChjdHgsIG5leHQsIHBhZ2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAkLmFqYXhzeXMoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB1cmw6IFwiL2FwaS9wYXJ0bmVyL2dldC9cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JqOiBKU09OLnN0cmluZ2lmeSh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IGN0eC5wYXJhbXMuaWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIChkYXRhKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYWdlLmRhdGEgPSBkYXRhO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbmV4dCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSlcclxuXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHJvdXRlOiBcIi9naWZ0L2xpc3RcIixcclxuICAgICAgICAgICAgICAgIHRhZzogXCJnaWZ0LWxpc3RcIlxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICByb3V0ZTogXCIvZ2lmdC9pdGVtLzppZFwiLFxyXG4gICAgICAgICAgICAgICAgdGFnOiBcImdpZnQtaXRlbVwiLFxyXG4gICAgICAgICAgICAgICAgdXNlOiBmdW5jdGlvbiAoY3R4LCBuZXh0LCBwYWdlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJC5hamF4c3lzKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdXJsOiBcIi9hcGkvZ2lmdC9nZXQvXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9iajogSlNPTi5zdHJpbmdpZnkoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiBjdHgucGFyYW1zLmlkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAoZGF0YSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFnZS5kYXRhID0gZGF0YTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5leHQoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcblxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICByb3V0ZTogXCIvY29uc3VsdGF0aW9uL2xpc3RcIixcclxuICAgICAgICAgICAgICAgIHRhZzogXCJjb25zdWx0YXRpb24tbGlzdFwiXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHJvdXRlOiBcIi9jb25zdWx0YXRpb24vaXRlbS86aWRcIixcclxuICAgICAgICAgICAgICAgIHRhZzogXCJjb25zdWx0YXRpb24taXRlbVwiLFxyXG4gICAgICAgICAgICAgICAgdXNlOiBmdW5jdGlvbiAoY3R4LCBuZXh0LCBwYWdlKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICQuYWpheHN5cyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVybDogXCIvYXBpL2NvbnN1bHRhdGlvbnMvZ2V0L1wiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYmo6IEpTT04uc3RyaW5naWZ5KHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogY3R4LnBhcmFtcy5pZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhZ2UuZGF0YSA9IGRhdGE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBuZXh0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG5cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIF1cclxuICAgIH0sXHJcbl07XHJcblxyXG5cclxuZXhwb3J0IGRlZmF1bHQgcm91dGVzO1xyXG5cclxuLy9odHRwczovL3d3dy5ucG1qcy5jb20vcGFja2FnZS9yaW90LXJvdXRlaGFuZGxlciIsIjxhcHA+XHJcblxyXG4gIDxkaXYgY2xhc3M9XCJjb250YWluZXItc2Nyb2xsZXJcIj5cclxuICAgIDwhLS0gcGFydGlhbDpwYXJ0aWFscy9faG9yaXpvbnRhbC1uYXZiYXIuaHRtbCAtLT5cclxuICAgIDxuYXYgY2xhc3M9XCJuYXZiYXIgaG9yaXpvbnRhbC1sYXlvdXQgY29sLWxnLTEyIGNvbC0xMiBwLTBcIj5cclxuICAgICAgPGRpdiBjbGFzcz1cImNvbnRhaW5lciBkLWZsZXggZmxleC1yb3dcIj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwidGV4dC1jZW50ZXIgbmF2YmFyLWJyYW5kLXdyYXBwZXIgZC1mbGV4IGFsaWduLWl0ZW1zLXRvcFwiPlxyXG4gICAgICAgICAgPGEgY2xhc3M9XCJuYXZiYXItYnJhbmQgYnJhbmQtbG9nb1wiIGhyZWY9XCIjL2FwcC9cIj5cclxuICAgICAgICAgICAgPGltZyBzcmM9XCJpbWFnZXMvbG9nby5wbmdcIiBhbHQ9XCJsb2dvXCIgLz5cclxuICAgICAgICAgIDwvYT5cclxuICAgICAgICAgIDxhIGNsYXNzPVwibmF2YmFyLWJyYW5kIGJyYW5kLWxvZ28tbWluaVwiIGhyZWY9XCIjL2FwcC9cIj5cclxuICAgICAgICAgICAgPGltZyBzcmM9XCJpbWFnZXMvbG9nby5wbmdcIiBhbHQ9XCJsb2dvXCIgLz5cclxuICAgICAgICAgIDwvYT5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwibmF2YmFyLW1lbnUtd3JhcHBlciBkLWZsZXggYWxpZ24taXRlbXMtY2VudGVyXCI+XHJcbiAgICAgICAgICA8Zm9ybSBjbGFzcz1cIiBtbC1hdXRvXCIgYWN0aW9uPVwiI1wiPjwvZm9ybT5cclxuICAgICAgICAgIDx1bCBjbGFzcz1cIm5hdmJhci1uYXYgbmF2YmFyLW5hdi1yaWdodCBtci0wXCI+XHJcbiAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtIGRyb3Bkb3duIGQtbm9uZSBkLXhsLWlubGluZS1ibG9ja1wiPlxyXG4gICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmsgZHJvcGRvd24tdG9nZ2xlXCIgaWQ9XCJVc2VyRHJvcGRvd25cIiBocmVmPVwiI1wiIGRhdGEtdG9nZ2xlPVwiZHJvcGRvd25cIiBhcmlhLWV4cGFuZGVkPVwiZmFsc2VcIj5cclxuICAgICAgICAgICAgICAgIDxpbWcgY2xhc3M9XCJpbWcteHMgcm91bmRlZC1jaXJjbGVcIiBzcmM9XCJpbWFnZXMvdXNlci5wbmdcIiBhbHQ9XCJQcm9maWxlIGltYWdlXCI+XHJcbiAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJkcm9wZG93bi1tZW51IGRyb3Bkb3duLW1lbnUtcmlnaHQgbmF2YmFyLWRyb3Bkb3duXCIgYXJpYS1sYWJlbGxlZGJ5PVwiVXNlckRyb3Bkb3duXCI+XHJcbiAgICAgICAgICAgICAgICA8YSBocmVmPVwiIy9sb2dvdXRcIiBjbGFzcz1cImRyb3Bkb3duLWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgU2lnbiBPdXRcclxuICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICA8YnV0dG9uIGNsYXNzPVwibmF2YmFyLXRvZ2dsZXIgYWxpZ24tc2VsZi1jZW50ZXJcIiB0eXBlPVwiYnV0dG9uXCIgZGF0YS10b2dnbGU9XCJtaW5pbWl6ZVwiPlxyXG4gICAgICAgICAgICA8c3BhbiBjbGFzcz1cIm1kaSBtZGktbWVudVwiPjwvc3Bhbj5cclxuICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICA8L2Rpdj5cclxuICAgICAgPGRpdiBjbGFzcz1cIm5hdi1ib3R0b21cIj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiY29udGFpbmVyXCI+XHJcbiAgICAgICAgICA8bWVudT48L21lbnU+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC9uYXY+XHJcblxyXG4gICAgPCEtLSBwYXJ0aWFsIC0tPlxyXG4gICAgPGRpdiBjbGFzcz1cImNvbnRhaW5lci1mbHVpZCBwYWdlLWJvZHktd3JhcHBlclwiPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwibWFpbi1wYW5lbFwiPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJjb250ZW50LXdyYXBwZXJcIj5cclxuICAgICAgICAgIDxyb3V0ZWhhbmRsZXI+PC9yb3V0ZWhhbmRsZXI+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbiAgPC9kaXY+XHJcbiAgPHNjcmlwdD5cclxuICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgIHNlbGYub24oJ21vdW50JywgZnVuY3Rpb24gKCkge1xyXG5cclxuICAgIH0pO1xyXG4gIDwvc2NyaXB0PlxyXG48L2FwcD4iLCI8Y29uc3VsdGF0aW9uLWl0ZW0+XHJcbiAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImNvbC0xMiBncmlkLW1hcmdpblwiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY2FyZFwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNhcmQtYm9keVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxoNCBjbGFzcz1cImNhcmQtdGl0bGVcIj7YqNmK2KfZhtin2Kog2KfZhNin2LPYqti02KfYsdipPC9oND5cclxuICAgICAgICAgICAgICAgICAgICA8Zm9ybSByZWY9XCJteWZvcm1cIiBvbnN1Ym1pdD17Zm5zYXZlfSBjbGFzcz1cImZvcm0tc2FtcGxlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtNlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIHJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJjb2wtc20tNCBjb2wtZm9ybS1sYWJlbFwiPtiq2KfYsdmK2K4g2KfZhNiz2KTYp9mEPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS04XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBkYXRhLWJhc2U9XCJkYXRlY3JlYXRlX2ZcIiBjbGFzcz1cImZvcm0tY29udHJvbCB2YWxcIiByZWFkb25seS8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTZcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cCByb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwiY29sLXNtLTQgY29sLWZvcm0tbGFiZWxcIj7Yp9iz2YUg2KfZhNiz2KfYptmEPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS04XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBkYXRhLWJhc2U9XCJwZXJzb25fbmFtZVwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgcmVhZG9ubHkgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cCByb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwiY29sLXNtLTIgY29sLWZvcm0tbGFiZWxcIj4g2KfZhNiz2KTYp9mEPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS0xMFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRleHRhcmVhIGRhdGEtYmFzZT1cInF1ZXN0aW9uXCIgcm93cz1cIjNcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIHJlYWRvbmx5PjwvdGV4dGFyZWE+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXAgcm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cImNvbC1zbS0yIGNvbC1mb3JtLWxhYmVsXCI+INin2YTYpdis2KfYqNipPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS0xMFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRleHRhcmVhIGRhdGEtYmFzZT1cImFuc3dlclwiIHJvd3M9XCIzXCIgY2xhc3M9XCJmb3JtLWNvbnRyb2wgdmFsXCIgcmVxdWlyZWQ+PC90ZXh0YXJlYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cCByb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwiY29sLXNtLTIgY29sLWZvcm0tbGFiZWxcIj4gPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS0xMCBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3M9XCJidG4gYnRuLXByaW1hcnlcIj7YrdmB2Lg8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9mb3JtPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbiAgICA8c2NyaXB0PlxyXG4gICAgICAgIHZhciBzZWxmID0gdGhpcztcclxuXHJcbiAgICAgICAgc2VsZi5vbignbW91bnQnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHNlbGYgPSB0aGlzO1xyXG4gICAgICAgICAgICAkKHNlbGYucmVmcy5teWZvcm0pLnZhbGlkYXRlKCk7XHJcbiAgICAgICAgICAgIGlmIChzZWxmLm9wdHMucGFyYW1zLmlkID4gLTEgJiYgc2VsZi5vcHRzLnBhZ2UuZGF0YSkge1xyXG4gICAgICAgICAgICAgICAgY2xzRm9ybS5zZXRDb250cm9sKHNlbGYucmVmcy5teWZvcm0sIHNlbGYub3B0cy5wYWdlLmRhdGFbMF0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgc2VsZi5mbnNhdmUgPSBmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgIGlmICghJChzZWxmLnJlZnMubXlmb3JtKS52YWxpZCgpKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdmFyIGogPSBjbHNGb3JtLmdldENvbnRyb2xzKHNlbGYucmVmcy5teWZvcm0pO1xyXG4gICAgICAgICAgICBqLmlkID0gc2VsZi5vcHRzLnBhcmFtcy5pZDtcclxuICAgICAgICAgICAgJC5hamF4c3lzKHtcclxuICAgICAgICAgICAgICAgIHVybDogXCIvYXBpL2NvbnN1bHRhdGlvbnMvc2V0L1wiLFxyXG4gICAgICAgICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgIG9iajogSlNPTi5zdHJpbmdpZnkoailcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAoZGF0YSkge1xyXG4gICAgICAgICAgICAgICAgICAgICQudG9hc3Qoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0OiBcItiq2YUg2KfZhNit2YHYuFwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpY29uOiAnc3VjY2VzcydcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICBzZWxmLm9wdHMucGFnZSgnL2FwcC9jb25zdWx0YXRpb24vbGlzdCcpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICA8L3NjcmlwdD5cclxuPC9jb25zdWx0YXRpb24taXRlbT4iLCI8Y29uc3VsdGF0aW9uLWxpc3Q+XHJcbiAgICA8ZGl2IGNsYXNzPVwiY2FyZFwiPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJjYXJkLWJvZHlcIj5cclxuICAgICAgICAgICAgPGg0IGNsYXNzPVwiY2FyZC10aXRsZVwiPtmC2KfYptmF2Kkg2KfZhNil2LPYqti02KfYsdin2Ko8L2g0PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLTEyIFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0YWJsZSByZWY9XCJteXRhYmxlXCIgY2xhc3M9XCJ0YWJsZSB0YWJsZS1ob3ZlciAgdGFibGUtYm9yZGVyZWRcIj48L3RhYmxlPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbiAgICA8c2NyaXB0PlxyXG4gICAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgICBzZWxmLm9uKCdtb3VudCcsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgJChzZWxmLnJlZnMubXl0YWJsZSkuRGF0YVRhYmxlKHtcclxuICAgICAgICAgICAgICAgIFwiYWpheFwiOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXJsOiAnL2FwaS9jb25zdWx0YXRpb25zL2dldC8nLFxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JqOiBKU09OLnN0cmluZ2lmeSh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhYWE6IDExXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIFwiY29sdW1uc1wiOiBbe1xyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiBcIiNcIixcclxuICAgICAgICAgICAgICAgICAgICBvcmRlcmFibGU6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIHJlbmRlcjogZnVuY3Rpb24gKGRhdGEsIHR5cGUsIGZ1bGwsIG1ldGEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIEhhbmRsZWJhcnMuY29tcGlsZShcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGBcclxuPGEgaHJlZj0nIy9hcHAvY29uc3VsdGF0aW9uL2l0ZW0ve3tpZH19Jz4je3tpZH19PC9hPmApKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZnVsbCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSwge1xyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiBcItin2YTYp9iz2YVcIixcclxuICAgICAgICAgICAgICAgICAgICBvcmRlcmFibGU6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIHJlbmRlcjogZnVuY3Rpb24gKGRhdGEsIHR5cGUsIGZ1bGwsIG1ldGEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIEhhbmRsZWJhcnMuY29tcGlsZShcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGBcclxuPGEgaHJlZj0nIy9hcHAvY29uc3VsdGF0aW9uL2l0ZW0ve3tpZH19Jz57e3BlcnNvbl9uYW1lfX08L2E+YFxyXG4gICAgICAgICAgICAgICAgICAgICAgICApKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZnVsbCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSwge1xyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiBcItin2YTYs9ik2KfZhFwiLFxyXG4gICAgICAgICAgICAgICAgICAgIG9yZGVyYWJsZTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YTogJ3F1ZXN0aW9uJ1xyXG5cclxuICAgICAgICAgICAgICAgIH1dXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfSlcclxuICAgIDwvc2NyaXB0PlxyXG48L2NvbnN1bHRhdGlvbi1saXN0PiIsIjxnaWZ0LWl0ZW0+XHJcbiAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImNvbC0xMiBncmlkLW1hcmdpblwiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY2FyZFwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNhcmQtYm9keVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxoNCBjbGFzcz1cImNhcmQtdGl0bGVcIj7YqNmK2KfZhtin2Kog2KfZhNmH2K/ZitipPC9oND5cclxuICAgICAgICAgICAgICAgICAgICA8Zm9ybSByZWY9XCJteWZvcm1cIiBvbnN1Ym1pdD17Zm5zYXZlfSBjbGFzcz1cImZvcm0tc2FtcGxlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtNlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIHJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJjb2wtc20tNCBjb2wtZm9ybS1sYWJlbFwiPti52YbZiNin2YYg2KfZhNmH2K/ZitipPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS04XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBkYXRhLWJhc2U9XCJ0aXRsZVwiIGNsYXNzPVwiZm9ybS1jb250cm9sIHZhbFwiIHJlcXVpcmVkIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIHJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJjb2wtc20tNCBjb2wtZm9ybS1sYWJlbFwiPtin2YTYtdmI2LHYqTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tOFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGZpbGUtdXBsb2FkZXIgcmVmPVwiaW1nXCIgZGF0YS1iYXNlPVwiaW1nXCI+PC9maWxlLXVwbG9hZGVyPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC02XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXAgcm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cImNvbC1zbS00IGNvbC1mb3JtLWxhYmVsXCI+2KfZhNi52K/YrzwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tOFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZWxcIiBkYXRhLWJhc2U9XCJnaWZ0Y291bnRcIiBjbGFzcz1cImZvcm0tY29udHJvbCB2YWxcIiByZXF1aXJlZCAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cCByb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwiY29sLXNtLTQgY29sLWZvcm0tbGFiZWxcIj7Yp9mE2LTYsdmK2YM8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLThcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkZGwtbG9va3VwIGRhdGEtYmFzZT1cInBhcnRuZXJfaWRcIiBkYXRhLXR4dD1cInBhcnRuZXJfbmFtZVwiIGRhdGEtYWpheC11cmw9XCIvYXBpL3BhcnRuZXIvZ2V0L1wiIGRhdGEtYWpheC12YWw9XCJpZFwiIGRhdGEtYWpheC10ZXh0PVwibmFtZVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3M9XCJ2YWxcIj48L2RkbC1sb29rdXA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXAgcm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cImNvbC1zbS0yIGNvbC1mb3JtLWxhYmVsXCI+INmI2LXZgSDYp9mE2YfYr9mK2KkgPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS0xMFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRleHRhcmVhIGRhdGEtYmFzZT1cImRlc2NyaXB0aW9uXCIgcm93cz1cIjNcIiBjbGFzcz1cImZvcm0tY29udHJvbCB2YWxcIiByZXF1aXJlZD48L3RleHRhcmVhPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIHJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJjb2wtc20tMiBjb2wtZm9ybS1sYWJlbFwiPiA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTEwIFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeVwiPtit2YHYuDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Zvcm0+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICAgIDxzY3JpcHQ+XHJcbiAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG5cclxuICAgICAgICBzZWxmLm9uKCdtb3VudCcsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgc2VsZiA9IHRoaXM7XHJcbiAgICAgICAgICAgICQoc2VsZi5yZWZzLm15Zm9ybSkudmFsaWRhdGUoKTtcclxuICAgICAgICAgICAgaWYgKHNlbGYub3B0cy5wYXJhbXMuaWQgPiAtMSAmJiBzZWxmLm9wdHMucGFnZS5kYXRhKSB7XHJcbiAgICAgICAgICAgICAgICBjbHNGb3JtLnNldENvbnRyb2woc2VsZi5yZWZzLm15Zm9ybSwgc2VsZi5vcHRzLnBhZ2UuZGF0YVswXSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICBzZWxmLmZuc2F2ZSA9IGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgaWYgKCEkKHNlbGYucmVmcy5teWZvcm0pLnZhbGlkKCkgfHwgIXNlbGYucmVmcy5pbWcudmFsaWQoKSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHZhciBqID0gY2xzRm9ybS5nZXRDb250cm9scyhzZWxmLnJlZnMubXlmb3JtKTtcclxuICAgICAgICAgICAgai5pZCA9IHNlbGYub3B0cy5wYXJhbXMuaWQ7XHJcbiAgICAgICAgICAgICQuYWpheHN5cyh7XHJcbiAgICAgICAgICAgICAgICB1cmw6IFwiL2FwaS9naWZ0L3NldC9cIixcclxuICAgICAgICAgICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICBvYmo6IEpTT04uc3RyaW5naWZ5KGopLFxyXG4gICAgICAgICAgICAgICAgICAgIFwiYmFzZTY0X2ltZ1wiOiBzZWxmLnJlZnMuaW1nLnZhbCgpXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICAkLnRvYXN0KHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGV4dDogXCLYqtmFINin2YTYrdmB2LhcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWNvbjogJ3N1Y2Nlc3MnXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5vcHRzLnBhZ2UoJy9hcHAvZ2lmdC9saXN0Jyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIDwvc2NyaXB0PlxyXG48L2dpZnQtaXRlbT4iLCI8Z2lmdC1saXN0PlxyXG4gICAgPGRpdiBjbGFzcz1cImNhcmRcIj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiY2FyZC1ib2R5XCI+XHJcbiAgICAgICAgICAgIDxoNCBjbGFzcz1cImNhcmQtdGl0bGVcIj5cclxuICAgICAgICAgICAgICAgIDxzcGFuPtmC2KfYptmF2Kkg2KfZhNmH2K/Yp9mK2Kc8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8YSBocmVmPVwiIy9hcHAvZ2lmdC9pdGVtLy0xXCIgY2xhc3M9XCJidG4gYnRuLWluZm8gYnRuLXhzIGZsb2F0LWxlZnRcIj4g2KzYr9mK2K88L2E+XHJcbiAgICAgICAgICAgIDwvaDQ+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtMTIgXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRhYmxlIHJlZj1cIm15dGFibGVcIiBjbGFzcz1cInRhYmxlIHRhYmxlLWhvdmVyICB0YWJsZS1ib3JkZXJlZFwiPjwvdGFibGU+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICAgIDxzY3JpcHQ+XHJcbiAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICAgIHNlbGYub24oJ21vdW50JywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAkKHNlbGYucmVmcy5teXRhYmxlKS5EYXRhVGFibGUoe1xyXG4gICAgICAgICAgICAgICAgXCJhamF4XCI6IHtcclxuICAgICAgICAgICAgICAgICAgICB1cmw6ICcvYXBpL2dpZnQvZ2V0LycsXHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYmo6IEpTT04uc3RyaW5naWZ5KHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFhYTogMTFcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgXCJjb2x1bW5zXCI6IFt7XHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IFwi2KfZhNin2LPZhVwiLFxyXG4gICAgICAgICAgICAgICAgICAgIG9yZGVyYWJsZTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgcmVuZGVyOiBmdW5jdGlvbiAoZGF0YSwgdHlwZSwgZnVsbCwgbWV0YSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gSGFuZGxlYmFycy5jb21waWxlKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYFxyXG48YSBocmVmPScjL2FwcC9naWZ0L2l0ZW0ve3tpZH19Jz57e3RpdGxlfX08L2E+YFxyXG4gICAgICAgICAgICAgICAgICAgICAgICApKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZnVsbCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSwge1xyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiBcIti52K/YryDYp9mE2YfYr9in2YrYp1wiLFxyXG4gICAgICAgICAgICAgICAgICAgIG9yZGVyYWJsZTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YTogJ2dpZnRjb3VudCdcclxuICAgICAgICAgICAgICAgIH0sIHtcclxuICAgICAgICAgICAgICAgICAgICB0aXRsZTogXCLYp9mE2LTYsdmK2YNcIixcclxuICAgICAgICAgICAgICAgICAgICBvcmRlcmFibGU6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIHJlbmRlcjogZnVuY3Rpb24gKGRhdGEsIHR5cGUsIGZ1bGwsIG1ldGEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIEhhbmRsZWJhcnMuY29tcGlsZShcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGBcclxuPGEgaHJlZj0nIy9hcHAvcGFydG5lci9pdGVtL3t7cGFydG5lcl9pZH19Jz57e3BhcnRuZXJfbmFtZX19PC9hPmBcclxuICAgICAgICAgICAgICAgICAgICAgICAgKShcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZ1bGwpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1dXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfSlcclxuICAgIDwvc2NyaXB0PlxyXG48L2dpZnQtbGlzdD4iLCI8aG9tZT5cclxuXHJcbiAgICA8c2NyaXB0PlxyXG4gICAgPC9zY3JpcHQ+XHJcbjwvaG9tZT4iLCI8bG9naW4+XHJcbiAgICA8ZGl2IGNsYXNzPVwiY29udGFpbmVyLXNjcm9sbGVyXCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImNvbnRhaW5lci1mbHVpZCBwYWdlLWJvZHktd3JhcHBlciBmdWxsLXBhZ2Utd3JhcHBlclwiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29udGVudC13cmFwcGVyIGQtZmxleCBhbGlnbi1pdGVtcy1jZW50ZXIgYXV0aCBhdXRoLWJnLTEgdGhlbWUtb25lXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93IHctMTAwIG14LWF1dG9cIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLWxnLTQgbXgtYXV0b1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYXV0by1mb3JtLXdyYXBwZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxmb3JtIHJlZj1cIm15Zm9ybVwiIG9uc3VibWl0PXtsb2dpbn0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwibGFiZWxcIj7Yp9mE2KjYsdmK2K8g2KfZiCDYsdmC2YUg2KfZhNmH2KfYqtmBPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgZGF0YS1iYXNlPVwidXNlcm5hbWVcIiByZXF1aXJlZCBkaXI9XCJsdHJcIiBjbGFzcz1cImZvcm0tY29udHJvbCB2YWxcIiBwbGFjZWhvbGRlcj1cItin2YTYqNix2YrYryDYp9mIINix2YLZhSDYp9mE2YfYp9iq2YFcIj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwibGFiZWxcIj7Zg9mE2YXYqSDYp9mE2YXYsdmI2LE8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInBhc3N3b3JkXCIgZGF0YS1iYXNlPVwicGFzc3dvcmRcIiByZXF1aXJlZCBkaXI9XCJsdHJcIiBjbGFzcz1cImZvcm0tY29udHJvbCB2YWxcIiBwbGFjZWhvbGRlcj1cIioqKioqKioqKlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgc3VibWl0LWJ0biBidG4tYmxvY2tcIj7Yqtiz2KzZitmEINin2YTYr9iu2YjZhDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwhLS0gPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXAgZC1mbGV4IGp1c3RpZnktY29udGVudC1iZXR3ZWVuXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWNoZWNrIGZvcm0tY2hlY2stZmxhdCBtdC0wXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJmb3JtLWNoZWNrLWxhYmVsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIGNsYXNzPVwiZm9ybS1jaGVjay1pbnB1dFwiIGNoZWNrZWQ+IEtlZXAgbWUgc2lnbmVkIGluXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cIiNcIiBjbGFzcz1cInRleHQtc21hbGwgZm9yZ290LXBhc3N3b3JkIHRleHQtYmxhY2tcIj5Gb3Jnb3QgUGFzc3dvcmQ8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+IC0tPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICAgIDxzY3JpcHQ+XHJcbiAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICAgIHNlbGYub24oJ21vdW50JywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAkKHNlbGYucmVmcy5teWZvcm0pLnZhbGlkYXRlKCk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHNlbGYubG9naW4gPSBmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgIGlmICghJChzZWxmLnJlZnMubXlmb3JtKS52YWxpZCgpKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdmFyIGogPSBjbHNGb3JtLmdldENvbnRyb2xzKHNlbGYucmVmcy5teWZvcm0pXHJcbiAgICAgICAgICAgIGoucnVsZV9pZCA9IDI7XHJcbiAgICAgICAgICAgICQuYWpheHN5cyh7XHJcbiAgICAgICAgICAgICAgICB1cmw6IFwiL2FwaS9wZXJzb24vbG9naW4vXCIsXHJcbiAgICAgICAgICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JqOiBKU09OLnN0cmluZ2lmeShqKVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIChkYXRhKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLmxlbmd0aCA9PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICQudG9hc3Qoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGV4dDogXCLZitmI2KzYryDYrti32KMg2YHZiiDYp9mE2KjZitin2YbYp9iqXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpY29uOiAnZXJyb3InXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChkYXRhWzBdW1wiZXJyb3JfYXJcIl0pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJC50b2FzdCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZXh0OiBkYXRhWzBdW1wiZXJyb3JfYXJcIl0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpY29uOiAnZXJyb3InXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnbG9naW4nLCBKU09OLnN0cmluZ2lmeShkYXRhWzBdKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5vcHRzLnBhZ2UoJy9hcHAvJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuXHJcblxyXG4gICAgICAgIH1cclxuICAgIDwvc2NyaXB0PlxyXG4gICAgPHN0eWxlPlxyXG4gICAgICAgIC5hdXRoLmF1dGgtYmctMSB7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImltYWdlcy9CbHVlQmFja2dyb3VuZC5qcGdcIik7XHJcbiAgICAgICAgfVxyXG4gICAgPC9zdHlsZT5cclxuPC9sb2dpbj4iLCI8bG9nb3V0PlxyXG4gICAgPHNjcmlwdD5cclxuICAgICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgICAgc2VsZi5vbignbW91bnQnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKCdsb2dpbicpO1xyXG4gICAgICAgICAgICBzZWxmLm9wdHMucGFnZSgnL2xvZ2luLycpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgPC9zY3JpcHQ+XHJcbjwvbG9nb3V0PiIsIjxwYXJ0bmVyLWl0ZW0+XHJcbiAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImNvbC0xMiBncmlkLW1hcmdpblwiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY2FyZFwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNhcmQtYm9keVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxoNCBjbGFzcz1cImNhcmQtdGl0bGVcIj7YqNmK2KfZhtin2Kog2KfZhNi02LHZitmDPC9oND5cclxuICAgICAgICAgICAgICAgICAgICA8Zm9ybSByZWY9XCJteWZvcm1cIiBvbnN1Ym1pdD17Zm5zYXZlfSBjbGFzcz1cImZvcm0tc2FtcGxlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtNlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIHJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJjb2wtc20tNCBjb2wtZm9ybS1sYWJlbFwiPtin2YTYp9iz2YU8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLThcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGRhdGEtYmFzZT1cIm5hbWVcIiBjbGFzcz1cImZvcm0tY29udHJvbCB2YWxcIiByZXF1aXJlZCAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cCByb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwiY29sLXNtLTQgY29sLWZvcm0tbGFiZWxcIj7Yp9mE2LXZiNix2Kk8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLThcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxmaWxlLXVwbG9hZGVyIHJlZj1cImltZ1wiIGRhdGEtYmFzZT1cImltZ1wiPjwvZmlsZS11cGxvYWRlcj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtNlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIHJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJjb2wtc20tNCBjb2wtZm9ybS1sYWJlbFwiPtix2YLZhSDYp9mE2KzZiNin2YQ8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLThcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGVsXCIgZGF0YS1iYXNlPVwicGhvbmVcIiBjbGFzcz1cImZvcm0tY29udHJvbCB2YWxcIiByZXF1aXJlZCAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIHJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJjb2wtc20tMiBjb2wtZm9ybS1sYWJlbFwiPiDYp9mE2LnZhtmI2KfZhjwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tMTBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZXh0YXJlYSBkYXRhLWJhc2U9XCJhZGRyZXNzXCIgcm93cz1cIjNcIiBjbGFzcz1cImZvcm0tY29udHJvbCB2YWxcIiByZXF1aXJlZD48L3RleHRhcmVhPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIHJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJjb2wtc20tMiBjb2wtZm9ybS1sYWJlbFwiPiDZhtio2LDYqSDYudmGINin2YTYtNix2YrZgzwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tMTBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZXh0YXJlYSBkYXRhLWJhc2U9XCJjb250ZW50XCIgcm93cz1cIjNcIiBjbGFzcz1cImZvcm0tY29udHJvbCB2YWxcIiByZXF1aXJlZD48L3RleHRhcmVhPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIHJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJjb2wtc20tMiBjb2wtZm9ybS1sYWJlbFwiPiA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTEwIFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeVwiPtit2YHYuDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Zvcm0+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICAgIDxzY3JpcHQ+XHJcbiAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG5cclxuICAgICAgICBzZWxmLm9uKCdtb3VudCcsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgc2VsZiA9IHRoaXM7XHJcbiAgICAgICAgICAgICQoc2VsZi5yZWZzLm15Zm9ybSkudmFsaWRhdGUoKTtcclxuICAgICAgICAgICAgaWYgKHNlbGYub3B0cy5wYXJhbXMuaWQgPiAtMSAmJiBzZWxmLm9wdHMucGFnZS5kYXRhKSB7XHJcbiAgICAgICAgICAgICAgICBjbHNGb3JtLnNldENvbnRyb2woc2VsZi5yZWZzLm15Zm9ybSwgc2VsZi5vcHRzLnBhZ2UuZGF0YVswXSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICBzZWxmLmZuc2F2ZSA9IGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgaWYgKCEkKHNlbGYucmVmcy5teWZvcm0pLnZhbGlkKCkgfHwgIXNlbGYucmVmcy5pbWcudmFsaWQoKSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHZhciBqID0gY2xzRm9ybS5nZXRDb250cm9scyhzZWxmLnJlZnMubXlmb3JtKTtcclxuICAgICAgICAgICAgai5pZCA9IHNlbGYub3B0cy5wYXJhbXMuaWQ7XHJcbiAgICAgICAgICAgICQuYWpheHN5cyh7XHJcbiAgICAgICAgICAgICAgICB1cmw6IFwiL2FwaS9wYXJ0bmVyL3NldC9cIixcclxuICAgICAgICAgICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICBvYmo6IEpTT04uc3RyaW5naWZ5KGopLFxyXG4gICAgICAgICAgICAgICAgICAgIFwiYmFzZTY0X2ltZ1wiOiBzZWxmLnJlZnMuaW1nLnZhbCgpXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICAkLnRvYXN0KHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGV4dDogXCLYqtmFINin2YTYrdmB2LhcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWNvbjogJ3N1Y2Nlc3MnXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5vcHRzLnBhZ2UoJy9hcHAvcGFydG5lci9saXN0Jyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIDwvc2NyaXB0PlxyXG48L3BhcnRuZXItaXRlbT4iLCI8cGFydG5lci1saXN0PlxyXG4gICAgPGRpdiBjbGFzcz1cImNhcmRcIj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiY2FyZC1ib2R5XCI+XHJcbiAgICAgICAgICAgIDxoNCBjbGFzcz1cImNhcmQtdGl0bGVcIj5cclxuICAgICAgICAgICAgICAgIDxzcGFuPtmC2KfYptmF2Kkg2KfZhNi02LHZg9in2KE8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8YSBocmVmPVwiIy9hcHAvcGFydG5lci9pdGVtLy0xXCIgY2xhc3M9XCJidG4gYnRuLWluZm8gYnRuLXhzIGZsb2F0LWxlZnRcIj4g2KzYr9mK2K88L2E+XHJcbiAgICAgICAgICAgIDwvaDQ+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtMTIgXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRhYmxlIHJlZj1cIm15dGFibGVcIiBjbGFzcz1cInRhYmxlIHRhYmxlLWhvdmVyICB0YWJsZS1ib3JkZXJlZFwiPjwvdGFibGU+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICAgIDxzY3JpcHQ+XHJcbiAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICAgIHNlbGYub24oJ21vdW50JywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAkKHNlbGYucmVmcy5teXRhYmxlKS5EYXRhVGFibGUoe1xyXG4gICAgICAgICAgICAgICAgXCJhamF4XCI6IHtcclxuICAgICAgICAgICAgICAgICAgICB1cmw6ICcvYXBpL3BhcnRuZXIvZ2V0LycsXHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYmo6IEpTT04uc3RyaW5naWZ5KHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFhYTogMTFcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgXCJjb2x1bW5zXCI6IFt7XHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IFwi2KfZhNin2LPZhVwiLFxyXG4gICAgICAgICAgICAgICAgICAgIG9yZGVyYWJsZTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgcmVuZGVyOiBmdW5jdGlvbiAoZGF0YSwgdHlwZSwgZnVsbCwgbWV0YSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gSGFuZGxlYmFycy5jb21waWxlKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYFxyXG48YSBocmVmPScjL2FwcC9wYXJ0bmVyL2l0ZW0ve3tpZH19Jz57e25hbWV9fTwvYT5gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICkoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmdWxsKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IFwi2LnYr9ivINin2YTZh9iv2KfZitinXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgb3JkZXJhYmxlOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICBkYXRhOiAnZ2lmdGNvdW50J1xyXG4gICAgICAgICAgICAgICAgfV1cclxuICAgICAgICAgICAgfSlcclxuICAgICAgICB9KVxyXG4gICAgPC9zY3JpcHQ+XHJcbjwvcGFydG5lci1saXN0PiIsIiBpbXBvcnQgXCIuL2FwcC5odG1sXCI7XHJcbiBpbXBvcnQgXCIuL2hvbWUuaHRtbFwiO1xyXG4gaW1wb3J0IFwiLi9sb2dpbi5odG1sXCI7XHJcbiBpbXBvcnQgXCIuL2xvZ291dC5odG1sXCI7XHJcblxyXG4gaW1wb3J0IFwiLi9jb25zdWx0YXRpb24taXRlbS5odG1sXCI7XHJcbiBpbXBvcnQgXCIuL2NvbnN1bHRhdGlvbi1saXN0Lmh0bWxcIjtcclxuIGltcG9ydCBcIi4vZ2lmdC1pdGVtLmh0bWxcIjtcclxuIGltcG9ydCBcIi4vZ2lmdC1saXN0Lmh0bWxcIjtcclxuXHJcbiBpbXBvcnQgXCIuL3BhcnRuZXItaXRlbS5odG1sXCI7XHJcbiBpbXBvcnQgXCIuL3BhcnRuZXItbGlzdC5odG1sXCI7Il19
