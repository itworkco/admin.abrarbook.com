(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
/* Riot v3.11.1, @license MIT */
(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (factory((global.riot = {})));
}(this, (function (exports) { 'use strict';

  /**
   * Shorter and fast way to select a single node in the DOM
   * @param   { String } selector - unique dom selector
   * @param   { Object } ctx - DOM node where the target of our search will is located
   * @returns { Object } dom node found
   */
  function $(selector, ctx) {
    return (ctx || document).querySelector(selector)
  }

  var
    // be aware, internal usage
    // ATTENTION: prefix the global dynamic variables with `__`
    // tags instances cache
    __TAGS_CACHE = [],
    // tags implementation cache
    __TAG_IMPL = {},
    YIELD_TAG = 'yield',

    /**
     * Const
     */
    GLOBAL_MIXIN = '__global_mixin',

    // riot specific prefixes or attributes
    ATTRS_PREFIX = 'riot-',

    // Riot Directives
    REF_DIRECTIVES = ['ref', 'data-ref'],
    IS_DIRECTIVE = 'data-is',
    CONDITIONAL_DIRECTIVE = 'if',
    LOOP_DIRECTIVE = 'each',
    LOOP_NO_REORDER_DIRECTIVE = 'no-reorder',
    SHOW_DIRECTIVE = 'show',
    HIDE_DIRECTIVE = 'hide',
    KEY_DIRECTIVE = 'key',
    RIOT_EVENTS_KEY = '__riot-events__',

    // for typeof == '' comparisons
    T_STRING = 'string',
    T_OBJECT = 'object',
    T_UNDEF  = 'undefined',
    T_FUNCTION = 'function',

    XLINK_NS = 'http://www.w3.org/1999/xlink',
    SVG_NS = 'http://www.w3.org/2000/svg',
    XLINK_REGEX = /^xlink:(\w+)/,

    WIN = typeof window === T_UNDEF ? /* istanbul ignore next */ undefined : window,

    // special native tags that cannot be treated like the others
    RE_SPECIAL_TAGS = /^(?:t(?:body|head|foot|[rhd])|caption|col(?:group)?|opt(?:ion|group))$/,
    RE_SPECIAL_TAGS_NO_OPTION = /^(?:t(?:body|head|foot|[rhd])|caption|col(?:group)?)$/,
    RE_EVENTS_PREFIX = /^on/,
    RE_HTML_ATTRS = /([-\w]+) ?= ?(?:"([^"]*)|'([^']*)|({[^}]*}))/g,
    // some DOM attributes must be normalized
    CASE_SENSITIVE_ATTRIBUTES = {
      'viewbox': 'viewBox',
      'preserveaspectratio': 'preserveAspectRatio'
    },
    /**
     * Matches boolean HTML attributes in the riot tag definition.
     * With a long list like this, a regex is faster than `[].indexOf` in most browsers.
     * @const {RegExp}
     * @see [attributes.md](https://github.com/riot/compiler/blob/dev/doc/attributes.md)
     */
    RE_BOOL_ATTRS = /^(?:disabled|checked|readonly|required|allowfullscreen|auto(?:focus|play)|compact|controls|default|formnovalidate|hidden|ismap|itemscope|loop|multiple|muted|no(?:resize|shade|validate|wrap)?|open|reversed|seamless|selected|sortable|truespeed|typemustmatch)$/,
    // version# for IE 8-11, 0 for others
    IE_VERSION = (WIN && WIN.document || /* istanbul ignore next */ {}).documentMode | 0;

  /**
   * Create a generic DOM node
   * @param   { String } name - name of the DOM node we want to create
   * @returns { Object } DOM node just created
   */
  function makeElement(name) {
    return name === 'svg' ? document.createElementNS(SVG_NS, name) : document.createElement(name)
  }

  /**
   * Set any DOM attribute
   * @param { Object } dom - DOM node we want to update
   * @param { String } name - name of the property we want to set
   * @param { String } val - value of the property we want to set
   */
  function setAttribute(dom, name, val) {
    var xlink = XLINK_REGEX.exec(name);
    if (xlink && xlink[1])
      { dom.setAttributeNS(XLINK_NS, xlink[1], val); }
    else
      { dom.setAttribute(name, val); }
  }

  var styleNode;
  // Create cache and shortcut to the correct property
  var cssTextProp;
  var byName = {};
  var needsInject = false;

  // skip the following code on the server
  if (WIN) {
    styleNode = ((function () {
      // create a new style element with the correct type
      var newNode = makeElement('style');
      // replace any user node or insert the new one into the head
      var userNode = $('style[type=riot]');

      setAttribute(newNode, 'type', 'text/css');
      /* istanbul ignore next */
      if (userNode) {
        if (userNode.id) { newNode.id = userNode.id; }
        userNode.parentNode.replaceChild(newNode, userNode);
      } else { document.head.appendChild(newNode); }

      return newNode
    }))();
    cssTextProp = styleNode.styleSheet;
  }

  /**
   * Object that will be used to inject and manage the css of every tag instance
   */
  var styleManager = {
    styleNode: styleNode,
    /**
     * Save a tag style to be later injected into DOM
     * @param { String } css - css string
     * @param { String } name - if it's passed we will map the css to a tagname
     */
    add: function add(css, name) {
      byName[name] = css;
      needsInject = true;
    },
    /**
     * Inject all previously saved tag styles into DOM
     * innerHTML seems slow: http://jsperf.com/riot-insert-style
     */
    inject: function inject() {
      if (!WIN || !needsInject) { return }
      needsInject = false;
      var style = Object.keys(byName)
        .map(function (k) { return byName[k]; })
        .join('\n');
      /* istanbul ignore next */
      if (cssTextProp) { cssTextProp.cssText = style; }
      else { styleNode.innerHTML = style; }
    },

    /**
     * Remove a tag style of injected DOM later.
     * @param {String} name a registered tagname
     */
    remove: function remove(name) {
      delete byName[name];
      needsInject = true;
    }
  }

  /**
   * The riot template engine
   * @version v3.0.8
   */

  /* istanbul ignore next */
  var skipRegex = (function () { //eslint-disable-line no-unused-vars

    var beforeReChars = '[{(,;:?=|&!^~>%*/';

    var beforeReWords = [
      'case',
      'default',
      'do',
      'else',
      'in',
      'instanceof',
      'prefix',
      'return',
      'typeof',
      'void',
      'yield'
    ];

    var wordsLastChar = beforeReWords.reduce(function (s, w) {
      return s + w.slice(-1)
    }, '');

    var RE_REGEX = /^\/(?=[^*>/])[^[/\\]*(?:(?:\\.|\[(?:\\.|[^\]\\]*)*\])[^[\\/]*)*?\/[gimuy]*/;
    var RE_VN_CHAR = /[$\w]/;

    function prev (code, pos) {
      while (--pos >= 0 && /\s/.test(code[pos])){ }
      return pos
    }

    function _skipRegex (code, start) {

      var re = /.*/g;
      var pos = re.lastIndex = start++;
      var match = re.exec(code)[0].match(RE_REGEX);

      if (match) {
        var next = pos + match[0].length;

        pos = prev(code, pos);
        var c = code[pos];

        if (pos < 0 || ~beforeReChars.indexOf(c)) {
          return next
        }

        if (c === '.') {

          if (code[pos - 1] === '.') {
            start = next;
          }

        } else if (c === '+' || c === '-') {

          if (code[--pos] !== c ||
              (pos = prev(code, pos)) < 0 ||
              !RE_VN_CHAR.test(code[pos])) {
            start = next;
          }

        } else if (~wordsLastChar.indexOf(c)) {

          var end = pos + 1;

          while (--pos >= 0 && RE_VN_CHAR.test(code[pos])){ }
          if (~beforeReWords.indexOf(code.slice(pos + 1, end))) {
            start = next;
          }
        }
      }

      return start
    }

    return _skipRegex

  })();

  /**
   * riot.util.brackets
   *
   * - `brackets    ` - Returns a string or regex based on its parameter
   * - `brackets.set` - Change the current riot brackets
   *
   * @module
   */

  /* global riot */

  /* istanbul ignore next */
  var brackets = (function (UNDEF) {

    var
      REGLOB = 'g',

      R_MLCOMMS = /\/\*[^*]*\*+(?:[^*\/][^*]*\*+)*\//g,

      R_STRINGS = /"[^"\\]*(?:\\[\S\s][^"\\]*)*"|'[^'\\]*(?:\\[\S\s][^'\\]*)*'|`[^`\\]*(?:\\[\S\s][^`\\]*)*`/g,

      S_QBLOCKS = R_STRINGS.source + '|' +
        /(?:\breturn\s+|(?:[$\w\)\]]|\+\+|--)\s*(\/)(?![*\/]))/.source + '|' +
        /\/(?=[^*\/])[^[\/\\]*(?:(?:\[(?:\\.|[^\]\\]*)*\]|\\.)[^[\/\\]*)*?([^<]\/)[gim]*/.source,

      UNSUPPORTED = RegExp('[\\' + 'x00-\\x1F<>a-zA-Z0-9\'",;\\\\]'),

      NEED_ESCAPE = /(?=[[\]()*+?.^$|])/g,

      S_QBLOCK2 = R_STRINGS.source + '|' + /(\/)(?![*\/])/.source,

      FINDBRACES = {
        '(': RegExp('([()])|'   + S_QBLOCK2, REGLOB),
        '[': RegExp('([[\\]])|' + S_QBLOCK2, REGLOB),
        '{': RegExp('([{}])|'   + S_QBLOCK2, REGLOB)
      },

      DEFAULT = '{ }';

    var _pairs = [
      '{', '}',
      '{', '}',
      /{[^}]*}/,
      /\\([{}])/g,
      /\\({)|{/g,
      RegExp('\\\\(})|([[({])|(})|' + S_QBLOCK2, REGLOB),
      DEFAULT,
      /^\s*{\^?\s*([$\w]+)(?:\s*,\s*(\S+))?\s+in\s+(\S.*)\s*}/,
      /(^|[^\\]){=[\S\s]*?}/
    ];

    var
      cachedBrackets = UNDEF,
      _regex,
      _cache = [],
      _settings;

    function _loopback (re) { return re }

    function _rewrite (re, bp) {
      if (!bp) { bp = _cache; }
      return new RegExp(
        re.source.replace(/{/g, bp[2]).replace(/}/g, bp[3]), re.global ? REGLOB : ''
      )
    }

    function _create (pair) {
      if (pair === DEFAULT) { return _pairs }

      var arr = pair.split(' ');

      if (arr.length !== 2 || UNSUPPORTED.test(pair)) {
        throw new Error('Unsupported brackets "' + pair + '"')
      }
      arr = arr.concat(pair.replace(NEED_ESCAPE, '\\').split(' '));

      arr[4] = _rewrite(arr[1].length > 1 ? /{[\S\s]*?}/ : _pairs[4], arr);
      arr[5] = _rewrite(pair.length > 3 ? /\\({|})/g : _pairs[5], arr);
      arr[6] = _rewrite(_pairs[6], arr);
      arr[7] = RegExp('\\\\(' + arr[3] + ')|([[({])|(' + arr[3] + ')|' + S_QBLOCK2, REGLOB);
      arr[8] = pair;
      return arr
    }

    function _brackets (reOrIdx) {
      return reOrIdx instanceof RegExp ? _regex(reOrIdx) : _cache[reOrIdx]
    }

    _brackets.split = function split (str, tmpl, _bp) {
      // istanbul ignore next: _bp is for the compiler
      if (!_bp) { _bp = _cache; }

      var
        parts = [],
        match,
        isexpr,
        start,
        pos,
        re = _bp[6];

      var qblocks = [];
      var prevStr = '';
      var mark, lastIndex;

      isexpr = start = re.lastIndex = 0;

      while ((match = re.exec(str))) {

        lastIndex = re.lastIndex;
        pos = match.index;

        if (isexpr) {

          if (match[2]) {

            var ch = match[2];
            var rech = FINDBRACES[ch];
            var ix = 1;

            rech.lastIndex = lastIndex;
            while ((match = rech.exec(str))) {
              if (match[1]) {
                if (match[1] === ch) { ++ix; }
                else if (!--ix) { break }
              } else {
                rech.lastIndex = pushQBlock(match.index, rech.lastIndex, match[2]);
              }
            }
            re.lastIndex = ix ? str.length : rech.lastIndex;
            continue
          }

          if (!match[3]) {
            re.lastIndex = pushQBlock(pos, lastIndex, match[4]);
            continue
          }
        }

        if (!match[1]) {
          unescapeStr(str.slice(start, pos));
          start = re.lastIndex;
          re = _bp[6 + (isexpr ^= 1)];
          re.lastIndex = start;
        }
      }

      if (str && start < str.length) {
        unescapeStr(str.slice(start));
      }

      parts.qblocks = qblocks;

      return parts

      function unescapeStr (s) {
        if (prevStr) {
          s = prevStr + s;
          prevStr = '';
        }
        if (tmpl || isexpr) {
          parts.push(s && s.replace(_bp[5], '$1'));
        } else {
          parts.push(s);
        }
      }

      function pushQBlock(_pos, _lastIndex, slash) { //eslint-disable-line
        if (slash) {
          _lastIndex = skipRegex(str, _pos);
        }

        if (tmpl && _lastIndex > _pos + 2) {
          mark = '\u2057' + qblocks.length + '~';
          qblocks.push(str.slice(_pos, _lastIndex));
          prevStr += str.slice(start, _pos) + mark;
          start = _lastIndex;
        }
        return _lastIndex
      }
    };

    _brackets.hasExpr = function hasExpr (str) {
      return _cache[4].test(str)
    };

    _brackets.loopKeys = function loopKeys (expr) {
      var m = expr.match(_cache[9]);

      return m
        ? { key: m[1], pos: m[2], val: _cache[0] + m[3].trim() + _cache[1] }
        : { val: expr.trim() }
    };

    _brackets.array = function array (pair) {
      return pair ? _create(pair) : _cache
    };

    function _reset (pair) {
      if ((pair || (pair = DEFAULT)) !== _cache[8]) {
        _cache = _create(pair);
        _regex = pair === DEFAULT ? _loopback : _rewrite;
        _cache[9] = _regex(_pairs[9]);
      }
      cachedBrackets = pair;
    }

    function _setSettings (o) {
      var b;

      o = o || {};
      b = o.brackets;
      Object.defineProperty(o, 'brackets', {
        set: _reset,
        get: function () { return cachedBrackets },
        enumerable: true
      });
      _settings = o;
      _reset(b);
    }

    Object.defineProperty(_brackets, 'settings', {
      set: _setSettings,
      get: function () { return _settings }
    });

    /* istanbul ignore next: in the browser riot is always in the scope */
    _brackets.settings = typeof riot !== 'undefined' && riot.settings || {};
    _brackets.set = _reset;
    _brackets.skipRegex = skipRegex;

    _brackets.R_STRINGS = R_STRINGS;
    _brackets.R_MLCOMMS = R_MLCOMMS;
    _brackets.S_QBLOCKS = S_QBLOCKS;
    _brackets.S_QBLOCK2 = S_QBLOCK2;

    return _brackets

  })();

  /**
   * @module tmpl
   *
   * tmpl          - Root function, returns the template value, render with data
   * tmpl.hasExpr  - Test the existence of a expression inside a string
   * tmpl.loopKeys - Get the keys for an 'each' loop (used by `_each`)
   */

  /* istanbul ignore next */
  var tmpl = (function () {

    var _cache = {};

    function _tmpl (str, data) {
      if (!str) { return str }

      return (_cache[str] || (_cache[str] = _create(str))).call(
        data, _logErr.bind({
          data: data,
          tmpl: str
        })
      )
    }

    _tmpl.hasExpr = brackets.hasExpr;

    _tmpl.loopKeys = brackets.loopKeys;

    // istanbul ignore next
    _tmpl.clearCache = function () { _cache = {}; };

    _tmpl.errorHandler = null;

    function _logErr (err, ctx) {

      err.riotData = {
        tagName: ctx && ctx.__ && ctx.__.tagName,
        _riot_id: ctx && ctx._riot_id  //eslint-disable-line camelcase
      };

      if (_tmpl.errorHandler) { _tmpl.errorHandler(err); }
      else if (
        typeof console !== 'undefined' &&
        typeof console.error === 'function'
      ) {
        console.error(err.message);
        console.log('<%s> %s', err.riotData.tagName || 'Unknown tag', this.tmpl); // eslint-disable-line
        console.log(this.data); // eslint-disable-line
      }
    }

    function _create (str) {
      var expr = _getTmpl(str);

      if (expr.slice(0, 11) !== 'try{return ') { expr = 'return ' + expr; }

      return new Function('E', expr + ';')    // eslint-disable-line no-new-func
    }

    var RE_DQUOTE = /\u2057/g;
    var RE_QBMARK = /\u2057(\d+)~/g;

    function _getTmpl (str) {
      var parts = brackets.split(str.replace(RE_DQUOTE, '"'), 1);
      var qstr = parts.qblocks;
      var expr;

      if (parts.length > 2 || parts[0]) {
        var i, j, list = [];

        for (i = j = 0; i < parts.length; ++i) {

          expr = parts[i];

          if (expr && (expr = i & 1

              ? _parseExpr(expr, 1, qstr)

              : '"' + expr
                  .replace(/\\/g, '\\\\')
                  .replace(/\r\n?|\n/g, '\\n')
                  .replace(/"/g, '\\"') +
                '"'

            )) { list[j++] = expr; }

        }

        expr = j < 2 ? list[0]
             : '[' + list.join(',') + '].join("")';

      } else {

        expr = _parseExpr(parts[1], 0, qstr);
      }

      if (qstr.length) {
        expr = expr.replace(RE_QBMARK, function (_, pos) {
          return qstr[pos]
            .replace(/\r/g, '\\r')
            .replace(/\n/g, '\\n')
        });
      }
      return expr
    }

    var RE_CSNAME = /^(?:(-?[_A-Za-z\xA0-\xFF][-\w\xA0-\xFF]*)|\u2057(\d+)~):/;
    var
      RE_BREND = {
        '(': /[()]/g,
        '[': /[[\]]/g,
        '{': /[{}]/g
      };

    function _parseExpr (expr, asText, qstr) {

      expr = expr
        .replace(/\s+/g, ' ').trim()
        .replace(/\ ?([[\({},?\.:])\ ?/g, '$1');

      if (expr) {
        var
          list = [],
          cnt = 0,
          match;

        while (expr &&
              (match = expr.match(RE_CSNAME)) &&
              !match.index
          ) {
          var
            key,
            jsb,
            re = /,|([[{(])|$/g;

          expr = RegExp.rightContext;
          key  = match[2] ? qstr[match[2]].slice(1, -1).trim().replace(/\s+/g, ' ') : match[1];

          while (jsb = (match = re.exec(expr))[1]) { skipBraces(jsb, re); }

          jsb  = expr.slice(0, match.index);
          expr = RegExp.rightContext;

          list[cnt++] = _wrapExpr(jsb, 1, key);
        }

        expr = !cnt ? _wrapExpr(expr, asText)
             : cnt > 1 ? '[' + list.join(',') + '].join(" ").trim()' : list[0];
      }
      return expr

      function skipBraces (ch, re) {
        var
          mm,
          lv = 1,
          ir = RE_BREND[ch];

        ir.lastIndex = re.lastIndex;
        while (mm = ir.exec(expr)) {
          if (mm[0] === ch) { ++lv; }
          else if (!--lv) { break }
        }
        re.lastIndex = lv ? expr.length : ir.lastIndex;
      }
    }

    // istanbul ignore next: not both
    var // eslint-disable-next-line max-len
      JS_CONTEXT = '"in this?this:' + (typeof window !== 'object' ? 'global' : 'window') + ').',
      JS_VARNAME = /[,{][\$\w]+(?=:)|(^ *|[^$\w\.{])(?!(?:typeof|true|false|null|undefined|in|instanceof|is(?:Finite|NaN)|void|NaN|new|Date|RegExp|Math)(?![$\w]))([$_A-Za-z][$\w]*)/g,
      JS_NOPROPS = /^(?=(\.[$\w]+))\1(?:[^.[(]|$)/;

    function _wrapExpr (expr, asText, key) {
      var tb;

      expr = expr.replace(JS_VARNAME, function (match, p, mvar, pos, s) {
        if (mvar) {
          pos = tb ? 0 : pos + match.length;

          if (mvar !== 'this' && mvar !== 'global' && mvar !== 'window') {
            match = p + '("' + mvar + JS_CONTEXT + mvar;
            if (pos) { tb = (s = s[pos]) === '.' || s === '(' || s === '['; }
          } else if (pos) {
            tb = !JS_NOPROPS.test(s.slice(pos));
          }
        }
        return match
      });

      if (tb) {
        expr = 'try{return ' + expr + '}catch(e){E(e,this)}';
      }

      if (key) {

        expr = (tb
            ? 'function(){' + expr + '}.call(this)' : '(' + expr + ')'
          ) + '?"' + key + '":""';

      } else if (asText) {

        expr = 'function(v){' + (tb
            ? expr.replace('return ', 'v=') : 'v=(' + expr + ')'
          ) + ';return v||v===0?v:""}.call(this)';
      }

      return expr
    }

    _tmpl.version = brackets.version = 'v3.0.8';

    return _tmpl

  })();

  /* istanbul ignore next */
  var observable = function(el) {

    /**
     * Extend the original object or create a new empty one
     * @type { Object }
     */

    el = el || {};

    /**
     * Private variables
     */
    var callbacks = {},
      slice = Array.prototype.slice;

    /**
     * Public Api
     */

    // extend the el object adding the observable methods
    Object.defineProperties(el, {
      /**
       * Listen to the given `event` ands
       * execute the `callback` each time an event is triggered.
       * @param  { String } event - event id
       * @param  { Function } fn - callback function
       * @returns { Object } el
       */
      on: {
        value: function(event, fn) {
          if (typeof fn == 'function')
            { (callbacks[event] = callbacks[event] || []).push(fn); }
          return el
        },
        enumerable: false,
        writable: false,
        configurable: false
      },

      /**
       * Removes the given `event` listeners
       * @param   { String } event - event id
       * @param   { Function } fn - callback function
       * @returns { Object } el
       */
      off: {
        value: function(event, fn) {
          if (event == '*' && !fn) { callbacks = {}; }
          else {
            if (fn) {
              var arr = callbacks[event];
              for (var i = 0, cb; cb = arr && arr[i]; ++i) {
                if (cb == fn) { arr.splice(i--, 1); }
              }
            } else { delete callbacks[event]; }
          }
          return el
        },
        enumerable: false,
        writable: false,
        configurable: false
      },

      /**
       * Listen to the given `event` and
       * execute the `callback` at most once
       * @param   { String } event - event id
       * @param   { Function } fn - callback function
       * @returns { Object } el
       */
      one: {
        value: function(event, fn) {
          function on() {
            el.off(event, on);
            fn.apply(el, arguments);
          }
          return el.on(event, on)
        },
        enumerable: false,
        writable: false,
        configurable: false
      },

      /**
       * Execute all callback functions that listen to
       * the given `event`
       * @param   { String } event - event id
       * @returns { Object } el
       */
      trigger: {
        value: function(event) {
          var arguments$1 = arguments;


          // getting the arguments
          var arglen = arguments.length - 1,
            args = new Array(arglen),
            fns,
            fn,
            i;

          for (i = 0; i < arglen; i++) {
            args[i] = arguments$1[i + 1]; // skip first argument
          }

          fns = slice.call(callbacks[event] || [], 0);

          for (i = 0; fn = fns[i]; ++i) {
            fn.apply(el, args);
          }

          if (callbacks['*'] && event != '*')
            { el.trigger.apply(el, ['*', event].concat(args)); }

          return el
        },
        enumerable: false,
        writable: false,
        configurable: false
      }
    });

    return el

  };

  /**
   * Short alias for Object.getOwnPropertyDescriptor
   */
  function getPropDescriptor (o, k) {
    return Object.getOwnPropertyDescriptor(o, k)
  }

  /**
   * Check if passed argument is undefined
   * @param   { * } value -
   * @returns { Boolean } -
   */
  function isUndefined(value) {
    return typeof value === T_UNDEF
  }

  /**
   * Check whether object's property could be overridden
   * @param   { Object }  obj - source object
   * @param   { String }  key - object property
   * @returns { Boolean } true if writable
   */
  function isWritable(obj, key) {
    var descriptor = getPropDescriptor(obj, key);
    return isUndefined(obj[key]) || descriptor && descriptor.writable
  }

  /**
   * Extend any object with other properties
   * @param   { Object } src - source object
   * @returns { Object } the resulting extended object
   *
   * var obj = { foo: 'baz' }
   * extend(obj, {bar: 'bar', foo: 'bar'})
   * console.log(obj) => {bar: 'bar', foo: 'bar'}
   *
   */
  function extend(src) {
    var obj;
    var i = 1;
    var args = arguments;
    var l = args.length;

    for (; i < l; i++) {
      if (obj = args[i]) {
        for (var key in obj) {
          // check if this property of the source object could be overridden
          if (isWritable(src, key))
            { src[key] = obj[key]; }
        }
      }
    }
    return src
  }

  /**
   * Alias for Object.create
   */
  function create(src) {
    return Object.create(src)
  }

  var settings = extend(create(brackets.settings), {
    skipAnonymousTags: true,
    // handle the auto updates on any DOM event
    autoUpdate: true
  })

  /**
   * Shorter and fast way to select multiple nodes in the DOM
   * @param   { String } selector - DOM selector
   * @param   { Object } ctx - DOM node where the targets of our search will is located
   * @returns { Object } dom nodes found
   */
  function $$(selector, ctx) {
    return [].slice.call((ctx || document).querySelectorAll(selector))
  }

  /**
   * Create a document text node
   * @returns { Object } create a text node to use as placeholder
   */
  function createDOMPlaceholder() {
    return document.createTextNode('')
  }

  /**
   * Toggle the visibility of any DOM node
   * @param   { Object }  dom - DOM node we want to hide
   * @param   { Boolean } show - do we want to show it?
   */

  function toggleVisibility(dom, show) {
    dom.style.display = show ? '' : 'none';
    dom.hidden = show ? false : true;
  }

  /**
   * Get the value of any DOM attribute on a node
   * @param   { Object } dom - DOM node we want to parse
   * @param   { String } name - name of the attribute we want to get
   * @returns { String | undefined } name of the node attribute whether it exists
   */
  function getAttribute(dom, name) {
    return dom.getAttribute(name)
  }

  /**
   * Remove any DOM attribute from a node
   * @param   { Object } dom - DOM node we want to update
   * @param   { String } name - name of the property we want to remove
   */
  function removeAttribute(dom, name) {
    dom.removeAttribute(name);
  }

  /**
   * Set the inner html of any DOM node SVGs included
   * @param { Object } container - DOM node where we'll inject new html
   * @param { String } html - html to inject
   * @param { Boolean } isSvg - svg tags should be treated a bit differently
   */
  /* istanbul ignore next */
  function setInnerHTML(container, html, isSvg) {
    // innerHTML is not supported on svg tags so we neet to treat them differently
    if (isSvg) {
      var node = container.ownerDocument.importNode(
        new DOMParser()
          .parseFromString(("<svg xmlns=\"" + SVG_NS + "\">" + html + "</svg>"), 'application/xml')
          .documentElement,
        true
      );

      container.appendChild(node);
    } else {
      container.innerHTML = html;
    }
  }

  /**
   * Minimize risk: only zero or one _space_ between attr & value
   * @param   { String }   html - html string we want to parse
   * @param   { Function } fn - callback function to apply on any attribute found
   */
  function walkAttributes(html, fn) {
    if (!html) { return }
    var m;
    while (m = RE_HTML_ATTRS.exec(html))
      { fn(m[1].toLowerCase(), m[2] || m[3] || m[4]); }
  }

  /**
   * Create a document fragment
   * @returns { Object } document fragment
   */
  function createFragment() {
    return document.createDocumentFragment()
  }

  /**
   * Insert safely a tag to fix #1962 #1649
   * @param   { HTMLElement } root - children container
   * @param   { HTMLElement } curr - node to insert
   * @param   { HTMLElement } next - node that should preceed the current node inserted
   */
  function safeInsert(root, curr, next) {
    root.insertBefore(curr, next.parentNode && next);
  }

  /**
   * Convert a style object to a string
   * @param   { Object } style - style object we need to parse
   * @returns { String } resulting css string
   * @example
   * styleObjectToString({ color: 'red', height: '10px'}) // => 'color: red; height: 10px'
   */
  function styleObjectToString(style) {
    return Object.keys(style).reduce(function (acc, prop) {
      return (acc + " " + prop + ": " + (style[prop]) + ";")
    }, '')
  }

  /**
   * Walk down recursively all the children tags starting dom node
   * @param   { Object }   dom - starting node where we will start the recursion
   * @param   { Function } fn - callback to transform the child node just found
   * @param   { Object }   context - fn can optionally return an object, which is passed to children
   */
  function walkNodes(dom, fn, context) {
    if (dom) {
      var res = fn(dom, context);
      var next;
      // stop the recursion
      if (res === false) { return }

      dom = dom.firstChild;

      while (dom) {
        next = dom.nextSibling;
        walkNodes(dom, fn, res);
        dom = next;
      }
    }
  }



  var dom = /*#__PURE__*/Object.freeze({
    $$: $$,
    $: $,
    createDOMPlaceholder: createDOMPlaceholder,
    mkEl: makeElement,
    setAttr: setAttribute,
    toggleVisibility: toggleVisibility,
    getAttr: getAttribute,
    remAttr: removeAttribute,
    setInnerHTML: setInnerHTML,
    walkAttrs: walkAttributes,
    createFrag: createFragment,
    safeInsert: safeInsert,
    styleObjectToString: styleObjectToString,
    walkNodes: walkNodes
  });

  /**
   * Check against the null and undefined values
   * @param   { * }  value -
   * @returns {Boolean} -
   */
  function isNil(value) {
    return isUndefined(value) || value === null
  }

  /**
   * Check if passed argument is empty. Different from falsy, because we dont consider 0 or false to be blank
   * @param { * } value -
   * @returns { Boolean } -
   */
  function isBlank(value) {
    return isNil(value) || value === ''
  }

  /**
   * Check if passed argument is a function
   * @param   { * } value -
   * @returns { Boolean } -
   */
  function isFunction(value) {
    return typeof value === T_FUNCTION
  }

  /**
   * Check if passed argument is an object, exclude null
   * NOTE: use isObject(x) && !isArray(x) to excludes arrays.
   * @param   { * } value -
   * @returns { Boolean } -
   */
  function isObject(value) {
    return value && typeof value === T_OBJECT // typeof null is 'object'
  }

  /**
   * Check if a DOM node is an svg tag or part of an svg
   * @param   { HTMLElement }  el - node we want to test
   * @returns {Boolean} true if it's an svg node
   */
  function isSvg(el) {
    var owner = el.ownerSVGElement;
    return !!owner || owner === null
  }

  /**
   * Check if passed argument is a kind of array
   * @param   { * } value -
   * @returns { Boolean } -
   */
  function isArray(value) {
    return Array.isArray(value) || value instanceof Array
  }

  /**
   * Check if the passed argument is a boolean attribute
   * @param   { String } value -
   * @returns { Boolean } -
   */
  function isBoolAttr(value) {
    return RE_BOOL_ATTRS.test(value)
  }

  /**
   * Check if passed argument is a string
   * @param   { * } value -
   * @returns { Boolean } -
   */
  function isString(value) {
    return typeof value === T_STRING
  }



  var check = /*#__PURE__*/Object.freeze({
    isBlank: isBlank,
    isFunction: isFunction,
    isObject: isObject,
    isSvg: isSvg,
    isWritable: isWritable,
    isArray: isArray,
    isBoolAttr: isBoolAttr,
    isNil: isNil,
    isString: isString,
    isUndefined: isUndefined
  });

  /**
   * Check whether an array contains an item
   * @param   { Array } array - target array
   * @param   { * } item - item to test
   * @returns { Boolean } -
   */
  function contains(array, item) {
    return array.indexOf(item) !== -1
  }

  /**
   * Specialized function for looping an array-like collection with `each={}`
   * @param   { Array } list - collection of items
   * @param   {Function} fn - callback function
   * @returns { Array } the array looped
   */
  function each(list, fn) {
    var len = list ? list.length : 0;
    var i = 0;
    for (; i < len; i++) { fn(list[i], i); }
    return list
  }

  /**
   * Faster String startsWith alternative
   * @param   { String } str - source string
   * @param   { String } value - test string
   * @returns { Boolean } -
   */
  function startsWith(str, value) {
    return str.slice(0, value.length) === value
  }

  /**
   * Function returning always a unique identifier
   * @returns { Number } - number from 0...n
   */
  var uid = (function uid() {
    var i = -1;
    return function () { return ++i; }
  })()

  /**
   * Helper function to set an immutable property
   * @param   { Object } el - object where the new property will be set
   * @param   { String } key - object key where the new property will be stored
   * @param   { * } value - value of the new property
   * @param   { Object } options - set the propery overriding the default options
   * @returns { Object } - the initial object
   */
  function define(el, key, value, options) {
    Object.defineProperty(el, key, extend({
      value: value,
      enumerable: false,
      writable: false,
      configurable: true
    }, options));
    return el
  }

  /**
   * Convert a string containing dashes to camel case
   * @param   { String } str - input string
   * @returns { String } my-string -> myString
   */
  function toCamel(str) {
    return str.replace(/-(\w)/g, function (_, c) { return c.toUpperCase(); })
  }

  /**
   * Warn a message via console
   * @param   {String} message - warning message
   */
  function warn(message) {
    if (console && console.warn) { console.warn(message); }
  }



  var misc = /*#__PURE__*/Object.freeze({
    contains: contains,
    each: each,
    getPropDescriptor: getPropDescriptor,
    startsWith: startsWith,
    uid: uid,
    defineProperty: define,
    objectCreate: create,
    extend: extend,
    toCamel: toCamel,
    warn: warn
  });

  /**
   * Set the property of an object for a given key. If something already
   * exists there, then it becomes an array containing both the old and new value.
   * @param { Object } obj - object on which to set the property
   * @param { String } key - property name
   * @param { Object } value - the value of the property to be set
   * @param { Boolean } ensureArray - ensure that the property remains an array
   * @param { Number } index - add the new item in a certain array position
   */
  function arrayishAdd(obj, key, value, ensureArray, index) {
    var dest = obj[key];
    var isArr = isArray(dest);
    var hasIndex = !isUndefined(index);

    if (dest && dest === value) { return }

    // if the key was never set, set it once
    if (!dest && ensureArray) { obj[key] = [value]; }
    else if (!dest) { obj[key] = value; }
    // if it was an array and not yet set
    else {
      if (isArr) {
        var oldIndex = dest.indexOf(value);
        // this item never changed its position
        if (oldIndex === index) { return }
        // remove the item from its old position
        if (oldIndex !== -1) { dest.splice(oldIndex, 1); }
        // move or add the item
        if (hasIndex) {
          dest.splice(index, 0, value);
        } else {
          dest.push(value);
        }
      } else { obj[key] = [dest, value]; }
    }
  }

  /**
   * Detect the tag implementation by a DOM node
   * @param   { Object } dom - DOM node we need to parse to get its tag implementation
   * @returns { Object } it returns an object containing the implementation of a custom tag (template and boot function)
   */
  function get(dom) {
    return dom.tagName && __TAG_IMPL[getAttribute(dom, IS_DIRECTIVE) ||
      getAttribute(dom, IS_DIRECTIVE) || dom.tagName.toLowerCase()]
  }

  /**
   * Get the tag name of any DOM node
   * @param   { Object } dom - DOM node we want to parse
   * @param   { Boolean } skipDataIs - hack to ignore the data-is attribute when attaching to parent
   * @returns { String } name to identify this dom node in riot
   */
  function getName(dom, skipDataIs) {
    var child = get(dom);
    var namedTag = !skipDataIs && getAttribute(dom, IS_DIRECTIVE);
    return namedTag && !tmpl.hasExpr(namedTag) ?
      namedTag : child ? child.name : dom.tagName.toLowerCase()
  }

  /**
   * Return a temporary context containing also the parent properties
   * @this Tag
   * @param { Tag } - temporary tag context containing all the parent properties
   */
  function inheritParentProps() {
    if (this.parent) { return extend(create(this), this.parent) }
    return this
  }

  /*
    Includes hacks needed for the Internet Explorer version 9 and below
    See: http://kangax.github.io/compat-table/es5/#ie8
         http://codeplanet.io/dropping-ie8/
  */

  var
    reHasYield  = /<yield\b/i,
    reYieldAll  = /<yield\s*(?:\/>|>([\S\s]*?)<\/yield\s*>|>)/ig,
    reYieldSrc  = /<yield\s+to=['"]([^'">]*)['"]\s*>([\S\s]*?)<\/yield\s*>/ig,
    reYieldDest = /<yield\s+from=['"]?([-\w]+)['"]?\s*(?:\/>|>([\S\s]*?)<\/yield\s*>)/ig,
    rootEls = { tr: 'tbody', th: 'tr', td: 'tr', col: 'colgroup' },
    tblTags = IE_VERSION && IE_VERSION < 10 ? RE_SPECIAL_TAGS : RE_SPECIAL_TAGS_NO_OPTION,
    GENERIC = 'div',
    SVG = 'svg';


  /*
    Creates the root element for table or select child elements:
    tr/th/td/thead/tfoot/tbody/caption/col/colgroup/option/optgroup
  */
  function specialTags(el, tmpl, tagName) {

    var
      select = tagName[0] === 'o',
      parent = select ? 'select>' : 'table>';

    // trim() is important here, this ensures we don't have artifacts,
    // so we can check if we have only one element inside the parent
    el.innerHTML = '<' + parent + tmpl.trim() + '</' + parent;
    parent = el.firstChild;

    // returns the immediate parent if tr/th/td/col is the only element, if not
    // returns the whole tree, as this can include additional elements
    /* istanbul ignore next */
    if (select) {
      parent.selectedIndex = -1;  // for IE9, compatible w/current riot behavior
    } else {
      // avoids insertion of cointainer inside container (ex: tbody inside tbody)
      var tname = rootEls[tagName];
      if (tname && parent.childElementCount === 1) { parent = $(tname, parent); }
    }
    return parent
  }

  /*
    Replace the yield tag from any tag template with the innerHTML of the
    original tag in the page
  */
  function replaceYield(tmpl, html) {
    // do nothing if no yield
    if (!reHasYield.test(tmpl)) { return tmpl }

    // be careful with #1343 - string on the source having `$1`
    var src = {};

    html = html && html.replace(reYieldSrc, function (_, ref, text) {
      src[ref] = src[ref] || text;   // preserve first definition
      return ''
    }).trim();

    return tmpl
      .replace(reYieldDest, function (_, ref, def) {  // yield with from - to attrs
        return src[ref] || def || ''
      })
      .replace(reYieldAll, function (_, def) {        // yield without any "from"
        return html || def || ''
      })
  }

  /**
   * Creates a DOM element to wrap the given content. Normally an `DIV`, but can be
   * also a `TABLE`, `SELECT`, `TBODY`, `TR`, or `COLGROUP` element.
   *
   * @param   { String } tmpl  - The template coming from the custom tag definition
   * @param   { String } html - HTML content that comes from the DOM element where you
   *           will mount the tag, mostly the original tag in the page
   * @param   { Boolean } isSvg - true if the root node is an svg
   * @returns { HTMLElement } DOM element with _tmpl_ merged through `YIELD` with the _html_.
   */
  function mkdom(tmpl, html, isSvg) {
    var match   = tmpl && tmpl.match(/^\s*<([-\w]+)/);
    var  tagName = match && match[1].toLowerCase();
    var el = makeElement(isSvg ? SVG : GENERIC);

    // replace all the yield tags with the tag inner html
    tmpl = replaceYield(tmpl, html);

    /* istanbul ignore next */
    if (tblTags.test(tagName))
      { el = specialTags(el, tmpl, tagName); }
    else
      { setInnerHTML(el, tmpl, isSvg); }

    return el
  }

  var EVENT_ATTR_RE = /^on/;

  /**
   * True if the event attribute starts with 'on'
   * @param   { String } attribute - event attribute
   * @returns { Boolean }
   */
  function isEventAttribute(attribute) {
    return EVENT_ATTR_RE.test(attribute)
  }

  /**
   * Loop backward all the parents tree to detect the first custom parent tag
   * @param   { Object } tag - a Tag instance
   * @returns { Object } the instance of the first custom parent tag found
   */
  function getImmediateCustomParent(tag) {
    var ptag = tag;
    while (ptag.__.isAnonymous) {
      if (!ptag.parent) { break }
      ptag = ptag.parent;
    }
    return ptag
  }

  /**
   * Trigger DOM events
   * @param   { HTMLElement } dom - dom element target of the event
   * @param   { Function } handler - user function
   * @param   { Object } e - event object
   */
  function handleEvent(dom, handler, e) {
    var ptag = this.__.parent;
    var item = this.__.item;

    if (!item)
      { while (ptag && !item) {
        item = ptag.__.item;
        ptag = ptag.__.parent;
      } }

    // override the event properties
    /* istanbul ignore next */
    if (isWritable(e, 'currentTarget')) { e.currentTarget = dom; }
    /* istanbul ignore next */
    if (isWritable(e, 'target')) { e.target = e.srcElement; }
    /* istanbul ignore next */
    if (isWritable(e, 'which')) { e.which = e.charCode || e.keyCode; }

    e.item = item;

    handler.call(this, e);

    // avoid auto updates
    if (!settings.autoUpdate) { return }

    if (!e.preventUpdate) {
      var p = getImmediateCustomParent(this);
      // fixes #2083
      if (p.isMounted) { p.update(); }
    }
  }

  /**
   * Attach an event to a DOM node
   * @param { String } name - event name
   * @param { Function } handler - event callback
   * @param { Object } dom - dom node
   * @param { Tag } tag - tag instance
   */
  function setEventHandler(name, handler, dom, tag) {
    var eventName;
    var cb = handleEvent.bind(tag, dom, handler);

    // avoid to bind twice the same event
    // possible fix for #2332
    dom[name] = null;

    // normalize event name
    eventName = name.replace(RE_EVENTS_PREFIX, '');

    // cache the listener into the listeners array
    if (!contains(tag.__.listeners, dom)) { tag.__.listeners.push(dom); }
    if (!dom[RIOT_EVENTS_KEY]) { dom[RIOT_EVENTS_KEY] = {}; }
    if (dom[RIOT_EVENTS_KEY][name]) { dom.removeEventListener(eventName, dom[RIOT_EVENTS_KEY][name]); }

    dom[RIOT_EVENTS_KEY][name] = cb;
    dom.addEventListener(eventName, cb, false);
  }

  /**
   * Create a new child tag including it correctly into its parent
   * @param   { Object } child - child tag implementation
   * @param   { Object } opts - tag options containing the DOM node where the tag will be mounted
   * @param   { String } innerHTML - inner html of the child node
   * @param   { Object } parent - instance of the parent tag including the child custom tag
   * @returns { Object } instance of the new child tag just created
   */
  function initChild(child, opts, innerHTML, parent) {
    var tag = createTag(child, opts, innerHTML);
    var tagName = opts.tagName || getName(opts.root, true);
    var ptag = getImmediateCustomParent(parent);
    // fix for the parent attribute in the looped elements
    define(tag, 'parent', ptag);
    // store the real parent tag
    // in some cases this could be different from the custom parent tag
    // for example in nested loops
    tag.__.parent = parent;

    // add this tag to the custom parent tag
    arrayishAdd(ptag.tags, tagName, tag);

    // and also to the real parent tag
    if (ptag !== parent)
      { arrayishAdd(parent.tags, tagName, tag); }

    return tag
  }

  /**
   * Removes an item from an object at a given key. If the key points to an array,
   * then the item is just removed from the array.
   * @param { Object } obj - object on which to remove the property
   * @param { String } key - property name
   * @param { Object } value - the value of the property to be removed
   * @param { Boolean } ensureArray - ensure that the property remains an array
  */
  function arrayishRemove(obj, key, value, ensureArray) {
    if (isArray(obj[key])) {
      var index = obj[key].indexOf(value);
      if (index !== -1) { obj[key].splice(index, 1); }
      if (!obj[key].length) { delete obj[key]; }
      else if (obj[key].length === 1 && !ensureArray) { obj[key] = obj[key][0]; }
    } else if (obj[key] === value)
      { delete obj[key]; } // otherwise just delete the key
  }

  /**
   * Adds the elements for a virtual tag
   * @this Tag
   * @param { Node } src - the node that will do the inserting or appending
   * @param { Tag } target - only if inserting, insert before this tag's first child
   */
  function makeVirtual(src, target) {
    var this$1 = this;

    var head = createDOMPlaceholder();
    var tail = createDOMPlaceholder();
    var frag = createFragment();
    var sib;
    var el;

    this.root.insertBefore(head, this.root.firstChild);
    this.root.appendChild(tail);

    this.__.head = el = head;
    this.__.tail = tail;

    while (el) {
      sib = el.nextSibling;
      frag.appendChild(el);
      this$1.__.virts.push(el); // hold for unmounting
      el = sib;
    }

    if (target)
      { src.insertBefore(frag, target.__.head); }
    else
      { src.appendChild(frag); }
  }

  /**
   * makes a tag virtual and replaces a reference in the dom
   * @this Tag
   * @param { tag } the tag to make virtual
   * @param { ref } the dom reference location
   */
  function makeReplaceVirtual(tag, ref) {
    var frag = createFragment();
    makeVirtual.call(tag, frag);
    ref.parentNode.replaceChild(frag, ref);
  }

  /**
   * Update dynamically created data-is tags with changing expressions
   * @param { Object } expr - expression tag and expression info
   * @param { Tag }    parent - parent for tag creation
   * @param { String } tagName - tag implementation we want to use
   */
  function updateDataIs(expr, parent, tagName) {
    var tag = expr.tag || expr.dom._tag;
    var ref;

    var ref$1 = tag ? tag.__ : {};
    var head = ref$1.head;
    var isVirtual = expr.dom.tagName === 'VIRTUAL';

    if (tag && expr.tagName === tagName) {
      tag.update();
      return
    }

    // sync _parent to accommodate changing tagnames
    if (tag) {
      // need placeholder before unmount
      if(isVirtual) {
        ref = createDOMPlaceholder();
        head.parentNode.insertBefore(ref, head);
      }

      tag.unmount(true);
    }

    // unable to get the tag name
    if (!isString(tagName)) { return }

    expr.impl = __TAG_IMPL[tagName];

    // unknown implementation
    if (!expr.impl) { return }

    expr.tag = tag = initChild(
      expr.impl, {
        root: expr.dom,
        parent: parent,
        tagName: tagName
      },
      expr.dom.innerHTML,
      parent
    );

    each(expr.attrs, function (a) { return setAttribute(tag.root, a.name, a.value); });
    expr.tagName = tagName;
    tag.mount();

    // root exist first time, after use placeholder
    if (isVirtual) { makeReplaceVirtual(tag, ref || tag.root); }

    // parent is the placeholder tag, not the dynamic tag so clean up
    parent.__.onUnmount = function () {
      var delName = tag.opts.dataIs;
      arrayishRemove(tag.parent.tags, delName, tag);
      arrayishRemove(tag.__.parent.tags, delName, tag);
      tag.unmount();
    };
  }

  /**
   * Nomalize any attribute removing the "riot-" prefix
   * @param   { String } attrName - original attribute name
   * @returns { String } valid html attribute name
   */
  function normalizeAttrName(attrName) {
    if (!attrName) { return null }
    attrName = attrName.replace(ATTRS_PREFIX, '');
    if (CASE_SENSITIVE_ATTRIBUTES[attrName]) { attrName = CASE_SENSITIVE_ATTRIBUTES[attrName]; }
    return attrName
  }

  /**
   * Update on single tag expression
   * @this Tag
   * @param { Object } expr - expression logic
   * @returns { undefined }
   */
  function updateExpression(expr) {
    if (this.root && getAttribute(this.root,'virtualized')) { return }

    var dom = expr.dom;
    // remove the riot- prefix
    var attrName = normalizeAttrName(expr.attr);
    var isToggle = contains([SHOW_DIRECTIVE, HIDE_DIRECTIVE], attrName);
    var isVirtual = expr.root && expr.root.tagName === 'VIRTUAL';
    var ref = this.__;
    var isAnonymous = ref.isAnonymous;
    var parent = dom && (expr.parent || dom.parentNode);
    // detect the style attributes
    var isStyleAttr = attrName === 'style';
    var isClassAttr = attrName === 'class';

    var value;

    // if it's a tag we could totally skip the rest
    if (expr._riot_id) {
      if (expr.__.wasCreated) {
        expr.update();
      // if it hasn't been mounted yet, do that now.
      } else {
        expr.mount();
        if (isVirtual) {
          makeReplaceVirtual(expr, expr.root);
        }
      }
      return
    }

    // if this expression has the update method it means it can handle the DOM changes by itself
    if (expr.update) { return expr.update() }

    var context = isToggle && !isAnonymous ? inheritParentProps.call(this) : this;

    // ...it seems to be a simple expression so we try to calculate its value
    value = tmpl(expr.expr, context);

    var hasValue = !isBlank(value);
    var isObj = isObject(value);

    // convert the style/class objects to strings
    if (isObj) {
      if (isClassAttr) {
        value = tmpl(JSON.stringify(value), this);
      } else if (isStyleAttr) {
        value = styleObjectToString(value);
      }
    }

    // remove original attribute
    if (expr.attr && (!expr.wasParsedOnce || !hasValue || value === false)) {
      // remove either riot-* attributes or just the attribute name
      removeAttribute(dom, getAttribute(dom, expr.attr) ? expr.attr : attrName);
    }

    // for the boolean attributes we don't need the value
    // we can convert it to checked=true to checked=checked
    if (expr.bool) { value = value ? attrName : false; }
    if (expr.isRtag) { return updateDataIs(expr, this, value) }
    if (expr.wasParsedOnce && expr.value === value) { return }

    // update the expression value
    expr.value = value;
    expr.wasParsedOnce = true;

    // if the value is an object (and it's not a style or class attribute) we can not do much more with it
    if (isObj && !isClassAttr && !isStyleAttr && !isToggle) { return }
    // avoid to render undefined/null values
    if (!hasValue) { value = ''; }

    // textarea and text nodes have no attribute name
    if (!attrName) {
      // about #815 w/o replace: the browser converts the value to a string,
      // the comparison by "==" does too, but not in the server
      value += '';
      // test for parent avoids error with invalid assignment to nodeValue
      if (parent) {
        // cache the parent node because somehow it will become null on IE
        // on the next iteration
        expr.parent = parent;
        if (parent.tagName === 'TEXTAREA') {
          parent.value = value;                    // #1113
          if (!IE_VERSION) { dom.nodeValue = value; }  // #1625 IE throws here, nodeValue
        }                                         // will be available on 'updated'
        else { dom.nodeValue = value; }
      }
      return
    }

    switch (true) {
    // handle events binding
    case isFunction(value):
      if (isEventAttribute(attrName)) {
        setEventHandler(attrName, value, dom, this);
      }
      break
    // show / hide
    case isToggle:
      toggleVisibility(dom, attrName === HIDE_DIRECTIVE ? !value : value);
      break
    // handle attributes
    default:
      if (expr.bool) {
        dom[attrName] = value;
      }

      if (attrName === 'value' && dom.value !== value) {
        dom.value = value;
      } else if (hasValue && value !== false) {
        setAttribute(dom, attrName, value);
      }

      // make sure that in case of style changes
      // the element stays hidden
      if (isStyleAttr && dom.hidden) { toggleVisibility(dom, false); }
    }
  }

  /**
   * Update all the expressions in a Tag instance
   * @this Tag
   * @param { Array } expressions - expression that must be re evaluated
   */
  function update(expressions) {
    each(expressions, updateExpression.bind(this));
  }

  /**
   * We need to update opts for this tag. That requires updating the expressions
   * in any attributes on the tag, and then copying the result onto opts.
   * @this Tag
   * @param   {Boolean} isLoop - is it a loop tag?
   * @param   { Tag }  parent - parent tag node
   * @param   { Boolean }  isAnonymous - is it a tag without any impl? (a tag not registered)
   * @param   { Object }  opts - tag options
   * @param   { Array }  instAttrs - tag attributes array
   */
  function updateOpts(isLoop, parent, isAnonymous, opts, instAttrs) {
    // isAnonymous `each` tags treat `dom` and `root` differently. In this case
    // (and only this case) we don't need to do updateOpts, because the regular parse
    // will update those attrs. Plus, isAnonymous tags don't need opts anyway
    if (isLoop && isAnonymous) { return }
    var ctx = isLoop ? inheritParentProps.call(this) : parent || this;

    each(instAttrs, function (attr) {
      if (attr.expr) { updateExpression.call(ctx, attr.expr); }
      // normalize the attribute names
      opts[toCamel(attr.name).replace(ATTRS_PREFIX, '')] = attr.expr ? attr.expr.value : attr.value;
    });
  }

  /**
   * Update the tag expressions and options
   * @param { Tag } tag - tag object
   * @param { * } data - data we want to use to extend the tag properties
   * @param { Array } expressions - component expressions array
   * @returns { Tag } the current tag instance
   */
  function componentUpdate(tag, data, expressions) {
    var __ = tag.__;
    var nextOpts = {};
    var canTrigger = tag.isMounted && !__.skipAnonymous;

    // inherit properties from the parent tag
    if (__.isAnonymous && __.parent) { extend(tag, __.parent); }
    extend(tag, data);

    updateOpts.apply(tag, [__.isLoop, __.parent, __.isAnonymous, nextOpts, __.instAttrs]);

    if (
      canTrigger &&
      tag.isMounted &&
      isFunction(tag.shouldUpdate) && !tag.shouldUpdate(data, nextOpts)
    ) {
      return tag
    }

    extend(tag.opts, nextOpts);

    if (canTrigger) { tag.trigger('update', data); }
    update.call(tag, expressions);
    if (canTrigger) { tag.trigger('updated'); }

    return tag
  }

  /**
   * Get selectors for tags
   * @param   { Array } tags - tag names to select
   * @returns { String } selector
   */
  function query(tags) {
    // select all tags
    if (!tags) {
      var keys = Object.keys(__TAG_IMPL);
      return keys + query(keys)
    }

    return tags
      .filter(function (t) { return !/[^-\w]/.test(t); })
      .reduce(function (list, t) {
        var name = t.trim().toLowerCase();
        return list + ",[" + IS_DIRECTIVE + "=\"" + name + "\"]"
      }, '')
  }

  /**
   * Another way to create a riot tag a bit more es6 friendly
   * @param { HTMLElement } el - tag DOM selector or DOM node/s
   * @param { Object } opts - tag logic
   * @returns { Tag } new riot tag instance
   */
  function Tag(el, opts) {
    // get the tag properties from the class constructor
    var ref = this;
    var name = ref.name;
    var tmpl = ref.tmpl;
    var css = ref.css;
    var attrs = ref.attrs;
    var onCreate = ref.onCreate;
    // register a new tag and cache the class prototype
    if (!__TAG_IMPL[name]) {
      tag(name, tmpl, css, attrs, onCreate);
      // cache the class constructor
      __TAG_IMPL[name].class = this.constructor;
    }

    // mount the tag using the class instance
    mount$1(el, name, opts, this);
    // inject the component css
    if (css) { styleManager.inject(); }

    return this
  }

  /**
   * Create a new riot tag implementation
   * @param   { String }   name - name/id of the new riot tag
   * @param   { String }   tmpl - tag template
   * @param   { String }   css - custom tag css
   * @param   { String }   attrs - root tag attributes
   * @param   { Function } fn - user function
   * @returns { String } name/id of the tag just created
   */
  function tag(name, tmpl, css, attrs, fn) {
    if (isFunction(attrs)) {
      fn = attrs;

      if (/^[\w-]+\s?=/.test(css)) {
        attrs = css;
        css = '';
      } else
        { attrs = ''; }
    }

    if (css) {
      if (isFunction(css))
        { fn = css; }
      else
        { styleManager.add(css, name); }
    }

    name = name.toLowerCase();
    __TAG_IMPL[name] = { name: name, tmpl: tmpl, attrs: attrs, fn: fn };

    return name
  }

  /**
   * Create a new riot tag implementation (for use by the compiler)
   * @param   { String }   name - name/id of the new riot tag
   * @param   { String }   tmpl - tag template
   * @param   { String }   css - custom tag css
   * @param   { String }   attrs - root tag attributes
   * @param   { Function } fn - user function
   * @returns { String } name/id of the tag just created
   */
  function tag2(name, tmpl, css, attrs, fn) {
    if (css) { styleManager.add(css, name); }

    __TAG_IMPL[name] = { name: name, tmpl: tmpl, attrs: attrs, fn: fn };

    return name
  }

  /**
   * Mount a tag using a specific tag implementation
   * @param   { * } selector - tag DOM selector or DOM node/s
   * @param   { String } tagName - tag implementation name
   * @param   { Object } opts - tag logic
   * @returns { Array } new tags instances
   */
  function mount(selector, tagName, opts) {
    var tags = [];
    var elem, allTags;

    function pushTagsTo(root) {
      if (root.tagName) {
        var riotTag = getAttribute(root, IS_DIRECTIVE), tag;

        // have tagName? force riot-tag to be the same
        if (tagName && riotTag !== tagName) {
          riotTag = tagName;
          setAttribute(root, IS_DIRECTIVE, tagName);
        }

        tag = mount$1(root, riotTag || root.tagName.toLowerCase(), opts);

        if (tag)
          { tags.push(tag); }
      } else if (root.length)
        { each(root, pushTagsTo); } // assume nodeList
    }

    // inject styles into DOM
    styleManager.inject();

    if (isObject(tagName)) {
      opts = tagName;
      tagName = 0;
    }

    // crawl the DOM to find the tag
    if (isString(selector)) {
      selector = selector === '*' ?
        // select all registered tags
        // & tags found with the riot-tag attribute set
        allTags = query() :
        // or just the ones named like the selector
        selector + query(selector.split(/, */));

      // make sure to pass always a selector
      // to the querySelectorAll function
      elem = selector ? $$(selector) : [];
    }
    else
      // probably you have passed already a tag or a NodeList
      { elem = selector; }

    // select all the registered and mount them inside their root elements
    if (tagName === '*') {
      // get all custom tags
      tagName = allTags || query();
      // if the root els it's just a single tag
      if (elem.tagName)
        { elem = $$(tagName, elem); }
      else {
        // select all the children for all the different root elements
        var nodeList = [];

        each(elem, function (_el) { return nodeList.push($$(tagName, _el)); });

        elem = nodeList;
      }
      // get rid of the tagName
      tagName = 0;
    }

    pushTagsTo(elem);

    return tags
  }

  // Create a mixin that could be globally shared across all the tags
  var mixins = {};
  var globals = mixins[GLOBAL_MIXIN] = {};
  var mixins_id = 0;

  /**
   * Create/Return a mixin by its name
   * @param   { String }  name - mixin name (global mixin if object)
   * @param   { Object }  mix - mixin logic
   * @param   { Boolean } g - is global?
   * @returns { Object }  the mixin logic
   */
  function mixin(name, mix, g) {
    // Unnamed global
    if (isObject(name)) {
      mixin(("__" + (mixins_id++) + "__"), name, true);
      return
    }

    var store = g ? globals : mixins;

    // Getter
    if (!mix) {
      if (isUndefined(store[name]))
        { throw new Error(("Unregistered mixin: " + name)) }

      return store[name]
    }

    // Setter
    store[name] = isFunction(mix) ?
      extend(mix.prototype, store[name] || {}) && mix :
      extend(store[name] || {}, mix);
  }

  /**
   * Update all the tags instances created
   * @returns { Array } all the tags instances
   */
  function update$1() {
    return each(__TAGS_CACHE, function (tag) { return tag.update(); })
  }

  function unregister(name) {
    styleManager.remove(name);
    return delete __TAG_IMPL[name]
  }

  var version = 'v3.11.1';

  var core = /*#__PURE__*/Object.freeze({
    Tag: Tag,
    tag: tag,
    tag2: tag2,
    mount: mount,
    mixin: mixin,
    update: update$1,
    unregister: unregister,
    version: version
  });

  /**
   * Add a mixin to this tag
   * @returns { Tag } the current tag instance
   */
  function componentMixin(tag$$1) {
    var mixins = [], len = arguments.length - 1;
    while ( len-- > 0 ) mixins[ len ] = arguments[ len + 1 ];

    each(mixins, function (mix) {
      var instance;
      var obj;
      var props = [];

      // properties blacklisted and will not be bound to the tag instance
      var propsBlacklist = ['init', '__proto__'];

      mix = isString(mix) ? mixin(mix) : mix;

      // check if the mixin is a function
      if (isFunction(mix)) {
        // create the new mixin instance
        instance = new mix();
      } else { instance = mix; }

      var proto = Object.getPrototypeOf(instance);

      // build multilevel prototype inheritance chain property list
      do { props = props.concat(Object.getOwnPropertyNames(obj || instance)); }
      while (obj = Object.getPrototypeOf(obj || instance))

      // loop the keys in the function prototype or the all object keys
      each(props, function (key) {
        // bind methods to tag
        // allow mixins to override other properties/parent mixins
        if (!contains(propsBlacklist, key)) {
          // check for getters/setters
          var descriptor = getPropDescriptor(instance, key) || getPropDescriptor(proto, key);
          var hasGetterSetter = descriptor && (descriptor.get || descriptor.set);

          // apply method only if it does not already exist on the instance
          if (!tag$$1.hasOwnProperty(key) && hasGetterSetter) {
            Object.defineProperty(tag$$1, key, descriptor);
          } else {
            tag$$1[key] = isFunction(instance[key]) ?
              instance[key].bind(tag$$1) :
              instance[key];
          }
        }
      });

      // init method will be called automatically
      if (instance.init)
        { instance.init.bind(tag$$1)(tag$$1.opts); }
    });

    return tag$$1
  }

  /**
   * Move the position of a custom tag in its parent tag
   * @this Tag
   * @param   { String } tagName - key where the tag was stored
   * @param   { Number } newPos - index where the new tag will be stored
   */
  function moveChild(tagName, newPos) {
    var parent = this.parent;
    var tags;
    // no parent no move
    if (!parent) { return }

    tags = parent.tags[tagName];

    if (isArray(tags))
      { tags.splice(newPos, 0, tags.splice(tags.indexOf(this), 1)[0]); }
    else { arrayishAdd(parent.tags, tagName, this); }
  }

  /**
   * Move virtual tag and all child nodes
   * @this Tag
   * @param { Node } src  - the node that will do the inserting
   * @param { Tag } target - insert before this tag's first child
   */
  function moveVirtual(src, target) {
    var this$1 = this;

    var el = this.__.head;
    var sib;
    var frag = createFragment();

    while (el) {
      sib = el.nextSibling;
      frag.appendChild(el);
      el = sib;
      if (el === this$1.__.tail) {
        frag.appendChild(el);
        src.insertBefore(frag, target.__.head);
        break
      }
    }
  }

  /**
   * Convert the item looped into an object used to extend the child tag properties
   * @param   { Object } expr - object containing the keys used to extend the children tags
   * @param   { * } key - value to assign to the new object returned
   * @param   { * } val - value containing the position of the item in the array
   * @returns { Object } - new object containing the values of the original item
   *
   * The variables 'key' and 'val' are arbitrary.
   * They depend on the collection type looped (Array, Object)
   * and on the expression used on the each tag
   *
   */
  function mkitem(expr, key, val) {
    var item = {};
    item[expr.key] = key;
    if (expr.pos) { item[expr.pos] = val; }
    return item
  }

  /**
   * Unmount the redundant tags
   * @param   { Array } items - array containing the current items to loop
   * @param   { Array } tags - array containing all the children tags
   */
  function unmountRedundant(items, tags, filteredItemsCount) {
    var i = tags.length;
    var j = items.length - filteredItemsCount;

    while (i > j) {
      i--;
      remove.apply(tags[i], [tags, i]);
    }
  }


  /**
   * Remove a child tag
   * @this Tag
   * @param   { Array } tags - tags collection
   * @param   { Number } i - index of the tag to remove
   */
  function remove(tags, i) {
    tags.splice(i, 1);
    this.unmount();
    arrayishRemove(this.parent, this, this.__.tagName, true);
  }

  /**
   * Move the nested custom tags in non custom loop tags
   * @this Tag
   * @param   { Number } i - current position of the loop tag
   */
  function moveNestedTags(i) {
    var this$1 = this;

    each(Object.keys(this.tags), function (tagName) {
      moveChild.apply(this$1.tags[tagName], [tagName, i]);
    });
  }

  /**
   * Move a child tag
   * @this Tag
   * @param   { HTMLElement } root - dom node containing all the loop children
   * @param   { Tag } nextTag - instance of the next tag preceding the one we want to move
   * @param   { Boolean } isVirtual - is it a virtual tag?
   */
  function move(root, nextTag, isVirtual) {
    if (isVirtual)
      { moveVirtual.apply(this, [root, nextTag]); }
    else
      { safeInsert(root, this.root, nextTag.root); }
  }

  /**
   * Insert and mount a child tag
   * @this Tag
   * @param   { HTMLElement } root - dom node containing all the loop children
   * @param   { Tag } nextTag - instance of the next tag preceding the one we want to insert
   * @param   { Boolean } isVirtual - is it a virtual tag?
   */
  function insert(root, nextTag, isVirtual) {
    if (isVirtual)
      { makeVirtual.apply(this, [root, nextTag]); }
    else
      { safeInsert(root, this.root, nextTag.root); }
  }

  /**
   * Append a new tag into the DOM
   * @this Tag
   * @param   { HTMLElement } root - dom node containing all the loop children
   * @param   { Boolean } isVirtual - is it a virtual tag?
   */
  function append(root, isVirtual) {
    if (isVirtual)
      { makeVirtual.call(this, root); }
    else
      { root.appendChild(this.root); }
  }

  /**
   * Return the value we want to use to lookup the postion of our items in the collection
   * @param   { String }  keyAttr         - lookup string or expression
   * @param   { * }       originalItem    - original item from the collection
   * @param   { Object }  keyedItem       - object created by riot via { item, i in collection }
   * @param   { Boolean } hasKeyAttrExpr  - flag to check whether the key is an expression
   * @returns { * } value that we will use to figure out the item position via collection.indexOf
   */
  function getItemId(keyAttr, originalItem, keyedItem, hasKeyAttrExpr) {
    if (keyAttr) {
      return hasKeyAttrExpr ?  tmpl(keyAttr, keyedItem) :  originalItem[keyAttr]
    }

    return originalItem
  }

  /**
   * Manage tags having the 'each'
   * @param   { HTMLElement } dom - DOM node we need to loop
   * @param   { Tag } parent - parent tag instance where the dom node is contained
   * @param   { String } expr - string contained in the 'each' attribute
   * @returns { Object } expression object for this each loop
   */
  function _each(dom, parent, expr) {
    var mustReorder = typeof getAttribute(dom, LOOP_NO_REORDER_DIRECTIVE) !== T_STRING || removeAttribute(dom, LOOP_NO_REORDER_DIRECTIVE);
    var keyAttr = getAttribute(dom, KEY_DIRECTIVE);
    var hasKeyAttrExpr = keyAttr ? tmpl.hasExpr(keyAttr) : false;
    var tagName = getName(dom);
    var impl = __TAG_IMPL[tagName];
    var parentNode = dom.parentNode;
    var placeholder = createDOMPlaceholder();
    var child = get(dom);
    var ifExpr = getAttribute(dom, CONDITIONAL_DIRECTIVE);
    var tags = [];
    var isLoop = true;
    var innerHTML = dom.innerHTML;
    var isAnonymous = !__TAG_IMPL[tagName];
    var isVirtual = dom.tagName === 'VIRTUAL';
    var oldItems = [];

    // remove the each property from the original tag
    removeAttribute(dom, LOOP_DIRECTIVE);
    removeAttribute(dom, KEY_DIRECTIVE);

    // parse the each expression
    expr = tmpl.loopKeys(expr);
    expr.isLoop = true;

    if (ifExpr) { removeAttribute(dom, CONDITIONAL_DIRECTIVE); }

    // insert a marked where the loop tags will be injected
    parentNode.insertBefore(placeholder, dom);
    parentNode.removeChild(dom);

    expr.update = function updateEach() {
      // get the new items collection
      expr.value = tmpl(expr.val, parent);

      var items = expr.value;
      var frag = createFragment();
      var isObject = !isArray(items) && !isString(items);
      var root = placeholder.parentNode;
      var tmpItems = [];
      var hasKeys = isObject && !!items;

      // if this DOM was removed the update here is useless
      // this condition fixes also a weird async issue on IE in our unit test
      if (!root) { return }

      // object loop. any changes cause full redraw
      if (isObject) {
        items = items ? Object.keys(items).map(function (key) { return mkitem(expr, items[key], key); }) : [];
      }

      // store the amount of filtered items
      var filteredItemsCount = 0;

      // loop all the new items
      each(items, function (_item, index) {
        var i = index - filteredItemsCount;
        var item = !hasKeys && expr.key ? mkitem(expr, _item, index) : _item;

        // skip this item because it must be filtered
        if (ifExpr && !tmpl(ifExpr, extend(create(parent), item))) {
          filteredItemsCount ++;
          return
        }

        var itemId = getItemId(keyAttr, _item, item, hasKeyAttrExpr);
        // reorder only if the items are not objects
        // or a key attribute has been provided
        var doReorder = !isObject && mustReorder && typeof _item === T_OBJECT || keyAttr;
        var oldPos = oldItems.indexOf(itemId);
        var isNew = oldPos === -1;
        var pos = !isNew && doReorder ? oldPos : i;
        // does a tag exist in this position?
        var tag = tags[pos];
        var mustAppend = i >= oldItems.length;
        var mustCreate = doReorder && isNew || !doReorder && !tag || !tags[i];

        // new tag
        if (mustCreate) {
          tag = createTag(impl, {
            parent: parent,
            isLoop: isLoop,
            isAnonymous: isAnonymous,
            tagName: tagName,
            root: dom.cloneNode(isAnonymous),
            item: item,
            index: i,
          }, innerHTML);

          // mount the tag
          tag.mount();

          if (mustAppend)
            { append.apply(tag, [frag || root, isVirtual]); }
          else
            { insert.apply(tag, [root, tags[i], isVirtual]); }

          if (!mustAppend) { oldItems.splice(i, 0, item); }
          tags.splice(i, 0, tag);
          if (child) { arrayishAdd(parent.tags, tagName, tag, true); }
        } else if (pos !== i && doReorder) {
          // move
          if (keyAttr || contains(items, oldItems[pos])) {
            move.apply(tag, [root, tags[i], isVirtual]);
            // move the old tag instance
            tags.splice(i, 0, tags.splice(pos, 1)[0]);
            // move the old item
            oldItems.splice(i, 0, oldItems.splice(pos, 1)[0]);
          }

          // update the position attribute if it exists
          if (expr.pos) { tag[expr.pos] = i; }

          // if the loop tags are not custom
          // we need to move all their custom tags into the right position
          if (!child && tag.tags) { moveNestedTags.call(tag, i); }
        }

        // cache the original item to use it in the events bound to this node
        // and its children
        extend(tag.__, {
          item: item,
          index: i,
          parent: parent
        });

        tmpItems[i] = itemId;

        if (!mustCreate) { tag.update(item); }
      });

      // remove the redundant tags
      unmountRedundant(items, tags, filteredItemsCount);

      // clone the items array
      oldItems = tmpItems.slice();

      root.insertBefore(frag, placeholder);
    };

    expr.unmount = function () {
      each(tags, function (t) { t.unmount(); });
    };

    return expr
  }

  var RefExpr = {
    init: function init(dom, parent, attrName, attrValue) {
      this.dom = dom;
      this.attr = attrName;
      this.rawValue = attrValue;
      this.parent = parent;
      this.hasExp = tmpl.hasExpr(attrValue);
      return this
    },
    update: function update() {
      var old = this.value;
      var customParent = this.parent && getImmediateCustomParent(this.parent);
      // if the referenced element is a custom tag, then we set the tag itself, rather than DOM
      var tagOrDom = this.dom.__ref || this.tag || this.dom;

      this.value = this.hasExp ? tmpl(this.rawValue, this.parent) : this.rawValue;

      // the name changed, so we need to remove it from the old key (if present)
      if (!isBlank(old) && customParent) { arrayishRemove(customParent.refs, old, tagOrDom); }
      if (!isBlank(this.value) && isString(this.value)) {
        // add it to the refs of parent tag (this behavior was changed >=3.0)
        if (customParent) { arrayishAdd(
          customParent.refs,
          this.value,
          tagOrDom,
          // use an array if it's a looped node and the ref is not an expression
          null,
          this.parent.__.index
        ); }

        if (this.value !== old) {
          setAttribute(this.dom, this.attr, this.value);
        }
      } else {
        removeAttribute(this.dom, this.attr);
      }

      // cache the ref bound to this dom node
      // to reuse it in future (see also #2329)
      if (!this.dom.__ref) { this.dom.__ref = tagOrDom; }
    },
    unmount: function unmount() {
      var tagOrDom = this.tag || this.dom;
      var customParent = this.parent && getImmediateCustomParent(this.parent);
      if (!isBlank(this.value) && customParent)
        { arrayishRemove(customParent.refs, this.value, tagOrDom); }
    }
  }

  /**
   * Create a new ref directive
   * @param   { HTMLElement } dom - dom node having the ref attribute
   * @param   { Tag } context - tag instance where the DOM node is located
   * @param   { String } attrName - either 'ref' or 'data-ref'
   * @param   { String } attrValue - value of the ref attribute
   * @returns { RefExpr } a new RefExpr object
   */
  function createRefDirective(dom, tag, attrName, attrValue) {
    return create(RefExpr).init(dom, tag, attrName, attrValue)
  }

  /**
   * Trigger the unmount method on all the expressions
   * @param   { Array } expressions - DOM expressions
   */
  function unmountAll(expressions) {
    each(expressions, function (expr) {
      if (expr.unmount) { expr.unmount(true); }
      else if (expr.tagName) { expr.tag.unmount(true); }
      else if (expr.unmount) { expr.unmount(); }
    });
  }

  var IfExpr = {
    init: function init(dom, tag, expr) {
      removeAttribute(dom, CONDITIONAL_DIRECTIVE);
      extend(this, { tag: tag, expr: expr, stub: createDOMPlaceholder(), pristine: dom });
      var p = dom.parentNode;
      p.insertBefore(this.stub, dom);
      p.removeChild(dom);

      return this
    },
    update: function update$$1() {
      this.value = tmpl(this.expr, this.tag);

      if (!this.stub.parentNode) { return }

      if (this.value && !this.current) { // insert
        this.current = this.pristine.cloneNode(true);
        this.stub.parentNode.insertBefore(this.current, this.stub);
        this.expressions = parseExpressions.apply(this.tag, [this.current, true]);
      } else if (!this.value && this.current) { // remove
        this.unmount();
        this.current = null;
        this.expressions = [];
      }

      if (this.value) { update.call(this.tag, this.expressions); }
    },
    unmount: function unmount() {
      if (this.current) {
        if (this.current._tag) {
          this.current._tag.unmount();
        } else if (this.current.parentNode) {
          this.current.parentNode.removeChild(this.current);
        }
      }

      unmountAll(this.expressions || []);
    }
  }

  /**
   * Create a new if directive
   * @param   { HTMLElement } dom - if root dom node
   * @param   { Tag } context - tag instance where the DOM node is located
   * @param   { String } attr - if expression
   * @returns { IFExpr } a new IfExpr object
   */
  function createIfDirective(dom, tag, attr) {
    return create(IfExpr).init(dom, tag, attr)
  }

  /**
   * Walk the tag DOM to detect the expressions to evaluate
   * @this Tag
   * @param   { HTMLElement } root - root tag where we will start digging the expressions
   * @param   { Boolean } mustIncludeRoot - flag to decide whether the root must be parsed as well
   * @returns { Array } all the expressions found
   */
  function parseExpressions(root, mustIncludeRoot) {
    var this$1 = this;

    var expressions = [];

    walkNodes(root, function (dom) {
      var type = dom.nodeType;
      var attr;
      var tagImpl;

      if (!mustIncludeRoot && dom === root) { return }

      // text node
      if (type === 3 && dom.parentNode.tagName !== 'STYLE' && tmpl.hasExpr(dom.nodeValue))
        { expressions.push({dom: dom, expr: dom.nodeValue}); }

      if (type !== 1) { return }

      var isVirtual = dom.tagName === 'VIRTUAL';

      // loop. each does it's own thing (for now)
      if (attr = getAttribute(dom, LOOP_DIRECTIVE)) {
        if(isVirtual) { setAttribute(dom, 'loopVirtual', true); } // ignore here, handled in _each
        expressions.push(_each(dom, this$1, attr));
        return false
      }

      // if-attrs become the new parent. Any following expressions (either on the current
      // element, or below it) become children of this expression.
      if (attr = getAttribute(dom, CONDITIONAL_DIRECTIVE)) {
        expressions.push(createIfDirective(dom, this$1, attr));
        return false
      }

      if (attr = getAttribute(dom, IS_DIRECTIVE)) {
        if (tmpl.hasExpr(attr)) {
          expressions.push({
            isRtag: true,
            expr: attr,
            dom: dom,
            attrs: [].slice.call(dom.attributes)
          });

          return false
        }
      }

      // if this is a tag, stop traversing here.
      // we ignore the root, since parseExpressions is called while we're mounting that root
      tagImpl = get(dom);

      if(isVirtual) {
        if(getAttribute(dom, 'virtualized')) {dom.parentElement.removeChild(dom); } // tag created, remove from dom
        if(!tagImpl && !getAttribute(dom, 'virtualized') && !getAttribute(dom, 'loopVirtual'))  // ok to create virtual tag
          { tagImpl = { tmpl: dom.outerHTML }; }
      }

      if (tagImpl && (dom !== root || mustIncludeRoot)) {
        var hasIsDirective = getAttribute(dom, IS_DIRECTIVE);
        if(isVirtual && !hasIsDirective) { // handled in update
          // can not remove attribute like directives
          // so flag for removal after creation to prevent maximum stack error
          setAttribute(dom, 'virtualized', true);
          var tag = createTag(
            {tmpl: dom.outerHTML},
            {root: dom, parent: this$1},
            dom.innerHTML
          );

          expressions.push(tag); // no return, anonymous tag, keep parsing
        } else {
          if (hasIsDirective && isVirtual)
            { warn(("Virtual tags shouldn't be used together with the \"" + IS_DIRECTIVE + "\" attribute - https://github.com/riot/riot/issues/2511")); }

          expressions.push(
            initChild(
              tagImpl,
              {
                root: dom,
                parent: this$1
              },
              dom.innerHTML,
              this$1
            )
          );
          return false
        }
      }

      // attribute expressions
      parseAttributes.apply(this$1, [dom, dom.attributes, function (attr, expr) {
        if (!expr) { return }
        expressions.push(expr);
      }]);
    });

    return expressions
  }

  /**
   * Calls `fn` for every attribute on an element. If that attr has an expression,
   * it is also passed to fn.
   * @this Tag
   * @param   { HTMLElement } dom - dom node to parse
   * @param   { Array } attrs - array of attributes
   * @param   { Function } fn - callback to exec on any iteration
   */
  function parseAttributes(dom, attrs, fn) {
    var this$1 = this;

    each(attrs, function (attr) {
      if (!attr) { return false }

      var name = attr.name;
      var bool = isBoolAttr(name);
      var expr;

      if (contains(REF_DIRECTIVES, name) && dom.tagName.toLowerCase() !== YIELD_TAG) {
        expr =  createRefDirective(dom, this$1, name, attr.value);
      } else if (tmpl.hasExpr(attr.value)) {
        expr = {dom: dom, expr: attr.value, attr: name, bool: bool};
      }

      fn(attr, expr);
    });
  }

  /**
   * Manage the mount state of a tag triggering also the observable events
   * @this Tag
   * @param { Boolean } value - ..of the isMounted flag
   */
  function setMountState(value) {
    var ref = this.__;
    var isAnonymous = ref.isAnonymous;

    define(this, 'isMounted', value);

    if (!isAnonymous) {
      if (value) { this.trigger('mount'); }
      else {
        this.trigger('unmount');
        this.off('*');
        this.__.wasCreated = false;
      }
    }
  }

  /**
   * Mount the current tag instance
   * @returns { Tag } the current tag instance
   */
  function componentMount(tag$$1, dom, expressions, opts) {
    var __ = tag$$1.__;
    var root = __.root;
    root._tag = tag$$1; // keep a reference to the tag just created

    // Read all the attrs on this instance. This give us the info we need for updateOpts
    parseAttributes.apply(__.parent, [root, root.attributes, function (attr, expr) {
      if (!__.isAnonymous && RefExpr.isPrototypeOf(expr)) { expr.tag = tag$$1; }
      attr.expr = expr;
      __.instAttrs.push(attr);
    }]);

    // update the root adding custom attributes coming from the compiler
    walkAttributes(__.impl.attrs, function (k, v) { __.implAttrs.push({name: k, value: v}); });
    parseAttributes.apply(tag$$1, [root, __.implAttrs, function (attr, expr) {
      if (expr) { expressions.push(expr); }
      else { setAttribute(root, attr.name, attr.value); }
    }]);

    // initialiation
    updateOpts.apply(tag$$1, [__.isLoop, __.parent, __.isAnonymous, opts, __.instAttrs]);

    // add global mixins
    var globalMixin = mixin(GLOBAL_MIXIN);

    if (globalMixin && !__.skipAnonymous) {
      for (var i in globalMixin) {
        if (globalMixin.hasOwnProperty(i)) {
          tag$$1.mixin(globalMixin[i]);
        }
      }
    }

    if (__.impl.fn) { __.impl.fn.call(tag$$1, opts); }

    if (!__.skipAnonymous) { tag$$1.trigger('before-mount'); }

    // parse layout after init. fn may calculate args for nested custom tags
    each(parseExpressions.apply(tag$$1, [dom, __.isAnonymous]), function (e) { return expressions.push(e); });

    tag$$1.update(__.item);

    if (!__.isAnonymous && !__.isInline) {
      while (dom.firstChild) { root.appendChild(dom.firstChild); }
    }

    define(tag$$1, 'root', root);

    // if we need to wait that the parent "mount" or "updated" event gets triggered
    if (!__.skipAnonymous && tag$$1.parent) {
      var p = getImmediateCustomParent(tag$$1.parent);
      p.one(!p.isMounted ? 'mount' : 'updated', function () {
        setMountState.call(tag$$1, true);
      });
    } else {
      // otherwise it's not a child tag we can trigger its mount event
      setMountState.call(tag$$1, true);
    }

    tag$$1.__.wasCreated = true;

    return tag$$1
  }

  /**
   * Unmount the tag instance
   * @param { Boolean } mustKeepRoot - if it's true the root node will not be removed
   * @returns { Tag } the current tag instance
   */
  function tagUnmount(tag, mustKeepRoot, expressions) {
    var __ = tag.__;
    var root = __.root;
    var tagIndex = __TAGS_CACHE.indexOf(tag);
    var p = root.parentNode;

    if (!__.skipAnonymous) { tag.trigger('before-unmount'); }

    // clear all attributes coming from the mounted tag
    walkAttributes(__.impl.attrs, function (name) {
      if (startsWith(name, ATTRS_PREFIX))
        { name = name.slice(ATTRS_PREFIX.length); }

      removeAttribute(root, name);
    });

    // remove all the event listeners
    tag.__.listeners.forEach(function (dom) {
      Object.keys(dom[RIOT_EVENTS_KEY]).forEach(function (eventName) {
        dom.removeEventListener(eventName, dom[RIOT_EVENTS_KEY][eventName]);
      });
    });

    // remove tag instance from the global tags cache collection
    if (tagIndex !== -1) { __TAGS_CACHE.splice(tagIndex, 1); }

    // clean up the parent tags object
    if (__.parent && !__.isAnonymous) {
      var ptag = getImmediateCustomParent(__.parent);

      if (__.isVirtual) {
        Object
          .keys(tag.tags)
          .forEach(function (tagName) { return arrayishRemove(ptag.tags, tagName, tag.tags[tagName]); });
      } else {
        arrayishRemove(ptag.tags, __.tagName, tag);
      }
    }

    // unmount all the virtual directives
    if (tag.__.virts) {
      each(tag.__.virts, function (v) {
        if (v.parentNode) { v.parentNode.removeChild(v); }
      });
    }

    // allow expressions to unmount themselves
    unmountAll(expressions);
    each(__.instAttrs, function (a) { return a.expr && a.expr.unmount && a.expr.unmount(); });

    // clear the tag html if it's necessary
    if (mustKeepRoot) { setInnerHTML(root, ''); }
    // otherwise detach the root tag from the DOM
    else if (p) { p.removeChild(root); }

    // custom internal unmount function to avoid relying on the observable
    if (__.onUnmount) { __.onUnmount(); }

    // weird fix for a weird edge case #2409 and #2436
    // some users might use your software not as you've expected
    // so I need to add these dirty hacks to mitigate unexpected issues
    if (!tag.isMounted) { setMountState.call(tag, true); }

    setMountState.call(tag, false);

    delete root._tag;

    return tag
  }

  /**
   * Tag creation factory function
   * @constructor
   * @param { Object } impl - it contains the tag template, and logic
   * @param { Object } conf - tag options
   * @param { String } innerHTML - html that eventually we need to inject in the tag
   */
  function createTag(impl, conf, innerHTML) {
    if ( impl === void 0 ) impl = {};
    if ( conf === void 0 ) conf = {};

    var tag = conf.context || {};
    var opts = conf.opts || {};
    var parent = conf.parent;
    var isLoop = conf.isLoop;
    var isAnonymous = !!conf.isAnonymous;
    var skipAnonymous = settings.skipAnonymousTags && isAnonymous;
    var item = conf.item;
    // available only for the looped nodes
    var index = conf.index;
    // All attributes on the Tag when it's first parsed
    var instAttrs = [];
    // expressions on this type of Tag
    var implAttrs = [];
    var tmpl = impl.tmpl;
    var expressions = [];
    var root = conf.root;
    var tagName = conf.tagName || getName(root);
    var isVirtual = tagName === 'virtual';
    var isInline = !isVirtual && !tmpl;
    var dom;

    if (isInline || isLoop && isAnonymous) {
      dom = root;
    } else {
      if (!isVirtual) { root.innerHTML = ''; }
      dom = mkdom(tmpl, innerHTML, isSvg(root));
    }

    // make this tag observable
    if (!skipAnonymous) { observable(tag); }

    // only call unmount if we have a valid __TAG_IMPL (has name property)
    if (impl.name && root._tag) { root._tag.unmount(true); }

    define(tag, '__', {
      impl: impl,
      root: root,
      skipAnonymous: skipAnonymous,
      implAttrs: implAttrs,
      isAnonymous: isAnonymous,
      instAttrs: instAttrs,
      innerHTML: innerHTML,
      tagName: tagName,
      index: index,
      isLoop: isLoop,
      isInline: isInline,
      item: item,
      parent: parent,
      // tags having event listeners
      // it would be better to use weak maps here but we can not introduce breaking changes now
      listeners: [],
      // these vars will be needed only for the virtual tags
      virts: [],
      wasCreated: false,
      tail: null,
      head: null
    });

    // tag protected properties
    return [
      ['isMounted', false],
      // create a unique id to this tag
      // it could be handy to use it also to improve the virtual dom rendering speed
      ['_riot_id', uid()],
      ['root', root],
      ['opts', opts, { writable: true, enumerable: true }],
      ['parent', parent || null],
      // protect the "tags" and "refs" property from being overridden
      ['tags', {}],
      ['refs', {}],
      ['update', function (data) { return componentUpdate(tag, data, expressions); }],
      ['mixin', function () {
        var mixins = [], len = arguments.length;
        while ( len-- ) mixins[ len ] = arguments[ len ];

        return componentMixin.apply(void 0, [ tag ].concat( mixins ));
    }],
      ['mount', function () { return componentMount(tag, dom, expressions, opts); }],
      ['unmount', function (mustKeepRoot) { return tagUnmount(tag, mustKeepRoot, expressions); }]
    ].reduce(function (acc, ref) {
      var key = ref[0];
      var value = ref[1];
      var opts = ref[2];

      define(tag, key, value, opts);
      return acc
    }, extend(tag, item))
  }

  /**
   * Mount a tag creating new Tag instance
   * @param   { Object } root - dom node where the tag will be mounted
   * @param   { String } tagName - name of the riot tag we want to mount
   * @param   { Object } opts - options to pass to the Tag instance
   * @param   { Object } ctx - optional context that will be used to extend an existing class ( used in riot.Tag )
   * @returns { Tag } a new Tag instance
   */
  function mount$1(root, tagName, opts, ctx) {
    var impl = __TAG_IMPL[tagName];
    var implClass = __TAG_IMPL[tagName].class;
    var context = ctx || (implClass ? create(implClass.prototype) : {});
    // cache the inner HTML to fix #855
    var innerHTML = root._innerHTML = root._innerHTML || root.innerHTML;
    var conf = extend({ root: root, opts: opts, context: context }, { parent: opts ? opts.parent : null });
    var tag;

    if (impl && root) { tag = createTag(impl, conf, innerHTML); }

    if (tag && tag.mount) {
      tag.mount(true);
      // add this tag to the virtualDom variable
      if (!contains(__TAGS_CACHE, tag)) { __TAGS_CACHE.push(tag); }
    }

    return tag
  }



  var tags = /*#__PURE__*/Object.freeze({
    arrayishAdd: arrayishAdd,
    getTagName: getName,
    inheritParentProps: inheritParentProps,
    mountTo: mount$1,
    selectTags: query,
    arrayishRemove: arrayishRemove,
    getTag: get,
    initChildTag: initChild,
    moveChildTag: moveChild,
    makeReplaceVirtual: makeReplaceVirtual,
    getImmediateCustomParentTag: getImmediateCustomParent,
    makeVirtual: makeVirtual,
    moveVirtual: moveVirtual,
    unmountAll: unmountAll,
    createIfDirective: createIfDirective,
    createRefDirective: createRefDirective
  });

  /**
   * Riot public api
   */
  var settings$1 = settings;
  var util = {
    tmpl: tmpl,
    brackets: brackets,
    styleManager: styleManager,
    vdom: __TAGS_CACHE,
    styleNode: styleManager.styleNode,
    // export the riot internal utils as well
    dom: dom,
    check: check,
    misc: misc,
    tags: tags
  };

  // export the core props/methods
  var Tag$1 = Tag;
  var tag$1 = tag;
  var tag2$1 = tag2;
  var mount$2 = mount;
  var mixin$1 = mixin;
  var update$2 = update$1;
  var unregister$1 = unregister;
  var version$1 = version;
  var observable$1 = observable;

  var riot$1 = extend({}, core, {
    observable: observable,
    settings: settings$1,
    util: util,
  })

  exports.settings = settings$1;
  exports.util = util;
  exports.Tag = Tag$1;
  exports.tag = tag$1;
  exports.tag2 = tag2$1;
  exports.mount = mount$2;
  exports.mixin = mixin$1;
  exports.update = update$2;
  exports.unregister = unregister$1;
  exports.version = version$1;
  exports.observable = observable$1;
  exports.default = riot$1;

  Object.defineProperty(exports, '__esModule', { value: true });

})));

},{}],2:[function(require,module,exports){
"use strict";

var _riot = require("riot");

var _riot2 = _interopRequireDefault(_riot);

require("./views/app.html");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_riot2.default.mount('*');

},{"./views/app.html":3,"riot":1}],3:[function(require,module,exports){
var riot = require('riot');
module.exports = riot.tag2('app', '<div class="container-scroller"><nav class="navbar horizontal-layout col-lg-12 col-12 p-0"><div class="container d-flex flex-row"><div class="text-center navbar-brand-wrapper d-flex align-items-top"><a class="navbar-brand brand-logo" href="index.html"><img src="images/logo-inverse.svg" alt="logo"></a><a class="navbar-brand brand-logo-mini" href="index.html"><img src="images/logo-mini.svg" alt="logo"></a></div><div class="navbar-menu-wrapper d-flex align-items-center"><form class="search-field ml-auto" action="#"><div class="form-group mb-0"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-magnify"></i></span></div><input type="text" class="form-control"></div></div></form><ul class="navbar-nav navbar-nav-right mr-0"><li class="nav-item dropdown ml-4"><a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown"><i class="mdi mdi-bell-outline"></i><span class="count bg-warning">4</span></a><div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown"><a class="dropdown-item py-3"><p class="mb-0 font-weight-medium float-left">You have 4 new notifications </p><span class="badge badge-pill badge-inverse-info float-right">View all</span></a><div class="dropdown-divider"></div><a class="dropdown-item preview-item"><div class="preview-thumbnail"><div class="preview-icon bg-inverse-success"><i class="mdi mdi-alert-circle-outline mx-0"></i></div></div><div class="preview-item-content"><h6 class="preview-subject font-weight-normal text-dark mb-1">Application Error</h6><p class="font-weight-light small-text mb-0"> Just now </p></div></a><div class="dropdown-divider"></div><a class="dropdown-item preview-item"><div class="preview-thumbnail"><div class="preview-icon bg-inverse-warning"><i class="mdi mdi-comment-text-outline mx-0"></i></div></div><div class="preview-item-content"><h6 class="preview-subject font-weight-normal text-dark mb-1">Settings</h6><p class="font-weight-light small-text mb-0"> Private message </p></div></a><div class="dropdown-divider"></div><a class="dropdown-item preview-item"><div class="preview-thumbnail"><div class="preview-icon bg-inverse-info"><i class="mdi mdi-email-outline mx-0"></i></div></div><div class="preview-item-content"><h6 class="preview-subject font-weight-normal text-dark mb-1">New user registration</h6><p class="font-weight-light small-text mb-0"> 2 days ago </p></div></a></div></li><li class="nav-item dropdown d-none d-xl-inline-block"><a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false"><img class="img-xs rounded-circle" src="images/faces/face1.jpg" alt="Profile image"></a><div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown"><a class="dropdown-item p-0"><div class="d-flex border-bottom"><div class="py-3 px-4 d-flex align-items-center justify-content-center"><i class="mdi mdi-bookmark-plus-outline mr-0 text-gray"></i></div><div class="py-3 px-4 d-flex align-items-center justify-content-center border-left border-right"><i class="mdi mdi-account-outline mr-0 text-gray"></i></div><div class="py-3 px-4 d-flex align-items-center justify-content-center"><i class="mdi mdi-alarm-check mr-0 text-gray"></i></div></div></a><a class="dropdown-item mt-2"> Manage Accounts </a><a class="dropdown-item"> Change Password </a><a class="dropdown-item"> Check Inbox </a><a class="dropdown-item"> Sign Out </a></div></li></ul><button class="navbar-toggler align-self-center" type="button" data-toggle="minimize"><span class="mdi mdi-menu"></span></button></div></div><div class="nav-bottom"><div class="container"><ul class="nav page-navigation"><li class="nav-item"><a href="index.html" class="nav-link"><i class="link-icon mdi mdi-television"></i><span class="menu-title">DASHBOARD</span></a></li><li class="nav-item"><a href="../widgets.html" class="nav-link"><i class="link-icon mdi mdi-apple-safari"></i><span class="menu-title">WIDGETS</span></a></li><li class="nav-item mega-menu"><a href="#" class="nav-link"><i class="link-icon mdi mdi-atom"></i><span class="menu-title">UI ELEMENTS</span><i class="menu-arrow"></i></a><div class="submenu"><div class="col-group-wrapper row"><div class="col-group col-md-4"><div class="row"><div class="col-12"><p class="category-heading">Basic Elements</p></div><div class="col-md-6"><ul class="submenu-item"><li class="nav-item"><a class="nav-link" href="../ui-features/accordions.html">Accordion</a></li><li class="nav-item"><a class="nav-link" href="../ui-features/buttons.html">Buttons</a></li><li class="nav-item"><a class="nav-link" href="../ui-features/badges.html">Badges</a></li><li class="nav-item"><a class="nav-link" href="../ui-features/breadcrumbs.html">Breadcrumbs</a></li><li class="nav-item"><a class="nav-link" href="../ui-features/dropdowns.html">Dropdown</a></li><li class="nav-item"><a class="nav-link" href="../ui-features/modals.html">Modals</a></li></ul></div><div class="col-md-6"><ul class="submenu-item"><li class="nav-item"><a class="nav-link" href="../ui-features/progress.html">Progress bar</a></li><li class="nav-item"><a class="nav-link" href="../ui-features/pagination.html">Pagination</a></li><li class="nav-item"><a class="nav-link" href="../ui-features/tabs.html">Tabs</a></li><li class="nav-item"><a class="nav-link" href="../ui-features/typography.html">Typography</a></li><li class="nav-item"><a class="nav-link" href="../ui-features/tooltips.html">Tooltip</a></li></ul></div></div></div><div class="col-group col-md-4"><div class="row"><div class="col-12"><p class="category-heading">Advanced Elements</p></div><div class="col-md-6"><ul class="submenu-item"><li class="nav-item"><a class="nav-link" href="../ui-features/dragula.html">Dragula</a></li><li class="nav-item"><a class="nav-link" href="../ui-features/carousel.html">Carousel</a></li><li class="nav-item"><a class="nav-link" href="../ui-features/clipboard.html">Clipboard</a></li><li class="nav-item"><a class="nav-link" href="../ui-features/context-menu.html">Context Menu</a></li><li class="nav-item"><a class="nav-link" href="../ui-features/loaders.html">Loader</a></li><li class="nav-item"><a class="nav-link" href="../ui-features/slider.html">Slider</a></li></ul></div><div class="col-md-6"><ul class="submenu-item"><li class="nav-item"><a class="nav-link" href="../ui-features/tour.html">Tour</a></li><li class="nav-item"><a class="nav-link" href="../ui-features/popups.html">Popup</a></li><li class="nav-item"><a class="nav-link" href="../ui-features/notifications.html">Notification</a></li></ul></div></div></div><div class="col-group col-md-2"><p class="category-heading">Table</p><ul class="submenu-item"><li class="nav-item"><a class="nav-link" href="../tables/basic-table.html">Basic Table</a></li><li class="nav-item"><a class="nav-link" href="../tables/data-table.html">Data Table</a></li><li class="nav-item"><a class="nav-link" href="../tables/js-grid.html">Js-grid</a></li><li class="nav-item"><a class="nav-link" href="../tables/sortable-table.html">Sortable Table</a></li></ul></div><div class="col-group col-md-2"><p class="category-heading">Icons</p><ul class="submenu-item"><li class="nav-item"><a class="nav-link" href="../icons/flag-icons.html">Flag Icons</a></li><li class="nav-item"><a class="nav-link" href="../icons/font-awesome.html">Font Awesome</a></li><li class="nav-item"><a class="nav-link" href="../icons/simple-line-icon.html">Simple Line Icons</a></li><li class="nav-item"><a class="nav-link" href="../icons/themify.html">Themify Icons</a></li></ul></div></div></div></li><li class="nav-item mega-menu"><a href="#" class="nav-link"><i class="link-icon mdi mdi-flag-outline"></i><span class="menu-title">PAGES</span><i class="menu-arrow"></i></a><div class="submenu"><div class="col-group-wrapper row"><div class="col-group col-md-3"><p class="category-heading">User Pages</p><ul class="submenu-item"><li class="nav-item"><a class="nav-link" href="../samples/login.html">Login</a></li><li class="nav-item"><a class="nav-link" href="../samples/login-2.html">Login 2</a></li><li class="nav-item"><a class="nav-link" href="../samples/register.html">Register</a></li><li class="nav-item"><a class="nav-link" href="../samples/register-2.html">Register 2</a></li><li class="nav-item"><a class="nav-link" href="../samples/lock-screen.html">Lockscreen</a></li></ul></div><div class="col-group col-md-3"><p class="category-heading">Error Pages</p><ul class="submenu-item"><li class="nav-item"><a class="nav-link" href="../samples/error-400.html">400</a></li><li class="nav-item"><a class="nav-link" href="../samples/error-404.html">404</a></li><li class="nav-item"><a class="nav-link" href="../samples/error-500.html">500</a></li><li class="nav-item"><a class="nav-link" href="../samples/error-505.html">505</a></li></ul></div><div class="col-group col-md-3"><p class="category-heading">E-commerce</p><ul class="submenu-item"><li class="nav-item"><a class="nav-link" href="../samples/invoice.html">Invoice</a></li><li class="nav-item"><a class="nav-link" href="../samples/pricing-table.html">Pricing Table</a></li><li class="nav-item"><a class="nav-link" href="../samples/orders.html">Orders</a></li></ul></div><div class="col-group col-md-3"><div class="row"><div class="col-12"><p class="category-heading">Layout</p><ul class="submenu-item"><li class="nav-item"><a class="nav-link" href="../layouts/rtl.html">RTL Layout</a></li></ul></div><div class="col-12 mt-3"><p class="category-heading">Documentation</p><ul class="submenu-item"><li class="nav-item"><a class="nav-link" href="../documentation.html">Documentation</a></li></ul></div></div></div></div></div></li><li class="nav-item mega-menu"><a href="#" class="nav-link"><i class="link-icon mdi mdi-android-studio"></i><span class="menu-title">FORMS</span><i class="menu-arrow"></i></a><div class="submenu"><div class="col-group-wrapper row"><div class="col-group col-md-3"><p class="category-heading">Basic Elements</p><ul class="submenu-item"><li class="nav-item"><a class="nav-link" href="basic_elements.html">Basic Elements</a></li><li class="nav-item"><a class="nav-link" href="advanced_elements.html">Advanced Elements</a></li><li class="nav-item"><a class="nav-link" href="validation.html">Validation</a></li><li class="nav-item"><a class="nav-link" href="wizard.html">Wizard</a></li><li class="nav-item"><a class="nav-link" href="text_editor.html">Text Editor</a></li><li class="nav-item"><a class="nav-link" href="code_editor.html">Code Editor</a></li></ul></div><div class="col-group col-md-3"><p class="category-heading">Charts</p><ul class="submenu-item"><li class="nav-item"><a class="nav-link" href="../charts/chartjs.html">Chart Js</a></li><li class="nav-item"><a class="nav-link" href="../charts/morris.html">Morris</a></li><li class="nav-item"><a class="nav-link" href="../charts/flot-chart.html">Flaot</a></li><li class="nav-item"><a class="nav-link" href="../charts/google-charts.html">Google Chart</a></li><li class="nav-item"><a class="nav-link" href="../charts/sparkline.html">Sparkline</a></li><li class="nav-item"><a class="nav-link" href="../charts/c3.html">C3 Chart</a></li><li class="nav-item"><a class="nav-link" href="../charts/chartist.html">Chartist</a></li><li class="nav-item"><a class="nav-link" href="../charts/justGage.html">JustGage</a></li></ul></div><div class="col-group col-md-3"><p class="category-heading">Maps</p><ul class="submenu-item"><li class="nav-item"><a class="nav-link" href="../maps/mapeal.html">Mapeal</a></li><li class="nav-item"><a class="nav-link" href="../maps/vector-map.html">Vector Map</a></li><li class="nav-item"><a class="nav-link" href="../maps/google-maps.html">Google Map</a></li></ul></div></div></div></li><li class="nav-item"><a href="#" class="nav-link"><i class="link-icon mdi mdi-asterisk"></i><span class="menu-title">APPS</span><i class="menu-arrow"></i></a><div class="submenu"><ul class="submenu-item"><li class="nav-item"><a class="nav-link" href="../apps/email.html">Email</a></li><li class="nav-item"><a class="nav-link" href="../apps/calendar.html">Calendar</a></li><li class="nav-item"><a class="nav-link" href="../apps/todo.html">Todo List</a></li><li class="nav-item"><a class="nav-link" href="../apps/gallery.html">Gallery</a></li></ul></div></li></ul></div></div></nav><div class="container-fluid page-body-wrapper"><div class="main-panel"><div class="content-wrapper"><div class="row"><div class="col-md-6 d-flex align-items-stretch"><div class="row flex-grow"><div class="col-12 grid-margin"><div class="card"><div class="card-body"><h4 class="card-title">Default form</h4><p class="card-description"> Basic form layout </p><form class="forms-sample"><div class="form-group"><label for="exampleInputEmail1">Email address</label><input class="form-control" id="exampleInputEmail1" placeholder="Enter email" type="email"></div><div class="form-group"><label for="exampleInputPassword1">Password</label><input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password"></div><button type="submit" class="btn btn-success mr-2">Submit</button><button class="btn btn-light">Cancel</button></form></div></div></div><div class="col-12 stretch-card grid-margin"><div class="card"><div class="card-body"><h4 class="card-title">Horizontal Form</h4><p class="card-description"> Horizontal form layout </p><form class="forms-sample"><div class="form-group row"><label for="exampleInputEmail2" class="col-sm-3 col-form-label">Email</label><div class="col-sm-9"><input class="form-control" id="exampleInputEmail2" placeholder="Enter email" type="email"></div></div><div class="form-group row"><label for="exampleInputPassword2" class="col-sm-3 col-form-label">Password</label><div class="col-sm-9"><input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password"></div></div><button type="submit" class="btn btn-success mr-2">Submit</button><button class="btn btn-light">Cancel</button></form></div></div></div></div></div><div class="col-md-6 grid-margin stretch-card"><div class="card"><div class="card-body"><h4 class="card-title">Basic form</h4><p class="card-description"> Basic form elements </p><form class="forms-sample"><div class="form-group"><label for="exampleInputName1">Name</label><input type="text" class="form-control" id="exampleInputName1" placeholder="Name"></div><div class="form-group"><label for="exampleInputEmail3">Email address</label><input class="form-control" id="exampleInputEmail3" placeholder="Email" type="email"></div><div class="form-group"><label for="exampleInputPassword4">Password</label><input type="password" class="form-control" id="exampleInputPassword4" placeholder="Password"></div><div class="form-group"><label>File upload</label><input type="file" name="img[]" class="file-upload-default"><div class="input-group col-xs-12"><input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image"><span class="input-group-append"><button class="file-upload-browse btn btn-info" type="button">Upload</button></span></div></div><div class="form-group"><label for="exampleInputCity1">City</label><input type="text" class="form-control" id="exampleInputCity1" placeholder="Location"></div><div class="form-group"><label for="exampleTextarea1">Textarea</label><textarea class="form-control" id="exampleTextarea1" rows="2"></textarea></div><button type="submit" class="btn btn-success mr-2">Submit</button><button class="btn btn-light">Cancel</button></form></div></div></div><div class="col-md-6 d-flex align-items-stretch"><div class="row flex-grow"><div class="col-md-12 grid-margin stretch-card"><div class="card"><div class="card-body"><h4 class="card-title">Select 2</h4><div class="form-group"><label>Single select box using select 2</label><select class="js-example-basic-single" style="width:100%"><option value="AL">Alabama</option><option value="WY">Wyoming</option><option value="AM">America</option><option value="CA">Canada</option><option value="RU">Russia</option></select></div><div class="form-group"><label>Multiple select using select 2</label><select class="js-example-basic-multiple" multiple="multiple" style="width:100%"><option value="AL">Alabama</option><option value="WY">Wyoming</option><option value="AM">America</option><option value="CA">Canada</option><option value="RU">Russia</option></select></div></div></div></div><div class="col-md-12 grid-margin stretch-card"><div class="card"><div class="card-body"><h4 class="card-title">Typeahead</h4><p class="card-description"> A simple suggestion engine </p><div class="form-group row"><div class="col"><label>Basic</label><div id="the-basics"><input class="typeahead" type="text" placeholder="States of USA"></div></div><div class="col"><label>Bloodhound</label><div id="bloodhound"><input class="typeahead" type="text" placeholder="States of USA"></div></div></div></div></div></div></div></div><div class="col-md-6 grid-margin stretch-card"><div class="card"><div class="card-body"><h4 class="card-title">Coloured select box</h4><p class="card-description"> Basic bootstrap select in theme colors </p><div class="form-group"><label for="exampleSelectPrimary">Primary</label><select class="form-control border-primary" id="exampleSelectPrimary"><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option></select></div><div class="form-group"><label for="exampleSelectInfo">Info</label><select class="form-control border-info" id="exampleSelectInfo"><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option></select></div><div class="form-group"><label for="exampleSelectSuccess">Success</label><select class="form-control border-success" id="exampleSelectSuccess"><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option></select></div><div class="form-group"><label for="exampleSelectDanger">Danger</label><select class="form-control border-danger" id="exampleSelectDanger"><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option></select></div><div class="form-group"><label for="exampleSelectWarning">Warning</label><select class="form-control border-warning" id="exampleSelectWarning"><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option></select></div></div></div></div><div class="col-md-5 d-flex align-items-stretch"><div class="row flex-grow"><div class="col-12 grid-margin"><div class="card"><div class="card-body"><h4 class="card-title">Basic input groups</h4><p class="card-description"> Basic bootstrap input groups </p><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text">@</span></div><input type="text" class="form-control" placeholder="Username" aria-label="Username"></div></div><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text">$</span></div><input type="text" class="form-control" aria-label="Amount (to the nearest dollar)"><div class="input-group-append"><span class="input-group-text">.00</span></div></div></div><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text">$</span></div><div class="input-group-prepend"><span class="input-group-text">0.00</span></div><input type="text" class="form-control" aria-label="Amount (to the nearest dollar)"></div></div></div></div></div><div class="col-12 grid-margin stretch-card"><div class="card"><div class="card-body"><h4 class="card-title">Colored input groups</h4><p class="card-description"> Input groups with colors </p><div class="form-group"><div class="input-group"><div class="input-group-prepend bg-info"><span class="input-group-text bg-transparent"><i class="mdi mdi-shield-outline text-white"></i></span></div><input type="text" class="form-control" placeholder="Username" aria-label="Username"></div></div><div class="form-group"><div class="input-group"><div class="input-group-prepend bg-primary border-primary"><span class="input-group-text bg-transparent"><i class="mdi mdi mdi-menu text-white"></i></span></div><input type="text" class="form-control" placeholder="Username" aria-label="Username"></div></div><div class="form-group"><div class="input-group"><input type="text" class="form-control" placeholder="Username" aria-label="Username"><div class="input-group-append bg-primary border-primary"><span class="input-group-text bg-transparent"><i class="mdi mdi-menu text-white"></i></span></div></div></div><div class="form-group"><div class="input-group"><div class="input-group-prepend bg-primary border-primary"><span class="input-group-text bg-transparent text-white">$</span></div><input type="text" class="form-control" aria-label="Amount (to the nearest dollar)"><div class="input-group-append bg-primary border-primary"><span class="input-group-text bg-transparent text-white">.00</span></div></div></div></div></div></div></div></div><div class="col-md-7 grid-margin stretch-card"><div class="card"><div class="card-body"><h4 class="card-title">Input size</h4><p class="card-description"> This is the default bootstrap form layout </p><div class="form-group"><label>Large input</label><input type="text" class="form-control form-control-lg" placeholder="Username" aria-label="Username"></div><div class="form-group"><label>Default input</label><input type="text" class="form-control" placeholder="Username" aria-label="Username"></div><div class="form-group"><label>Small input</label><input type="text" class="form-control form-control-sm" placeholder="Username" aria-label="Username"></div></div><div class="card-body"><h4 class="card-title">Selectize</h4><div class="form-group"><label for="exampleFormControlSelect1">Large select</label><select class="form-control form-control-lg" id="exampleFormControlSelect1"><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option></select></div><div class="form-group"><label for="exampleFormControlSelect2">Default select</label><select class="form-control" id="exampleFormControlSelect2"><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option></select></div><div class="form-group"><label for="exampleFormControlSelect3">Small select</label><select class="form-control form-control-sm" id="exampleFormControlSelect3"><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option></select></div></div></div></div><div class="col-md-6 grid-margin stretch-card"><div class="card"><div class="card-body"><h4 class="card-title">Checkbox Controls</h4><p class="card-description">Checkbox and radio controls</p><form class="forms-sample"><div class="row"><div class="col-md-6"><div class="form-group"><div class="form-check"><label class="form-check-label"><input type="checkbox" class="form-check-input"> Default </label></div><div class="form-check"><label class="form-check-label"><input type="checkbox" class="form-check-input" checked> Checked </label></div><div class="form-check"><label class="form-check-label"><input type="checkbox" class="form-check-input" disabled> Disabled </label></div><div class="form-check"><label class="form-check-label"><input type="checkbox" class="form-check-input" disabled checked> Disabled checked </label></div></div></div><div class="col-md-6"><div class="form-group"><div class="form-radio"><label class="form-check-label"><input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios1" value="" checked> Option one </label></div><div class="form-radio"><label class="form-check-label"><input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios2" value="option2"> Option two </label></div></div><div class="form-group"><div class="form-radio disabled"><label class="form-check-label"><input type="radio" class="form-check-input" name="optionsRadios2" id="optionsRadios3" value="option3" disabled> Option three is disabled </label></div><div class="form-radio disabled"><label class="form-check-label"><input type="radio" class="form-check-input" name="optionsRadio2" id="optionsRadios4" value="option4" disabled checked> Option four is selected and disabled </label></div></div></div></div></form></div></div></div><div class="col-md-6 grid-margin stretch-card"><div class="card"><div class="card-body"><h4 class="card-title">Checkbox Flat Controls</h4><p class="card-description">Checkbox and radio controls with flat design</p><form class="forms-sample"><div class="row"><div class="col-md-6"><div class="form-group"><div class="form-check form-check-flat"><label class="form-check-label"><input type="checkbox" class="form-check-input"> Default </label></div><div class="form-check form-check-flat"><label class="form-check-label"><input type="checkbox" class="form-check-input" checked> Checked </label></div><div class="form-check form-check-flat"><label class="form-check-label"><input type="checkbox" class="form-check-input" disabled> Disabled </label></div><div class="form-check form-check-flat"><label class="form-check-label"><input type="checkbox" class="form-check-input" disabled checked> Disabled checked </label></div></div></div><div class="col-md-6"><div class="form-group"><div class="form-radio form-radio-flat"><label class="form-check-label"><input type="radio" class="form-check-input" name="flatRadios1" id="flatRadios1" value="" checked> Option one </label></div><div class="form-radio form-radio-flat"><label class="form-check-label"><input type="radio" class="form-check-input" name="flatRadios2" id="flatRadios2" value="option2"> Option two </label></div></div><div class="form-group"><div class="form-radio form-radio-flat disabled"><label class="form-check-label"><input type="radio" class="form-check-input" name="flatRadios3" id="flatRadios3" value="option3" disabled> Option three is disabled </label></div><div class="form-radio form-radio-flat disabled"><label class="form-check-label"><input type="radio" class="form-check-input" name="flatRadios4" id="flatRadios4" value="option4" disabled checked> Option four is selected and disabled </label></div></div></div></div></form></div></div></div><div class="col-12 grid-margin"><div class="card"><div class="card-body"><h4 class="card-title">Horizontal Two column</h4><form class="form-sample"><p class="card-description"> Personal info </p><div class="row"><div class="col-md-6"><div class="form-group row"><label class="col-sm-3 col-form-label">First Name</label><div class="col-sm-9"><input type="text" class="form-control"></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-3 col-form-label">Last Name</label><div class="col-sm-9"><input type="text" class="form-control"></div></div></div></div><div class="row"><div class="col-md-6"><div class="form-group row"><label class="col-sm-3 col-form-label">Gender</label><div class="col-sm-9"><select class="form-control"><option>Male</option><option>Female</option></select></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-3 col-form-label">Date of Birth</label><div class="col-sm-9"><input class="form-control" placeholder="dd/mm/yyyy"></div></div></div></div><div class="row"><div class="col-md-6"><div class="form-group row"><label class="col-sm-3 col-form-label">Category</label><div class="col-sm-9"><select class="form-control"><option>Category1</option><option>Category2</option><option>Category3</option><option>Category4</option></select></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-3 col-form-label">Membership</label><div class="col-sm-4"><div class="form-radio"><label class="form-check-label"><input type="radio" class="form-check-input" name="membershipRadios" id="membershipRadios1" value="" checked> Free </label></div></div><div class="col-sm-5"><div class="form-radio"><label class="form-check-label"><input type="radio" class="form-check-input" name="membershipRadios" id="membershipRadios2" value="option2"> Professional </label></div></div></div></div></div><p class="card-description"> Address </p><div class="row"><div class="col-md-6"><div class="form-group row"><label class="col-sm-3 col-form-label">Address 1</label><div class="col-sm-9"><input type="text" class="form-control"></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-3 col-form-label">State</label><div class="col-sm-9"><input type="text" class="form-control"></div></div></div></div><div class="row"><div class="col-md-6"><div class="form-group row"><label class="col-sm-3 col-form-label">Address 2</label><div class="col-sm-9"><input type="text" class="form-control"></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-3 col-form-label">Postcode</label><div class="col-sm-9"><input type="text" class="form-control"></div></div></div></div><div class="row"><div class="col-md-6"><div class="form-group row"><label class="col-sm-3 col-form-label">City</label><div class="col-sm-9"><input type="text" class="form-control"></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-3 col-form-label">Country</label><div class="col-sm-9"><select class="form-control"><option>America</option><option>Italy</option><option>Russia</option><option>Britain</option></select></div></div></div></div></form></div></div></div><div class="col-12"><div class="card"><div class="card-body pb-0"><h4 class="card-title mb-0">Icheck</h4></div><div class="row"><div class="col-lg-6 grid-margin grid-margin-lg-0"><div class="card-body"><p class="card-description">Minimal skin</p><div class="icheck"><input tabindex="5" type="checkbox" id="minimal-checkbox-1"><label for="minimal-checkbox-1">Checkbox 1</label></div><div class="icheck"><input tabindex="6" type="checkbox" id="minimal-checkbox-2" checked><label for="minimal-checkbox-2">Checkbox 2</label></div><div class="icheck"><input type="checkbox" id="minimal-checkbox-disabled" disabled><label for="minimal-checkbox-disabled">Disabled</label></div><div class="icheck"><input type="checkbox" id="minimal-checkbox-disabled-checked" checked disabled><label for="minimal-checkbox-disabled-checked">Disabled &amp; checked</label></div><div class="icheck"><input tabindex="7" type="radio" id="minimal-radio-1" name="minimal-radio"><label for="minimal-radio-1">Radio button 1</label></div><div class="icheck"><input tabindex="8" type="radio" id="minimal-radio-2" name="minimal-radio" checked><label for="minimal-radio-2">Radio button 2</label></div><div class="icheck"><input type="radio" id="minimal-radio-disabled" disabled><label for="minimal-radio-disabled">Disabled</label></div><div class="icheck"><input type="radio" id="minimal-radio-disabled-checked" checked disabled><label for="minimal-radio-disabled-checked">Disabled &amp; checked</label></div></div></div><div class="col-lg-6 grid-margin grid-margin-lg-0"><div class="card-body"><p class="card-description">Square skin</p><div class="icheck-square"><input tabindex="5" type="checkbox" id="square-checkbox-1"><label for="square-checkbox-1">Checkbox 1</label></div><div class="icheck-square"><input tabindex="6" type="checkbox" id="square-checkbox-2" checked><label for="square-checkbox-2">Checkbox 2</label></div><div class="icheck-square"><input type="checkbox" id="square-checkbox-disabled" disabled><label for="square-checkbox-disabled">Disabled</label></div><div class="icheck-square"><input type="checkbox" id="square-checkbox-disabled-checked" checked disabled><label for="square-checkbox-disabled-checked">Disabled &amp; checked</label></div><div class="icheck-square"><input tabindex="7" type="radio" id="square-radio-1" name="minimal-radio"><label for="square-radio-1">Radio button 1</label></div><div class="icheck-square"><input tabindex="8" type="radio" id="square-radio-2" name="minimal-radio" checked><label for="square-radio-2">Radio button 2</label></div><div class="icheck-square"><input type="radio" id="square-radio-disabled" disabled><label for="square-radio-disabled">Disabled</label></div><div class="icheck-square"><input type="radio" id="square-radio-disabled-checked" checked disabled><label for="square-radio-disabled-checked">Disabled &amp; checked</label></div></div></div><div class="col-lg-6 grid-margin grid-margin-lg-0"><div class="card-body"><p class="card-description">Flat skin</p><div class="icheck-flat"><input tabindex="5" type="checkbox" id="flat-checkbox-1"><label for="flat-checkbox-1">Checkbox 1</label></div><div class="icheck-flat"><input tabindex="6" type="checkbox" id="flat-checkbox-2" checked><label for="flat-checkbox-2">Checkbox 2</label></div><div class="icheck-flat"><input type="checkbox" id="flat-checkbox-disabled" disabled><label for="flat-checkbox-disabled">Disabled</label></div><div class="icheck-flat"><input type="checkbox" id="flat-checkbox-disabled-checked" checked disabled><label for="flat-checkbox-disabled-checked">Disabled &amp; checked</label></div><div class="icheck-flat"><input tabindex="7" type="radio" id="flat-radio-1" name="minimal-radio"><label for="flat-radio-1">Radio button 1</label></div><div class="icheck-flat"><input tabindex="8" type="radio" id="flat-radio-2" name="minimal-radio" checked><label for="flat-radio-2">Radio button 2</label></div><div class="icheck-flat"><input type="radio" id="flat-radio-disabled" disabled><label for="flat-radio-disabled">Disabled</label></div><div class="icheck-flat"><input type="radio" id="flat-radio-disabled-checked" checked disabled><label for="flat-radio-disabled-checked">Disabled &amp; checked</label></div></div></div><div class="col-lg-6"><div class="card-body"><p class="card-description">Line skin</p><div class="icheck-line"><input tabindex="5" type="checkbox" id="line-checkbox-1"><label for="line-checkbox-1">Checkbox 1</label></div><div class="icheck-line"><input tabindex="6" type="checkbox" id="line-checkbox-2" checked><label for="line-checkbox-2">Checkbox 2</label></div><div class="icheck-line"><input type="checkbox" id="line-checkbox-disabled" disabled><label for="line-checkbox-disabled">Disabled</label></div><div class="icheck-line"><input type="checkbox" id="line-checkbox-disabled-checked" checked disabled><label for="line-checkbox-disabled-checked">Disabled &amp; checked</label></div><div class="icheck-line"><input tabindex="7" type="radio" id="line-radio-1" name="minimal-radio"><label for="line-radio-1">Radio button 1</label></div><div class="icheck-line"><input tabindex="8" type="radio" id="line-radio-2" name="minimal-radio" checked><label for="line-radio-2">Radio button 2</label></div><div class="icheck-line disabled"><input type="radio" id="line-radio-disabled" disabled><label for="line-radio-disabled">Disabled</label></div><div class="icheck-line disabled"><input type="radio" id="line-radio-disabled-checked" checked disabled><label for="line-radio-disabled-checked">Disabled &amp; checked</label></div></div></div></div></div></div></div></div><footer class="footer"><div class="container-fluid clearfix"><span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 <a href="http://www.urbanui.com/" target="_blank">Urbanui</a>. All rights reserved.</span><span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span></div></footer></div></div></div>', '', '', function(opts) {
});

},{"riot":1}]},{},[2])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJub2RlX21vZHVsZXMvcmlvdC9yaW90LmpzIiwic3JjL21haW4uanMiLCJEOi9fUHJvamVjdHMvYWJyYXJib29rL2FkbWluLmFicmFyYm9vay5jb20vc3JjL3ZpZXdzL2FwcC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUMvZ0dBOzs7O0FBQ0E7Ozs7QUFHQSxlQUFLLEtBQUwsQ0FBVyxHQUFYOzs7Q0NKQSxBQUlFLEFBRUUsQUFFQSxBQUVFLEFBRUUsQUFFRSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVFLEFBRUUsQUFFRSxBQUVFLEFBRUYsQUFFRixBQUVBLEFBRUYsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVFLEFBRUUsQUFFQSxBQUVGLEFBRUEsQUFFRSxBQUVFLEFBRUEsQUFFQSxBQUVGLEFBRUEsQUFFQSxBQUVFLEFBRUUsQUFFRSxBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUEsQUFFRSxBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUEsQUFFRSxBQUVFLEFBRUUsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVBLEFBRUUsQUFFRixBQUVGLEFBRUYsQUFFQSxBQUVBLEFBRUUsQUFFRSxBQUVFLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFQSxBQUVFLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVFLEFBRUUsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRixBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFRSxBQUVFLEFBRUUsQUFFQSxBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFQSxBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFQSxBQUVBLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFRSxBQUVFLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUYsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVFLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVFLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUYsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVFLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVBLEFBRUUsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFQSxBQUVFLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFQSxBQUVBLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFRSxBQUVBLEFBRUUsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFQSxBQUVFLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFRixBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUEsQUFFRSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUYsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVFLEFBRUUsQUFFQSxBQUVFLEFBRUUsQUFFRixBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUEsQUFFRSxBQUVFLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVBLEFBRUEsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVFLEFBRUEsQUFFRSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUYsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVBLEFBRUUsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFQSxBQUVFLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFQSxBQUVBLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFJQSxBQUVBLEFBRUUsQUFFRSxBQUVFLEFBRUUsQUFFRSxBQUVFLEFBRUUsQUFFRSxBQUVFLEFBRUEsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVFLEFBRUEsQUFFRixBQUVBLEFBRUUsQUFFQSxBQUVGLEFBRUEsQUFFQSxBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVFLEFBRUUsQUFFQSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFQSxBQUVFLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFQSxBQUVFLEFBRUYsQUFFRixBQUVBLEFBRUEsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFRSxBQUVBLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVBLEFBRUYsQUFFQSxBQUVFLEFBRUEsQUFFRixBQUVBLEFBRUUsQUFFQSxBQUVGLEFBRUEsQUFFRSxBQUVBLEFBRUEsQUFFRSxBQUVBLEFBRUUsQUFFRixBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUEsQUFFRixBQUVBLEFBRUUsQUFFQSxBQUVGLEFBRUEsQUFFQSxBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVFLEFBRUUsQUFFRSxBQUVFLEFBRUEsQUFFRSxBQUVBLEFBRUUsQUFFQSxBQUVBLEFBRUEsQUFFQSxBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUEsQUFFRSxBQUVBLEFBRUEsQUFFQSxBQUVBLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVFLEFBRUEsQUFFRSxBQUVGLEFBRUEsQUFFRSxBQUVFLEFBRUEsQUFFRSxBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUEsQUFFRSxBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFRSxBQUVBLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFQSxBQUVFLEFBRUEsQUFFQSxBQUVBLEFBRUEsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVBLEFBRUUsQUFFQSxBQUVBLEFBRUEsQUFFQSxBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUEsQUFFRSxBQUVBLEFBRUEsQUFFQSxBQUVBLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFQSxBQUVFLEFBRUEsQUFFQSxBQUVBLEFBRUEsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVBLEFBRUUsQUFFQSxBQUVBLEFBRUEsQUFFQSxBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFRSxBQUVFLEFBRUUsQUFFQSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFRSxBQUVGLEFBRUEsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVFLEFBRUUsQUFFRixBQUVBLEFBRUEsQUFFRSxBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUYsQUFFQSxBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFRSxBQUVBLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVFLEFBRUUsQUFFRixBQUVGLEFBRUEsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVFLEFBRUUsQUFFRSxBQUVGLEFBRUYsQUFFQSxBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFQSxBQUVFLEFBRUUsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVFLEFBRUYsQUFFQSxBQUVBLEFBRUUsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVFLEFBRUUsQUFFQSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUEsQUFFRixBQUVBLEFBRUUsQUFFQSxBQUVGLEFBRUEsQUFFRSxBQUVBLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFQSxBQUVFLEFBRUEsQUFFRSxBQUVBLEFBRUEsQUFFQSxBQUVBLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFQSxBQUVFLEFBRUEsQUFFQSxBQUVBLEFBRUEsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVBLEFBRUUsQUFFQSxBQUVBLEFBRUEsQUFFQSxBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFRSxBQUVBLEFBRUEsQUFFRSxBQUVFLEFBRUUsQUFFRSxBQUVFLEFBRUUsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVFLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVFLEFBRUUsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVFLEFBRUYsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVFLEFBRUUsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVFLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVFLEFBRUUsQUFFQSxBQUVBLEFBRUUsQUFFRSxBQUVFLEFBRUUsQUFFRSxBQUVFLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVFLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFRSxBQUVFLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVFLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVFLEFBRUEsQUFFRSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFRSxBQUVBLEFBRUUsQUFFRixBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFQSxBQUVFLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFRSxBQUVBLEFBRUUsQUFFRSxBQUVBLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFQSxBQUVFLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFRSxBQUVBLEFBRUUsQUFFRSxBQUVBLEFBRUEsQUFFQSxBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVFLEFBRUEsQUFFRSxBQUVFLEFBRUUsQUFFRixBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFRSxBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVFLEFBRUEsQUFFRSxBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVBLEFBRUUsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVFLEFBRUEsQUFFRSxBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVBLEFBRUUsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVFLEFBRUEsQUFFRSxBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVBLEFBRUUsQUFFRSxBQUVBLEFBRUEsQUFFQSxBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVFLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFRSxBQUVBLEFBRUUsQUFFQSxBQUVGLEFBRUEsQUFFRSxBQUVBLEFBRUYsQUFFQSxBQUVFLEFBRUEsQUFFRixBQUVBLEFBRUUsQUFFQSxBQUVGLEFBRUEsQUFFRSxBQUVBLEFBRUYsQUFFQSxBQUVFLEFBRUEsQUFFRixBQUVBLEFBRUUsQUFFQSxBQUVGLEFBRUEsQUFFRSxBQUVBLEFBRUYsQUFFRixBQUVGLEFBRUEsQUFFRSxBQUVFLEFBRUEsQUFFRSxBQUVBLEFBRUYsQUFFQSxBQUVFLEFBRUEsQUFFRixBQUVBLEFBRUUsQUFFQSxBQUVGLEFBRUEsQUFFRSxBQUVBLEFBRUYsQUFFQSxBQUVFLEFBRUEsQUFFRixBQUVBLEFBRUUsQUFFQSxBQUVGLEFBRUEsQUFFRSxBQUVBLEFBRUYsQUFFQSxBQUVFLEFBRUEsQUFFRixBQUVGLEFBRUYsQUFFQSxBQUVFLEFBRUUsQUFFQSxBQUVFLEFBRUEsQUFFRixBQUVBLEFBRUUsQUFFQSxBQUVGLEFBRUEsQUFFRSxBQUVBLEFBRUYsQUFFQSxBQUVFLEFBRUEsQUFFRixBQUVBLEFBRUUsQUFFQSxBQUVGLEFBRUEsQUFFRSxBQUVBLEFBRUYsQUFFQSxBQUVFLEFBRUEsQUFFRixBQUVBLEFBRUUsQUFFQSxBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUUsQUFFRSxBQUVBLEFBRUUsQUFFQSxBQUVGLEFBRUEsQUFFRSxBQUVBLEFBRUYsQUFFQSxBQUVFLEFBRUEsQUFFRixBQUVBLEFBRUUsQUFFQSxBQUVGLEFBRUEsQUFFRSxBQUVBLEFBRUYsQUFFQSxBQUVFLEFBRUEsQUFFRixBQUVBLEFBRUUsQUFFQSxBQUVGLEFBRUEsQUFFRSxBQUVBLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVGLEFBRUYsQUFFRixBQUVBLEFBRUEsQUFFQSxBQUVFLEFBRUUsQUFFRSxBQUVGLEFBRUUsQUFFRixBQUVGLEFBRUYsQUFFQSxBQUVGLEFBRUEsQUFFRixBQUVBLEFBRUYsQUFFQSxBQUVBLEFBRUYiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigpe2Z1bmN0aW9uIHIoZSxuLHQpe2Z1bmN0aW9uIG8oaSxmKXtpZighbltpXSl7aWYoIWVbaV0pe3ZhciBjPVwiZnVuY3Rpb25cIj09dHlwZW9mIHJlcXVpcmUmJnJlcXVpcmU7aWYoIWYmJmMpcmV0dXJuIGMoaSwhMCk7aWYodSlyZXR1cm4gdShpLCEwKTt2YXIgYT1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK2krXCInXCIpO3Rocm93IGEuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixhfXZhciBwPW5baV09e2V4cG9ydHM6e319O2VbaV1bMF0uY2FsbChwLmV4cG9ydHMsZnVuY3Rpb24ocil7dmFyIG49ZVtpXVsxXVtyXTtyZXR1cm4gbyhufHxyKX0scCxwLmV4cG9ydHMscixlLG4sdCl9cmV0dXJuIG5baV0uZXhwb3J0c31mb3IodmFyIHU9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZSxpPTA7aTx0Lmxlbmd0aDtpKyspbyh0W2ldKTtyZXR1cm4gb31yZXR1cm4gcn0pKCkiLCIvKiBSaW90IHYzLjExLjEsIEBsaWNlbnNlIE1JVCAqL1xuKGZ1bmN0aW9uIChnbG9iYWwsIGZhY3RvcnkpIHtcbiAgdHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnICYmIHR5cGVvZiBtb2R1bGUgIT09ICd1bmRlZmluZWQnID8gZmFjdG9yeShleHBvcnRzKSA6XG4gIHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZCA/IGRlZmluZShbJ2V4cG9ydHMnXSwgZmFjdG9yeSkgOlxuICAoZmFjdG9yeSgoZ2xvYmFsLnJpb3QgPSB7fSkpKTtcbn0odGhpcywgKGZ1bmN0aW9uIChleHBvcnRzKSB7ICd1c2Ugc3RyaWN0JztcblxuICAvKipcbiAgICogU2hvcnRlciBhbmQgZmFzdCB3YXkgdG8gc2VsZWN0IGEgc2luZ2xlIG5vZGUgaW4gdGhlIERPTVxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IHNlbGVjdG9yIC0gdW5pcXVlIGRvbSBzZWxlY3RvclxuICAgKiBAcGFyYW0gICB7IE9iamVjdCB9IGN0eCAtIERPTSBub2RlIHdoZXJlIHRoZSB0YXJnZXQgb2Ygb3VyIHNlYXJjaCB3aWxsIGlzIGxvY2F0ZWRcbiAgICogQHJldHVybnMgeyBPYmplY3QgfSBkb20gbm9kZSBmb3VuZFxuICAgKi9cbiAgZnVuY3Rpb24gJChzZWxlY3RvciwgY3R4KSB7XG4gICAgcmV0dXJuIChjdHggfHwgZG9jdW1lbnQpLnF1ZXJ5U2VsZWN0b3Ioc2VsZWN0b3IpXG4gIH1cblxuICB2YXJcbiAgICAvLyBiZSBhd2FyZSwgaW50ZXJuYWwgdXNhZ2VcbiAgICAvLyBBVFRFTlRJT046IHByZWZpeCB0aGUgZ2xvYmFsIGR5bmFtaWMgdmFyaWFibGVzIHdpdGggYF9fYFxuICAgIC8vIHRhZ3MgaW5zdGFuY2VzIGNhY2hlXG4gICAgX19UQUdTX0NBQ0hFID0gW10sXG4gICAgLy8gdGFncyBpbXBsZW1lbnRhdGlvbiBjYWNoZVxuICAgIF9fVEFHX0lNUEwgPSB7fSxcbiAgICBZSUVMRF9UQUcgPSAneWllbGQnLFxuXG4gICAgLyoqXG4gICAgICogQ29uc3RcbiAgICAgKi9cbiAgICBHTE9CQUxfTUlYSU4gPSAnX19nbG9iYWxfbWl4aW4nLFxuXG4gICAgLy8gcmlvdCBzcGVjaWZpYyBwcmVmaXhlcyBvciBhdHRyaWJ1dGVzXG4gICAgQVRUUlNfUFJFRklYID0gJ3Jpb3QtJyxcblxuICAgIC8vIFJpb3QgRGlyZWN0aXZlc1xuICAgIFJFRl9ESVJFQ1RJVkVTID0gWydyZWYnLCAnZGF0YS1yZWYnXSxcbiAgICBJU19ESVJFQ1RJVkUgPSAnZGF0YS1pcycsXG4gICAgQ09ORElUSU9OQUxfRElSRUNUSVZFID0gJ2lmJyxcbiAgICBMT09QX0RJUkVDVElWRSA9ICdlYWNoJyxcbiAgICBMT09QX05PX1JFT1JERVJfRElSRUNUSVZFID0gJ25vLXJlb3JkZXInLFxuICAgIFNIT1dfRElSRUNUSVZFID0gJ3Nob3cnLFxuICAgIEhJREVfRElSRUNUSVZFID0gJ2hpZGUnLFxuICAgIEtFWV9ESVJFQ1RJVkUgPSAna2V5JyxcbiAgICBSSU9UX0VWRU5UU19LRVkgPSAnX19yaW90LWV2ZW50c19fJyxcblxuICAgIC8vIGZvciB0eXBlb2YgPT0gJycgY29tcGFyaXNvbnNcbiAgICBUX1NUUklORyA9ICdzdHJpbmcnLFxuICAgIFRfT0JKRUNUID0gJ29iamVjdCcsXG4gICAgVF9VTkRFRiAgPSAndW5kZWZpbmVkJyxcbiAgICBUX0ZVTkNUSU9OID0gJ2Z1bmN0aW9uJyxcblxuICAgIFhMSU5LX05TID0gJ2h0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsnLFxuICAgIFNWR19OUyA9ICdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZycsXG4gICAgWExJTktfUkVHRVggPSAvXnhsaW5rOihcXHcrKS8sXG5cbiAgICBXSU4gPSB0eXBlb2Ygd2luZG93ID09PSBUX1VOREVGID8gLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi8gdW5kZWZpbmVkIDogd2luZG93LFxuXG4gICAgLy8gc3BlY2lhbCBuYXRpdmUgdGFncyB0aGF0IGNhbm5vdCBiZSB0cmVhdGVkIGxpa2UgdGhlIG90aGVyc1xuICAgIFJFX1NQRUNJQUxfVEFHUyA9IC9eKD86dCg/OmJvZHl8aGVhZHxmb290fFtyaGRdKXxjYXB0aW9ufGNvbCg/Omdyb3VwKT98b3B0KD86aW9ufGdyb3VwKSkkLyxcbiAgICBSRV9TUEVDSUFMX1RBR1NfTk9fT1BUSU9OID0gL14oPzp0KD86Ym9keXxoZWFkfGZvb3R8W3JoZF0pfGNhcHRpb258Y29sKD86Z3JvdXApPykkLyxcbiAgICBSRV9FVkVOVFNfUFJFRklYID0gL15vbi8sXG4gICAgUkVfSFRNTF9BVFRSUyA9IC8oWy1cXHddKykgPz0gPyg/OlwiKFteXCJdKil8JyhbXiddKil8KHtbXn1dKn0pKS9nLFxuICAgIC8vIHNvbWUgRE9NIGF0dHJpYnV0ZXMgbXVzdCBiZSBub3JtYWxpemVkXG4gICAgQ0FTRV9TRU5TSVRJVkVfQVRUUklCVVRFUyA9IHtcbiAgICAgICd2aWV3Ym94JzogJ3ZpZXdCb3gnLFxuICAgICAgJ3ByZXNlcnZlYXNwZWN0cmF0aW8nOiAncHJlc2VydmVBc3BlY3RSYXRpbydcbiAgICB9LFxuICAgIC8qKlxuICAgICAqIE1hdGNoZXMgYm9vbGVhbiBIVE1MIGF0dHJpYnV0ZXMgaW4gdGhlIHJpb3QgdGFnIGRlZmluaXRpb24uXG4gICAgICogV2l0aCBhIGxvbmcgbGlzdCBsaWtlIHRoaXMsIGEgcmVnZXggaXMgZmFzdGVyIHRoYW4gYFtdLmluZGV4T2ZgIGluIG1vc3QgYnJvd3NlcnMuXG4gICAgICogQGNvbnN0IHtSZWdFeHB9XG4gICAgICogQHNlZSBbYXR0cmlidXRlcy5tZF0oaHR0cHM6Ly9naXRodWIuY29tL3Jpb3QvY29tcGlsZXIvYmxvYi9kZXYvZG9jL2F0dHJpYnV0ZXMubWQpXG4gICAgICovXG4gICAgUkVfQk9PTF9BVFRSUyA9IC9eKD86ZGlzYWJsZWR8Y2hlY2tlZHxyZWFkb25seXxyZXF1aXJlZHxhbGxvd2Z1bGxzY3JlZW58YXV0byg/OmZvY3VzfHBsYXkpfGNvbXBhY3R8Y29udHJvbHN8ZGVmYXVsdHxmb3Jtbm92YWxpZGF0ZXxoaWRkZW58aXNtYXB8aXRlbXNjb3BlfGxvb3B8bXVsdGlwbGV8bXV0ZWR8bm8oPzpyZXNpemV8c2hhZGV8dmFsaWRhdGV8d3JhcCk/fG9wZW58cmV2ZXJzZWR8c2VhbWxlc3N8c2VsZWN0ZWR8c29ydGFibGV8dHJ1ZXNwZWVkfHR5cGVtdXN0bWF0Y2gpJC8sXG4gICAgLy8gdmVyc2lvbiMgZm9yIElFIDgtMTEsIDAgZm9yIG90aGVyc1xuICAgIElFX1ZFUlNJT04gPSAoV0lOICYmIFdJTi5kb2N1bWVudCB8fCAvKiBpc3RhbmJ1bCBpZ25vcmUgbmV4dCAqLyB7fSkuZG9jdW1lbnRNb2RlIHwgMDtcblxuICAvKipcbiAgICogQ3JlYXRlIGEgZ2VuZXJpYyBET00gbm9kZVxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IG5hbWUgLSBuYW1lIG9mIHRoZSBET00gbm9kZSB3ZSB3YW50IHRvIGNyZWF0ZVxuICAgKiBAcmV0dXJucyB7IE9iamVjdCB9IERPTSBub2RlIGp1c3QgY3JlYXRlZFxuICAgKi9cbiAgZnVuY3Rpb24gbWFrZUVsZW1lbnQobmFtZSkge1xuICAgIHJldHVybiBuYW1lID09PSAnc3ZnJyA/IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnROUyhTVkdfTlMsIG5hbWUpIDogZG9jdW1lbnQuY3JlYXRlRWxlbWVudChuYW1lKVxuICB9XG5cbiAgLyoqXG4gICAqIFNldCBhbnkgRE9NIGF0dHJpYnV0ZVxuICAgKiBAcGFyYW0geyBPYmplY3QgfSBkb20gLSBET00gbm9kZSB3ZSB3YW50IHRvIHVwZGF0ZVxuICAgKiBAcGFyYW0geyBTdHJpbmcgfSBuYW1lIC0gbmFtZSBvZiB0aGUgcHJvcGVydHkgd2Ugd2FudCB0byBzZXRcbiAgICogQHBhcmFtIHsgU3RyaW5nIH0gdmFsIC0gdmFsdWUgb2YgdGhlIHByb3BlcnR5IHdlIHdhbnQgdG8gc2V0XG4gICAqL1xuICBmdW5jdGlvbiBzZXRBdHRyaWJ1dGUoZG9tLCBuYW1lLCB2YWwpIHtcbiAgICB2YXIgeGxpbmsgPSBYTElOS19SRUdFWC5leGVjKG5hbWUpO1xuICAgIGlmICh4bGluayAmJiB4bGlua1sxXSlcbiAgICAgIHsgZG9tLnNldEF0dHJpYnV0ZU5TKFhMSU5LX05TLCB4bGlua1sxXSwgdmFsKTsgfVxuICAgIGVsc2VcbiAgICAgIHsgZG9tLnNldEF0dHJpYnV0ZShuYW1lLCB2YWwpOyB9XG4gIH1cblxuICB2YXIgc3R5bGVOb2RlO1xuICAvLyBDcmVhdGUgY2FjaGUgYW5kIHNob3J0Y3V0IHRvIHRoZSBjb3JyZWN0IHByb3BlcnR5XG4gIHZhciBjc3NUZXh0UHJvcDtcbiAgdmFyIGJ5TmFtZSA9IHt9O1xuICB2YXIgbmVlZHNJbmplY3QgPSBmYWxzZTtcblxuICAvLyBza2lwIHRoZSBmb2xsb3dpbmcgY29kZSBvbiB0aGUgc2VydmVyXG4gIGlmIChXSU4pIHtcbiAgICBzdHlsZU5vZGUgPSAoKGZ1bmN0aW9uICgpIHtcbiAgICAgIC8vIGNyZWF0ZSBhIG5ldyBzdHlsZSBlbGVtZW50IHdpdGggdGhlIGNvcnJlY3QgdHlwZVxuICAgICAgdmFyIG5ld05vZGUgPSBtYWtlRWxlbWVudCgnc3R5bGUnKTtcbiAgICAgIC8vIHJlcGxhY2UgYW55IHVzZXIgbm9kZSBvciBpbnNlcnQgdGhlIG5ldyBvbmUgaW50byB0aGUgaGVhZFxuICAgICAgdmFyIHVzZXJOb2RlID0gJCgnc3R5bGVbdHlwZT1yaW90XScpO1xuXG4gICAgICBzZXRBdHRyaWJ1dGUobmV3Tm9kZSwgJ3R5cGUnLCAndGV4dC9jc3MnKTtcbiAgICAgIC8qIGlzdGFuYnVsIGlnbm9yZSBuZXh0ICovXG4gICAgICBpZiAodXNlck5vZGUpIHtcbiAgICAgICAgaWYgKHVzZXJOb2RlLmlkKSB7IG5ld05vZGUuaWQgPSB1c2VyTm9kZS5pZDsgfVxuICAgICAgICB1c2VyTm9kZS5wYXJlbnROb2RlLnJlcGxhY2VDaGlsZChuZXdOb2RlLCB1c2VyTm9kZSk7XG4gICAgICB9IGVsc2UgeyBkb2N1bWVudC5oZWFkLmFwcGVuZENoaWxkKG5ld05vZGUpOyB9XG5cbiAgICAgIHJldHVybiBuZXdOb2RlXG4gICAgfSkpKCk7XG4gICAgY3NzVGV4dFByb3AgPSBzdHlsZU5vZGUuc3R5bGVTaGVldDtcbiAgfVxuXG4gIC8qKlxuICAgKiBPYmplY3QgdGhhdCB3aWxsIGJlIHVzZWQgdG8gaW5qZWN0IGFuZCBtYW5hZ2UgdGhlIGNzcyBvZiBldmVyeSB0YWcgaW5zdGFuY2VcbiAgICovXG4gIHZhciBzdHlsZU1hbmFnZXIgPSB7XG4gICAgc3R5bGVOb2RlOiBzdHlsZU5vZGUsXG4gICAgLyoqXG4gICAgICogU2F2ZSBhIHRhZyBzdHlsZSB0byBiZSBsYXRlciBpbmplY3RlZCBpbnRvIERPTVxuICAgICAqIEBwYXJhbSB7IFN0cmluZyB9IGNzcyAtIGNzcyBzdHJpbmdcbiAgICAgKiBAcGFyYW0geyBTdHJpbmcgfSBuYW1lIC0gaWYgaXQncyBwYXNzZWQgd2Ugd2lsbCBtYXAgdGhlIGNzcyB0byBhIHRhZ25hbWVcbiAgICAgKi9cbiAgICBhZGQ6IGZ1bmN0aW9uIGFkZChjc3MsIG5hbWUpIHtcbiAgICAgIGJ5TmFtZVtuYW1lXSA9IGNzcztcbiAgICAgIG5lZWRzSW5qZWN0ID0gdHJ1ZTtcbiAgICB9LFxuICAgIC8qKlxuICAgICAqIEluamVjdCBhbGwgcHJldmlvdXNseSBzYXZlZCB0YWcgc3R5bGVzIGludG8gRE9NXG4gICAgICogaW5uZXJIVE1MIHNlZW1zIHNsb3c6IGh0dHA6Ly9qc3BlcmYuY29tL3Jpb3QtaW5zZXJ0LXN0eWxlXG4gICAgICovXG4gICAgaW5qZWN0OiBmdW5jdGlvbiBpbmplY3QoKSB7XG4gICAgICBpZiAoIVdJTiB8fCAhbmVlZHNJbmplY3QpIHsgcmV0dXJuIH1cbiAgICAgIG5lZWRzSW5qZWN0ID0gZmFsc2U7XG4gICAgICB2YXIgc3R5bGUgPSBPYmplY3Qua2V5cyhieU5hbWUpXG4gICAgICAgIC5tYXAoZnVuY3Rpb24gKGspIHsgcmV0dXJuIGJ5TmFtZVtrXTsgfSlcbiAgICAgICAgLmpvaW4oJ1xcbicpO1xuICAgICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cbiAgICAgIGlmIChjc3NUZXh0UHJvcCkgeyBjc3NUZXh0UHJvcC5jc3NUZXh0ID0gc3R5bGU7IH1cbiAgICAgIGVsc2UgeyBzdHlsZU5vZGUuaW5uZXJIVE1MID0gc3R5bGU7IH1cbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogUmVtb3ZlIGEgdGFnIHN0eWxlIG9mIGluamVjdGVkIERPTSBsYXRlci5cbiAgICAgKiBAcGFyYW0ge1N0cmluZ30gbmFtZSBhIHJlZ2lzdGVyZWQgdGFnbmFtZVxuICAgICAqL1xuICAgIHJlbW92ZTogZnVuY3Rpb24gcmVtb3ZlKG5hbWUpIHtcbiAgICAgIGRlbGV0ZSBieU5hbWVbbmFtZV07XG4gICAgICBuZWVkc0luamVjdCA9IHRydWU7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIFRoZSByaW90IHRlbXBsYXRlIGVuZ2luZVxuICAgKiBAdmVyc2lvbiB2My4wLjhcbiAgICovXG5cbiAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cbiAgdmFyIHNraXBSZWdleCA9IChmdW5jdGlvbiAoKSB7IC8vZXNsaW50LWRpc2FibGUtbGluZSBuby11bnVzZWQtdmFyc1xuXG4gICAgdmFyIGJlZm9yZVJlQ2hhcnMgPSAnW3soLDs6Pz18JiFefj4lKi8nO1xuXG4gICAgdmFyIGJlZm9yZVJlV29yZHMgPSBbXG4gICAgICAnY2FzZScsXG4gICAgICAnZGVmYXVsdCcsXG4gICAgICAnZG8nLFxuICAgICAgJ2Vsc2UnLFxuICAgICAgJ2luJyxcbiAgICAgICdpbnN0YW5jZW9mJyxcbiAgICAgICdwcmVmaXgnLFxuICAgICAgJ3JldHVybicsXG4gICAgICAndHlwZW9mJyxcbiAgICAgICd2b2lkJyxcbiAgICAgICd5aWVsZCdcbiAgICBdO1xuXG4gICAgdmFyIHdvcmRzTGFzdENoYXIgPSBiZWZvcmVSZVdvcmRzLnJlZHVjZShmdW5jdGlvbiAocywgdykge1xuICAgICAgcmV0dXJuIHMgKyB3LnNsaWNlKC0xKVxuICAgIH0sICcnKTtcblxuICAgIHZhciBSRV9SRUdFWCA9IC9eXFwvKD89W14qPi9dKVteWy9cXFxcXSooPzooPzpcXFxcLnxcXFsoPzpcXFxcLnxbXlxcXVxcXFxdKikqXFxdKVteW1xcXFwvXSopKj9cXC9bZ2ltdXldKi87XG4gICAgdmFyIFJFX1ZOX0NIQVIgPSAvWyRcXHddLztcblxuICAgIGZ1bmN0aW9uIHByZXYgKGNvZGUsIHBvcykge1xuICAgICAgd2hpbGUgKC0tcG9zID49IDAgJiYgL1xccy8udGVzdChjb2RlW3Bvc10pKXsgfVxuICAgICAgcmV0dXJuIHBvc1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIF9za2lwUmVnZXggKGNvZGUsIHN0YXJ0KSB7XG5cbiAgICAgIHZhciByZSA9IC8uKi9nO1xuICAgICAgdmFyIHBvcyA9IHJlLmxhc3RJbmRleCA9IHN0YXJ0Kys7XG4gICAgICB2YXIgbWF0Y2ggPSByZS5leGVjKGNvZGUpWzBdLm1hdGNoKFJFX1JFR0VYKTtcblxuICAgICAgaWYgKG1hdGNoKSB7XG4gICAgICAgIHZhciBuZXh0ID0gcG9zICsgbWF0Y2hbMF0ubGVuZ3RoO1xuXG4gICAgICAgIHBvcyA9IHByZXYoY29kZSwgcG9zKTtcbiAgICAgICAgdmFyIGMgPSBjb2RlW3Bvc107XG5cbiAgICAgICAgaWYgKHBvcyA8IDAgfHwgfmJlZm9yZVJlQ2hhcnMuaW5kZXhPZihjKSkge1xuICAgICAgICAgIHJldHVybiBuZXh0XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoYyA9PT0gJy4nKSB7XG5cbiAgICAgICAgICBpZiAoY29kZVtwb3MgLSAxXSA9PT0gJy4nKSB7XG4gICAgICAgICAgICBzdGFydCA9IG5leHQ7XG4gICAgICAgICAgfVxuXG4gICAgICAgIH0gZWxzZSBpZiAoYyA9PT0gJysnIHx8IGMgPT09ICctJykge1xuXG4gICAgICAgICAgaWYgKGNvZGVbLS1wb3NdICE9PSBjIHx8XG4gICAgICAgICAgICAgIChwb3MgPSBwcmV2KGNvZGUsIHBvcykpIDwgMCB8fFxuICAgICAgICAgICAgICAhUkVfVk5fQ0hBUi50ZXN0KGNvZGVbcG9zXSkpIHtcbiAgICAgICAgICAgIHN0YXJ0ID0gbmV4dDtcbiAgICAgICAgICB9XG5cbiAgICAgICAgfSBlbHNlIGlmICh+d29yZHNMYXN0Q2hhci5pbmRleE9mKGMpKSB7XG5cbiAgICAgICAgICB2YXIgZW5kID0gcG9zICsgMTtcblxuICAgICAgICAgIHdoaWxlICgtLXBvcyA+PSAwICYmIFJFX1ZOX0NIQVIudGVzdChjb2RlW3Bvc10pKXsgfVxuICAgICAgICAgIGlmICh+YmVmb3JlUmVXb3Jkcy5pbmRleE9mKGNvZGUuc2xpY2UocG9zICsgMSwgZW5kKSkpIHtcbiAgICAgICAgICAgIHN0YXJ0ID0gbmV4dDtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHN0YXJ0XG4gICAgfVxuXG4gICAgcmV0dXJuIF9za2lwUmVnZXhcblxuICB9KSgpO1xuXG4gIC8qKlxuICAgKiByaW90LnV0aWwuYnJhY2tldHNcbiAgICpcbiAgICogLSBgYnJhY2tldHMgICAgYCAtIFJldHVybnMgYSBzdHJpbmcgb3IgcmVnZXggYmFzZWQgb24gaXRzIHBhcmFtZXRlclxuICAgKiAtIGBicmFja2V0cy5zZXRgIC0gQ2hhbmdlIHRoZSBjdXJyZW50IHJpb3QgYnJhY2tldHNcbiAgICpcbiAgICogQG1vZHVsZVxuICAgKi9cblxuICAvKiBnbG9iYWwgcmlvdCAqL1xuXG4gIC8qIGlzdGFuYnVsIGlnbm9yZSBuZXh0ICovXG4gIHZhciBicmFja2V0cyA9IChmdW5jdGlvbiAoVU5ERUYpIHtcblxuICAgIHZhclxuICAgICAgUkVHTE9CID0gJ2cnLFxuXG4gICAgICBSX01MQ09NTVMgPSAvXFwvXFwqW14qXSpcXCorKD86W14qXFwvXVteKl0qXFwqKykqXFwvL2csXG5cbiAgICAgIFJfU1RSSU5HUyA9IC9cIlteXCJcXFxcXSooPzpcXFxcW1xcU1xcc11bXlwiXFxcXF0qKSpcInwnW14nXFxcXF0qKD86XFxcXFtcXFNcXHNdW14nXFxcXF0qKSonfGBbXmBcXFxcXSooPzpcXFxcW1xcU1xcc11bXmBcXFxcXSopKmAvZyxcblxuICAgICAgU19RQkxPQ0tTID0gUl9TVFJJTkdTLnNvdXJjZSArICd8JyArXG4gICAgICAgIC8oPzpcXGJyZXR1cm5cXHMrfCg/OlskXFx3XFwpXFxdXXxcXCtcXCt8LS0pXFxzKihcXC8pKD8hWypcXC9dKSkvLnNvdXJjZSArICd8JyArXG4gICAgICAgIC9cXC8oPz1bXipcXC9dKVteW1xcL1xcXFxdKig/Oig/OlxcWyg/OlxcXFwufFteXFxdXFxcXF0qKSpcXF18XFxcXC4pW15bXFwvXFxcXF0qKSo/KFtePF1cXC8pW2dpbV0qLy5zb3VyY2UsXG5cbiAgICAgIFVOU1VQUE9SVEVEID0gUmVnRXhwKCdbXFxcXCcgKyAneDAwLVxcXFx4MUY8PmEtekEtWjAtOVxcJ1wiLDtcXFxcXFxcXF0nKSxcblxuICAgICAgTkVFRF9FU0NBUEUgPSAvKD89W1tcXF0oKSorPy5eJHxdKS9nLFxuXG4gICAgICBTX1FCTE9DSzIgPSBSX1NUUklOR1Muc291cmNlICsgJ3wnICsgLyhcXC8pKD8hWypcXC9dKS8uc291cmNlLFxuXG4gICAgICBGSU5EQlJBQ0VTID0ge1xuICAgICAgICAnKCc6IFJlZ0V4cCgnKFsoKV0pfCcgICArIFNfUUJMT0NLMiwgUkVHTE9CKSxcbiAgICAgICAgJ1snOiBSZWdFeHAoJyhbW1xcXFxdXSl8JyArIFNfUUJMT0NLMiwgUkVHTE9CKSxcbiAgICAgICAgJ3snOiBSZWdFeHAoJyhbe31dKXwnICAgKyBTX1FCTE9DSzIsIFJFR0xPQilcbiAgICAgIH0sXG5cbiAgICAgIERFRkFVTFQgPSAneyB9JztcblxuICAgIHZhciBfcGFpcnMgPSBbXG4gICAgICAneycsICd9JyxcbiAgICAgICd7JywgJ30nLFxuICAgICAgL3tbXn1dKn0vLFxuICAgICAgL1xcXFwoW3t9XSkvZyxcbiAgICAgIC9cXFxcKHspfHsvZyxcbiAgICAgIFJlZ0V4cCgnXFxcXFxcXFwofSl8KFtbKHtdKXwofSl8JyArIFNfUUJMT0NLMiwgUkVHTE9CKSxcbiAgICAgIERFRkFVTFQsXG4gICAgICAvXlxccyp7XFxeP1xccyooWyRcXHddKykoPzpcXHMqLFxccyooXFxTKykpP1xccytpblxccysoXFxTLiopXFxzKn0vLFxuICAgICAgLyhefFteXFxcXF0pez1bXFxTXFxzXSo/fS9cbiAgICBdO1xuXG4gICAgdmFyXG4gICAgICBjYWNoZWRCcmFja2V0cyA9IFVOREVGLFxuICAgICAgX3JlZ2V4LFxuICAgICAgX2NhY2hlID0gW10sXG4gICAgICBfc2V0dGluZ3M7XG5cbiAgICBmdW5jdGlvbiBfbG9vcGJhY2sgKHJlKSB7IHJldHVybiByZSB9XG5cbiAgICBmdW5jdGlvbiBfcmV3cml0ZSAocmUsIGJwKSB7XG4gICAgICBpZiAoIWJwKSB7IGJwID0gX2NhY2hlOyB9XG4gICAgICByZXR1cm4gbmV3IFJlZ0V4cChcbiAgICAgICAgcmUuc291cmNlLnJlcGxhY2UoL3svZywgYnBbMl0pLnJlcGxhY2UoL30vZywgYnBbM10pLCByZS5nbG9iYWwgPyBSRUdMT0IgOiAnJ1xuICAgICAgKVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIF9jcmVhdGUgKHBhaXIpIHtcbiAgICAgIGlmIChwYWlyID09PSBERUZBVUxUKSB7IHJldHVybiBfcGFpcnMgfVxuXG4gICAgICB2YXIgYXJyID0gcGFpci5zcGxpdCgnICcpO1xuXG4gICAgICBpZiAoYXJyLmxlbmd0aCAhPT0gMiB8fCBVTlNVUFBPUlRFRC50ZXN0KHBhaXIpKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcignVW5zdXBwb3J0ZWQgYnJhY2tldHMgXCInICsgcGFpciArICdcIicpXG4gICAgICB9XG4gICAgICBhcnIgPSBhcnIuY29uY2F0KHBhaXIucmVwbGFjZShORUVEX0VTQ0FQRSwgJ1xcXFwnKS5zcGxpdCgnICcpKTtcblxuICAgICAgYXJyWzRdID0gX3Jld3JpdGUoYXJyWzFdLmxlbmd0aCA+IDEgPyAve1tcXFNcXHNdKj99LyA6IF9wYWlyc1s0XSwgYXJyKTtcbiAgICAgIGFycls1XSA9IF9yZXdyaXRlKHBhaXIubGVuZ3RoID4gMyA/IC9cXFxcKHt8fSkvZyA6IF9wYWlyc1s1XSwgYXJyKTtcbiAgICAgIGFycls2XSA9IF9yZXdyaXRlKF9wYWlyc1s2XSwgYXJyKTtcbiAgICAgIGFycls3XSA9IFJlZ0V4cCgnXFxcXFxcXFwoJyArIGFyclszXSArICcpfChbWyh7XSl8KCcgKyBhcnJbM10gKyAnKXwnICsgU19RQkxPQ0syLCBSRUdMT0IpO1xuICAgICAgYXJyWzhdID0gcGFpcjtcbiAgICAgIHJldHVybiBhcnJcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBfYnJhY2tldHMgKHJlT3JJZHgpIHtcbiAgICAgIHJldHVybiByZU9ySWR4IGluc3RhbmNlb2YgUmVnRXhwID8gX3JlZ2V4KHJlT3JJZHgpIDogX2NhY2hlW3JlT3JJZHhdXG4gICAgfVxuXG4gICAgX2JyYWNrZXRzLnNwbGl0ID0gZnVuY3Rpb24gc3BsaXQgKHN0ciwgdG1wbCwgX2JwKSB7XG4gICAgICAvLyBpc3RhbmJ1bCBpZ25vcmUgbmV4dDogX2JwIGlzIGZvciB0aGUgY29tcGlsZXJcbiAgICAgIGlmICghX2JwKSB7IF9icCA9IF9jYWNoZTsgfVxuXG4gICAgICB2YXJcbiAgICAgICAgcGFydHMgPSBbXSxcbiAgICAgICAgbWF0Y2gsXG4gICAgICAgIGlzZXhwcixcbiAgICAgICAgc3RhcnQsXG4gICAgICAgIHBvcyxcbiAgICAgICAgcmUgPSBfYnBbNl07XG5cbiAgICAgIHZhciBxYmxvY2tzID0gW107XG4gICAgICB2YXIgcHJldlN0ciA9ICcnO1xuICAgICAgdmFyIG1hcmssIGxhc3RJbmRleDtcblxuICAgICAgaXNleHByID0gc3RhcnQgPSByZS5sYXN0SW5kZXggPSAwO1xuXG4gICAgICB3aGlsZSAoKG1hdGNoID0gcmUuZXhlYyhzdHIpKSkge1xuXG4gICAgICAgIGxhc3RJbmRleCA9IHJlLmxhc3RJbmRleDtcbiAgICAgICAgcG9zID0gbWF0Y2guaW5kZXg7XG5cbiAgICAgICAgaWYgKGlzZXhwcikge1xuXG4gICAgICAgICAgaWYgKG1hdGNoWzJdKSB7XG5cbiAgICAgICAgICAgIHZhciBjaCA9IG1hdGNoWzJdO1xuICAgICAgICAgICAgdmFyIHJlY2ggPSBGSU5EQlJBQ0VTW2NoXTtcbiAgICAgICAgICAgIHZhciBpeCA9IDE7XG5cbiAgICAgICAgICAgIHJlY2gubGFzdEluZGV4ID0gbGFzdEluZGV4O1xuICAgICAgICAgICAgd2hpbGUgKChtYXRjaCA9IHJlY2guZXhlYyhzdHIpKSkge1xuICAgICAgICAgICAgICBpZiAobWF0Y2hbMV0pIHtcbiAgICAgICAgICAgICAgICBpZiAobWF0Y2hbMV0gPT09IGNoKSB7ICsraXg7IH1cbiAgICAgICAgICAgICAgICBlbHNlIGlmICghLS1peCkgeyBicmVhayB9XG4gICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcmVjaC5sYXN0SW5kZXggPSBwdXNoUUJsb2NrKG1hdGNoLmluZGV4LCByZWNoLmxhc3RJbmRleCwgbWF0Y2hbMl0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZS5sYXN0SW5kZXggPSBpeCA/IHN0ci5sZW5ndGggOiByZWNoLmxhc3RJbmRleDtcbiAgICAgICAgICAgIGNvbnRpbnVlXG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYgKCFtYXRjaFszXSkge1xuICAgICAgICAgICAgcmUubGFzdEluZGV4ID0gcHVzaFFCbG9jayhwb3MsIGxhc3RJbmRleCwgbWF0Y2hbNF0pO1xuICAgICAgICAgICAgY29udGludWVcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoIW1hdGNoWzFdKSB7XG4gICAgICAgICAgdW5lc2NhcGVTdHIoc3RyLnNsaWNlKHN0YXJ0LCBwb3MpKTtcbiAgICAgICAgICBzdGFydCA9IHJlLmxhc3RJbmRleDtcbiAgICAgICAgICByZSA9IF9icFs2ICsgKGlzZXhwciBePSAxKV07XG4gICAgICAgICAgcmUubGFzdEluZGV4ID0gc3RhcnQ7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKHN0ciAmJiBzdGFydCA8IHN0ci5sZW5ndGgpIHtcbiAgICAgICAgdW5lc2NhcGVTdHIoc3RyLnNsaWNlKHN0YXJ0KSk7XG4gICAgICB9XG5cbiAgICAgIHBhcnRzLnFibG9ja3MgPSBxYmxvY2tzO1xuXG4gICAgICByZXR1cm4gcGFydHNcblxuICAgICAgZnVuY3Rpb24gdW5lc2NhcGVTdHIgKHMpIHtcbiAgICAgICAgaWYgKHByZXZTdHIpIHtcbiAgICAgICAgICBzID0gcHJldlN0ciArIHM7XG4gICAgICAgICAgcHJldlN0ciA9ICcnO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0bXBsIHx8IGlzZXhwcikge1xuICAgICAgICAgIHBhcnRzLnB1c2gocyAmJiBzLnJlcGxhY2UoX2JwWzVdLCAnJDEnKSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcGFydHMucHVzaChzKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBmdW5jdGlvbiBwdXNoUUJsb2NrKF9wb3MsIF9sYXN0SW5kZXgsIHNsYXNoKSB7IC8vZXNsaW50LWRpc2FibGUtbGluZVxuICAgICAgICBpZiAoc2xhc2gpIHtcbiAgICAgICAgICBfbGFzdEluZGV4ID0gc2tpcFJlZ2V4KHN0ciwgX3Bvcyk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodG1wbCAmJiBfbGFzdEluZGV4ID4gX3BvcyArIDIpIHtcbiAgICAgICAgICBtYXJrID0gJ1xcdTIwNTcnICsgcWJsb2Nrcy5sZW5ndGggKyAnfic7XG4gICAgICAgICAgcWJsb2Nrcy5wdXNoKHN0ci5zbGljZShfcG9zLCBfbGFzdEluZGV4KSk7XG4gICAgICAgICAgcHJldlN0ciArPSBzdHIuc2xpY2Uoc3RhcnQsIF9wb3MpICsgbWFyaztcbiAgICAgICAgICBzdGFydCA9IF9sYXN0SW5kZXg7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIF9sYXN0SW5kZXhcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgX2JyYWNrZXRzLmhhc0V4cHIgPSBmdW5jdGlvbiBoYXNFeHByIChzdHIpIHtcbiAgICAgIHJldHVybiBfY2FjaGVbNF0udGVzdChzdHIpXG4gICAgfTtcblxuICAgIF9icmFja2V0cy5sb29wS2V5cyA9IGZ1bmN0aW9uIGxvb3BLZXlzIChleHByKSB7XG4gICAgICB2YXIgbSA9IGV4cHIubWF0Y2goX2NhY2hlWzldKTtcblxuICAgICAgcmV0dXJuIG1cbiAgICAgICAgPyB7IGtleTogbVsxXSwgcG9zOiBtWzJdLCB2YWw6IF9jYWNoZVswXSArIG1bM10udHJpbSgpICsgX2NhY2hlWzFdIH1cbiAgICAgICAgOiB7IHZhbDogZXhwci50cmltKCkgfVxuICAgIH07XG5cbiAgICBfYnJhY2tldHMuYXJyYXkgPSBmdW5jdGlvbiBhcnJheSAocGFpcikge1xuICAgICAgcmV0dXJuIHBhaXIgPyBfY3JlYXRlKHBhaXIpIDogX2NhY2hlXG4gICAgfTtcblxuICAgIGZ1bmN0aW9uIF9yZXNldCAocGFpcikge1xuICAgICAgaWYgKChwYWlyIHx8IChwYWlyID0gREVGQVVMVCkpICE9PSBfY2FjaGVbOF0pIHtcbiAgICAgICAgX2NhY2hlID0gX2NyZWF0ZShwYWlyKTtcbiAgICAgICAgX3JlZ2V4ID0gcGFpciA9PT0gREVGQVVMVCA/IF9sb29wYmFjayA6IF9yZXdyaXRlO1xuICAgICAgICBfY2FjaGVbOV0gPSBfcmVnZXgoX3BhaXJzWzldKTtcbiAgICAgIH1cbiAgICAgIGNhY2hlZEJyYWNrZXRzID0gcGFpcjtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBfc2V0U2V0dGluZ3MgKG8pIHtcbiAgICAgIHZhciBiO1xuXG4gICAgICBvID0gbyB8fCB7fTtcbiAgICAgIGIgPSBvLmJyYWNrZXRzO1xuICAgICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KG8sICdicmFja2V0cycsIHtcbiAgICAgICAgc2V0OiBfcmVzZXQsXG4gICAgICAgIGdldDogZnVuY3Rpb24gKCkgeyByZXR1cm4gY2FjaGVkQnJhY2tldHMgfSxcbiAgICAgICAgZW51bWVyYWJsZTogdHJ1ZVxuICAgICAgfSk7XG4gICAgICBfc2V0dGluZ3MgPSBvO1xuICAgICAgX3Jlc2V0KGIpO1xuICAgIH1cblxuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShfYnJhY2tldHMsICdzZXR0aW5ncycsIHtcbiAgICAgIHNldDogX3NldFNldHRpbmdzLFxuICAgICAgZ2V0OiBmdW5jdGlvbiAoKSB7IHJldHVybiBfc2V0dGluZ3MgfVxuICAgIH0pO1xuXG4gICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQ6IGluIHRoZSBicm93c2VyIHJpb3QgaXMgYWx3YXlzIGluIHRoZSBzY29wZSAqL1xuICAgIF9icmFja2V0cy5zZXR0aW5ncyA9IHR5cGVvZiByaW90ICE9PSAndW5kZWZpbmVkJyAmJiByaW90LnNldHRpbmdzIHx8IHt9O1xuICAgIF9icmFja2V0cy5zZXQgPSBfcmVzZXQ7XG4gICAgX2JyYWNrZXRzLnNraXBSZWdleCA9IHNraXBSZWdleDtcblxuICAgIF9icmFja2V0cy5SX1NUUklOR1MgPSBSX1NUUklOR1M7XG4gICAgX2JyYWNrZXRzLlJfTUxDT01NUyA9IFJfTUxDT01NUztcbiAgICBfYnJhY2tldHMuU19RQkxPQ0tTID0gU19RQkxPQ0tTO1xuICAgIF9icmFja2V0cy5TX1FCTE9DSzIgPSBTX1FCTE9DSzI7XG5cbiAgICByZXR1cm4gX2JyYWNrZXRzXG5cbiAgfSkoKTtcblxuICAvKipcbiAgICogQG1vZHVsZSB0bXBsXG4gICAqXG4gICAqIHRtcGwgICAgICAgICAgLSBSb290IGZ1bmN0aW9uLCByZXR1cm5zIHRoZSB0ZW1wbGF0ZSB2YWx1ZSwgcmVuZGVyIHdpdGggZGF0YVxuICAgKiB0bXBsLmhhc0V4cHIgIC0gVGVzdCB0aGUgZXhpc3RlbmNlIG9mIGEgZXhwcmVzc2lvbiBpbnNpZGUgYSBzdHJpbmdcbiAgICogdG1wbC5sb29wS2V5cyAtIEdldCB0aGUga2V5cyBmb3IgYW4gJ2VhY2gnIGxvb3AgKHVzZWQgYnkgYF9lYWNoYClcbiAgICovXG5cbiAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cbiAgdmFyIHRtcGwgPSAoZnVuY3Rpb24gKCkge1xuXG4gICAgdmFyIF9jYWNoZSA9IHt9O1xuXG4gICAgZnVuY3Rpb24gX3RtcGwgKHN0ciwgZGF0YSkge1xuICAgICAgaWYgKCFzdHIpIHsgcmV0dXJuIHN0ciB9XG5cbiAgICAgIHJldHVybiAoX2NhY2hlW3N0cl0gfHwgKF9jYWNoZVtzdHJdID0gX2NyZWF0ZShzdHIpKSkuY2FsbChcbiAgICAgICAgZGF0YSwgX2xvZ0Vyci5iaW5kKHtcbiAgICAgICAgICBkYXRhOiBkYXRhLFxuICAgICAgICAgIHRtcGw6IHN0clxuICAgICAgICB9KVxuICAgICAgKVxuICAgIH1cblxuICAgIF90bXBsLmhhc0V4cHIgPSBicmFja2V0cy5oYXNFeHByO1xuXG4gICAgX3RtcGwubG9vcEtleXMgPSBicmFja2V0cy5sb29wS2V5cztcblxuICAgIC8vIGlzdGFuYnVsIGlnbm9yZSBuZXh0XG4gICAgX3RtcGwuY2xlYXJDYWNoZSA9IGZ1bmN0aW9uICgpIHsgX2NhY2hlID0ge307IH07XG5cbiAgICBfdG1wbC5lcnJvckhhbmRsZXIgPSBudWxsO1xuXG4gICAgZnVuY3Rpb24gX2xvZ0VyciAoZXJyLCBjdHgpIHtcblxuICAgICAgZXJyLnJpb3REYXRhID0ge1xuICAgICAgICB0YWdOYW1lOiBjdHggJiYgY3R4Ll9fICYmIGN0eC5fXy50YWdOYW1lLFxuICAgICAgICBfcmlvdF9pZDogY3R4ICYmIGN0eC5fcmlvdF9pZCAgLy9lc2xpbnQtZGlzYWJsZS1saW5lIGNhbWVsY2FzZVxuICAgICAgfTtcblxuICAgICAgaWYgKF90bXBsLmVycm9ySGFuZGxlcikgeyBfdG1wbC5lcnJvckhhbmRsZXIoZXJyKTsgfVxuICAgICAgZWxzZSBpZiAoXG4gICAgICAgIHR5cGVvZiBjb25zb2xlICE9PSAndW5kZWZpbmVkJyAmJlxuICAgICAgICB0eXBlb2YgY29uc29sZS5lcnJvciA9PT0gJ2Z1bmN0aW9uJ1xuICAgICAgKSB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyLm1lc3NhZ2UpO1xuICAgICAgICBjb25zb2xlLmxvZygnPCVzPiAlcycsIGVyci5yaW90RGF0YS50YWdOYW1lIHx8ICdVbmtub3duIHRhZycsIHRoaXMudG1wbCk7IC8vIGVzbGludC1kaXNhYmxlLWxpbmVcbiAgICAgICAgY29uc29sZS5sb2codGhpcy5kYXRhKTsgLy8gZXNsaW50LWRpc2FibGUtbGluZVxuICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIF9jcmVhdGUgKHN0cikge1xuICAgICAgdmFyIGV4cHIgPSBfZ2V0VG1wbChzdHIpO1xuXG4gICAgICBpZiAoZXhwci5zbGljZSgwLCAxMSkgIT09ICd0cnl7cmV0dXJuICcpIHsgZXhwciA9ICdyZXR1cm4gJyArIGV4cHI7IH1cblxuICAgICAgcmV0dXJuIG5ldyBGdW5jdGlvbignRScsIGV4cHIgKyAnOycpICAgIC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tbmV3LWZ1bmNcbiAgICB9XG5cbiAgICB2YXIgUkVfRFFVT1RFID0gL1xcdTIwNTcvZztcbiAgICB2YXIgUkVfUUJNQVJLID0gL1xcdTIwNTcoXFxkKyl+L2c7XG5cbiAgICBmdW5jdGlvbiBfZ2V0VG1wbCAoc3RyKSB7XG4gICAgICB2YXIgcGFydHMgPSBicmFja2V0cy5zcGxpdChzdHIucmVwbGFjZShSRV9EUVVPVEUsICdcIicpLCAxKTtcbiAgICAgIHZhciBxc3RyID0gcGFydHMucWJsb2NrcztcbiAgICAgIHZhciBleHByO1xuXG4gICAgICBpZiAocGFydHMubGVuZ3RoID4gMiB8fCBwYXJ0c1swXSkge1xuICAgICAgICB2YXIgaSwgaiwgbGlzdCA9IFtdO1xuXG4gICAgICAgIGZvciAoaSA9IGogPSAwOyBpIDwgcGFydHMubGVuZ3RoOyArK2kpIHtcblxuICAgICAgICAgIGV4cHIgPSBwYXJ0c1tpXTtcblxuICAgICAgICAgIGlmIChleHByICYmIChleHByID0gaSAmIDFcblxuICAgICAgICAgICAgICA/IF9wYXJzZUV4cHIoZXhwciwgMSwgcXN0cilcblxuICAgICAgICAgICAgICA6ICdcIicgKyBleHByXG4gICAgICAgICAgICAgICAgICAucmVwbGFjZSgvXFxcXC9nLCAnXFxcXFxcXFwnKVxuICAgICAgICAgICAgICAgICAgLnJlcGxhY2UoL1xcclxcbj98XFxuL2csICdcXFxcbicpXG4gICAgICAgICAgICAgICAgICAucmVwbGFjZSgvXCIvZywgJ1xcXFxcIicpICtcbiAgICAgICAgICAgICAgICAnXCInXG5cbiAgICAgICAgICAgICkpIHsgbGlzdFtqKytdID0gZXhwcjsgfVxuXG4gICAgICAgIH1cblxuICAgICAgICBleHByID0gaiA8IDIgPyBsaXN0WzBdXG4gICAgICAgICAgICAgOiAnWycgKyBsaXN0LmpvaW4oJywnKSArICddLmpvaW4oXCJcIiknO1xuXG4gICAgICB9IGVsc2Uge1xuXG4gICAgICAgIGV4cHIgPSBfcGFyc2VFeHByKHBhcnRzWzFdLCAwLCBxc3RyKTtcbiAgICAgIH1cblxuICAgICAgaWYgKHFzdHIubGVuZ3RoKSB7XG4gICAgICAgIGV4cHIgPSBleHByLnJlcGxhY2UoUkVfUUJNQVJLLCBmdW5jdGlvbiAoXywgcG9zKSB7XG4gICAgICAgICAgcmV0dXJuIHFzdHJbcG9zXVxuICAgICAgICAgICAgLnJlcGxhY2UoL1xcci9nLCAnXFxcXHInKVxuICAgICAgICAgICAgLnJlcGxhY2UoL1xcbi9nLCAnXFxcXG4nKVxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBleHByXG4gICAgfVxuXG4gICAgdmFyIFJFX0NTTkFNRSA9IC9eKD86KC0/W19BLVphLXpcXHhBMC1cXHhGRl1bLVxcd1xceEEwLVxceEZGXSopfFxcdTIwNTcoXFxkKyl+KTovO1xuICAgIHZhclxuICAgICAgUkVfQlJFTkQgPSB7XG4gICAgICAgICcoJzogL1soKV0vZyxcbiAgICAgICAgJ1snOiAvW1tcXF1dL2csXG4gICAgICAgICd7JzogL1t7fV0vZ1xuICAgICAgfTtcblxuICAgIGZ1bmN0aW9uIF9wYXJzZUV4cHIgKGV4cHIsIGFzVGV4dCwgcXN0cikge1xuXG4gICAgICBleHByID0gZXhwclxuICAgICAgICAucmVwbGFjZSgvXFxzKy9nLCAnICcpLnRyaW0oKVxuICAgICAgICAucmVwbGFjZSgvXFwgPyhbW1xcKHt9LD9cXC46XSlcXCA/L2csICckMScpO1xuXG4gICAgICBpZiAoZXhwcikge1xuICAgICAgICB2YXJcbiAgICAgICAgICBsaXN0ID0gW10sXG4gICAgICAgICAgY250ID0gMCxcbiAgICAgICAgICBtYXRjaDtcblxuICAgICAgICB3aGlsZSAoZXhwciAmJlxuICAgICAgICAgICAgICAobWF0Y2ggPSBleHByLm1hdGNoKFJFX0NTTkFNRSkpICYmXG4gICAgICAgICAgICAgICFtYXRjaC5pbmRleFxuICAgICAgICAgICkge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAga2V5LFxuICAgICAgICAgICAganNiLFxuICAgICAgICAgICAgcmUgPSAvLHwoW1t7KF0pfCQvZztcblxuICAgICAgICAgIGV4cHIgPSBSZWdFeHAucmlnaHRDb250ZXh0O1xuICAgICAgICAgIGtleSAgPSBtYXRjaFsyXSA/IHFzdHJbbWF0Y2hbMl1dLnNsaWNlKDEsIC0xKS50cmltKCkucmVwbGFjZSgvXFxzKy9nLCAnICcpIDogbWF0Y2hbMV07XG5cbiAgICAgICAgICB3aGlsZSAoanNiID0gKG1hdGNoID0gcmUuZXhlYyhleHByKSlbMV0pIHsgc2tpcEJyYWNlcyhqc2IsIHJlKTsgfVxuXG4gICAgICAgICAganNiICA9IGV4cHIuc2xpY2UoMCwgbWF0Y2guaW5kZXgpO1xuICAgICAgICAgIGV4cHIgPSBSZWdFeHAucmlnaHRDb250ZXh0O1xuXG4gICAgICAgICAgbGlzdFtjbnQrK10gPSBfd3JhcEV4cHIoanNiLCAxLCBrZXkpO1xuICAgICAgICB9XG5cbiAgICAgICAgZXhwciA9ICFjbnQgPyBfd3JhcEV4cHIoZXhwciwgYXNUZXh0KVxuICAgICAgICAgICAgIDogY250ID4gMSA/ICdbJyArIGxpc3Quam9pbignLCcpICsgJ10uam9pbihcIiBcIikudHJpbSgpJyA6IGxpc3RbMF07XG4gICAgICB9XG4gICAgICByZXR1cm4gZXhwclxuXG4gICAgICBmdW5jdGlvbiBza2lwQnJhY2VzIChjaCwgcmUpIHtcbiAgICAgICAgdmFyXG4gICAgICAgICAgbW0sXG4gICAgICAgICAgbHYgPSAxLFxuICAgICAgICAgIGlyID0gUkVfQlJFTkRbY2hdO1xuXG4gICAgICAgIGlyLmxhc3RJbmRleCA9IHJlLmxhc3RJbmRleDtcbiAgICAgICAgd2hpbGUgKG1tID0gaXIuZXhlYyhleHByKSkge1xuICAgICAgICAgIGlmIChtbVswXSA9PT0gY2gpIHsgKytsdjsgfVxuICAgICAgICAgIGVsc2UgaWYgKCEtLWx2KSB7IGJyZWFrIH1cbiAgICAgICAgfVxuICAgICAgICByZS5sYXN0SW5kZXggPSBsdiA/IGV4cHIubGVuZ3RoIDogaXIubGFzdEluZGV4O1xuICAgICAgfVxuICAgIH1cblxuICAgIC8vIGlzdGFuYnVsIGlnbm9yZSBuZXh0OiBub3QgYm90aFxuICAgIHZhciAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbWF4LWxlblxuICAgICAgSlNfQ09OVEVYVCA9ICdcImluIHRoaXM/dGhpczonICsgKHR5cGVvZiB3aW5kb3cgIT09ICdvYmplY3QnID8gJ2dsb2JhbCcgOiAnd2luZG93JykgKyAnKS4nLFxuICAgICAgSlNfVkFSTkFNRSA9IC9bLHtdW1xcJFxcd10rKD89Oil8KF4gKnxbXiRcXHdcXC57XSkoPyEoPzp0eXBlb2Z8dHJ1ZXxmYWxzZXxudWxsfHVuZGVmaW5lZHxpbnxpbnN0YW5jZW9mfGlzKD86RmluaXRlfE5hTil8dm9pZHxOYU58bmV3fERhdGV8UmVnRXhwfE1hdGgpKD8hWyRcXHddKSkoWyRfQS1aYS16XVskXFx3XSopL2csXG4gICAgICBKU19OT1BST1BTID0gL14oPz0oXFwuWyRcXHddKykpXFwxKD86W14uWyhdfCQpLztcblxuICAgIGZ1bmN0aW9uIF93cmFwRXhwciAoZXhwciwgYXNUZXh0LCBrZXkpIHtcbiAgICAgIHZhciB0YjtcblxuICAgICAgZXhwciA9IGV4cHIucmVwbGFjZShKU19WQVJOQU1FLCBmdW5jdGlvbiAobWF0Y2gsIHAsIG12YXIsIHBvcywgcykge1xuICAgICAgICBpZiAobXZhcikge1xuICAgICAgICAgIHBvcyA9IHRiID8gMCA6IHBvcyArIG1hdGNoLmxlbmd0aDtcblxuICAgICAgICAgIGlmIChtdmFyICE9PSAndGhpcycgJiYgbXZhciAhPT0gJ2dsb2JhbCcgJiYgbXZhciAhPT0gJ3dpbmRvdycpIHtcbiAgICAgICAgICAgIG1hdGNoID0gcCArICcoXCInICsgbXZhciArIEpTX0NPTlRFWFQgKyBtdmFyO1xuICAgICAgICAgICAgaWYgKHBvcykgeyB0YiA9IChzID0gc1twb3NdKSA9PT0gJy4nIHx8IHMgPT09ICcoJyB8fCBzID09PSAnWyc7IH1cbiAgICAgICAgICB9IGVsc2UgaWYgKHBvcykge1xuICAgICAgICAgICAgdGIgPSAhSlNfTk9QUk9QUy50ZXN0KHMuc2xpY2UocG9zKSk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBtYXRjaFxuICAgICAgfSk7XG5cbiAgICAgIGlmICh0Yikge1xuICAgICAgICBleHByID0gJ3RyeXtyZXR1cm4gJyArIGV4cHIgKyAnfWNhdGNoKGUpe0UoZSx0aGlzKX0nO1xuICAgICAgfVxuXG4gICAgICBpZiAoa2V5KSB7XG5cbiAgICAgICAgZXhwciA9ICh0YlxuICAgICAgICAgICAgPyAnZnVuY3Rpb24oKXsnICsgZXhwciArICd9LmNhbGwodGhpcyknIDogJygnICsgZXhwciArICcpJ1xuICAgICAgICAgICkgKyAnP1wiJyArIGtleSArICdcIjpcIlwiJztcblxuICAgICAgfSBlbHNlIGlmIChhc1RleHQpIHtcblxuICAgICAgICBleHByID0gJ2Z1bmN0aW9uKHYpeycgKyAodGJcbiAgICAgICAgICAgID8gZXhwci5yZXBsYWNlKCdyZXR1cm4gJywgJ3Y9JykgOiAndj0oJyArIGV4cHIgKyAnKSdcbiAgICAgICAgICApICsgJztyZXR1cm4gdnx8dj09PTA/djpcIlwifS5jYWxsKHRoaXMpJztcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGV4cHJcbiAgICB9XG5cbiAgICBfdG1wbC52ZXJzaW9uID0gYnJhY2tldHMudmVyc2lvbiA9ICd2My4wLjgnO1xuXG4gICAgcmV0dXJuIF90bXBsXG5cbiAgfSkoKTtcblxuICAvKiBpc3RhbmJ1bCBpZ25vcmUgbmV4dCAqL1xuICB2YXIgb2JzZXJ2YWJsZSA9IGZ1bmN0aW9uKGVsKSB7XG5cbiAgICAvKipcbiAgICAgKiBFeHRlbmQgdGhlIG9yaWdpbmFsIG9iamVjdCBvciBjcmVhdGUgYSBuZXcgZW1wdHkgb25lXG4gICAgICogQHR5cGUgeyBPYmplY3QgfVxuICAgICAqL1xuXG4gICAgZWwgPSBlbCB8fCB7fTtcblxuICAgIC8qKlxuICAgICAqIFByaXZhdGUgdmFyaWFibGVzXG4gICAgICovXG4gICAgdmFyIGNhbGxiYWNrcyA9IHt9LFxuICAgICAgc2xpY2UgPSBBcnJheS5wcm90b3R5cGUuc2xpY2U7XG5cbiAgICAvKipcbiAgICAgKiBQdWJsaWMgQXBpXG4gICAgICovXG5cbiAgICAvLyBleHRlbmQgdGhlIGVsIG9iamVjdCBhZGRpbmcgdGhlIG9ic2VydmFibGUgbWV0aG9kc1xuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0aWVzKGVsLCB7XG4gICAgICAvKipcbiAgICAgICAqIExpc3RlbiB0byB0aGUgZ2l2ZW4gYGV2ZW50YCBhbmRzXG4gICAgICAgKiBleGVjdXRlIHRoZSBgY2FsbGJhY2tgIGVhY2ggdGltZSBhbiBldmVudCBpcyB0cmlnZ2VyZWQuXG4gICAgICAgKiBAcGFyYW0gIHsgU3RyaW5nIH0gZXZlbnQgLSBldmVudCBpZFxuICAgICAgICogQHBhcmFtICB7IEZ1bmN0aW9uIH0gZm4gLSBjYWxsYmFjayBmdW5jdGlvblxuICAgICAgICogQHJldHVybnMgeyBPYmplY3QgfSBlbFxuICAgICAgICovXG4gICAgICBvbjoge1xuICAgICAgICB2YWx1ZTogZnVuY3Rpb24oZXZlbnQsIGZuKSB7XG4gICAgICAgICAgaWYgKHR5cGVvZiBmbiA9PSAnZnVuY3Rpb24nKVxuICAgICAgICAgICAgeyAoY2FsbGJhY2tzW2V2ZW50XSA9IGNhbGxiYWNrc1tldmVudF0gfHwgW10pLnB1c2goZm4pOyB9XG4gICAgICAgICAgcmV0dXJuIGVsXG4gICAgICAgIH0sXG4gICAgICAgIGVudW1lcmFibGU6IGZhbHNlLFxuICAgICAgICB3cml0YWJsZTogZmFsc2UsXG4gICAgICAgIGNvbmZpZ3VyYWJsZTogZmFsc2VcbiAgICAgIH0sXG5cbiAgICAgIC8qKlxuICAgICAgICogUmVtb3ZlcyB0aGUgZ2l2ZW4gYGV2ZW50YCBsaXN0ZW5lcnNcbiAgICAgICAqIEBwYXJhbSAgIHsgU3RyaW5nIH0gZXZlbnQgLSBldmVudCBpZFxuICAgICAgICogQHBhcmFtICAgeyBGdW5jdGlvbiB9IGZuIC0gY2FsbGJhY2sgZnVuY3Rpb25cbiAgICAgICAqIEByZXR1cm5zIHsgT2JqZWN0IH0gZWxcbiAgICAgICAqL1xuICAgICAgb2ZmOiB7XG4gICAgICAgIHZhbHVlOiBmdW5jdGlvbihldmVudCwgZm4pIHtcbiAgICAgICAgICBpZiAoZXZlbnQgPT0gJyonICYmICFmbikgeyBjYWxsYmFja3MgPSB7fTsgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgaWYgKGZuKSB7XG4gICAgICAgICAgICAgIHZhciBhcnIgPSBjYWxsYmFja3NbZXZlbnRdO1xuICAgICAgICAgICAgICBmb3IgKHZhciBpID0gMCwgY2I7IGNiID0gYXJyICYmIGFycltpXTsgKytpKSB7XG4gICAgICAgICAgICAgICAgaWYgKGNiID09IGZuKSB7IGFyci5zcGxpY2UoaS0tLCAxKTsgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2UgeyBkZWxldGUgY2FsbGJhY2tzW2V2ZW50XTsgfVxuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gZWxcbiAgICAgICAgfSxcbiAgICAgICAgZW51bWVyYWJsZTogZmFsc2UsXG4gICAgICAgIHdyaXRhYmxlOiBmYWxzZSxcbiAgICAgICAgY29uZmlndXJhYmxlOiBmYWxzZVxuICAgICAgfSxcblxuICAgICAgLyoqXG4gICAgICAgKiBMaXN0ZW4gdG8gdGhlIGdpdmVuIGBldmVudGAgYW5kXG4gICAgICAgKiBleGVjdXRlIHRoZSBgY2FsbGJhY2tgIGF0IG1vc3Qgb25jZVxuICAgICAgICogQHBhcmFtICAgeyBTdHJpbmcgfSBldmVudCAtIGV2ZW50IGlkXG4gICAgICAgKiBAcGFyYW0gICB7IEZ1bmN0aW9uIH0gZm4gLSBjYWxsYmFjayBmdW5jdGlvblxuICAgICAgICogQHJldHVybnMgeyBPYmplY3QgfSBlbFxuICAgICAgICovXG4gICAgICBvbmU6IHtcbiAgICAgICAgdmFsdWU6IGZ1bmN0aW9uKGV2ZW50LCBmbikge1xuICAgICAgICAgIGZ1bmN0aW9uIG9uKCkge1xuICAgICAgICAgICAgZWwub2ZmKGV2ZW50LCBvbik7XG4gICAgICAgICAgICBmbi5hcHBseShlbCwgYXJndW1lbnRzKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIGVsLm9uKGV2ZW50LCBvbilcbiAgICAgICAgfSxcbiAgICAgICAgZW51bWVyYWJsZTogZmFsc2UsXG4gICAgICAgIHdyaXRhYmxlOiBmYWxzZSxcbiAgICAgICAgY29uZmlndXJhYmxlOiBmYWxzZVxuICAgICAgfSxcblxuICAgICAgLyoqXG4gICAgICAgKiBFeGVjdXRlIGFsbCBjYWxsYmFjayBmdW5jdGlvbnMgdGhhdCBsaXN0ZW4gdG9cbiAgICAgICAqIHRoZSBnaXZlbiBgZXZlbnRgXG4gICAgICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IGV2ZW50IC0gZXZlbnQgaWRcbiAgICAgICAqIEByZXR1cm5zIHsgT2JqZWN0IH0gZWxcbiAgICAgICAqL1xuICAgICAgdHJpZ2dlcjoge1xuICAgICAgICB2YWx1ZTogZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICB2YXIgYXJndW1lbnRzJDEgPSBhcmd1bWVudHM7XG5cblxuICAgICAgICAgIC8vIGdldHRpbmcgdGhlIGFyZ3VtZW50c1xuICAgICAgICAgIHZhciBhcmdsZW4gPSBhcmd1bWVudHMubGVuZ3RoIC0gMSxcbiAgICAgICAgICAgIGFyZ3MgPSBuZXcgQXJyYXkoYXJnbGVuKSxcbiAgICAgICAgICAgIGZucyxcbiAgICAgICAgICAgIGZuLFxuICAgICAgICAgICAgaTtcblxuICAgICAgICAgIGZvciAoaSA9IDA7IGkgPCBhcmdsZW47IGkrKykge1xuICAgICAgICAgICAgYXJnc1tpXSA9IGFyZ3VtZW50cyQxW2kgKyAxXTsgLy8gc2tpcCBmaXJzdCBhcmd1bWVudFxuICAgICAgICAgIH1cblxuICAgICAgICAgIGZucyA9IHNsaWNlLmNhbGwoY2FsbGJhY2tzW2V2ZW50XSB8fCBbXSwgMCk7XG5cbiAgICAgICAgICBmb3IgKGkgPSAwOyBmbiA9IGZuc1tpXTsgKytpKSB7XG4gICAgICAgICAgICBmbi5hcHBseShlbCwgYXJncyk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYgKGNhbGxiYWNrc1snKiddICYmIGV2ZW50ICE9ICcqJylcbiAgICAgICAgICAgIHsgZWwudHJpZ2dlci5hcHBseShlbCwgWycqJywgZXZlbnRdLmNvbmNhdChhcmdzKSk7IH1cblxuICAgICAgICAgIHJldHVybiBlbFxuICAgICAgICB9LFxuICAgICAgICBlbnVtZXJhYmxlOiBmYWxzZSxcbiAgICAgICAgd3JpdGFibGU6IGZhbHNlLFxuICAgICAgICBjb25maWd1cmFibGU6IGZhbHNlXG4gICAgICB9XG4gICAgfSk7XG5cbiAgICByZXR1cm4gZWxcblxuICB9O1xuXG4gIC8qKlxuICAgKiBTaG9ydCBhbGlhcyBmb3IgT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvclxuICAgKi9cbiAgZnVuY3Rpb24gZ2V0UHJvcERlc2NyaXB0b3IgKG8sIGspIHtcbiAgICByZXR1cm4gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihvLCBrKVxuICB9XG5cbiAgLyoqXG4gICAqIENoZWNrIGlmIHBhc3NlZCBhcmd1bWVudCBpcyB1bmRlZmluZWRcbiAgICogQHBhcmFtICAgeyAqIH0gdmFsdWUgLVxuICAgKiBAcmV0dXJucyB7IEJvb2xlYW4gfSAtXG4gICAqL1xuICBmdW5jdGlvbiBpc1VuZGVmaW5lZCh2YWx1ZSkge1xuICAgIHJldHVybiB0eXBlb2YgdmFsdWUgPT09IFRfVU5ERUZcbiAgfVxuXG4gIC8qKlxuICAgKiBDaGVjayB3aGV0aGVyIG9iamVjdCdzIHByb3BlcnR5IGNvdWxkIGJlIG92ZXJyaWRkZW5cbiAgICogQHBhcmFtICAgeyBPYmplY3QgfSAgb2JqIC0gc291cmNlIG9iamVjdFxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9ICBrZXkgLSBvYmplY3QgcHJvcGVydHlcbiAgICogQHJldHVybnMgeyBCb29sZWFuIH0gdHJ1ZSBpZiB3cml0YWJsZVxuICAgKi9cbiAgZnVuY3Rpb24gaXNXcml0YWJsZShvYmosIGtleSkge1xuICAgIHZhciBkZXNjcmlwdG9yID0gZ2V0UHJvcERlc2NyaXB0b3Iob2JqLCBrZXkpO1xuICAgIHJldHVybiBpc1VuZGVmaW5lZChvYmpba2V5XSkgfHwgZGVzY3JpcHRvciAmJiBkZXNjcmlwdG9yLndyaXRhYmxlXG4gIH1cblxuICAvKipcbiAgICogRXh0ZW5kIGFueSBvYmplY3Qgd2l0aCBvdGhlciBwcm9wZXJ0aWVzXG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gc3JjIC0gc291cmNlIG9iamVjdFxuICAgKiBAcmV0dXJucyB7IE9iamVjdCB9IHRoZSByZXN1bHRpbmcgZXh0ZW5kZWQgb2JqZWN0XG4gICAqXG4gICAqIHZhciBvYmogPSB7IGZvbzogJ2JheicgfVxuICAgKiBleHRlbmQob2JqLCB7YmFyOiAnYmFyJywgZm9vOiAnYmFyJ30pXG4gICAqIGNvbnNvbGUubG9nKG9iaikgPT4ge2JhcjogJ2JhcicsIGZvbzogJ2Jhcid9XG4gICAqXG4gICAqL1xuICBmdW5jdGlvbiBleHRlbmQoc3JjKSB7XG4gICAgdmFyIG9iajtcbiAgICB2YXIgaSA9IDE7XG4gICAgdmFyIGFyZ3MgPSBhcmd1bWVudHM7XG4gICAgdmFyIGwgPSBhcmdzLmxlbmd0aDtcblxuICAgIGZvciAoOyBpIDwgbDsgaSsrKSB7XG4gICAgICBpZiAob2JqID0gYXJnc1tpXSkge1xuICAgICAgICBmb3IgKHZhciBrZXkgaW4gb2JqKSB7XG4gICAgICAgICAgLy8gY2hlY2sgaWYgdGhpcyBwcm9wZXJ0eSBvZiB0aGUgc291cmNlIG9iamVjdCBjb3VsZCBiZSBvdmVycmlkZGVuXG4gICAgICAgICAgaWYgKGlzV3JpdGFibGUoc3JjLCBrZXkpKVxuICAgICAgICAgICAgeyBzcmNba2V5XSA9IG9ialtrZXldOyB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHNyY1xuICB9XG5cbiAgLyoqXG4gICAqIEFsaWFzIGZvciBPYmplY3QuY3JlYXRlXG4gICAqL1xuICBmdW5jdGlvbiBjcmVhdGUoc3JjKSB7XG4gICAgcmV0dXJuIE9iamVjdC5jcmVhdGUoc3JjKVxuICB9XG5cbiAgdmFyIHNldHRpbmdzID0gZXh0ZW5kKGNyZWF0ZShicmFja2V0cy5zZXR0aW5ncyksIHtcbiAgICBza2lwQW5vbnltb3VzVGFnczogdHJ1ZSxcbiAgICAvLyBoYW5kbGUgdGhlIGF1dG8gdXBkYXRlcyBvbiBhbnkgRE9NIGV2ZW50XG4gICAgYXV0b1VwZGF0ZTogdHJ1ZVxuICB9KVxuXG4gIC8qKlxuICAgKiBTaG9ydGVyIGFuZCBmYXN0IHdheSB0byBzZWxlY3QgbXVsdGlwbGUgbm9kZXMgaW4gdGhlIERPTVxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IHNlbGVjdG9yIC0gRE9NIHNlbGVjdG9yXG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gY3R4IC0gRE9NIG5vZGUgd2hlcmUgdGhlIHRhcmdldHMgb2Ygb3VyIHNlYXJjaCB3aWxsIGlzIGxvY2F0ZWRcbiAgICogQHJldHVybnMgeyBPYmplY3QgfSBkb20gbm9kZXMgZm91bmRcbiAgICovXG4gIGZ1bmN0aW9uICQkKHNlbGVjdG9yLCBjdHgpIHtcbiAgICByZXR1cm4gW10uc2xpY2UuY2FsbCgoY3R4IHx8IGRvY3VtZW50KS5xdWVyeVNlbGVjdG9yQWxsKHNlbGVjdG9yKSlcbiAgfVxuXG4gIC8qKlxuICAgKiBDcmVhdGUgYSBkb2N1bWVudCB0ZXh0IG5vZGVcbiAgICogQHJldHVybnMgeyBPYmplY3QgfSBjcmVhdGUgYSB0ZXh0IG5vZGUgdG8gdXNlIGFzIHBsYWNlaG9sZGVyXG4gICAqL1xuICBmdW5jdGlvbiBjcmVhdGVET01QbGFjZWhvbGRlcigpIHtcbiAgICByZXR1cm4gZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoJycpXG4gIH1cblxuICAvKipcbiAgICogVG9nZ2xlIHRoZSB2aXNpYmlsaXR5IG9mIGFueSBET00gbm9kZVxuICAgKiBAcGFyYW0gICB7IE9iamVjdCB9ICBkb20gLSBET00gbm9kZSB3ZSB3YW50IHRvIGhpZGVcbiAgICogQHBhcmFtICAgeyBCb29sZWFuIH0gc2hvdyAtIGRvIHdlIHdhbnQgdG8gc2hvdyBpdD9cbiAgICovXG5cbiAgZnVuY3Rpb24gdG9nZ2xlVmlzaWJpbGl0eShkb20sIHNob3cpIHtcbiAgICBkb20uc3R5bGUuZGlzcGxheSA9IHNob3cgPyAnJyA6ICdub25lJztcbiAgICBkb20uaGlkZGVuID0gc2hvdyA/IGZhbHNlIDogdHJ1ZTtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXQgdGhlIHZhbHVlIG9mIGFueSBET00gYXR0cmlidXRlIG9uIGEgbm9kZVxuICAgKiBAcGFyYW0gICB7IE9iamVjdCB9IGRvbSAtIERPTSBub2RlIHdlIHdhbnQgdG8gcGFyc2VcbiAgICogQHBhcmFtICAgeyBTdHJpbmcgfSBuYW1lIC0gbmFtZSBvZiB0aGUgYXR0cmlidXRlIHdlIHdhbnQgdG8gZ2V0XG4gICAqIEByZXR1cm5zIHsgU3RyaW5nIHwgdW5kZWZpbmVkIH0gbmFtZSBvZiB0aGUgbm9kZSBhdHRyaWJ1dGUgd2hldGhlciBpdCBleGlzdHNcbiAgICovXG4gIGZ1bmN0aW9uIGdldEF0dHJpYnV0ZShkb20sIG5hbWUpIHtcbiAgICByZXR1cm4gZG9tLmdldEF0dHJpYnV0ZShuYW1lKVxuICB9XG5cbiAgLyoqXG4gICAqIFJlbW92ZSBhbnkgRE9NIGF0dHJpYnV0ZSBmcm9tIGEgbm9kZVxuICAgKiBAcGFyYW0gICB7IE9iamVjdCB9IGRvbSAtIERPTSBub2RlIHdlIHdhbnQgdG8gdXBkYXRlXG4gICAqIEBwYXJhbSAgIHsgU3RyaW5nIH0gbmFtZSAtIG5hbWUgb2YgdGhlIHByb3BlcnR5IHdlIHdhbnQgdG8gcmVtb3ZlXG4gICAqL1xuICBmdW5jdGlvbiByZW1vdmVBdHRyaWJ1dGUoZG9tLCBuYW1lKSB7XG4gICAgZG9tLnJlbW92ZUF0dHJpYnV0ZShuYW1lKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBTZXQgdGhlIGlubmVyIGh0bWwgb2YgYW55IERPTSBub2RlIFNWR3MgaW5jbHVkZWRcbiAgICogQHBhcmFtIHsgT2JqZWN0IH0gY29udGFpbmVyIC0gRE9NIG5vZGUgd2hlcmUgd2UnbGwgaW5qZWN0IG5ldyBodG1sXG4gICAqIEBwYXJhbSB7IFN0cmluZyB9IGh0bWwgLSBodG1sIHRvIGluamVjdFxuICAgKiBAcGFyYW0geyBCb29sZWFuIH0gaXNTdmcgLSBzdmcgdGFncyBzaG91bGQgYmUgdHJlYXRlZCBhIGJpdCBkaWZmZXJlbnRseVxuICAgKi9cbiAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cbiAgZnVuY3Rpb24gc2V0SW5uZXJIVE1MKGNvbnRhaW5lciwgaHRtbCwgaXNTdmcpIHtcbiAgICAvLyBpbm5lckhUTUwgaXMgbm90IHN1cHBvcnRlZCBvbiBzdmcgdGFncyBzbyB3ZSBuZWV0IHRvIHRyZWF0IHRoZW0gZGlmZmVyZW50bHlcbiAgICBpZiAoaXNTdmcpIHtcbiAgICAgIHZhciBub2RlID0gY29udGFpbmVyLm93bmVyRG9jdW1lbnQuaW1wb3J0Tm9kZShcbiAgICAgICAgbmV3IERPTVBhcnNlcigpXG4gICAgICAgICAgLnBhcnNlRnJvbVN0cmluZygoXCI8c3ZnIHhtbG5zPVxcXCJcIiArIFNWR19OUyArIFwiXFxcIj5cIiArIGh0bWwgKyBcIjwvc3ZnPlwiKSwgJ2FwcGxpY2F0aW9uL3htbCcpXG4gICAgICAgICAgLmRvY3VtZW50RWxlbWVudCxcbiAgICAgICAgdHJ1ZVxuICAgICAgKTtcblxuICAgICAgY29udGFpbmVyLmFwcGVuZENoaWxkKG5vZGUpO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb250YWluZXIuaW5uZXJIVE1MID0gaHRtbDtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogTWluaW1pemUgcmlzazogb25seSB6ZXJvIG9yIG9uZSBfc3BhY2VfIGJldHdlZW4gYXR0ciAmIHZhbHVlXG4gICAqIEBwYXJhbSAgIHsgU3RyaW5nIH0gICBodG1sIC0gaHRtbCBzdHJpbmcgd2Ugd2FudCB0byBwYXJzZVxuICAgKiBAcGFyYW0gICB7IEZ1bmN0aW9uIH0gZm4gLSBjYWxsYmFjayBmdW5jdGlvbiB0byBhcHBseSBvbiBhbnkgYXR0cmlidXRlIGZvdW5kXG4gICAqL1xuICBmdW5jdGlvbiB3YWxrQXR0cmlidXRlcyhodG1sLCBmbikge1xuICAgIGlmICghaHRtbCkgeyByZXR1cm4gfVxuICAgIHZhciBtO1xuICAgIHdoaWxlIChtID0gUkVfSFRNTF9BVFRSUy5leGVjKGh0bWwpKVxuICAgICAgeyBmbihtWzFdLnRvTG93ZXJDYXNlKCksIG1bMl0gfHwgbVszXSB8fCBtWzRdKTsgfVxuICB9XG5cbiAgLyoqXG4gICAqIENyZWF0ZSBhIGRvY3VtZW50IGZyYWdtZW50XG4gICAqIEByZXR1cm5zIHsgT2JqZWN0IH0gZG9jdW1lbnQgZnJhZ21lbnRcbiAgICovXG4gIGZ1bmN0aW9uIGNyZWF0ZUZyYWdtZW50KCkge1xuICAgIHJldHVybiBkb2N1bWVudC5jcmVhdGVEb2N1bWVudEZyYWdtZW50KClcbiAgfVxuXG4gIC8qKlxuICAgKiBJbnNlcnQgc2FmZWx5IGEgdGFnIHRvIGZpeCAjMTk2MiAjMTY0OVxuICAgKiBAcGFyYW0gICB7IEhUTUxFbGVtZW50IH0gcm9vdCAtIGNoaWxkcmVuIGNvbnRhaW5lclxuICAgKiBAcGFyYW0gICB7IEhUTUxFbGVtZW50IH0gY3VyciAtIG5vZGUgdG8gaW5zZXJ0XG4gICAqIEBwYXJhbSAgIHsgSFRNTEVsZW1lbnQgfSBuZXh0IC0gbm9kZSB0aGF0IHNob3VsZCBwcmVjZWVkIHRoZSBjdXJyZW50IG5vZGUgaW5zZXJ0ZWRcbiAgICovXG4gIGZ1bmN0aW9uIHNhZmVJbnNlcnQocm9vdCwgY3VyciwgbmV4dCkge1xuICAgIHJvb3QuaW5zZXJ0QmVmb3JlKGN1cnIsIG5leHQucGFyZW50Tm9kZSAmJiBuZXh0KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBDb252ZXJ0IGEgc3R5bGUgb2JqZWN0IHRvIGEgc3RyaW5nXG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gc3R5bGUgLSBzdHlsZSBvYmplY3Qgd2UgbmVlZCB0byBwYXJzZVxuICAgKiBAcmV0dXJucyB7IFN0cmluZyB9IHJlc3VsdGluZyBjc3Mgc3RyaW5nXG4gICAqIEBleGFtcGxlXG4gICAqIHN0eWxlT2JqZWN0VG9TdHJpbmcoeyBjb2xvcjogJ3JlZCcsIGhlaWdodDogJzEwcHgnfSkgLy8gPT4gJ2NvbG9yOiByZWQ7IGhlaWdodDogMTBweCdcbiAgICovXG4gIGZ1bmN0aW9uIHN0eWxlT2JqZWN0VG9TdHJpbmcoc3R5bGUpIHtcbiAgICByZXR1cm4gT2JqZWN0LmtleXMoc3R5bGUpLnJlZHVjZShmdW5jdGlvbiAoYWNjLCBwcm9wKSB7XG4gICAgICByZXR1cm4gKGFjYyArIFwiIFwiICsgcHJvcCArIFwiOiBcIiArIChzdHlsZVtwcm9wXSkgKyBcIjtcIilcbiAgICB9LCAnJylcbiAgfVxuXG4gIC8qKlxuICAgKiBXYWxrIGRvd24gcmVjdXJzaXZlbHkgYWxsIHRoZSBjaGlsZHJlbiB0YWdzIHN0YXJ0aW5nIGRvbSBub2RlXG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gICBkb20gLSBzdGFydGluZyBub2RlIHdoZXJlIHdlIHdpbGwgc3RhcnQgdGhlIHJlY3Vyc2lvblxuICAgKiBAcGFyYW0gICB7IEZ1bmN0aW9uIH0gZm4gLSBjYWxsYmFjayB0byB0cmFuc2Zvcm0gdGhlIGNoaWxkIG5vZGUganVzdCBmb3VuZFxuICAgKiBAcGFyYW0gICB7IE9iamVjdCB9ICAgY29udGV4dCAtIGZuIGNhbiBvcHRpb25hbGx5IHJldHVybiBhbiBvYmplY3QsIHdoaWNoIGlzIHBhc3NlZCB0byBjaGlsZHJlblxuICAgKi9cbiAgZnVuY3Rpb24gd2Fsa05vZGVzKGRvbSwgZm4sIGNvbnRleHQpIHtcbiAgICBpZiAoZG9tKSB7XG4gICAgICB2YXIgcmVzID0gZm4oZG9tLCBjb250ZXh0KTtcbiAgICAgIHZhciBuZXh0O1xuICAgICAgLy8gc3RvcCB0aGUgcmVjdXJzaW9uXG4gICAgICBpZiAocmVzID09PSBmYWxzZSkgeyByZXR1cm4gfVxuXG4gICAgICBkb20gPSBkb20uZmlyc3RDaGlsZDtcblxuICAgICAgd2hpbGUgKGRvbSkge1xuICAgICAgICBuZXh0ID0gZG9tLm5leHRTaWJsaW5nO1xuICAgICAgICB3YWxrTm9kZXMoZG9tLCBmbiwgcmVzKTtcbiAgICAgICAgZG9tID0gbmV4dDtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuXG5cbiAgdmFyIGRvbSA9IC8qI19fUFVSRV9fKi9PYmplY3QuZnJlZXplKHtcbiAgICAkJDogJCQsXG4gICAgJDogJCxcbiAgICBjcmVhdGVET01QbGFjZWhvbGRlcjogY3JlYXRlRE9NUGxhY2Vob2xkZXIsXG4gICAgbWtFbDogbWFrZUVsZW1lbnQsXG4gICAgc2V0QXR0cjogc2V0QXR0cmlidXRlLFxuICAgIHRvZ2dsZVZpc2liaWxpdHk6IHRvZ2dsZVZpc2liaWxpdHksXG4gICAgZ2V0QXR0cjogZ2V0QXR0cmlidXRlLFxuICAgIHJlbUF0dHI6IHJlbW92ZUF0dHJpYnV0ZSxcbiAgICBzZXRJbm5lckhUTUw6IHNldElubmVySFRNTCxcbiAgICB3YWxrQXR0cnM6IHdhbGtBdHRyaWJ1dGVzLFxuICAgIGNyZWF0ZUZyYWc6IGNyZWF0ZUZyYWdtZW50LFxuICAgIHNhZmVJbnNlcnQ6IHNhZmVJbnNlcnQsXG4gICAgc3R5bGVPYmplY3RUb1N0cmluZzogc3R5bGVPYmplY3RUb1N0cmluZyxcbiAgICB3YWxrTm9kZXM6IHdhbGtOb2Rlc1xuICB9KTtcblxuICAvKipcbiAgICogQ2hlY2sgYWdhaW5zdCB0aGUgbnVsbCBhbmQgdW5kZWZpbmVkIHZhbHVlc1xuICAgKiBAcGFyYW0gICB7ICogfSAgdmFsdWUgLVxuICAgKiBAcmV0dXJucyB7Qm9vbGVhbn0gLVxuICAgKi9cbiAgZnVuY3Rpb24gaXNOaWwodmFsdWUpIHtcbiAgICByZXR1cm4gaXNVbmRlZmluZWQodmFsdWUpIHx8IHZhbHVlID09PSBudWxsXG4gIH1cblxuICAvKipcbiAgICogQ2hlY2sgaWYgcGFzc2VkIGFyZ3VtZW50IGlzIGVtcHR5LiBEaWZmZXJlbnQgZnJvbSBmYWxzeSwgYmVjYXVzZSB3ZSBkb250IGNvbnNpZGVyIDAgb3IgZmFsc2UgdG8gYmUgYmxhbmtcbiAgICogQHBhcmFtIHsgKiB9IHZhbHVlIC1cbiAgICogQHJldHVybnMgeyBCb29sZWFuIH0gLVxuICAgKi9cbiAgZnVuY3Rpb24gaXNCbGFuayh2YWx1ZSkge1xuICAgIHJldHVybiBpc05pbCh2YWx1ZSkgfHwgdmFsdWUgPT09ICcnXG4gIH1cblxuICAvKipcbiAgICogQ2hlY2sgaWYgcGFzc2VkIGFyZ3VtZW50IGlzIGEgZnVuY3Rpb25cbiAgICogQHBhcmFtICAgeyAqIH0gdmFsdWUgLVxuICAgKiBAcmV0dXJucyB7IEJvb2xlYW4gfSAtXG4gICAqL1xuICBmdW5jdGlvbiBpc0Z1bmN0aW9uKHZhbHVlKSB7XG4gICAgcmV0dXJuIHR5cGVvZiB2YWx1ZSA9PT0gVF9GVU5DVElPTlxuICB9XG5cbiAgLyoqXG4gICAqIENoZWNrIGlmIHBhc3NlZCBhcmd1bWVudCBpcyBhbiBvYmplY3QsIGV4Y2x1ZGUgbnVsbFxuICAgKiBOT1RFOiB1c2UgaXNPYmplY3QoeCkgJiYgIWlzQXJyYXkoeCkgdG8gZXhjbHVkZXMgYXJyYXlzLlxuICAgKiBAcGFyYW0gICB7ICogfSB2YWx1ZSAtXG4gICAqIEByZXR1cm5zIHsgQm9vbGVhbiB9IC1cbiAgICovXG4gIGZ1bmN0aW9uIGlzT2JqZWN0KHZhbHVlKSB7XG4gICAgcmV0dXJuIHZhbHVlICYmIHR5cGVvZiB2YWx1ZSA9PT0gVF9PQkpFQ1QgLy8gdHlwZW9mIG51bGwgaXMgJ29iamVjdCdcbiAgfVxuXG4gIC8qKlxuICAgKiBDaGVjayBpZiBhIERPTSBub2RlIGlzIGFuIHN2ZyB0YWcgb3IgcGFydCBvZiBhbiBzdmdcbiAgICogQHBhcmFtICAgeyBIVE1MRWxlbWVudCB9ICBlbCAtIG5vZGUgd2Ugd2FudCB0byB0ZXN0XG4gICAqIEByZXR1cm5zIHtCb29sZWFufSB0cnVlIGlmIGl0J3MgYW4gc3ZnIG5vZGVcbiAgICovXG4gIGZ1bmN0aW9uIGlzU3ZnKGVsKSB7XG4gICAgdmFyIG93bmVyID0gZWwub3duZXJTVkdFbGVtZW50O1xuICAgIHJldHVybiAhIW93bmVyIHx8IG93bmVyID09PSBudWxsXG4gIH1cblxuICAvKipcbiAgICogQ2hlY2sgaWYgcGFzc2VkIGFyZ3VtZW50IGlzIGEga2luZCBvZiBhcnJheVxuICAgKiBAcGFyYW0gICB7ICogfSB2YWx1ZSAtXG4gICAqIEByZXR1cm5zIHsgQm9vbGVhbiB9IC1cbiAgICovXG4gIGZ1bmN0aW9uIGlzQXJyYXkodmFsdWUpIHtcbiAgICByZXR1cm4gQXJyYXkuaXNBcnJheSh2YWx1ZSkgfHwgdmFsdWUgaW5zdGFuY2VvZiBBcnJheVxuICB9XG5cbiAgLyoqXG4gICAqIENoZWNrIGlmIHRoZSBwYXNzZWQgYXJndW1lbnQgaXMgYSBib29sZWFuIGF0dHJpYnV0ZVxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IHZhbHVlIC1cbiAgICogQHJldHVybnMgeyBCb29sZWFuIH0gLVxuICAgKi9cbiAgZnVuY3Rpb24gaXNCb29sQXR0cih2YWx1ZSkge1xuICAgIHJldHVybiBSRV9CT09MX0FUVFJTLnRlc3QodmFsdWUpXG4gIH1cblxuICAvKipcbiAgICogQ2hlY2sgaWYgcGFzc2VkIGFyZ3VtZW50IGlzIGEgc3RyaW5nXG4gICAqIEBwYXJhbSAgIHsgKiB9IHZhbHVlIC1cbiAgICogQHJldHVybnMgeyBCb29sZWFuIH0gLVxuICAgKi9cbiAgZnVuY3Rpb24gaXNTdHJpbmcodmFsdWUpIHtcbiAgICByZXR1cm4gdHlwZW9mIHZhbHVlID09PSBUX1NUUklOR1xuICB9XG5cblxuXG4gIHZhciBjaGVjayA9IC8qI19fUFVSRV9fKi9PYmplY3QuZnJlZXplKHtcbiAgICBpc0JsYW5rOiBpc0JsYW5rLFxuICAgIGlzRnVuY3Rpb246IGlzRnVuY3Rpb24sXG4gICAgaXNPYmplY3Q6IGlzT2JqZWN0LFxuICAgIGlzU3ZnOiBpc1N2ZyxcbiAgICBpc1dyaXRhYmxlOiBpc1dyaXRhYmxlLFxuICAgIGlzQXJyYXk6IGlzQXJyYXksXG4gICAgaXNCb29sQXR0cjogaXNCb29sQXR0cixcbiAgICBpc05pbDogaXNOaWwsXG4gICAgaXNTdHJpbmc6IGlzU3RyaW5nLFxuICAgIGlzVW5kZWZpbmVkOiBpc1VuZGVmaW5lZFxuICB9KTtcblxuICAvKipcbiAgICogQ2hlY2sgd2hldGhlciBhbiBhcnJheSBjb250YWlucyBhbiBpdGVtXG4gICAqIEBwYXJhbSAgIHsgQXJyYXkgfSBhcnJheSAtIHRhcmdldCBhcnJheVxuICAgKiBAcGFyYW0gICB7ICogfSBpdGVtIC0gaXRlbSB0byB0ZXN0XG4gICAqIEByZXR1cm5zIHsgQm9vbGVhbiB9IC1cbiAgICovXG4gIGZ1bmN0aW9uIGNvbnRhaW5zKGFycmF5LCBpdGVtKSB7XG4gICAgcmV0dXJuIGFycmF5LmluZGV4T2YoaXRlbSkgIT09IC0xXG4gIH1cblxuICAvKipcbiAgICogU3BlY2lhbGl6ZWQgZnVuY3Rpb24gZm9yIGxvb3BpbmcgYW4gYXJyYXktbGlrZSBjb2xsZWN0aW9uIHdpdGggYGVhY2g9e31gXG4gICAqIEBwYXJhbSAgIHsgQXJyYXkgfSBsaXN0IC0gY29sbGVjdGlvbiBvZiBpdGVtc1xuICAgKiBAcGFyYW0gICB7RnVuY3Rpb259IGZuIC0gY2FsbGJhY2sgZnVuY3Rpb25cbiAgICogQHJldHVybnMgeyBBcnJheSB9IHRoZSBhcnJheSBsb29wZWRcbiAgICovXG4gIGZ1bmN0aW9uIGVhY2gobGlzdCwgZm4pIHtcbiAgICB2YXIgbGVuID0gbGlzdCA/IGxpc3QubGVuZ3RoIDogMDtcbiAgICB2YXIgaSA9IDA7XG4gICAgZm9yICg7IGkgPCBsZW47IGkrKykgeyBmbihsaXN0W2ldLCBpKTsgfVxuICAgIHJldHVybiBsaXN0XG4gIH1cblxuICAvKipcbiAgICogRmFzdGVyIFN0cmluZyBzdGFydHNXaXRoIGFsdGVybmF0aXZlXG4gICAqIEBwYXJhbSAgIHsgU3RyaW5nIH0gc3RyIC0gc291cmNlIHN0cmluZ1xuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IHZhbHVlIC0gdGVzdCBzdHJpbmdcbiAgICogQHJldHVybnMgeyBCb29sZWFuIH0gLVxuICAgKi9cbiAgZnVuY3Rpb24gc3RhcnRzV2l0aChzdHIsIHZhbHVlKSB7XG4gICAgcmV0dXJuIHN0ci5zbGljZSgwLCB2YWx1ZS5sZW5ndGgpID09PSB2YWx1ZVxuICB9XG5cbiAgLyoqXG4gICAqIEZ1bmN0aW9uIHJldHVybmluZyBhbHdheXMgYSB1bmlxdWUgaWRlbnRpZmllclxuICAgKiBAcmV0dXJucyB7IE51bWJlciB9IC0gbnVtYmVyIGZyb20gMC4uLm5cbiAgICovXG4gIHZhciB1aWQgPSAoZnVuY3Rpb24gdWlkKCkge1xuICAgIHZhciBpID0gLTE7XG4gICAgcmV0dXJuIGZ1bmN0aW9uICgpIHsgcmV0dXJuICsraTsgfVxuICB9KSgpXG5cbiAgLyoqXG4gICAqIEhlbHBlciBmdW5jdGlvbiB0byBzZXQgYW4gaW1tdXRhYmxlIHByb3BlcnR5XG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gZWwgLSBvYmplY3Qgd2hlcmUgdGhlIG5ldyBwcm9wZXJ0eSB3aWxsIGJlIHNldFxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IGtleSAtIG9iamVjdCBrZXkgd2hlcmUgdGhlIG5ldyBwcm9wZXJ0eSB3aWxsIGJlIHN0b3JlZFxuICAgKiBAcGFyYW0gICB7ICogfSB2YWx1ZSAtIHZhbHVlIG9mIHRoZSBuZXcgcHJvcGVydHlcbiAgICogQHBhcmFtICAgeyBPYmplY3QgfSBvcHRpb25zIC0gc2V0IHRoZSBwcm9wZXJ5IG92ZXJyaWRpbmcgdGhlIGRlZmF1bHQgb3B0aW9uc1xuICAgKiBAcmV0dXJucyB7IE9iamVjdCB9IC0gdGhlIGluaXRpYWwgb2JqZWN0XG4gICAqL1xuICBmdW5jdGlvbiBkZWZpbmUoZWwsIGtleSwgdmFsdWUsIG9wdGlvbnMpIHtcbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkoZWwsIGtleSwgZXh0ZW5kKHtcbiAgICAgIHZhbHVlOiB2YWx1ZSxcbiAgICAgIGVudW1lcmFibGU6IGZhbHNlLFxuICAgICAgd3JpdGFibGU6IGZhbHNlLFxuICAgICAgY29uZmlndXJhYmxlOiB0cnVlXG4gICAgfSwgb3B0aW9ucykpO1xuICAgIHJldHVybiBlbFxuICB9XG5cbiAgLyoqXG4gICAqIENvbnZlcnQgYSBzdHJpbmcgY29udGFpbmluZyBkYXNoZXMgdG8gY2FtZWwgY2FzZVxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IHN0ciAtIGlucHV0IHN0cmluZ1xuICAgKiBAcmV0dXJucyB7IFN0cmluZyB9IG15LXN0cmluZyAtPiBteVN0cmluZ1xuICAgKi9cbiAgZnVuY3Rpb24gdG9DYW1lbChzdHIpIHtcbiAgICByZXR1cm4gc3RyLnJlcGxhY2UoLy0oXFx3KS9nLCBmdW5jdGlvbiAoXywgYykgeyByZXR1cm4gYy50b1VwcGVyQ2FzZSgpOyB9KVxuICB9XG5cbiAgLyoqXG4gICAqIFdhcm4gYSBtZXNzYWdlIHZpYSBjb25zb2xlXG4gICAqIEBwYXJhbSAgIHtTdHJpbmd9IG1lc3NhZ2UgLSB3YXJuaW5nIG1lc3NhZ2VcbiAgICovXG4gIGZ1bmN0aW9uIHdhcm4obWVzc2FnZSkge1xuICAgIGlmIChjb25zb2xlICYmIGNvbnNvbGUud2FybikgeyBjb25zb2xlLndhcm4obWVzc2FnZSk7IH1cbiAgfVxuXG5cblxuICB2YXIgbWlzYyA9IC8qI19fUFVSRV9fKi9PYmplY3QuZnJlZXplKHtcbiAgICBjb250YWluczogY29udGFpbnMsXG4gICAgZWFjaDogZWFjaCxcbiAgICBnZXRQcm9wRGVzY3JpcHRvcjogZ2V0UHJvcERlc2NyaXB0b3IsXG4gICAgc3RhcnRzV2l0aDogc3RhcnRzV2l0aCxcbiAgICB1aWQ6IHVpZCxcbiAgICBkZWZpbmVQcm9wZXJ0eTogZGVmaW5lLFxuICAgIG9iamVjdENyZWF0ZTogY3JlYXRlLFxuICAgIGV4dGVuZDogZXh0ZW5kLFxuICAgIHRvQ2FtZWw6IHRvQ2FtZWwsXG4gICAgd2Fybjogd2FyblxuICB9KTtcblxuICAvKipcbiAgICogU2V0IHRoZSBwcm9wZXJ0eSBvZiBhbiBvYmplY3QgZm9yIGEgZ2l2ZW4ga2V5LiBJZiBzb21ldGhpbmcgYWxyZWFkeVxuICAgKiBleGlzdHMgdGhlcmUsIHRoZW4gaXQgYmVjb21lcyBhbiBhcnJheSBjb250YWluaW5nIGJvdGggdGhlIG9sZCBhbmQgbmV3IHZhbHVlLlxuICAgKiBAcGFyYW0geyBPYmplY3QgfSBvYmogLSBvYmplY3Qgb24gd2hpY2ggdG8gc2V0IHRoZSBwcm9wZXJ0eVxuICAgKiBAcGFyYW0geyBTdHJpbmcgfSBrZXkgLSBwcm9wZXJ0eSBuYW1lXG4gICAqIEBwYXJhbSB7IE9iamVjdCB9IHZhbHVlIC0gdGhlIHZhbHVlIG9mIHRoZSBwcm9wZXJ0eSB0byBiZSBzZXRcbiAgICogQHBhcmFtIHsgQm9vbGVhbiB9IGVuc3VyZUFycmF5IC0gZW5zdXJlIHRoYXQgdGhlIHByb3BlcnR5IHJlbWFpbnMgYW4gYXJyYXlcbiAgICogQHBhcmFtIHsgTnVtYmVyIH0gaW5kZXggLSBhZGQgdGhlIG5ldyBpdGVtIGluIGEgY2VydGFpbiBhcnJheSBwb3NpdGlvblxuICAgKi9cbiAgZnVuY3Rpb24gYXJyYXlpc2hBZGQob2JqLCBrZXksIHZhbHVlLCBlbnN1cmVBcnJheSwgaW5kZXgpIHtcbiAgICB2YXIgZGVzdCA9IG9ialtrZXldO1xuICAgIHZhciBpc0FyciA9IGlzQXJyYXkoZGVzdCk7XG4gICAgdmFyIGhhc0luZGV4ID0gIWlzVW5kZWZpbmVkKGluZGV4KTtcblxuICAgIGlmIChkZXN0ICYmIGRlc3QgPT09IHZhbHVlKSB7IHJldHVybiB9XG5cbiAgICAvLyBpZiB0aGUga2V5IHdhcyBuZXZlciBzZXQsIHNldCBpdCBvbmNlXG4gICAgaWYgKCFkZXN0ICYmIGVuc3VyZUFycmF5KSB7IG9ialtrZXldID0gW3ZhbHVlXTsgfVxuICAgIGVsc2UgaWYgKCFkZXN0KSB7IG9ialtrZXldID0gdmFsdWU7IH1cbiAgICAvLyBpZiBpdCB3YXMgYW4gYXJyYXkgYW5kIG5vdCB5ZXQgc2V0XG4gICAgZWxzZSB7XG4gICAgICBpZiAoaXNBcnIpIHtcbiAgICAgICAgdmFyIG9sZEluZGV4ID0gZGVzdC5pbmRleE9mKHZhbHVlKTtcbiAgICAgICAgLy8gdGhpcyBpdGVtIG5ldmVyIGNoYW5nZWQgaXRzIHBvc2l0aW9uXG4gICAgICAgIGlmIChvbGRJbmRleCA9PT0gaW5kZXgpIHsgcmV0dXJuIH1cbiAgICAgICAgLy8gcmVtb3ZlIHRoZSBpdGVtIGZyb20gaXRzIG9sZCBwb3NpdGlvblxuICAgICAgICBpZiAob2xkSW5kZXggIT09IC0xKSB7IGRlc3Quc3BsaWNlKG9sZEluZGV4LCAxKTsgfVxuICAgICAgICAvLyBtb3ZlIG9yIGFkZCB0aGUgaXRlbVxuICAgICAgICBpZiAoaGFzSW5kZXgpIHtcbiAgICAgICAgICBkZXN0LnNwbGljZShpbmRleCwgMCwgdmFsdWUpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGRlc3QucHVzaCh2YWx1ZSk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7IG9ialtrZXldID0gW2Rlc3QsIHZhbHVlXTsgfVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBEZXRlY3QgdGhlIHRhZyBpbXBsZW1lbnRhdGlvbiBieSBhIERPTSBub2RlXG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gZG9tIC0gRE9NIG5vZGUgd2UgbmVlZCB0byBwYXJzZSB0byBnZXQgaXRzIHRhZyBpbXBsZW1lbnRhdGlvblxuICAgKiBAcmV0dXJucyB7IE9iamVjdCB9IGl0IHJldHVybnMgYW4gb2JqZWN0IGNvbnRhaW5pbmcgdGhlIGltcGxlbWVudGF0aW9uIG9mIGEgY3VzdG9tIHRhZyAodGVtcGxhdGUgYW5kIGJvb3QgZnVuY3Rpb24pXG4gICAqL1xuICBmdW5jdGlvbiBnZXQoZG9tKSB7XG4gICAgcmV0dXJuIGRvbS50YWdOYW1lICYmIF9fVEFHX0lNUExbZ2V0QXR0cmlidXRlKGRvbSwgSVNfRElSRUNUSVZFKSB8fFxuICAgICAgZ2V0QXR0cmlidXRlKGRvbSwgSVNfRElSRUNUSVZFKSB8fCBkb20udGFnTmFtZS50b0xvd2VyQ2FzZSgpXVxuICB9XG5cbiAgLyoqXG4gICAqIEdldCB0aGUgdGFnIG5hbWUgb2YgYW55IERPTSBub2RlXG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gZG9tIC0gRE9NIG5vZGUgd2Ugd2FudCB0byBwYXJzZVxuICAgKiBAcGFyYW0gICB7IEJvb2xlYW4gfSBza2lwRGF0YUlzIC0gaGFjayB0byBpZ25vcmUgdGhlIGRhdGEtaXMgYXR0cmlidXRlIHdoZW4gYXR0YWNoaW5nIHRvIHBhcmVudFxuICAgKiBAcmV0dXJucyB7IFN0cmluZyB9IG5hbWUgdG8gaWRlbnRpZnkgdGhpcyBkb20gbm9kZSBpbiByaW90XG4gICAqL1xuICBmdW5jdGlvbiBnZXROYW1lKGRvbSwgc2tpcERhdGFJcykge1xuICAgIHZhciBjaGlsZCA9IGdldChkb20pO1xuICAgIHZhciBuYW1lZFRhZyA9ICFza2lwRGF0YUlzICYmIGdldEF0dHJpYnV0ZShkb20sIElTX0RJUkVDVElWRSk7XG4gICAgcmV0dXJuIG5hbWVkVGFnICYmICF0bXBsLmhhc0V4cHIobmFtZWRUYWcpID9cbiAgICAgIG5hbWVkVGFnIDogY2hpbGQgPyBjaGlsZC5uYW1lIDogZG9tLnRhZ05hbWUudG9Mb3dlckNhc2UoKVxuICB9XG5cbiAgLyoqXG4gICAqIFJldHVybiBhIHRlbXBvcmFyeSBjb250ZXh0IGNvbnRhaW5pbmcgYWxzbyB0aGUgcGFyZW50IHByb3BlcnRpZXNcbiAgICogQHRoaXMgVGFnXG4gICAqIEBwYXJhbSB7IFRhZyB9IC0gdGVtcG9yYXJ5IHRhZyBjb250ZXh0IGNvbnRhaW5pbmcgYWxsIHRoZSBwYXJlbnQgcHJvcGVydGllc1xuICAgKi9cbiAgZnVuY3Rpb24gaW5oZXJpdFBhcmVudFByb3BzKCkge1xuICAgIGlmICh0aGlzLnBhcmVudCkgeyByZXR1cm4gZXh0ZW5kKGNyZWF0ZSh0aGlzKSwgdGhpcy5wYXJlbnQpIH1cbiAgICByZXR1cm4gdGhpc1xuICB9XG5cbiAgLypcbiAgICBJbmNsdWRlcyBoYWNrcyBuZWVkZWQgZm9yIHRoZSBJbnRlcm5ldCBFeHBsb3JlciB2ZXJzaW9uIDkgYW5kIGJlbG93XG4gICAgU2VlOiBodHRwOi8va2FuZ2F4LmdpdGh1Yi5pby9jb21wYXQtdGFibGUvZXM1LyNpZThcbiAgICAgICAgIGh0dHA6Ly9jb2RlcGxhbmV0LmlvL2Ryb3BwaW5nLWllOC9cbiAgKi9cblxuICB2YXJcbiAgICByZUhhc1lpZWxkICA9IC88eWllbGRcXGIvaSxcbiAgICByZVlpZWxkQWxsICA9IC88eWllbGRcXHMqKD86XFwvPnw+KFtcXFNcXHNdKj8pPFxcL3lpZWxkXFxzKj58PikvaWcsXG4gICAgcmVZaWVsZFNyYyAgPSAvPHlpZWxkXFxzK3RvPVsnXCJdKFteJ1wiPl0qKVsnXCJdXFxzKj4oW1xcU1xcc10qPyk8XFwveWllbGRcXHMqPi9pZyxcbiAgICByZVlpZWxkRGVzdCA9IC88eWllbGRcXHMrZnJvbT1bJ1wiXT8oWy1cXHddKylbJ1wiXT9cXHMqKD86XFwvPnw+KFtcXFNcXHNdKj8pPFxcL3lpZWxkXFxzKj4pL2lnLFxuICAgIHJvb3RFbHMgPSB7IHRyOiAndGJvZHknLCB0aDogJ3RyJywgdGQ6ICd0cicsIGNvbDogJ2NvbGdyb3VwJyB9LFxuICAgIHRibFRhZ3MgPSBJRV9WRVJTSU9OICYmIElFX1ZFUlNJT04gPCAxMCA/IFJFX1NQRUNJQUxfVEFHUyA6IFJFX1NQRUNJQUxfVEFHU19OT19PUFRJT04sXG4gICAgR0VORVJJQyA9ICdkaXYnLFxuICAgIFNWRyA9ICdzdmcnO1xuXG5cbiAgLypcbiAgICBDcmVhdGVzIHRoZSByb290IGVsZW1lbnQgZm9yIHRhYmxlIG9yIHNlbGVjdCBjaGlsZCBlbGVtZW50czpcbiAgICB0ci90aC90ZC90aGVhZC90Zm9vdC90Ym9keS9jYXB0aW9uL2NvbC9jb2xncm91cC9vcHRpb24vb3B0Z3JvdXBcbiAgKi9cbiAgZnVuY3Rpb24gc3BlY2lhbFRhZ3MoZWwsIHRtcGwsIHRhZ05hbWUpIHtcblxuICAgIHZhclxuICAgICAgc2VsZWN0ID0gdGFnTmFtZVswXSA9PT0gJ28nLFxuICAgICAgcGFyZW50ID0gc2VsZWN0ID8gJ3NlbGVjdD4nIDogJ3RhYmxlPic7XG5cbiAgICAvLyB0cmltKCkgaXMgaW1wb3J0YW50IGhlcmUsIHRoaXMgZW5zdXJlcyB3ZSBkb24ndCBoYXZlIGFydGlmYWN0cyxcbiAgICAvLyBzbyB3ZSBjYW4gY2hlY2sgaWYgd2UgaGF2ZSBvbmx5IG9uZSBlbGVtZW50IGluc2lkZSB0aGUgcGFyZW50XG4gICAgZWwuaW5uZXJIVE1MID0gJzwnICsgcGFyZW50ICsgdG1wbC50cmltKCkgKyAnPC8nICsgcGFyZW50O1xuICAgIHBhcmVudCA9IGVsLmZpcnN0Q2hpbGQ7XG5cbiAgICAvLyByZXR1cm5zIHRoZSBpbW1lZGlhdGUgcGFyZW50IGlmIHRyL3RoL3RkL2NvbCBpcyB0aGUgb25seSBlbGVtZW50LCBpZiBub3RcbiAgICAvLyByZXR1cm5zIHRoZSB3aG9sZSB0cmVlLCBhcyB0aGlzIGNhbiBpbmNsdWRlIGFkZGl0aW9uYWwgZWxlbWVudHNcbiAgICAvKiBpc3RhbmJ1bCBpZ25vcmUgbmV4dCAqL1xuICAgIGlmIChzZWxlY3QpIHtcbiAgICAgIHBhcmVudC5zZWxlY3RlZEluZGV4ID0gLTE7ICAvLyBmb3IgSUU5LCBjb21wYXRpYmxlIHcvY3VycmVudCByaW90IGJlaGF2aW9yXG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIGF2b2lkcyBpbnNlcnRpb24gb2YgY29pbnRhaW5lciBpbnNpZGUgY29udGFpbmVyIChleDogdGJvZHkgaW5zaWRlIHRib2R5KVxuICAgICAgdmFyIHRuYW1lID0gcm9vdEVsc1t0YWdOYW1lXTtcbiAgICAgIGlmICh0bmFtZSAmJiBwYXJlbnQuY2hpbGRFbGVtZW50Q291bnQgPT09IDEpIHsgcGFyZW50ID0gJCh0bmFtZSwgcGFyZW50KTsgfVxuICAgIH1cbiAgICByZXR1cm4gcGFyZW50XG4gIH1cblxuICAvKlxuICAgIFJlcGxhY2UgdGhlIHlpZWxkIHRhZyBmcm9tIGFueSB0YWcgdGVtcGxhdGUgd2l0aCB0aGUgaW5uZXJIVE1MIG9mIHRoZVxuICAgIG9yaWdpbmFsIHRhZyBpbiB0aGUgcGFnZVxuICAqL1xuICBmdW5jdGlvbiByZXBsYWNlWWllbGQodG1wbCwgaHRtbCkge1xuICAgIC8vIGRvIG5vdGhpbmcgaWYgbm8geWllbGRcbiAgICBpZiAoIXJlSGFzWWllbGQudGVzdCh0bXBsKSkgeyByZXR1cm4gdG1wbCB9XG5cbiAgICAvLyBiZSBjYXJlZnVsIHdpdGggIzEzNDMgLSBzdHJpbmcgb24gdGhlIHNvdXJjZSBoYXZpbmcgYCQxYFxuICAgIHZhciBzcmMgPSB7fTtcblxuICAgIGh0bWwgPSBodG1sICYmIGh0bWwucmVwbGFjZShyZVlpZWxkU3JjLCBmdW5jdGlvbiAoXywgcmVmLCB0ZXh0KSB7XG4gICAgICBzcmNbcmVmXSA9IHNyY1tyZWZdIHx8IHRleHQ7ICAgLy8gcHJlc2VydmUgZmlyc3QgZGVmaW5pdGlvblxuICAgICAgcmV0dXJuICcnXG4gICAgfSkudHJpbSgpO1xuXG4gICAgcmV0dXJuIHRtcGxcbiAgICAgIC5yZXBsYWNlKHJlWWllbGREZXN0LCBmdW5jdGlvbiAoXywgcmVmLCBkZWYpIHsgIC8vIHlpZWxkIHdpdGggZnJvbSAtIHRvIGF0dHJzXG4gICAgICAgIHJldHVybiBzcmNbcmVmXSB8fCBkZWYgfHwgJydcbiAgICAgIH0pXG4gICAgICAucmVwbGFjZShyZVlpZWxkQWxsLCBmdW5jdGlvbiAoXywgZGVmKSB7ICAgICAgICAvLyB5aWVsZCB3aXRob3V0IGFueSBcImZyb21cIlxuICAgICAgICByZXR1cm4gaHRtbCB8fCBkZWYgfHwgJydcbiAgICAgIH0pXG4gIH1cblxuICAvKipcbiAgICogQ3JlYXRlcyBhIERPTSBlbGVtZW50IHRvIHdyYXAgdGhlIGdpdmVuIGNvbnRlbnQuIE5vcm1hbGx5IGFuIGBESVZgLCBidXQgY2FuIGJlXG4gICAqIGFsc28gYSBgVEFCTEVgLCBgU0VMRUNUYCwgYFRCT0RZYCwgYFRSYCwgb3IgYENPTEdST1VQYCBlbGVtZW50LlxuICAgKlxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IHRtcGwgIC0gVGhlIHRlbXBsYXRlIGNvbWluZyBmcm9tIHRoZSBjdXN0b20gdGFnIGRlZmluaXRpb25cbiAgICogQHBhcmFtICAgeyBTdHJpbmcgfSBodG1sIC0gSFRNTCBjb250ZW50IHRoYXQgY29tZXMgZnJvbSB0aGUgRE9NIGVsZW1lbnQgd2hlcmUgeW91XG4gICAqICAgICAgICAgICB3aWxsIG1vdW50IHRoZSB0YWcsIG1vc3RseSB0aGUgb3JpZ2luYWwgdGFnIGluIHRoZSBwYWdlXG4gICAqIEBwYXJhbSAgIHsgQm9vbGVhbiB9IGlzU3ZnIC0gdHJ1ZSBpZiB0aGUgcm9vdCBub2RlIGlzIGFuIHN2Z1xuICAgKiBAcmV0dXJucyB7IEhUTUxFbGVtZW50IH0gRE9NIGVsZW1lbnQgd2l0aCBfdG1wbF8gbWVyZ2VkIHRocm91Z2ggYFlJRUxEYCB3aXRoIHRoZSBfaHRtbF8uXG4gICAqL1xuICBmdW5jdGlvbiBta2RvbSh0bXBsLCBodG1sLCBpc1N2Zykge1xuICAgIHZhciBtYXRjaCAgID0gdG1wbCAmJiB0bXBsLm1hdGNoKC9eXFxzKjwoWy1cXHddKykvKTtcbiAgICB2YXIgIHRhZ05hbWUgPSBtYXRjaCAmJiBtYXRjaFsxXS50b0xvd2VyQ2FzZSgpO1xuICAgIHZhciBlbCA9IG1ha2VFbGVtZW50KGlzU3ZnID8gU1ZHIDogR0VORVJJQyk7XG5cbiAgICAvLyByZXBsYWNlIGFsbCB0aGUgeWllbGQgdGFncyB3aXRoIHRoZSB0YWcgaW5uZXIgaHRtbFxuICAgIHRtcGwgPSByZXBsYWNlWWllbGQodG1wbCwgaHRtbCk7XG5cbiAgICAvKiBpc3RhbmJ1bCBpZ25vcmUgbmV4dCAqL1xuICAgIGlmICh0YmxUYWdzLnRlc3QodGFnTmFtZSkpXG4gICAgICB7IGVsID0gc3BlY2lhbFRhZ3MoZWwsIHRtcGwsIHRhZ05hbWUpOyB9XG4gICAgZWxzZVxuICAgICAgeyBzZXRJbm5lckhUTUwoZWwsIHRtcGwsIGlzU3ZnKTsgfVxuXG4gICAgcmV0dXJuIGVsXG4gIH1cblxuICB2YXIgRVZFTlRfQVRUUl9SRSA9IC9eb24vO1xuXG4gIC8qKlxuICAgKiBUcnVlIGlmIHRoZSBldmVudCBhdHRyaWJ1dGUgc3RhcnRzIHdpdGggJ29uJ1xuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IGF0dHJpYnV0ZSAtIGV2ZW50IGF0dHJpYnV0ZVxuICAgKiBAcmV0dXJucyB7IEJvb2xlYW4gfVxuICAgKi9cbiAgZnVuY3Rpb24gaXNFdmVudEF0dHJpYnV0ZShhdHRyaWJ1dGUpIHtcbiAgICByZXR1cm4gRVZFTlRfQVRUUl9SRS50ZXN0KGF0dHJpYnV0ZSlcbiAgfVxuXG4gIC8qKlxuICAgKiBMb29wIGJhY2t3YXJkIGFsbCB0aGUgcGFyZW50cyB0cmVlIHRvIGRldGVjdCB0aGUgZmlyc3QgY3VzdG9tIHBhcmVudCB0YWdcbiAgICogQHBhcmFtICAgeyBPYmplY3QgfSB0YWcgLSBhIFRhZyBpbnN0YW5jZVxuICAgKiBAcmV0dXJucyB7IE9iamVjdCB9IHRoZSBpbnN0YW5jZSBvZiB0aGUgZmlyc3QgY3VzdG9tIHBhcmVudCB0YWcgZm91bmRcbiAgICovXG4gIGZ1bmN0aW9uIGdldEltbWVkaWF0ZUN1c3RvbVBhcmVudCh0YWcpIHtcbiAgICB2YXIgcHRhZyA9IHRhZztcbiAgICB3aGlsZSAocHRhZy5fXy5pc0Fub255bW91cykge1xuICAgICAgaWYgKCFwdGFnLnBhcmVudCkgeyBicmVhayB9XG4gICAgICBwdGFnID0gcHRhZy5wYXJlbnQ7XG4gICAgfVxuICAgIHJldHVybiBwdGFnXG4gIH1cblxuICAvKipcbiAgICogVHJpZ2dlciBET00gZXZlbnRzXG4gICAqIEBwYXJhbSAgIHsgSFRNTEVsZW1lbnQgfSBkb20gLSBkb20gZWxlbWVudCB0YXJnZXQgb2YgdGhlIGV2ZW50XG4gICAqIEBwYXJhbSAgIHsgRnVuY3Rpb24gfSBoYW5kbGVyIC0gdXNlciBmdW5jdGlvblxuICAgKiBAcGFyYW0gICB7IE9iamVjdCB9IGUgLSBldmVudCBvYmplY3RcbiAgICovXG4gIGZ1bmN0aW9uIGhhbmRsZUV2ZW50KGRvbSwgaGFuZGxlciwgZSkge1xuICAgIHZhciBwdGFnID0gdGhpcy5fXy5wYXJlbnQ7XG4gICAgdmFyIGl0ZW0gPSB0aGlzLl9fLml0ZW07XG5cbiAgICBpZiAoIWl0ZW0pXG4gICAgICB7IHdoaWxlIChwdGFnICYmICFpdGVtKSB7XG4gICAgICAgIGl0ZW0gPSBwdGFnLl9fLml0ZW07XG4gICAgICAgIHB0YWcgPSBwdGFnLl9fLnBhcmVudDtcbiAgICAgIH0gfVxuXG4gICAgLy8gb3ZlcnJpZGUgdGhlIGV2ZW50IHByb3BlcnRpZXNcbiAgICAvKiBpc3RhbmJ1bCBpZ25vcmUgbmV4dCAqL1xuICAgIGlmIChpc1dyaXRhYmxlKGUsICdjdXJyZW50VGFyZ2V0JykpIHsgZS5jdXJyZW50VGFyZ2V0ID0gZG9tOyB9XG4gICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cbiAgICBpZiAoaXNXcml0YWJsZShlLCAndGFyZ2V0JykpIHsgZS50YXJnZXQgPSBlLnNyY0VsZW1lbnQ7IH1cbiAgICAvKiBpc3RhbmJ1bCBpZ25vcmUgbmV4dCAqL1xuICAgIGlmIChpc1dyaXRhYmxlKGUsICd3aGljaCcpKSB7IGUud2hpY2ggPSBlLmNoYXJDb2RlIHx8IGUua2V5Q29kZTsgfVxuXG4gICAgZS5pdGVtID0gaXRlbTtcblxuICAgIGhhbmRsZXIuY2FsbCh0aGlzLCBlKTtcblxuICAgIC8vIGF2b2lkIGF1dG8gdXBkYXRlc1xuICAgIGlmICghc2V0dGluZ3MuYXV0b1VwZGF0ZSkgeyByZXR1cm4gfVxuXG4gICAgaWYgKCFlLnByZXZlbnRVcGRhdGUpIHtcbiAgICAgIHZhciBwID0gZ2V0SW1tZWRpYXRlQ3VzdG9tUGFyZW50KHRoaXMpO1xuICAgICAgLy8gZml4ZXMgIzIwODNcbiAgICAgIGlmIChwLmlzTW91bnRlZCkgeyBwLnVwZGF0ZSgpOyB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEF0dGFjaCBhbiBldmVudCB0byBhIERPTSBub2RlXG4gICAqIEBwYXJhbSB7IFN0cmluZyB9IG5hbWUgLSBldmVudCBuYW1lXG4gICAqIEBwYXJhbSB7IEZ1bmN0aW9uIH0gaGFuZGxlciAtIGV2ZW50IGNhbGxiYWNrXG4gICAqIEBwYXJhbSB7IE9iamVjdCB9IGRvbSAtIGRvbSBub2RlXG4gICAqIEBwYXJhbSB7IFRhZyB9IHRhZyAtIHRhZyBpbnN0YW5jZVxuICAgKi9cbiAgZnVuY3Rpb24gc2V0RXZlbnRIYW5kbGVyKG5hbWUsIGhhbmRsZXIsIGRvbSwgdGFnKSB7XG4gICAgdmFyIGV2ZW50TmFtZTtcbiAgICB2YXIgY2IgPSBoYW5kbGVFdmVudC5iaW5kKHRhZywgZG9tLCBoYW5kbGVyKTtcblxuICAgIC8vIGF2b2lkIHRvIGJpbmQgdHdpY2UgdGhlIHNhbWUgZXZlbnRcbiAgICAvLyBwb3NzaWJsZSBmaXggZm9yICMyMzMyXG4gICAgZG9tW25hbWVdID0gbnVsbDtcblxuICAgIC8vIG5vcm1hbGl6ZSBldmVudCBuYW1lXG4gICAgZXZlbnROYW1lID0gbmFtZS5yZXBsYWNlKFJFX0VWRU5UU19QUkVGSVgsICcnKTtcblxuICAgIC8vIGNhY2hlIHRoZSBsaXN0ZW5lciBpbnRvIHRoZSBsaXN0ZW5lcnMgYXJyYXlcbiAgICBpZiAoIWNvbnRhaW5zKHRhZy5fXy5saXN0ZW5lcnMsIGRvbSkpIHsgdGFnLl9fLmxpc3RlbmVycy5wdXNoKGRvbSk7IH1cbiAgICBpZiAoIWRvbVtSSU9UX0VWRU5UU19LRVldKSB7IGRvbVtSSU9UX0VWRU5UU19LRVldID0ge307IH1cbiAgICBpZiAoZG9tW1JJT1RfRVZFTlRTX0tFWV1bbmFtZV0pIHsgZG9tLnJlbW92ZUV2ZW50TGlzdGVuZXIoZXZlbnROYW1lLCBkb21bUklPVF9FVkVOVFNfS0VZXVtuYW1lXSk7IH1cblxuICAgIGRvbVtSSU9UX0VWRU5UU19LRVldW25hbWVdID0gY2I7XG4gICAgZG9tLmFkZEV2ZW50TGlzdGVuZXIoZXZlbnROYW1lLCBjYiwgZmFsc2UpO1xuICB9XG5cbiAgLyoqXG4gICAqIENyZWF0ZSBhIG5ldyBjaGlsZCB0YWcgaW5jbHVkaW5nIGl0IGNvcnJlY3RseSBpbnRvIGl0cyBwYXJlbnRcbiAgICogQHBhcmFtICAgeyBPYmplY3QgfSBjaGlsZCAtIGNoaWxkIHRhZyBpbXBsZW1lbnRhdGlvblxuICAgKiBAcGFyYW0gICB7IE9iamVjdCB9IG9wdHMgLSB0YWcgb3B0aW9ucyBjb250YWluaW5nIHRoZSBET00gbm9kZSB3aGVyZSB0aGUgdGFnIHdpbGwgYmUgbW91bnRlZFxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IGlubmVySFRNTCAtIGlubmVyIGh0bWwgb2YgdGhlIGNoaWxkIG5vZGVcbiAgICogQHBhcmFtICAgeyBPYmplY3QgfSBwYXJlbnQgLSBpbnN0YW5jZSBvZiB0aGUgcGFyZW50IHRhZyBpbmNsdWRpbmcgdGhlIGNoaWxkIGN1c3RvbSB0YWdcbiAgICogQHJldHVybnMgeyBPYmplY3QgfSBpbnN0YW5jZSBvZiB0aGUgbmV3IGNoaWxkIHRhZyBqdXN0IGNyZWF0ZWRcbiAgICovXG4gIGZ1bmN0aW9uIGluaXRDaGlsZChjaGlsZCwgb3B0cywgaW5uZXJIVE1MLCBwYXJlbnQpIHtcbiAgICB2YXIgdGFnID0gY3JlYXRlVGFnKGNoaWxkLCBvcHRzLCBpbm5lckhUTUwpO1xuICAgIHZhciB0YWdOYW1lID0gb3B0cy50YWdOYW1lIHx8IGdldE5hbWUob3B0cy5yb290LCB0cnVlKTtcbiAgICB2YXIgcHRhZyA9IGdldEltbWVkaWF0ZUN1c3RvbVBhcmVudChwYXJlbnQpO1xuICAgIC8vIGZpeCBmb3IgdGhlIHBhcmVudCBhdHRyaWJ1dGUgaW4gdGhlIGxvb3BlZCBlbGVtZW50c1xuICAgIGRlZmluZSh0YWcsICdwYXJlbnQnLCBwdGFnKTtcbiAgICAvLyBzdG9yZSB0aGUgcmVhbCBwYXJlbnQgdGFnXG4gICAgLy8gaW4gc29tZSBjYXNlcyB0aGlzIGNvdWxkIGJlIGRpZmZlcmVudCBmcm9tIHRoZSBjdXN0b20gcGFyZW50IHRhZ1xuICAgIC8vIGZvciBleGFtcGxlIGluIG5lc3RlZCBsb29wc1xuICAgIHRhZy5fXy5wYXJlbnQgPSBwYXJlbnQ7XG5cbiAgICAvLyBhZGQgdGhpcyB0YWcgdG8gdGhlIGN1c3RvbSBwYXJlbnQgdGFnXG4gICAgYXJyYXlpc2hBZGQocHRhZy50YWdzLCB0YWdOYW1lLCB0YWcpO1xuXG4gICAgLy8gYW5kIGFsc28gdG8gdGhlIHJlYWwgcGFyZW50IHRhZ1xuICAgIGlmIChwdGFnICE9PSBwYXJlbnQpXG4gICAgICB7IGFycmF5aXNoQWRkKHBhcmVudC50YWdzLCB0YWdOYW1lLCB0YWcpOyB9XG5cbiAgICByZXR1cm4gdGFnXG4gIH1cblxuICAvKipcbiAgICogUmVtb3ZlcyBhbiBpdGVtIGZyb20gYW4gb2JqZWN0IGF0IGEgZ2l2ZW4ga2V5LiBJZiB0aGUga2V5IHBvaW50cyB0byBhbiBhcnJheSxcbiAgICogdGhlbiB0aGUgaXRlbSBpcyBqdXN0IHJlbW92ZWQgZnJvbSB0aGUgYXJyYXkuXG4gICAqIEBwYXJhbSB7IE9iamVjdCB9IG9iaiAtIG9iamVjdCBvbiB3aGljaCB0byByZW1vdmUgdGhlIHByb3BlcnR5XG4gICAqIEBwYXJhbSB7IFN0cmluZyB9IGtleSAtIHByb3BlcnR5IG5hbWVcbiAgICogQHBhcmFtIHsgT2JqZWN0IH0gdmFsdWUgLSB0aGUgdmFsdWUgb2YgdGhlIHByb3BlcnR5IHRvIGJlIHJlbW92ZWRcbiAgICogQHBhcmFtIHsgQm9vbGVhbiB9IGVuc3VyZUFycmF5IC0gZW5zdXJlIHRoYXQgdGhlIHByb3BlcnR5IHJlbWFpbnMgYW4gYXJyYXlcbiAgKi9cbiAgZnVuY3Rpb24gYXJyYXlpc2hSZW1vdmUob2JqLCBrZXksIHZhbHVlLCBlbnN1cmVBcnJheSkge1xuICAgIGlmIChpc0FycmF5KG9ialtrZXldKSkge1xuICAgICAgdmFyIGluZGV4ID0gb2JqW2tleV0uaW5kZXhPZih2YWx1ZSk7XG4gICAgICBpZiAoaW5kZXggIT09IC0xKSB7IG9ialtrZXldLnNwbGljZShpbmRleCwgMSk7IH1cbiAgICAgIGlmICghb2JqW2tleV0ubGVuZ3RoKSB7IGRlbGV0ZSBvYmpba2V5XTsgfVxuICAgICAgZWxzZSBpZiAob2JqW2tleV0ubGVuZ3RoID09PSAxICYmICFlbnN1cmVBcnJheSkgeyBvYmpba2V5XSA9IG9ialtrZXldWzBdOyB9XG4gICAgfSBlbHNlIGlmIChvYmpba2V5XSA9PT0gdmFsdWUpXG4gICAgICB7IGRlbGV0ZSBvYmpba2V5XTsgfSAvLyBvdGhlcndpc2UganVzdCBkZWxldGUgdGhlIGtleVxuICB9XG5cbiAgLyoqXG4gICAqIEFkZHMgdGhlIGVsZW1lbnRzIGZvciBhIHZpcnR1YWwgdGFnXG4gICAqIEB0aGlzIFRhZ1xuICAgKiBAcGFyYW0geyBOb2RlIH0gc3JjIC0gdGhlIG5vZGUgdGhhdCB3aWxsIGRvIHRoZSBpbnNlcnRpbmcgb3IgYXBwZW5kaW5nXG4gICAqIEBwYXJhbSB7IFRhZyB9IHRhcmdldCAtIG9ubHkgaWYgaW5zZXJ0aW5nLCBpbnNlcnQgYmVmb3JlIHRoaXMgdGFnJ3MgZmlyc3QgY2hpbGRcbiAgICovXG4gIGZ1bmN0aW9uIG1ha2VWaXJ0dWFsKHNyYywgdGFyZ2V0KSB7XG4gICAgdmFyIHRoaXMkMSA9IHRoaXM7XG5cbiAgICB2YXIgaGVhZCA9IGNyZWF0ZURPTVBsYWNlaG9sZGVyKCk7XG4gICAgdmFyIHRhaWwgPSBjcmVhdGVET01QbGFjZWhvbGRlcigpO1xuICAgIHZhciBmcmFnID0gY3JlYXRlRnJhZ21lbnQoKTtcbiAgICB2YXIgc2liO1xuICAgIHZhciBlbDtcblxuICAgIHRoaXMucm9vdC5pbnNlcnRCZWZvcmUoaGVhZCwgdGhpcy5yb290LmZpcnN0Q2hpbGQpO1xuICAgIHRoaXMucm9vdC5hcHBlbmRDaGlsZCh0YWlsKTtcblxuICAgIHRoaXMuX18uaGVhZCA9IGVsID0gaGVhZDtcbiAgICB0aGlzLl9fLnRhaWwgPSB0YWlsO1xuXG4gICAgd2hpbGUgKGVsKSB7XG4gICAgICBzaWIgPSBlbC5uZXh0U2libGluZztcbiAgICAgIGZyYWcuYXBwZW5kQ2hpbGQoZWwpO1xuICAgICAgdGhpcyQxLl9fLnZpcnRzLnB1c2goZWwpOyAvLyBob2xkIGZvciB1bm1vdW50aW5nXG4gICAgICBlbCA9IHNpYjtcbiAgICB9XG5cbiAgICBpZiAodGFyZ2V0KVxuICAgICAgeyBzcmMuaW5zZXJ0QmVmb3JlKGZyYWcsIHRhcmdldC5fXy5oZWFkKTsgfVxuICAgIGVsc2VcbiAgICAgIHsgc3JjLmFwcGVuZENoaWxkKGZyYWcpOyB9XG4gIH1cblxuICAvKipcbiAgICogbWFrZXMgYSB0YWcgdmlydHVhbCBhbmQgcmVwbGFjZXMgYSByZWZlcmVuY2UgaW4gdGhlIGRvbVxuICAgKiBAdGhpcyBUYWdcbiAgICogQHBhcmFtIHsgdGFnIH0gdGhlIHRhZyB0byBtYWtlIHZpcnR1YWxcbiAgICogQHBhcmFtIHsgcmVmIH0gdGhlIGRvbSByZWZlcmVuY2UgbG9jYXRpb25cbiAgICovXG4gIGZ1bmN0aW9uIG1ha2VSZXBsYWNlVmlydHVhbCh0YWcsIHJlZikge1xuICAgIHZhciBmcmFnID0gY3JlYXRlRnJhZ21lbnQoKTtcbiAgICBtYWtlVmlydHVhbC5jYWxsKHRhZywgZnJhZyk7XG4gICAgcmVmLnBhcmVudE5vZGUucmVwbGFjZUNoaWxkKGZyYWcsIHJlZik7XG4gIH1cblxuICAvKipcbiAgICogVXBkYXRlIGR5bmFtaWNhbGx5IGNyZWF0ZWQgZGF0YS1pcyB0YWdzIHdpdGggY2hhbmdpbmcgZXhwcmVzc2lvbnNcbiAgICogQHBhcmFtIHsgT2JqZWN0IH0gZXhwciAtIGV4cHJlc3Npb24gdGFnIGFuZCBleHByZXNzaW9uIGluZm9cbiAgICogQHBhcmFtIHsgVGFnIH0gICAgcGFyZW50IC0gcGFyZW50IGZvciB0YWcgY3JlYXRpb25cbiAgICogQHBhcmFtIHsgU3RyaW5nIH0gdGFnTmFtZSAtIHRhZyBpbXBsZW1lbnRhdGlvbiB3ZSB3YW50IHRvIHVzZVxuICAgKi9cbiAgZnVuY3Rpb24gdXBkYXRlRGF0YUlzKGV4cHIsIHBhcmVudCwgdGFnTmFtZSkge1xuICAgIHZhciB0YWcgPSBleHByLnRhZyB8fCBleHByLmRvbS5fdGFnO1xuICAgIHZhciByZWY7XG5cbiAgICB2YXIgcmVmJDEgPSB0YWcgPyB0YWcuX18gOiB7fTtcbiAgICB2YXIgaGVhZCA9IHJlZiQxLmhlYWQ7XG4gICAgdmFyIGlzVmlydHVhbCA9IGV4cHIuZG9tLnRhZ05hbWUgPT09ICdWSVJUVUFMJztcblxuICAgIGlmICh0YWcgJiYgZXhwci50YWdOYW1lID09PSB0YWdOYW1lKSB7XG4gICAgICB0YWcudXBkYXRlKCk7XG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICAvLyBzeW5jIF9wYXJlbnQgdG8gYWNjb21tb2RhdGUgY2hhbmdpbmcgdGFnbmFtZXNcbiAgICBpZiAodGFnKSB7XG4gICAgICAvLyBuZWVkIHBsYWNlaG9sZGVyIGJlZm9yZSB1bm1vdW50XG4gICAgICBpZihpc1ZpcnR1YWwpIHtcbiAgICAgICAgcmVmID0gY3JlYXRlRE9NUGxhY2Vob2xkZXIoKTtcbiAgICAgICAgaGVhZC5wYXJlbnROb2RlLmluc2VydEJlZm9yZShyZWYsIGhlYWQpO1xuICAgICAgfVxuXG4gICAgICB0YWcudW5tb3VudCh0cnVlKTtcbiAgICB9XG5cbiAgICAvLyB1bmFibGUgdG8gZ2V0IHRoZSB0YWcgbmFtZVxuICAgIGlmICghaXNTdHJpbmcodGFnTmFtZSkpIHsgcmV0dXJuIH1cblxuICAgIGV4cHIuaW1wbCA9IF9fVEFHX0lNUExbdGFnTmFtZV07XG5cbiAgICAvLyB1bmtub3duIGltcGxlbWVudGF0aW9uXG4gICAgaWYgKCFleHByLmltcGwpIHsgcmV0dXJuIH1cblxuICAgIGV4cHIudGFnID0gdGFnID0gaW5pdENoaWxkKFxuICAgICAgZXhwci5pbXBsLCB7XG4gICAgICAgIHJvb3Q6IGV4cHIuZG9tLFxuICAgICAgICBwYXJlbnQ6IHBhcmVudCxcbiAgICAgICAgdGFnTmFtZTogdGFnTmFtZVxuICAgICAgfSxcbiAgICAgIGV4cHIuZG9tLmlubmVySFRNTCxcbiAgICAgIHBhcmVudFxuICAgICk7XG5cbiAgICBlYWNoKGV4cHIuYXR0cnMsIGZ1bmN0aW9uIChhKSB7IHJldHVybiBzZXRBdHRyaWJ1dGUodGFnLnJvb3QsIGEubmFtZSwgYS52YWx1ZSk7IH0pO1xuICAgIGV4cHIudGFnTmFtZSA9IHRhZ05hbWU7XG4gICAgdGFnLm1vdW50KCk7XG5cbiAgICAvLyByb290IGV4aXN0IGZpcnN0IHRpbWUsIGFmdGVyIHVzZSBwbGFjZWhvbGRlclxuICAgIGlmIChpc1ZpcnR1YWwpIHsgbWFrZVJlcGxhY2VWaXJ0dWFsKHRhZywgcmVmIHx8IHRhZy5yb290KTsgfVxuXG4gICAgLy8gcGFyZW50IGlzIHRoZSBwbGFjZWhvbGRlciB0YWcsIG5vdCB0aGUgZHluYW1pYyB0YWcgc28gY2xlYW4gdXBcbiAgICBwYXJlbnQuX18ub25Vbm1vdW50ID0gZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIGRlbE5hbWUgPSB0YWcub3B0cy5kYXRhSXM7XG4gICAgICBhcnJheWlzaFJlbW92ZSh0YWcucGFyZW50LnRhZ3MsIGRlbE5hbWUsIHRhZyk7XG4gICAgICBhcnJheWlzaFJlbW92ZSh0YWcuX18ucGFyZW50LnRhZ3MsIGRlbE5hbWUsIHRhZyk7XG4gICAgICB0YWcudW5tb3VudCgpO1xuICAgIH07XG4gIH1cblxuICAvKipcbiAgICogTm9tYWxpemUgYW55IGF0dHJpYnV0ZSByZW1vdmluZyB0aGUgXCJyaW90LVwiIHByZWZpeFxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IGF0dHJOYW1lIC0gb3JpZ2luYWwgYXR0cmlidXRlIG5hbWVcbiAgICogQHJldHVybnMgeyBTdHJpbmcgfSB2YWxpZCBodG1sIGF0dHJpYnV0ZSBuYW1lXG4gICAqL1xuICBmdW5jdGlvbiBub3JtYWxpemVBdHRyTmFtZShhdHRyTmFtZSkge1xuICAgIGlmICghYXR0ck5hbWUpIHsgcmV0dXJuIG51bGwgfVxuICAgIGF0dHJOYW1lID0gYXR0ck5hbWUucmVwbGFjZShBVFRSU19QUkVGSVgsICcnKTtcbiAgICBpZiAoQ0FTRV9TRU5TSVRJVkVfQVRUUklCVVRFU1thdHRyTmFtZV0pIHsgYXR0ck5hbWUgPSBDQVNFX1NFTlNJVElWRV9BVFRSSUJVVEVTW2F0dHJOYW1lXTsgfVxuICAgIHJldHVybiBhdHRyTmFtZVxuICB9XG5cbiAgLyoqXG4gICAqIFVwZGF0ZSBvbiBzaW5nbGUgdGFnIGV4cHJlc3Npb25cbiAgICogQHRoaXMgVGFnXG4gICAqIEBwYXJhbSB7IE9iamVjdCB9IGV4cHIgLSBleHByZXNzaW9uIGxvZ2ljXG4gICAqIEByZXR1cm5zIHsgdW5kZWZpbmVkIH1cbiAgICovXG4gIGZ1bmN0aW9uIHVwZGF0ZUV4cHJlc3Npb24oZXhwcikge1xuICAgIGlmICh0aGlzLnJvb3QgJiYgZ2V0QXR0cmlidXRlKHRoaXMucm9vdCwndmlydHVhbGl6ZWQnKSkgeyByZXR1cm4gfVxuXG4gICAgdmFyIGRvbSA9IGV4cHIuZG9tO1xuICAgIC8vIHJlbW92ZSB0aGUgcmlvdC0gcHJlZml4XG4gICAgdmFyIGF0dHJOYW1lID0gbm9ybWFsaXplQXR0ck5hbWUoZXhwci5hdHRyKTtcbiAgICB2YXIgaXNUb2dnbGUgPSBjb250YWlucyhbU0hPV19ESVJFQ1RJVkUsIEhJREVfRElSRUNUSVZFXSwgYXR0ck5hbWUpO1xuICAgIHZhciBpc1ZpcnR1YWwgPSBleHByLnJvb3QgJiYgZXhwci5yb290LnRhZ05hbWUgPT09ICdWSVJUVUFMJztcbiAgICB2YXIgcmVmID0gdGhpcy5fXztcbiAgICB2YXIgaXNBbm9ueW1vdXMgPSByZWYuaXNBbm9ueW1vdXM7XG4gICAgdmFyIHBhcmVudCA9IGRvbSAmJiAoZXhwci5wYXJlbnQgfHwgZG9tLnBhcmVudE5vZGUpO1xuICAgIC8vIGRldGVjdCB0aGUgc3R5bGUgYXR0cmlidXRlc1xuICAgIHZhciBpc1N0eWxlQXR0ciA9IGF0dHJOYW1lID09PSAnc3R5bGUnO1xuICAgIHZhciBpc0NsYXNzQXR0ciA9IGF0dHJOYW1lID09PSAnY2xhc3MnO1xuXG4gICAgdmFyIHZhbHVlO1xuXG4gICAgLy8gaWYgaXQncyBhIHRhZyB3ZSBjb3VsZCB0b3RhbGx5IHNraXAgdGhlIHJlc3RcbiAgICBpZiAoZXhwci5fcmlvdF9pZCkge1xuICAgICAgaWYgKGV4cHIuX18ud2FzQ3JlYXRlZCkge1xuICAgICAgICBleHByLnVwZGF0ZSgpO1xuICAgICAgLy8gaWYgaXQgaGFzbid0IGJlZW4gbW91bnRlZCB5ZXQsIGRvIHRoYXQgbm93LlxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgZXhwci5tb3VudCgpO1xuICAgICAgICBpZiAoaXNWaXJ0dWFsKSB7XG4gICAgICAgICAgbWFrZVJlcGxhY2VWaXJ0dWFsKGV4cHIsIGV4cHIucm9vdCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIC8vIGlmIHRoaXMgZXhwcmVzc2lvbiBoYXMgdGhlIHVwZGF0ZSBtZXRob2QgaXQgbWVhbnMgaXQgY2FuIGhhbmRsZSB0aGUgRE9NIGNoYW5nZXMgYnkgaXRzZWxmXG4gICAgaWYgKGV4cHIudXBkYXRlKSB7IHJldHVybiBleHByLnVwZGF0ZSgpIH1cblxuICAgIHZhciBjb250ZXh0ID0gaXNUb2dnbGUgJiYgIWlzQW5vbnltb3VzID8gaW5oZXJpdFBhcmVudFByb3BzLmNhbGwodGhpcykgOiB0aGlzO1xuXG4gICAgLy8gLi4uaXQgc2VlbXMgdG8gYmUgYSBzaW1wbGUgZXhwcmVzc2lvbiBzbyB3ZSB0cnkgdG8gY2FsY3VsYXRlIGl0cyB2YWx1ZVxuICAgIHZhbHVlID0gdG1wbChleHByLmV4cHIsIGNvbnRleHQpO1xuXG4gICAgdmFyIGhhc1ZhbHVlID0gIWlzQmxhbmsodmFsdWUpO1xuICAgIHZhciBpc09iaiA9IGlzT2JqZWN0KHZhbHVlKTtcblxuICAgIC8vIGNvbnZlcnQgdGhlIHN0eWxlL2NsYXNzIG9iamVjdHMgdG8gc3RyaW5nc1xuICAgIGlmIChpc09iaikge1xuICAgICAgaWYgKGlzQ2xhc3NBdHRyKSB7XG4gICAgICAgIHZhbHVlID0gdG1wbChKU09OLnN0cmluZ2lmeSh2YWx1ZSksIHRoaXMpO1xuICAgICAgfSBlbHNlIGlmIChpc1N0eWxlQXR0cikge1xuICAgICAgICB2YWx1ZSA9IHN0eWxlT2JqZWN0VG9TdHJpbmcodmFsdWUpO1xuICAgICAgfVxuICAgIH1cblxuICAgIC8vIHJlbW92ZSBvcmlnaW5hbCBhdHRyaWJ1dGVcbiAgICBpZiAoZXhwci5hdHRyICYmICghZXhwci53YXNQYXJzZWRPbmNlIHx8ICFoYXNWYWx1ZSB8fCB2YWx1ZSA9PT0gZmFsc2UpKSB7XG4gICAgICAvLyByZW1vdmUgZWl0aGVyIHJpb3QtKiBhdHRyaWJ1dGVzIG9yIGp1c3QgdGhlIGF0dHJpYnV0ZSBuYW1lXG4gICAgICByZW1vdmVBdHRyaWJ1dGUoZG9tLCBnZXRBdHRyaWJ1dGUoZG9tLCBleHByLmF0dHIpID8gZXhwci5hdHRyIDogYXR0ck5hbWUpO1xuICAgIH1cblxuICAgIC8vIGZvciB0aGUgYm9vbGVhbiBhdHRyaWJ1dGVzIHdlIGRvbid0IG5lZWQgdGhlIHZhbHVlXG4gICAgLy8gd2UgY2FuIGNvbnZlcnQgaXQgdG8gY2hlY2tlZD10cnVlIHRvIGNoZWNrZWQ9Y2hlY2tlZFxuICAgIGlmIChleHByLmJvb2wpIHsgdmFsdWUgPSB2YWx1ZSA/IGF0dHJOYW1lIDogZmFsc2U7IH1cbiAgICBpZiAoZXhwci5pc1J0YWcpIHsgcmV0dXJuIHVwZGF0ZURhdGFJcyhleHByLCB0aGlzLCB2YWx1ZSkgfVxuICAgIGlmIChleHByLndhc1BhcnNlZE9uY2UgJiYgZXhwci52YWx1ZSA9PT0gdmFsdWUpIHsgcmV0dXJuIH1cblxuICAgIC8vIHVwZGF0ZSB0aGUgZXhwcmVzc2lvbiB2YWx1ZVxuICAgIGV4cHIudmFsdWUgPSB2YWx1ZTtcbiAgICBleHByLndhc1BhcnNlZE9uY2UgPSB0cnVlO1xuXG4gICAgLy8gaWYgdGhlIHZhbHVlIGlzIGFuIG9iamVjdCAoYW5kIGl0J3Mgbm90IGEgc3R5bGUgb3IgY2xhc3MgYXR0cmlidXRlKSB3ZSBjYW4gbm90IGRvIG11Y2ggbW9yZSB3aXRoIGl0XG4gICAgaWYgKGlzT2JqICYmICFpc0NsYXNzQXR0ciAmJiAhaXNTdHlsZUF0dHIgJiYgIWlzVG9nZ2xlKSB7IHJldHVybiB9XG4gICAgLy8gYXZvaWQgdG8gcmVuZGVyIHVuZGVmaW5lZC9udWxsIHZhbHVlc1xuICAgIGlmICghaGFzVmFsdWUpIHsgdmFsdWUgPSAnJzsgfVxuXG4gICAgLy8gdGV4dGFyZWEgYW5kIHRleHQgbm9kZXMgaGF2ZSBubyBhdHRyaWJ1dGUgbmFtZVxuICAgIGlmICghYXR0ck5hbWUpIHtcbiAgICAgIC8vIGFib3V0ICM4MTUgdy9vIHJlcGxhY2U6IHRoZSBicm93c2VyIGNvbnZlcnRzIHRoZSB2YWx1ZSB0byBhIHN0cmluZyxcbiAgICAgIC8vIHRoZSBjb21wYXJpc29uIGJ5IFwiPT1cIiBkb2VzIHRvbywgYnV0IG5vdCBpbiB0aGUgc2VydmVyXG4gICAgICB2YWx1ZSArPSAnJztcbiAgICAgIC8vIHRlc3QgZm9yIHBhcmVudCBhdm9pZHMgZXJyb3Igd2l0aCBpbnZhbGlkIGFzc2lnbm1lbnQgdG8gbm9kZVZhbHVlXG4gICAgICBpZiAocGFyZW50KSB7XG4gICAgICAgIC8vIGNhY2hlIHRoZSBwYXJlbnQgbm9kZSBiZWNhdXNlIHNvbWVob3cgaXQgd2lsbCBiZWNvbWUgbnVsbCBvbiBJRVxuICAgICAgICAvLyBvbiB0aGUgbmV4dCBpdGVyYXRpb25cbiAgICAgICAgZXhwci5wYXJlbnQgPSBwYXJlbnQ7XG4gICAgICAgIGlmIChwYXJlbnQudGFnTmFtZSA9PT0gJ1RFWFRBUkVBJykge1xuICAgICAgICAgIHBhcmVudC52YWx1ZSA9IHZhbHVlOyAgICAgICAgICAgICAgICAgICAgLy8gIzExMTNcbiAgICAgICAgICBpZiAoIUlFX1ZFUlNJT04pIHsgZG9tLm5vZGVWYWx1ZSA9IHZhbHVlOyB9ICAvLyAjMTYyNSBJRSB0aHJvd3MgaGVyZSwgbm9kZVZhbHVlXG4gICAgICAgIH0gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIHdpbGwgYmUgYXZhaWxhYmxlIG9uICd1cGRhdGVkJ1xuICAgICAgICBlbHNlIHsgZG9tLm5vZGVWYWx1ZSA9IHZhbHVlOyB9XG4gICAgICB9XG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICBzd2l0Y2ggKHRydWUpIHtcbiAgICAvLyBoYW5kbGUgZXZlbnRzIGJpbmRpbmdcbiAgICBjYXNlIGlzRnVuY3Rpb24odmFsdWUpOlxuICAgICAgaWYgKGlzRXZlbnRBdHRyaWJ1dGUoYXR0ck5hbWUpKSB7XG4gICAgICAgIHNldEV2ZW50SGFuZGxlcihhdHRyTmFtZSwgdmFsdWUsIGRvbSwgdGhpcyk7XG4gICAgICB9XG4gICAgICBicmVha1xuICAgIC8vIHNob3cgLyBoaWRlXG4gICAgY2FzZSBpc1RvZ2dsZTpcbiAgICAgIHRvZ2dsZVZpc2liaWxpdHkoZG9tLCBhdHRyTmFtZSA9PT0gSElERV9ESVJFQ1RJVkUgPyAhdmFsdWUgOiB2YWx1ZSk7XG4gICAgICBicmVha1xuICAgIC8vIGhhbmRsZSBhdHRyaWJ1dGVzXG4gICAgZGVmYXVsdDpcbiAgICAgIGlmIChleHByLmJvb2wpIHtcbiAgICAgICAgZG9tW2F0dHJOYW1lXSA9IHZhbHVlO1xuICAgICAgfVxuXG4gICAgICBpZiAoYXR0ck5hbWUgPT09ICd2YWx1ZScgJiYgZG9tLnZhbHVlICE9PSB2YWx1ZSkge1xuICAgICAgICBkb20udmFsdWUgPSB2YWx1ZTtcbiAgICAgIH0gZWxzZSBpZiAoaGFzVmFsdWUgJiYgdmFsdWUgIT09IGZhbHNlKSB7XG4gICAgICAgIHNldEF0dHJpYnV0ZShkb20sIGF0dHJOYW1lLCB2YWx1ZSk7XG4gICAgICB9XG5cbiAgICAgIC8vIG1ha2Ugc3VyZSB0aGF0IGluIGNhc2Ugb2Ygc3R5bGUgY2hhbmdlc1xuICAgICAgLy8gdGhlIGVsZW1lbnQgc3RheXMgaGlkZGVuXG4gICAgICBpZiAoaXNTdHlsZUF0dHIgJiYgZG9tLmhpZGRlbikgeyB0b2dnbGVWaXNpYmlsaXR5KGRvbSwgZmFsc2UpOyB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIFVwZGF0ZSBhbGwgdGhlIGV4cHJlc3Npb25zIGluIGEgVGFnIGluc3RhbmNlXG4gICAqIEB0aGlzIFRhZ1xuICAgKiBAcGFyYW0geyBBcnJheSB9IGV4cHJlc3Npb25zIC0gZXhwcmVzc2lvbiB0aGF0IG11c3QgYmUgcmUgZXZhbHVhdGVkXG4gICAqL1xuICBmdW5jdGlvbiB1cGRhdGUoZXhwcmVzc2lvbnMpIHtcbiAgICBlYWNoKGV4cHJlc3Npb25zLCB1cGRhdGVFeHByZXNzaW9uLmJpbmQodGhpcykpO1xuICB9XG5cbiAgLyoqXG4gICAqIFdlIG5lZWQgdG8gdXBkYXRlIG9wdHMgZm9yIHRoaXMgdGFnLiBUaGF0IHJlcXVpcmVzIHVwZGF0aW5nIHRoZSBleHByZXNzaW9uc1xuICAgKiBpbiBhbnkgYXR0cmlidXRlcyBvbiB0aGUgdGFnLCBhbmQgdGhlbiBjb3B5aW5nIHRoZSByZXN1bHQgb250byBvcHRzLlxuICAgKiBAdGhpcyBUYWdcbiAgICogQHBhcmFtICAge0Jvb2xlYW59IGlzTG9vcCAtIGlzIGl0IGEgbG9vcCB0YWc/XG4gICAqIEBwYXJhbSAgIHsgVGFnIH0gIHBhcmVudCAtIHBhcmVudCB0YWcgbm9kZVxuICAgKiBAcGFyYW0gICB7IEJvb2xlYW4gfSAgaXNBbm9ueW1vdXMgLSBpcyBpdCBhIHRhZyB3aXRob3V0IGFueSBpbXBsPyAoYSB0YWcgbm90IHJlZ2lzdGVyZWQpXG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gIG9wdHMgLSB0YWcgb3B0aW9uc1xuICAgKiBAcGFyYW0gICB7IEFycmF5IH0gIGluc3RBdHRycyAtIHRhZyBhdHRyaWJ1dGVzIGFycmF5XG4gICAqL1xuICBmdW5jdGlvbiB1cGRhdGVPcHRzKGlzTG9vcCwgcGFyZW50LCBpc0Fub255bW91cywgb3B0cywgaW5zdEF0dHJzKSB7XG4gICAgLy8gaXNBbm9ueW1vdXMgYGVhY2hgIHRhZ3MgdHJlYXQgYGRvbWAgYW5kIGByb290YCBkaWZmZXJlbnRseS4gSW4gdGhpcyBjYXNlXG4gICAgLy8gKGFuZCBvbmx5IHRoaXMgY2FzZSkgd2UgZG9uJ3QgbmVlZCB0byBkbyB1cGRhdGVPcHRzLCBiZWNhdXNlIHRoZSByZWd1bGFyIHBhcnNlXG4gICAgLy8gd2lsbCB1cGRhdGUgdGhvc2UgYXR0cnMuIFBsdXMsIGlzQW5vbnltb3VzIHRhZ3MgZG9uJ3QgbmVlZCBvcHRzIGFueXdheVxuICAgIGlmIChpc0xvb3AgJiYgaXNBbm9ueW1vdXMpIHsgcmV0dXJuIH1cbiAgICB2YXIgY3R4ID0gaXNMb29wID8gaW5oZXJpdFBhcmVudFByb3BzLmNhbGwodGhpcykgOiBwYXJlbnQgfHwgdGhpcztcblxuICAgIGVhY2goaW5zdEF0dHJzLCBmdW5jdGlvbiAoYXR0cikge1xuICAgICAgaWYgKGF0dHIuZXhwcikgeyB1cGRhdGVFeHByZXNzaW9uLmNhbGwoY3R4LCBhdHRyLmV4cHIpOyB9XG4gICAgICAvLyBub3JtYWxpemUgdGhlIGF0dHJpYnV0ZSBuYW1lc1xuICAgICAgb3B0c1t0b0NhbWVsKGF0dHIubmFtZSkucmVwbGFjZShBVFRSU19QUkVGSVgsICcnKV0gPSBhdHRyLmV4cHIgPyBhdHRyLmV4cHIudmFsdWUgOiBhdHRyLnZhbHVlO1xuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIFVwZGF0ZSB0aGUgdGFnIGV4cHJlc3Npb25zIGFuZCBvcHRpb25zXG4gICAqIEBwYXJhbSB7IFRhZyB9IHRhZyAtIHRhZyBvYmplY3RcbiAgICogQHBhcmFtIHsgKiB9IGRhdGEgLSBkYXRhIHdlIHdhbnQgdG8gdXNlIHRvIGV4dGVuZCB0aGUgdGFnIHByb3BlcnRpZXNcbiAgICogQHBhcmFtIHsgQXJyYXkgfSBleHByZXNzaW9ucyAtIGNvbXBvbmVudCBleHByZXNzaW9ucyBhcnJheVxuICAgKiBAcmV0dXJucyB7IFRhZyB9IHRoZSBjdXJyZW50IHRhZyBpbnN0YW5jZVxuICAgKi9cbiAgZnVuY3Rpb24gY29tcG9uZW50VXBkYXRlKHRhZywgZGF0YSwgZXhwcmVzc2lvbnMpIHtcbiAgICB2YXIgX18gPSB0YWcuX187XG4gICAgdmFyIG5leHRPcHRzID0ge307XG4gICAgdmFyIGNhblRyaWdnZXIgPSB0YWcuaXNNb3VudGVkICYmICFfXy5za2lwQW5vbnltb3VzO1xuXG4gICAgLy8gaW5oZXJpdCBwcm9wZXJ0aWVzIGZyb20gdGhlIHBhcmVudCB0YWdcbiAgICBpZiAoX18uaXNBbm9ueW1vdXMgJiYgX18ucGFyZW50KSB7IGV4dGVuZCh0YWcsIF9fLnBhcmVudCk7IH1cbiAgICBleHRlbmQodGFnLCBkYXRhKTtcblxuICAgIHVwZGF0ZU9wdHMuYXBwbHkodGFnLCBbX18uaXNMb29wLCBfXy5wYXJlbnQsIF9fLmlzQW5vbnltb3VzLCBuZXh0T3B0cywgX18uaW5zdEF0dHJzXSk7XG5cbiAgICBpZiAoXG4gICAgICBjYW5UcmlnZ2VyICYmXG4gICAgICB0YWcuaXNNb3VudGVkICYmXG4gICAgICBpc0Z1bmN0aW9uKHRhZy5zaG91bGRVcGRhdGUpICYmICF0YWcuc2hvdWxkVXBkYXRlKGRhdGEsIG5leHRPcHRzKVxuICAgICkge1xuICAgICAgcmV0dXJuIHRhZ1xuICAgIH1cblxuICAgIGV4dGVuZCh0YWcub3B0cywgbmV4dE9wdHMpO1xuXG4gICAgaWYgKGNhblRyaWdnZXIpIHsgdGFnLnRyaWdnZXIoJ3VwZGF0ZScsIGRhdGEpOyB9XG4gICAgdXBkYXRlLmNhbGwodGFnLCBleHByZXNzaW9ucyk7XG4gICAgaWYgKGNhblRyaWdnZXIpIHsgdGFnLnRyaWdnZXIoJ3VwZGF0ZWQnKTsgfVxuXG4gICAgcmV0dXJuIHRhZ1xuICB9XG5cbiAgLyoqXG4gICAqIEdldCBzZWxlY3RvcnMgZm9yIHRhZ3NcbiAgICogQHBhcmFtICAgeyBBcnJheSB9IHRhZ3MgLSB0YWcgbmFtZXMgdG8gc2VsZWN0XG4gICAqIEByZXR1cm5zIHsgU3RyaW5nIH0gc2VsZWN0b3JcbiAgICovXG4gIGZ1bmN0aW9uIHF1ZXJ5KHRhZ3MpIHtcbiAgICAvLyBzZWxlY3QgYWxsIHRhZ3NcbiAgICBpZiAoIXRhZ3MpIHtcbiAgICAgIHZhciBrZXlzID0gT2JqZWN0LmtleXMoX19UQUdfSU1QTCk7XG4gICAgICByZXR1cm4ga2V5cyArIHF1ZXJ5KGtleXMpXG4gICAgfVxuXG4gICAgcmV0dXJuIHRhZ3NcbiAgICAgIC5maWx0ZXIoZnVuY3Rpb24gKHQpIHsgcmV0dXJuICEvW14tXFx3XS8udGVzdCh0KTsgfSlcbiAgICAgIC5yZWR1Y2UoZnVuY3Rpb24gKGxpc3QsIHQpIHtcbiAgICAgICAgdmFyIG5hbWUgPSB0LnRyaW0oKS50b0xvd2VyQ2FzZSgpO1xuICAgICAgICByZXR1cm4gbGlzdCArIFwiLFtcIiArIElTX0RJUkVDVElWRSArIFwiPVxcXCJcIiArIG5hbWUgKyBcIlxcXCJdXCJcbiAgICAgIH0sICcnKVxuICB9XG5cbiAgLyoqXG4gICAqIEFub3RoZXIgd2F5IHRvIGNyZWF0ZSBhIHJpb3QgdGFnIGEgYml0IG1vcmUgZXM2IGZyaWVuZGx5XG4gICAqIEBwYXJhbSB7IEhUTUxFbGVtZW50IH0gZWwgLSB0YWcgRE9NIHNlbGVjdG9yIG9yIERPTSBub2RlL3NcbiAgICogQHBhcmFtIHsgT2JqZWN0IH0gb3B0cyAtIHRhZyBsb2dpY1xuICAgKiBAcmV0dXJucyB7IFRhZyB9IG5ldyByaW90IHRhZyBpbnN0YW5jZVxuICAgKi9cbiAgZnVuY3Rpb24gVGFnKGVsLCBvcHRzKSB7XG4gICAgLy8gZ2V0IHRoZSB0YWcgcHJvcGVydGllcyBmcm9tIHRoZSBjbGFzcyBjb25zdHJ1Y3RvclxuICAgIHZhciByZWYgPSB0aGlzO1xuICAgIHZhciBuYW1lID0gcmVmLm5hbWU7XG4gICAgdmFyIHRtcGwgPSByZWYudG1wbDtcbiAgICB2YXIgY3NzID0gcmVmLmNzcztcbiAgICB2YXIgYXR0cnMgPSByZWYuYXR0cnM7XG4gICAgdmFyIG9uQ3JlYXRlID0gcmVmLm9uQ3JlYXRlO1xuICAgIC8vIHJlZ2lzdGVyIGEgbmV3IHRhZyBhbmQgY2FjaGUgdGhlIGNsYXNzIHByb3RvdHlwZVxuICAgIGlmICghX19UQUdfSU1QTFtuYW1lXSkge1xuICAgICAgdGFnKG5hbWUsIHRtcGwsIGNzcywgYXR0cnMsIG9uQ3JlYXRlKTtcbiAgICAgIC8vIGNhY2hlIHRoZSBjbGFzcyBjb25zdHJ1Y3RvclxuICAgICAgX19UQUdfSU1QTFtuYW1lXS5jbGFzcyA9IHRoaXMuY29uc3RydWN0b3I7XG4gICAgfVxuXG4gICAgLy8gbW91bnQgdGhlIHRhZyB1c2luZyB0aGUgY2xhc3MgaW5zdGFuY2VcbiAgICBtb3VudCQxKGVsLCBuYW1lLCBvcHRzLCB0aGlzKTtcbiAgICAvLyBpbmplY3QgdGhlIGNvbXBvbmVudCBjc3NcbiAgICBpZiAoY3NzKSB7IHN0eWxlTWFuYWdlci5pbmplY3QoKTsgfVxuXG4gICAgcmV0dXJuIHRoaXNcbiAgfVxuXG4gIC8qKlxuICAgKiBDcmVhdGUgYSBuZXcgcmlvdCB0YWcgaW1wbGVtZW50YXRpb25cbiAgICogQHBhcmFtICAgeyBTdHJpbmcgfSAgIG5hbWUgLSBuYW1lL2lkIG9mIHRoZSBuZXcgcmlvdCB0YWdcbiAgICogQHBhcmFtICAgeyBTdHJpbmcgfSAgIHRtcGwgLSB0YWcgdGVtcGxhdGVcbiAgICogQHBhcmFtICAgeyBTdHJpbmcgfSAgIGNzcyAtIGN1c3RvbSB0YWcgY3NzXG4gICAqIEBwYXJhbSAgIHsgU3RyaW5nIH0gICBhdHRycyAtIHJvb3QgdGFnIGF0dHJpYnV0ZXNcbiAgICogQHBhcmFtICAgeyBGdW5jdGlvbiB9IGZuIC0gdXNlciBmdW5jdGlvblxuICAgKiBAcmV0dXJucyB7IFN0cmluZyB9IG5hbWUvaWQgb2YgdGhlIHRhZyBqdXN0IGNyZWF0ZWRcbiAgICovXG4gIGZ1bmN0aW9uIHRhZyhuYW1lLCB0bXBsLCBjc3MsIGF0dHJzLCBmbikge1xuICAgIGlmIChpc0Z1bmN0aW9uKGF0dHJzKSkge1xuICAgICAgZm4gPSBhdHRycztcblxuICAgICAgaWYgKC9eW1xcdy1dK1xccz89Ly50ZXN0KGNzcykpIHtcbiAgICAgICAgYXR0cnMgPSBjc3M7XG4gICAgICAgIGNzcyA9ICcnO1xuICAgICAgfSBlbHNlXG4gICAgICAgIHsgYXR0cnMgPSAnJzsgfVxuICAgIH1cblxuICAgIGlmIChjc3MpIHtcbiAgICAgIGlmIChpc0Z1bmN0aW9uKGNzcykpXG4gICAgICAgIHsgZm4gPSBjc3M7IH1cbiAgICAgIGVsc2VcbiAgICAgICAgeyBzdHlsZU1hbmFnZXIuYWRkKGNzcywgbmFtZSk7IH1cbiAgICB9XG5cbiAgICBuYW1lID0gbmFtZS50b0xvd2VyQ2FzZSgpO1xuICAgIF9fVEFHX0lNUExbbmFtZV0gPSB7IG5hbWU6IG5hbWUsIHRtcGw6IHRtcGwsIGF0dHJzOiBhdHRycywgZm46IGZuIH07XG5cbiAgICByZXR1cm4gbmFtZVxuICB9XG5cbiAgLyoqXG4gICAqIENyZWF0ZSBhIG5ldyByaW90IHRhZyBpbXBsZW1lbnRhdGlvbiAoZm9yIHVzZSBieSB0aGUgY29tcGlsZXIpXG4gICAqIEBwYXJhbSAgIHsgU3RyaW5nIH0gICBuYW1lIC0gbmFtZS9pZCBvZiB0aGUgbmV3IHJpb3QgdGFnXG4gICAqIEBwYXJhbSAgIHsgU3RyaW5nIH0gICB0bXBsIC0gdGFnIHRlbXBsYXRlXG4gICAqIEBwYXJhbSAgIHsgU3RyaW5nIH0gICBjc3MgLSBjdXN0b20gdGFnIGNzc1xuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9ICAgYXR0cnMgLSByb290IHRhZyBhdHRyaWJ1dGVzXG4gICAqIEBwYXJhbSAgIHsgRnVuY3Rpb24gfSBmbiAtIHVzZXIgZnVuY3Rpb25cbiAgICogQHJldHVybnMgeyBTdHJpbmcgfSBuYW1lL2lkIG9mIHRoZSB0YWcganVzdCBjcmVhdGVkXG4gICAqL1xuICBmdW5jdGlvbiB0YWcyKG5hbWUsIHRtcGwsIGNzcywgYXR0cnMsIGZuKSB7XG4gICAgaWYgKGNzcykgeyBzdHlsZU1hbmFnZXIuYWRkKGNzcywgbmFtZSk7IH1cblxuICAgIF9fVEFHX0lNUExbbmFtZV0gPSB7IG5hbWU6IG5hbWUsIHRtcGw6IHRtcGwsIGF0dHJzOiBhdHRycywgZm46IGZuIH07XG5cbiAgICByZXR1cm4gbmFtZVxuICB9XG5cbiAgLyoqXG4gICAqIE1vdW50IGEgdGFnIHVzaW5nIGEgc3BlY2lmaWMgdGFnIGltcGxlbWVudGF0aW9uXG4gICAqIEBwYXJhbSAgIHsgKiB9IHNlbGVjdG9yIC0gdGFnIERPTSBzZWxlY3RvciBvciBET00gbm9kZS9zXG4gICAqIEBwYXJhbSAgIHsgU3RyaW5nIH0gdGFnTmFtZSAtIHRhZyBpbXBsZW1lbnRhdGlvbiBuYW1lXG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gb3B0cyAtIHRhZyBsb2dpY1xuICAgKiBAcmV0dXJucyB7IEFycmF5IH0gbmV3IHRhZ3MgaW5zdGFuY2VzXG4gICAqL1xuICBmdW5jdGlvbiBtb3VudChzZWxlY3RvciwgdGFnTmFtZSwgb3B0cykge1xuICAgIHZhciB0YWdzID0gW107XG4gICAgdmFyIGVsZW0sIGFsbFRhZ3M7XG5cbiAgICBmdW5jdGlvbiBwdXNoVGFnc1RvKHJvb3QpIHtcbiAgICAgIGlmIChyb290LnRhZ05hbWUpIHtcbiAgICAgICAgdmFyIHJpb3RUYWcgPSBnZXRBdHRyaWJ1dGUocm9vdCwgSVNfRElSRUNUSVZFKSwgdGFnO1xuXG4gICAgICAgIC8vIGhhdmUgdGFnTmFtZT8gZm9yY2UgcmlvdC10YWcgdG8gYmUgdGhlIHNhbWVcbiAgICAgICAgaWYgKHRhZ05hbWUgJiYgcmlvdFRhZyAhPT0gdGFnTmFtZSkge1xuICAgICAgICAgIHJpb3RUYWcgPSB0YWdOYW1lO1xuICAgICAgICAgIHNldEF0dHJpYnV0ZShyb290LCBJU19ESVJFQ1RJVkUsIHRhZ05hbWUpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGFnID0gbW91bnQkMShyb290LCByaW90VGFnIHx8IHJvb3QudGFnTmFtZS50b0xvd2VyQ2FzZSgpLCBvcHRzKTtcblxuICAgICAgICBpZiAodGFnKVxuICAgICAgICAgIHsgdGFncy5wdXNoKHRhZyk7IH1cbiAgICAgIH0gZWxzZSBpZiAocm9vdC5sZW5ndGgpXG4gICAgICAgIHsgZWFjaChyb290LCBwdXNoVGFnc1RvKTsgfSAvLyBhc3N1bWUgbm9kZUxpc3RcbiAgICB9XG5cbiAgICAvLyBpbmplY3Qgc3R5bGVzIGludG8gRE9NXG4gICAgc3R5bGVNYW5hZ2VyLmluamVjdCgpO1xuXG4gICAgaWYgKGlzT2JqZWN0KHRhZ05hbWUpKSB7XG4gICAgICBvcHRzID0gdGFnTmFtZTtcbiAgICAgIHRhZ05hbWUgPSAwO1xuICAgIH1cblxuICAgIC8vIGNyYXdsIHRoZSBET00gdG8gZmluZCB0aGUgdGFnXG4gICAgaWYgKGlzU3RyaW5nKHNlbGVjdG9yKSkge1xuICAgICAgc2VsZWN0b3IgPSBzZWxlY3RvciA9PT0gJyonID9cbiAgICAgICAgLy8gc2VsZWN0IGFsbCByZWdpc3RlcmVkIHRhZ3NcbiAgICAgICAgLy8gJiB0YWdzIGZvdW5kIHdpdGggdGhlIHJpb3QtdGFnIGF0dHJpYnV0ZSBzZXRcbiAgICAgICAgYWxsVGFncyA9IHF1ZXJ5KCkgOlxuICAgICAgICAvLyBvciBqdXN0IHRoZSBvbmVzIG5hbWVkIGxpa2UgdGhlIHNlbGVjdG9yXG4gICAgICAgIHNlbGVjdG9yICsgcXVlcnkoc2VsZWN0b3Iuc3BsaXQoLywgKi8pKTtcblxuICAgICAgLy8gbWFrZSBzdXJlIHRvIHBhc3MgYWx3YXlzIGEgc2VsZWN0b3JcbiAgICAgIC8vIHRvIHRoZSBxdWVyeVNlbGVjdG9yQWxsIGZ1bmN0aW9uXG4gICAgICBlbGVtID0gc2VsZWN0b3IgPyAkJChzZWxlY3RvcikgOiBbXTtcbiAgICB9XG4gICAgZWxzZVxuICAgICAgLy8gcHJvYmFibHkgeW91IGhhdmUgcGFzc2VkIGFscmVhZHkgYSB0YWcgb3IgYSBOb2RlTGlzdFxuICAgICAgeyBlbGVtID0gc2VsZWN0b3I7IH1cblxuICAgIC8vIHNlbGVjdCBhbGwgdGhlIHJlZ2lzdGVyZWQgYW5kIG1vdW50IHRoZW0gaW5zaWRlIHRoZWlyIHJvb3QgZWxlbWVudHNcbiAgICBpZiAodGFnTmFtZSA9PT0gJyonKSB7XG4gICAgICAvLyBnZXQgYWxsIGN1c3RvbSB0YWdzXG4gICAgICB0YWdOYW1lID0gYWxsVGFncyB8fCBxdWVyeSgpO1xuICAgICAgLy8gaWYgdGhlIHJvb3QgZWxzIGl0J3MganVzdCBhIHNpbmdsZSB0YWdcbiAgICAgIGlmIChlbGVtLnRhZ05hbWUpXG4gICAgICAgIHsgZWxlbSA9ICQkKHRhZ05hbWUsIGVsZW0pOyB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgLy8gc2VsZWN0IGFsbCB0aGUgY2hpbGRyZW4gZm9yIGFsbCB0aGUgZGlmZmVyZW50IHJvb3QgZWxlbWVudHNcbiAgICAgICAgdmFyIG5vZGVMaXN0ID0gW107XG5cbiAgICAgICAgZWFjaChlbGVtLCBmdW5jdGlvbiAoX2VsKSB7IHJldHVybiBub2RlTGlzdC5wdXNoKCQkKHRhZ05hbWUsIF9lbCkpOyB9KTtcblxuICAgICAgICBlbGVtID0gbm9kZUxpc3Q7XG4gICAgICB9XG4gICAgICAvLyBnZXQgcmlkIG9mIHRoZSB0YWdOYW1lXG4gICAgICB0YWdOYW1lID0gMDtcbiAgICB9XG5cbiAgICBwdXNoVGFnc1RvKGVsZW0pO1xuXG4gICAgcmV0dXJuIHRhZ3NcbiAgfVxuXG4gIC8vIENyZWF0ZSBhIG1peGluIHRoYXQgY291bGQgYmUgZ2xvYmFsbHkgc2hhcmVkIGFjcm9zcyBhbGwgdGhlIHRhZ3NcbiAgdmFyIG1peGlucyA9IHt9O1xuICB2YXIgZ2xvYmFscyA9IG1peGluc1tHTE9CQUxfTUlYSU5dID0ge307XG4gIHZhciBtaXhpbnNfaWQgPSAwO1xuXG4gIC8qKlxuICAgKiBDcmVhdGUvUmV0dXJuIGEgbWl4aW4gYnkgaXRzIG5hbWVcbiAgICogQHBhcmFtICAgeyBTdHJpbmcgfSAgbmFtZSAtIG1peGluIG5hbWUgKGdsb2JhbCBtaXhpbiBpZiBvYmplY3QpXG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gIG1peCAtIG1peGluIGxvZ2ljXG4gICAqIEBwYXJhbSAgIHsgQm9vbGVhbiB9IGcgLSBpcyBnbG9iYWw/XG4gICAqIEByZXR1cm5zIHsgT2JqZWN0IH0gIHRoZSBtaXhpbiBsb2dpY1xuICAgKi9cbiAgZnVuY3Rpb24gbWl4aW4obmFtZSwgbWl4LCBnKSB7XG4gICAgLy8gVW5uYW1lZCBnbG9iYWxcbiAgICBpZiAoaXNPYmplY3QobmFtZSkpIHtcbiAgICAgIG1peGluKChcIl9fXCIgKyAobWl4aW5zX2lkKyspICsgXCJfX1wiKSwgbmFtZSwgdHJ1ZSk7XG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICB2YXIgc3RvcmUgPSBnID8gZ2xvYmFscyA6IG1peGlucztcblxuICAgIC8vIEdldHRlclxuICAgIGlmICghbWl4KSB7XG4gICAgICBpZiAoaXNVbmRlZmluZWQoc3RvcmVbbmFtZV0pKVxuICAgICAgICB7IHRocm93IG5ldyBFcnJvcigoXCJVbnJlZ2lzdGVyZWQgbWl4aW46IFwiICsgbmFtZSkpIH1cblxuICAgICAgcmV0dXJuIHN0b3JlW25hbWVdXG4gICAgfVxuXG4gICAgLy8gU2V0dGVyXG4gICAgc3RvcmVbbmFtZV0gPSBpc0Z1bmN0aW9uKG1peCkgP1xuICAgICAgZXh0ZW5kKG1peC5wcm90b3R5cGUsIHN0b3JlW25hbWVdIHx8IHt9KSAmJiBtaXggOlxuICAgICAgZXh0ZW5kKHN0b3JlW25hbWVdIHx8IHt9LCBtaXgpO1xuICB9XG5cbiAgLyoqXG4gICAqIFVwZGF0ZSBhbGwgdGhlIHRhZ3MgaW5zdGFuY2VzIGNyZWF0ZWRcbiAgICogQHJldHVybnMgeyBBcnJheSB9IGFsbCB0aGUgdGFncyBpbnN0YW5jZXNcbiAgICovXG4gIGZ1bmN0aW9uIHVwZGF0ZSQxKCkge1xuICAgIHJldHVybiBlYWNoKF9fVEFHU19DQUNIRSwgZnVuY3Rpb24gKHRhZykgeyByZXR1cm4gdGFnLnVwZGF0ZSgpOyB9KVxuICB9XG5cbiAgZnVuY3Rpb24gdW5yZWdpc3RlcihuYW1lKSB7XG4gICAgc3R5bGVNYW5hZ2VyLnJlbW92ZShuYW1lKTtcbiAgICByZXR1cm4gZGVsZXRlIF9fVEFHX0lNUExbbmFtZV1cbiAgfVxuXG4gIHZhciB2ZXJzaW9uID0gJ3YzLjExLjEnO1xuXG4gIHZhciBjb3JlID0gLyojX19QVVJFX18qL09iamVjdC5mcmVlemUoe1xuICAgIFRhZzogVGFnLFxuICAgIHRhZzogdGFnLFxuICAgIHRhZzI6IHRhZzIsXG4gICAgbW91bnQ6IG1vdW50LFxuICAgIG1peGluOiBtaXhpbixcbiAgICB1cGRhdGU6IHVwZGF0ZSQxLFxuICAgIHVucmVnaXN0ZXI6IHVucmVnaXN0ZXIsXG4gICAgdmVyc2lvbjogdmVyc2lvblxuICB9KTtcblxuICAvKipcbiAgICogQWRkIGEgbWl4aW4gdG8gdGhpcyB0YWdcbiAgICogQHJldHVybnMgeyBUYWcgfSB0aGUgY3VycmVudCB0YWcgaW5zdGFuY2VcbiAgICovXG4gIGZ1bmN0aW9uIGNvbXBvbmVudE1peGluKHRhZyQkMSkge1xuICAgIHZhciBtaXhpbnMgPSBbXSwgbGVuID0gYXJndW1lbnRzLmxlbmd0aCAtIDE7XG4gICAgd2hpbGUgKCBsZW4tLSA+IDAgKSBtaXhpbnNbIGxlbiBdID0gYXJndW1lbnRzWyBsZW4gKyAxIF07XG5cbiAgICBlYWNoKG1peGlucywgZnVuY3Rpb24gKG1peCkge1xuICAgICAgdmFyIGluc3RhbmNlO1xuICAgICAgdmFyIG9iajtcbiAgICAgIHZhciBwcm9wcyA9IFtdO1xuXG4gICAgICAvLyBwcm9wZXJ0aWVzIGJsYWNrbGlzdGVkIGFuZCB3aWxsIG5vdCBiZSBib3VuZCB0byB0aGUgdGFnIGluc3RhbmNlXG4gICAgICB2YXIgcHJvcHNCbGFja2xpc3QgPSBbJ2luaXQnLCAnX19wcm90b19fJ107XG5cbiAgICAgIG1peCA9IGlzU3RyaW5nKG1peCkgPyBtaXhpbihtaXgpIDogbWl4O1xuXG4gICAgICAvLyBjaGVjayBpZiB0aGUgbWl4aW4gaXMgYSBmdW5jdGlvblxuICAgICAgaWYgKGlzRnVuY3Rpb24obWl4KSkge1xuICAgICAgICAvLyBjcmVhdGUgdGhlIG5ldyBtaXhpbiBpbnN0YW5jZVxuICAgICAgICBpbnN0YW5jZSA9IG5ldyBtaXgoKTtcbiAgICAgIH0gZWxzZSB7IGluc3RhbmNlID0gbWl4OyB9XG5cbiAgICAgIHZhciBwcm90byA9IE9iamVjdC5nZXRQcm90b3R5cGVPZihpbnN0YW5jZSk7XG5cbiAgICAgIC8vIGJ1aWxkIG11bHRpbGV2ZWwgcHJvdG90eXBlIGluaGVyaXRhbmNlIGNoYWluIHByb3BlcnR5IGxpc3RcbiAgICAgIGRvIHsgcHJvcHMgPSBwcm9wcy5jb25jYXQoT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXMob2JqIHx8IGluc3RhbmNlKSk7IH1cbiAgICAgIHdoaWxlIChvYmogPSBPYmplY3QuZ2V0UHJvdG90eXBlT2Yob2JqIHx8IGluc3RhbmNlKSlcblxuICAgICAgLy8gbG9vcCB0aGUga2V5cyBpbiB0aGUgZnVuY3Rpb24gcHJvdG90eXBlIG9yIHRoZSBhbGwgb2JqZWN0IGtleXNcbiAgICAgIGVhY2gocHJvcHMsIGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgLy8gYmluZCBtZXRob2RzIHRvIHRhZ1xuICAgICAgICAvLyBhbGxvdyBtaXhpbnMgdG8gb3ZlcnJpZGUgb3RoZXIgcHJvcGVydGllcy9wYXJlbnQgbWl4aW5zXG4gICAgICAgIGlmICghY29udGFpbnMocHJvcHNCbGFja2xpc3QsIGtleSkpIHtcbiAgICAgICAgICAvLyBjaGVjayBmb3IgZ2V0dGVycy9zZXR0ZXJzXG4gICAgICAgICAgdmFyIGRlc2NyaXB0b3IgPSBnZXRQcm9wRGVzY3JpcHRvcihpbnN0YW5jZSwga2V5KSB8fCBnZXRQcm9wRGVzY3JpcHRvcihwcm90bywga2V5KTtcbiAgICAgICAgICB2YXIgaGFzR2V0dGVyU2V0dGVyID0gZGVzY3JpcHRvciAmJiAoZGVzY3JpcHRvci5nZXQgfHwgZGVzY3JpcHRvci5zZXQpO1xuXG4gICAgICAgICAgLy8gYXBwbHkgbWV0aG9kIG9ubHkgaWYgaXQgZG9lcyBub3QgYWxyZWFkeSBleGlzdCBvbiB0aGUgaW5zdGFuY2VcbiAgICAgICAgICBpZiAoIXRhZyQkMS5oYXNPd25Qcm9wZXJ0eShrZXkpICYmIGhhc0dldHRlclNldHRlcikge1xuICAgICAgICAgICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhZyQkMSwga2V5LCBkZXNjcmlwdG9yKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGFnJCQxW2tleV0gPSBpc0Z1bmN0aW9uKGluc3RhbmNlW2tleV0pID9cbiAgICAgICAgICAgICAgaW5zdGFuY2Vba2V5XS5iaW5kKHRhZyQkMSkgOlxuICAgICAgICAgICAgICBpbnN0YW5jZVtrZXldO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSk7XG5cbiAgICAgIC8vIGluaXQgbWV0aG9kIHdpbGwgYmUgY2FsbGVkIGF1dG9tYXRpY2FsbHlcbiAgICAgIGlmIChpbnN0YW5jZS5pbml0KVxuICAgICAgICB7IGluc3RhbmNlLmluaXQuYmluZCh0YWckJDEpKHRhZyQkMS5vcHRzKTsgfVxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRhZyQkMVxuICB9XG5cbiAgLyoqXG4gICAqIE1vdmUgdGhlIHBvc2l0aW9uIG9mIGEgY3VzdG9tIHRhZyBpbiBpdHMgcGFyZW50IHRhZ1xuICAgKiBAdGhpcyBUYWdcbiAgICogQHBhcmFtICAgeyBTdHJpbmcgfSB0YWdOYW1lIC0ga2V5IHdoZXJlIHRoZSB0YWcgd2FzIHN0b3JlZFxuICAgKiBAcGFyYW0gICB7IE51bWJlciB9IG5ld1BvcyAtIGluZGV4IHdoZXJlIHRoZSBuZXcgdGFnIHdpbGwgYmUgc3RvcmVkXG4gICAqL1xuICBmdW5jdGlvbiBtb3ZlQ2hpbGQodGFnTmFtZSwgbmV3UG9zKSB7XG4gICAgdmFyIHBhcmVudCA9IHRoaXMucGFyZW50O1xuICAgIHZhciB0YWdzO1xuICAgIC8vIG5vIHBhcmVudCBubyBtb3ZlXG4gICAgaWYgKCFwYXJlbnQpIHsgcmV0dXJuIH1cblxuICAgIHRhZ3MgPSBwYXJlbnQudGFnc1t0YWdOYW1lXTtcblxuICAgIGlmIChpc0FycmF5KHRhZ3MpKVxuICAgICAgeyB0YWdzLnNwbGljZShuZXdQb3MsIDAsIHRhZ3Muc3BsaWNlKHRhZ3MuaW5kZXhPZih0aGlzKSwgMSlbMF0pOyB9XG4gICAgZWxzZSB7IGFycmF5aXNoQWRkKHBhcmVudC50YWdzLCB0YWdOYW1lLCB0aGlzKTsgfVxuICB9XG5cbiAgLyoqXG4gICAqIE1vdmUgdmlydHVhbCB0YWcgYW5kIGFsbCBjaGlsZCBub2Rlc1xuICAgKiBAdGhpcyBUYWdcbiAgICogQHBhcmFtIHsgTm9kZSB9IHNyYyAgLSB0aGUgbm9kZSB0aGF0IHdpbGwgZG8gdGhlIGluc2VydGluZ1xuICAgKiBAcGFyYW0geyBUYWcgfSB0YXJnZXQgLSBpbnNlcnQgYmVmb3JlIHRoaXMgdGFnJ3MgZmlyc3QgY2hpbGRcbiAgICovXG4gIGZ1bmN0aW9uIG1vdmVWaXJ0dWFsKHNyYywgdGFyZ2V0KSB7XG4gICAgdmFyIHRoaXMkMSA9IHRoaXM7XG5cbiAgICB2YXIgZWwgPSB0aGlzLl9fLmhlYWQ7XG4gICAgdmFyIHNpYjtcbiAgICB2YXIgZnJhZyA9IGNyZWF0ZUZyYWdtZW50KCk7XG5cbiAgICB3aGlsZSAoZWwpIHtcbiAgICAgIHNpYiA9IGVsLm5leHRTaWJsaW5nO1xuICAgICAgZnJhZy5hcHBlbmRDaGlsZChlbCk7XG4gICAgICBlbCA9IHNpYjtcbiAgICAgIGlmIChlbCA9PT0gdGhpcyQxLl9fLnRhaWwpIHtcbiAgICAgICAgZnJhZy5hcHBlbmRDaGlsZChlbCk7XG4gICAgICAgIHNyYy5pbnNlcnRCZWZvcmUoZnJhZywgdGFyZ2V0Ll9fLmhlYWQpO1xuICAgICAgICBicmVha1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBDb252ZXJ0IHRoZSBpdGVtIGxvb3BlZCBpbnRvIGFuIG9iamVjdCB1c2VkIHRvIGV4dGVuZCB0aGUgY2hpbGQgdGFnIHByb3BlcnRpZXNcbiAgICogQHBhcmFtICAgeyBPYmplY3QgfSBleHByIC0gb2JqZWN0IGNvbnRhaW5pbmcgdGhlIGtleXMgdXNlZCB0byBleHRlbmQgdGhlIGNoaWxkcmVuIHRhZ3NcbiAgICogQHBhcmFtICAgeyAqIH0ga2V5IC0gdmFsdWUgdG8gYXNzaWduIHRvIHRoZSBuZXcgb2JqZWN0IHJldHVybmVkXG4gICAqIEBwYXJhbSAgIHsgKiB9IHZhbCAtIHZhbHVlIGNvbnRhaW5pbmcgdGhlIHBvc2l0aW9uIG9mIHRoZSBpdGVtIGluIHRoZSBhcnJheVxuICAgKiBAcmV0dXJucyB7IE9iamVjdCB9IC0gbmV3IG9iamVjdCBjb250YWluaW5nIHRoZSB2YWx1ZXMgb2YgdGhlIG9yaWdpbmFsIGl0ZW1cbiAgICpcbiAgICogVGhlIHZhcmlhYmxlcyAna2V5JyBhbmQgJ3ZhbCcgYXJlIGFyYml0cmFyeS5cbiAgICogVGhleSBkZXBlbmQgb24gdGhlIGNvbGxlY3Rpb24gdHlwZSBsb29wZWQgKEFycmF5LCBPYmplY3QpXG4gICAqIGFuZCBvbiB0aGUgZXhwcmVzc2lvbiB1c2VkIG9uIHRoZSBlYWNoIHRhZ1xuICAgKlxuICAgKi9cbiAgZnVuY3Rpb24gbWtpdGVtKGV4cHIsIGtleSwgdmFsKSB7XG4gICAgdmFyIGl0ZW0gPSB7fTtcbiAgICBpdGVtW2V4cHIua2V5XSA9IGtleTtcbiAgICBpZiAoZXhwci5wb3MpIHsgaXRlbVtleHByLnBvc10gPSB2YWw7IH1cbiAgICByZXR1cm4gaXRlbVxuICB9XG5cbiAgLyoqXG4gICAqIFVubW91bnQgdGhlIHJlZHVuZGFudCB0YWdzXG4gICAqIEBwYXJhbSAgIHsgQXJyYXkgfSBpdGVtcyAtIGFycmF5IGNvbnRhaW5pbmcgdGhlIGN1cnJlbnQgaXRlbXMgdG8gbG9vcFxuICAgKiBAcGFyYW0gICB7IEFycmF5IH0gdGFncyAtIGFycmF5IGNvbnRhaW5pbmcgYWxsIHRoZSBjaGlsZHJlbiB0YWdzXG4gICAqL1xuICBmdW5jdGlvbiB1bm1vdW50UmVkdW5kYW50KGl0ZW1zLCB0YWdzLCBmaWx0ZXJlZEl0ZW1zQ291bnQpIHtcbiAgICB2YXIgaSA9IHRhZ3MubGVuZ3RoO1xuICAgIHZhciBqID0gaXRlbXMubGVuZ3RoIC0gZmlsdGVyZWRJdGVtc0NvdW50O1xuXG4gICAgd2hpbGUgKGkgPiBqKSB7XG4gICAgICBpLS07XG4gICAgICByZW1vdmUuYXBwbHkodGFnc1tpXSwgW3RhZ3MsIGldKTtcbiAgICB9XG4gIH1cblxuXG4gIC8qKlxuICAgKiBSZW1vdmUgYSBjaGlsZCB0YWdcbiAgICogQHRoaXMgVGFnXG4gICAqIEBwYXJhbSAgIHsgQXJyYXkgfSB0YWdzIC0gdGFncyBjb2xsZWN0aW9uXG4gICAqIEBwYXJhbSAgIHsgTnVtYmVyIH0gaSAtIGluZGV4IG9mIHRoZSB0YWcgdG8gcmVtb3ZlXG4gICAqL1xuICBmdW5jdGlvbiByZW1vdmUodGFncywgaSkge1xuICAgIHRhZ3Muc3BsaWNlKGksIDEpO1xuICAgIHRoaXMudW5tb3VudCgpO1xuICAgIGFycmF5aXNoUmVtb3ZlKHRoaXMucGFyZW50LCB0aGlzLCB0aGlzLl9fLnRhZ05hbWUsIHRydWUpO1xuICB9XG5cbiAgLyoqXG4gICAqIE1vdmUgdGhlIG5lc3RlZCBjdXN0b20gdGFncyBpbiBub24gY3VzdG9tIGxvb3AgdGFnc1xuICAgKiBAdGhpcyBUYWdcbiAgICogQHBhcmFtICAgeyBOdW1iZXIgfSBpIC0gY3VycmVudCBwb3NpdGlvbiBvZiB0aGUgbG9vcCB0YWdcbiAgICovXG4gIGZ1bmN0aW9uIG1vdmVOZXN0ZWRUYWdzKGkpIHtcbiAgICB2YXIgdGhpcyQxID0gdGhpcztcblxuICAgIGVhY2goT2JqZWN0LmtleXModGhpcy50YWdzKSwgZnVuY3Rpb24gKHRhZ05hbWUpIHtcbiAgICAgIG1vdmVDaGlsZC5hcHBseSh0aGlzJDEudGFnc1t0YWdOYW1lXSwgW3RhZ05hbWUsIGldKTtcbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBNb3ZlIGEgY2hpbGQgdGFnXG4gICAqIEB0aGlzIFRhZ1xuICAgKiBAcGFyYW0gICB7IEhUTUxFbGVtZW50IH0gcm9vdCAtIGRvbSBub2RlIGNvbnRhaW5pbmcgYWxsIHRoZSBsb29wIGNoaWxkcmVuXG4gICAqIEBwYXJhbSAgIHsgVGFnIH0gbmV4dFRhZyAtIGluc3RhbmNlIG9mIHRoZSBuZXh0IHRhZyBwcmVjZWRpbmcgdGhlIG9uZSB3ZSB3YW50IHRvIG1vdmVcbiAgICogQHBhcmFtICAgeyBCb29sZWFuIH0gaXNWaXJ0dWFsIC0gaXMgaXQgYSB2aXJ0dWFsIHRhZz9cbiAgICovXG4gIGZ1bmN0aW9uIG1vdmUocm9vdCwgbmV4dFRhZywgaXNWaXJ0dWFsKSB7XG4gICAgaWYgKGlzVmlydHVhbClcbiAgICAgIHsgbW92ZVZpcnR1YWwuYXBwbHkodGhpcywgW3Jvb3QsIG5leHRUYWddKTsgfVxuICAgIGVsc2VcbiAgICAgIHsgc2FmZUluc2VydChyb290LCB0aGlzLnJvb3QsIG5leHRUYWcucm9vdCk7IH1cbiAgfVxuXG4gIC8qKlxuICAgKiBJbnNlcnQgYW5kIG1vdW50IGEgY2hpbGQgdGFnXG4gICAqIEB0aGlzIFRhZ1xuICAgKiBAcGFyYW0gICB7IEhUTUxFbGVtZW50IH0gcm9vdCAtIGRvbSBub2RlIGNvbnRhaW5pbmcgYWxsIHRoZSBsb29wIGNoaWxkcmVuXG4gICAqIEBwYXJhbSAgIHsgVGFnIH0gbmV4dFRhZyAtIGluc3RhbmNlIG9mIHRoZSBuZXh0IHRhZyBwcmVjZWRpbmcgdGhlIG9uZSB3ZSB3YW50IHRvIGluc2VydFxuICAgKiBAcGFyYW0gICB7IEJvb2xlYW4gfSBpc1ZpcnR1YWwgLSBpcyBpdCBhIHZpcnR1YWwgdGFnP1xuICAgKi9cbiAgZnVuY3Rpb24gaW5zZXJ0KHJvb3QsIG5leHRUYWcsIGlzVmlydHVhbCkge1xuICAgIGlmIChpc1ZpcnR1YWwpXG4gICAgICB7IG1ha2VWaXJ0dWFsLmFwcGx5KHRoaXMsIFtyb290LCBuZXh0VGFnXSk7IH1cbiAgICBlbHNlXG4gICAgICB7IHNhZmVJbnNlcnQocm9vdCwgdGhpcy5yb290LCBuZXh0VGFnLnJvb3QpOyB9XG4gIH1cblxuICAvKipcbiAgICogQXBwZW5kIGEgbmV3IHRhZyBpbnRvIHRoZSBET01cbiAgICogQHRoaXMgVGFnXG4gICAqIEBwYXJhbSAgIHsgSFRNTEVsZW1lbnQgfSByb290IC0gZG9tIG5vZGUgY29udGFpbmluZyBhbGwgdGhlIGxvb3AgY2hpbGRyZW5cbiAgICogQHBhcmFtICAgeyBCb29sZWFuIH0gaXNWaXJ0dWFsIC0gaXMgaXQgYSB2aXJ0dWFsIHRhZz9cbiAgICovXG4gIGZ1bmN0aW9uIGFwcGVuZChyb290LCBpc1ZpcnR1YWwpIHtcbiAgICBpZiAoaXNWaXJ0dWFsKVxuICAgICAgeyBtYWtlVmlydHVhbC5jYWxsKHRoaXMsIHJvb3QpOyB9XG4gICAgZWxzZVxuICAgICAgeyByb290LmFwcGVuZENoaWxkKHRoaXMucm9vdCk7IH1cbiAgfVxuXG4gIC8qKlxuICAgKiBSZXR1cm4gdGhlIHZhbHVlIHdlIHdhbnQgdG8gdXNlIHRvIGxvb2t1cCB0aGUgcG9zdGlvbiBvZiBvdXIgaXRlbXMgaW4gdGhlIGNvbGxlY3Rpb25cbiAgICogQHBhcmFtICAgeyBTdHJpbmcgfSAga2V5QXR0ciAgICAgICAgIC0gbG9va3VwIHN0cmluZyBvciBleHByZXNzaW9uXG4gICAqIEBwYXJhbSAgIHsgKiB9ICAgICAgIG9yaWdpbmFsSXRlbSAgICAtIG9yaWdpbmFsIGl0ZW0gZnJvbSB0aGUgY29sbGVjdGlvblxuICAgKiBAcGFyYW0gICB7IE9iamVjdCB9ICBrZXllZEl0ZW0gICAgICAgLSBvYmplY3QgY3JlYXRlZCBieSByaW90IHZpYSB7IGl0ZW0sIGkgaW4gY29sbGVjdGlvbiB9XG4gICAqIEBwYXJhbSAgIHsgQm9vbGVhbiB9IGhhc0tleUF0dHJFeHByICAtIGZsYWcgdG8gY2hlY2sgd2hldGhlciB0aGUga2V5IGlzIGFuIGV4cHJlc3Npb25cbiAgICogQHJldHVybnMgeyAqIH0gdmFsdWUgdGhhdCB3ZSB3aWxsIHVzZSB0byBmaWd1cmUgb3V0IHRoZSBpdGVtIHBvc2l0aW9uIHZpYSBjb2xsZWN0aW9uLmluZGV4T2ZcbiAgICovXG4gIGZ1bmN0aW9uIGdldEl0ZW1JZChrZXlBdHRyLCBvcmlnaW5hbEl0ZW0sIGtleWVkSXRlbSwgaGFzS2V5QXR0ckV4cHIpIHtcbiAgICBpZiAoa2V5QXR0cikge1xuICAgICAgcmV0dXJuIGhhc0tleUF0dHJFeHByID8gIHRtcGwoa2V5QXR0ciwga2V5ZWRJdGVtKSA6ICBvcmlnaW5hbEl0ZW1ba2V5QXR0cl1cbiAgICB9XG5cbiAgICByZXR1cm4gb3JpZ2luYWxJdGVtXG4gIH1cblxuICAvKipcbiAgICogTWFuYWdlIHRhZ3MgaGF2aW5nIHRoZSAnZWFjaCdcbiAgICogQHBhcmFtICAgeyBIVE1MRWxlbWVudCB9IGRvbSAtIERPTSBub2RlIHdlIG5lZWQgdG8gbG9vcFxuICAgKiBAcGFyYW0gICB7IFRhZyB9IHBhcmVudCAtIHBhcmVudCB0YWcgaW5zdGFuY2Ugd2hlcmUgdGhlIGRvbSBub2RlIGlzIGNvbnRhaW5lZFxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IGV4cHIgLSBzdHJpbmcgY29udGFpbmVkIGluIHRoZSAnZWFjaCcgYXR0cmlidXRlXG4gICAqIEByZXR1cm5zIHsgT2JqZWN0IH0gZXhwcmVzc2lvbiBvYmplY3QgZm9yIHRoaXMgZWFjaCBsb29wXG4gICAqL1xuICBmdW5jdGlvbiBfZWFjaChkb20sIHBhcmVudCwgZXhwcikge1xuICAgIHZhciBtdXN0UmVvcmRlciA9IHR5cGVvZiBnZXRBdHRyaWJ1dGUoZG9tLCBMT09QX05PX1JFT1JERVJfRElSRUNUSVZFKSAhPT0gVF9TVFJJTkcgfHwgcmVtb3ZlQXR0cmlidXRlKGRvbSwgTE9PUF9OT19SRU9SREVSX0RJUkVDVElWRSk7XG4gICAgdmFyIGtleUF0dHIgPSBnZXRBdHRyaWJ1dGUoZG9tLCBLRVlfRElSRUNUSVZFKTtcbiAgICB2YXIgaGFzS2V5QXR0ckV4cHIgPSBrZXlBdHRyID8gdG1wbC5oYXNFeHByKGtleUF0dHIpIDogZmFsc2U7XG4gICAgdmFyIHRhZ05hbWUgPSBnZXROYW1lKGRvbSk7XG4gICAgdmFyIGltcGwgPSBfX1RBR19JTVBMW3RhZ05hbWVdO1xuICAgIHZhciBwYXJlbnROb2RlID0gZG9tLnBhcmVudE5vZGU7XG4gICAgdmFyIHBsYWNlaG9sZGVyID0gY3JlYXRlRE9NUGxhY2Vob2xkZXIoKTtcbiAgICB2YXIgY2hpbGQgPSBnZXQoZG9tKTtcbiAgICB2YXIgaWZFeHByID0gZ2V0QXR0cmlidXRlKGRvbSwgQ09ORElUSU9OQUxfRElSRUNUSVZFKTtcbiAgICB2YXIgdGFncyA9IFtdO1xuICAgIHZhciBpc0xvb3AgPSB0cnVlO1xuICAgIHZhciBpbm5lckhUTUwgPSBkb20uaW5uZXJIVE1MO1xuICAgIHZhciBpc0Fub255bW91cyA9ICFfX1RBR19JTVBMW3RhZ05hbWVdO1xuICAgIHZhciBpc1ZpcnR1YWwgPSBkb20udGFnTmFtZSA9PT0gJ1ZJUlRVQUwnO1xuICAgIHZhciBvbGRJdGVtcyA9IFtdO1xuXG4gICAgLy8gcmVtb3ZlIHRoZSBlYWNoIHByb3BlcnR5IGZyb20gdGhlIG9yaWdpbmFsIHRhZ1xuICAgIHJlbW92ZUF0dHJpYnV0ZShkb20sIExPT1BfRElSRUNUSVZFKTtcbiAgICByZW1vdmVBdHRyaWJ1dGUoZG9tLCBLRVlfRElSRUNUSVZFKTtcblxuICAgIC8vIHBhcnNlIHRoZSBlYWNoIGV4cHJlc3Npb25cbiAgICBleHByID0gdG1wbC5sb29wS2V5cyhleHByKTtcbiAgICBleHByLmlzTG9vcCA9IHRydWU7XG5cbiAgICBpZiAoaWZFeHByKSB7IHJlbW92ZUF0dHJpYnV0ZShkb20sIENPTkRJVElPTkFMX0RJUkVDVElWRSk7IH1cblxuICAgIC8vIGluc2VydCBhIG1hcmtlZCB3aGVyZSB0aGUgbG9vcCB0YWdzIHdpbGwgYmUgaW5qZWN0ZWRcbiAgICBwYXJlbnROb2RlLmluc2VydEJlZm9yZShwbGFjZWhvbGRlciwgZG9tKTtcbiAgICBwYXJlbnROb2RlLnJlbW92ZUNoaWxkKGRvbSk7XG5cbiAgICBleHByLnVwZGF0ZSA9IGZ1bmN0aW9uIHVwZGF0ZUVhY2goKSB7XG4gICAgICAvLyBnZXQgdGhlIG5ldyBpdGVtcyBjb2xsZWN0aW9uXG4gICAgICBleHByLnZhbHVlID0gdG1wbChleHByLnZhbCwgcGFyZW50KTtcblxuICAgICAgdmFyIGl0ZW1zID0gZXhwci52YWx1ZTtcbiAgICAgIHZhciBmcmFnID0gY3JlYXRlRnJhZ21lbnQoKTtcbiAgICAgIHZhciBpc09iamVjdCA9ICFpc0FycmF5KGl0ZW1zKSAmJiAhaXNTdHJpbmcoaXRlbXMpO1xuICAgICAgdmFyIHJvb3QgPSBwbGFjZWhvbGRlci5wYXJlbnROb2RlO1xuICAgICAgdmFyIHRtcEl0ZW1zID0gW107XG4gICAgICB2YXIgaGFzS2V5cyA9IGlzT2JqZWN0ICYmICEhaXRlbXM7XG5cbiAgICAgIC8vIGlmIHRoaXMgRE9NIHdhcyByZW1vdmVkIHRoZSB1cGRhdGUgaGVyZSBpcyB1c2VsZXNzXG4gICAgICAvLyB0aGlzIGNvbmRpdGlvbiBmaXhlcyBhbHNvIGEgd2VpcmQgYXN5bmMgaXNzdWUgb24gSUUgaW4gb3VyIHVuaXQgdGVzdFxuICAgICAgaWYgKCFyb290KSB7IHJldHVybiB9XG5cbiAgICAgIC8vIG9iamVjdCBsb29wLiBhbnkgY2hhbmdlcyBjYXVzZSBmdWxsIHJlZHJhd1xuICAgICAgaWYgKGlzT2JqZWN0KSB7XG4gICAgICAgIGl0ZW1zID0gaXRlbXMgPyBPYmplY3Qua2V5cyhpdGVtcykubWFwKGZ1bmN0aW9uIChrZXkpIHsgcmV0dXJuIG1raXRlbShleHByLCBpdGVtc1trZXldLCBrZXkpOyB9KSA6IFtdO1xuICAgICAgfVxuXG4gICAgICAvLyBzdG9yZSB0aGUgYW1vdW50IG9mIGZpbHRlcmVkIGl0ZW1zXG4gICAgICB2YXIgZmlsdGVyZWRJdGVtc0NvdW50ID0gMDtcblxuICAgICAgLy8gbG9vcCBhbGwgdGhlIG5ldyBpdGVtc1xuICAgICAgZWFjaChpdGVtcywgZnVuY3Rpb24gKF9pdGVtLCBpbmRleCkge1xuICAgICAgICB2YXIgaSA9IGluZGV4IC0gZmlsdGVyZWRJdGVtc0NvdW50O1xuICAgICAgICB2YXIgaXRlbSA9ICFoYXNLZXlzICYmIGV4cHIua2V5ID8gbWtpdGVtKGV4cHIsIF9pdGVtLCBpbmRleCkgOiBfaXRlbTtcblxuICAgICAgICAvLyBza2lwIHRoaXMgaXRlbSBiZWNhdXNlIGl0IG11c3QgYmUgZmlsdGVyZWRcbiAgICAgICAgaWYgKGlmRXhwciAmJiAhdG1wbChpZkV4cHIsIGV4dGVuZChjcmVhdGUocGFyZW50KSwgaXRlbSkpKSB7XG4gICAgICAgICAgZmlsdGVyZWRJdGVtc0NvdW50ICsrO1xuICAgICAgICAgIHJldHVyblxuICAgICAgICB9XG5cbiAgICAgICAgdmFyIGl0ZW1JZCA9IGdldEl0ZW1JZChrZXlBdHRyLCBfaXRlbSwgaXRlbSwgaGFzS2V5QXR0ckV4cHIpO1xuICAgICAgICAvLyByZW9yZGVyIG9ubHkgaWYgdGhlIGl0ZW1zIGFyZSBub3Qgb2JqZWN0c1xuICAgICAgICAvLyBvciBhIGtleSBhdHRyaWJ1dGUgaGFzIGJlZW4gcHJvdmlkZWRcbiAgICAgICAgdmFyIGRvUmVvcmRlciA9ICFpc09iamVjdCAmJiBtdXN0UmVvcmRlciAmJiB0eXBlb2YgX2l0ZW0gPT09IFRfT0JKRUNUIHx8IGtleUF0dHI7XG4gICAgICAgIHZhciBvbGRQb3MgPSBvbGRJdGVtcy5pbmRleE9mKGl0ZW1JZCk7XG4gICAgICAgIHZhciBpc05ldyA9IG9sZFBvcyA9PT0gLTE7XG4gICAgICAgIHZhciBwb3MgPSAhaXNOZXcgJiYgZG9SZW9yZGVyID8gb2xkUG9zIDogaTtcbiAgICAgICAgLy8gZG9lcyBhIHRhZyBleGlzdCBpbiB0aGlzIHBvc2l0aW9uP1xuICAgICAgICB2YXIgdGFnID0gdGFnc1twb3NdO1xuICAgICAgICB2YXIgbXVzdEFwcGVuZCA9IGkgPj0gb2xkSXRlbXMubGVuZ3RoO1xuICAgICAgICB2YXIgbXVzdENyZWF0ZSA9IGRvUmVvcmRlciAmJiBpc05ldyB8fCAhZG9SZW9yZGVyICYmICF0YWcgfHwgIXRhZ3NbaV07XG5cbiAgICAgICAgLy8gbmV3IHRhZ1xuICAgICAgICBpZiAobXVzdENyZWF0ZSkge1xuICAgICAgICAgIHRhZyA9IGNyZWF0ZVRhZyhpbXBsLCB7XG4gICAgICAgICAgICBwYXJlbnQ6IHBhcmVudCxcbiAgICAgICAgICAgIGlzTG9vcDogaXNMb29wLFxuICAgICAgICAgICAgaXNBbm9ueW1vdXM6IGlzQW5vbnltb3VzLFxuICAgICAgICAgICAgdGFnTmFtZTogdGFnTmFtZSxcbiAgICAgICAgICAgIHJvb3Q6IGRvbS5jbG9uZU5vZGUoaXNBbm9ueW1vdXMpLFxuICAgICAgICAgICAgaXRlbTogaXRlbSxcbiAgICAgICAgICAgIGluZGV4OiBpLFxuICAgICAgICAgIH0sIGlubmVySFRNTCk7XG5cbiAgICAgICAgICAvLyBtb3VudCB0aGUgdGFnXG4gICAgICAgICAgdGFnLm1vdW50KCk7XG5cbiAgICAgICAgICBpZiAobXVzdEFwcGVuZClcbiAgICAgICAgICAgIHsgYXBwZW5kLmFwcGx5KHRhZywgW2ZyYWcgfHwgcm9vdCwgaXNWaXJ0dWFsXSk7IH1cbiAgICAgICAgICBlbHNlXG4gICAgICAgICAgICB7IGluc2VydC5hcHBseSh0YWcsIFtyb290LCB0YWdzW2ldLCBpc1ZpcnR1YWxdKTsgfVxuXG4gICAgICAgICAgaWYgKCFtdXN0QXBwZW5kKSB7IG9sZEl0ZW1zLnNwbGljZShpLCAwLCBpdGVtKTsgfVxuICAgICAgICAgIHRhZ3Muc3BsaWNlKGksIDAsIHRhZyk7XG4gICAgICAgICAgaWYgKGNoaWxkKSB7IGFycmF5aXNoQWRkKHBhcmVudC50YWdzLCB0YWdOYW1lLCB0YWcsIHRydWUpOyB9XG4gICAgICAgIH0gZWxzZSBpZiAocG9zICE9PSBpICYmIGRvUmVvcmRlcikge1xuICAgICAgICAgIC8vIG1vdmVcbiAgICAgICAgICBpZiAoa2V5QXR0ciB8fCBjb250YWlucyhpdGVtcywgb2xkSXRlbXNbcG9zXSkpIHtcbiAgICAgICAgICAgIG1vdmUuYXBwbHkodGFnLCBbcm9vdCwgdGFnc1tpXSwgaXNWaXJ0dWFsXSk7XG4gICAgICAgICAgICAvLyBtb3ZlIHRoZSBvbGQgdGFnIGluc3RhbmNlXG4gICAgICAgICAgICB0YWdzLnNwbGljZShpLCAwLCB0YWdzLnNwbGljZShwb3MsIDEpWzBdKTtcbiAgICAgICAgICAgIC8vIG1vdmUgdGhlIG9sZCBpdGVtXG4gICAgICAgICAgICBvbGRJdGVtcy5zcGxpY2UoaSwgMCwgb2xkSXRlbXMuc3BsaWNlKHBvcywgMSlbMF0pO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIC8vIHVwZGF0ZSB0aGUgcG9zaXRpb24gYXR0cmlidXRlIGlmIGl0IGV4aXN0c1xuICAgICAgICAgIGlmIChleHByLnBvcykgeyB0YWdbZXhwci5wb3NdID0gaTsgfVxuXG4gICAgICAgICAgLy8gaWYgdGhlIGxvb3AgdGFncyBhcmUgbm90IGN1c3RvbVxuICAgICAgICAgIC8vIHdlIG5lZWQgdG8gbW92ZSBhbGwgdGhlaXIgY3VzdG9tIHRhZ3MgaW50byB0aGUgcmlnaHQgcG9zaXRpb25cbiAgICAgICAgICBpZiAoIWNoaWxkICYmIHRhZy50YWdzKSB7IG1vdmVOZXN0ZWRUYWdzLmNhbGwodGFnLCBpKTsgfVxuICAgICAgICB9XG5cbiAgICAgICAgLy8gY2FjaGUgdGhlIG9yaWdpbmFsIGl0ZW0gdG8gdXNlIGl0IGluIHRoZSBldmVudHMgYm91bmQgdG8gdGhpcyBub2RlXG4gICAgICAgIC8vIGFuZCBpdHMgY2hpbGRyZW5cbiAgICAgICAgZXh0ZW5kKHRhZy5fXywge1xuICAgICAgICAgIGl0ZW06IGl0ZW0sXG4gICAgICAgICAgaW5kZXg6IGksXG4gICAgICAgICAgcGFyZW50OiBwYXJlbnRcbiAgICAgICAgfSk7XG5cbiAgICAgICAgdG1wSXRlbXNbaV0gPSBpdGVtSWQ7XG5cbiAgICAgICAgaWYgKCFtdXN0Q3JlYXRlKSB7IHRhZy51cGRhdGUoaXRlbSk7IH1cbiAgICAgIH0pO1xuXG4gICAgICAvLyByZW1vdmUgdGhlIHJlZHVuZGFudCB0YWdzXG4gICAgICB1bm1vdW50UmVkdW5kYW50KGl0ZW1zLCB0YWdzLCBmaWx0ZXJlZEl0ZW1zQ291bnQpO1xuXG4gICAgICAvLyBjbG9uZSB0aGUgaXRlbXMgYXJyYXlcbiAgICAgIG9sZEl0ZW1zID0gdG1wSXRlbXMuc2xpY2UoKTtcblxuICAgICAgcm9vdC5pbnNlcnRCZWZvcmUoZnJhZywgcGxhY2Vob2xkZXIpO1xuICAgIH07XG5cbiAgICBleHByLnVubW91bnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICBlYWNoKHRhZ3MsIGZ1bmN0aW9uICh0KSB7IHQudW5tb3VudCgpOyB9KTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIGV4cHJcbiAgfVxuXG4gIHZhciBSZWZFeHByID0ge1xuICAgIGluaXQ6IGZ1bmN0aW9uIGluaXQoZG9tLCBwYXJlbnQsIGF0dHJOYW1lLCBhdHRyVmFsdWUpIHtcbiAgICAgIHRoaXMuZG9tID0gZG9tO1xuICAgICAgdGhpcy5hdHRyID0gYXR0ck5hbWU7XG4gICAgICB0aGlzLnJhd1ZhbHVlID0gYXR0clZhbHVlO1xuICAgICAgdGhpcy5wYXJlbnQgPSBwYXJlbnQ7XG4gICAgICB0aGlzLmhhc0V4cCA9IHRtcGwuaGFzRXhwcihhdHRyVmFsdWUpO1xuICAgICAgcmV0dXJuIHRoaXNcbiAgICB9LFxuICAgIHVwZGF0ZTogZnVuY3Rpb24gdXBkYXRlKCkge1xuICAgICAgdmFyIG9sZCA9IHRoaXMudmFsdWU7XG4gICAgICB2YXIgY3VzdG9tUGFyZW50ID0gdGhpcy5wYXJlbnQgJiYgZ2V0SW1tZWRpYXRlQ3VzdG9tUGFyZW50KHRoaXMucGFyZW50KTtcbiAgICAgIC8vIGlmIHRoZSByZWZlcmVuY2VkIGVsZW1lbnQgaXMgYSBjdXN0b20gdGFnLCB0aGVuIHdlIHNldCB0aGUgdGFnIGl0c2VsZiwgcmF0aGVyIHRoYW4gRE9NXG4gICAgICB2YXIgdGFnT3JEb20gPSB0aGlzLmRvbS5fX3JlZiB8fCB0aGlzLnRhZyB8fCB0aGlzLmRvbTtcblxuICAgICAgdGhpcy52YWx1ZSA9IHRoaXMuaGFzRXhwID8gdG1wbCh0aGlzLnJhd1ZhbHVlLCB0aGlzLnBhcmVudCkgOiB0aGlzLnJhd1ZhbHVlO1xuXG4gICAgICAvLyB0aGUgbmFtZSBjaGFuZ2VkLCBzbyB3ZSBuZWVkIHRvIHJlbW92ZSBpdCBmcm9tIHRoZSBvbGQga2V5IChpZiBwcmVzZW50KVxuICAgICAgaWYgKCFpc0JsYW5rKG9sZCkgJiYgY3VzdG9tUGFyZW50KSB7IGFycmF5aXNoUmVtb3ZlKGN1c3RvbVBhcmVudC5yZWZzLCBvbGQsIHRhZ09yRG9tKTsgfVxuICAgICAgaWYgKCFpc0JsYW5rKHRoaXMudmFsdWUpICYmIGlzU3RyaW5nKHRoaXMudmFsdWUpKSB7XG4gICAgICAgIC8vIGFkZCBpdCB0byB0aGUgcmVmcyBvZiBwYXJlbnQgdGFnICh0aGlzIGJlaGF2aW9yIHdhcyBjaGFuZ2VkID49My4wKVxuICAgICAgICBpZiAoY3VzdG9tUGFyZW50KSB7IGFycmF5aXNoQWRkKFxuICAgICAgICAgIGN1c3RvbVBhcmVudC5yZWZzLFxuICAgICAgICAgIHRoaXMudmFsdWUsXG4gICAgICAgICAgdGFnT3JEb20sXG4gICAgICAgICAgLy8gdXNlIGFuIGFycmF5IGlmIGl0J3MgYSBsb29wZWQgbm9kZSBhbmQgdGhlIHJlZiBpcyBub3QgYW4gZXhwcmVzc2lvblxuICAgICAgICAgIG51bGwsXG4gICAgICAgICAgdGhpcy5wYXJlbnQuX18uaW5kZXhcbiAgICAgICAgKTsgfVxuXG4gICAgICAgIGlmICh0aGlzLnZhbHVlICE9PSBvbGQpIHtcbiAgICAgICAgICBzZXRBdHRyaWJ1dGUodGhpcy5kb20sIHRoaXMuYXR0ciwgdGhpcy52YWx1ZSk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJlbW92ZUF0dHJpYnV0ZSh0aGlzLmRvbSwgdGhpcy5hdHRyKTtcbiAgICAgIH1cblxuICAgICAgLy8gY2FjaGUgdGhlIHJlZiBib3VuZCB0byB0aGlzIGRvbSBub2RlXG4gICAgICAvLyB0byByZXVzZSBpdCBpbiBmdXR1cmUgKHNlZSBhbHNvICMyMzI5KVxuICAgICAgaWYgKCF0aGlzLmRvbS5fX3JlZikgeyB0aGlzLmRvbS5fX3JlZiA9IHRhZ09yRG9tOyB9XG4gICAgfSxcbiAgICB1bm1vdW50OiBmdW5jdGlvbiB1bm1vdW50KCkge1xuICAgICAgdmFyIHRhZ09yRG9tID0gdGhpcy50YWcgfHwgdGhpcy5kb207XG4gICAgICB2YXIgY3VzdG9tUGFyZW50ID0gdGhpcy5wYXJlbnQgJiYgZ2V0SW1tZWRpYXRlQ3VzdG9tUGFyZW50KHRoaXMucGFyZW50KTtcbiAgICAgIGlmICghaXNCbGFuayh0aGlzLnZhbHVlKSAmJiBjdXN0b21QYXJlbnQpXG4gICAgICAgIHsgYXJyYXlpc2hSZW1vdmUoY3VzdG9tUGFyZW50LnJlZnMsIHRoaXMudmFsdWUsIHRhZ09yRG9tKTsgfVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBDcmVhdGUgYSBuZXcgcmVmIGRpcmVjdGl2ZVxuICAgKiBAcGFyYW0gICB7IEhUTUxFbGVtZW50IH0gZG9tIC0gZG9tIG5vZGUgaGF2aW5nIHRoZSByZWYgYXR0cmlidXRlXG4gICAqIEBwYXJhbSAgIHsgVGFnIH0gY29udGV4dCAtIHRhZyBpbnN0YW5jZSB3aGVyZSB0aGUgRE9NIG5vZGUgaXMgbG9jYXRlZFxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IGF0dHJOYW1lIC0gZWl0aGVyICdyZWYnIG9yICdkYXRhLXJlZidcbiAgICogQHBhcmFtICAgeyBTdHJpbmcgfSBhdHRyVmFsdWUgLSB2YWx1ZSBvZiB0aGUgcmVmIGF0dHJpYnV0ZVxuICAgKiBAcmV0dXJucyB7IFJlZkV4cHIgfSBhIG5ldyBSZWZFeHByIG9iamVjdFxuICAgKi9cbiAgZnVuY3Rpb24gY3JlYXRlUmVmRGlyZWN0aXZlKGRvbSwgdGFnLCBhdHRyTmFtZSwgYXR0clZhbHVlKSB7XG4gICAgcmV0dXJuIGNyZWF0ZShSZWZFeHByKS5pbml0KGRvbSwgdGFnLCBhdHRyTmFtZSwgYXR0clZhbHVlKVxuICB9XG5cbiAgLyoqXG4gICAqIFRyaWdnZXIgdGhlIHVubW91bnQgbWV0aG9kIG9uIGFsbCB0aGUgZXhwcmVzc2lvbnNcbiAgICogQHBhcmFtICAgeyBBcnJheSB9IGV4cHJlc3Npb25zIC0gRE9NIGV4cHJlc3Npb25zXG4gICAqL1xuICBmdW5jdGlvbiB1bm1vdW50QWxsKGV4cHJlc3Npb25zKSB7XG4gICAgZWFjaChleHByZXNzaW9ucywgZnVuY3Rpb24gKGV4cHIpIHtcbiAgICAgIGlmIChleHByLnVubW91bnQpIHsgZXhwci51bm1vdW50KHRydWUpOyB9XG4gICAgICBlbHNlIGlmIChleHByLnRhZ05hbWUpIHsgZXhwci50YWcudW5tb3VudCh0cnVlKTsgfVxuICAgICAgZWxzZSBpZiAoZXhwci51bm1vdW50KSB7IGV4cHIudW5tb3VudCgpOyB9XG4gICAgfSk7XG4gIH1cblxuICB2YXIgSWZFeHByID0ge1xuICAgIGluaXQ6IGZ1bmN0aW9uIGluaXQoZG9tLCB0YWcsIGV4cHIpIHtcbiAgICAgIHJlbW92ZUF0dHJpYnV0ZShkb20sIENPTkRJVElPTkFMX0RJUkVDVElWRSk7XG4gICAgICBleHRlbmQodGhpcywgeyB0YWc6IHRhZywgZXhwcjogZXhwciwgc3R1YjogY3JlYXRlRE9NUGxhY2Vob2xkZXIoKSwgcHJpc3RpbmU6IGRvbSB9KTtcbiAgICAgIHZhciBwID0gZG9tLnBhcmVudE5vZGU7XG4gICAgICBwLmluc2VydEJlZm9yZSh0aGlzLnN0dWIsIGRvbSk7XG4gICAgICBwLnJlbW92ZUNoaWxkKGRvbSk7XG5cbiAgICAgIHJldHVybiB0aGlzXG4gICAgfSxcbiAgICB1cGRhdGU6IGZ1bmN0aW9uIHVwZGF0ZSQkMSgpIHtcbiAgICAgIHRoaXMudmFsdWUgPSB0bXBsKHRoaXMuZXhwciwgdGhpcy50YWcpO1xuXG4gICAgICBpZiAoIXRoaXMuc3R1Yi5wYXJlbnROb2RlKSB7IHJldHVybiB9XG5cbiAgICAgIGlmICh0aGlzLnZhbHVlICYmICF0aGlzLmN1cnJlbnQpIHsgLy8gaW5zZXJ0XG4gICAgICAgIHRoaXMuY3VycmVudCA9IHRoaXMucHJpc3RpbmUuY2xvbmVOb2RlKHRydWUpO1xuICAgICAgICB0aGlzLnN0dWIucGFyZW50Tm9kZS5pbnNlcnRCZWZvcmUodGhpcy5jdXJyZW50LCB0aGlzLnN0dWIpO1xuICAgICAgICB0aGlzLmV4cHJlc3Npb25zID0gcGFyc2VFeHByZXNzaW9ucy5hcHBseSh0aGlzLnRhZywgW3RoaXMuY3VycmVudCwgdHJ1ZV0pO1xuICAgICAgfSBlbHNlIGlmICghdGhpcy52YWx1ZSAmJiB0aGlzLmN1cnJlbnQpIHsgLy8gcmVtb3ZlXG4gICAgICAgIHRoaXMudW5tb3VudCgpO1xuICAgICAgICB0aGlzLmN1cnJlbnQgPSBudWxsO1xuICAgICAgICB0aGlzLmV4cHJlc3Npb25zID0gW107XG4gICAgICB9XG5cbiAgICAgIGlmICh0aGlzLnZhbHVlKSB7IHVwZGF0ZS5jYWxsKHRoaXMudGFnLCB0aGlzLmV4cHJlc3Npb25zKTsgfVxuICAgIH0sXG4gICAgdW5tb3VudDogZnVuY3Rpb24gdW5tb3VudCgpIHtcbiAgICAgIGlmICh0aGlzLmN1cnJlbnQpIHtcbiAgICAgICAgaWYgKHRoaXMuY3VycmVudC5fdGFnKSB7XG4gICAgICAgICAgdGhpcy5jdXJyZW50Ll90YWcudW5tb3VudCgpO1xuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuY3VycmVudC5wYXJlbnROb2RlKSB7XG4gICAgICAgICAgdGhpcy5jdXJyZW50LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodGhpcy5jdXJyZW50KTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICB1bm1vdW50QWxsKHRoaXMuZXhwcmVzc2lvbnMgfHwgW10pO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBDcmVhdGUgYSBuZXcgaWYgZGlyZWN0aXZlXG4gICAqIEBwYXJhbSAgIHsgSFRNTEVsZW1lbnQgfSBkb20gLSBpZiByb290IGRvbSBub2RlXG4gICAqIEBwYXJhbSAgIHsgVGFnIH0gY29udGV4dCAtIHRhZyBpbnN0YW5jZSB3aGVyZSB0aGUgRE9NIG5vZGUgaXMgbG9jYXRlZFxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IGF0dHIgLSBpZiBleHByZXNzaW9uXG4gICAqIEByZXR1cm5zIHsgSUZFeHByIH0gYSBuZXcgSWZFeHByIG9iamVjdFxuICAgKi9cbiAgZnVuY3Rpb24gY3JlYXRlSWZEaXJlY3RpdmUoZG9tLCB0YWcsIGF0dHIpIHtcbiAgICByZXR1cm4gY3JlYXRlKElmRXhwcikuaW5pdChkb20sIHRhZywgYXR0cilcbiAgfVxuXG4gIC8qKlxuICAgKiBXYWxrIHRoZSB0YWcgRE9NIHRvIGRldGVjdCB0aGUgZXhwcmVzc2lvbnMgdG8gZXZhbHVhdGVcbiAgICogQHRoaXMgVGFnXG4gICAqIEBwYXJhbSAgIHsgSFRNTEVsZW1lbnQgfSByb290IC0gcm9vdCB0YWcgd2hlcmUgd2Ugd2lsbCBzdGFydCBkaWdnaW5nIHRoZSBleHByZXNzaW9uc1xuICAgKiBAcGFyYW0gICB7IEJvb2xlYW4gfSBtdXN0SW5jbHVkZVJvb3QgLSBmbGFnIHRvIGRlY2lkZSB3aGV0aGVyIHRoZSByb290IG11c3QgYmUgcGFyc2VkIGFzIHdlbGxcbiAgICogQHJldHVybnMgeyBBcnJheSB9IGFsbCB0aGUgZXhwcmVzc2lvbnMgZm91bmRcbiAgICovXG4gIGZ1bmN0aW9uIHBhcnNlRXhwcmVzc2lvbnMocm9vdCwgbXVzdEluY2x1ZGVSb290KSB7XG4gICAgdmFyIHRoaXMkMSA9IHRoaXM7XG5cbiAgICB2YXIgZXhwcmVzc2lvbnMgPSBbXTtcblxuICAgIHdhbGtOb2Rlcyhyb290LCBmdW5jdGlvbiAoZG9tKSB7XG4gICAgICB2YXIgdHlwZSA9IGRvbS5ub2RlVHlwZTtcbiAgICAgIHZhciBhdHRyO1xuICAgICAgdmFyIHRhZ0ltcGw7XG5cbiAgICAgIGlmICghbXVzdEluY2x1ZGVSb290ICYmIGRvbSA9PT0gcm9vdCkgeyByZXR1cm4gfVxuXG4gICAgICAvLyB0ZXh0IG5vZGVcbiAgICAgIGlmICh0eXBlID09PSAzICYmIGRvbS5wYXJlbnROb2RlLnRhZ05hbWUgIT09ICdTVFlMRScgJiYgdG1wbC5oYXNFeHByKGRvbS5ub2RlVmFsdWUpKVxuICAgICAgICB7IGV4cHJlc3Npb25zLnB1c2goe2RvbTogZG9tLCBleHByOiBkb20ubm9kZVZhbHVlfSk7IH1cblxuICAgICAgaWYgKHR5cGUgIT09IDEpIHsgcmV0dXJuIH1cblxuICAgICAgdmFyIGlzVmlydHVhbCA9IGRvbS50YWdOYW1lID09PSAnVklSVFVBTCc7XG5cbiAgICAgIC8vIGxvb3AuIGVhY2ggZG9lcyBpdCdzIG93biB0aGluZyAoZm9yIG5vdylcbiAgICAgIGlmIChhdHRyID0gZ2V0QXR0cmlidXRlKGRvbSwgTE9PUF9ESVJFQ1RJVkUpKSB7XG4gICAgICAgIGlmKGlzVmlydHVhbCkgeyBzZXRBdHRyaWJ1dGUoZG9tLCAnbG9vcFZpcnR1YWwnLCB0cnVlKTsgfSAvLyBpZ25vcmUgaGVyZSwgaGFuZGxlZCBpbiBfZWFjaFxuICAgICAgICBleHByZXNzaW9ucy5wdXNoKF9lYWNoKGRvbSwgdGhpcyQxLCBhdHRyKSk7XG4gICAgICAgIHJldHVybiBmYWxzZVxuICAgICAgfVxuXG4gICAgICAvLyBpZi1hdHRycyBiZWNvbWUgdGhlIG5ldyBwYXJlbnQuIEFueSBmb2xsb3dpbmcgZXhwcmVzc2lvbnMgKGVpdGhlciBvbiB0aGUgY3VycmVudFxuICAgICAgLy8gZWxlbWVudCwgb3IgYmVsb3cgaXQpIGJlY29tZSBjaGlsZHJlbiBvZiB0aGlzIGV4cHJlc3Npb24uXG4gICAgICBpZiAoYXR0ciA9IGdldEF0dHJpYnV0ZShkb20sIENPTkRJVElPTkFMX0RJUkVDVElWRSkpIHtcbiAgICAgICAgZXhwcmVzc2lvbnMucHVzaChjcmVhdGVJZkRpcmVjdGl2ZShkb20sIHRoaXMkMSwgYXR0cikpO1xuICAgICAgICByZXR1cm4gZmFsc2VcbiAgICAgIH1cblxuICAgICAgaWYgKGF0dHIgPSBnZXRBdHRyaWJ1dGUoZG9tLCBJU19ESVJFQ1RJVkUpKSB7XG4gICAgICAgIGlmICh0bXBsLmhhc0V4cHIoYXR0cikpIHtcbiAgICAgICAgICBleHByZXNzaW9ucy5wdXNoKHtcbiAgICAgICAgICAgIGlzUnRhZzogdHJ1ZSxcbiAgICAgICAgICAgIGV4cHI6IGF0dHIsXG4gICAgICAgICAgICBkb206IGRvbSxcbiAgICAgICAgICAgIGF0dHJzOiBbXS5zbGljZS5jYWxsKGRvbS5hdHRyaWJ1dGVzKVxuICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgcmV0dXJuIGZhbHNlXG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgLy8gaWYgdGhpcyBpcyBhIHRhZywgc3RvcCB0cmF2ZXJzaW5nIGhlcmUuXG4gICAgICAvLyB3ZSBpZ25vcmUgdGhlIHJvb3QsIHNpbmNlIHBhcnNlRXhwcmVzc2lvbnMgaXMgY2FsbGVkIHdoaWxlIHdlJ3JlIG1vdW50aW5nIHRoYXQgcm9vdFxuICAgICAgdGFnSW1wbCA9IGdldChkb20pO1xuXG4gICAgICBpZihpc1ZpcnR1YWwpIHtcbiAgICAgICAgaWYoZ2V0QXR0cmlidXRlKGRvbSwgJ3ZpcnR1YWxpemVkJykpIHtkb20ucGFyZW50RWxlbWVudC5yZW1vdmVDaGlsZChkb20pOyB9IC8vIHRhZyBjcmVhdGVkLCByZW1vdmUgZnJvbSBkb21cbiAgICAgICAgaWYoIXRhZ0ltcGwgJiYgIWdldEF0dHJpYnV0ZShkb20sICd2aXJ0dWFsaXplZCcpICYmICFnZXRBdHRyaWJ1dGUoZG9tLCAnbG9vcFZpcnR1YWwnKSkgIC8vIG9rIHRvIGNyZWF0ZSB2aXJ0dWFsIHRhZ1xuICAgICAgICAgIHsgdGFnSW1wbCA9IHsgdG1wbDogZG9tLm91dGVySFRNTCB9OyB9XG4gICAgICB9XG5cbiAgICAgIGlmICh0YWdJbXBsICYmIChkb20gIT09IHJvb3QgfHwgbXVzdEluY2x1ZGVSb290KSkge1xuICAgICAgICB2YXIgaGFzSXNEaXJlY3RpdmUgPSBnZXRBdHRyaWJ1dGUoZG9tLCBJU19ESVJFQ1RJVkUpO1xuICAgICAgICBpZihpc1ZpcnR1YWwgJiYgIWhhc0lzRGlyZWN0aXZlKSB7IC8vIGhhbmRsZWQgaW4gdXBkYXRlXG4gICAgICAgICAgLy8gY2FuIG5vdCByZW1vdmUgYXR0cmlidXRlIGxpa2UgZGlyZWN0aXZlc1xuICAgICAgICAgIC8vIHNvIGZsYWcgZm9yIHJlbW92YWwgYWZ0ZXIgY3JlYXRpb24gdG8gcHJldmVudCBtYXhpbXVtIHN0YWNrIGVycm9yXG4gICAgICAgICAgc2V0QXR0cmlidXRlKGRvbSwgJ3ZpcnR1YWxpemVkJywgdHJ1ZSk7XG4gICAgICAgICAgdmFyIHRhZyA9IGNyZWF0ZVRhZyhcbiAgICAgICAgICAgIHt0bXBsOiBkb20ub3V0ZXJIVE1MfSxcbiAgICAgICAgICAgIHtyb290OiBkb20sIHBhcmVudDogdGhpcyQxfSxcbiAgICAgICAgICAgIGRvbS5pbm5lckhUTUxcbiAgICAgICAgICApO1xuXG4gICAgICAgICAgZXhwcmVzc2lvbnMucHVzaCh0YWcpOyAvLyBubyByZXR1cm4sIGFub255bW91cyB0YWcsIGtlZXAgcGFyc2luZ1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGlmIChoYXNJc0RpcmVjdGl2ZSAmJiBpc1ZpcnR1YWwpXG4gICAgICAgICAgICB7IHdhcm4oKFwiVmlydHVhbCB0YWdzIHNob3VsZG4ndCBiZSB1c2VkIHRvZ2V0aGVyIHdpdGggdGhlIFxcXCJcIiArIElTX0RJUkVDVElWRSArIFwiXFxcIiBhdHRyaWJ1dGUgLSBodHRwczovL2dpdGh1Yi5jb20vcmlvdC9yaW90L2lzc3Vlcy8yNTExXCIpKTsgfVxuXG4gICAgICAgICAgZXhwcmVzc2lvbnMucHVzaChcbiAgICAgICAgICAgIGluaXRDaGlsZChcbiAgICAgICAgICAgICAgdGFnSW1wbCxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHJvb3Q6IGRvbSxcbiAgICAgICAgICAgICAgICBwYXJlbnQ6IHRoaXMkMVxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICBkb20uaW5uZXJIVE1MLFxuICAgICAgICAgICAgICB0aGlzJDFcbiAgICAgICAgICAgIClcbiAgICAgICAgICApO1xuICAgICAgICAgIHJldHVybiBmYWxzZVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIC8vIGF0dHJpYnV0ZSBleHByZXNzaW9uc1xuICAgICAgcGFyc2VBdHRyaWJ1dGVzLmFwcGx5KHRoaXMkMSwgW2RvbSwgZG9tLmF0dHJpYnV0ZXMsIGZ1bmN0aW9uIChhdHRyLCBleHByKSB7XG4gICAgICAgIGlmICghZXhwcikgeyByZXR1cm4gfVxuICAgICAgICBleHByZXNzaW9ucy5wdXNoKGV4cHIpO1xuICAgICAgfV0pO1xuICAgIH0pO1xuXG4gICAgcmV0dXJuIGV4cHJlc3Npb25zXG4gIH1cblxuICAvKipcbiAgICogQ2FsbHMgYGZuYCBmb3IgZXZlcnkgYXR0cmlidXRlIG9uIGFuIGVsZW1lbnQuIElmIHRoYXQgYXR0ciBoYXMgYW4gZXhwcmVzc2lvbixcbiAgICogaXQgaXMgYWxzbyBwYXNzZWQgdG8gZm4uXG4gICAqIEB0aGlzIFRhZ1xuICAgKiBAcGFyYW0gICB7IEhUTUxFbGVtZW50IH0gZG9tIC0gZG9tIG5vZGUgdG8gcGFyc2VcbiAgICogQHBhcmFtICAgeyBBcnJheSB9IGF0dHJzIC0gYXJyYXkgb2YgYXR0cmlidXRlc1xuICAgKiBAcGFyYW0gICB7IEZ1bmN0aW9uIH0gZm4gLSBjYWxsYmFjayB0byBleGVjIG9uIGFueSBpdGVyYXRpb25cbiAgICovXG4gIGZ1bmN0aW9uIHBhcnNlQXR0cmlidXRlcyhkb20sIGF0dHJzLCBmbikge1xuICAgIHZhciB0aGlzJDEgPSB0aGlzO1xuXG4gICAgZWFjaChhdHRycywgZnVuY3Rpb24gKGF0dHIpIHtcbiAgICAgIGlmICghYXR0cikgeyByZXR1cm4gZmFsc2UgfVxuXG4gICAgICB2YXIgbmFtZSA9IGF0dHIubmFtZTtcbiAgICAgIHZhciBib29sID0gaXNCb29sQXR0cihuYW1lKTtcbiAgICAgIHZhciBleHByO1xuXG4gICAgICBpZiAoY29udGFpbnMoUkVGX0RJUkVDVElWRVMsIG5hbWUpICYmIGRvbS50YWdOYW1lLnRvTG93ZXJDYXNlKCkgIT09IFlJRUxEX1RBRykge1xuICAgICAgICBleHByID0gIGNyZWF0ZVJlZkRpcmVjdGl2ZShkb20sIHRoaXMkMSwgbmFtZSwgYXR0ci52YWx1ZSk7XG4gICAgICB9IGVsc2UgaWYgKHRtcGwuaGFzRXhwcihhdHRyLnZhbHVlKSkge1xuICAgICAgICBleHByID0ge2RvbTogZG9tLCBleHByOiBhdHRyLnZhbHVlLCBhdHRyOiBuYW1lLCBib29sOiBib29sfTtcbiAgICAgIH1cblxuICAgICAgZm4oYXR0ciwgZXhwcik7XG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogTWFuYWdlIHRoZSBtb3VudCBzdGF0ZSBvZiBhIHRhZyB0cmlnZ2VyaW5nIGFsc28gdGhlIG9ic2VydmFibGUgZXZlbnRzXG4gICAqIEB0aGlzIFRhZ1xuICAgKiBAcGFyYW0geyBCb29sZWFuIH0gdmFsdWUgLSAuLm9mIHRoZSBpc01vdW50ZWQgZmxhZ1xuICAgKi9cbiAgZnVuY3Rpb24gc2V0TW91bnRTdGF0ZSh2YWx1ZSkge1xuICAgIHZhciByZWYgPSB0aGlzLl9fO1xuICAgIHZhciBpc0Fub255bW91cyA9IHJlZi5pc0Fub255bW91cztcblxuICAgIGRlZmluZSh0aGlzLCAnaXNNb3VudGVkJywgdmFsdWUpO1xuXG4gICAgaWYgKCFpc0Fub255bW91cykge1xuICAgICAgaWYgKHZhbHVlKSB7IHRoaXMudHJpZ2dlcignbW91bnQnKTsgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIHRoaXMudHJpZ2dlcigndW5tb3VudCcpO1xuICAgICAgICB0aGlzLm9mZignKicpO1xuICAgICAgICB0aGlzLl9fLndhc0NyZWF0ZWQgPSBmYWxzZTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogTW91bnQgdGhlIGN1cnJlbnQgdGFnIGluc3RhbmNlXG4gICAqIEByZXR1cm5zIHsgVGFnIH0gdGhlIGN1cnJlbnQgdGFnIGluc3RhbmNlXG4gICAqL1xuICBmdW5jdGlvbiBjb21wb25lbnRNb3VudCh0YWckJDEsIGRvbSwgZXhwcmVzc2lvbnMsIG9wdHMpIHtcbiAgICB2YXIgX18gPSB0YWckJDEuX187XG4gICAgdmFyIHJvb3QgPSBfXy5yb290O1xuICAgIHJvb3QuX3RhZyA9IHRhZyQkMTsgLy8ga2VlcCBhIHJlZmVyZW5jZSB0byB0aGUgdGFnIGp1c3QgY3JlYXRlZFxuXG4gICAgLy8gUmVhZCBhbGwgdGhlIGF0dHJzIG9uIHRoaXMgaW5zdGFuY2UuIFRoaXMgZ2l2ZSB1cyB0aGUgaW5mbyB3ZSBuZWVkIGZvciB1cGRhdGVPcHRzXG4gICAgcGFyc2VBdHRyaWJ1dGVzLmFwcGx5KF9fLnBhcmVudCwgW3Jvb3QsIHJvb3QuYXR0cmlidXRlcywgZnVuY3Rpb24gKGF0dHIsIGV4cHIpIHtcbiAgICAgIGlmICghX18uaXNBbm9ueW1vdXMgJiYgUmVmRXhwci5pc1Byb3RvdHlwZU9mKGV4cHIpKSB7IGV4cHIudGFnID0gdGFnJCQxOyB9XG4gICAgICBhdHRyLmV4cHIgPSBleHByO1xuICAgICAgX18uaW5zdEF0dHJzLnB1c2goYXR0cik7XG4gICAgfV0pO1xuXG4gICAgLy8gdXBkYXRlIHRoZSByb290IGFkZGluZyBjdXN0b20gYXR0cmlidXRlcyBjb21pbmcgZnJvbSB0aGUgY29tcGlsZXJcbiAgICB3YWxrQXR0cmlidXRlcyhfXy5pbXBsLmF0dHJzLCBmdW5jdGlvbiAoaywgdikgeyBfXy5pbXBsQXR0cnMucHVzaCh7bmFtZTogaywgdmFsdWU6IHZ9KTsgfSk7XG4gICAgcGFyc2VBdHRyaWJ1dGVzLmFwcGx5KHRhZyQkMSwgW3Jvb3QsIF9fLmltcGxBdHRycywgZnVuY3Rpb24gKGF0dHIsIGV4cHIpIHtcbiAgICAgIGlmIChleHByKSB7IGV4cHJlc3Npb25zLnB1c2goZXhwcik7IH1cbiAgICAgIGVsc2UgeyBzZXRBdHRyaWJ1dGUocm9vdCwgYXR0ci5uYW1lLCBhdHRyLnZhbHVlKTsgfVxuICAgIH1dKTtcblxuICAgIC8vIGluaXRpYWxpYXRpb25cbiAgICB1cGRhdGVPcHRzLmFwcGx5KHRhZyQkMSwgW19fLmlzTG9vcCwgX18ucGFyZW50LCBfXy5pc0Fub255bW91cywgb3B0cywgX18uaW5zdEF0dHJzXSk7XG5cbiAgICAvLyBhZGQgZ2xvYmFsIG1peGluc1xuICAgIHZhciBnbG9iYWxNaXhpbiA9IG1peGluKEdMT0JBTF9NSVhJTik7XG5cbiAgICBpZiAoZ2xvYmFsTWl4aW4gJiYgIV9fLnNraXBBbm9ueW1vdXMpIHtcbiAgICAgIGZvciAodmFyIGkgaW4gZ2xvYmFsTWl4aW4pIHtcbiAgICAgICAgaWYgKGdsb2JhbE1peGluLmhhc093blByb3BlcnR5KGkpKSB7XG4gICAgICAgICAgdGFnJCQxLm1peGluKGdsb2JhbE1peGluW2ldKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChfXy5pbXBsLmZuKSB7IF9fLmltcGwuZm4uY2FsbCh0YWckJDEsIG9wdHMpOyB9XG5cbiAgICBpZiAoIV9fLnNraXBBbm9ueW1vdXMpIHsgdGFnJCQxLnRyaWdnZXIoJ2JlZm9yZS1tb3VudCcpOyB9XG5cbiAgICAvLyBwYXJzZSBsYXlvdXQgYWZ0ZXIgaW5pdC4gZm4gbWF5IGNhbGN1bGF0ZSBhcmdzIGZvciBuZXN0ZWQgY3VzdG9tIHRhZ3NcbiAgICBlYWNoKHBhcnNlRXhwcmVzc2lvbnMuYXBwbHkodGFnJCQxLCBbZG9tLCBfXy5pc0Fub255bW91c10pLCBmdW5jdGlvbiAoZSkgeyByZXR1cm4gZXhwcmVzc2lvbnMucHVzaChlKTsgfSk7XG5cbiAgICB0YWckJDEudXBkYXRlKF9fLml0ZW0pO1xuXG4gICAgaWYgKCFfXy5pc0Fub255bW91cyAmJiAhX18uaXNJbmxpbmUpIHtcbiAgICAgIHdoaWxlIChkb20uZmlyc3RDaGlsZCkgeyByb290LmFwcGVuZENoaWxkKGRvbS5maXJzdENoaWxkKTsgfVxuICAgIH1cblxuICAgIGRlZmluZSh0YWckJDEsICdyb290Jywgcm9vdCk7XG5cbiAgICAvLyBpZiB3ZSBuZWVkIHRvIHdhaXQgdGhhdCB0aGUgcGFyZW50IFwibW91bnRcIiBvciBcInVwZGF0ZWRcIiBldmVudCBnZXRzIHRyaWdnZXJlZFxuICAgIGlmICghX18uc2tpcEFub255bW91cyAmJiB0YWckJDEucGFyZW50KSB7XG4gICAgICB2YXIgcCA9IGdldEltbWVkaWF0ZUN1c3RvbVBhcmVudCh0YWckJDEucGFyZW50KTtcbiAgICAgIHAub25lKCFwLmlzTW91bnRlZCA/ICdtb3VudCcgOiAndXBkYXRlZCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgc2V0TW91bnRTdGF0ZS5jYWxsKHRhZyQkMSwgdHJ1ZSk7XG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gb3RoZXJ3aXNlIGl0J3Mgbm90IGEgY2hpbGQgdGFnIHdlIGNhbiB0cmlnZ2VyIGl0cyBtb3VudCBldmVudFxuICAgICAgc2V0TW91bnRTdGF0ZS5jYWxsKHRhZyQkMSwgdHJ1ZSk7XG4gICAgfVxuXG4gICAgdGFnJCQxLl9fLndhc0NyZWF0ZWQgPSB0cnVlO1xuXG4gICAgcmV0dXJuIHRhZyQkMVxuICB9XG5cbiAgLyoqXG4gICAqIFVubW91bnQgdGhlIHRhZyBpbnN0YW5jZVxuICAgKiBAcGFyYW0geyBCb29sZWFuIH0gbXVzdEtlZXBSb290IC0gaWYgaXQncyB0cnVlIHRoZSByb290IG5vZGUgd2lsbCBub3QgYmUgcmVtb3ZlZFxuICAgKiBAcmV0dXJucyB7IFRhZyB9IHRoZSBjdXJyZW50IHRhZyBpbnN0YW5jZVxuICAgKi9cbiAgZnVuY3Rpb24gdGFnVW5tb3VudCh0YWcsIG11c3RLZWVwUm9vdCwgZXhwcmVzc2lvbnMpIHtcbiAgICB2YXIgX18gPSB0YWcuX187XG4gICAgdmFyIHJvb3QgPSBfXy5yb290O1xuICAgIHZhciB0YWdJbmRleCA9IF9fVEFHU19DQUNIRS5pbmRleE9mKHRhZyk7XG4gICAgdmFyIHAgPSByb290LnBhcmVudE5vZGU7XG5cbiAgICBpZiAoIV9fLnNraXBBbm9ueW1vdXMpIHsgdGFnLnRyaWdnZXIoJ2JlZm9yZS11bm1vdW50Jyk7IH1cblxuICAgIC8vIGNsZWFyIGFsbCBhdHRyaWJ1dGVzIGNvbWluZyBmcm9tIHRoZSBtb3VudGVkIHRhZ1xuICAgIHdhbGtBdHRyaWJ1dGVzKF9fLmltcGwuYXR0cnMsIGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgICBpZiAoc3RhcnRzV2l0aChuYW1lLCBBVFRSU19QUkVGSVgpKVxuICAgICAgICB7IG5hbWUgPSBuYW1lLnNsaWNlKEFUVFJTX1BSRUZJWC5sZW5ndGgpOyB9XG5cbiAgICAgIHJlbW92ZUF0dHJpYnV0ZShyb290LCBuYW1lKTtcbiAgICB9KTtcblxuICAgIC8vIHJlbW92ZSBhbGwgdGhlIGV2ZW50IGxpc3RlbmVyc1xuICAgIHRhZy5fXy5saXN0ZW5lcnMuZm9yRWFjaChmdW5jdGlvbiAoZG9tKSB7XG4gICAgICBPYmplY3Qua2V5cyhkb21bUklPVF9FVkVOVFNfS0VZXSkuZm9yRWFjaChmdW5jdGlvbiAoZXZlbnROYW1lKSB7XG4gICAgICAgIGRvbS5yZW1vdmVFdmVudExpc3RlbmVyKGV2ZW50TmFtZSwgZG9tW1JJT1RfRVZFTlRTX0tFWV1bZXZlbnROYW1lXSk7XG4gICAgICB9KTtcbiAgICB9KTtcblxuICAgIC8vIHJlbW92ZSB0YWcgaW5zdGFuY2UgZnJvbSB0aGUgZ2xvYmFsIHRhZ3MgY2FjaGUgY29sbGVjdGlvblxuICAgIGlmICh0YWdJbmRleCAhPT0gLTEpIHsgX19UQUdTX0NBQ0hFLnNwbGljZSh0YWdJbmRleCwgMSk7IH1cblxuICAgIC8vIGNsZWFuIHVwIHRoZSBwYXJlbnQgdGFncyBvYmplY3RcbiAgICBpZiAoX18ucGFyZW50ICYmICFfXy5pc0Fub255bW91cykge1xuICAgICAgdmFyIHB0YWcgPSBnZXRJbW1lZGlhdGVDdXN0b21QYXJlbnQoX18ucGFyZW50KTtcblxuICAgICAgaWYgKF9fLmlzVmlydHVhbCkge1xuICAgICAgICBPYmplY3RcbiAgICAgICAgICAua2V5cyh0YWcudGFncylcbiAgICAgICAgICAuZm9yRWFjaChmdW5jdGlvbiAodGFnTmFtZSkgeyByZXR1cm4gYXJyYXlpc2hSZW1vdmUocHRhZy50YWdzLCB0YWdOYW1lLCB0YWcudGFnc1t0YWdOYW1lXSk7IH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgYXJyYXlpc2hSZW1vdmUocHRhZy50YWdzLCBfXy50YWdOYW1lLCB0YWcpO1xuICAgICAgfVxuICAgIH1cblxuICAgIC8vIHVubW91bnQgYWxsIHRoZSB2aXJ0dWFsIGRpcmVjdGl2ZXNcbiAgICBpZiAodGFnLl9fLnZpcnRzKSB7XG4gICAgICBlYWNoKHRhZy5fXy52aXJ0cywgZnVuY3Rpb24gKHYpIHtcbiAgICAgICAgaWYgKHYucGFyZW50Tm9kZSkgeyB2LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodik7IH1cbiAgICAgIH0pO1xuICAgIH1cblxuICAgIC8vIGFsbG93IGV4cHJlc3Npb25zIHRvIHVubW91bnQgdGhlbXNlbHZlc1xuICAgIHVubW91bnRBbGwoZXhwcmVzc2lvbnMpO1xuICAgIGVhY2goX18uaW5zdEF0dHJzLCBmdW5jdGlvbiAoYSkgeyByZXR1cm4gYS5leHByICYmIGEuZXhwci51bm1vdW50ICYmIGEuZXhwci51bm1vdW50KCk7IH0pO1xuXG4gICAgLy8gY2xlYXIgdGhlIHRhZyBodG1sIGlmIGl0J3MgbmVjZXNzYXJ5XG4gICAgaWYgKG11c3RLZWVwUm9vdCkgeyBzZXRJbm5lckhUTUwocm9vdCwgJycpOyB9XG4gICAgLy8gb3RoZXJ3aXNlIGRldGFjaCB0aGUgcm9vdCB0YWcgZnJvbSB0aGUgRE9NXG4gICAgZWxzZSBpZiAocCkgeyBwLnJlbW92ZUNoaWxkKHJvb3QpOyB9XG5cbiAgICAvLyBjdXN0b20gaW50ZXJuYWwgdW5tb3VudCBmdW5jdGlvbiB0byBhdm9pZCByZWx5aW5nIG9uIHRoZSBvYnNlcnZhYmxlXG4gICAgaWYgKF9fLm9uVW5tb3VudCkgeyBfXy5vblVubW91bnQoKTsgfVxuXG4gICAgLy8gd2VpcmQgZml4IGZvciBhIHdlaXJkIGVkZ2UgY2FzZSAjMjQwOSBhbmQgIzI0MzZcbiAgICAvLyBzb21lIHVzZXJzIG1pZ2h0IHVzZSB5b3VyIHNvZnR3YXJlIG5vdCBhcyB5b3UndmUgZXhwZWN0ZWRcbiAgICAvLyBzbyBJIG5lZWQgdG8gYWRkIHRoZXNlIGRpcnR5IGhhY2tzIHRvIG1pdGlnYXRlIHVuZXhwZWN0ZWQgaXNzdWVzXG4gICAgaWYgKCF0YWcuaXNNb3VudGVkKSB7IHNldE1vdW50U3RhdGUuY2FsbCh0YWcsIHRydWUpOyB9XG5cbiAgICBzZXRNb3VudFN0YXRlLmNhbGwodGFnLCBmYWxzZSk7XG5cbiAgICBkZWxldGUgcm9vdC5fdGFnO1xuXG4gICAgcmV0dXJuIHRhZ1xuICB9XG5cbiAgLyoqXG4gICAqIFRhZyBjcmVhdGlvbiBmYWN0b3J5IGZ1bmN0aW9uXG4gICAqIEBjb25zdHJ1Y3RvclxuICAgKiBAcGFyYW0geyBPYmplY3QgfSBpbXBsIC0gaXQgY29udGFpbnMgdGhlIHRhZyB0ZW1wbGF0ZSwgYW5kIGxvZ2ljXG4gICAqIEBwYXJhbSB7IE9iamVjdCB9IGNvbmYgLSB0YWcgb3B0aW9uc1xuICAgKiBAcGFyYW0geyBTdHJpbmcgfSBpbm5lckhUTUwgLSBodG1sIHRoYXQgZXZlbnR1YWxseSB3ZSBuZWVkIHRvIGluamVjdCBpbiB0aGUgdGFnXG4gICAqL1xuICBmdW5jdGlvbiBjcmVhdGVUYWcoaW1wbCwgY29uZiwgaW5uZXJIVE1MKSB7XG4gICAgaWYgKCBpbXBsID09PSB2b2lkIDAgKSBpbXBsID0ge307XG4gICAgaWYgKCBjb25mID09PSB2b2lkIDAgKSBjb25mID0ge307XG5cbiAgICB2YXIgdGFnID0gY29uZi5jb250ZXh0IHx8IHt9O1xuICAgIHZhciBvcHRzID0gY29uZi5vcHRzIHx8IHt9O1xuICAgIHZhciBwYXJlbnQgPSBjb25mLnBhcmVudDtcbiAgICB2YXIgaXNMb29wID0gY29uZi5pc0xvb3A7XG4gICAgdmFyIGlzQW5vbnltb3VzID0gISFjb25mLmlzQW5vbnltb3VzO1xuICAgIHZhciBza2lwQW5vbnltb3VzID0gc2V0dGluZ3Muc2tpcEFub255bW91c1RhZ3MgJiYgaXNBbm9ueW1vdXM7XG4gICAgdmFyIGl0ZW0gPSBjb25mLml0ZW07XG4gICAgLy8gYXZhaWxhYmxlIG9ubHkgZm9yIHRoZSBsb29wZWQgbm9kZXNcbiAgICB2YXIgaW5kZXggPSBjb25mLmluZGV4O1xuICAgIC8vIEFsbCBhdHRyaWJ1dGVzIG9uIHRoZSBUYWcgd2hlbiBpdCdzIGZpcnN0IHBhcnNlZFxuICAgIHZhciBpbnN0QXR0cnMgPSBbXTtcbiAgICAvLyBleHByZXNzaW9ucyBvbiB0aGlzIHR5cGUgb2YgVGFnXG4gICAgdmFyIGltcGxBdHRycyA9IFtdO1xuICAgIHZhciB0bXBsID0gaW1wbC50bXBsO1xuICAgIHZhciBleHByZXNzaW9ucyA9IFtdO1xuICAgIHZhciByb290ID0gY29uZi5yb290O1xuICAgIHZhciB0YWdOYW1lID0gY29uZi50YWdOYW1lIHx8IGdldE5hbWUocm9vdCk7XG4gICAgdmFyIGlzVmlydHVhbCA9IHRhZ05hbWUgPT09ICd2aXJ0dWFsJztcbiAgICB2YXIgaXNJbmxpbmUgPSAhaXNWaXJ0dWFsICYmICF0bXBsO1xuICAgIHZhciBkb207XG5cbiAgICBpZiAoaXNJbmxpbmUgfHwgaXNMb29wICYmIGlzQW5vbnltb3VzKSB7XG4gICAgICBkb20gPSByb290O1xuICAgIH0gZWxzZSB7XG4gICAgICBpZiAoIWlzVmlydHVhbCkgeyByb290LmlubmVySFRNTCA9ICcnOyB9XG4gICAgICBkb20gPSBta2RvbSh0bXBsLCBpbm5lckhUTUwsIGlzU3ZnKHJvb3QpKTtcbiAgICB9XG5cbiAgICAvLyBtYWtlIHRoaXMgdGFnIG9ic2VydmFibGVcbiAgICBpZiAoIXNraXBBbm9ueW1vdXMpIHsgb2JzZXJ2YWJsZSh0YWcpOyB9XG5cbiAgICAvLyBvbmx5IGNhbGwgdW5tb3VudCBpZiB3ZSBoYXZlIGEgdmFsaWQgX19UQUdfSU1QTCAoaGFzIG5hbWUgcHJvcGVydHkpXG4gICAgaWYgKGltcGwubmFtZSAmJiByb290Ll90YWcpIHsgcm9vdC5fdGFnLnVubW91bnQodHJ1ZSk7IH1cblxuICAgIGRlZmluZSh0YWcsICdfXycsIHtcbiAgICAgIGltcGw6IGltcGwsXG4gICAgICByb290OiByb290LFxuICAgICAgc2tpcEFub255bW91czogc2tpcEFub255bW91cyxcbiAgICAgIGltcGxBdHRyczogaW1wbEF0dHJzLFxuICAgICAgaXNBbm9ueW1vdXM6IGlzQW5vbnltb3VzLFxuICAgICAgaW5zdEF0dHJzOiBpbnN0QXR0cnMsXG4gICAgICBpbm5lckhUTUw6IGlubmVySFRNTCxcbiAgICAgIHRhZ05hbWU6IHRhZ05hbWUsXG4gICAgICBpbmRleDogaW5kZXgsXG4gICAgICBpc0xvb3A6IGlzTG9vcCxcbiAgICAgIGlzSW5saW5lOiBpc0lubGluZSxcbiAgICAgIGl0ZW06IGl0ZW0sXG4gICAgICBwYXJlbnQ6IHBhcmVudCxcbiAgICAgIC8vIHRhZ3MgaGF2aW5nIGV2ZW50IGxpc3RlbmVyc1xuICAgICAgLy8gaXQgd291bGQgYmUgYmV0dGVyIHRvIHVzZSB3ZWFrIG1hcHMgaGVyZSBidXQgd2UgY2FuIG5vdCBpbnRyb2R1Y2UgYnJlYWtpbmcgY2hhbmdlcyBub3dcbiAgICAgIGxpc3RlbmVyczogW10sXG4gICAgICAvLyB0aGVzZSB2YXJzIHdpbGwgYmUgbmVlZGVkIG9ubHkgZm9yIHRoZSB2aXJ0dWFsIHRhZ3NcbiAgICAgIHZpcnRzOiBbXSxcbiAgICAgIHdhc0NyZWF0ZWQ6IGZhbHNlLFxuICAgICAgdGFpbDogbnVsbCxcbiAgICAgIGhlYWQ6IG51bGxcbiAgICB9KTtcblxuICAgIC8vIHRhZyBwcm90ZWN0ZWQgcHJvcGVydGllc1xuICAgIHJldHVybiBbXG4gICAgICBbJ2lzTW91bnRlZCcsIGZhbHNlXSxcbiAgICAgIC8vIGNyZWF0ZSBhIHVuaXF1ZSBpZCB0byB0aGlzIHRhZ1xuICAgICAgLy8gaXQgY291bGQgYmUgaGFuZHkgdG8gdXNlIGl0IGFsc28gdG8gaW1wcm92ZSB0aGUgdmlydHVhbCBkb20gcmVuZGVyaW5nIHNwZWVkXG4gICAgICBbJ19yaW90X2lkJywgdWlkKCldLFxuICAgICAgWydyb290Jywgcm9vdF0sXG4gICAgICBbJ29wdHMnLCBvcHRzLCB7IHdyaXRhYmxlOiB0cnVlLCBlbnVtZXJhYmxlOiB0cnVlIH1dLFxuICAgICAgWydwYXJlbnQnLCBwYXJlbnQgfHwgbnVsbF0sXG4gICAgICAvLyBwcm90ZWN0IHRoZSBcInRhZ3NcIiBhbmQgXCJyZWZzXCIgcHJvcGVydHkgZnJvbSBiZWluZyBvdmVycmlkZGVuXG4gICAgICBbJ3RhZ3MnLCB7fV0sXG4gICAgICBbJ3JlZnMnLCB7fV0sXG4gICAgICBbJ3VwZGF0ZScsIGZ1bmN0aW9uIChkYXRhKSB7IHJldHVybiBjb21wb25lbnRVcGRhdGUodGFnLCBkYXRhLCBleHByZXNzaW9ucyk7IH1dLFxuICAgICAgWydtaXhpbicsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIG1peGlucyA9IFtdLCBsZW4gPSBhcmd1bWVudHMubGVuZ3RoO1xuICAgICAgICB3aGlsZSAoIGxlbi0tICkgbWl4aW5zWyBsZW4gXSA9IGFyZ3VtZW50c1sgbGVuIF07XG5cbiAgICAgICAgcmV0dXJuIGNvbXBvbmVudE1peGluLmFwcGx5KHZvaWQgMCwgWyB0YWcgXS5jb25jYXQoIG1peGlucyApKTtcbiAgICB9XSxcbiAgICAgIFsnbW91bnQnLCBmdW5jdGlvbiAoKSB7IHJldHVybiBjb21wb25lbnRNb3VudCh0YWcsIGRvbSwgZXhwcmVzc2lvbnMsIG9wdHMpOyB9XSxcbiAgICAgIFsndW5tb3VudCcsIGZ1bmN0aW9uIChtdXN0S2VlcFJvb3QpIHsgcmV0dXJuIHRhZ1VubW91bnQodGFnLCBtdXN0S2VlcFJvb3QsIGV4cHJlc3Npb25zKTsgfV1cbiAgICBdLnJlZHVjZShmdW5jdGlvbiAoYWNjLCByZWYpIHtcbiAgICAgIHZhciBrZXkgPSByZWZbMF07XG4gICAgICB2YXIgdmFsdWUgPSByZWZbMV07XG4gICAgICB2YXIgb3B0cyA9IHJlZlsyXTtcblxuICAgICAgZGVmaW5lKHRhZywga2V5LCB2YWx1ZSwgb3B0cyk7XG4gICAgICByZXR1cm4gYWNjXG4gICAgfSwgZXh0ZW5kKHRhZywgaXRlbSkpXG4gIH1cblxuICAvKipcbiAgICogTW91bnQgYSB0YWcgY3JlYXRpbmcgbmV3IFRhZyBpbnN0YW5jZVxuICAgKiBAcGFyYW0gICB7IE9iamVjdCB9IHJvb3QgLSBkb20gbm9kZSB3aGVyZSB0aGUgdGFnIHdpbGwgYmUgbW91bnRlZFxuICAgKiBAcGFyYW0gICB7IFN0cmluZyB9IHRhZ05hbWUgLSBuYW1lIG9mIHRoZSByaW90IHRhZyB3ZSB3YW50IHRvIG1vdW50XG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gb3B0cyAtIG9wdGlvbnMgdG8gcGFzcyB0byB0aGUgVGFnIGluc3RhbmNlXG4gICAqIEBwYXJhbSAgIHsgT2JqZWN0IH0gY3R4IC0gb3B0aW9uYWwgY29udGV4dCB0aGF0IHdpbGwgYmUgdXNlZCB0byBleHRlbmQgYW4gZXhpc3RpbmcgY2xhc3MgKCB1c2VkIGluIHJpb3QuVGFnIClcbiAgICogQHJldHVybnMgeyBUYWcgfSBhIG5ldyBUYWcgaW5zdGFuY2VcbiAgICovXG4gIGZ1bmN0aW9uIG1vdW50JDEocm9vdCwgdGFnTmFtZSwgb3B0cywgY3R4KSB7XG4gICAgdmFyIGltcGwgPSBfX1RBR19JTVBMW3RhZ05hbWVdO1xuICAgIHZhciBpbXBsQ2xhc3MgPSBfX1RBR19JTVBMW3RhZ05hbWVdLmNsYXNzO1xuICAgIHZhciBjb250ZXh0ID0gY3R4IHx8IChpbXBsQ2xhc3MgPyBjcmVhdGUoaW1wbENsYXNzLnByb3RvdHlwZSkgOiB7fSk7XG4gICAgLy8gY2FjaGUgdGhlIGlubmVyIEhUTUwgdG8gZml4ICM4NTVcbiAgICB2YXIgaW5uZXJIVE1MID0gcm9vdC5faW5uZXJIVE1MID0gcm9vdC5faW5uZXJIVE1MIHx8IHJvb3QuaW5uZXJIVE1MO1xuICAgIHZhciBjb25mID0gZXh0ZW5kKHsgcm9vdDogcm9vdCwgb3B0czogb3B0cywgY29udGV4dDogY29udGV4dCB9LCB7IHBhcmVudDogb3B0cyA/IG9wdHMucGFyZW50IDogbnVsbCB9KTtcbiAgICB2YXIgdGFnO1xuXG4gICAgaWYgKGltcGwgJiYgcm9vdCkgeyB0YWcgPSBjcmVhdGVUYWcoaW1wbCwgY29uZiwgaW5uZXJIVE1MKTsgfVxuXG4gICAgaWYgKHRhZyAmJiB0YWcubW91bnQpIHtcbiAgICAgIHRhZy5tb3VudCh0cnVlKTtcbiAgICAgIC8vIGFkZCB0aGlzIHRhZyB0byB0aGUgdmlydHVhbERvbSB2YXJpYWJsZVxuICAgICAgaWYgKCFjb250YWlucyhfX1RBR1NfQ0FDSEUsIHRhZykpIHsgX19UQUdTX0NBQ0hFLnB1c2godGFnKTsgfVxuICAgIH1cblxuICAgIHJldHVybiB0YWdcbiAgfVxuXG5cblxuICB2YXIgdGFncyA9IC8qI19fUFVSRV9fKi9PYmplY3QuZnJlZXplKHtcbiAgICBhcnJheWlzaEFkZDogYXJyYXlpc2hBZGQsXG4gICAgZ2V0VGFnTmFtZTogZ2V0TmFtZSxcbiAgICBpbmhlcml0UGFyZW50UHJvcHM6IGluaGVyaXRQYXJlbnRQcm9wcyxcbiAgICBtb3VudFRvOiBtb3VudCQxLFxuICAgIHNlbGVjdFRhZ3M6IHF1ZXJ5LFxuICAgIGFycmF5aXNoUmVtb3ZlOiBhcnJheWlzaFJlbW92ZSxcbiAgICBnZXRUYWc6IGdldCxcbiAgICBpbml0Q2hpbGRUYWc6IGluaXRDaGlsZCxcbiAgICBtb3ZlQ2hpbGRUYWc6IG1vdmVDaGlsZCxcbiAgICBtYWtlUmVwbGFjZVZpcnR1YWw6IG1ha2VSZXBsYWNlVmlydHVhbCxcbiAgICBnZXRJbW1lZGlhdGVDdXN0b21QYXJlbnRUYWc6IGdldEltbWVkaWF0ZUN1c3RvbVBhcmVudCxcbiAgICBtYWtlVmlydHVhbDogbWFrZVZpcnR1YWwsXG4gICAgbW92ZVZpcnR1YWw6IG1vdmVWaXJ0dWFsLFxuICAgIHVubW91bnRBbGw6IHVubW91bnRBbGwsXG4gICAgY3JlYXRlSWZEaXJlY3RpdmU6IGNyZWF0ZUlmRGlyZWN0aXZlLFxuICAgIGNyZWF0ZVJlZkRpcmVjdGl2ZTogY3JlYXRlUmVmRGlyZWN0aXZlXG4gIH0pO1xuXG4gIC8qKlxuICAgKiBSaW90IHB1YmxpYyBhcGlcbiAgICovXG4gIHZhciBzZXR0aW5ncyQxID0gc2V0dGluZ3M7XG4gIHZhciB1dGlsID0ge1xuICAgIHRtcGw6IHRtcGwsXG4gICAgYnJhY2tldHM6IGJyYWNrZXRzLFxuICAgIHN0eWxlTWFuYWdlcjogc3R5bGVNYW5hZ2VyLFxuICAgIHZkb206IF9fVEFHU19DQUNIRSxcbiAgICBzdHlsZU5vZGU6IHN0eWxlTWFuYWdlci5zdHlsZU5vZGUsXG4gICAgLy8gZXhwb3J0IHRoZSByaW90IGludGVybmFsIHV0aWxzIGFzIHdlbGxcbiAgICBkb206IGRvbSxcbiAgICBjaGVjazogY2hlY2ssXG4gICAgbWlzYzogbWlzYyxcbiAgICB0YWdzOiB0YWdzXG4gIH07XG5cbiAgLy8gZXhwb3J0IHRoZSBjb3JlIHByb3BzL21ldGhvZHNcbiAgdmFyIFRhZyQxID0gVGFnO1xuICB2YXIgdGFnJDEgPSB0YWc7XG4gIHZhciB0YWcyJDEgPSB0YWcyO1xuICB2YXIgbW91bnQkMiA9IG1vdW50O1xuICB2YXIgbWl4aW4kMSA9IG1peGluO1xuICB2YXIgdXBkYXRlJDIgPSB1cGRhdGUkMTtcbiAgdmFyIHVucmVnaXN0ZXIkMSA9IHVucmVnaXN0ZXI7XG4gIHZhciB2ZXJzaW9uJDEgPSB2ZXJzaW9uO1xuICB2YXIgb2JzZXJ2YWJsZSQxID0gb2JzZXJ2YWJsZTtcblxuICB2YXIgcmlvdCQxID0gZXh0ZW5kKHt9LCBjb3JlLCB7XG4gICAgb2JzZXJ2YWJsZTogb2JzZXJ2YWJsZSxcbiAgICBzZXR0aW5nczogc2V0dGluZ3MkMSxcbiAgICB1dGlsOiB1dGlsLFxuICB9KVxuXG4gIGV4cG9ydHMuc2V0dGluZ3MgPSBzZXR0aW5ncyQxO1xuICBleHBvcnRzLnV0aWwgPSB1dGlsO1xuICBleHBvcnRzLlRhZyA9IFRhZyQxO1xuICBleHBvcnRzLnRhZyA9IHRhZyQxO1xuICBleHBvcnRzLnRhZzIgPSB0YWcyJDE7XG4gIGV4cG9ydHMubW91bnQgPSBtb3VudCQyO1xuICBleHBvcnRzLm1peGluID0gbWl4aW4kMTtcbiAgZXhwb3J0cy51cGRhdGUgPSB1cGRhdGUkMjtcbiAgZXhwb3J0cy51bnJlZ2lzdGVyID0gdW5yZWdpc3RlciQxO1xuICBleHBvcnRzLnZlcnNpb24gPSB2ZXJzaW9uJDE7XG4gIGV4cG9ydHMub2JzZXJ2YWJsZSA9IG9ic2VydmFibGUkMTtcbiAgZXhwb3J0cy5kZWZhdWx0ID0gcmlvdCQxO1xuXG4gIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG5cbn0pKSk7XG4iLCJpbXBvcnQgcmlvdCBmcm9tIFwicmlvdFwiXHJcbmltcG9ydCBcIi4vdmlld3MvYXBwLmh0bWxcIlxyXG5cclxuXHJcbnJpb3QubW91bnQoJyonKSIsIjxhcHA+XHJcblxyXG4gIDxkaXYgY2xhc3M9XCJjb250YWluZXItc2Nyb2xsZXJcIj5cclxuICAgIDwhLS0gcGFydGlhbDpwYXJ0aWFscy9faG9yaXpvbnRhbC1uYXZiYXIuaHRtbCAtLT5cclxuICAgIDxuYXYgY2xhc3M9XCJuYXZiYXIgaG9yaXpvbnRhbC1sYXlvdXQgY29sLWxnLTEyIGNvbC0xMiBwLTBcIj5cclxuICAgICAgPGRpdiBjbGFzcz1cImNvbnRhaW5lciBkLWZsZXggZmxleC1yb3dcIj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwidGV4dC1jZW50ZXIgbmF2YmFyLWJyYW5kLXdyYXBwZXIgZC1mbGV4IGFsaWduLWl0ZW1zLXRvcFwiPlxyXG4gICAgICAgICAgPGEgY2xhc3M9XCJuYXZiYXItYnJhbmQgYnJhbmQtbG9nb1wiIGhyZWY9XCJpbmRleC5odG1sXCI+XHJcbiAgICAgICAgICAgIDxpbWcgc3JjPVwiaW1hZ2VzL2xvZ28taW52ZXJzZS5zdmdcIiBhbHQ9XCJsb2dvXCIgLz5cclxuICAgICAgICAgIDwvYT5cclxuICAgICAgICAgIDxhIGNsYXNzPVwibmF2YmFyLWJyYW5kIGJyYW5kLWxvZ28tbWluaVwiIGhyZWY9XCJpbmRleC5odG1sXCI+XHJcbiAgICAgICAgICAgIDxpbWcgc3JjPVwiaW1hZ2VzL2xvZ28tbWluaS5zdmdcIiBhbHQ9XCJsb2dvXCIgLz5cclxuICAgICAgICAgIDwvYT5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwibmF2YmFyLW1lbnUtd3JhcHBlciBkLWZsZXggYWxpZ24taXRlbXMtY2VudGVyXCI+XHJcbiAgICAgICAgICA8Zm9ybSBjbGFzcz1cInNlYXJjaC1maWVsZCBtbC1hdXRvXCIgYWN0aW9uPVwiI1wiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cCBtYi0wXCI+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaW5wdXQtZ3JvdXAtcHJlcGVuZFwiPlxyXG4gICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLXRleHRcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aSBjbGFzcz1cIm1kaSBtZGktbWFnbmlmeVwiPjwvaT5cclxuICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiPlxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgIDx1bCBjbGFzcz1cIm5hdmJhci1uYXYgbmF2YmFyLW5hdi1yaWdodCBtci0wXCI+XHJcbiAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtIGRyb3Bkb3duIG1sLTRcIj5cclxuICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rIGNvdW50LWluZGljYXRvciBkcm9wZG93bi10b2dnbGVcIiBpZD1cIm5vdGlmaWNhdGlvbkRyb3Bkb3duXCIgaHJlZj1cIiNcIiBkYXRhLXRvZ2dsZT1cImRyb3Bkb3duXCI+XHJcbiAgICAgICAgICAgICAgICA8aSBjbGFzcz1cIm1kaSBtZGktYmVsbC1vdXRsaW5lXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJjb3VudCBiZy13YXJuaW5nXCI+NDwvc3Bhbj5cclxuICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImRyb3Bkb3duLW1lbnUgZHJvcGRvd24tbWVudS1yaWdodCBuYXZiYXItZHJvcGRvd24gcHJldmlldy1saXN0XCIgYXJpYS1sYWJlbGxlZGJ5PVwibm90aWZpY2F0aW9uRHJvcGRvd25cIj5cclxuICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwiZHJvcGRvd24taXRlbSBweS0zXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxwIGNsYXNzPVwibWItMCBmb250LXdlaWdodC1tZWRpdW0gZmxvYXQtbGVmdFwiPllvdSBoYXZlIDQgbmV3IG5vdGlmaWNhdGlvbnNcclxuICAgICAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImJhZGdlIGJhZGdlLXBpbGwgYmFkZ2UtaW52ZXJzZS1pbmZvIGZsb2F0LXJpZ2h0XCI+VmlldyBhbGw8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZHJvcGRvd24tZGl2aWRlclwiPjwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJkcm9wZG93bi1pdGVtIHByZXZpZXctaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicHJldmlldy10aHVtYm5haWxcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicHJldmlldy1pY29uIGJnLWludmVyc2Utc3VjY2Vzc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJtZGkgbWRpLWFsZXJ0LWNpcmNsZS1vdXRsaW5lIG14LTBcIj48L2k+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicHJldmlldy1pdGVtLWNvbnRlbnRcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aDYgY2xhc3M9XCJwcmV2aWV3LXN1YmplY3QgZm9udC13ZWlnaHQtbm9ybWFsIHRleHQtZGFyayBtYi0xXCI+QXBwbGljYXRpb24gRXJyb3I8L2g2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzPVwiZm9udC13ZWlnaHQtbGlnaHQgc21hbGwtdGV4dCBtYi0wXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICBKdXN0IG5vd1xyXG4gICAgICAgICAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZHJvcGRvd24tZGl2aWRlclwiPjwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJkcm9wZG93bi1pdGVtIHByZXZpZXctaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicHJldmlldy10aHVtYm5haWxcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicHJldmlldy1pY29uIGJnLWludmVyc2Utd2FybmluZ1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJtZGkgbWRpLWNvbW1lbnQtdGV4dC1vdXRsaW5lIG14LTBcIj48L2k+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicHJldmlldy1pdGVtLWNvbnRlbnRcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aDYgY2xhc3M9XCJwcmV2aWV3LXN1YmplY3QgZm9udC13ZWlnaHQtbm9ybWFsIHRleHQtZGFyayBtYi0xXCI+U2V0dGluZ3M8L2g2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzPVwiZm9udC13ZWlnaHQtbGlnaHQgc21hbGwtdGV4dCBtYi0wXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICBQcml2YXRlIG1lc3NhZ2VcclxuICAgICAgICAgICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImRyb3Bkb3duLWRpdmlkZXJcIj48L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwiZHJvcGRvd24taXRlbSBwcmV2aWV3LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInByZXZpZXctdGh1bWJuYWlsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInByZXZpZXctaWNvbiBiZy1pbnZlcnNlLWluZm9cIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibWRpIG1kaS1lbWFpbC1vdXRsaW5lIG14LTBcIj48L2k+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicHJldmlldy1pdGVtLWNvbnRlbnRcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aDYgY2xhc3M9XCJwcmV2aWV3LXN1YmplY3QgZm9udC13ZWlnaHQtbm9ybWFsIHRleHQtZGFyayBtYi0xXCI+TmV3IHVzZXIgcmVnaXN0cmF0aW9uPC9oNj5cclxuICAgICAgICAgICAgICAgICAgICA8cCBjbGFzcz1cImZvbnQtd2VpZ2h0LWxpZ2h0IHNtYWxsLXRleHQgbWItMFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgMiBkYXlzIGFnb1xyXG4gICAgICAgICAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtIGRyb3Bkb3duIGQtbm9uZSBkLXhsLWlubGluZS1ibG9ja1wiPlxyXG4gICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmsgZHJvcGRvd24tdG9nZ2xlXCIgaWQ9XCJVc2VyRHJvcGRvd25cIiBocmVmPVwiI1wiIGRhdGEtdG9nZ2xlPVwiZHJvcGRvd25cIiBhcmlhLWV4cGFuZGVkPVwiZmFsc2VcIj5cclxuICAgICAgICAgICAgICAgIDxpbWcgY2xhc3M9XCJpbWcteHMgcm91bmRlZC1jaXJjbGVcIiBzcmM9XCJpbWFnZXMvZmFjZXMvZmFjZTEuanBnXCIgYWx0PVwiUHJvZmlsZSBpbWFnZVwiPlxyXG4gICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZHJvcGRvd24tbWVudSBkcm9wZG93bi1tZW51LXJpZ2h0IG5hdmJhci1kcm9wZG93blwiIGFyaWEtbGFiZWxsZWRieT1cIlVzZXJEcm9wZG93blwiPlxyXG4gICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJkcm9wZG93bi1pdGVtIHAtMFwiPlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZC1mbGV4IGJvcmRlci1ib3R0b21cIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicHktMyBweC00IGQtZmxleCBhbGlnbi1pdGVtcy1jZW50ZXIganVzdGlmeS1jb250ZW50LWNlbnRlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJtZGkgbWRpLWJvb2ttYXJrLXBsdXMtb3V0bGluZSBtci0wIHRleHQtZ3JheVwiPjwvaT5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicHktMyBweC00IGQtZmxleCBhbGlnbi1pdGVtcy1jZW50ZXIganVzdGlmeS1jb250ZW50LWNlbnRlciBib3JkZXItbGVmdCBib3JkZXItcmlnaHRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibWRpIG1kaS1hY2NvdW50LW91dGxpbmUgbXItMCB0ZXh0LWdyYXlcIj48L2k+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInB5LTMgcHgtNCBkLWZsZXggYWxpZ24taXRlbXMtY2VudGVyIGp1c3RpZnktY29udGVudC1jZW50ZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibWRpIG1kaS1hbGFybS1jaGVjayBtci0wIHRleHQtZ3JheVwiPjwvaT5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICA8YSBjbGFzcz1cImRyb3Bkb3duLWl0ZW0gbXQtMlwiPlxyXG4gICAgICAgICAgICAgICAgICBNYW5hZ2UgQWNjb3VudHNcclxuICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwiZHJvcGRvd24taXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICBDaGFuZ2UgUGFzc3dvcmRcclxuICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwiZHJvcGRvd24taXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICBDaGVjayBJbmJveFxyXG4gICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJkcm9wZG93bi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgIFNpZ24gT3V0XHJcbiAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cIm5hdmJhci10b2dnbGVyIGFsaWduLXNlbGYtY2VudGVyXCIgdHlwZT1cImJ1dHRvblwiIGRhdGEtdG9nZ2xlPVwibWluaW1pemVcIj5cclxuICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJtZGkgbWRpLW1lbnVcIj48L3NwYW4+XHJcbiAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC9kaXY+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJuYXYtYm90dG9tXCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgPHVsIGNsYXNzPVwibmF2IHBhZ2UtbmF2aWdhdGlvblwiPlxyXG4gICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgIDxhIGhyZWY9XCJpbmRleC5odG1sXCIgY2xhc3M9XCJuYXYtbGlua1wiPlxyXG4gICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJsaW5rLWljb24gbWRpIG1kaS10ZWxldmlzaW9uXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJtZW51LXRpdGxlXCI+REFTSEJPQVJEPC9zcGFuPlxyXG4gICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICA8YSBocmVmPVwiLi4vd2lkZ2V0cy5odG1sXCIgY2xhc3M9XCJuYXYtbGlua1wiPlxyXG4gICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJsaW5rLWljb24gbWRpIG1kaS1hcHBsZS1zYWZhcmlcIj48L2k+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cIm1lbnUtdGl0bGVcIj5XSURHRVRTPC9zcGFuPlxyXG4gICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW0gbWVnYS1tZW51XCI+XHJcbiAgICAgICAgICAgICAgPGEgaHJlZj1cIiNcIiBjbGFzcz1cIm5hdi1saW5rXCI+XHJcbiAgICAgICAgICAgICAgICA8aSBjbGFzcz1cImxpbmstaWNvbiBtZGkgbWRpLWF0b21cIj48L2k+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cIm1lbnUtdGl0bGVcIj5VSSBFTEVNRU5UUzwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibWVudS1hcnJvd1wiPjwvaT5cclxuICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInN1Ym1lbnVcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtZ3JvdXAtd3JhcHBlciByb3dcIj5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1ncm91cCBjb2wtbWQtNFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtMTJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHAgY2xhc3M9XCJjYXRlZ29yeS1oZWFkaW5nXCI+QmFzaWMgRWxlbWVudHM8L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtNlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8dWwgY2xhc3M9XCJzdWJtZW51LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi91aS1mZWF0dXJlcy9hY2NvcmRpb25zLmh0bWxcIj5BY2NvcmRpb248L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi91aS1mZWF0dXJlcy9idXR0b25zLmh0bWxcIj5CdXR0b25zPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vdWktZmVhdHVyZXMvYmFkZ2VzLmh0bWxcIj5CYWRnZXM8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi91aS1mZWF0dXJlcy9icmVhZGNydW1icy5odG1sXCI+QnJlYWRjcnVtYnM8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi91aS1mZWF0dXJlcy9kcm9wZG93bnMuaHRtbFwiPkRyb3Bkb3duPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vdWktZmVhdHVyZXMvbW9kYWxzLmh0bWxcIj5Nb2RhbHM8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC02XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDx1bCBjbGFzcz1cInN1Ym1lbnUtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIi4uL3VpLWZlYXR1cmVzL3Byb2dyZXNzLmh0bWxcIj5Qcm9ncmVzcyBiYXI8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi91aS1mZWF0dXJlcy9wYWdpbmF0aW9uLmh0bWxcIj5QYWdpbmF0aW9uPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vdWktZmVhdHVyZXMvdGFicy5odG1sXCI+VGFiczwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIi4uL3VpLWZlYXR1cmVzL3R5cG9ncmFwaHkuaHRtbFwiPlR5cG9ncmFwaHk8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi91aS1mZWF0dXJlcy90b29sdGlwcy5odG1sXCI+VG9vbHRpcDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLWdyb3VwIGNvbC1tZC00XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC0xMlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8cCBjbGFzcz1cImNhdGVnb3J5LWhlYWRpbmdcIj5BZHZhbmNlZCBFbGVtZW50czwvcD5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC02XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDx1bCBjbGFzcz1cInN1Ym1lbnUtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIi4uL3VpLWZlYXR1cmVzL2RyYWd1bGEuaHRtbFwiPkRyYWd1bGE8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi91aS1mZWF0dXJlcy9jYXJvdXNlbC5odG1sXCI+Q2Fyb3VzZWw8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi91aS1mZWF0dXJlcy9jbGlwYm9hcmQuaHRtbFwiPkNsaXBib2FyZDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIi4uL3VpLWZlYXR1cmVzL2NvbnRleHQtbWVudS5odG1sXCI+Q29udGV4dCBNZW51PC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vdWktZmVhdHVyZXMvbG9hZGVycy5odG1sXCI+TG9hZGVyPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vdWktZmVhdHVyZXMvc2xpZGVyLmh0bWxcIj5TbGlkZXI8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC02XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDx1bCBjbGFzcz1cInN1Ym1lbnUtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIi4uL3VpLWZlYXR1cmVzL3RvdXIuaHRtbFwiPlRvdXI8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi91aS1mZWF0dXJlcy9wb3B1cHMuaHRtbFwiPlBvcHVwPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vdWktZmVhdHVyZXMvbm90aWZpY2F0aW9ucy5odG1sXCI+Tm90aWZpY2F0aW9uPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtZ3JvdXAgY29sLW1kLTJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8cCBjbGFzcz1cImNhdGVnb3J5LWhlYWRpbmdcIj5UYWJsZTwvcD5cclxuICAgICAgICAgICAgICAgICAgICA8dWwgY2xhc3M9XCJzdWJtZW51LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vdGFibGVzL2Jhc2ljLXRhYmxlLmh0bWxcIj5CYXNpYyBUYWJsZTwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIi4uL3RhYmxlcy9kYXRhLXRhYmxlLmh0bWxcIj5EYXRhIFRhYmxlPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vdGFibGVzL2pzLWdyaWQuaHRtbFwiPkpzLWdyaWQ8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi90YWJsZXMvc29ydGFibGUtdGFibGUuaHRtbFwiPlNvcnRhYmxlIFRhYmxlPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1ncm91cCBjb2wtbWQtMlwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzPVwiY2F0ZWdvcnktaGVhZGluZ1wiPkljb25zPC9wPlxyXG4gICAgICAgICAgICAgICAgICAgIDx1bCBjbGFzcz1cInN1Ym1lbnUtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi9pY29ucy9mbGFnLWljb25zLmh0bWxcIj5GbGFnIEljb25zPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vaWNvbnMvZm9udC1hd2Vzb21lLmh0bWxcIj5Gb250IEF3ZXNvbWU8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi9pY29ucy9zaW1wbGUtbGluZS1pY29uLmh0bWxcIj5TaW1wbGUgTGluZSBJY29uczwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIi4uL2ljb25zL3RoZW1pZnkuaHRtbFwiPlRoZW1pZnkgSWNvbnM8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtIG1lZ2EtbWVudVwiPlxyXG4gICAgICAgICAgICAgIDxhIGhyZWY9XCIjXCIgY2xhc3M9XCJuYXYtbGlua1wiPlxyXG4gICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJsaW5rLWljb24gbWRpIG1kaS1mbGFnLW91dGxpbmVcIj48L2k+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cIm1lbnUtdGl0bGVcIj5QQUdFUzwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibWVudS1hcnJvd1wiPjwvaT5cclxuICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInN1Ym1lbnVcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtZ3JvdXAtd3JhcHBlciByb3dcIj5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1ncm91cCBjb2wtbWQtM1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzPVwiY2F0ZWdvcnktaGVhZGluZ1wiPlVzZXIgUGFnZXM8L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgPHVsIGNsYXNzPVwic3VibWVudS1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIi4uL3NhbXBsZXMvbG9naW4uaHRtbFwiPkxvZ2luPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vc2FtcGxlcy9sb2dpbi0yLmh0bWxcIj5Mb2dpbiAyPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vc2FtcGxlcy9yZWdpc3Rlci5odG1sXCI+UmVnaXN0ZXI8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi9zYW1wbGVzL3JlZ2lzdGVyLTIuaHRtbFwiPlJlZ2lzdGVyIDI8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi9zYW1wbGVzL2xvY2stc2NyZWVuLmh0bWxcIj5Mb2Nrc2NyZWVuPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1ncm91cCBjb2wtbWQtM1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzPVwiY2F0ZWdvcnktaGVhZGluZ1wiPkVycm9yIFBhZ2VzPC9wPlxyXG4gICAgICAgICAgICAgICAgICAgIDx1bCBjbGFzcz1cInN1Ym1lbnUtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi9zYW1wbGVzL2Vycm9yLTQwMC5odG1sXCI+NDAwPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vc2FtcGxlcy9lcnJvci00MDQuaHRtbFwiPjQwNDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIi4uL3NhbXBsZXMvZXJyb3ItNTAwLmh0bWxcIj41MDA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi9zYW1wbGVzL2Vycm9yLTUwNS5odG1sXCI+NTA1PC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1ncm91cCBjb2wtbWQtM1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzPVwiY2F0ZWdvcnktaGVhZGluZ1wiPkUtY29tbWVyY2U8L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgPHVsIGNsYXNzPVwic3VibWVudS1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIi4uL3NhbXBsZXMvaW52b2ljZS5odG1sXCI+SW52b2ljZTwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIi4uL3NhbXBsZXMvcHJpY2luZy10YWJsZS5odG1sXCI+UHJpY2luZyBUYWJsZTwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIi4uL3NhbXBsZXMvb3JkZXJzLmh0bWxcIj5PcmRlcnM8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLWdyb3VwIGNvbC1tZC0zXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC0xMlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8cCBjbGFzcz1cImNhdGVnb3J5LWhlYWRpbmdcIj5MYXlvdXQ8L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDx1bCBjbGFzcz1cInN1Ym1lbnUtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIi4uL2xheW91dHMvcnRsLmh0bWxcIj5SVEwgTGF5b3V0PC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtMTIgbXQtM1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8cCBjbGFzcz1cImNhdGVnb3J5LWhlYWRpbmdcIj5Eb2N1bWVudGF0aW9uPC9wPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8dWwgY2xhc3M9XCJzdWJtZW51LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi9kb2N1bWVudGF0aW9uLmh0bWxcIj5Eb2N1bWVudGF0aW9uPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW0gbWVnYS1tZW51XCI+XHJcbiAgICAgICAgICAgICAgPGEgaHJlZj1cIiNcIiBjbGFzcz1cIm5hdi1saW5rXCI+XHJcbiAgICAgICAgICAgICAgICA8aSBjbGFzcz1cImxpbmstaWNvbiBtZGkgbWRpLWFuZHJvaWQtc3R1ZGlvXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJtZW51LXRpdGxlXCI+Rk9STVM8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8aSBjbGFzcz1cIm1lbnUtYXJyb3dcIj48L2k+XHJcbiAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJzdWJtZW51XCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLWdyb3VwLXdyYXBwZXIgcm93XCI+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtZ3JvdXAgY29sLW1kLTNcIj5cclxuICAgICAgICAgICAgICAgICAgICA8cCBjbGFzcz1cImNhdGVnb3J5LWhlYWRpbmdcIj5CYXNpYyBFbGVtZW50czwvcD5cclxuICAgICAgICAgICAgICAgICAgICA8dWwgY2xhc3M9XCJzdWJtZW51LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiYmFzaWNfZWxlbWVudHMuaHRtbFwiPkJhc2ljIEVsZW1lbnRzPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiYWR2YW5jZWRfZWxlbWVudHMuaHRtbFwiPkFkdmFuY2VkIEVsZW1lbnRzPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwidmFsaWRhdGlvbi5odG1sXCI+VmFsaWRhdGlvbjwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIndpemFyZC5odG1sXCI+V2l6YXJkPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwidGV4dF9lZGl0b3IuaHRtbFwiPlRleHQgRWRpdG9yPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiY29kZV9lZGl0b3IuaHRtbFwiPkNvZGUgRWRpdG9yPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1ncm91cCBjb2wtbWQtM1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzPVwiY2F0ZWdvcnktaGVhZGluZ1wiPkNoYXJ0czwvcD5cclxuICAgICAgICAgICAgICAgICAgICA8dWwgY2xhc3M9XCJzdWJtZW51LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vY2hhcnRzL2NoYXJ0anMuaHRtbFwiPkNoYXJ0IEpzPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vY2hhcnRzL21vcnJpcy5odG1sXCI+TW9ycmlzPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vY2hhcnRzL2Zsb3QtY2hhcnQuaHRtbFwiPkZsYW90PC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vY2hhcnRzL2dvb2dsZS1jaGFydHMuaHRtbFwiPkdvb2dsZSBDaGFydDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIi4uL2NoYXJ0cy9zcGFya2xpbmUuaHRtbFwiPlNwYXJrbGluZTwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIi4uL2NoYXJ0cy9jMy5odG1sXCI+QzMgQ2hhcnQ8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi9jaGFydHMvY2hhcnRpc3QuaHRtbFwiPkNoYXJ0aXN0PC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vY2hhcnRzL2p1c3RHYWdlLmh0bWxcIj5KdXN0R2FnZTwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtZ3JvdXAgY29sLW1kLTNcIj5cclxuICAgICAgICAgICAgICAgICAgICA8cCBjbGFzcz1cImNhdGVnb3J5LWhlYWRpbmdcIj5NYXBzPC9wPlxyXG4gICAgICAgICAgICAgICAgICAgIDx1bCBjbGFzcz1cInN1Ym1lbnUtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi9tYXBzL21hcGVhbC5odG1sXCI+TWFwZWFsPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vbWFwcy92ZWN0b3ItbWFwLmh0bWxcIj5WZWN0b3IgTWFwPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vbWFwcy9nb29nbGUtbWFwcy5odG1sXCI+R29vZ2xlIE1hcDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICA8YSBocmVmPVwiI1wiIGNsYXNzPVwibmF2LWxpbmtcIj5cclxuICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibGluay1pY29uIG1kaSBtZGktYXN0ZXJpc2tcIj48L2k+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cIm1lbnUtdGl0bGVcIj5BUFBTPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJtZW51LWFycm93XCI+PC9pPlxyXG4gICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic3VibWVudVwiPlxyXG4gICAgICAgICAgICAgICAgPHVsIGNsYXNzPVwic3VibWVudS1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi9hcHBzL2VtYWlsLmh0bWxcIj5FbWFpbDwvYT5cclxuICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIi4uL2FwcHMvY2FsZW5kYXIuaHRtbFwiPkNhbGVuZGFyPC9hPlxyXG4gICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiLi4vYXBwcy90b2RvLmh0bWxcIj5Ub2RvIExpc3Q8L2E+XHJcbiAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIuLi9hcHBzL2dhbGxlcnkuaHRtbFwiPkdhbGxlcnk8L2E+XHJcbiAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgPC91bD5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L25hdj5cclxuXHJcbiAgICA8IS0tIHBhcnRpYWwgLS0+XHJcbiAgICA8ZGl2IGNsYXNzPVwiY29udGFpbmVyLWZsdWlkIHBhZ2UtYm9keS13cmFwcGVyXCI+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJtYWluLXBhbmVsXCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImNvbnRlbnQtd3JhcHBlclwiPlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cInJvd1wiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTYgZC1mbGV4IGFsaWduLWl0ZW1zLXN0cmV0Y2hcIj5cclxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93IGZsZXgtZ3Jvd1wiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC0xMiBncmlkLW1hcmdpblwiPlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY2FyZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjYXJkLWJvZHlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxoNCBjbGFzcz1cImNhcmQtdGl0bGVcIj5EZWZhdWx0IGZvcm08L2g0PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPHAgY2xhc3M9XCJjYXJkLWRlc2NyaXB0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIEJhc2ljIGZvcm0gbGF5b3V0XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8Zm9ybSBjbGFzcz1cImZvcm1zLXNhbXBsZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJleGFtcGxlSW5wdXRFbWFpbDFcIj5FbWFpbCBhZGRyZXNzPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImVtYWlsXCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBpZD1cImV4YW1wbGVJbnB1dEVtYWlsMVwiIHBsYWNlaG9sZGVyPVwiRW50ZXIgZW1haWxcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cImV4YW1wbGVJbnB1dFBhc3N3b3JkMVwiPlBhc3N3b3JkPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInBhc3N3b3JkXCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBpZD1cImV4YW1wbGVJbnB1dFBhc3N3b3JkMVwiIHBsYWNlaG9sZGVyPVwiUGFzc3dvcmRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT1cInN1Ym1pdFwiIGNsYXNzPVwiYnRuIGJ0bi1zdWNjZXNzIG1yLTJcIj5TdWJtaXQ8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cImJ0biBidG4tbGlnaHRcIj5DYW5jZWw8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtMTIgc3RyZXRjaC1jYXJkIGdyaWQtbWFyZ2luXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjYXJkXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNhcmQtYm9keVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGg0IGNsYXNzPVwiY2FyZC10aXRsZVwiPkhvcml6b250YWwgRm9ybTwvaDQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8cCBjbGFzcz1cImNhcmQtZGVzY3JpcHRpb25cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgSG9yaXpvbnRhbCBmb3JtIGxheW91dFxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGZvcm0gY2xhc3M9XCJmb3Jtcy1zYW1wbGVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXAgcm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cImV4YW1wbGVJbnB1dEVtYWlsMlwiIGNsYXNzPVwiY29sLXNtLTMgY29sLWZvcm0tbGFiZWxcIj5FbWFpbDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS05XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImVtYWlsXCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBpZD1cImV4YW1wbGVJbnB1dEVtYWlsMlwiIHBsYWNlaG9sZGVyPVwiRW50ZXIgZW1haWxcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIHJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJleGFtcGxlSW5wdXRQYXNzd29yZDJcIiBjbGFzcz1cImNvbC1zbS0zIGNvbC1mb3JtLWxhYmVsXCI+UGFzc3dvcmQ8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tOVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJwYXNzd29yZFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgaWQ9XCJleGFtcGxlSW5wdXRQYXNzd29yZDJcIiBwbGFjZWhvbGRlcj1cIlBhc3N3b3JkXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJzdWJtaXRcIiBjbGFzcz1cImJ0biBidG4tc3VjY2VzcyBtci0yXCI+U3VibWl0PC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3M9XCJidG4gYnRuLWxpZ2h0XCI+Q2FuY2VsPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Zvcm0+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTYgZ3JpZC1tYXJnaW4gc3RyZXRjaC1jYXJkXCI+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNhcmRcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjYXJkLWJvZHlcIj5cclxuICAgICAgICAgICAgICAgICAgPGg0IGNsYXNzPVwiY2FyZC10aXRsZVwiPkJhc2ljIGZvcm08L2g0PlxyXG4gICAgICAgICAgICAgICAgICA8cCBjbGFzcz1cImNhcmQtZGVzY3JpcHRpb25cIj5cclxuICAgICAgICAgICAgICAgICAgICBCYXNpYyBmb3JtIGVsZW1lbnRzXHJcbiAgICAgICAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgICAgICAgICAgPGZvcm0gY2xhc3M9XCJmb3Jtcy1zYW1wbGVcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cImV4YW1wbGVJbnB1dE5hbWUxXCI+TmFtZTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIGlkPVwiZXhhbXBsZUlucHV0TmFtZTFcIiBwbGFjZWhvbGRlcj1cIk5hbWVcIj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cImV4YW1wbGVJbnB1dEVtYWlsM1wiPkVtYWlsIGFkZHJlc3M8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJlbWFpbFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgaWQ9XCJleGFtcGxlSW5wdXRFbWFpbDNcIiBwbGFjZWhvbGRlcj1cIkVtYWlsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJleGFtcGxlSW5wdXRQYXNzd29yZDRcIj5QYXNzd29yZDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInBhc3N3b3JkXCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBpZD1cImV4YW1wbGVJbnB1dFBhc3N3b3JkNFwiIHBsYWNlaG9sZGVyPVwiUGFzc3dvcmRcIj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGxhYmVsPkZpbGUgdXBsb2FkPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiZmlsZVwiIG5hbWU9XCJpbWdbXVwiIGNsYXNzPVwiZmlsZS11cGxvYWQtZGVmYXVsdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwIGNvbC14cy0xMlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbCBmaWxlLXVwbG9hZC1pbmZvXCIgZGlzYWJsZWQgcGxhY2Vob2xkZXI9XCJVcGxvYWQgSW1hZ2VcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJpbnB1dC1ncm91cC1hcHBlbmRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVwiZmlsZS11cGxvYWQtYnJvd3NlIGJ0biBidG4taW5mb1wiIHR5cGU9XCJidXR0b25cIj5VcGxvYWQ8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJleGFtcGxlSW5wdXRDaXR5MVwiPkNpdHk8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBpZD1cImV4YW1wbGVJbnB1dENpdHkxXCIgcGxhY2Vob2xkZXI9XCJMb2NhdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgZm9yPVwiZXhhbXBsZVRleHRhcmVhMVwiPlRleHRhcmVhPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgIDx0ZXh0YXJlYSBjbGFzcz1cImZvcm0tY29udHJvbFwiIGlkPVwiZXhhbXBsZVRleHRhcmVhMVwiIHJvd3M9XCIyXCI+PC90ZXh0YXJlYT5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJzdWJtaXRcIiBjbGFzcz1cImJ0biBidG4tc3VjY2VzcyBtci0yXCI+U3VibWl0PC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cImJ0biBidG4tbGlnaHRcIj5DYW5jZWw8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgPC9mb3JtPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTYgZC1mbGV4IGFsaWduLWl0ZW1zLXN0cmV0Y2hcIj5cclxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93IGZsZXgtZ3Jvd1wiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMiBncmlkLW1hcmdpbiBzdHJldGNoLWNhcmRcIj5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNhcmRcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY2FyZC1ib2R5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8aDQgY2xhc3M9XCJjYXJkLXRpdGxlXCI+U2VsZWN0IDI8L2g0PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsPlNpbmdsZSBzZWxlY3QgYm94IHVzaW5nIHNlbGVjdCAyPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHNlbGVjdCBjbGFzcz1cImpzLWV4YW1wbGUtYmFzaWMtc2luZ2xlXCIgc3R5bGU9XCJ3aWR0aDoxMDAlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cIkFMXCI+QWxhYmFtYTwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJXWVwiPld5b21pbmc8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiQU1cIj5BbWVyaWNhPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cIkNBXCI+Q2FuYWRhPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cIlJVXCI+UnVzc2lhPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvc2VsZWN0PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWw+TXVsdGlwbGUgc2VsZWN0IHVzaW5nIHNlbGVjdCAyPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHNlbGVjdCBjbGFzcz1cImpzLWV4YW1wbGUtYmFzaWMtbXVsdGlwbGVcIiBtdWx0aXBsZT1cIm11bHRpcGxlXCIgc3R5bGU9XCJ3aWR0aDoxMDAlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cIkFMXCI+QWxhYmFtYTwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJXWVwiPld5b21pbmc8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiQU1cIj5BbWVyaWNhPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cIkNBXCI+Q2FuYWRhPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cIlJVXCI+UnVzc2lhPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvc2VsZWN0PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEyIGdyaWQtbWFyZ2luIHN0cmV0Y2gtY2FyZFwiPlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY2FyZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjYXJkLWJvZHlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxoNCBjbGFzcz1cImNhcmQtdGl0bGVcIj5UeXBlYWhlYWQ8L2g0PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPHAgY2xhc3M9XCJjYXJkLWRlc2NyaXB0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIEEgc2ltcGxlIHN1Z2dlc3Rpb24gZW5naW5lXHJcbiAgICAgICAgICAgICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cCByb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbD5CYXNpYzwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBpZD1cInRoZS1iYXNpY3NcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCBjbGFzcz1cInR5cGVhaGVhZFwiIHR5cGU9XCJ0ZXh0XCIgcGxhY2Vob2xkZXI9XCJTdGF0ZXMgb2YgVVNBXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsPkJsb29kaG91bmQ8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgaWQ9XCJibG9vZGhvdW5kXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgY2xhc3M9XCJ0eXBlYWhlYWRcIiB0eXBlPVwidGV4dFwiIHBsYWNlaG9sZGVyPVwiU3RhdGVzIG9mIFVTQVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC02IGdyaWQtbWFyZ2luIHN0cmV0Y2gtY2FyZFwiPlxyXG4gICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjYXJkXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY2FyZC1ib2R5XCI+XHJcbiAgICAgICAgICAgICAgICAgIDxoNCBjbGFzcz1cImNhcmQtdGl0bGVcIj5Db2xvdXJlZCBzZWxlY3QgYm94PC9oND5cclxuICAgICAgICAgICAgICAgICAgPHAgY2xhc3M9XCJjYXJkLWRlc2NyaXB0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgQmFzaWMgYm9vdHN0cmFwIHNlbGVjdCBpbiB0aGVtZSBjb2xvcnNcclxuICAgICAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJleGFtcGxlU2VsZWN0UHJpbWFyeVwiPlByaW1hcnk8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgIDxzZWxlY3QgY2xhc3M9XCJmb3JtLWNvbnRyb2wgYm9yZGVyLXByaW1hcnlcIiBpZD1cImV4YW1wbGVTZWxlY3RQcmltYXJ5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPjE8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24+Mjwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbj4zPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPjQ8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24+NTwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvc2VsZWN0PlxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgICAgICA8bGFiZWwgZm9yPVwiZXhhbXBsZVNlbGVjdEluZm9cIj5JbmZvPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICA8c2VsZWN0IGNsYXNzPVwiZm9ybS1jb250cm9sIGJvcmRlci1pbmZvXCIgaWQ9XCJleGFtcGxlU2VsZWN0SW5mb1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbj4xPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPjI8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24+Mzwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbj40PC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPjU8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICA8L3NlbGVjdD5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cImV4YW1wbGVTZWxlY3RTdWNjZXNzXCI+U3VjY2VzczwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgPHNlbGVjdCBjbGFzcz1cImZvcm0tY29udHJvbCBib3JkZXItc3VjY2Vzc1wiIGlkPVwiZXhhbXBsZVNlbGVjdFN1Y2Nlc3NcIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24+MTwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbj4yPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPjM8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24+NDwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbj41PC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9zZWxlY3Q+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJleGFtcGxlU2VsZWN0RGFuZ2VyXCI+RGFuZ2VyPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICA8c2VsZWN0IGNsYXNzPVwiZm9ybS1jb250cm9sIGJvcmRlci1kYW5nZXJcIiBpZD1cImV4YW1wbGVTZWxlY3REYW5nZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24+MTwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbj4yPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPjM8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24+NDwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbj41PC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9zZWxlY3Q+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJleGFtcGxlU2VsZWN0V2FybmluZ1wiPldhcm5pbmc8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgIDxzZWxlY3QgY2xhc3M9XCJmb3JtLWNvbnRyb2wgYm9yZGVyLXdhcm5pbmdcIiBpZD1cImV4YW1wbGVTZWxlY3RXYXJuaW5nXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPjE8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24+Mjwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbj4zPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPjQ8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24+NTwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvc2VsZWN0PlxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC01IGQtZmxleCBhbGlnbi1pdGVtcy1zdHJldGNoXCI+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvdyBmbGV4LWdyb3dcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtMTIgZ3JpZC1tYXJnaW5cIj5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNhcmRcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY2FyZC1ib2R5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8aDQgY2xhc3M9XCJjYXJkLXRpdGxlXCI+QmFzaWMgaW5wdXQgZ3JvdXBzPC9oND5cclxuICAgICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzPVwiY2FyZC1kZXNjcmlwdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBCYXNpYyBib290c3RyYXAgaW5wdXQgZ3JvdXBzXHJcbiAgICAgICAgICAgICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaW5wdXQtZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaW5wdXQtZ3JvdXAtcHJlcGVuZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJpbnB1dC1ncm91cC10ZXh0XCI+QDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIHBsYWNlaG9sZGVyPVwiVXNlcm5hbWVcIiBhcmlhLWxhYmVsPVwiVXNlcm5hbWVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cC1wcmVwZW5kXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLXRleHRcIj4kPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgYXJpYS1sYWJlbD1cIkFtb3VudCAodG8gdGhlIG5lYXJlc3QgZG9sbGFyKVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cC1hcHBlbmRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaW5wdXQtZ3JvdXAtdGV4dFwiPi4wMDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cC1wcmVwZW5kXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLXRleHRcIj4kPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cC1wcmVwZW5kXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLXRleHRcIj4wLjAwPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgYXJpYS1sYWJlbD1cIkFtb3VudCAodG8gdGhlIG5lYXJlc3QgZG9sbGFyKVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC0xMiBncmlkLW1hcmdpbiBzdHJldGNoLWNhcmRcIj5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNhcmRcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY2FyZC1ib2R5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8aDQgY2xhc3M9XCJjYXJkLXRpdGxlXCI+Q29sb3JlZCBpbnB1dCBncm91cHM8L2g0PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPHAgY2xhc3M9XCJjYXJkLWRlc2NyaXB0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIElucHV0IGdyb3VwcyB3aXRoIGNvbG9yc1xyXG4gICAgICAgICAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwLXByZXBlbmQgYmctaW5mb1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJpbnB1dC1ncm91cC10ZXh0IGJnLXRyYW5zcGFyZW50XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibWRpIG1kaS1zaGllbGQtb3V0bGluZSB0ZXh0LXdoaXRlXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgcGxhY2Vob2xkZXI9XCJVc2VybmFtZVwiIGFyaWEtbGFiZWw9XCJVc2VybmFtZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwLXByZXBlbmQgYmctcHJpbWFyeSBib3JkZXItcHJpbWFyeVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJpbnB1dC1ncm91cC10ZXh0IGJnLXRyYW5zcGFyZW50XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibWRpIG1kaSBtZGktbWVudSB0ZXh0LXdoaXRlXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgcGxhY2Vob2xkZXI9XCJVc2VybmFtZVwiIGFyaWEtbGFiZWw9XCJVc2VybmFtZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBwbGFjZWhvbGRlcj1cIlVzZXJuYW1lXCIgYXJpYS1sYWJlbD1cIlVzZXJuYW1lXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwLWFwcGVuZCBiZy1wcmltYXJ5IGJvcmRlci1wcmltYXJ5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLXRleHQgYmctdHJhbnNwYXJlbnRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJtZGkgbWRpLW1lbnUgdGV4dC13aGl0ZVwiPjwvaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cC1wcmVwZW5kIGJnLXByaW1hcnkgYm9yZGVyLXByaW1hcnlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaW5wdXQtZ3JvdXAtdGV4dCBiZy10cmFuc3BhcmVudCB0ZXh0LXdoaXRlXCI+JDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIGFyaWEtbGFiZWw9XCJBbW91bnQgKHRvIHRoZSBuZWFyZXN0IGRvbGxhcilcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaW5wdXQtZ3JvdXAtYXBwZW5kIGJnLXByaW1hcnkgYm9yZGVyLXByaW1hcnlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaW5wdXQtZ3JvdXAtdGV4dCBiZy10cmFuc3BhcmVudCB0ZXh0LXdoaXRlXCI+LjAwPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC03IGdyaWQtbWFyZ2luIHN0cmV0Y2gtY2FyZFwiPlxyXG4gICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjYXJkXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY2FyZC1ib2R5XCI+XHJcbiAgICAgICAgICAgICAgICAgIDxoNCBjbGFzcz1cImNhcmQtdGl0bGVcIj5JbnB1dCBzaXplPC9oND5cclxuICAgICAgICAgICAgICAgICAgPHAgY2xhc3M9XCJjYXJkLWRlc2NyaXB0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgVGhpcyBpcyB0aGUgZGVmYXVsdCBib290c3RyYXAgZm9ybSBsYXlvdXRcclxuICAgICAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbD5MYXJnZSBpbnB1dDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2wgZm9ybS1jb250cm9sLWxnXCIgcGxhY2Vob2xkZXI9XCJVc2VybmFtZVwiIGFyaWEtbGFiZWw9XCJVc2VybmFtZVwiPlxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgICAgICA8bGFiZWw+RGVmYXVsdCBpbnB1dDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBwbGFjZWhvbGRlcj1cIlVzZXJuYW1lXCIgYXJpYS1sYWJlbD1cIlVzZXJuYW1lXCI+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbD5TbWFsbCBpbnB1dDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2wgZm9ybS1jb250cm9sLXNtXCIgcGxhY2Vob2xkZXI9XCJVc2VybmFtZVwiIGFyaWEtbGFiZWw9XCJVc2VybmFtZVwiPlxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNhcmQtYm9keVwiPlxyXG4gICAgICAgICAgICAgICAgICA8aDQgY2xhc3M9XCJjYXJkLXRpdGxlXCI+U2VsZWN0aXplPC9oND5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgICAgICA8bGFiZWwgZm9yPVwiZXhhbXBsZUZvcm1Db250cm9sU2VsZWN0MVwiPkxhcmdlIHNlbGVjdDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgPHNlbGVjdCBjbGFzcz1cImZvcm0tY29udHJvbCBmb3JtLWNvbnRyb2wtbGdcIiBpZD1cImV4YW1wbGVGb3JtQ29udHJvbFNlbGVjdDFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24+MTwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbj4yPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPjM8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24+NDwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbj41PC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9zZWxlY3Q+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJleGFtcGxlRm9ybUNvbnRyb2xTZWxlY3QyXCI+RGVmYXVsdCBzZWxlY3Q8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgIDxzZWxlY3QgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBpZD1cImV4YW1wbGVGb3JtQ29udHJvbFNlbGVjdDJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24+MTwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbj4yPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPjM8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24+NDwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbj41PC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9zZWxlY3Q+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJleGFtcGxlRm9ybUNvbnRyb2xTZWxlY3QzXCI+U21hbGwgc2VsZWN0PC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICA8c2VsZWN0IGNsYXNzPVwiZm9ybS1jb250cm9sIGZvcm0tY29udHJvbC1zbVwiIGlkPVwiZXhhbXBsZUZvcm1Db250cm9sU2VsZWN0M1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbj4xPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPjI8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24+Mzwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbj40PC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPjU8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICA8L3NlbGVjdD5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtNiBncmlkLW1hcmdpbiBzdHJldGNoLWNhcmRcIj5cclxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY2FyZFwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNhcmQtYm9keVwiPlxyXG4gICAgICAgICAgICAgICAgICA8aDQgY2xhc3M9XCJjYXJkLXRpdGxlXCI+Q2hlY2tib3ggQ29udHJvbHM8L2g0PlxyXG4gICAgICAgICAgICAgICAgICA8cCBjbGFzcz1cImNhcmQtZGVzY3JpcHRpb25cIj5DaGVja2JveCBhbmQgcmFkaW8gY29udHJvbHM8L3A+XHJcbiAgICAgICAgICAgICAgICAgIDxmb3JtIGNsYXNzPVwiZm9ybXMtc2FtcGxlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC02XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tY2hlY2tcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cImZvcm0tY2hlY2stbGFiZWxcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIGNsYXNzPVwiZm9ybS1jaGVjay1pbnB1dFwiPiBEZWZhdWx0XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWNoZWNrXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJmb3JtLWNoZWNrLWxhYmVsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBjbGFzcz1cImZvcm0tY2hlY2staW5wdXRcIiBjaGVja2VkPiBDaGVja2VkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWNoZWNrXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJmb3JtLWNoZWNrLWxhYmVsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBjbGFzcz1cImZvcm0tY2hlY2staW5wdXRcIiBkaXNhYmxlZD4gRGlzYWJsZWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tY2hlY2tcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cImZvcm0tY2hlY2stbGFiZWxcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIGNsYXNzPVwiZm9ybS1jaGVjay1pbnB1dFwiIGRpc2FibGVkIGNoZWNrZWQ+IERpc2FibGVkIGNoZWNrZWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTZcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1yYWRpb1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwiZm9ybS1jaGVjay1sYWJlbFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInJhZGlvXCIgY2xhc3M9XCJmb3JtLWNoZWNrLWlucHV0XCIgbmFtZT1cIm9wdGlvbnNSYWRpb3NcIiBpZD1cIm9wdGlvbnNSYWRpb3MxXCIgdmFsdWU9XCJcIiBjaGVja2VkPiBPcHRpb24gb25lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLXJhZGlvXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJmb3JtLWNoZWNrLWxhYmVsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwicmFkaW9cIiBjbGFzcz1cImZvcm0tY2hlY2staW5wdXRcIiBuYW1lPVwib3B0aW9uc1JhZGlvc1wiIGlkPVwib3B0aW9uc1JhZGlvczJcIiB2YWx1ZT1cIm9wdGlvbjJcIj4gT3B0aW9uIHR3b1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tcmFkaW8gZGlzYWJsZWRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cImZvcm0tY2hlY2stbGFiZWxcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJyYWRpb1wiIGNsYXNzPVwiZm9ybS1jaGVjay1pbnB1dFwiIG5hbWU9XCJvcHRpb25zUmFkaW9zMlwiIGlkPVwib3B0aW9uc1JhZGlvczNcIiB2YWx1ZT1cIm9wdGlvbjNcIiBkaXNhYmxlZD4gT3B0aW9uIHRocmVlIGlzIGRpc2FibGVkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLXJhZGlvIGRpc2FibGVkXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJmb3JtLWNoZWNrLWxhYmVsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwicmFkaW9cIiBjbGFzcz1cImZvcm0tY2hlY2staW5wdXRcIiBuYW1lPVwib3B0aW9uc1JhZGlvMlwiIGlkPVwib3B0aW9uc1JhZGlvczRcIiB2YWx1ZT1cIm9wdGlvbjRcIiBkaXNhYmxlZCBjaGVja2VkPiBPcHRpb24gZm91ciBpcyBzZWxlY3RlZCBhbmQgZGlzYWJsZWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC02IGdyaWQtbWFyZ2luIHN0cmV0Y2gtY2FyZFwiPlxyXG4gICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjYXJkXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY2FyZC1ib2R5XCI+XHJcbiAgICAgICAgICAgICAgICAgIDxoNCBjbGFzcz1cImNhcmQtdGl0bGVcIj5DaGVja2JveCBGbGF0IENvbnRyb2xzPC9oND5cclxuICAgICAgICAgICAgICAgICAgPHAgY2xhc3M9XCJjYXJkLWRlc2NyaXB0aW9uXCI+Q2hlY2tib3ggYW5kIHJhZGlvIGNvbnRyb2xzIHdpdGggZmxhdCBkZXNpZ248L3A+XHJcbiAgICAgICAgICAgICAgICAgIDxmb3JtIGNsYXNzPVwiZm9ybXMtc2FtcGxlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC02XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tY2hlY2sgZm9ybS1jaGVjay1mbGF0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJmb3JtLWNoZWNrLWxhYmVsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBjbGFzcz1cImZvcm0tY2hlY2staW5wdXRcIj4gRGVmYXVsdFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1jaGVjayBmb3JtLWNoZWNrLWZsYXRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cImZvcm0tY2hlY2stbGFiZWxcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIGNsYXNzPVwiZm9ybS1jaGVjay1pbnB1dFwiIGNoZWNrZWQ+IENoZWNrZWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tY2hlY2sgZm9ybS1jaGVjay1mbGF0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJmb3JtLWNoZWNrLWxhYmVsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBjbGFzcz1cImZvcm0tY2hlY2staW5wdXRcIiBkaXNhYmxlZD4gRGlzYWJsZWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tY2hlY2sgZm9ybS1jaGVjay1mbGF0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJmb3JtLWNoZWNrLWxhYmVsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBjbGFzcz1cImZvcm0tY2hlY2staW5wdXRcIiBkaXNhYmxlZCBjaGVja2VkPiBEaXNhYmxlZCBjaGVja2VkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC02XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tcmFkaW8gZm9ybS1yYWRpby1mbGF0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJmb3JtLWNoZWNrLWxhYmVsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwicmFkaW9cIiBjbGFzcz1cImZvcm0tY2hlY2staW5wdXRcIiBuYW1lPVwiZmxhdFJhZGlvczFcIiBpZD1cImZsYXRSYWRpb3MxXCIgdmFsdWU9XCJcIiBjaGVja2VkPiBPcHRpb24gb25lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLXJhZGlvIGZvcm0tcmFkaW8tZmxhdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwiZm9ybS1jaGVjay1sYWJlbFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInJhZGlvXCIgY2xhc3M9XCJmb3JtLWNoZWNrLWlucHV0XCIgbmFtZT1cImZsYXRSYWRpb3MyXCIgaWQ9XCJmbGF0UmFkaW9zMlwiIHZhbHVlPVwib3B0aW9uMlwiPiBPcHRpb24gdHdvXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1yYWRpbyBmb3JtLXJhZGlvLWZsYXQgZGlzYWJsZWRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cImZvcm0tY2hlY2stbGFiZWxcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJyYWRpb1wiIGNsYXNzPVwiZm9ybS1jaGVjay1pbnB1dFwiIG5hbWU9XCJmbGF0UmFkaW9zM1wiIGlkPVwiZmxhdFJhZGlvczNcIiB2YWx1ZT1cIm9wdGlvbjNcIiBkaXNhYmxlZD4gT3B0aW9uIHRocmVlIGlzIGRpc2FibGVkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLXJhZGlvIGZvcm0tcmFkaW8tZmxhdCBkaXNhYmxlZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwiZm9ybS1jaGVjay1sYWJlbFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInJhZGlvXCIgY2xhc3M9XCJmb3JtLWNoZWNrLWlucHV0XCIgbmFtZT1cImZsYXRSYWRpb3M0XCIgaWQ9XCJmbGF0UmFkaW9zNFwiIHZhbHVlPVwib3B0aW9uNFwiIGRpc2FibGVkIGNoZWNrZWQ+IE9wdGlvbiBmb3VyIGlzIHNlbGVjdGVkIGFuZCBkaXNhYmxlZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPC9mb3JtPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLTEyIGdyaWQtbWFyZ2luXCI+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNhcmRcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjYXJkLWJvZHlcIj5cclxuICAgICAgICAgICAgICAgICAgPGg0IGNsYXNzPVwiY2FyZC10aXRsZVwiPkhvcml6b250YWwgVHdvIGNvbHVtbjwvaDQ+XHJcbiAgICAgICAgICAgICAgICAgIDxmb3JtIGNsYXNzPVwiZm9ybS1zYW1wbGVcIj5cclxuICAgICAgICAgICAgICAgICAgICA8cCBjbGFzcz1cImNhcmQtZGVzY3JpcHRpb25cIj5cclxuICAgICAgICAgICAgICAgICAgICAgIFBlcnNvbmFsIGluZm9cclxuICAgICAgICAgICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC02XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIHJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cImNvbC1zbS0zIGNvbC1mb3JtLWxhYmVsXCI+Rmlyc3QgTmFtZTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS05XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTZcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXAgcm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwiY29sLXNtLTMgY29sLWZvcm0tbGFiZWxcIj5MYXN0IE5hbWU8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tOVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtNlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cCByb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJjb2wtc20tMyBjb2wtZm9ybS1sYWJlbFwiPkdlbmRlcjwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS05XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c2VsZWN0IGNsYXNzPVwiZm9ybS1jb250cm9sXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24+TWFsZTwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPkZlbWFsZTwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zZWxlY3Q+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTZcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXAgcm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwiY29sLXNtLTMgY29sLWZvcm0tbGFiZWxcIj5EYXRlIG9mIEJpcnRoPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCBjbGFzcz1cImZvcm0tY29udHJvbFwiIHBsYWNlaG9sZGVyPVwiZGQvbW0veXl5eVwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC02XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIHJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cImNvbC1zbS0zIGNvbC1mb3JtLWxhYmVsXCI+Q2F0ZWdvcnk8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tOVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNlbGVjdCBjbGFzcz1cImZvcm0tY29udHJvbFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPkNhdGVnb3J5MTwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPkNhdGVnb3J5Mjwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPkNhdGVnb3J5Mzwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPkNhdGVnb3J5NDwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zZWxlY3Q+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTZcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXAgcm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwiY29sLXNtLTMgY29sLWZvcm0tbGFiZWxcIj5NZW1iZXJzaGlwPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLXJhZGlvXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cImZvcm0tY2hlY2stbGFiZWxcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInJhZGlvXCIgY2xhc3M9XCJmb3JtLWNoZWNrLWlucHV0XCIgbmFtZT1cIm1lbWJlcnNoaXBSYWRpb3NcIiBpZD1cIm1lbWJlcnNoaXBSYWRpb3MxXCIgdmFsdWU9XCJcIiBjaGVja2VkPiBGcmVlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLXJhZGlvXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cImZvcm0tY2hlY2stbGFiZWxcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInJhZGlvXCIgY2xhc3M9XCJmb3JtLWNoZWNrLWlucHV0XCIgbmFtZT1cIm1lbWJlcnNoaXBSYWRpb3NcIiBpZD1cIm1lbWJlcnNoaXBSYWRpb3MyXCIgdmFsdWU9XCJvcHRpb24yXCI+IFByb2Zlc3Npb25hbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPHAgY2xhc3M9XCJjYXJkLWRlc2NyaXB0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICBBZGRyZXNzXHJcbiAgICAgICAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtNlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cCByb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJjb2wtc20tMyBjb2wtZm9ybS1sYWJlbFwiPkFkZHJlc3MgMTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS05XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTZcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXAgcm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwiY29sLXNtLTMgY29sLWZvcm0tbGFiZWxcIj5TdGF0ZTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS05XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC02XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIHJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cImNvbC1zbS0zIGNvbC1mb3JtLWxhYmVsXCI+QWRkcmVzcyAyPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtNlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cCByb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJjb2wtc20tMyBjb2wtZm9ybS1sYWJlbFwiPlBvc3Rjb2RlPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTZcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXAgcm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwiY29sLXNtLTMgY29sLWZvcm0tbGFiZWxcIj5DaXR5PC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtNlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cCByb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJjb2wtc20tMyBjb2wtZm9ybS1sYWJlbFwiPkNvdW50cnk8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tOVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNlbGVjdCBjbGFzcz1cImZvcm0tY29udHJvbFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPkFtZXJpY2E8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbj5JdGFseTwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPlJ1c3NpYTwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPkJyaXRhaW48L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc2VsZWN0PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8L2Zvcm0+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtMTJcIj5cclxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY2FyZFwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNhcmQtYm9keSBwYi0wXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxoNCBjbGFzcz1cImNhcmQtdGl0bGUgbWItMFwiPkljaGVjazwvaDQ+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1sZy02IGdyaWQtbWFyZ2luIGdyaWQtbWFyZ2luLWxnLTBcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY2FyZC1ib2R5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8cCBjbGFzcz1cImNhcmQtZGVzY3JpcHRpb25cIj5NaW5pbWFsIHNraW48L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaWNoZWNrXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0YWJpbmRleD1cIjVcIiB0eXBlPVwiY2hlY2tib3hcIiBpZD1cIm1pbmltYWwtY2hlY2tib3gtMVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgZm9yPVwibWluaW1hbC1jaGVja2JveC0xXCI+Q2hlY2tib3ggMTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpY2hlY2tcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHRhYmluZGV4PVwiNlwiIHR5cGU9XCJjaGVja2JveFwiIGlkPVwibWluaW1hbC1jaGVja2JveC0yXCIgY2hlY2tlZD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cIm1pbmltYWwtY2hlY2tib3gtMlwiPkNoZWNrYm94IDI8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaWNoZWNrXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBpZD1cIm1pbmltYWwtY2hlY2tib3gtZGlzYWJsZWRcIiBkaXNhYmxlZD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cIm1pbmltYWwtY2hlY2tib3gtZGlzYWJsZWRcIj5EaXNhYmxlZDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpY2hlY2tcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIGlkPVwibWluaW1hbC1jaGVja2JveC1kaXNhYmxlZC1jaGVja2VkXCIgY2hlY2tlZCBkaXNhYmxlZD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cIm1pbmltYWwtY2hlY2tib3gtZGlzYWJsZWQtY2hlY2tlZFwiPkRpc2FibGVkICZhbXA7IGNoZWNrZWQ8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaWNoZWNrXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0YWJpbmRleD1cIjdcIiB0eXBlPVwicmFkaW9cIiBpZD1cIm1pbmltYWwtcmFkaW8tMVwiIG5hbWU9XCJtaW5pbWFsLXJhZGlvXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJtaW5pbWFsLXJhZGlvLTFcIj5SYWRpbyBidXR0b24gMTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpY2hlY2tcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHRhYmluZGV4PVwiOFwiIHR5cGU9XCJyYWRpb1wiIGlkPVwibWluaW1hbC1yYWRpby0yXCIgbmFtZT1cIm1pbmltYWwtcmFkaW9cIiBjaGVja2VkPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgZm9yPVwibWluaW1hbC1yYWRpby0yXCI+UmFkaW8gYnV0dG9uIDI8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaWNoZWNrXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwicmFkaW9cIiBpZD1cIm1pbmltYWwtcmFkaW8tZGlzYWJsZWRcIiBkaXNhYmxlZD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cIm1pbmltYWwtcmFkaW8tZGlzYWJsZWRcIj5EaXNhYmxlZDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpY2hlY2tcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJyYWRpb1wiIGlkPVwibWluaW1hbC1yYWRpby1kaXNhYmxlZC1jaGVja2VkXCIgY2hlY2tlZCBkaXNhYmxlZD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cIm1pbmltYWwtcmFkaW8tZGlzYWJsZWQtY2hlY2tlZFwiPkRpc2FibGVkICZhbXA7IGNoZWNrZWQ8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLWxnLTYgZ3JpZC1tYXJnaW4gZ3JpZC1tYXJnaW4tbGctMFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjYXJkLWJvZHlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzPVwiY2FyZC1kZXNjcmlwdGlvblwiPlNxdWFyZSBza2luPC9wPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImljaGVjay1zcXVhcmVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHRhYmluZGV4PVwiNVwiIHR5cGU9XCJjaGVja2JveFwiIGlkPVwic3F1YXJlLWNoZWNrYm94LTFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cInNxdWFyZS1jaGVja2JveC0xXCI+Q2hlY2tib3ggMTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpY2hlY2stc3F1YXJlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0YWJpbmRleD1cIjZcIiB0eXBlPVwiY2hlY2tib3hcIiBpZD1cInNxdWFyZS1jaGVja2JveC0yXCIgY2hlY2tlZD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cInNxdWFyZS1jaGVja2JveC0yXCI+Q2hlY2tib3ggMjwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpY2hlY2stc3F1YXJlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBpZD1cInNxdWFyZS1jaGVja2JveC1kaXNhYmxlZFwiIGRpc2FibGVkPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgZm9yPVwic3F1YXJlLWNoZWNrYm94LWRpc2FibGVkXCI+RGlzYWJsZWQ8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaWNoZWNrLXNxdWFyZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImNoZWNrYm94XCIgaWQ9XCJzcXVhcmUtY2hlY2tib3gtZGlzYWJsZWQtY2hlY2tlZFwiIGNoZWNrZWQgZGlzYWJsZWQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJzcXVhcmUtY2hlY2tib3gtZGlzYWJsZWQtY2hlY2tlZFwiPkRpc2FibGVkICZhbXA7IGNoZWNrZWQ8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaWNoZWNrLXNxdWFyZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdGFiaW5kZXg9XCI3XCIgdHlwZT1cInJhZGlvXCIgaWQ9XCJzcXVhcmUtcmFkaW8tMVwiIG5hbWU9XCJtaW5pbWFsLXJhZGlvXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJzcXVhcmUtcmFkaW8tMVwiPlJhZGlvIGJ1dHRvbiAxPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImljaGVjay1zcXVhcmVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHRhYmluZGV4PVwiOFwiIHR5cGU9XCJyYWRpb1wiIGlkPVwic3F1YXJlLXJhZGlvLTJcIiBuYW1lPVwibWluaW1hbC1yYWRpb1wiIGNoZWNrZWQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJzcXVhcmUtcmFkaW8tMlwiPlJhZGlvIGJ1dHRvbiAyPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImljaGVjay1zcXVhcmVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJyYWRpb1wiIGlkPVwic3F1YXJlLXJhZGlvLWRpc2FibGVkXCIgZGlzYWJsZWQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJzcXVhcmUtcmFkaW8tZGlzYWJsZWRcIj5EaXNhYmxlZDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpY2hlY2stc3F1YXJlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwicmFkaW9cIiBpZD1cInNxdWFyZS1yYWRpby1kaXNhYmxlZC1jaGVja2VkXCIgY2hlY2tlZCBkaXNhYmxlZD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cInNxdWFyZS1yYWRpby1kaXNhYmxlZC1jaGVja2VkXCI+RGlzYWJsZWQgJmFtcDsgY2hlY2tlZDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbGctNiBncmlkLW1hcmdpbiBncmlkLW1hcmdpbi1sZy0wXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNhcmQtYm9keVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPHAgY2xhc3M9XCJjYXJkLWRlc2NyaXB0aW9uXCI+RmxhdCBza2luPC9wPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImljaGVjay1mbGF0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0YWJpbmRleD1cIjVcIiB0eXBlPVwiY2hlY2tib3hcIiBpZD1cImZsYXQtY2hlY2tib3gtMVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgZm9yPVwiZmxhdC1jaGVja2JveC0xXCI+Q2hlY2tib3ggMTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpY2hlY2stZmxhdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdGFiaW5kZXg9XCI2XCIgdHlwZT1cImNoZWNrYm94XCIgaWQ9XCJmbGF0LWNoZWNrYm94LTJcIiBjaGVja2VkPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgZm9yPVwiZmxhdC1jaGVja2JveC0yXCI+Q2hlY2tib3ggMjwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpY2hlY2stZmxhdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImNoZWNrYm94XCIgaWQ9XCJmbGF0LWNoZWNrYm94LWRpc2FibGVkXCIgZGlzYWJsZWQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJmbGF0LWNoZWNrYm94LWRpc2FibGVkXCI+RGlzYWJsZWQ8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaWNoZWNrLWZsYXRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIGlkPVwiZmxhdC1jaGVja2JveC1kaXNhYmxlZC1jaGVja2VkXCIgY2hlY2tlZCBkaXNhYmxlZD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cImZsYXQtY2hlY2tib3gtZGlzYWJsZWQtY2hlY2tlZFwiPkRpc2FibGVkICZhbXA7IGNoZWNrZWQ8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaWNoZWNrLWZsYXRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHRhYmluZGV4PVwiN1wiIHR5cGU9XCJyYWRpb1wiIGlkPVwiZmxhdC1yYWRpby0xXCIgbmFtZT1cIm1pbmltYWwtcmFkaW9cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cImZsYXQtcmFkaW8tMVwiPlJhZGlvIGJ1dHRvbiAxPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImljaGVjay1mbGF0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0YWJpbmRleD1cIjhcIiB0eXBlPVwicmFkaW9cIiBpZD1cImZsYXQtcmFkaW8tMlwiIG5hbWU9XCJtaW5pbWFsLXJhZGlvXCIgY2hlY2tlZD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cImZsYXQtcmFkaW8tMlwiPlJhZGlvIGJ1dHRvbiAyPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImljaGVjay1mbGF0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwicmFkaW9cIiBpZD1cImZsYXQtcmFkaW8tZGlzYWJsZWRcIiBkaXNhYmxlZD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cImZsYXQtcmFkaW8tZGlzYWJsZWRcIj5EaXNhYmxlZDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpY2hlY2stZmxhdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInJhZGlvXCIgaWQ9XCJmbGF0LXJhZGlvLWRpc2FibGVkLWNoZWNrZWRcIiBjaGVja2VkIGRpc2FibGVkPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgZm9yPVwiZmxhdC1yYWRpby1kaXNhYmxlZC1jaGVja2VkXCI+RGlzYWJsZWQgJmFtcDsgY2hlY2tlZDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbGctNlwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjYXJkLWJvZHlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzPVwiY2FyZC1kZXNjcmlwdGlvblwiPkxpbmUgc2tpbjwvcD5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpY2hlY2stbGluZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdGFiaW5kZXg9XCI1XCIgdHlwZT1cImNoZWNrYm94XCIgaWQ9XCJsaW5lLWNoZWNrYm94LTFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cImxpbmUtY2hlY2tib3gtMVwiPkNoZWNrYm94IDE8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaWNoZWNrLWxpbmVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHRhYmluZGV4PVwiNlwiIHR5cGU9XCJjaGVja2JveFwiIGlkPVwibGluZS1jaGVja2JveC0yXCIgY2hlY2tlZD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cImxpbmUtY2hlY2tib3gtMlwiPkNoZWNrYm94IDI8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaWNoZWNrLWxpbmVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIGlkPVwibGluZS1jaGVja2JveC1kaXNhYmxlZFwiIGRpc2FibGVkPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgZm9yPVwibGluZS1jaGVja2JveC1kaXNhYmxlZFwiPkRpc2FibGVkPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImljaGVjay1saW5lXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBpZD1cImxpbmUtY2hlY2tib3gtZGlzYWJsZWQtY2hlY2tlZFwiIGNoZWNrZWQgZGlzYWJsZWQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJsaW5lLWNoZWNrYm94LWRpc2FibGVkLWNoZWNrZWRcIj5EaXNhYmxlZCAmYW1wOyBjaGVja2VkPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImljaGVjay1saW5lXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0YWJpbmRleD1cIjdcIiB0eXBlPVwicmFkaW9cIiBpZD1cImxpbmUtcmFkaW8tMVwiIG5hbWU9XCJtaW5pbWFsLXJhZGlvXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJsaW5lLXJhZGlvLTFcIj5SYWRpbyBidXR0b24gMTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpY2hlY2stbGluZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdGFiaW5kZXg9XCI4XCIgdHlwZT1cInJhZGlvXCIgaWQ9XCJsaW5lLXJhZGlvLTJcIiBuYW1lPVwibWluaW1hbC1yYWRpb1wiIGNoZWNrZWQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJsaW5lLXJhZGlvLTJcIj5SYWRpbyBidXR0b24gMjwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpY2hlY2stbGluZSBkaXNhYmxlZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInJhZGlvXCIgaWQ9XCJsaW5lLXJhZGlvLWRpc2FibGVkXCIgZGlzYWJsZWQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJsaW5lLXJhZGlvLWRpc2FibGVkXCI+RGlzYWJsZWQ8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaWNoZWNrLWxpbmUgZGlzYWJsZWRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJyYWRpb1wiIGlkPVwibGluZS1yYWRpby1kaXNhYmxlZC1jaGVja2VkXCIgY2hlY2tlZCBkaXNhYmxlZD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cImxpbmUtcmFkaW8tZGlzYWJsZWQtY2hlY2tlZFwiPkRpc2FibGVkICZhbXA7IGNoZWNrZWQ8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPCEtLSBjb250ZW50LXdyYXBwZXIgZW5kcyAtLT5cclxuICAgICAgICA8IS0tIHBhcnRpYWw6cGFydGlhbHMvX2Zvb3Rlci5odG1sIC0tPlxyXG4gICAgICAgIDxmb290ZXIgY2xhc3M9XCJmb290ZXJcIj5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb250YWluZXItZmx1aWQgY2xlYXJmaXhcIj5cclxuICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJ0ZXh0LW11dGVkIGQtYmxvY2sgdGV4dC1jZW50ZXIgdGV4dC1zbS1sZWZ0IGQtc20taW5saW5lLWJsb2NrXCI+Q29weXJpZ2h0IMKpIDIwMThcclxuICAgICAgICAgICAgICA8YSBocmVmPVwiaHR0cDovL3d3dy51cmJhbnVpLmNvbS9cIiB0YXJnZXQ9XCJfYmxhbmtcIj5VcmJhbnVpPC9hPi4gQWxsIHJpZ2h0cyByZXNlcnZlZC48L3NwYW4+XHJcbiAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiZmxvYXQtbm9uZSBmbG9hdC1zbS1yaWdodCBkLWJsb2NrIG10LTEgbXQtc20tMCB0ZXh0LWNlbnRlclwiPkhhbmQtY3JhZnRlZCAmIG1hZGUgd2l0aFxyXG4gICAgICAgICAgICAgIDxpIGNsYXNzPVwibWRpIG1kaS1oZWFydCB0ZXh0LWRhbmdlclwiPjwvaT5cclxuICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9mb290ZXI+XHJcbiAgICAgICAgPCEtLSBwYXJ0aWFsIC0tPlxyXG4gICAgICA8L2Rpdj5cclxuICAgICAgPCEtLSBtYWluLXBhbmVsIGVuZHMgLS0+XHJcbiAgICA8L2Rpdj5cclxuICAgIDwhLS0gcGFnZS1ib2R5LXdyYXBwZXIgZW5kcyAtLT5cclxuICA8L2Rpdj5cclxuICA8c2NyaXB0PlxyXG4gIDwvc2NyaXB0PlxyXG48L2FwcD4iXX0=
